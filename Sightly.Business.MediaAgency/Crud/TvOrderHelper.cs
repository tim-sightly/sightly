using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class TvOrderHelper : ITvOrderHelper
    {
        private readonly ITvDbRepository _tvDbRepository;
        private readonly IOrderTargetAgeGroupHelper _targetAgeGroupHelper;
        private readonly IOrderTargetGenderHelper _targetGenderHelper;

        public TvOrderHelper(ITvDbRepository tvDbRepository, IOrderTargetAgeGroupHelper targetAgeGroupHelper, IOrderTargetGenderHelper targetGenderHelper)
        {
            _tvDbRepository = tvDbRepository;
            _targetAgeGroupHelper = targetAgeGroupHelper;
            _targetGenderHelper = targetGenderHelper;
        }

        public TvOrder CheckExistanceOfOrderByName(string orderName)
        {
            var order = _tvDbRepository.GetOrderDataByName(orderName);
            return order;
        }

        public TvOrder CreateOrderIfNotExisting(
            string orderName, 
            Guid accountId, 
            Guid advertiserId, 
            Guid campaignId,
            Guid orderStatusId, 
            decimal campaignBudget, 
            DateTime startDate, 
            DateTime endDate)
        {
            var order = CheckExistanceOfOrderByName(orderName);
            if (order != null) return order;
            
            //this is a new order , we will add the Gender and AGE targeting here since we don't need it anywhere else


            order = new TvOrder
            {
                OrderId = Guid.NewGuid(),
                OrderName = orderName,
                OrderStatusId = orderStatusId,
                AccountId = accountId,
                AdvertiserId = advertiserId,
                CampaignBudget = campaignBudget,
                CampaignId = campaignId,
                StartDate = startDate,
                EndDate = endDate,
                CreatedBy = "Data Pipeline"
            };

            _tvDbRepository.InsertOrder(order);
            
            // Only on create we make the default header for the Age and Gender targeting
            _targetAgeGroupHelper.CreateOrderTargetAgeGroup(order.OrderId,
                Guid.Parse("F3E31A02-EBE2-4D07-9D7F-3E5FFB03D909")); //Unknown Age

            _targetGenderHelper.CreateOrderTargetGender(order.OrderId,
                Guid.Parse("62BA835D-5AF0-49EC-AEC3-8492B936BD89")); //Unknown Gender

            return order;

        }
    }
}