using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class TvAdHelper : ITvAdHelper
    {

        private readonly ITvDbRepository _tvDbRepository;
        private readonly IAdAdwordsInfoHelper _adAdwordsInfoHelper;
        private readonly IBudgetGroupXrefAdHelper _budgetGroupXrefAdHelper;

        public TvAdHelper(ITvDbRepository tvDbRepository, IAdAdwordsInfoHelper adAdwordsInfoHelper, IBudgetGroupXrefAdHelper budgetGroupXrefAdHelper)
        {
            _tvDbRepository = tvDbRepository;
            _adAdwordsInfoHelper = adAdwordsInfoHelper;
            _budgetGroupXrefAdHelper = budgetGroupXrefAdHelper;
        }

        public TvAd CheckExistanceOfAdByVideoAssetName(string videoAssetName)
        {
            var ad = _tvDbRepository.GetAdByVideoAssetName(videoAssetName);
            return ad;
        }

        public TvAd CreateAdIfNotExisting(Guid accountId, Guid advertiserId, Guid videoAssetVersionId, string videoAssetName, string destinationUrl, string displayUrl)
        {
            var ad = CheckExistanceOfAdByVideoAssetName(videoAssetName);
            if (ad != null) return ad;

            ad = new TvAd
            {
                AdId = Guid.NewGuid(),
                VideoAdName = videoAssetName,
                DestinationUrl = destinationUrl,
                DisplayUrl = displayUrl,
                VideoAssetVersionId = videoAssetVersionId,
                CreatedBy = "Data Pipeline"
            };

            _tvDbRepository.InsertAd(ad);

            return ad;
        }

        public TvAd CreateAdAndRelationshipsIfNotExisting(Guid accountId, Guid advertiserId, Guid videoAssetVersionId, string videoAssetVersionName, 
            string destinationUrl, string displayUrl, string adName, long adId, long awCampaignId, Guid budgetGroupId)
        {
            var ad = CreateAdIfNotExisting(accountId, advertiserId, videoAssetVersionId, videoAssetVersionName, destinationUrl, displayUrl);

            var adAdwordsInfo = _adAdwordsInfoHelper.CreateAdAdwordsInfoIfNotExist(ad.AdId, adName, adId, awCampaignId, budgetGroupId);

            var budgetGroupXrefAd = _budgetGroupXrefAdHelper.CreateBudgetGroupXrefAdIfNotExist(budgetGroupId, ad.AdId);

            return ad;
        }
    }
}