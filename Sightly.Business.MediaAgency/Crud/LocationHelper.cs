using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class LocationHelper : ILocationHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public LocationHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvLocation GetExistingLocation(Guid accountId, string locationName)
        {
            var location = _tvDbRepository.GetlocationByAccountAndName(accountId, locationName);
            return location;
        }

        public TvLocation CreateLocation(Guid accountId, string locationName)
        {
            var location = new TvLocation
            {
                LocationId = Guid.NewGuid(),
                LocationName = locationName,
                AccountId = accountId,
                CreatedBy = "Data Pipeline"

            };

            _tvDbRepository.InsertLocation(location);

            return location;
        }

        public TvLocation CreateLocationIfNotExisting(Guid accountId, string awCampaignName)
        {
            var location = GetExistingLocation(accountId, awCampaignName);
            if (location != null) return location;

            location = CreateLocation(accountId, awCampaignName);

            return location;
        }
    }
}