using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class PlacementHelper : IPlacementHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public PlacementHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }


        public TvPlacement CheckExistanceOfPlacementByNameAndValue(string placementName, string placementValue)
        {
            var placement = _tvDbRepository.GetPlacementByNameAndValue(placementName, placementValue);
            return placement;
        }

        public TvPlacement CreatePlacementIfNotExisting(Guid advertiserId, string placementName, string placementValue)
        {
            var placement = CheckExistanceOfPlacementByNameAndValue(placementName, placementValue);
            if (placement != null) return placement;

            placement = new TvPlacement()
            {
                PlacementId = Guid.NewGuid(),
                AdvertiserId = advertiserId,
                PlacementName = placementName,
                PlacementValue = placementValue,
                CreatedBy = "Data Pipeline"
            };

            _tvDbRepository.InsertPlacement(placement);

            return placement;
        }
    }
}