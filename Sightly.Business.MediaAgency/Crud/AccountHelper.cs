using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class AccountHelper : IAccountHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public AccountHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvAccount GetAccountByName(string accountName)
        {
            var account = _tvDbRepository.GetAccountDataByName(accountName);
            return account;
        }

        public TvAccount GetAccountByCampaignName(string campaignName)
        {
            var account = _tvDbRepository.GetAccountDataByCampaignName(campaignName);
            return account;
        }

        public TvAccount GetAccountByCampaignId(Guid campaignId)
        {
            var account = _tvDbRepository.GetAccountDateByCampaignId(campaignId);
            return account;
        }
    }
}