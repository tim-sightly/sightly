using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class OrderTargetAgeGroupHelper : IOrderTargetAgeGroupHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public OrderTargetAgeGroupHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvOrderTargetAgeGroup CreateOrderTargetAgeGroup(Guid orderId, Guid ageGroupId)
        {
            var orderTargetAgeGroup = new TvOrderTargetAgeGroup
            {
                AgeGroupId = ageGroupId,
                OrderId = orderId,
                LastModifiedBy = "Data Pipeline"
            };

            _tvDbRepository.InsertTargetAgeGroup(orderTargetAgeGroup);

            return orderTargetAgeGroup;
        }
    }
}