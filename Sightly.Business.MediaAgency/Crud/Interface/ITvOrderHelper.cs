using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface ITvOrderHelper
    {
        TvOrder CheckExistanceOfOrderByName(string orderName);

        TvOrder CreateOrderIfNotExisting(string orderName, 
            Guid accountId, 
            Guid advertiserId, 
            Guid campaignId,
            Guid orderStatusId, 
            decimal campaignBudget, 
            DateTime startDate, 
            DateTime endDate);
    }
}