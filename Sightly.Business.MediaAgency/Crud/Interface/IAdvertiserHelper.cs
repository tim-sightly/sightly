using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IAdvertiserHelper
    {
        TvAdvertiser GetAdvertiserByName(string advertiserName);
        TvAdvertiser GetAdvertiserByCampaignName(string campaignName);
        TvAdvertiser GetAdvertiserByCampaignId(Guid campaignId);
        AccountMetaData GetAdvertiserMetaData(Guid infoCampaign);
    }
}