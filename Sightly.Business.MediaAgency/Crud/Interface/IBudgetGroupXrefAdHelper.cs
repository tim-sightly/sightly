using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IBudgetGroupXrefAdHelper
    {
        TvBudgetGroupXrefAd GetExistingBudgetGroupXrefAd(Guid budgetGroupId, Guid adId);
        TvBudgetGroupXrefAd CreateBudgetGroupXrefAd(
            Guid budgetGroupId,
            Guid adId);

        TvBudgetGroupXrefAd CreateBudgetGroupXrefAdIfNotExist(Guid budgetGroupId, Guid adAdId);
    }
}