using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IBudgetGroupXrefPlacementHelper
    {
        TvBudgetGroupXrefPlacement CheckExistingBudgetGroupXrefPlacement(
            Guid budgetGroupId,
            Guid placementId);
        TvBudgetGroupXrefPlacement CreateBudgetGroupXrefPlacement(
            Guid budgetGroupId,
            Guid placementId);
        
    }
}