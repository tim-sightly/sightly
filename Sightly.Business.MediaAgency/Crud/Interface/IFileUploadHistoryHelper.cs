namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IFileUploadHistoryHelper
    {
        bool IsCustomerLocked(long customerId);
        void LockCustomer(long customerId, string email, string importType);
        void UnlockCustomer(long customerId, string email);
    }
}