using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface ITvAdHelper
    {
        TvAd CheckExistanceOfAdByVideoAssetName(string videoAssetName);

        TvAd CreateAdIfNotExisting(Guid accountId, Guid advertiserId, Guid videoAssetVersionId, string videoAssetName, string destinationUrl, string displayUrl);

        TvAd CreateAdAndRelationshipsIfNotExisting(Guid accountId, Guid advertiserId, Guid videoAssetVersionId, string videoAssetVersionName, string destinationUrl, string displayUrl, string adName, long adId, long awCampaignId, Guid budgetGroupId);
    }
}