using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IBudgetGroupAdWordsInfoHelper
    {
        AdMetaData GetAdMetaData(long customerId, long campaignId);
        TvBudgetGroupAdwordsInfo GetExistingBudgetGroupAdwordsInfo(long campaignId);
        TvBudgetGroupAdwordsInfo CreateBudgetGroupAdwordsInfo(
            Guid budgetGroupId,
            long customerId,
            long campaignId,
            string campaignName);

        TvBudgetGroupAdwordsInfo CreateBudgetGroupAdwordsInfoIfNotExisting(
            Guid budgetGroupId, 
            long awCustomerId, 
            long awCampaignId, 
            string awCampaignName);
    }
}