using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IOrderTargetAgeGroupHelper
    {
        TvOrderTargetAgeGroup CreateOrderTargetAgeGroup(Guid orderId, Guid ageGroupId);
    }
}