using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface ICampaignAdWordsInfoHelper
    {
        TvCampaignAdWordsInfo CheckExistanceOfCampaignAdWordsInfoBycustomerId(long customerId);

        TvCampaignAdWordsInfo CreateCampaignAdWordsInfoIfNotExisting(long awCustomerId, Guid advertiserId, Guid campaignId, string campaignMangerEmail);
        string GetTvCustomerNameByCustomerId(long customerId);
        TvBudgetGroupAndPlacement DoesCampaignExistReturnXrefPlacementId(long awCampaignId);
        void EnsureProperCampaignMangerAssignedToCustomer(long customerId, string campaignMangerEmail);
    }
}