using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IAdAdwordsInfoHelper
    {
        TvAdAdwordsInfo GetExistingAdAdwordsInfo(long awAdId);
        TvAdAdwordsInfo CreateAdAdwordsInfo(
            Guid adId, 
            string awAdName, 
            long awAdId, 
            long awCampaignId,
            Guid budgetGroupId);

        TvAdAdwordsInfo CreateAdAdwordsInfoIfNotExist(Guid adId, string awAdName, long awAdId, long awCampaignId, Guid budgetGroupId);
    }
}