using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IBudgetGroupTimedBudgetHelper
    {
        void PrepareBudgetGroupTimedBudgetsIfReorder(long awcustomerId);

        TvBudgetGroupTimedBudget GetExistingBudgetGroupTimedBudget(
            Guid budgetGroupId, 
            DateTime startDate,
            Guid budgetGroupXrefPlacementId);
        TvBudgetGroupTimedBudget CreateBudgetGroupTimedBudget(
            Guid budgetGroupId,
            decimal budgetGroupAmount,
            DateTime startDate,
            DateTime endDate,
            decimal margin,
            Guid budgetGroupXrefPlacementId,
            Guid orderId);

        TvBudgetGroupTimedBudget CreateBudgetGroupTimedBudgetIfNotExisting(
            Guid budgetGroupId, 
            decimal campaignBudget, 
            DateTime startDate, 
            DateTime endDate, 
            decimal margin, 
            Guid budgetGroupXrefPlacementId,
            Guid orderId);

        TvBudgetGroupTimedBudget UpdateLiveBudgetGroupTimedBudget(
            Guid BudgetGroupTimedBudgetId,
            decimal campaignBudget,
            DateTime startDate,
            DateTime endDate,
            decimal margin,
            Guid budgetGroupXrefPlacementId);

        void UpdateHistoricalBudgetGroupTimedBudget(
            Guid BudgetGroupTimedBudgetId,
            decimal campaignBudget,
            DateTime startDate,
            DateTime endDate,
            decimal margin,
            Guid budgetGroupXrefPlacementId);

        void UpdateProposedBudgetGroupTimedBudget(
            Guid BudgetGroupTimedBudgetId, 
            decimal updatedBudget, 
            DateTime startDate, 
            DateTime endDate,
            decimal margin,
            Guid budgetGroupXrefPlacementId);

        TvBudgetGroupTimedBudget InsertBudgetGroupTimedBudget(
            Guid budgetGroupId,
            decimal campaignBudget,
            DateTime startDate,
            DateTime endDate,
            decimal margin,
            Guid budgetGroupXrefPlacementId,
            Guid orderId);

        Guid? FindLiveBudgetGroupXrefPlacement(Guid budgetGroupXrefPlacementId);
    }
}