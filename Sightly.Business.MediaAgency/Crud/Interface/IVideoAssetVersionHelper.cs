using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IVideoAssetVersionHelper
    {
        TvVideoAssetVersion CheckExistingVideoAssetVersionByYouTubeId(string youTubeId);
        TvVideoAssetVersion CreateVideoAssetVersionIfNotExisting(
            Guid videoAssetId,
            string videoAssetVersionName,
            string videoAssetVersionRefCode,
            int videoLengthInSeconds,
            string youTubeId,
            string youTubeUrl);

        TvVideoAssetVersion CreateVideoAssetAndVersionIfNotExisting(Guid accountId, 
            Guid advertiserId, 
            string videoId, 
            string videoName, 
            int videoDuration);
    }
}