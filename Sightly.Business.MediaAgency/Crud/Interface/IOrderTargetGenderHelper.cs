using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IOrderTargetGenderHelper
    {
        TvOrderTargetGender CreateOrderTargetGender(Guid orderId, Guid genderId);
    }
}