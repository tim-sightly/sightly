using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IOrderXrefBudgetGroupHelper
    {
        TvOrderXrefBudgetGroup GetExistingOrderXrefBudgetGroup(Guid orderId, Guid budgetGroupId);
        TvOrderXrefBudgetGroup CreateOrderXrefBudgetGroup(Guid orderId, Guid budgetGroupId);
        TvOrderXrefBudgetGroup CreateOrderXrefBudgetGroupIfNotExisting(Guid orderId, Guid budgetGroupId);
    }
}