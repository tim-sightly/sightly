using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface ICampaignHelper
    {
        TvCampaign CheckExistanceOfCampaignByName(string campaignName);
        TvCampaign CreateCampaignIfNotExisting(string advertiserName, string campaignName);
        TvCampaign GetCampaignByCampaignId(Guid campaignId);
        TvCampaign GetCampaignByOrderName(string orderName);
    }
}