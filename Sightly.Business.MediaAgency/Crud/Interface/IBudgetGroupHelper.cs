using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IBudgetGroupHelper
    {
        TvBudgetGroup GetExistingBudgetGroup(string budgetGroupName, decimal budgetGroupBudget, Guid orderId);
        TvBudgetGroup CreateBudgetGroup(string budgetGroupName, decimal budgetGroupBudget);
        TvBudgetGroup CreateBudgetGroupIfNotExisting(string budgetGroupName, decimal budgetGroupBudget, Guid orderId);
        TvBudgetGroup GetExistingBudgetGroupByAWCampaignId(long awCampaignId);
        void UpdateBudgetGroupStructureFromProposeToLive(Guid budgetGroupId);
        List<TvBudgetGroup> GetBudgetGroupsByAwCustomerId(long customerId);
    }
}
    