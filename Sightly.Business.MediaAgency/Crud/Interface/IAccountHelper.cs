using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IAccountHelper
    {
        TvAccount GetAccountByName(string accountName);
        TvAccount GetAccountByCampaignName(string campaignName);
        TvAccount GetAccountByCampaignId(Guid campaignId);
    }
}