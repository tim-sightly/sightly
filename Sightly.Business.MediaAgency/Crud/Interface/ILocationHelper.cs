using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface ILocationHelper
    {
        TvLocation GetExistingLocation(Guid accountId, string locationName);
        TvLocation CreateLocation(Guid accountId, string locationName);
        TvLocation CreateLocationIfNotExisting(Guid accountId, string awCampaignName);
    }
}