using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IBudgetGroupXrefLocationHelper
    {
        TvBudgetGroupXrefLocation GetExistingBudgetGroupXrefLocation(
            Guid budgetGroupId,
            Guid locationId);

        TvBudgetGroupXrefLocation CreateBudgetGroupXrefLocation(
            Guid budgetGroupId,
            Guid locationId);

        TvBudgetGroupXrefLocation CreateBudgetGroupXrefLocationIfNotExisting(
            Guid budgetGroupId, 
            Guid locationId);
    }
}