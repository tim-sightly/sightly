using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IPlacementHelper
    {
        TvPlacement CheckExistanceOfPlacementByNameAndValue(string placementName, string placementValue);
        TvPlacement CreatePlacementIfNotExisting(Guid advertiserId, string placementName, string placementValue);
    }
}