using System;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.Crud.Interface
{
    public interface IVideoAssetHelper
    {
        TvVideoAsset CheckExistanceVideoAssetByVideoAssetName(string videoAsssetName);

        TvVideoAsset CreateVideoAssetIfNotExisting(
            Guid accountId,
            Guid advertiserId,
            string videoAssetFileName,
            string videoAssetName,
            string videoAssetRefCode);
    }
}