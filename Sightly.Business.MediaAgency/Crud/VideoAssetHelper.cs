using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class VideoAssetHelper : IVideoAssetHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public VideoAssetHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvVideoAsset CheckExistanceVideoAssetByVideoAssetName(string videoAsssetName)
        {
            var videoAsset = _tvDbRepository.GetVideoAssetDataByVideoAssetName(videoAsssetName);
            return videoAsset;
        }

        public TvVideoAsset CreateVideoAssetIfNotExisting(Guid accountId, Guid advertiserId, string videoAssetFileName,
            string videoAssetName, string videoAssetRefCode)
        {
            var videoAsset = CheckExistanceVideoAssetByVideoAssetName(videoAssetName);
            if (videoAsset != null) return videoAsset;

            videoAsset = new TvVideoAsset
            {
                VideoAssetId =  Guid.NewGuid(),
                AccountId =  accountId,
                AdvertiserId = advertiserId,
                VideoAssetName = videoAssetName,
                VideoAssetFileName =  videoAssetFileName,
                VideoAssetRefCode = videoAssetRefCode,
                CreatedBy = "Data Pipeline"
            };

            _tvDbRepository.InsertVideoAsset(videoAsset);

            return videoAsset;
        }
    }
}