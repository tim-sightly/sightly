using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class AdvertiserHelper : IAdvertiserHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public AdvertiserHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvAdvertiser GetAdvertiserByName(string advertiserName)
        {
            var advertiser = _tvDbRepository.GetAdvertiserDataByName(advertiserName);
            return advertiser;
        }

        public TvAdvertiser GetAdvertiserByCampaignName(string campaignName)
        {
            var advertiser = _tvDbRepository.GetAdvertiserDataByCampaignName(campaignName);
            return advertiser;
        }

        public TvAdvertiser GetAdvertiserByCampaignId(Guid campaignId)
        {
            var advertiser = _tvDbRepository.GetAdvertiserDataByCampaignId(campaignId);
            return advertiser;
        }

        public AccountMetaData GetAdvertiserMetaData(Guid infoCampaign)
        {
            return _tvDbRepository.GetAdvertiserMetaData(infoCampaign);
        }
    }
}