using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class AdAdwordsInfoHelper : IAdAdwordsInfoHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public AdAdwordsInfoHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvAdAdwordsInfo GetExistingAdAdwordsInfo(long awAdId)
        {
            var adAdwordsInfo = _tvDbRepository.GetAdAdwordsInfo(awAdId);
            return adAdwordsInfo;
        }

        public TvAdAdwordsInfo CreateAdAdwordsInfo(Guid adId, string awAdName, long awAdId, long awCampaignId, Guid budgetGroupId)
        {
            var tvAdAdwordsInfo = new TvAdAdwordsInfo
            {
                AdAdwordsInfoId = Guid.NewGuid(),
                AdId = adId,
                AdWordsAdId = awAdId,
                AdWordsAdName = awAdName,
                AdWordsCampaignId = awCampaignId,
                BudgetGroupId = budgetGroupId,
                CreatedBy = "Data Pipeline"
            };

            _tvDbRepository.InsertAdAdwordsInfo(tvAdAdwordsInfo);

            return tvAdAdwordsInfo;
        }

        public TvAdAdwordsInfo CreateAdAdwordsInfoIfNotExist(Guid adId, string awAdName, long awAdId, long awCampaignId,
            Guid budgetGroupId)
        {
            var adAdwordsInfo = GetExistingAdAdwordsInfo(awAdId);
            if (adAdwordsInfo != null) return adAdwordsInfo;

            adAdwordsInfo = CreateAdAdwordsInfo(adId, awAdName, awAdId, awCampaignId, budgetGroupId);
            return adAdwordsInfo;
        }
    }
}