using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class OrderTargetGenderHelper : IOrderTargetGenderHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public OrderTargetGenderHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvOrderTargetGender CreateOrderTargetGender(Guid orderId, Guid genderId)
        {
            var orderTargetGender = new TvOrderTargetGender
            {
                GenderId = genderId,
                OrderId = orderId,
                LastModifiedBy = "Data Pipeline"
            };

            _tvDbRepository.InsertTargetGender(orderTargetGender);

            return orderTargetGender;
        }
    }
}