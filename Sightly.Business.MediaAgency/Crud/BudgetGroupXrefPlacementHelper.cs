using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class BudgetGroupXrefPlacementHelper : IBudgetGroupXrefPlacementHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public BudgetGroupXrefPlacementHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvBudgetGroupXrefPlacement CheckExistingBudgetGroupXrefPlacement(Guid budgetGroupId, Guid placementId)
        {
            var budgetGroupPlacement = _tvDbRepository.GetBudgetGroupXrefPlacement(budgetGroupId, placementId);
            return budgetGroupPlacement;
        }

        public TvBudgetGroupXrefPlacement CreateBudgetGroupXrefPlacement(Guid budgetGroupId, Guid placementId)
        {
            var budgetGroupXrefPlacement = CheckExistingBudgetGroupXrefPlacement(budgetGroupId, placementId);
            if (budgetGroupXrefPlacement != null) return budgetGroupXrefPlacement;

            budgetGroupXrefPlacement = new TvBudgetGroupXrefPlacement
            {
                BudgetGroupXrefPlacementId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupId,
                PlacementId = placementId,
                AssignedBy = "Data Pipeline",
                AssignedDate = DateTime.UtcNow
            };

            _tvDbRepository.InsertBudgetGroupXrefPlacement(budgetGroupXrefPlacement);

            return budgetGroupXrefPlacement;
        }
    }
}