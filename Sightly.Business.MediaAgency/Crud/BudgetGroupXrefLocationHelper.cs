using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class BudgetGroupXrefLocationHelper : IBudgetGroupXrefLocationHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public BudgetGroupXrefLocationHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvBudgetGroupXrefLocation GetExistingBudgetGroupXrefLocation(Guid budgetGroupId, Guid locationId)
        {
            var budgetGroupXrefLocation = _tvDbRepository.GetBudgetGroupXrefLocation(budgetGroupId, locationId);
            return budgetGroupXrefLocation;
        }

        public TvBudgetGroupXrefLocation CreateBudgetGroupXrefLocation(Guid budgetGroupId, Guid locationId)
        {
            var budgetGroupXrefLocation = new TvBudgetGroupXrefLocation
            {
                BudgetGroupXrefLocationId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupId,
                LocationId = locationId,
                CreatedBy = "Data Pipeline"
            };

            _tvDbRepository.InsertBudgetGroupXrefLocation(budgetGroupXrefLocation);

            return budgetGroupXrefLocation;
        }

        public TvBudgetGroupXrefLocation CreateBudgetGroupXrefLocationIfNotExisting(Guid budgetGroupId, Guid locationId)
        {
            var budgetGroupXrefLocation = GetExistingBudgetGroupXrefLocation(budgetGroupId, locationId);
            if (budgetGroupXrefLocation != null) return budgetGroupXrefLocation;

            budgetGroupXrefLocation = CreateBudgetGroupXrefLocation(budgetGroupId, locationId);

            return budgetGroupXrefLocation;
        }
    }
}