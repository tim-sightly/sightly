using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class BudgetGroupAdWordsInfoHelper : IBudgetGroupAdWordsInfoHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public BudgetGroupAdWordsInfoHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public AdMetaData GetAdMetaData(long customerId, long campaignId)
        {
            var adMetaData = _tvDbRepository.GetAdMetaData(customerId, campaignId);
            return adMetaData;
        }

        public TvBudgetGroupAdwordsInfo GetExistingBudgetGroupAdwordsInfo(long campaignId)
        {
            var budgetGroupAdwordsInfo = _tvDbRepository.GetBudgetGroupAdwordsInfo(campaignId);
            return budgetGroupAdwordsInfo;
        }

        public TvBudgetGroupAdwordsInfo CreateBudgetGroupAdwordsInfo(Guid budgetGroupId, long customerId, long campaignId,
            string campaignName)
        {
            var budgetGroupAdwordsInfo = new TvBudgetGroupAdwordsInfo
            {
                BudgetGroupAdwordsInfoId = Guid.NewGuid(),
                AdwordsCustomerId = customerId,
                AdwordsCampaignId = campaignId,
                AdwordsCampaignName = campaignName,
                BudgetGroupId = budgetGroupId,
                CreatedBy = "Data Pipeline"
            };

            _tvDbRepository.InsertBudgetGroupAdwordsInfo(budgetGroupAdwordsInfo);
            return budgetGroupAdwordsInfo;
        }

        public TvBudgetGroupAdwordsInfo CreateBudgetGroupAdwordsInfoIfNotExisting(Guid budgetGroupId, long awCustomerId,
            long awCampaignId, string awCampaignName)
        {
            var budgetGroupAdwordsInfo = GetExistingBudgetGroupAdwordsInfo(awCampaignId);
            if (budgetGroupAdwordsInfo != null) return budgetGroupAdwordsInfo;

            budgetGroupAdwordsInfo = CreateBudgetGroupAdwordsInfo(budgetGroupId, awCustomerId, awCampaignId, awCampaignName);

            return budgetGroupAdwordsInfo;
        }
    }
}