using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class FileUploadHistoryHelper : IFileUploadHistoryHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public FileUploadHistoryHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }


        public bool IsCustomerLocked(long customerId)
        {
            return _tvDbRepository.IsCustomerLocked(customerId);
        }

        public void LockCustomer(long customerId, string email, string importType)
        {
            _tvDbRepository.LockCustomer(customerId, email, importType);
        }

        public void UnlockCustomer(long customerId, string email)
        {
            _tvDbRepository.UnlockCustomer(customerId, email);
        }
    }
}