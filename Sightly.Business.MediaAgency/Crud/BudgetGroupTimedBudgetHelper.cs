using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class BudgetGroupTimedBudgetHelper : IBudgetGroupTimedBudgetHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public BudgetGroupTimedBudgetHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public void PrepareBudgetGroupTimedBudgetsIfReorder(long awcustomerId)
        {
            _tvDbRepository.PrepareBudgetGroupTimedBudgetsIfReorder(awcustomerId);
        }

        public TvBudgetGroupTimedBudget GetExistingBudgetGroupTimedBudget(Guid budgetGroupId, DateTime startDate, Guid budgetGroupXrefPlacementId)
        {
            var budgetGroupTimedBudget = _tvDbRepository.GetBudgetGroupTimedBudget(budgetGroupId, startDate, budgetGroupXrefPlacementId);
            return budgetGroupTimedBudget;
        }

        public TvBudgetGroupTimedBudget CreateBudgetGroupTimedBudget(Guid budgetGroupId, decimal budgetGroupAmount, DateTime startDate,
            DateTime endDate, decimal margin, Guid budgetGroupXrefPlacementId, Guid orderId)
        {
            var budgetGroupTimedBudget = new TvBudgetGroupTimedBudget
            {
                BudgetGroupTimedBudgetId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupId,
                BudgetAmount = budgetGroupAmount,
                Margin = margin,
                StartDate = startDate,
                EndDate = endDate,
                CreatedBy = "Data Pipeline",
                BudgetGroupXrefPlacementId = budgetGroupXrefPlacementId,
                OrderId = orderId

            };

            _tvDbRepository.InsertBudgetGroupTimedBudget(budgetGroupTimedBudget);

            return budgetGroupTimedBudget;
        }
        

        public TvBudgetGroupTimedBudget CreateBudgetGroupTimedBudgetIfNotExisting(Guid budgetGroupId, decimal campaignBudget,
            DateTime startDate, DateTime endDate, decimal margin, Guid budgetGroupXrefPlacementId, Guid orderId)
        {
            var budgetGroupTimedBudget = GetExistingBudgetGroupTimedBudget(budgetGroupId, startDate, budgetGroupXrefPlacementId);
            if (budgetGroupTimedBudget != null) return budgetGroupTimedBudget;

            budgetGroupTimedBudget = CreateBudgetGroupTimedBudget(
                budgetGroupId, 
                campaignBudget, 
                startDate, 
                endDate,
                margin, 
                budgetGroupXrefPlacementId,
                orderId);

            return budgetGroupTimedBudget;
        }

        public TvBudgetGroupTimedBudget UpdateLiveBudgetGroupTimedBudget(
            Guid budgetGroupTimedBudgetId, 
            decimal campaignBudget, 
            DateTime startDate,
            DateTime endDate, 
            decimal margin,
            Guid budgetGroupXrefPlacementId)
        {
           return _tvDbRepository.UpdateBudgetGroupTimedBudget(
            budgetGroupTimedBudgetId,
            campaignBudget,
            startDate,
            endDate,
            margin,
            budgetGroupXrefPlacementId);
        }

        public void UpdateHistoricalBudgetGroupTimedBudget(
            Guid budgetGroupTimedBudgetId, 
            decimal campaignBudget, 
            DateTime startDate, 
            DateTime endDate, 
            decimal margin,
            Guid budgetGroupXrefPlacementId)
        {
            _tvDbRepository.UpdateHistoricalBudgetGroupTimedBudget(
                budgetGroupTimedBudgetId,
                campaignBudget,
                startDate,
                endDate,
                margin,
                budgetGroupXrefPlacementId);
        }

        public void UpdateProposedBudgetGroupTimedBudget(
            Guid budgetGroupTimedBudgetId, 
            decimal campaignBudget, 
            DateTime startDate,
            DateTime endDate, 
            decimal margin,
            Guid budgetGroupXrefPlacementId)
        {

            _tvDbRepository.UpdateProposedBudgetGroupTimedBudget(
                budgetGroupTimedBudgetId,
                campaignBudget,
                startDate,
                endDate,
                margin,
                budgetGroupXrefPlacementId);
            
        }

        public TvBudgetGroupTimedBudget InsertBudgetGroupTimedBudget(Guid budgetGroupId, decimal campaignBudget, DateTime startDate,
            DateTime endDate, decimal margin, Guid budgetGroupXrefPlacementId, Guid orderId)
        {
            var budgetGroupTimedBudget = CreateBudgetGroupTimedBudget(
                budgetGroupId,
                campaignBudget,
                startDate,
                endDate,
                margin,
                budgetGroupXrefPlacementId,
                orderId);

            return budgetGroupTimedBudget;
        }

        public Guid? FindLiveBudgetGroupXrefPlacement(Guid budgetGroupXrefPlacementId)
        {
            var liveBudgetGroupXrefPlacement = _tvDbRepository.FindLiveBudgetGroupXrefPlacement(budgetGroupXrefPlacementId);
            return liveBudgetGroupXrefPlacement;
        }
    }
}