using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class CampaignHelper : ICampaignHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public CampaignHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvCampaign CheckExistanceOfCampaignByName(string campaignName)
        {
            var campaign =  _tvDbRepository.GetCampaignDataByName(campaignName);
            return campaign;
        }

        public TvCampaign CreateCampaignIfNotExisting(string advertiserName, string campaignName)
        {
            var campaign = CheckExistanceOfCampaignByName(campaignName);
            if (campaign != null) return campaign;

            var advertiser = _tvDbRepository.GetAdvertiserDataByName(advertiserName);
            if (advertiser == null)
            {
                throw new DataMisalignedException($"{advertiserName} is not set up");
            }

            campaign = new TvCampaign
            {
                AccountId = advertiser.AccountId,
                AdvertiserId = advertiser.AdvertiserId,
                CampaignId = Guid.NewGuid(),
                CampaignName = campaignName,
                CreatedBy = "Data  Pipeline"
            };

           _tvDbRepository.InsertCampaign(campaign);
           

            return campaign;


        }

        public TvCampaign GetCampaignByCampaignId(Guid campaignId)
        {
            return _tvDbRepository.GetCampaignByCampaignId(campaignId);
        }

        public TvCampaign GetCampaignByOrderName(string orderName)
        {
            return _tvDbRepository.GetCampaignByOrderName(orderName);
        }
    }
}