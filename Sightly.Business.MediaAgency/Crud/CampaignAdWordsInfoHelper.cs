using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class CampaignAdWordsInfoHelper : ICampaignAdWordsInfoHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public CampaignAdWordsInfoHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvCampaignAdWordsInfo CheckExistanceOfCampaignAdWordsInfoBycustomerId(long customerId)
        {
            var campaignAdwordsInfo = _tvDbRepository.GetCampaignAdwordsInfoDataByCustomerId(customerId);
            return campaignAdwordsInfo;
        }

        public TvCampaignAdWordsInfo CreateCampaignAdWordsInfoIfNotExisting(long awCustomerId,  Guid advertiserId, Guid campaignId, string campaignMangerEmail)
        {
            var campaignAdwordsInfo = CheckExistanceOfCampaignAdWordsInfoBycustomerId(awCustomerId);
            if (campaignAdwordsInfo != null) return campaignAdwordsInfo;

            campaignAdwordsInfo = new TvCampaignAdWordsInfo
            {
                CampaignAdWordsInfoId = Guid.NewGuid(),
                AdvertiserId = advertiserId,
                CampaignId = campaignId,
                AdWordsCustomerId = awCustomerId,
                CreatedBy = "Data Pipeline",
                CampaignMangerEmail = campaignMangerEmail
            };

            _tvDbRepository.InsertCampaignAdwordsInfo(campaignAdwordsInfo);

            return campaignAdwordsInfo;

        }

        public string GetTvCustomerNameByCustomerId(long customerId)
        {
            var customerName = _tvDbRepository.GetTvCampaignNameByCustomerId(customerId);
            return customerName;
        }

        public TvBudgetGroupAndPlacement DoesCampaignExistReturnXrefPlacementId(long awCampaignId)
        {
            var result = _tvDbRepository.GetBudgetGroupXrefPlacementByAWCampaignId(awCampaignId);
            return result;
        }

        public void EnsureProperCampaignMangerAssignedToCustomer(long customerId, string campaignMangerEmail)
        {
            _tvDbRepository.EnsureProperCampaignMangerAssignedToCustomer(customerId, campaignMangerEmail);
        }
    }
}