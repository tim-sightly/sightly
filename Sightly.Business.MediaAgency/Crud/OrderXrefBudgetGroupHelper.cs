using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class OrderXrefBudgetGroupHelper : IOrderXrefBudgetGroupHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public OrderXrefBudgetGroupHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvOrderXrefBudgetGroup GetExistingOrderXrefBudgetGroup(Guid orderId, Guid budgetGroupId)
        {
            var orderXrefBudgetGroup = _tvDbRepository.GetOrderXrefBudgetGroup(orderId, budgetGroupId);
            return orderXrefBudgetGroup;
        }

        public TvOrderXrefBudgetGroup CreateOrderXrefBudgetGroup(Guid orderId, Guid budgetGroupId)
        {
            var orderXrefBudgetGroup = new TvOrderXrefBudgetGroup
            {
                OrderXrefBudgetGroupId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupId,
                OrderId = orderId,
                CreatedBy = "Data Pipeline"
            };

            _tvDbRepository.InsertOrderXrefBudgetGroup(orderXrefBudgetGroup);

            return orderXrefBudgetGroup;
        }

        public TvOrderXrefBudgetGroup CreateOrderXrefBudgetGroupIfNotExisting(Guid orderId, Guid budgetGroupId)
        {
            var orderXrefBudgetGroup = GetExistingOrderXrefBudgetGroup(orderId, budgetGroupId);
            if (orderXrefBudgetGroup != null) return orderXrefBudgetGroup;

            orderXrefBudgetGroup = CreateOrderXrefBudgetGroup(orderId, budgetGroupId);

            return orderXrefBudgetGroup;
        }
    }
}