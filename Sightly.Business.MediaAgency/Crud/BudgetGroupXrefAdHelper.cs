using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class BudgetGroupXrefAdHelper : IBudgetGroupXrefAdHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public BudgetGroupXrefAdHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvBudgetGroupXrefAd GetExistingBudgetGroupXrefAd(Guid budgetGroupId, Guid adId)
        {
            var budgetGroupXrefAd = _tvDbRepository.GetBudgetXrefAd(budgetGroupId, adId);
            return budgetGroupXrefAd;
        }

        public TvBudgetGroupXrefAd CreateBudgetGroupXrefAd(Guid budgetGroupId, Guid adId)
        {
            var budgetGroupXrefAd = new TvBudgetGroupXrefAd
            {
                BudgetGroupXrefAdId = Guid.NewGuid(),
                AdId = adId,
                BudgetGroupId = budgetGroupId,
                CreatedBy = "Data Pipeline"
            };

            _tvDbRepository.InsertBudgetGroupXrefAd(budgetGroupXrefAd);

            return budgetGroupXrefAd;
        }

        public TvBudgetGroupXrefAd CreateBudgetGroupXrefAdIfNotExist(Guid budgetGroupId, Guid adId)
        {
            var budgetGroupXrefAd = GetExistingBudgetGroupXrefAd(budgetGroupId, adId);
            if (budgetGroupXrefAd != null) return budgetGroupXrefAd;

            budgetGroupXrefAd = CreateBudgetGroupXrefAd(budgetGroupId, adId);
            return budgetGroupXrefAd;
        }
    }
}