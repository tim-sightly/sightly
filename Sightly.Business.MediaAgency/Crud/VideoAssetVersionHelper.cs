using System;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class VideoAssetVersionHelper : IVideoAssetVersionHelper
    {
        private readonly ITvDbRepository _tvDbRepository;
        private readonly IVideoAssetHelper _videoAssetHelper;

        public VideoAssetVersionHelper(ITvDbRepository tvDbRepository, IVideoAssetHelper videoAssetHelper)
        {
            _tvDbRepository = tvDbRepository;
            _videoAssetHelper = videoAssetHelper;
        }

        public TvVideoAssetVersion CheckExistingVideoAssetVersionByYouTubeId(string youTubeId)
        {
            var videoAssetVersion = _tvDbRepository.GetVideoAssetVersionDataByYouTubeId(youTubeId);
            return videoAssetVersion;
        }

        public TvVideoAssetVersion CreateVideoAssetVersionIfNotExisting(Guid videoAssetId, string videoAssetVersionName,
            string videoAssetVersionRefCode, int videoLengthInSeconds, string youTubeId, string youTubeUrl)
        {
            var videoAssetVersion = CheckExistingVideoAssetVersionByYouTubeId(youTubeId);
            if (videoAssetVersion != null) return videoAssetVersion;

            videoAssetVersion = new TvVideoAssetVersion
            {
                VideoAssetVersionId = Guid.NewGuid(),
                VideoAssetId = videoAssetId,
                VideoAssetVersionName = videoAssetVersionName,
                VideoAssetVersionRefCode = videoAssetVersionRefCode,
                VideoLengthInSeconds = videoLengthInSeconds,
                YouTubeUrl = youTubeUrl,
                YouTubeId = youTubeId,
                CreatedBy = "Data Pipeline"
            };

            _tvDbRepository.InsertVideoAssetVersion(videoAssetVersion);

            return videoAssetVersion;
        }

        public TvVideoAssetVersion CreateVideoAssetAndVersionIfNotExisting(Guid accountId, Guid advertiserId, string videoId, string videoName, int videoDuration)
       
        {
            var videoAsset = _videoAssetHelper.CreateVideoAssetIfNotExisting(accountId, advertiserId, videoName, videoName, String.Empty);

            var videoAssetVersion = CreateVideoAssetVersionIfNotExisting(videoAsset.VideoAssetId, videoName, string.Empty, videoDuration, videoId, $"https://www.youtube.com/watch?v={videoId}");

            return videoAssetVersion;
        }
    }
}