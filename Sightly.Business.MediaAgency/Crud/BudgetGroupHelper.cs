using System;
using System.Collections.Generic;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency.Crud
{
    public class BudgetGroupHelper : IBudgetGroupHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public BudgetGroupHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvBudgetGroup GetExistingBudgetGroup(string budgetGroupName, decimal budgetGroupBudget, Guid orderId)
        {
            var budgetGroup = _tvDbRepository.GetBudgetGroupByNameAndBudget(budgetGroupName, budgetGroupBudget, orderId);
            return budgetGroup;
        }

        public TvBudgetGroup CreateBudgetGroup(string budgetGroupName, decimal budgetGroupBudget)
        {
            var budgetGroup = new TvBudgetGroup
            {
                BudgetGroupId = Guid.NewGuid(),
                BudgetGroupBudget = budgetGroupBudget,
                BudgetGroupName = budgetGroupName,
                CreatedBy = "Data Pipeline"
            };

            _tvDbRepository.InsertBudgetGroup(budgetGroup);

            return budgetGroup;
        }

        public TvBudgetGroup CreateBudgetGroupIfNotExisting(string awCampaignName, decimal campaignBudget, Guid orderId)
        {
            var budgetGroup = GetExistingBudgetGroup(awCampaignName, campaignBudget, orderId);
            if (budgetGroup != null) return budgetGroup;

            budgetGroup = CreateBudgetGroup(awCampaignName, campaignBudget);

            return budgetGroup;
        }

        public TvBudgetGroup GetExistingBudgetGroupByAWCampaignId(long awCampaignId)
        {
            var budgetGroup = _tvDbRepository.GetBudgetGrouByAWCampaignId(awCampaignId);
            return budgetGroup;
        }

        public void UpdateBudgetGroupStructureFromProposeToLive(Guid budgetGroupId)
        {
            _tvDbRepository.UpdateBudgetGroupStructureFromProposeToLive(budgetGroupId);
        }

        public List<TvBudgetGroup> GetBudgetGroupsByAwCustomerId(long customerId)
        {
            var budgetGroupList = _tvDbRepository.GetBudgetGroupByAwCustomerId(customerId);
            return budgetGroupList;
        }
    }
}