﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.MediaAgency
{
    public interface IDomainManager
    {
        List<TvOrder> CreateTvMediaAgencyDomain(List<MediaAgencyImport> mediaAgencyData, bool isUpdate = false);

        void UpdateTvMediaAgencyAds(long customerId);

        void UpdateBudgetGroupTimedBudgetInfo(List<MediaAgencyDateBudgetUpdate> mediaAgencyUpdates);

        bool IsCustomerLocked(long customerId);

        void LockCustomer(long customerId, string email, string importType);

        void UnlockCustomer(long customerId, string email);

        string GetTvCustomerNameByCustomerId(long customerId);
        TvBudgetGroupAndPlacement DoesAdwordsCampaignExist(long campAwCampaignId);
        void CreateBudgetGroupTimedBudget(MediaAgencyImport camp, Guid budgetGroupId, Guid budgetGroupXrefPlacementId, Guid orderId);
        void UpdateBudgetGroupTimedBudgets(long customerId);
        void EnsureProperCampaignMangerAssignedToCustomer(long customerId, string campaignMangerEmail);
    }
}