﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Business.MediaAgency.AwModel;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;

namespace Sightly.Business.MediaAgency
{
    public class DomainManager : IDomainManager
    {
        //Adwords Data Collection
        private readonly IAwDataAssociation _dataAssociation;

        //TargetView CRUD
        private readonly IAccountHelper _accountHelper;
        private readonly IAdvertiserHelper _advertiserHelper;
        private readonly ITvAdHelper _tvAdHelper;
        private readonly IBudgetGroupAdWordsInfoHelper _budgetGroupAdWordsInfoHelper;
        private readonly IBudgetGroupHelper _budgetGroupHelper;
        private readonly IBudgetGroupTimedBudgetHelper _budgetGroupTimedBudgetHelper;
        private readonly IBudgetGroupXrefLocationHelper _budgetGroupXrefLocationHelper;
        private readonly IBudgetGroupXrefPlacementHelper _budgetGroupXrefPlacementHelper;
        private readonly ICampaignAdWordsInfoHelper _campaignAdWordsInfoHelper;
        private readonly ICampaignHelper _campaignHelper;
        private readonly IFileUploadHistoryHelper _fileUploadHistoryHelper;
        private readonly ILocationHelper _locationHelper;
        private readonly ITvOrderHelper _tvOrderHelper;
        private readonly IOrderXrefBudgetGroupHelper _orderXrefBudgetGroupHelper;
        private readonly IPlacementHelper _placementHelper;
        private readonly IVideoAssetVersionHelper _videoAssetVersionHelper;

        public DomainManager(IAwDataAssociation dataAssociation, 
            IAccountHelper accountHelper, 
            IAdvertiserHelper advertiserHelper, 
            ITvAdHelper tvAdHelper, 
            IBudgetGroupAdWordsInfoHelper budgetGroupAdWordsInfoHelper, 
            IBudgetGroupHelper budgetGroupHelper, 
            IBudgetGroupTimedBudgetHelper budgetGroupTimedBudgetHelper, 
            IBudgetGroupXrefLocationHelper budgetGroupXrefLocationHelper, 
            IBudgetGroupXrefPlacementHelper budgetGroupXrefPlacementHelper, 
            ICampaignAdWordsInfoHelper campaignAdWordsInfoHelper, 
            ICampaignHelper campaignHelper, 
            IFileUploadHistoryHelper fileUploadHistoryHelper,
            ILocationHelper locationHelper, 
            ITvOrderHelper tvOrderHelper, 
            IOrderXrefBudgetGroupHelper orderXrefBudgetGroupHelper, 
            IPlacementHelper placementHelper, 
            IVideoAssetVersionHelper videoAssetVersionHelper)
        {
            _dataAssociation = dataAssociation;
            _accountHelper = accountHelper;
            _advertiserHelper = advertiserHelper;
            _tvAdHelper = tvAdHelper;
            _budgetGroupAdWordsInfoHelper = budgetGroupAdWordsInfoHelper;
            _budgetGroupHelper = budgetGroupHelper;
            _budgetGroupTimedBudgetHelper = budgetGroupTimedBudgetHelper;
            _budgetGroupXrefLocationHelper = budgetGroupXrefLocationHelper;
            _budgetGroupXrefPlacementHelper = budgetGroupXrefPlacementHelper;
            _campaignAdWordsInfoHelper = campaignAdWordsInfoHelper;
            _campaignHelper = campaignHelper;
            _fileUploadHistoryHelper = fileUploadHistoryHelper;
            _locationHelper = locationHelper;
            _tvOrderHelper = tvOrderHelper;
            _orderXrefBudgetGroupHelper = orderXrefBudgetGroupHelper;
            _placementHelper = placementHelper;
            _videoAssetVersionHelper = videoAssetVersionHelper;
            _tvAdHelper = tvAdHelper;
        }


        public List<TvOrder> CreateTvMediaAgencyDomain(List<MediaAgencyImport> mediaAgencyData, bool fullUpdate  = false)
        {
            var newTvOrders = new List<TvOrder>();

            //Run the code to remove TimedBudgetGroups if Reimporting
            if (fullUpdate)
            {
                _budgetGroupTimedBudgetHelper.PrepareBudgetGroupTimedBudgetsIfReorder(mediaAgencyData.First().AwCustomerId);
            }

            mediaAgencyData.OrderBy(c=>c.AwCampaignId).ToList().ForEach(maData =>
            {
                Console.WriteLine($"{maData.AwCustomerId} - {maData.AwCampaignId}: {maData.AwCampaignName}");
                var adwordsData = _dataAssociation.GetAdwordsData(
                    maData.AwCustomerId, 
                    maData.AwCampaignId);

                //GetAccountData
                var account = _accountHelper.GetAccountByCampaignName(maData.CampaignName);
                if(account == null) throw new DataMisalignedException($"the account data for Campaign {maData.CampaignName} has not been set up yet. You can not proceed.");

                var advertiser = _advertiserHelper.GetAdvertiserByCampaignName(maData.CampaignName);
                if(advertiser == null) throw new DataMisalignedException($"the Advertiser data for Campagin {maData.CampaignName} has not been set up yet. You can not proceed.");

                var campaign = _campaignHelper.CreateCampaignIfNotExisting(
                    advertiser.AdvertiserName, 
                    maData.CampaignName);
                if(campaign == null) throw new DataMisalignedException($"{maData.CampaignName} has not been set up yet. You can not proceed.");

                var campaignXrefAdwordsInfo = _campaignAdWordsInfoHelper.CreateCampaignAdWordsInfoIfNotExisting(
                    maData.AwCustomerId, 
                    advertiser.AdvertiserId, 
                    campaign.CampaignId,
                    maData.CampaignMangerEmail);

                //figure out the orderStatusId by the start and end date 
                var orderStatusId = GetOrderStatusId(
                    maData.StartDate, 
                    maData.EndDate);

                var order = _tvOrderHelper.CreateOrderIfNotExisting(
                    maData.OrderName, 
                    account.AccountId,
                    advertiser.AdvertiserId,
                    campaign.CampaignId, 
                    orderStatusId, 
                    maData.TotalBudget, 
                    maData.StartDate,
                    maData.EndDate);

                var location = _locationHelper.CreateLocationIfNotExisting(
                    account.AccountId, 
                    adwordsData.SafeCampaignName);
                    //adwordsData.AwCampaignName);

                //Check to see if there is an existing budgetGroupAdwordsInfo record for the AWCampaignId
                var budgetGroup = new TvBudgetGroup();
                var budgetGroupAdwordsInfo =  _budgetGroupAdWordsInfoHelper.GetExistingBudgetGroupAdwordsInfo(maData.AwCampaignId);
                if (budgetGroupAdwordsInfo == null)
                {
                    budgetGroup = _budgetGroupHelper.CreateBudgetGroupIfNotExisting(
                        adwordsData.SafeCampaignName,
                        //adwordsData.AwCampaignName,
                        maData.CampaignBudget,
                        order.OrderId);

                    budgetGroupAdwordsInfo = _budgetGroupAdWordsInfoHelper.CreateBudgetGroupAdwordsInfoIfNotExisting(
                        budgetGroup.BudgetGroupId,
                        maData.AwCustomerId,
                        maData.AwCampaignId,
                        adwordsData.SafeCampaignName);
                }
                else
                {
                    budgetGroup = _budgetGroupHelper.GetExistingBudgetGroupByAWCampaignId(maData.AwCampaignId);
                }
                

                var budgetGroupXrefLocation = _budgetGroupXrefLocationHelper.CreateBudgetGroupXrefLocationIfNotExisting(
                    budgetGroup.BudgetGroupId, 
                    location.LocationId);

                var placement = _placementHelper.CreatePlacementIfNotExisting(
                    advertiser.AdvertiserId, 
                    maData.PlacementName, 
                    maData.PlacementId.ToString());

                var budgetGroupXrefPlacement = _budgetGroupXrefPlacementHelper.CreateBudgetGroupXrefPlacement(
                    budgetGroup.BudgetGroupId, 
                    placement.PlacementId);

                var budgetGroupTimedBudget = _budgetGroupTimedBudgetHelper.CreateBudgetGroupTimedBudgetIfNotExisting(
                    budgetGroup.BudgetGroupId,
                    maData.CampaignBudget,
                    maData.StartDate,
                    maData.EndDate,
                    maData.Margin,
                    budgetGroupXrefPlacement.BudgetGroupXrefPlacementId,
                    order.OrderId);

                //var adList = new List<TvAd>();

                CheckAdsAndUpdateNew(adwordsData, account.AccountId, advertiser.AdvertiserId, budgetGroup.BudgetGroupId);
                

                var orderXrefBudgetGroup = _orderXrefBudgetGroupHelper.CreateOrderXrefBudgetGroupIfNotExisting(
                    order.OrderId,
                    budgetGroup.BudgetGroupId);

                newTvOrders.Add(order);

                if (!fullUpdate)
                {
                    //Push the Purposed BudgetGroup objects To Live and History By BudgetGroupId
                    //_budgetGroupHelper.UpdateBudgetGroupStructureFromProposeToLive(budgetGroup.BudgetGroupId);
                }


            });

            return newTvOrders.GroupBy(o => o.OrderId).Select(or => or.First()).ToList();
        }

        public void UpdateTvMediaAgencyAds(long customerId)
        {

            var campaignList = _dataAssociation.GetAdwordsCampaigns(customerId);
            campaignList.ForEach(campaign =>
            {
                var adwordsData = _dataAssociation.GetAdwordsData(
                    customerId,
                    campaign);

                Console.WriteLine($"{DateTime.Now} - geting campaignId - {campaign}");
                var adMetaData = _budgetGroupAdWordsInfoHelper.GetAdMetaData(customerId, campaign);
                if (adMetaData != null)
                {
                    Console.WriteLine($"{DateTime.Now} new AdData for BG- {adMetaData.BudgetGroupId}");
                    CheckAdsAndUpdateNew(adwordsData, adMetaData.AccountId, adMetaData.AdvertiserId,
                        adMetaData.BudgetGroupId);
                }
            });
            
        }

        public void UpdateBudgetGroupTimedBudgetInfo(List<MediaAgencyDateBudgetUpdate> mediaAgencyUpdates)
        {
            mediaAgencyUpdates.ForEach(maRecord =>
            {
                //Validate the BudgetGroupXrefPlacementId and add it to the updating. Make sure to add if it doesn't exist
                Guid budgetGroupXrefPlacementId = ValidatePlacementAndBudgetGroupRelationship(maRecord);
                var updatedBudget = maRecord.CampaignBudget * (1 - maRecord.Margin);

                _budgetGroupTimedBudgetHelper.UpdateProposedBudgetGroupTimedBudget(
                    maRecord.BudgetGroupTimedBudgetId,
                    maRecord.CampaignBudget, 
                    maRecord.StartDate, 
                    maRecord.EndDate,
                    maRecord.Margin,
                    budgetGroupXrefPlacementId);

                Guid? liveBudgetGroupXrefPlacementId = ValidateLiveBudgetGroupXrefPlacement(budgetGroupXrefPlacementId);
                if (liveBudgetGroupXrefPlacementId != null)
                {
                    _budgetGroupTimedBudgetHelper.UpdateLiveBudgetGroupTimedBudget(
                        maRecord.BudgetGroupTimedBudgetId,
                        updatedBudget, 
                        maRecord.StartDate, 
                        maRecord.EndDate,
                        maRecord.Margin,
                        budgetGroupXrefPlacementId);
                    _budgetGroupTimedBudgetHelper.UpdateHistoricalBudgetGroupTimedBudget(
                        maRecord.BudgetGroupTimedBudgetId,
                        updatedBudget, 
                        maRecord.StartDate, 
                        maRecord.EndDate,
                        maRecord.Margin,
                        budgetGroupXrefPlacementId);
                }
               
            });
        }

        

        public bool IsCustomerLocked(long customerId)
        {
            return _fileUploadHistoryHelper.IsCustomerLocked(customerId);
        }

        public void LockCustomer(long customerId, string email, string importType)
        {
            _fileUploadHistoryHelper.LockCustomer(customerId, email, importType);
        }

        public void UnlockCustomer(long customerId, string email)
        {
            _fileUploadHistoryHelper.UnlockCustomer(customerId, email);
        }

        public string GetTvCustomerNameByCustomerId(long customerId)
        {
            return _campaignAdWordsInfoHelper.GetTvCustomerNameByCustomerId(customerId);
        }

        public TvBudgetGroupAndPlacement DoesAdwordsCampaignExist(long awCampaignId)
        {
            return _campaignAdWordsInfoHelper.DoesCampaignExistReturnXrefPlacementId(awCampaignId);
        }

        public void CreateBudgetGroupTimedBudget(MediaAgencyImport camp, Guid budgetGroupId, Guid budgetGroupXrefPlacementId, Guid orderId)
        {
            _budgetGroupTimedBudgetHelper.InsertBudgetGroupTimedBudget(budgetGroupId, camp.CampaignBudget,
                camp.StartDate, camp.EndDate, camp.Margin, budgetGroupXrefPlacementId, orderId);
        }

        public void UpdateBudgetGroupTimedBudgets(long customerId)
        {
            List<TvBudgetGroup> budgetGroups = _budgetGroupHelper.GetBudgetGroupsByAwCustomerId(customerId);
            budgetGroups.ForEach(bg =>
            {
                _budgetGroupHelper.UpdateBudgetGroupStructureFromProposeToLive(bg.BudgetGroupId);
            });
        }

        public void EnsureProperCampaignMangerAssignedToCustomer(long customerId, string campaignMangerEmail)
        {
            _campaignAdWordsInfoHelper.EnsureProperCampaignMangerAssignedToCustomer(customerId, campaignMangerEmail);
        }

        private Guid? ValidateLiveBudgetGroupXrefPlacement(Guid budgetGroupXrefPlacementId)
        {
            var liveBudgetGroupXrefPlacement = _budgetGroupTimedBudgetHelper.FindLiveBudgetGroupXrefPlacement(budgetGroupXrefPlacementId);
            return liveBudgetGroupXrefPlacement;
        }

        private Guid ValidatePlacementAndBudgetGroupRelationship(MediaAgencyDateBudgetUpdate maRecord)
        {
            var advertiser = _advertiserHelper.GetAdvertiserByCampaignName(maRecord.CampaignName);
            if (advertiser == null) throw new DataMisalignedException($"the Advertiser data for Order {maRecord.CampaignName} has not been set up yet. You can not proceed.");

            var budgetGroup = _budgetGroupHelper.GetExistingBudgetGroupByAWCampaignId(maRecord.AwCampaignId);

            var placement = _placementHelper.CreatePlacementIfNotExisting(
                advertiser.AdvertiserId,
                maRecord.PlacementName,
                maRecord.PlacementId.ToString());

            var budgetGroupXrefPlacement = _budgetGroupXrefPlacementHelper.CreateBudgetGroupXrefPlacement(
                budgetGroup.BudgetGroupId,
                placement.PlacementId);

            return budgetGroupXrefPlacement.BudgetGroupXrefPlacementId;

        }

        private static Guid GetOrderStatusId(DateTime startDate, DateTime endDate)
        {
            var today = DateTime.Now;
            Guid orderStatusId;
            if (today > startDate && today > endDate)
            {
                orderStatusId = Guid.Parse("BA38918D-07D4-4197-959C-2F409C9BC1D5");
            }
            else if (today >= startDate && today <= endDate)
            {
                orderStatusId = Guid.Parse("20D17615-CDCF-44BF-8906-F2F3C432BD57");
            }
            else
            {
                orderStatusId = Guid.Parse("8B96CAA6-9746-4783-AAD6-BC52077A542B");
            }

            return orderStatusId;
        }

        private void CheckAdsAndUpdateNew(AdwordsData adwordsData, Guid accountId, Guid advertiserId, Guid budgetGroupId)
        {
            adwordsData.AdwordsAds.GroupBy(z => new { z.VideoId, z.VideoName, z.VideoDuration }).ToList().ForEach(
                ad =>
                {
                    var existingVideoVersion = _videoAssetVersionHelper.CreateVideoAssetAndVersionIfNotExisting(
                        accountId,
                        advertiserId,
                        ad.Key.VideoId,
                        ad.Key.VideoName,
                        Convert.ToInt32(ad.Key.VideoDuration) / 1000);
                });

            adwordsData.AdwordsAds.GroupBy(aa => new { aa.DestinationUrl, aa.DisplayUrl, aa.VideoName, aa.VideoId }).ToList().ForEach(
                ad =>
                {
                    var effectedAds = adwordsData.AdwordsAds.Where(spec => spec.VideoName == ad.Key.VideoName && spec.DestinationUrl == ad.Key.DestinationUrl).ToList();
                    effectedAds.ForEach(eads =>
                    {
                        var vav = _videoAssetVersionHelper.CheckExistingVideoAssetVersionByYouTubeId(eads.VideoId);
                        var tvAd = _tvAdHelper.CreateAdAndRelationshipsIfNotExisting(
                            accountId,
                            advertiserId,
                            vav.VideoAssetVersionId,
                            vav.VideoAssetVersionName,
                            eads.DestinationUrl,
                            eads.DisplayUrl,
                            eads.AdName,
                            eads.AdId,
                            eads.AwCampaignId,
                            budgetGroupId);

                    });
                });
        }

        
    }
}
