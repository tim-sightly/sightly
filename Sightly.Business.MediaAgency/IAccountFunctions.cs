using Sightly.Models;

namespace Sightly.Business.MediaAgency
{
    public interface IAccountFunctions
    {
        TvAccount CheckExistanceOfAccountByName(string accountName);
        TvAccount CreateAccountIfNotExisting(string accountName, string parentAccountName);
    }
}