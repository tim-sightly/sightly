using System;
using System.Linq;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.MediaAgency
{
    public class AccountFunctions : IAccountFunctions
    {
        private readonly ITvDbRepository _tvDbRepository;

        public AccountFunctions(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public TvAccount CheckExistanceOfAccountByName(string accountName)
        {
            return _tvDbRepository.GetAccountDataByName(accountName);
        }

        public TvAccount CreateAccountIfNotExisting(string accountName, string parentAccountName)
        {
            var account = _tvDbRepository.GetAccountDataByName(accountName);
            if (account != null) return account;
            
            var parentAccount = _tvDbRepository.GetAccountDataByName(parentAccountName == string.Empty ? "Sightly" : parentAccountName);

            account = new TvAccount
            {
                AccountId = Guid.NewGuid(),
                AccountName = accountName,
                ShortCode =  accountName.Substring(0,2).ToUpper(),
                AccountTypeId = Guid.Parse("D600FA41-FFC0-4B03-8272-CAE6E093082F"),
                SightlyMargin = 0.0M,
                ParentAccountId = parentAccount.AccountId

            };

            _tvDbRepository.InsertAccount(account);

            return account;
        }
    }
}