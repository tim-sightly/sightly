using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.AwModel
{
    public class AwDataAssociation : IAwDataAssociation
    {
        private readonly IAd _adAdwordService;
        private readonly ICampaign _campaignAdwordService;
        private readonly ICampaignLabel _campaignLabelAdwordService;
        private readonly IVideo _videoAdwordService;

        public AwDataAssociation(IAd adAdwordService, ICampaign campaignAdwordService, ICampaignLabel campaignLabelAdwordService, IVideo videoAdwordService)
        {
            _adAdwordService = adAdwordService;
            _campaignAdwordService = campaignAdwordService;
            _campaignLabelAdwordService = campaignLabelAdwordService;
            _videoAdwordService = videoAdwordService;
        }


        public List<long> GetAdwordsCampaigns(long customerId)
        {
            var campaignList = new List<long>();
            var campaigns = _campaignAdwordService.GetCampaignData(customerId.ToString());
            campaigns.ForEach(camp =>
            {
                campaignList.Add(camp.AwCampaignId);
            });

            return campaignList;
        }

        public AdwordsData GetAdwordsData(long customerId, long campaignId)
        {
            var adwordsData = new AdwordsData();

            var campData = _campaignAdwordService.GetCampaignData(customerId.ToString());

            campData.Where(c => c.AwCampaignId == campaignId).ToList().ForEach(campaign => {
                var awdata = new AdwordsData
                {
                    CustomerId = customerId,
                    AwCampaignId = campaign.AwCampaignId,
                    AwCampaignName = campaign.AwCampaignName,
                    StartDate = campaign.StartDate,
                    EndDate = campaign.EndDate
                };
                adwordsData = awdata;
            });

            var startDate = campData.Min(x => x.StartDate);
            var endDate = campData.Max(y => y.EndDate);

            var labelData = _campaignLabelAdwordService.GetCampaignLabelData(customerId.ToString(), startDate, endDate);

            labelData.ForEach(label => {
                if (adwordsData.AwCampaignId == Convert.ToInt64(label.CampaignId))
                {
                    adwordsData.Labels.AddRange(label.Labels);
                }
            });

            var adData = _adAdwordService.GetAdData(customerId.ToString());
            var videoData = _videoAdwordService.GetVideoData(customerId.ToString());

            var videoList = (from a in adData
                            join v in videoData on a.AdId equals v.AdId
                            where a.AwCampaignId == adwordsData.AwCampaignId // && a.AdState == "enabled"
                            select new AdwordsAd
                            {
                                AdId = a.AdId,
                                AdName = a.AdName,
                                AdState = a.AdState,
                                AwCampaignId = a.AwCampaignId,
                                VideoDuration = v.VideoDuration,
                                VideoId = v.VideoId,
                                VideoName = v.VideoName,
                                DestinationUrl = a.DestinationUrl,
                                DisplayUrl = a.DisplayUrl
                            }).Distinct().ToList();

            adwordsData.AdwordsAds = videoList;


            return adwordsData;
        }
    }
}