﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.MediaAgency.AwModel
{
    public interface IAwDataAssociation
    {
        List<long> GetAdwordsCampaigns(long customerId);
        AdwordsData GetAdwordsData(long customerId, long campaignId);

    }
}
