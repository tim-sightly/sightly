﻿using System;
using System.Collections.Generic;
using System.Data;
using Sightly.BusinessLayer.DomainObjects.Audience;

namespace Sightly.BusinessLayer
{
    public interface IOrderManagerSm
    {
        List<OrderTargetAgeGroup> GetTargetAgeGroupsForOrder(Guid orderId, IDbConnection connection = null);
        List<OrderTargetGenderGroup> GetTargetGenderGroupsForOrder(Guid orderId, IDbConnection connection = null);
        List<OrderParentalStatusGroup> GetParentalStatusGroupsForOrder(Guid orderId, IDbConnection connection = null);
        List<OrderHouseholdIncomeGroup> GetHouseholdIncomeGroupsForOrder(Guid orderId, IDbConnection connection = null);
    }
}
