﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.BusinessLayer
{
    public class OrderBudgetGroup
    {
        public Guid OrderBudgetGroupId { get; set; }
        public Guid OrderId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime Modified { get; set; }
    }
}
