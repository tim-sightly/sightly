﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Sightly.BusinessLayer.DomainObjects.Audience;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;

namespace Sightly.BusinessLayer
{
    public class OrderManagerSm : IOrderManagerSm
    {
        private IOrderStore _orderStore;

        public OrderManagerSm(IOrderStore orderStore)
        {
            _orderStore = orderStore;
        }

        public List<OrderTargetAgeGroup> GetTargetAgeGroupsForOrder(Guid orderId, IDbConnection connection = null)
        {
            return _orderStore.GetTargetAgeGroupsForOrder(orderId, connection).Select(ta => Mapper.Map(ta)).ToList();
        }

        public List<OrderTargetGenderGroup> GetTargetGenderGroupsForOrder(Guid orderId, IDbConnection connection = null)
        {
            return _orderStore.GetTargetGenderGroupsForOrder(orderId, connection).Select(tg => Mapper.Map(tg)).ToList();
        }

        public List<OrderParentalStatusGroup> GetParentalStatusGroupsForOrder(Guid orderId, IDbConnection connection = null)
        {
            return _orderStore.GetParentalStatusGroupsForOrder(orderId, connection).Select(ps => Mapper.Map(ps)).ToList();
        }

        public List<OrderHouseholdIncomeGroup> GetHouseholdIncomeGroupsForOrder(Guid orderId, IDbConnection connection = null)
        {
            return _orderStore.GetHouseholdIncomeGroupsForOrder(orderId).Select(hig => Mapper.Map(hig)).ToList();
        }
    }
}
