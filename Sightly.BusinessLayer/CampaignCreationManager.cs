﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;
using Sightly.Models;
using AccountDto = Sightly.DAL.DTO.Account;
using Account = Sightly.BusinessLayer.DomainObjects.Account;
using AdvertiserDto = Sightly.DAL.DTO.Advertiser;
using Advertiser = Sightly.BusinessLayer.DomainObjects.Advertiser;
using CampaignDto = Sightly.DAL.DTO.Campaign;
using Campaign = Sightly.BusinessLayer.DomainObjects.Campaign;
using OrderBasicDto = Sightly.DAL.DTO.OrderBasic;
using OrderBasic = Sightly.BusinessLayer.DomainObjects.OrderBasic;
using CampaignAbbreviatedDto = Sightly.DAL.DTO.CampaignAbbreviated;
using CampaignAbbreviated = Sightly.Models.CampaignAbbreviated;

namespace Sightly.BusinessLayer
{
    public class CampaignCreationManager : ICampaignCreationManager
    {
        private readonly ICampaignCreationStore _campaignCreationStore;

        public CampaignCreationManager(ICampaignCreationStore campaignCreationStore)
        {
            _campaignCreationStore = campaignCreationStore;
        }


        public List<Account> GetAllAccounts()
        {
            List<AccountDto> accounts = _campaignCreationStore.GetAllAccounts();
            return accounts.Select(Mapper.Map).ToList();
        }

        public List<Account> GetAllAccountsByUser(Guid userId)
        {
            List<AccountDto> accounts = _campaignCreationStore.GetAllAccountsByUser(userId);
            return accounts.Select(Mapper.Map).ToList();
        }

        public List<Advertiser> GetAllAdvertisersByUser(Guid userId)
        {
            List<AdvertiserDto> advertiser = _campaignCreationStore.GetAllAdvertisersByUser(userId);
            return advertiser.Select(Mapper.Map).ToList();
        }

        public List<CampaignAbbreviated> GetAllCampaignsByUser(Guid userId)
        {
            List<CampaignAbbreviatedDto> campaign = _campaignCreationStore.GetAllCampaignsByUser(userId);
            return campaign.Select(Mapper.Map).ToList();
        }

        public List<Advertiser> GetAdvertiserByAccountId(Guid accountId)
        {
            List<AdvertiserDto> advertiser = _campaignCreationStore.GetAdvertiserByAccountId(accountId);
            return advertiser.Select(Mapper.Map).ToList();
        }

        public List<Advertiser> GetAdvertiserByAccountIdAndUser(Guid accountId, Guid userId)
        {
            List<AdvertiserDto> advertiser = _campaignCreationStore.GetAdvertiserByAccountIdAndUser(accountId, userId);
            return advertiser.Select(Mapper.Map).ToList();
        }

        public Account InsertSubAccount(string accountName, Guid accountTypeId, Guid parentAccountId)
        {
            AccountDto newAccount = _campaignCreationStore.InsertSubAccount(accountName, accountTypeId, parentAccountId);
            return Mapper.Map(newAccount);
        }

        public Advertiser InsertAdvertiser(string advertiserName, Guid accountId)
        {
            AdvertiserDto advertiser = _campaignCreationStore.InsertAdvertiser(advertiserName, accountId);
            return Mapper.Map(advertiser);
        }

        public Campaign InsertCampaign(string campaignName, Guid advertiserId)
        {
            CampaignDto campaign = _campaignCreationStore.InsertCampaign(campaignName, advertiserId);
            return Mapper.Map(campaign);
        }

        public OrderBasic InsertOrder(string orderName, string orderRefCode, Guid campaignId)
        {
            OrderBasicDto order = _campaignCreationStore.InsertOrder(orderName, orderRefCode, campaignId);
            return Mapper.Map(order);
        }

        public Advertiser InsertAdvertiserExtended(string advertiserName, string advertiserRefCode, Guid accountId,
            Guid advertiserCategoryId, Guid advertiserSubCategoryId)
        {
            AdvertiserDto advertiser = _campaignCreationStore.InsertAdvertiserExtended(
                advertiserName, 
                advertiserRefCode,
                accountId,
                advertiserCategoryId,
                advertiserSubCategoryId);
            return Mapper.Map(advertiser);
        }
    }
}