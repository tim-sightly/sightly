﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.BusinessLayer.Common;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupAdStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupConversionActionStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupLocationStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupPlacementStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using Sightly.BusinessLayer.Exceptions;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;
using BudgetGroupDto = Sightly.DAL.DTO.BudgetGroupStates.BudgetGroup;
using BudgetGroupProposalDto = Sightly.DAL.DTO.BudgetGroupStates.BudgetGroupProposal;
using BudgetGroupLedgerDto = Sightly.DAL.DTO.BudgetGroupLedger;

namespace Sightly.BusinessLayer
{
    public class BudgetGroupManager : IBudgetGroupManager
    {
        private readonly IBudgetGroupStore _budgetGroupStore;
        private readonly IBudgetGroupLedgerStore _budgetGroupLedgerStore;
        private readonly IBudgetGroupProposalStore _budgetGroupProposalStore;

        public BudgetGroupManager(IBudgetGroupStore budgetGroupStore, IBudgetGroupLedgerStore budgetGroupLedgerStore, IBudgetGroupProposalStore budgetGroupProposalStore)
        {
            _budgetGroupStore = budgetGroupStore;
            _budgetGroupLedgerStore = budgetGroupLedgerStore;
            _budgetGroupProposalStore = budgetGroupProposalStore;
        }

        public BudgetGroup GetById(Guid budgetGroupId, Guid user)
        {
            return Mapper.Map(_budgetGroupStore.GetById(budgetGroupId));
        }

        public List<BudgetGroup> GetAll(Guid user)
        {
            List<BudgetGroupDto> budgetGroupDtos = _budgetGroupStore.GetBy(currentUser: user, column: "None");
            List<BudgetGroup> budgetGroups = budgetGroupDtos.Select(bgd => Mapper.Map(bgd)).ToList();
            return budgetGroups;
        }

        public List<BudgetGroup> GetByBudgetGroupStatus(BudgetGroupStatus status, Guid user)
        {
            List<BudgetGroupDto> budgetGroupDtos = _budgetGroupStore.GetBy(currentUser: user, column: "BudgetGroupStatusId", statusId: (int)status);
            List<BudgetGroup> budgetGroups = budgetGroupDtos.Select(bgd => Mapper.Map(bgd)).ToList();
            return budgetGroups;
        }

        public BudgetGroup UpdateStatus(Guid budgetGroupId, BudgetGroupStatus status, Guid user)
        {
            throw new NotImplementedException();
        }


        public BudgetGroup MergeBudgetGroupAndUpdate(BudgetGroup newBudgetGroup, Guid user)
        {
            //BudgetGroupDto oldBudgetGroupDto = _budgetGroupStore.GetBudgetGroupById(newBudgetGroup.BudgetGroupId.Value);

            //if (oldBudgetGroupDto == null)
            //    throw new EntityNotFoundException();
            
            //BudgetGroup oldBudgetGroup = Mapper.Map(oldBudgetGroupDto);
            //DateTime now = DateTime.UtcNow;

            ////MERGE old and new
            //BudgetGroup mergedBudgetGroup = MergeBudgetGroup(oldBudgetGroup, newBudgetGroup, user, now);
            //mergedBudgetGroup.BudgetGroupAds = MergeAds(oldBudgetGroup.BudgetGroupAds, newBudgetGroup.BudgetGroupAds);
            //mergedBudgetGroup.BudgetGroupLocations = MergeLocations(oldBudgetGroup.BudgetGroupLocations, newBudgetGroup.BudgetGroupLocations);
            //mergedBudgetGroup.BudgetGroupTimedBudget = newBudgetGroup.BudgetGroupTimedBudget;
            //mergedBudgetGroup.BudgetGroupConversionActions = MergeConversionActions(oldBudgetGroup.BudgetGroupConversionActions, newBudgetGroup.BudgetGroupConversionActions);
            //mergedBudgetGroup.BudgetGroupAds = MergeAds(oldBudgetGroup.BudgetGroupAds, newBudgetGroup.BudgetGroupAds);

            //Dictionary<string, List<Guid>> itemsToDelete = BuildDeleteDictionaryAndStripDeletedItems(mergedBudgetGroup);
            //BudgetGroupDto mergedBudgetGroupDto = Mapper.Map(mergedBudgetGroup);

            //return Mapper.Map(_budgetGroupStore.UpdateBudgetGroup(mergedBudgetGroupDto, itemsToDelete));
            throw new NotImplementedException();
        }

        public BudgetGroupProposal MoveToProposed(Guid budgetGroupId, Guid userId, string notes = null)
        {
            //BudgetGroup budgetGroup = Mapper.Map(_budgetGroupStore.GetBudgetGroupById(budgetGroupId));

            //if (budgetGroup == null)
            //{
            //    throw new EntityNotFoundException($"Could not find BudgetGroup with BudgetGroupId={budgetGroupId}.");
            //}

            //if (budgetGroup.BudgetGroupStatusId != (int)BudgetGroupStatus.Live)
            //{
            //    throw new DataConditionException("BudgetGroup must be in Live Status to be used for Editing.");
            //}

            ////check to see if an existing Proposal is available
            //var budgetGroupProposalId = _budgetGroupLedgerStore.GetBudgetGroupLedgerByBudgetGroup(budgetGroupId).BudgetGroupProposalId ?? new Guid();

            //BudgetGroupProposal budgetGroupProposal = LiveToProposal.Map(budgetGroup, budgetGroupProposalId, userId);

            ////budgetGroupProposal.BudgetGroupStatusId = (int) BudgetGroupStatus.Draft;

            //var budgetGroupLedger = new BudgetGroupLedger
            //{
            //    BudgetGroupId = budgetGroup.BudgetGroupId,
            //    BudgetGroupProposalId = budgetGroupProposal.BudgetGroupId,
            //    CreatedBy = userId,
            //    Created = DateTime.UtcNow,
            //    BudgetGroupEventId = BudgetGroupEvent.ChangeOrder,
            //    Notes = string.IsNullOrEmpty(notes) ? null : notes
            //};

            ////BudgetGroupProposalDto budgetGroupProposalDto = Mapper.Map(budgetGroupProposal);
            //BudgetGroupLedgerDto budgetGroupLedgerDto = Mapper.Map(budgetGroupLedger);

            //BudgetGroupProposalDto updatedBudgetGroupProposal = _budgetGroupProposalStore.CreateOrUpdateBudgetGroupProposal(budgetGroupProposalDto, budgetGroupLedgerDto);

            //return Mapper.Map(updatedBudgetGroupProposal);
            throw new NotImplementedException();
        }

        private Dictionary<string, List<Guid>> BuildDeleteDictionaryAndStripDeletedItems(BudgetGroup budgetGroup)
        {
            //Dictionary<string, List<Guid>> itemsToDelete = new Dictionary<string, List<Guid>>()
            //    {
            //        {"ads", new List<Guid>()},
            //        {"locations", new List<Guid>()},
            //        {"timedBudget", new List<Guid>()},
            //        {"conversionActions", new List<Guid>()},
            //        {"placements", new List<Guid>()},
            //    };

            //if (budgetGroup.BudgetGroupAds != null)
            //{
            //    for (int i = budgetGroup.BudgetGroupAds.Count - 1; i >= 0; i--)
            //    {
            //        if (budgetGroup.BudgetGroupAds[i].DeleteMe.HasValue && budgetGroup.BudgetGroupAds[i].DeleteMe.Value)
            //        {
            //            itemsToDelete["ads"].Add(budgetGroup.BudgetGroupAds[i].BudgetGroupAdId.Value);
            //            budgetGroup.BudgetGroupAds.RemoveAt(i);
            //        }
            //    }
            //}

            //if (budgetGroup.BudgetGroupLocations != null)
            //{
            //    for (int i = budgetGroup.BudgetGroupLocations.Count - 1; i >= 0; i--)
            //    {
            //        if (budgetGroup.BudgetGroupLocations[i].DeleteMe.HasValue && budgetGroup.BudgetGroupLocations[i].DeleteMe.Value)
            //        {
            //            itemsToDelete["locations"].Add(budgetGroup.BudgetGroupLocations[i].BudgetGroupLocationId.Value);
            //            budgetGroup.BudgetGroupLocations.RemoveAt(i);
            //        }
            //    }
            //}

            //if (budgetGroup.BudgetGroupTimedBudget != null && budgetGroup.BudgetGroupTimedBudget.DeleteMe.HasValue)
            //{
            //    itemsToDelete["timedBudget"].Add(budgetGroup.BudgetGroupTimedBudget.BudgetGroupTimedBudgetId);
            //}

            //if (budgetGroup.BudgetGroupConversionActions != null)
            //{
            //    for (int i = budgetGroup.BudgetGroupConversionActions.Count - 1; i >= 0; i--)
            //    {
            //        if (budgetGroup.BudgetGroupConversionActions[i].DeleteMe.HasValue && budgetGroup.BudgetGroupConversionActions[i].DeleteMe.Value)
            //        {
            //            itemsToDelete["conversionActions"].Add(budgetGroup.BudgetGroupConversionActions[i].BudgetGroupConversionActionId.Value);
            //            budgetGroup.BudgetGroupConversionActions.RemoveAt(i);
            //        }
            //    }
            //}

            //if (budgetGroup.BudgetGroupPlacements != null)
            //{
            //    for (int i = budgetGroup.BudgetGroupPlacements.Count - 1; i >= 0; i--)
            //    {
            //        if (budgetGroup.BudgetGroupPlacements[i].DeleteMe.HasValue && budgetGroup.BudgetGroupPlacements[i].DeleteMe.Value)
            //        {
            //            itemsToDelete["placements"].Add(budgetGroup.BudgetGroupPlacements[i].BudgetGroupPlacementId.Value);
            //            budgetGroup.BudgetGroupPlacements.RemoveAt(i);
            //        }
            //    }
            //}

            //return itemsToDelete;
            return null;
        }

        private BudgetGroup MergeBudgetGroup(BudgetGroup oldBg, BudgetGroup newBg, Guid user, DateTime now)
        {
            //var mergedBudgetGroup = new BudgetGroup()
            //{
            //    BudgetGroupId = oldBg.BudgetGroupId,
            //    BudgetGroupStatusId = oldBg.BudgetGroupStatusId,
            //    BudgetGroupName = newBg.BudgetGroupName ?? oldBg.BudgetGroupName,
            //    BudgetGroupBudget = newBg.BudgetGroupBudget ?? oldBg.BudgetGroupBudget,
            //    CreatedBy = oldBg.CreatedBy,
            //    Created = oldBg.Created,
            //    ModifiedBy = (!string.IsNullOrEmpty(newBg.BudgetGroupName) || newBg.BudgetGroupBudget.HasValue) ? user : oldBg.ModifiedBy,
            //    Modified = (!string.IsNullOrEmpty(newBg.BudgetGroupName) || newBg.BudgetGroupBudget.HasValue) ? now : oldBg.Modified,
            //};
            //return mergedBudgetGroup;
            //return null;

            throw new NotImplementedException();
        }

        public List<BudgetGroupAd> MergeAds(List<BudgetGroupAd> oldAds, List<BudgetGroupAd> newAds)
        {
            //var mergedBg = new List<BudgetGroupAd>();
            //mergedBg.AddRange(oldAds);
            //mergedBg.ForEach(bg =>{ bg.DeleteMe = true; });

            //mergedBg.AddRange(newAds);

            //return mergedBg;
            return null;
        }

        private List<BudgetGroupLocation> MergeLocations(List<BudgetGroupLocation> oldLocations, List<BudgetGroupLocation> newLocations)
        {
            //var mergedBg = new List<BudgetGroupLocation>();
            //mergedBg.AddRange(oldLocations);
            //mergedBg.ForEach(bg => { bg.DeleteMe = true;  });

            //mergedBg.AddRange(newLocations);

            //return mergedBg;
            return null;
        }

        private List<BudgetGroupConversionAction> MergeConversionActions(List<BudgetGroupConversionAction> oldConversionActions, List<BudgetGroupConversionAction> newConversionActions)
        {
            var mergedBg = new List<BudgetGroupConversionAction>();

            mergedBg.AddRange(oldConversionActions);
            mergedBg.ForEach(bg => { bg.DeleteMe = true; });

            mergedBg.AddRange(newConversionActions);

            return mergedBg;
        }

        //private List<BudgetGroupPlacement> MergePlacements(List<BudgetGroupPlacement> oldPlacements, List<BudgetGroupPlacement> newPlacements)
        //{
        //    var mergedBg = new List<BudgetGroupPlacement>();

        //    mergedBg.AddRange(oldPlacements);
        //    mergedBg.ForEach(bg => { bg.DeleteMe = true; });

        //    mergedBg.AddRange(newPlacements);

        //    return mergedBg;
        //}
    }
}
