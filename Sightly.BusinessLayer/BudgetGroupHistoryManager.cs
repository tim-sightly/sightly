using System;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;

namespace Sightly.BusinessLayer
{
    public class BudgetGroupHistoryManager : IBudgetGroupHistoryManager
    {
        private IBudgetGroupHistoryStore _budgetGroupHistoryStore;

        public BudgetGroupHistoryManager(IBudgetGroupHistoryStore budgetGroupHistoryStore)
        {
            _budgetGroupHistoryStore = budgetGroupHistoryStore;
        }

        public BudgetGroupHistory CreateBudgetGroupHistory(BudgetGroupHistory budgetGroupHistory, Guid user)
        {
            var historyDto = Mapper.Map(budgetGroupHistory);
            _budgetGroupHistoryStore.CreateBudgetGroupHistory(historyDto);
            return Mapper.Map(_budgetGroupHistoryStore.GetBudgetGroupHistoryById(historyDto.BudgetGroupId));
        }
    }
}