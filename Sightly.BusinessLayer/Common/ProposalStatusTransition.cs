﻿using System.Collections.Generic;

namespace Sightly.BusinessLayer.Common
{
    public static class ProposalStatusTransition
    {
        private static List<Transition> TransitionWhiteList = new List<Transition>
        {
            new Transition()
            {
                From = BudgetGroupStatus.Draft,
                To = BudgetGroupStatus.DraftPending
            },
            new Transition()
            {
                From = BudgetGroupStatus.Draft,
                To = BudgetGroupStatus.Submitted
            },
            new Transition()
            {
                From = BudgetGroupStatus.DraftPending,
                To = BudgetGroupStatus.Submitted
            },
            new Transition()
            {
                From = BudgetGroupStatus.Submitted,
                To = BudgetGroupStatus.Locked
            },
            new Transition()
            {
                From = BudgetGroupStatus.Submitted,
                To = BudgetGroupStatus.Locked
            },
            new Transition()
            {
                From = BudgetGroupStatus.Submitted,
                To = BudgetGroupStatus.Draft
            }
        };

        public static bool IsValid(BudgetGroupStatus from, BudgetGroupStatus to)
        {
            foreach (var transition in TransitionWhiteList)
            {
                if (transition.From == from && transition.To == to)
                    return true;
            }
            return false;
        }
    }
}
