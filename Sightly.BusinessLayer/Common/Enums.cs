﻿namespace Sightly.BusinessLayer.Common
{
    public enum BudgetGroupStatus
    {
        Draft = 1,
        DraftPending = 2,
        Submitted = 3,
        Locked = 4,
        Ready = 5,
        Live = 6,
        Completed = 7,
        Cancelled = 8,
        Paused = 9
    }

    public enum BudgetGroupEvent
    {
        ProposalCreated = 1,
        DraftSubmitted = 2,
        DraftApproved = 3,
        MediaBriefPulled = 4,
        MediaBriefProvisioned = 5,
        StatsAvailable = 6,
        BudgetGroupFinished = 7,
        ChangeOrder = 8,
        RenewOrder = 9
    }

    public enum BudgetGroupProposalColumn
    {
        None = 0,
        BudgetGroupProposalId = 1,
        BudgetGroupStatusId = 2,
        BudgetGroupName = 3,
        BudgetGroupBudget = 4,
        CreatedBy = 5,
        ModifiedBy = 6
    }
}
