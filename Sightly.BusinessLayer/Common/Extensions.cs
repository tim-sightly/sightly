﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.DAL.DTO.BudgetGroupStates;

namespace Sightly.BusinessLayer.Common
{
    public static class Extensions
    {
        public static bool IsTheSame(this BudgetGroup budgetGroup, BudgetGroupProposal budgetGroupProposal)
        {
            return 
                new HashSet<Guid>(budgetGroup.BudgetGroupTimedBudgets.Select(b => b.BudgetGroupTimedBudgetId)).SetEquals(budgetGroupProposal.BudgetGroupTimedBudgetProposals.Select(b => b.BudgetGroupTimedBudgetId)) &&
                new HashSet<Guid>(budgetGroup.BudgetGroupXrefAds.Select(a => a.BudgetGroupXrefAdId)).SetEquals(budgetGroupProposal.BudgetGroupXrefAdProposals.Select(a => a.BudgetGroupXrefAdId)) &&
                new HashSet<Guid>(budgetGroup.BudgetGroupXrefLocations.Select(i => i.BudgetGroupXrefLocationId)).SetEquals(budgetGroupProposal.BudgetGroupXrefLocationProposals.Select(l => l.BudgetGroupXrefLocationId)) &&
                new HashSet<Guid>(budgetGroup.BudgetGroupXrefConversionActions.Select(ca => ca.BudgetGroupXrefConversionActionId)).SetEquals(budgetGroupProposal.BudgetGroupXrefConversionActionProposals.Select(ca => ca.BudgetGroupXrefConversionActionProposalId)) &&
                new HashSet<Guid>(budgetGroup.BudgetGroupXrefPlacements.Select(p => p.BudgetGroupXrefPlacementId)).SetEquals(budgetGroupProposal.BudgetGroupXrefPlacementProposals.Select(p => p.BudgetGroupXrefPlacementId));
        }
    }
}
