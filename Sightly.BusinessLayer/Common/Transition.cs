﻿namespace Sightly.BusinessLayer.Common
{
    public class Transition
    {
        public BudgetGroupStatus From { get; set; }
        public BudgetGroupStatus To { get; set; }
    }
}
