﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.Common;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;

namespace Sightly.BusinessLayer
{
    public interface IBudgetGroupManager
    {
        BudgetGroup GetById(Guid budgetGroupId, Guid user);
        List<BudgetGroup> GetAll(Guid user);
        List<BudgetGroup> GetByBudgetGroupStatus(BudgetGroupStatus status, Guid user);
        BudgetGroup UpdateStatus(Guid budgetGroupId, BudgetGroupStatus status, Guid user);
        BudgetGroup MergeBudgetGroupAndUpdate(BudgetGroup newBudgetGroup, Guid user);

        BudgetGroupProposal MoveToProposed(Guid budgetGroupId, Guid userId, string notes = null);

    }
}
