﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;

namespace Sightly.BusinessLayer
{
    public class StatManager : IStatManager
    {
        private readonly IStatStore _statStore;

        public StatManager(IStatStore statStore)
        {
            _statStore = statStore;
        }

        public List<DailyStats> GetDailyDeviceStatsForAdWordsCustomerId(long adWordsCustomerId)
        {
            List<DAL.DTO.DailyStats> stats = _statStore.GetDailyDeviceStatsForAdWordsCustomerId(adWordsCustomerId);
            return stats.Select(s => Mapper.Map(s)).ToList();
        }

        public List<DailyStats> GetDailyDeviceStatsForCampaign(Guid campaignId)
        {
            List<DAL.DTO.DailyStats> stats = _statStore.GetDailyDeviceStatsForCampaign(campaignId);
            return stats.Select(s => Mapper.Map(s)).ToList();
        }
        
        public List<DailyStats> GetDailyDeviceStatsForAccount(Guid accountId)
        {
            List<DAL.DTO.DailyStats> stats = _statStore.GetDailyDeviceStatsForAccount(accountId);
            return stats.Select(s => Mapper.Map(s)).ToList();
        }

        public List<DailyStats> GetDailyDeviceStatsForAdvertiser(Guid advertiserId)
        {
            List<DAL.DTO.DailyStats> stats = _statStore.GetDailyDeviceStatsForAdvertiser(advertiserId);
            return stats.Select(s => Mapper.Map(s)).ToList();
        }
    }
}