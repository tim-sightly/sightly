﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.Common;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;

namespace Sightly.BusinessLayer
{
    public interface IBudgetGroupHistoryManager
    {
        BudgetGroupHistory CreateBudgetGroupHistory(BudgetGroupHistory budgetGroupHistory, Guid user);
    }
}
