﻿using System;
using System.Linq;
using Sightly.BusinessLayer.Common;
using Sightly.DAL.DTO.BudgetGroupStates;
using Sightly.DAL.DTO.BudgetGroupXrefAdStates;
using Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates;
using Sightly.DAL.DTO.BudgetGroupXrefLocationStates;
using Sightly.DAL.DTO.BudgetGroupXrefPlacementStates;
using Sightly.DAL.DTO.BudgetGroupTimedBudgetStates;

namespace Sightly.BusinessLayer.Mapping
{
    public class ProposalToLiveDto
    {
        public static BudgetGroup Map(BudgetGroupProposal proposal, Guid createdBy)
        {
            Guid budgetGroupId = proposal.BudgetGroupId;

            var budgetGroup = new BudgetGroup()
            {
                BudgetGroupId = budgetGroupId,
                BudgetGroupStatusId = (int)BudgetGroupStatus.Ready,
                BudgetGroupName = proposal.BudgetGroupName,
                CreatedBy = proposal.CreatedById ?? createdBy,
                Created = proposal.CreatedDatetime,
                ModifiedBy = proposal.ModifiedById ?? createdBy,
                Modified = proposal.LastModifiedDatetime,

                BudgetGroupXrefAds = proposal.BudgetGroupXrefAdProposals.Select(a => Map(a, budgetGroupId, createdBy)).ToList(),
                BudgetGroupXrefLocations = proposal.BudgetGroupXrefLocationProposals.Select(l => Map(l, budgetGroupId, createdBy)).ToList(),
                BudgetGroupXrefPlacements = proposal.BudgetGroupXrefPlacementProposals.Select(p => Map(p, budgetGroupId, createdBy)).ToList(),
                BudgetGroupTimedBudgets = proposal.BudgetGroupTimedBudgetProposals.Select(b => Map(b, budgetGroupId, createdBy)).ToList(),
                BudgetGroupXrefConversionActions = proposal.BudgetGroupXrefConversionActionProposals.Select(ca => Map(ca, budgetGroupId, createdBy)).ToList()
            };
            return budgetGroup;
        }

        public static BudgetGroupXrefAd Map(BudgetGroupXrefAdProposal proposal, Guid budgetGroupId, Guid createdBy)
        {
            var budgetGroupAd = new BudgetGroupXrefAd()
            {
                BudgetGroupXrefAdId = proposal.BudgetGroupXrefAdId,
                BudgetGroupId = budgetGroupId,
                AdId = proposal.AdId,
                CreatedBy = proposal.CreatedById ?? createdBy,
                Created = proposal.CreatedDatetime,
            };
            return budgetGroupAd;
        }

        public static BudgetGroupXrefLocation Map(BudgetGroupXrefLocationProposal location, Guid budgetGroupId, Guid createdBy)
        {
            var budgetGroupLocation = new BudgetGroupXrefLocation()
            {
                BudgetGroupXrefLocationId = location.BudgetGroupXrefLocationId,
                BudgetGroupId = budgetGroupId,
                LocationId = location.LocationId,
                CreatedBy = location.CreatedById ?? createdBy,
                Created = location.CreatedDatetime
            };
            return budgetGroupLocation;
        }

        public static BudgetGroupTimedBudget Map(BudgetGroupTimedBudgetProposal timedBudget, Guid budgetGroupId, Guid createdBy)
        {
            var budgetGroupTimedBudget = new BudgetGroupTimedBudget()
            {
                BudgetGroupTimedBudgetId = timedBudget.BudgetGroupTimedBudgetId,
                BudgetGroupId = budgetGroupId,
                BudgetAmount = timedBudget.NetBudgetGroup, //timedBudget.BudgetAmount, we need the net budgetAmount not the Gross
                Margin = timedBudget.Margin,
                StartDate = timedBudget.StartDate,
                EndDate = timedBudget.EndDate,
                CreatedBy = timedBudget.CreatedById ?? createdBy,
                Created = timedBudget.CreatedDatetime,
                BudgetGroupXrefPlacementId = timedBudget.BudgetGroupXrefPlacementId,
                OrderId = timedBudget.OrderId
            };
            return budgetGroupTimedBudget;
        }

        public static BudgetGroupXrefPlacement Map(BudgetGroupXrefPlacementProposal placement, Guid budgetGroupId, Guid createdBy)
        {
            var budgetGroupPlacement = new BudgetGroupXrefPlacement()
            {
                BudgetGroupXrefPlacementId = placement.BudgetGroupXrefPlacementId,
                BudgetGroupId = budgetGroupId,
                PlacementId = placement.PlacementId,
                CreatedBy = placement.AssignedById ?? createdBy,
                Created = placement.AssignedDate
            };
            return budgetGroupPlacement;
        }

        public static BudgetGroupXrefConversionAction Map(BudgetGroupXrefConversionActionProposal conversionAction, Guid budgetGroupId, Guid createdBy)
        {
            var budgetGroupConversionAction = new BudgetGroupXrefConversionAction()
            {
                BudgetGroupXrefConversionActionId = conversionAction.BudgetGroupXrefConversionActionProposalId,
                BudgetGroupId = budgetGroupId,
                ConversionActionId = conversionAction.ConversionActionId,
                CreatedBy = conversionAction.CreatedById ?? createdBy,
                Created = conversionAction.Created
            };
            return budgetGroupConversionAction;
        }
    }
}
