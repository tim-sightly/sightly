﻿using System;
using System.Linq;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupAdStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupLocationStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupTimedBudgetStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupPlacementStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupConversionActionStates;
using Sightly.BusinessLayer.Common;

namespace Sightly.BusinessLayer.Mapping
{
    public static class ProposalToLive
    {
        public static BudgetGroup Map(BudgetGroupProposal proposal, Guid budgetGroupId, Guid createdBy)
        {
            DateTime now = DateTime.UtcNow;

            var budgetGroup = new BudgetGroup()
            {
                BudgetGroupId = budgetGroupId,
                BudgetGroupStatusId = (int)BudgetGroupStatus.Live,
                BudgetGroupName = proposal.BudgetGroupName,
                CreatedBy = createdBy,
                Created = now,

                BudgetGroupAds = proposal.BudgetGroupAdProposals.Select(a => Map(a, budgetGroupId, createdBy, now)).ToList(),
                BudgetGroupLocations = proposal.BudgetGroupLocationProposals.Select(l => Map(l, budgetGroupId, createdBy, now)).ToList(),
                BudgetGroupTimedBudgets = proposal.BudgetGroupTimedBudgetProposals.Select(b => Map(b , budgetGroupId, createdBy, now)).ToList(),
                BudgetGroupPlacements = proposal.BudgetGroupPlacementProposals.Select(p => Map(p, budgetGroupId, createdBy, now)).ToList(),
                BudgetGroupConversionActions = proposal.BudgetGroupConversionActionProposals.Select(ca => Map(ca, budgetGroupId, createdBy, now)).ToList()
            };
            return budgetGroup;
        }

        public static BudgetGroupAd Map(BudgetGroupAdProposal proposal, Guid budgetGroupId, Guid createdBy, DateTime now)
        {
            var budgetGroupAd = new BudgetGroupAd()
            {
                BudgetGroupAdId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupId,
                AdId = proposal.AdId,
                CreatedBy = createdBy,
                Created = now,
            };
            return budgetGroupAd;
        }

        public static BudgetGroupLocation Map(BudgetGroupLocationProposal location, Guid budgetGroupId, Guid createdBy, DateTime now)
        {
            var budgetGroupLocation = new BudgetGroupLocation()
            {
                BudgetGroupLocationId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupId,
                LocationId = location.LocationId,
                CreatedBy = createdBy,
                Created = now
            };
            return budgetGroupLocation;
        }

        public static BudgetGroupTimedBudget Map(BudgetGroupTimedBudgetProposal timedBudget, Guid budgetGroupId, Guid createdBy, DateTime now)
        {
            var budgetGroupTimedBudget = new BudgetGroupTimedBudget()
            {
                BudgetGroupTimedBudgetId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupId,
                BudgetAmount = timedBudget.BudgetAmount,
                Margin = timedBudget.Margin,
                StartDate = timedBudget.StartDate,
                EndDate = timedBudget.EndDate,
                CreatedBy = createdBy,
                Created = now,
                BudgetGroupXrefPlacementId = timedBudget.BudgetGroupXrefPlacementId,
                OrderId = timedBudget.OrderId
            };
            return budgetGroupTimedBudget;
        }

        public static BudgetGroupPlacement Map(BudgetGroupPlacementProposal placement, Guid budgetGroupId, Guid createdBy, DateTime now)
        {
            var budgetGroupPlacement = new BudgetGroupPlacement()
            {
                BudgetGroupPlacementId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupId,
                PlacementId = placement.PlacementId,
                CreatedBy = createdBy,
                Created = now
            };
            return budgetGroupPlacement;
        }

        public static BudgetGroupConversionAction Map(BudgetGroupConversionActionProposal conversionAction, Guid budgetGroupId, Guid createdBy, DateTime now)
        {
            var budgetGroupConversionAction = new BudgetGroupConversionAction()
            {
                //BudgetGroupConversionActionId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupId,
                ConversionActionId = conversionAction.ConversionActionId,
                CreatedBy = createdBy,
                Created = now
            };
            return budgetGroupConversionAction;
        }
    }
}
