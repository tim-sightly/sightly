﻿using Sightly.DAL.DTO.BudgetGroupStates;
using System;
using System.Linq;
using Sightly.DAL.DTO.BudgetGroupTimedBudgetStates;
using Sightly.DAL.DTO.BudgetGroupXrefAdStates;
using Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates;
using Sightly.DAL.DTO.BudgetGroupXrefLocationStates;
using Sightly.DAL.DTO.BudgetGroupXrefPlacementStates;

namespace Sightly.BusinessLayer.Mapping
{
    public static class LiveToHistoryDto
    {
        public static BudgetGroupHistory Map(BudgetGroup budgetGroup, Guid budgetGroupHistoryId, DateTime now)
        {
            if (budgetGroup == null)
                return null;

            var budgetGroupHistory = new BudgetGroupHistory()
            {
                BudgetGroupId = budgetGroupHistoryId,
                BudgetGroupStatusId = budgetGroup.BudgetGroupStatusId,
                BudgetGroupName = budgetGroup.BudgetGroupName,
                CreatedBy = budgetGroup.CreatedBy,
                Created = budgetGroup.Created,
                ModifiedBy = budgetGroup.ModifiedBy,
                Modified = budgetGroup.Modified,
                BudgetGroupXrefAdHistories = budgetGroup.BudgetGroupXrefAds.Select(a => Map(a, budgetGroupHistoryId)).ToList(),
                BudgetGroupXrefLocationHistories = budgetGroup.BudgetGroupXrefLocations.Select(l => Map(l, budgetGroupHistoryId)).ToList(),
                BudgetGroupTimedBudgetHistories = budgetGroup.BudgetGroupTimedBudgets.Select(b => Map(b, budgetGroupHistoryId)).ToList(),
                BudgetGroupXrefPlacementHistories = budgetGroup.BudgetGroupXrefPlacements.Select(p => Map(p, budgetGroupHistoryId, now)).ToList(),
                BudgetGroupXrefConversionActionHistories = budgetGroup.BudgetGroupXrefConversionActions.Select(ca => Map(ca, budgetGroupHistoryId)).ToList()
            };
            return budgetGroupHistory;
        }

        public static BudgetGroupXrefAdHistory Map(BudgetGroupXrefAd ad, Guid budgetGroupHistoryId)
        {
            if (ad == null)
                return null;

            var budgetGroupAdHistory = new BudgetGroupXrefAdHistory()
            {
                BudgetGroupXrefAdId = ad.BudgetGroupXrefAdId,
                BudgetGroupId = budgetGroupHistoryId,
                AdId = ad.AdId,
                CreatedBy = ad.CreatedBy,
                Created = ad.Created
            };
            return budgetGroupAdHistory;
        }

        public static BudgetGroupXrefLocationHistory Map(BudgetGroupXrefLocation location, Guid budgetGroupHistoryId)
        {
            if (location == null)
                return null;

            var budgetGroupLocationHistory = new BudgetGroupXrefLocationHistory()
            {
                BudgetGroupXrefLocationId = location.BudgetGroupXrefLocationId,
                BudgetGroupId = budgetGroupHistoryId,
                LocationId = location.LocationId,
                CreatedBy = location.CreatedBy,
                Created = location.Created,
            };
            return budgetGroupLocationHistory;
        }

        public static BudgetGroupTimedBudgetHistory Map(BudgetGroupTimedBudget timedBudget, Guid budgetGroupHistoryId)
        {
            if (timedBudget == null)
                return null;

            var budgetGroupTimedBudgetHistory = new BudgetGroupTimedBudgetHistory()
            {
                BudgetGroupTimedBudgetId = timedBudget.BudgetGroupTimedBudgetId,
                BudgetGroupId = budgetGroupHistoryId,
                BudgetAmount = timedBudget.BudgetAmount,
                Margin = timedBudget.Margin,
                StartDate = timedBudget.StartDate,
                EndDate = timedBudget.EndDate,
                CreatedBy = timedBudget.CreatedBy,
                Created = timedBudget.Created,
                BudgetGroupXrefPlacementId = timedBudget.BudgetGroupXrefPlacementId,
                OrderId = timedBudget.OrderId
            };
            return budgetGroupTimedBudgetHistory;
        }

        public static BudgetGroupXrefPlacementHistory Map(BudgetGroupXrefPlacement placement, Guid budgetGroupHistoryId, DateTime now)
        {
            if (placement == null)
                return null;

            var budgetGroupPlacementHistory = new BudgetGroupXrefPlacementHistory()
            {
                BudgetGroupXrefPlacementId = placement.BudgetGroupXrefPlacementId,
                BudgetGroupId = budgetGroupHistoryId,
                PlacementId = placement.PlacementId,
                CreatedBy = placement.CreatedBy,
                Created = placement.Created,
                AddedToHistory = now
            };
            return budgetGroupPlacementHistory;
        }

        public static BudgetGroupXrefConversionActionHistory Map(BudgetGroupXrefConversionAction conversionAction, Guid budgetGroupHistoryId)
        {
            if (conversionAction == null)
                return null;

            var budgetGroupConversionActionHistory = new BudgetGroupXrefConversionActionHistory()
            {
                BudgetGroupXrefConversionActionHistoryId = Guid.NewGuid(),
                BudgetGroupHistoryId = budgetGroupHistoryId,
                ConversionActionId = conversionAction.ConversionActionId,
                CreatedBy = conversionAction.CreatedBy,
                Created = conversionAction.Created,
                ModifiedBy = conversionAction.ModifiedBy,
                Modified = conversionAction.Modified
            };
            return budgetGroupConversionActionHistory;
        }
    }
}
