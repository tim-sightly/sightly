﻿using Sightly.BusinessLayer.DomainObjects.BudgetGroupAdStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupConversionActionStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupLocationStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupPlacementStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupTimedBudgetStates;
using System;
using System.Linq;
using Sightly.BusinessLayer.Common;


namespace Sightly.BusinessLayer.Mapping
{
    public static class LiveToProposal
    {
        public static BudgetGroupProposal Map(BudgetGroup budgetGroup, Guid budgetGroupProposalId, Guid createdBy)
        {
            //if (budgetGroup == null)
            //{
            //    return null;
            //}

            //var now = DateTime.UtcNow;

            //var budgetGroupProposal = new BudgetGroupProposal
            //{
            //    BudgetGroupProposalId = budgetGroupProposalId,
            //    BudgetGroupStatusId = (int)BudgetGroupStatus.Draft,
            //    BudgetGroupName = budgetGroup.BudgetGroupName,
            //    BudgetGroupBudget = budgetGroup.BudgetGroupBudget,
            //    CreatedBy = createdBy,
            //    Created = now,
            //    BudgetGroupAdProposals = budgetGroup.BudgetGroupAds.Select(a => Map(a, budgetGroupProposalId, createdBy, now)).ToList(),
            //    BudgetGroupLocationProposals = budgetGroup.BudgetGroupLocations.Select(l => Map(l, budgetGroupProposalId, createdBy, now)).ToList(),
            //    BudgetGroupTimedBudgetProposal = Map(budgetGroup.BudgetGroupTimedBudget, budgetGroupProposalId, createdBy, now),
            //    BudgetGroupPlacementProposals = budgetGroup.BudgetGroupPlacements.Select(p => Map(p, budgetGroupProposalId)).ToList(),
            //    BudgetGroupConversionActionProposals = budgetGroup.BudgetGroupConversionActions.Select(c =>Map(c, budgetGroupProposalId, createdBy, now)).ToList()

            //};
            //return budgetGroupProposal;
            return null;
        }

        public static BudgetGroupAdProposal Map(BudgetGroupAd bgAd, Guid budgetGroupProposalId, Guid createdBy, DateTime now)
        {
            //var budgetGroupAdProposal = new BudgetGroupAdProposal()
            //{
            //    BudgetGroupAdProposalId = Guid.NewGuid(),
            //    BudgetGroupProposalId = budgetGroupProposalId,
            //    AdId = bgAd.AdId,
            //    CreatedBy = createdBy,
            //    Created = now,
            //};
            //return budgetGroupAdProposal;
            return null;
        }

        public static BudgetGroupLocationProposal Map(BudgetGroupLocation location, Guid budgetGroupProposalId, Guid createdBy, DateTime now)
        {
            //var budgetGroupLocationProposal = new BudgetGroupLocationProposal()
            //{
            //    BudgetGroupLocationProposalId = Guid.NewGuid(),
            //    BudgetGroupProposalId = budgetGroupProposalId,
            //    LocationId = location.LocationId,
            //    CreatedBy = createdBy,
            //    Created = now
            //};
            //return budgetGroupLocationProposal;
            return null;
        }

        public static BudgetGroupTimedBudgetProposal Map(BudgetGroupTimedBudget timedBudget, Guid budgetGroupProposalId, Guid createdBy, DateTime now)
        {
            var budgetGroupTimedBudgetProposal = new BudgetGroupTimedBudgetProposal()
            {
                BudgetGroupTimedBudgetProposalId = Guid.NewGuid(),
                BudgetGroupProposalId = budgetGroupProposalId,
                BudgetAmount = timedBudget.BudgetAmount,
                Margin = timedBudget.Margin,
                StartDate = timedBudget.StartDate,
                EndDate = timedBudget.EndDate,
                CreatedBy = createdBy,
                Created = now
            };
            return budgetGroupTimedBudgetProposal;
        }

        public static BudgetGroupPlacementProposal Map(BudgetGroupPlacement placement, Guid budgetGroupProposalId)
        {
            var budgetGroupPlacementProposal = new BudgetGroupPlacementProposal()
            {
                BudgetGroupPlacementId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupProposalId,
                PlacementId = placement.PlacementId,
                AssignedBy = placement.CreatedBy.ToString()
            };
            return budgetGroupPlacementProposal;
        }

        public static BudgetGroupConversionActionProposal Map(BudgetGroupConversionAction conversionAction, Guid budgetGroupProposalId, Guid createdBy, DateTime now)
        {
            var budgetGroupConversionActionProposal = new BudgetGroupConversionActionProposal()
            {
                BudgetGroupConversionActionProposalId = Guid.NewGuid(),
                BudgetGroupProposalId = budgetGroupProposalId,
                ConversionActionId = conversionAction.ConversionActionId,
                CreatedBy = createdBy,
                Created = now
            };
            return budgetGroupConversionActionProposal;
        }
    }
}
