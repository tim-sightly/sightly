﻿using Sightly.BusinessLayer.DomainObjects.BudgetGroupAdStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupConversionActionStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupLocationStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupPlacementStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupTimedBudgetStates;
using System;
using System.Linq;

namespace Sightly.BusinessLayer.Mapping
{
    public static class LiveToHistory
    {
        public static BudgetGroupHistory Map(BudgetGroup budgetGroup)
        {
            if (budgetGroup == null)
                return null;

            Guid budgetGroupHistoryId = Guid.NewGuid();

            var budgetGroupHistory = new BudgetGroupHistory()
            {
                BudgetGroupId = budgetGroupHistoryId,
                BudgetGroupStatusId = budgetGroup.BudgetGroupStatusId,
                BudgetGroupName = budgetGroup.BudgetGroupName,
                CreatedBy = budgetGroup.CreatedBy,
                Created = budgetGroup.Created,
                ModifiedBy = budgetGroup.ModifiedBy,
                Modified = budgetGroup.Modified,
                BudgetGroupAdHistories = budgetGroup.BudgetGroupAds.Select(a => Map(a, budgetGroupHistoryId)).ToList(),
                BudgetGroupLocationHistories = budgetGroup.BudgetGroupLocations.Select(l => Map(l, budgetGroupHistoryId)).ToList(),
                BudgetGroupTimedBudgetHistories = budgetGroup.BudgetGroupTimedBudgets.Select(b => Map(b, budgetGroupHistoryId)).ToList(),
                BudgetGroupPlacementHistories = budgetGroup.BudgetGroupPlacements.Select(p => Map(p, budgetGroupHistoryId)).ToList(),
                BudgetGroupConversionActionHistories = budgetGroup.BudgetGroupConversionActions.Select(ca => Map(ca, budgetGroupHistoryId)).ToList()
            };
            return budgetGroupHistory;
        }

        public static BudgetGroupAdHistory Map(BudgetGroupAd ad, Guid budgetGroupHistoryId)
        {
            if (ad == null)
                return null;

            var budgetGroupAdHistory = new BudgetGroupAdHistory()
            {
                BudgetGroupAdId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupHistoryId,
                AdId = ad.AdId,
                CreatedBy = ad.CreatedBy,
                Created = ad.Created
            };
            return budgetGroupAdHistory;
        }

        public static BudgetGroupLocationHistory Map(BudgetGroupLocation location, Guid budgetGroupHistoryId)
        {
            if (location == null)
                return null;

            var budgetGroupLocationHistory = new BudgetGroupLocationHistory()
            {
                BudgetGroupLocationId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupHistoryId,
                LocationId = location.LocationId,
                CreatedBy = location.CreatedBy,
                Created = location.Created,
            };
            return budgetGroupLocationHistory;
        }

        public static BudgetGroupTimedBudgetHistory Map(BudgetGroupTimedBudget timedBudget, Guid budgetGroupHistoryId)
        {
            if (timedBudget == null)
                return null;

            var budgetGroupTimedBudgetHistory = new BudgetGroupTimedBudgetHistory()
            {
                BudgetGroupTimedBudgetId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupHistoryId,
                BudgetAmount = timedBudget.BudgetAmount,
                Margin = timedBudget.Margin,
                StartDate = timedBudget.StartDate,
                EndDate = timedBudget.EndDate,
                CreatedBy = timedBudget.CreatedBy,
                Created = timedBudget.Created,
                BudgetGroupXrefPlacementId =  timedBudget.BudgetGroupXrefPlacementId,
                OrderId = timedBudget.OrderId
            };
            return budgetGroupTimedBudgetHistory;
        }

        public static BudgetGroupPlacementHistory Map(BudgetGroupPlacement placement, Guid budgetGroupHistoryId)
        {
            if (placement == null)
                return null;

            var budgetGroupPlacementHistory = new BudgetGroupPlacementHistory()
            {
                BudgetGroupPlacementId = Guid.NewGuid(),
                BudgetGroupId = budgetGroupHistoryId,
                PlacementId = placement.PlacementId,
                CreatedBy = placement.CreatedBy,
                Created =placement.Created
            };
            return budgetGroupPlacementHistory;
        }

        public static BudgetGroupConversionActionHistory Map(BudgetGroupConversionAction conversionAction, Guid budgetGroupHistoryId)
        {
            if (conversionAction == null)
                return null;

            var budgetGroupConversionActionHistory = new BudgetGroupConversionActionHistory()
            {
                BudgetGroupConversionActionHistoryId = Guid.NewGuid(),
                BudgetGroupHistoryId = budgetGroupHistoryId,
                ConversionActionId = conversionAction.ConversionActionId,
                CreatedBy = conversionAction.CreatedBy,
                Created = conversionAction.Created,
                ModifiedBy = conversionAction.ModifiedBy,
                Modified = conversionAction.Modified
            };
            return budgetGroupConversionActionHistory;
        }
    }
}
