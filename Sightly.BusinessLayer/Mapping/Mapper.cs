﻿using System;
using System.Linq;
using Sightly.BusinessLayer.Common;
using Sightly.BusinessLayer.DomainObjects.Audience;
using Sightly.DAL.DTO.BudgetGroupXrefAdStates;
using Sightly.DAL.DTO.BudgetGroupXrefLocationStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupTimedBudgetStates;
using Sightly.DAL.DTO.BudgetGroupXrefPlacementStates;
using Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupAdStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupConversionActionStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupLocationStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupPlacementStates;

using AccountDto = Sightly.DAL.DTO.Account;
using AdvertiserDto = Sightly.DAL.DTO.Advertiser;
using BudgetGroupDto = Sightly.DAL.DTO.BudgetGroupStates.BudgetGroup;
using BudgetGroupTimedBudgetDto = Sightly.DAL.DTO.BudgetGroupTimedBudgetStates.BudgetGroupTimedBudget;
using BudgetGroupProposalDto = Sightly.DAL.DTO.BudgetGroupStates.BudgetGroupProposal;
using BudgetGroupTimedBudgetProposalDto = Sightly.DAL.DTO.BudgetGroupTimedBudgetStates.BudgetGroupTimedBudgetProposal;
using BudgetGroupHistoryDto = Sightly.DAL.DTO.BudgetGroupStates.BudgetGroupHistory;
using BudgetGroupTimedBudgetHistoryDto = Sightly.DAL.DTO.BudgetGroupTimedBudgetStates.BudgetGroupTimedBudgetHistory;
using BudgetGroupLedgerDto = Sightly.DAL.DTO.BudgetGroupLedger;
using OrderDto = Sightly.DAL.DTO.Order;
using CustomerPacingDateDto = Sightly.DAL.DTO.CustomerPacingDate;
using DailyStatsDto = Sightly.DAL.DTO.DailyStats;
using Sightly.DAL.DTO;
using Account = Sightly.BusinessLayer.DomainObjects.Account;
using Advertiser = Sightly.BusinessLayer.DomainObjects.Advertiser;
using BudgetGroupLedger = Sightly.BusinessLayer.DomainObjects.BudgetGroupLedger;

using AdwordCustomer = Sightly.BusinessLayer.DomainObjects.AdwordCustomer;
using AdwordCustomerDto = Sightly.DAL.DTO.AdwordCustomer;
using KpiDefinitionData = Sightly.BusinessLayer.DomainObjects.KpiDefinitionData;
using KpiDefinitionDataDto = Sightly.DAL.DTO.KpiDefinitionData;
using KpiMetric = Sightly.BusinessLayer.DomainObjects.KpiMetric;
using KpiMetricDto = Sightly.DAL.DTO.KpiMetric;
using KpiCondition = Sightly.BusinessLayer.DomainObjects.KpiCondition;
using KpiConditionDto = Sightly.DAL.DTO.KpiCondition;
using KpiEditorData = Sightly.BusinessLayer.DomainObjects.KpiEditorData;
using KpiEditorDataDto = Sightly.DAL.DTO.KpiEditorData;
using AdwordsKpiStatsData = Sightly.BusinessLayer.DomainObjects.AdwordsKpiStatsData;
using AdwordsKpiStatsDataDto = Sightly.DAL.DTO.AdwordsKpiStatsData;
using MoatKpiStatsData = Sightly.BusinessLayer.DomainObjects.MoatKpiStatsData;
using MoatKpiStatsDataDto = Sightly.DAL.DTO.MoatKpiStatsData;
using NielsenKpiStatsData = Sightly.BusinessLayer.DomainObjects.NielsenKpiStatsData;
using NielsenKpiStatsDataDto = Sightly.DAL.DTO.NielsenKpiStatsData;
using DoubleVerifyKpiStatsData = Sightly.BusinessLayer.DomainObjects.DoubleVerifyKpiStatsData;
using DoubleVerifyKpiStatsDataDto = Sightly.DAL.DTO.DoubleVerifyKpiStatsData;
using ManagerDto = Sightly.DAL.DTO.Manager;
using Manager = Sightly.BusinessLayer.DomainObjects.Manager;
using CampaignAbbreviatedDto = Sightly.DAL.DTO.CampaignAbbreviated;
using CampaignAbbreviated = Sightly.Models.CampaignAbbreviated;

using Order = Sightly.BusinessLayer.DomainObjects.Order;
using User = Sightly.BusinessLayer.DomainObjects.User;
using Campaign = Sightly.BusinessLayer.DomainObjects.Campaign;
using UserDto = Sightly.DAL.DTO.User;
using CampaignDto = Sightly.DAL.DTO.Campaign;
using CampaignPacingData = Sightly.BusinessLayer.DomainObjects.CampaignPacingData;
using CampaignPacingDataDto = Sightly.DAL.DTO.CampaignPacingData;
using DailyStats = Sightly.BusinessLayer.DomainObjects.DailyStats;
using PlacementAssociation = Sightly.BusinessLayer.DomainObjects.PlacementAssociation;
using PlacementAssociationDto = Sightly.DAL.DTO.PlacementAssociation;
using OrderEmailDto = Sightly.DAL.DTO.OrderEmail;
using OrderChangesDto = Sightly.DAL.DTO.OrderChangeInfo;
using OrderChanges = Sightly.Models.OrderChangeInfo;
using OrderBasicDto = Sightly.DAL.DTO.OrderBasic;
using OrderBasic = Sightly.BusinessLayer.DomainObjects.OrderBasic;
using OrderTargetAgeGroupDto = Sightly.DAL.DTO.Audience.OrderTargetAgeGroup;
using OrderTargetGenderGroupDto = Sightly.DAL.DTO.Audience.OrderTargetGenderGroup;
using OrderParentalStatusGroupDto = Sightly.DAL.DTO.Audience.OrderParentalStatusGroup;
using OrderHouseholdIncomeGroupDto = Sightly.DAL.DTO.Audience.OrderHouseholdIncomeGroup;
using Sightly.Models;

namespace Sightly.BusinessLayer.Mapping
{
    public static class Mapper
    {
        #region budgetGroupDto -> budgetGroupDomain

        public static BudgetGroup Map(BudgetGroupDto budgetGroupDto)
        {
            if (budgetGroupDto == null)
                return null;

            var budgetGroup = new BudgetGroup()
            {
                BudgetGroupId = budgetGroupDto.BudgetGroupId,
                BudgetGroupStatusId = budgetGroupDto.BudgetGroupStatusId,
                BudgetGroupName = budgetGroupDto.BudgetGroupName,
                CreatedBy = budgetGroupDto.CreatedBy,
                Created = budgetGroupDto.Created,
                Modified = budgetGroupDto.Modified,
                ModifiedBy = budgetGroupDto.ModifiedBy,               
                BudgetGroupAds = budgetGroupDto.BudgetGroupXrefAds.Select(a => Map(a)).ToList(),
                BudgetGroupLocations = budgetGroupDto.BudgetGroupXrefLocations.Select(l => Map(l)).ToList(),
                BudgetGroupConversionActions = budgetGroupDto.BudgetGroupXrefConversionActions.Select(a => Map(a)).ToList(),
                BudgetGroupPlacements = budgetGroupDto.BudgetGroupXrefPlacements.Select(p => Map(p)).ToList(),
                BudgetGroupTimedBudgets = budgetGroupDto.BudgetGroupTimedBudgets.Select(b => Map(b)).ToList() 
            };
            return budgetGroup;
        }

        public static BudgetGroupAd Map(BudgetGroupXrefAd ad)
        {
            BudgetGroupAd budgetGroupAd = new BudgetGroupAd()
            {
                BudgetGroupAdId = ad.BudgetGroupXrefAdId,
                BudgetGroupId = ad.BudgetGroupId,
                AdId = ad.AdId,
                CreatedBy = ad.CreatedBy,
                Created = ad.Created,
            };
            return budgetGroupAd;
        }

        public static BudgetGroupLocation Map(BudgetGroupXrefLocation location)
        {
            if (location == null)
                return null;

            var budgetGroupLocation = new BudgetGroupLocation()
            {
                BudgetGroupLocationId = location.BudgetGroupXrefLocationId,
                BudgetGroupId = location.BudgetGroupId,
                LocationId = location.LocationId,
                CreatedBy = location.CreatedBy,
                Created = location.Created
            };
            return budgetGroupLocation;
        }

        public static BudgetGroupTimedBudget Map(BudgetGroupTimedBudgetDto timedBudget)
        {
            if (timedBudget == null)
                return null;

            var budgetGroupTimedBudget = new BudgetGroupTimedBudget()
            {
                BudgetGroupTimedBudgetId = timedBudget.BudgetGroupTimedBudgetId,
                BudgetGroupId = timedBudget.BudgetGroupId,
                BudgetAmount = timedBudget.BudgetAmount,
                Margin = timedBudget.Margin,
                StartDate = timedBudget.StartDate,
                EndDate = timedBudget.EndDate,
                CreatedBy = timedBudget.CreatedBy,
                Created = timedBudget.Created,
                BudgetGroupXrefPlacementId = timedBudget.BudgetGroupXrefPlacementId,
                OrderId = timedBudget.OrderId
            };
            return budgetGroupTimedBudget;
        }

        public static BudgetGroupPlacement Map(BudgetGroupXrefPlacement placement)
        {
            if (placement == null)
                return null;

            var budgetGroupPlacement = new BudgetGroupPlacement()
            {
                BudgetGroupPlacementId = placement.BudgetGroupXrefPlacementId,
                BudgetGroupId = placement.BudgetGroupId,
                PlacementId = placement.PlacementId,
                CreatedBy = placement.CreatedBy,
                Created = placement.Created
            };
            return budgetGroupPlacement;
        }

        public static BudgetGroupConversionAction Map(BudgetGroupXrefConversionAction ca)
        {
            if (ca == null)
                return null;

            var budgetGroupConversionAction = new BudgetGroupConversionAction()
            {
                BudgetGroupConversionActionId = ca.BudgetGroupXrefConversionActionId,
                BudgetGroupId = ca.BudgetGroupId,
                ConversionActionId = ca.ConversionActionId,
                CreatedBy = ca.CreatedBy,
                Created = ca.Created,
                ModifiedBy = ca.ModifiedBy,
                Modified = ca.Modified
            };
            return budgetGroupConversionAction;
        }

        #endregion

        #region budgetGroupDomain -> budgetGroupDto

        public static BudgetGroupDto Map(BudgetGroup budgetGroup)
        {
            if (budgetGroup == null)
                return null;

            var budgetGroupDto = new BudgetGroupDto()
            {
                BudgetGroupId = budgetGroup.BudgetGroupId.Value,
                BudgetGroupStatusId = budgetGroup.BudgetGroupStatusId.Value,
                BudgetGroupName = budgetGroup.BudgetGroupName,
                CreatedBy = budgetGroup.CreatedBy.Value,
                Created = budgetGroup.Created.Value,
                Modified = budgetGroup.Modified,
                ModifiedBy = budgetGroup.ModifiedBy,

                BudgetGroupXrefAds = budgetGroup.BudgetGroupAds?.Select(Map).ToList(),
                BudgetGroupXrefLocations = budgetGroup.BudgetGroupLocations?.Select(Map).ToList(),
                BudgetGroupTimedBudgets = budgetGroup.BudgetGroupTimedBudgets?.Select(Map).ToList(),
                BudgetGroupXrefPlacements = budgetGroup.BudgetGroupPlacements?.Select(Map).ToList(),
                BudgetGroupXrefConversionActions = budgetGroup.BudgetGroupConversionActions?.Select(Map).ToList()
            };
            return budgetGroupDto;
        }

        public static BudgetGroupXrefAd Map(BudgetGroupAd ad)
        {
            if (ad == null)
                return null;

            var budgetGroupXrefAd = new BudgetGroupXrefAd()
            {
                BudgetGroupXrefAdId = ad.BudgetGroupAdId,
                BudgetGroupId = ad.BudgetGroupId,
                AdId = ad.AdId.Value,
                CreatedBy = ad.CreatedBy.Value,
                Created = ad.Created.Value,
            };
            return budgetGroupXrefAd;
        }

        public static BudgetGroupXrefLocation Map(BudgetGroupLocation location)
        {
            if (location == null)
                return null;

            var budgetGroupXrefLocation = new BudgetGroupXrefLocation()
            {
                BudgetGroupXrefLocationId = location.BudgetGroupLocationId,
                BudgetGroupId = location.BudgetGroupId,
                LocationId = location.LocationId,
                CreatedBy = location.CreatedBy,
                Created = location.Created,
            };
            return budgetGroupXrefLocation;
        }

        public static BudgetGroupTimedBudgetDto Map(BudgetGroupTimedBudget timedBudget)
        {
            if (timedBudget == null)
                return null;

            var budgetGroupTimedBudgetDto = new BudgetGroupTimedBudgetDto()
            {
                BudgetGroupTimedBudgetId = timedBudget.BudgetGroupTimedBudgetId.Value,
                BudgetGroupId = timedBudget.BudgetGroupId.Value,
                BudgetAmount = timedBudget.BudgetAmount.Value,
                Margin = timedBudget.Margin ,
                StartDate = timedBudget.StartDate.Value,
                EndDate = timedBudget.EndDate.Value,
                CreatedBy = timedBudget.CreatedBy.Value,
                Created = timedBudget.Created.Value,
                BudgetGroupXrefPlacementId = timedBudget.BudgetGroupXrefPlacementId,
                OrderId = timedBudget.OrderId
            };
            return budgetGroupTimedBudgetDto;
        }

        public static BudgetGroupXrefPlacement Map(BudgetGroupPlacement placement)
        {
            if (placement == null)
                return null;

            var budgetGroupXrefPlacement = new BudgetGroupXrefPlacement()
            {
                BudgetGroupXrefPlacementId = placement.BudgetGroupPlacementId.Value,
                BudgetGroupId = placement.BudgetGroupId.Value,
                PlacementId = placement.PlacementId.Value,
                CreatedBy = placement.CreatedBy.Value,
                Created = placement.Created.Value
            };
            return budgetGroupXrefPlacement;
        }

        public static BudgetGroupXrefConversionAction Map(BudgetGroupConversionAction ca)
        {
            if (ca == null)
                return null;

            var budgetGroupXrefConversionAction = new BudgetGroupXrefConversionAction()
            {
                BudgetGroupXrefConversionActionId = ca.BudgetGroupConversionActionId.Value,
                BudgetGroupId = ca.BudgetGroupId,
                ConversionActionId = ca.ConversionActionId.Value,
                CreatedBy = ca.CreatedBy.Value,
                Created = ca.Created,
                ModifiedBy = ca.ModifiedBy,
                Modified = ca.Modified
            };
            return budgetGroupXrefConversionAction;
        }

        #endregion

        #region budgetGroupProposalDto -> budgetGroupPropsalDomain

        public static BudgetGroupProposal Map(BudgetGroupProposalDto budgetGroupProposalDto)
        {
            if (budgetGroupProposalDto == null)
                return null;

            var budgetGroupProposal = new BudgetGroupProposal()
            {
                BudgetGroupId = budgetGroupProposalDto.BudgetGroupId,
                BudgetGroupName = budgetGroupProposalDto.BudgetGroupName,
                BudgetGroupBudget = budgetGroupProposalDto.BudgetGroupBudget,
                CreatedBy = budgetGroupProposalDto.CreatedBy,
                CreatedDatetime = budgetGroupProposalDto.CreatedDatetime,
                LastModifiedBy = budgetGroupProposalDto.LastModifiedBy,
                LastModifiedDatetime = budgetGroupProposalDto.LastModifiedDatetime
            };

            if (budgetGroupProposalDto.BudgetGroupXrefAdProposals != null)
            {
                budgetGroupProposal.BudgetGroupAdProposals = budgetGroupProposalDto.BudgetGroupXrefAdProposals.Select(a => Map(a)).ToList();
            }

            if (budgetGroupProposalDto.BudgetGroupXrefLocationProposals != null)
            {
                budgetGroupProposal.BudgetGroupLocationProposals = budgetGroupProposalDto.BudgetGroupXrefLocationProposals.Select(l => Map(l)).ToList();
            }

            if (budgetGroupProposalDto.BudgetGroupTimedBudgetProposals != null)
            {
                budgetGroupProposal.BudgetGroupTimedBudgetProposals = budgetGroupProposalDto.BudgetGroupTimedBudgetProposals.Select(b => Map(b)).ToList();
            }

            if (budgetGroupProposalDto.BudgetGroupXrefConversionActionProposals != null)
            {
                budgetGroupProposal.BudgetGroupConversionActionProposals = budgetGroupProposalDto.BudgetGroupXrefConversionActionProposals.Select(a => Map(a)).ToList();
            }

            if (budgetGroupProposalDto.BudgetGroupXrefPlacementProposals != null)
            {
                budgetGroupProposal.BudgetGroupPlacementProposals = budgetGroupProposalDto.BudgetGroupXrefPlacementProposals.Select(p => Map(p)).ToList();
            }

            return budgetGroupProposal;
        }

        public static BudgetGroupAdProposal Map(BudgetGroupXrefAdProposal ad)
        {
            BudgetGroupAdProposal budgetGroupAdProposal = new BudgetGroupAdProposal()
            {
                BudgetGroupAdId = ad.BudgetGroupXrefAdId,
                BudgetGroupId = ad.BudgetGroupId,
                AdId = ad.AdId,
                CreatedBy = ad.CreatedBy,
                CreatedDatetime = ad.CreatedDatetime,
                LastModifiedBy = ad.LastModifiedBy,
                LastModifiedDatetime = ad.LastModifiedDatetime
            };
            return budgetGroupAdProposal;
        }

        public static BudgetGroupLocationProposal Map(BudgetGroupXrefLocationProposal location)
        {
            BudgetGroupLocationProposal locationDom = null;
            if (location != null)
            {
                locationDom = new BudgetGroupLocationProposal()
                {
                    BudgetGroupLocationId = location.BudgetGroupXrefLocationId,
                    BudgetGroupId = location.BudgetGroupId,
                    LocationId = location.LocationId,
                    CreatedBy = location.CreatedBy,
                    CreatedDatetime = location.CreatedDatetime,
                    LastModifiedBy = location.LastModifiedBy,
                    LastModifiedDatetime = location.LastModifiedDatetime
                };
            }
            return locationDom;
        }

        public static BudgetGroupConversionActionProposal Map(BudgetGroupXrefConversionActionProposal conversionAction)
        {
            BudgetGroupConversionActionProposal conversionActionDom = null;
            if (conversionAction != null)
            {
                conversionActionDom = new BudgetGroupConversionActionProposal()
                {
                    BudgetGroupConversionActionProposalId = conversionAction.BudgetGroupXrefConversionActionProposalId,
                    BudgetGroupProposalId = conversionAction.BudgetGroupProposalId,
                    ConversionActionId = conversionAction.ConversionActionId,
                    CreatedBy = conversionAction.CreatedBy,
                    Created = conversionAction.Created,
                    ModifiedBy = conversionAction.ModifiedBy,
                    Modified = conversionAction.Modified
                };
            }
            return conversionActionDom;
        }

        public static BudgetGroupPlacementProposal Map(BudgetGroupXrefPlacementProposal placement)
        {
            BudgetGroupPlacementProposal budgetGroupPlacementProposal = null;
            if (placement != null)
            {
                budgetGroupPlacementProposal = new BudgetGroupPlacementProposal()
                {
                    BudgetGroupPlacementId = placement.BudgetGroupXrefPlacementId,
                    BudgetGroupId = placement.BudgetGroupId,
                    PlacementId = placement.PlacementId,
                    AssignedBy = placement.AssignedBy,
                    AssignedDate = placement.AssignedDate,
                };
            }
            return budgetGroupPlacementProposal;
        }

        public static BudgetGroupTimedBudgetProposal Map(BudgetGroupTimedBudgetProposalDto timedBudgetDto)
        {
            if (timedBudgetDto == null)
                return null;

            var budgetGroupTimedBudgetProposal = new BudgetGroupTimedBudgetProposal()
            {
                BudgetGroupTimedBudgetId = timedBudgetDto.BudgetGroupTimedBudgetId,
                BudgetGroupId = timedBudgetDto.BudgetGroupId,
                BudgetAmount = timedBudgetDto.BudgetAmount,
                Margin = timedBudgetDto.Margin,
                StartDate = timedBudgetDto.StartDate,
                EndDate = timedBudgetDto.EndDate,
                CreatedBy = timedBudgetDto.CreatedBy,
                CreatedDatetime = timedBudgetDto.CreatedDatetime,
                BudgetGroupXrefPlacementId = timedBudgetDto.BudgetGroupXrefPlacementId,
                OrderId = timedBudgetDto.OrderId
            };
            return budgetGroupTimedBudgetProposal;
        }

        #endregion

        #region budgetGroupProposalDomain -> budgetGroupProposalDto

        //public static BudgetGroupProposalDto Map(BudgetGroupProposal budgetGroupProposal)
        //{
        //    if (budgetGroupProposal == null)
        //        return null;

        //    var budgetGroupProposalDto = new BudgetGroupProposalDto()
        //    {
        //        BudgetGroupProposalId = budgetGroupProposal.BudgetGroupProposalId.Value,
        //        BudgetGroupStatusId = budgetGroupProposal.BudgetGroupStatusId.Value,
        //        BudgetGroupName = budgetGroupProposal.BudgetGroupName,
        //        BudgetGroupBudget = budgetGroupProposal.BudgetGroupBudget.Value,
        //        CreatedBy = budgetGroupProposal.CreatedBy,
        //        Created = budgetGroupProposal.Created,
        //        ModifiedBy = budgetGroupProposal.ModifiedBy,
        //        Modified = budgetGroupProposal.Modified,
        //        BudgetGroupXrefAdProposals = (budgetGroupProposal.BudgetGroupAdProposals == null) ? null : budgetGroupProposal.BudgetGroupAdProposals.Select(a => Map(a)).ToList(),
        //        BudgetGroupXrefLocationProposals = (budgetGroupProposal.BudgetGroupLocationProposals == null) ? null : budgetGroupProposal.BudgetGroupLocationProposals.Select(l => Map(l)).ToList(),
        //        BudgetGroupXrefConversionActionProposals = (budgetGroupProposal.BudgetGroupConversionActionProposals == null) ? null : budgetGroupProposal.BudgetGroupConversionActionProposals.Select(a => Map(a)).ToList(),
        //        BudgetGroupXrefPlacementProposals = (budgetGroupProposal.BudgetGroupPlacementProposals == null) ? null : budgetGroupProposal.BudgetGroupPlacementProposals.Select(p => Map(p)).ToList(),
        //        BudgetGroupTimedBudgetProposal = Map(budgetGroupProposal.BudgetGroupTimedBudgetProposal)
        //    };
        //    return budgetGroupProposalDto;
        //}

        public static BudgetGroupXrefAdProposal Map(BudgetGroupAdProposal ad)
        {
            if (ad == null)
                return null;

            var budgetGroupXrefAdProposal = new BudgetGroupXrefAdProposal()
            {
                BudgetGroupXrefAdId = ad.BudgetGroupAdId,
                BudgetGroupId = ad.BudgetGroupId,
                AdId = ad.AdId.Value,
                CreatedBy = ad.CreatedBy,
                CreatedDatetime = ad.CreatedDatetime,
                LastModifiedBy = ad.LastModifiedBy,
                LastModifiedDatetime = ad.LastModifiedDatetime
            };
            return budgetGroupXrefAdProposal;
        }

        public static BudgetGroupXrefLocationProposal Map(BudgetGroupLocationProposal location)
        {
            if (location == null)
                return null;

            var budgetGroupXrefLocationProposal = new BudgetGroupXrefLocationProposal()
            {
                BudgetGroupXrefLocationId = location.BudgetGroupLocationId,
                BudgetGroupId = location.BudgetGroupId,
                LocationId = location.LocationId,
                CreatedBy = location.CreatedBy,
                CreatedDatetime = location.CreatedDatetime,
                LastModifiedBy = location.LastModifiedBy,
                LastModifiedDatetime = location.LastModifiedDatetime
            };
            return budgetGroupXrefLocationProposal;
        }

        public static BudgetGroupXrefConversionActionProposal Map(BudgetGroupConversionActionProposal conversionAction)
        {
            var budgetGroupXrefConversionActionProposal = new BudgetGroupXrefConversionActionProposal()
            {
                BudgetGroupXrefConversionActionProposalId = conversionAction.BudgetGroupConversionActionProposalId.Value,
                BudgetGroupProposalId = conversionAction.BudgetGroupProposalId.Value,
                ConversionActionId = conversionAction.ConversionActionId.Value,
                CreatedBy = conversionAction.CreatedBy.Value,
                Created = conversionAction.Created,
                ModifiedBy = conversionAction.ModifiedBy,
                Modified = conversionAction.Modified
            };
            return budgetGroupXrefConversionActionProposal;
        }

        public static BudgetGroupXrefPlacementProposal Map(BudgetGroupPlacementProposal placement)
        {
            var budgetGroupXrefPlacementProposal = new BudgetGroupXrefPlacementProposal()
            {
                BudgetGroupXrefPlacementId = placement.BudgetGroupPlacementId.Value,
                BudgetGroupId = placement.BudgetGroupId.Value,
                PlacementId = placement.PlacementId.Value,
                AssignedBy = placement.AssignedBy,
                AssignedDate = placement.AssignedDate
            };
            return budgetGroupXrefPlacementProposal;
        }

        public static BudgetGroupTimedBudgetProposalDto Map(BudgetGroupTimedBudgetProposal timedBudget)
        {
            if (timedBudget == null)
                return null;

            var budgetGroupTimedBudgetProposalDto = new BudgetGroupTimedBudgetProposalDto()
            {
                BudgetGroupTimedBudgetId = timedBudget.BudgetGroupTimedBudgetId.Value,
                BudgetGroupId = timedBudget.BudgetGroupId.Value,
                BudgetAmount = timedBudget.BudgetAmount.Value,
                Margin = timedBudget.Margin,
                StartDate = timedBudget.StartDate.Value,
                EndDate = timedBudget.EndDate.Value,
                CreatedBy = timedBudget.CreatedBy,
                CreatedDatetime = timedBudget.CreatedDatetime,
                BudgetGroupXrefPlacementId = timedBudget.BudgetGroupXrefPlacementId,
                OrderId = timedBudget.OrderId
            };
            return budgetGroupTimedBudgetProposalDto;
        }

        #endregion

        #region budgetGroupHistoryDto -> budgetGroupHistoryDomain

        public static BudgetGroupHistory Map(BudgetGroupHistoryDto budgetGroupHistoryDto)
        {
            if (budgetGroupHistoryDto == null)
                return null;

            var budgetGroupHistory = new BudgetGroupHistory()
            {
                BudgetGroupId = budgetGroupHistoryDto.BudgetGroupId,
                BudgetGroupName = budgetGroupHistoryDto.BudgetGroupName,
                CreatedBy = budgetGroupHistoryDto.CreatedBy,
                Created = budgetGroupHistoryDto.Created,
                BudgetGroupAdHistories = budgetGroupHistoryDto.BudgetGroupXrefAdHistories.Select(a => Map(a)).ToList(),
                BudgetGroupLocationHistories = budgetGroupHistoryDto.BudgetGroupXrefLocationHistories.Select(l => Map(l)).ToList(),
                BudgetGroupConversionActionHistories = budgetGroupHistoryDto.BudgetGroupXrefConversionActionHistories.Select(a => Map(a)).ToList(),
                BudgetGroupPlacementHistories = budgetGroupHistoryDto.BudgetGroupXrefPlacementHistories.Select(p => Map(p)).ToList(),
                BudgetGroupTimedBudgetHistories = budgetGroupHistoryDto.BudgetGroupTimedBudgetHistories.Select(b => Map(b)).ToList()
            };
            return budgetGroupHistory;
        }

        public static BudgetGroupAdHistory Map(BudgetGroupXrefAdHistory ad)
        {
            if (ad == null)
                return null;

            var budgetGroupAdHistory = new BudgetGroupAdHistory()
            {
                BudgetGroupAdId = ad.BudgetGroupXrefAdId,
                BudgetGroupId = ad.BudgetGroupId,
                AdId = ad.AdId,
                CreatedBy = ad.CreatedBy,
                Created = ad.Created
            };
            return budgetGroupAdHistory;
        }

        public static BudgetGroupLocationHistory Map(BudgetGroupXrefLocationHistory location)
        {
            if (location == null)
                return null;

            var budgetGroupLocationHistory = new BudgetGroupLocationHistory()
            {
                BudgetGroupLocationId = location.BudgetGroupXrefLocationId,
                BudgetGroupId = location.BudgetGroupId,
                LocationId = location.LocationId,
                CreatedBy = location.CreatedBy,
                Created = location.Created
            };
            return budgetGroupLocationHistory;
        }

        public static BudgetGroupConversionActionHistory Map(BudgetGroupXrefConversionActionHistory conversionAction)
        {
            if (conversionAction == null)
                return null;

            var budgetGroupConversionActionHistory = new BudgetGroupConversionActionHistory()
            {
                BudgetGroupConversionActionHistoryId = conversionAction.BudgetGroupXrefConversionActionHistoryId,
                BudgetGroupHistoryId = conversionAction.BudgetGroupHistoryId,
                ConversionActionId = conversionAction.ConversionActionId,
                CreatedBy = conversionAction.CreatedBy,
                Created = conversionAction.Created
            };
            return budgetGroupConversionActionHistory;
        }

        public static BudgetGroupPlacementHistory Map(BudgetGroupXrefPlacementHistory placement)
        {
            if (placement == null)
                return null;

            var budgetGroupPlacementHistory = new BudgetGroupPlacementHistory()
            {
                BudgetGroupPlacementId = placement.BudgetGroupXrefPlacementId,
                BudgetGroupId = placement.BudgetGroupId,
                PlacementId = placement.PlacementId,
                CreatedBy = placement.CreatedBy,
                Created = placement.Created
            };
            return budgetGroupPlacementHistory;
        }

        public static BudgetGroupTimedBudgetHistory Map(BudgetGroupTimedBudgetHistoryDto timedBudgetDto)
        {
            if (timedBudgetDto == null)
                return null;

            var budgetGroupTimedBudgetHistory = new BudgetGroupTimedBudgetHistory()
            {
                BudgetGroupTimedBudgetId = timedBudgetDto.BudgetGroupTimedBudgetId,
                BudgetGroupId = timedBudgetDto.BudgetGroupId,
                BudgetAmount = timedBudgetDto.BudgetAmount,
                Margin = timedBudgetDto.Margin,
                StartDate = timedBudgetDto.StartDate,
                EndDate = timedBudgetDto.EndDate,
                CreatedBy = timedBudgetDto.CreatedBy,
                Created = timedBudgetDto.Created,
                BudgetGroupXrefPlacementId = timedBudgetDto.BudgetGroupXrefPlacementId,
                OrderId = timedBudgetDto.OrderId
            };
            return budgetGroupTimedBudgetHistory;
        }

        #endregion

        #region budgetGroupHistoryDomain -> budgetGroupHistoryDto

        public static BudgetGroupHistoryDto Map(BudgetGroupHistory budgetGroupHistory)
        {
            if (budgetGroupHistory == null)
                return null;

            var budgetGroupHistoryDto = new BudgetGroupHistoryDto()
            {
                BudgetGroupId = budgetGroupHistory.BudgetGroupId.Value,
                BudgetGroupStatusId = budgetGroupHistory.BudgetGroupStatusId.Value,
                BudgetGroupName = budgetGroupHistory.BudgetGroupName,
                CreatedBy = budgetGroupHistory.CreatedBy.Value,
                Created = budgetGroupHistory.Created.Value,

                BudgetGroupXrefAdHistories = budgetGroupHistory.BudgetGroupAdHistories.Select(a => Map(a)).ToList(),
                BudgetGroupXrefLocationHistories = budgetGroupHistory.BudgetGroupLocationHistories.Select(l => Map(l)).ToList(),
                BudgetGroupXrefConversionActionHistories = budgetGroupHistory.BudgetGroupConversionActionHistories.Select(a => Map(a)).ToList(),
                BudgetGroupXrefPlacementHistories = budgetGroupHistory.BudgetGroupPlacementHistories.Select(p => Map(p)).ToList(),
                BudgetGroupTimedBudgetHistories = budgetGroupHistory.BudgetGroupTimedBudgetHistories.Select(b => Map(b)).ToList()
            };
            return budgetGroupHistoryDto;
        }

        public static BudgetGroupXrefAdHistory Map(BudgetGroupAdHistory ad)
        {
            if (ad == null)
                return null;

            var budgetGroupXrefAdHistory = new BudgetGroupXrefAdHistory()
            {
                BudgetGroupXrefAdId = ad.BudgetGroupAdId,
                BudgetGroupId = ad.BudgetGroupId,
                AdId = ad.AdId.Value,
                CreatedBy = ad.CreatedBy.Value,
                Created = ad.Created.Value
            };
            return budgetGroupXrefAdHistory;
        }

        public static BudgetGroupXrefLocationHistory Map(BudgetGroupLocationHistory location)
        {
            if (location == null)
                return null;

            var budgetGroupXrefLocationHistory = new BudgetGroupXrefLocationHistory()
            {
                BudgetGroupXrefLocationId = location.BudgetGroupLocationId,
                BudgetGroupId = location.BudgetGroupId,
                LocationId = location.LocationId,
                CreatedBy = location.CreatedBy,
                Created = location.Created
            };
            return budgetGroupXrefLocationHistory;
        }

        public static BudgetGroupXrefConversionActionHistory Map(BudgetGroupConversionActionHistory conversionAction)
        {
            if (conversionAction == null)
                return null;

            var budgetGroupXrefConversionActionHistory = new BudgetGroupXrefConversionActionHistory()
            {
                BudgetGroupXrefConversionActionHistoryId = conversionAction.BudgetGroupConversionActionHistoryId,
                BudgetGroupHistoryId = conversionAction.BudgetGroupHistoryId,
                ConversionActionId = conversionAction.ConversionActionId.Value,
                CreatedBy = conversionAction.CreatedBy.Value,
                Created = conversionAction.Created,
                ModifiedBy = conversionAction.ModifiedBy,
                Modified = conversionAction.Modified,
            };
            return budgetGroupXrefConversionActionHistory;
        }

        public static BudgetGroupXrefPlacementHistory Map(BudgetGroupPlacementHistory placement)
        {
            if (placement == null)
                return null;

            var budgetGroupXrefPlacementHistory = new BudgetGroupXrefPlacementHistory()
            {
                BudgetGroupXrefPlacementId = placement.BudgetGroupPlacementId.Value,
                BudgetGroupId = placement.BudgetGroupId.Value,
                PlacementId = placement.PlacementId.Value,
                CreatedBy = placement.CreatedBy.Value,
                Created = placement.Created.Value
            };
            return budgetGroupXrefPlacementHistory;
        }

        public static BudgetGroupTimedBudgetHistoryDto Map(BudgetGroupTimedBudgetHistory timedBudgetHistory)
        {
            if (timedBudgetHistory == null)
                return null;

            var budgetGroupTimedBudgetHistoryDto = new BudgetGroupTimedBudgetHistoryDto()
            {
                BudgetGroupTimedBudgetId = timedBudgetHistory.BudgetGroupTimedBudgetId.Value,
                BudgetGroupId = timedBudgetHistory.BudgetGroupId.Value,
                BudgetAmount = timedBudgetHistory.BudgetAmount.Value,
                Margin = timedBudgetHistory.Margin,
                StartDate = timedBudgetHistory.StartDate.Value,
                EndDate = timedBudgetHistory.EndDate.Value,
                CreatedBy = timedBudgetHistory.CreatedBy.Value,
                Created = timedBudgetHistory.Created.Value,
                BudgetGroupXrefPlacementId = timedBudgetHistory.BudgetGroupXrefPlacementId,
                OrderId = timedBudgetHistory.OrderId
            };
            return budgetGroupTimedBudgetHistoryDto;
        }

        #endregion

        #region Ledger

        public static BudgetGroupLedgerDto Map(BudgetGroupLedger ledger)
        {
            if (ledger == null)
                return null;

            var budgetGroupLedgerDto = new BudgetGroupLedgerDto()
            {
                BudgetGroupLedgerId = ledger.BudgetGroupLedgerId,
                BudgetGroupId = ledger.BudgetGroupId,
                BudgetGroupProposalId = ledger.BudgetGroupProposalId,
                BudgetGroupHistoryId = ledger.BudgetGroupHistoryId,
                CreatedBy = ledger.CreatedBy.Value,
                Created = ledger.Created.Value,
                BudgetGroupEventId = (int)ledger.BudgetGroupEventId,
                Notes = ledger.Notes
            };
            return budgetGroupLedgerDto;
        }

        public static BudgetGroupLedger Map(BudgetGroupLedgerDto ledger)
        {
            if (ledger == null)
                return null;

            var budgetGroupLedgerDto = new BudgetGroupLedger()
            {
                BudgetGroupLedgerId = ledger.BudgetGroupLedgerId,
                BudgetGroupId = ledger.BudgetGroupId,
                BudgetGroupProposalId = ledger.BudgetGroupProposalId,
                BudgetGroupHistoryId = ledger.BudgetGroupHistoryId,
                CreatedBy = ledger.CreatedBy,
                Created = ledger.Created,
                BudgetGroupEventId = (BudgetGroupEvent)ledger.BudgetGroupEventId,
                Notes = ledger.Notes
            };
            return budgetGroupLedgerDto;
        }

        #endregion

        #region Order

        public static OrderDto Map(Order order)
        {
            if (order == null)
                return null;

            var orderDto = new OrderDto()
            {
                OrderId = order.OrderId,
                OrderTypeId = order.OrderTypeId,
                OrderName = order.OrderName,
                OrderRefCode = order.OrderRefCode,
                AccountId = order.AccountId,
                CampaignId = order.CampaignId,
                AdvertiserId = order.AdvertiserId,
                AdvertiserSubCategoryId = order.AdvertiserSubCategoryId,
                BudgetTypeId = order.BudgetTypeId,
                CampaignBudget = order.CampaignBudget,
                MonthlyBudget = order.MonthlyBudget,
                MonthCount = order.MonthCount,
                StartDate = order.StartDate,
                StartDateHard = order.StartDateHard,
                EndDate = order.EndDate,
                EndDateHard = order.EndDateHard,
                SpendFlex = order.SpendFlex,
                DeliveryPriorityId = order.DeliveryPriorityId,
                Deleted = order.Deleted,
                CreatedDatetime = order.CreatedDatetime,
                CreatorId = order.CreatorId,
                CreatedBy = order.CreatedBy,
                LastModifiedBy = order.LastModifiedBy,
                LastModifiedDateTime = order.LastModifiedDateTime
            };
            return orderDto;
        }

        public static Order Map(OrderDto orderDto)
        {
            if (orderDto == null)
                return null;

            var order = new Order()
            {
                OrderId = orderDto.OrderId,
                OrderTypeId = orderDto.OrderTypeId,
                OrderName = orderDto.OrderName,
                OrderRefCode = orderDto.OrderRefCode,
                AccountId = orderDto.AccountId,
                CampaignId = orderDto.CampaignId,
                AdvertiserId = orderDto.AdvertiserId,
                AdvertiserSubCategoryId = orderDto.AdvertiserSubCategoryId,
                BudgetTypeId = orderDto.BudgetTypeId,
                CampaignBudget = orderDto.CampaignBudget,
                MonthlyBudget = orderDto.MonthlyBudget,
                MonthCount = orderDto.MonthCount,
                StartDate = orderDto.StartDate,
                StartDateHard = orderDto.StartDateHard,
                EndDate = orderDto.EndDate,
                EndDateHard = orderDto.EndDateHard,
                SpendFlex = orderDto.SpendFlex,
                DeliveryPriorityId = orderDto.DeliveryPriorityId,
                CreatedDatetime = orderDto.CreatedDatetime,
                CreatorId = orderDto.CreatorId,
                CreatedBy = orderDto.CreatedBy,
                LastModifiedBy = orderDto.LastModifiedBy,
                LastModifiedDateTime = orderDto.LastModifiedDateTime
            };
            return order;
        }

        public static OrderXrefBudgetGroup Map(OrderBudgetGroup orderBudgetGroup)
        {
            if (orderBudgetGroup == null)
                return null;

            var orderXrefBudgetGroup = new OrderXrefBudgetGroup()
            {
                OrderXrefBudgetGroupId = orderBudgetGroup.OrderBudgetGroupId,
                OrderId = orderBudgetGroup.OrderId,
                BudgetGroupId = orderBudgetGroup.BudgetGroupId,
                Deleted = orderBudgetGroup.Deleted,
                CreatedBy = orderBudgetGroup.CreatedBy,
                CreatedDatetime = orderBudgetGroup.Created,
                LastModifiedBy = orderBudgetGroup.ModifiedBy,
                LastModifiedDatetime = orderBudgetGroup.Modified
            };
            return orderXrefBudgetGroup;
        }

        public static OrderBudgetGroup Map(OrderXrefBudgetGroup orderXrefBudgetGroup)
        {
            if (orderXrefBudgetGroup == null)
                return null;

            var orderBudgetGroup = new OrderBudgetGroup()
            {
                OrderBudgetGroupId = orderXrefBudgetGroup.OrderXrefBudgetGroupId,
                OrderId = orderXrefBudgetGroup.OrderId,
                BudgetGroupId = orderXrefBudgetGroup.BudgetGroupId,
                Deleted = orderXrefBudgetGroup.Deleted,
                CreatedBy = orderXrefBudgetGroup.CreatedBy,
                Created = orderXrefBudgetGroup.CreatedDatetime,
                ModifiedBy = orderXrefBudgetGroup.LastModifiedBy,
                Modified = orderXrefBudgetGroup.LastModifiedDatetime
            };
            return orderBudgetGroup;
        }

        #endregion
        
        #region User

        public static UserDto Map(User user)
        {
            if (user == null)
                return null;

            var userDto = new UserDto()
            {
                UserId = user.UserId.Value,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                AccountId = user.AccountId.Value
            };
            return userDto;
        }

        public static User Map(UserDto userDto)
        {
            if (userDto == null)
                return null;

            var user = new User()
            {
                UserId = userDto.UserId,
                FirstName = userDto.FirstName,
                LastName = userDto.LastName,
                Email = userDto.Email,
                AccountId = userDto.AccountId
            };
            return user;
        }

        #endregion

        #region Campaign

        public static Campaign Map(CampaignDto campaignDto)
        {
            if (campaignDto == null)
                return null;

            var campaign = new Campaign()
            {
                CampaignId = campaignDto.CampaignId,
                AccountId = campaignDto.AccountId,
                AdvertiserId = campaignDto.AdvertiserId,
                CampaignName = campaignDto.CampaignName,
                CampaignRefCode = campaignDto.CampaignRefCode,
                ParentAccountNames = campaignDto.ParentAccountNames,
                AdWordsCustomerId = campaignDto.AdWordsCustomerId
            };
            return campaign;
        }

        public static CampaignDto Map(Campaign campaign)
        {
            if (campaign == null)
                return null;

            var campaignDto = new CampaignDto()
            {
                CampaignId = campaign.CampaignId,
                AccountId = campaign.AccountId,
                AdvertiserId = campaign.AdvertiserId,
                CampaignName = campaign.CampaignName,
                CampaignRefCode = campaign.CampaignRefCode,
                ParentAccountNames = campaign.ParentAccountNames,
                AdWordsCustomerId = campaign.AdWordsCustomerId
            };
            return campaignDto;
        }

        #endregion

        #region PlacementAssociations

        public static PlacementAssociation Map(PlacementAssociationDto placementAssociationDto)
        {
            if (placementAssociationDto == null)
                return null;

            var placementAssociation = new PlacementAssociation
            {
                AdwordsCampaignId = placementAssociationDto.AdwordsCampaignId,
                BudgetGroupId = placementAssociationDto.BudgetGroupId,
                PlacementValue = placementAssociationDto.PlacementValue,
                CampaignManagerEmail = placementAssociationDto.CampaignManagerEmail
            };

            return placementAssociation;
        }

        public static PlacementAssociationDto Map(PlacementAssociation placementAssociation)
        {
            if (placementAssociation == null)
                return null;

            var placementAssociationDto = new PlacementAssociationDto
            {
                AdwordsCampaignId = placementAssociation.AdwordsCampaignId,
                BudgetGroupId = placementAssociation.BudgetGroupId,
                PlacementValue = placementAssociation.PlacementValue,
                CampaignManagerEmail = placementAssociation.CampaignManagerEmail
            };

            return placementAssociationDto;
        }

        #endregion

        #region CampaignPacingData

        public static CampaignPacingData Map(CampaignPacingDataDto campaignPacingDto)
        {
            if (campaignPacingDto == null)
                return null;
            
            var campaignPacing = new CampaignPacingData
            {
                CampaignName = campaignPacingDto.CampaignName,
                AdWordsCampaignId = campaignPacingDto.AdWordsCampaignId,
                AdWordsCampaignName = campaignPacingDto.AdWordsCampaignName,
                AdWordsCustomerId = campaignPacingDto.AdWordsCustomerId,
                StartDate = campaignPacingDto.StartDate,
                EndDate = campaignPacingDto.EndDate,
                TotalDays = campaignPacingDto.TotalDays,
                DaysSoFar = campaignPacingDto.DaysSoFar,
                DaysLeft = campaignPacingDto.DaysLeft,
                Margin = campaignPacingDto.Margin,
                BudgetAmount = campaignPacingDto.BudgetAmount,
                CostSoFar = campaignPacingDto.CostSoFar,
                RemainingBudget = campaignPacingDto.RemainingBudget,
                ProposedDailyBudget = campaignPacingDto.ProposedDailyBudget,
                PacingRate = campaignPacingDto.PacingRate,
                BudgetGroupTimedBudgetId =  campaignPacingDto.BudgetGroupTimedBudgetId,
                YesterdaysOverPacingRate = campaignPacingDto.YesterdaysOverPacingRate,
                CampaignManagerEmail = campaignPacingDto.CampaignManagerEmail
            };
             
            return campaignPacing;
        }

        public static CampaignPacingDataDto Map(CampaignPacingData campaignPacing)
        {
            if (campaignPacing == null)
                return null;

            var campaignPacingDto = new CampaignPacingDataDto
            {
                CampaignName = campaignPacing.CampaignName,
                AdWordsCampaignId = campaignPacing.AdWordsCampaignId,
                AdWordsCampaignName = campaignPacing.AdWordsCampaignName,
                AdWordsCustomerId = campaignPacing.AdWordsCustomerId,
                StartDate = campaignPacing.StartDate,
                EndDate = campaignPacing.EndDate,
                TotalDays = campaignPacing.TotalDays,
                DaysSoFar = campaignPacing.DaysSoFar,
                DaysLeft = campaignPacing.DaysLeft,
                Margin = campaignPacing.Margin,
                BudgetAmount = campaignPacing.BudgetAmount,
                CostSoFar = campaignPacing.CostSoFar,
                RemainingBudget = campaignPacing.RemainingBudget,
                ProposedDailyBudget = campaignPacing.ProposedDailyBudget,
                PacingRate = campaignPacing.PacingRate,
                BudgetGroupTimedBudgetId =  campaignPacing.BudgetGroupTimedBudgetId,
                YesterdaysOverPacingRate = campaignPacing.YesterdaysOverPacingRate,
                CampaignManagerEmail = campaignPacing.CampaignManagerEmail
            };
            return campaignPacingDto;
        }
        #endregion

        #region CustomerPacingDate

        public static DomainObjects.CustomerPacingDate Map(CustomerPacingDateDto customerPacingDateDto)
        {
            if (customerPacingDateDto == null)
                return null;

            var customerPacingDate = new Sightly.BusinessLayer.DomainObjects.CustomerPacingDate
            {
                CustomerId = customerPacingDateDto.CustomerId,
                PacingDate = customerPacingDateDto.PacingDate
            };

            return customerPacingDate;
        }

        public static CustomerPacingDateDto Map(DomainObjects.CustomerPacingDate customerPacingDate)
        {
            if (customerPacingDate == null)
                return null;

            var customerPacingDateDto = new CustomerPacingDateDto
            {
                CustomerId = customerPacingDate.CustomerId,
                PacingDate = customerPacingDate.PacingDate
            };

            return customerPacingDateDto;
        }

        #endregion
        
        #region DailyStats

        public static DailyStats Map(DailyStatsDto dailyStatsDto)
        {
            if (dailyStatsDto == null)
                return null;

            var dailyStats = new DailyStats
            {
                StatDate = dailyStatsDto.StatDate,
                Impressions = dailyStatsDto.Impressions,
                Clicks = dailyStatsDto.Clicks,
                Views = dailyStatsDto.Views,
                Cost = dailyStatsDto.Cost,
                PacingRate = dailyStatsDto.PacingRate,
                Margin = dailyStatsDto.Margin,
                AwCustomerId = dailyStatsDto.AwCustomerId
            };

            return dailyStats;
        }

        public static DailyStatsDto Map(DailyStats dailyStats)
        {
            if (dailyStats == null)
                return null;

            var dailyStatsDto = new DailyStatsDto
            {
                StatDate = dailyStats.StatDate,
                Impressions = dailyStats.Impressions,
                Clicks = dailyStats.Clicks,
                Views = dailyStats.Views,
                Cost = dailyStats.Cost,
                PacingRate = dailyStats.PacingRate,
                Margin = dailyStats.Margin,
                AwCustomerId = dailyStats.AwCustomerId
            };

            return dailyStatsDto;
        }

        #endregion
        
        #region Accounts

        public static Account Map(AccountDto accountDto)
        {
            if (accountDto == null)
                return null;

            var account = new Account
            {
                AccountId = accountDto.AccountId,
                AccountName = accountDto.AccountName,
                AccountTypeId = accountDto.AccountTypeId,
                AccountMargin = accountDto.AccountMargin,
                ParentId = accountDto.ParentId
            };

            return account;
        }

        public static AccountDto Map(Account account)
        {
            if (account == null)
                return null;

            var accountDto = new AccountDto
            {
                AccountId = account.AccountId,
                AccountName = account.AccountName,
                AccountTypeId = account.AccountTypeId,
                AccountMargin = account.AccountMargin,
                ParentId = account.ParentId
            };

            return accountDto;
        }

        #endregion
        
        #region Advertisers

        public static Advertiser Map(AdvertiserDto advertiserDto)
        {
            if (advertiserDto == null)
                return null;

            var advertiser = new Advertiser
            {
                AdvertiserId = advertiserDto.AdvertiserId,
                AccountId = advertiserDto.AccountId,
                AdvertiserName = advertiserDto.AdvertiserName,
                AdvertiserRefCode = advertiserDto.AdvertiserRefCode,
                AdvertiserCategoryId = advertiserDto.AdvertiserCategoryId,
                AdvertiserMargin = advertiserDto.AdvertiserMargin
            };

            return advertiser;
        }

        public static AdvertiserDto Map(Advertiser advertiser)
        {
            if (advertiser == null)
                return null;

            var advertiserDto = new AdvertiserDto
            {
                AdvertiserId = advertiser.AdvertiserId,
                AccountId = advertiser.AccountId,
                AdvertiserName = advertiser.AdvertiserName,
                AdvertiserRefCode = advertiser.AdvertiserRefCode,
                AdvertiserCategoryId = advertiser.AdvertiserCategoryId,
                AdvertiserMargin = advertiser.AdvertiserMargin
            };

            return advertiserDto;
        }

        #endregion

        #region KpiData

        public static KpiDefinitionData Map(KpiDefinitionDataDto kpiDefinitionDataDto)
        {
            if (kpiDefinitionDataDto == null)
                return null;

            var kpiDefinitionData = new KpiDefinitionData
            {
                AdwordsCustomerId = kpiDefinitionDataDto.AdwordsCustomerId,
                ConditionDisplay = kpiDefinitionDataDto.ConditionDisplay,
                EquationValue = kpiDefinitionDataDto.EquationValue,
                KpiMetricAbbreviation = kpiDefinitionDataDto.KpiMetricAbbreviation,
                KpiMetricName = kpiDefinitionDataDto.KpiMetricName,
                KpiSourceName = kpiDefinitionDataDto.KpiSourceName,
                KpiUnitTypeName = kpiDefinitionDataDto.KpiUnitTypeName,
                OutcomeTypeName = kpiDefinitionDataDto.OutcomeTypeName,
                Sequence = kpiDefinitionDataDto.Sequence,
                Email = kpiDefinitionDataDto.Email
            };
            return kpiDefinitionData;
        }

        public static KpiDefinitionDataDto Map(KpiDefinitionData kpiDefinitionData)
        {
            if (kpiDefinitionData == null)
                return null;

            var kpiDefinitionDataDto = new KpiDefinitionDataDto
            {
                AdwordsCustomerId = kpiDefinitionData.AdwordsCustomerId,
                ConditionDisplay = kpiDefinitionData.ConditionDisplay,
                EquationValue = kpiDefinitionData.EquationValue,
                KpiMetricAbbreviation = kpiDefinitionData.KpiMetricAbbreviation,
                KpiMetricName = kpiDefinitionData.KpiMetricName,
                KpiSourceName = kpiDefinitionData.KpiSourceName,
                KpiUnitTypeName = kpiDefinitionData.KpiUnitTypeName,
                OutcomeTypeName = kpiDefinitionData.OutcomeTypeName,
                Sequence = kpiDefinitionData.Sequence,
                Email = kpiDefinitionData.Email
            };
            return kpiDefinitionDataDto;
        }

        public static KpiCondition Map(KpiConditionDto kpiConditionDto)
        {
            if (kpiConditionDto == null)
                return null;
            var kpiCondition = new KpiCondition
            {
                EquationTypeId = kpiConditionDto.EquationTypeId,
                ConditionDisplay = kpiConditionDto.ConditionDisplay,
                ConditionName = kpiConditionDto.ConditionName
            };
            return kpiCondition;
        }

        public static KpiConditionDto Map(KpiCondition kpiCondition)
        {
            if (kpiCondition == null)
                return null;
            var kpiConditionDto = new KpiConditionDto
            {
                EquationTypeId = kpiCondition.EquationTypeId,
                ConditionDisplay = kpiCondition.ConditionDisplay,
                ConditionName = kpiCondition.ConditionName
            };
            return kpiConditionDto;
        }

        public static KpiMetric Map(KpiMetricDto kpiMetricDto)
        {
            if (kpiMetricDto == null)
                return null;
            var kpiMetric = new KpiMetric
            {
                KpiMetricId = kpiMetricDto.KpiMetricId,
                KpiAbbreviation = kpiMetricDto.KpiAbbreviation,
                KpiMetricName = kpiMetricDto.KpiMetricName
            };
            return kpiMetric;
        }

        public static KpiMetricDto Map(KpiMetric kpiMetric)
        {

            if (kpiMetric == null)
                return null;
            var kpiMetricDto = new KpiMetricDto
            {
                KpiMetricId = kpiMetric.KpiMetricId,
                KpiAbbreviation = kpiMetric.KpiAbbreviation,
                KpiMetricName = kpiMetric.KpiMetricName
            };
            return kpiMetricDto;
        }

        public static KpiEditorData Map(KpiEditorDataDto kpiEditorDataDto)
        {
            if (kpiEditorDataDto == null)
                return null;
            var kpiEditorData = new KpiEditorData
            {
                AdwordsCustomerId = kpiEditorDataDto.AdwordsCustomerId,
                AdwordsCustomerKpiId = kpiEditorDataDto.AdwordsCustomerKpiId,
                ConditionDisplay = kpiEditorDataDto.ConditionDisplay,
                KpiMetricName = kpiEditorDataDto.KpiMetricName,
                KpiAbbreviation = kpiEditorDataDto.KpiAbbreviation,
                EquationValue = kpiEditorDataDto.EquationValue,
                Sequence = kpiEditorDataDto.Sequence,
                OutcomeTypeName = kpiEditorDataDto.OutcomeTypeName
            };
            return kpiEditorData;
        }

        public static KpiEditorDataDto Map(KpiEditorData kpiEditorData)
        {
            if (kpiEditorData == null)
                return null;
            var kpiEditorDataDto = new KpiEditorDataDto
            {
                AdwordsCustomerId = kpiEditorData.AdwordsCustomerId,
                AdwordsCustomerKpiId = kpiEditorData.AdwordsCustomerKpiId,
                ConditionDisplay = kpiEditorData.ConditionDisplay,
                KpiMetricName = kpiEditorData.KpiMetricName,
                KpiAbbreviation = kpiEditorData.KpiAbbreviation,
                EquationValue = kpiEditorData.EquationValue,
                Sequence = kpiEditorData.Sequence,
                OutcomeTypeName = kpiEditorData.OutcomeTypeName
            };
            return kpiEditorDataDto;
        }

        #endregion

        #region AdwordCustomer

        public static AdwordCustomer Map(AdwordCustomerDto adwordCustomerDto)
        {
            if (adwordCustomerDto == null)
                return null;

            var adwordsCustomer = new AdwordCustomer
            {
                CustomerId = adwordCustomerDto.CustomerId,
                CustomerName = adwordCustomerDto.CustomerName,
                StartDate = adwordCustomerDto.StartDate,
                EndDate = adwordCustomerDto.EndDate,
                TotalDays = adwordCustomerDto.TotalDays,
                DaysSoFar = adwordCustomerDto.DaysSoFar,
                PercentComplete = adwordCustomerDto.PercentComplete,
                DaysLeft = adwordCustomerDto.DaysLeft
                
            };

            return adwordsCustomer;
        }
        public static AdwordCustomerDto Map(AdwordCustomer adwordCustomer)
        {
            if (adwordCustomer == null)
                return null;

            var adwordsCustomerDto = new AdwordCustomerDto
            {
                CustomerId = adwordCustomer.CustomerId,
                CustomerName = adwordCustomer.CustomerName,
                StartDate = adwordCustomer.StartDate,
                EndDate = adwordCustomer.EndDate,
                TotalDays = adwordCustomer.TotalDays,
                DaysSoFar = adwordCustomer.DaysSoFar,
                PercentComplete = adwordCustomer.PercentComplete,
                DaysLeft = adwordCustomer.DaysLeft
            };

            return adwordsCustomerDto;
        }

        #endregion
        
        #region AdwordsKpiStatsData

        public static AdwordsKpiStatsData Map(AdwordsKpiStatsDataDto adwordsKpiStatsDataDto)
        {
            if (adwordsKpiStatsDataDto == null)
                return null;

            var adwordsKpiStatsData = new AdwordsKpiStatsData
            {
                CustomerId = adwordsKpiStatsDataDto.CustomerId,
                CPC = adwordsKpiStatsDataDto.CPC,
                CPM = adwordsKpiStatsDataDto.CPM,
                CTR = adwordsKpiStatsDataDto.CTR,
                CPV = adwordsKpiStatsDataDto.CPV,
                Clicks = adwordsKpiStatsDataDto.Clicks,
                GS = adwordsKpiStatsDataDto.GS,
                Impressions = adwordsKpiStatsDataDto.Impressions,
                VR = adwordsKpiStatsDataDto.VR,
                Views = adwordsKpiStatsDataDto.Views
            };
            return adwordsKpiStatsData;
        }

        public static AdwordsKpiStatsDataDto Map(AdwordsKpiStatsData adwordsKpiStatsData)
        {
            if (adwordsKpiStatsData == null)
                return null;

            var adwordsKpiStatsDataDto = new AdwordsKpiStatsDataDto
            {
                CustomerId = adwordsKpiStatsData.CustomerId,
                CPC = adwordsKpiStatsData.CPC,
                CPM = adwordsKpiStatsData.CPM,
                CTR = adwordsKpiStatsData.CTR,
                CPV = adwordsKpiStatsData.CPV,
                Clicks = adwordsKpiStatsData.Clicks,
                GS = adwordsKpiStatsData.GS,
                Impressions = adwordsKpiStatsData.Impressions,
                VR = adwordsKpiStatsData.VR,
                Views = adwordsKpiStatsData.Views
            };
            return adwordsKpiStatsDataDto;
        }
        #endregion

        #region MoatKpiStatsData

        public static MoatKpiStatsData Map(MoatKpiStatsDataDto moatKpiStatsDataDto)
        {
            if (moatKpiStatsDataDto == null)
                return null;

            var moatKpiStatsData = new MoatKpiStatsData
            {
                AdvertiserId = moatKpiStatsDataDto.AdvertiserId,
                CustomerQuality = moatKpiStatsDataDto.CustomerQuality,
                HumanAndAvocRate = moatKpiStatsDataDto.HumanAndAvocRate,
                HumanRate = moatKpiStatsDataDto.HumanRate,
                Two_Sec_In_View_Rate_Unfiltered = moatKpiStatsDataDto.Two_Sec_In_View_Rate_Unfiltered,
                StatDate = moatKpiStatsDataDto.StatDate,
                AwCustomerId = moatKpiStatsDataDto.AwCustomerId
            };
            return moatKpiStatsData;
        }

        public static MoatKpiStatsDataDto Map(MoatKpiStatsData moatKpiStatsData)
        {
            if (moatKpiStatsData == null)
                return null;

            var moatKpiStatsDataDto = new MoatKpiStatsDataDto
            {
                AdvertiserId = moatKpiStatsData.AdvertiserId,
                CustomerQuality = moatKpiStatsData.CustomerQuality,
                HumanAndAvocRate = moatKpiStatsData.HumanAndAvocRate,
                HumanRate = moatKpiStatsData.HumanRate,
                Two_Sec_In_View_Rate_Unfiltered = moatKpiStatsData.Two_Sec_In_View_Rate_Unfiltered,
                StatDate = moatKpiStatsData.StatDate,
                AwCustomerId= moatKpiStatsData.AwCustomerId
            };
            return moatKpiStatsDataDto;
        }


        #endregion

        #region NielsenKpiStatsData

        public static NielsenKpiStatsData Map(NielsenKpiStatsDataDto nielsenKpiStatsDataDto)
        {
            if (nielsenKpiStatsDataDto == null)
                return null;

            var nielsenKpiStatsData = new NielsenKpiStatsData
            {
                CustomerId = nielsenKpiStatsDataDto.CustomerId,
                DarComputer = nielsenKpiStatsDataDto.DarComputer,
                DarMobile = nielsenKpiStatsDataDto.DarMobile,
                DarTotalDigital = nielsenKpiStatsDataDto.DarTotalDigital,
                StatDate = nielsenKpiStatsDataDto.StatDate,
                AwCustomerId = nielsenKpiStatsDataDto.AwCustomerId
            };
            return nielsenKpiStatsData;
        }
        public static NielsenKpiStatsDataDto Map(NielsenKpiStatsData nielsenKpiStatsData)
        {
            if (nielsenKpiStatsData == null)
                return null;

            var nielsenKpiStatsDataDto = new NielsenKpiStatsDataDto
            {
                CustomerId = nielsenKpiStatsData.CustomerId,
                DarComputer = nielsenKpiStatsData.DarComputer,
                DarMobile = nielsenKpiStatsData.DarMobile,
                DarTotalDigital = nielsenKpiStatsData.DarTotalDigital,
                StatDate = nielsenKpiStatsData.StatDate,
                AwCustomerId = nielsenKpiStatsData.AwCustomerId

            };
            return nielsenKpiStatsDataDto;
        }

        #endregion

        #region DoubleVerifyKpiStatsData

        public static DoubleVerifyKpiStatsData Map(DoubleVerifyKpiStatsDataDto doubleVerifyKpiStatsDataDto)
        {
            if (doubleVerifyKpiStatsDataDto == null)
                return null;

            var doubleVerifyKpiStatsData = new DoubleVerifyKpiStatsData
            {
                CustomerId = doubleVerifyKpiStatsDataDto.CustomerId,
                BrandSafetyOnTargetPercent_1x1 = doubleVerifyKpiStatsDataDto.BrandSafetyOnTargetPercent_1x1,
                FraudSivtRate = doubleVerifyKpiStatsDataDto.FraudSivtRate,
                FraudSivtRate_1x1 = doubleVerifyKpiStatsDataDto.FraudSivtRate_1x1,
                InGeoRate_1x1 = doubleVerifyKpiStatsDataDto.InGeoRate_1x1,
                VideoViewableRate = doubleVerifyKpiStatsDataDto.VideoViewableRate,
                StatDate = doubleVerifyKpiStatsDataDto.StatDate,
                AwCustomerId = doubleVerifyKpiStatsDataDto.AwCustomerId
            };
            return doubleVerifyKpiStatsData;
        }
        public static DoubleVerifyKpiStatsDataDto Map (DoubleVerifyKpiStatsData doubleVerifyKpiStatsData)
        {
            if (doubleVerifyKpiStatsData == null)
                return null;

            var doubleVerifyKpiStatsDataDto = new DoubleVerifyKpiStatsDataDto
            {
                CustomerId = doubleVerifyKpiStatsData.CustomerId,
                BrandSafetyOnTargetPercent_1x1 = doubleVerifyKpiStatsData.BrandSafetyOnTargetPercent_1x1,
                FraudSivtRate = doubleVerifyKpiStatsData.FraudSivtRate,
                FraudSivtRate_1x1 = doubleVerifyKpiStatsData.FraudSivtRate_1x1,
                InGeoRate_1x1 = doubleVerifyKpiStatsData.InGeoRate_1x1,
                VideoViewableRate = doubleVerifyKpiStatsData.VideoViewableRate,
                StatDate = doubleVerifyKpiStatsData.StatDate,
                AwCustomerId = doubleVerifyKpiStatsData.AwCustomerId
            };
            return doubleVerifyKpiStatsDataDto;
        }



        #endregion
        
        #region OrderEmailStuffs
        public static OrderEmailData Map(OrderEmailDto orderEmailDto)
        {
            if (orderEmailDto == null)
                return null;
            var orderEmail = new OrderEmailData
            {
                AccountName = orderEmailDto.AccountName,
                AdvertiserName = orderEmailDto.AdvertiserName,
                CampaignName = orderEmailDto.CampaignName,
                CreaterName = orderEmailDto.CreaterName,
                OrderName = orderEmailDto.OrderName,
                OrderRefCode = orderEmailDto.OrderRefCode,
                StartDate = orderEmailDto.StartDate,
                EndDate = orderEmailDto.EndDate,
                TotalBudget = orderEmailDto.TotalBudget,
                Notes = orderEmailDto.Notes,
                CampaignManagerEmail =  orderEmailDto.CampaignManagerEmail
            };

            return orderEmail;
        }

        public static OrderEmailDto Map(OrderEmailData orderEmail)
        {
            if (orderEmail == null)
                return null;

            var orderEmailDto = new OrderEmailDto
            {
                AccountName =  orderEmail.AccountName,
                AdvertiserName = orderEmail.AdvertiserName,
                CampaignName = orderEmail.CampaignName,
                CreaterName = orderEmail.CreaterName,
                OrderName = orderEmail.OrderName,
                OrderRefCode = orderEmail.OrderRefCode,
                StartDate = orderEmail.StartDate,
                EndDate = orderEmail.EndDate,
                TotalBudget = orderEmail.TotalBudget,
                Notes = orderEmail.Notes,
                CampaignManagerEmail = orderEmail.CampaignManagerEmail
            };

            return orderEmailDto;
        }


        public static OrderChangesDto Map(OrderChanges orderChanges)
        {
            if (orderChanges == null)
                return null;
            var orderChangesDto = new OrderChangesDto
            {
                ChangeDetails = orderChanges.ChangeDetails,
                ChangeType = orderChanges.ChangeType
            };

            return orderChangesDto;
        }

        public static OrderChanges Map(OrderChangesDto orderChangesDto)
        {
            if (orderChangesDto == null)
                return null;
            var orderChanges = new OrderChanges
            {
                ChangeDetails = orderChangesDto.ChangeDetails,
                ChangeType = orderChangesDto.ChangeType
            };

            return orderChanges;
        }
        #endregion

        #region Manager
        public static ManagerDto Map(Manager manager)
        {
            if (manager == null)
                return null;
            var managerDto = new ManagerDto
            {
                UserId = manager.UserId,
                FirstName = manager.FirstName,
                LastName = manager.LastName,
                Email = manager.Email
            };

            return managerDto;
        }

        public static Manager Map(ManagerDto managerDto)
        {
            if (managerDto == null)
                return null;
            var manager = new Manager
            {
                UserId = managerDto.UserId,
                FirstName = managerDto.FirstName,
                LastName = managerDto.LastName,
                Email = managerDto.Email
            };

            return manager;
        }
        #endregion

        public static OrderBasic Map(OrderBasicDto orderBasicDto)
        {
            if (orderBasicDto == null)
                return null;

            var orderBasic = new OrderBasic
            {
                AccountId = orderBasicDto.AccountId,
                AdvertiserId = orderBasicDto.AdvertiserId,
                CampaignId = orderBasicDto.CampaignId,
                CreatedBy = orderBasicDto.CreatedBy,
                CreatedDatetime = orderBasicDto.CreatedDatetime,
                CreatorId = orderBasicDto.CreatorId,
                Deleted = orderBasicDto.Deleted,
                LastModifiedBy = orderBasicDto.LastModifiedBy,
                OrderId = orderBasicDto.OrderId,
                OrderName = orderBasicDto.OrderName,
                OrderRefCode = orderBasicDto.OrderRefCode,
                OrderStatusId = orderBasicDto.OrderStatusId,
                OrderTypeId = orderBasicDto.OrderTypeId,
                SpendFlex = orderBasicDto.SpendFlex
            };
            return orderBasic;
        }

        public static OrderBasicDto Map(OrderBasic orderBasic)
        {
            if (orderBasic == null)
                return null;

            var orderBasicDto = new OrderBasicDto
            {
                AccountId = orderBasic.AccountId,
                AdvertiserId = orderBasic.AdvertiserId,
                CampaignId = orderBasic.CampaignId,
                CreatedBy = orderBasic.CreatedBy,
                CreatedDatetime = orderBasic.CreatedDatetime,
                CreatorId = orderBasic.CreatorId,
                Deleted = orderBasic.Deleted,
                LastModifiedBy = orderBasic.LastModifiedBy,
                OrderId = orderBasic.OrderId,
                OrderName = orderBasic.OrderName,
                OrderRefCode = orderBasic.OrderRefCode,
                OrderStatusId = orderBasic.OrderStatusId,
                OrderTypeId = orderBasic.OrderTypeId,
                SpendFlex = orderBasic.SpendFlex
            };
            return orderBasicDto;
        }

        public static CampaignAbbreviated Map(CampaignAbbreviatedDto campAbbreviatedDto)
        {
            if (campAbbreviatedDto == null)
                return null;

            var campAbbreviated = new CampaignAbbreviated
            {
                AccountId = campAbbreviatedDto.AccountId,
                AdvertiserId = campAbbreviatedDto.AdvertiserId,
                CampaignId = campAbbreviatedDto.CampaignId,
                CampaignName = campAbbreviatedDto.CampaignName,
                CampaignRefCode = campAbbreviatedDto.CampaignRefCode
            };
            return campAbbreviated;
        }

        public static CampaignAbbreviatedDto Map(CampaignAbbreviated campAbbreviated)
        {
            if (campAbbreviated == null)
                return null;

            var campAbbreviatedDto = new CampaignAbbreviatedDto
            {
                AccountId = campAbbreviated.AccountId,
                AdvertiserId = campAbbreviated.AdvertiserId,
                CampaignId = campAbbreviated.CampaignId,
                CampaignName = campAbbreviated.CampaignName,
                CampaignRefCode = campAbbreviated.CampaignRefCode
            };
            return campAbbreviatedDto;
        }

        public static OrderTargetAgeGroup Map(OrderTargetAgeGroupDto orderTargetAgeGroupDto)
        {
            if (orderTargetAgeGroupDto == null)
            {
                return null;
            }

            var orderTargetAgeGroup = new OrderTargetAgeGroup()
            {
                OrderId = orderTargetAgeGroupDto.OrderId,
                AgeGroupId = orderTargetAgeGroupDto.AgeGroupId,
                AgeGroupName = orderTargetAgeGroupDto.AgeGroupName,
                Deleted = orderTargetAgeGroupDto.Deleted,
                CreatedBy = orderTargetAgeGroupDto.CreatedBy,
                Created = orderTargetAgeGroupDto.CreatedDatetime,
                LastModifiedBy = orderTargetAgeGroupDto.LastModifiedBy,
                LastModified = orderTargetAgeGroupDto.LastModifiedDatetime
            };
            return orderTargetAgeGroup;
        }

        public static OrderTargetAgeGroupDto Map(OrderTargetAgeGroup orderTargetAgeGroup)
        {
            if (orderTargetAgeGroup == null)
            {
                return null;
            }

            var orderTargetAgeGroupDto = new OrderTargetAgeGroupDto()
            {
                OrderId = orderTargetAgeGroup.OrderId,
                AgeGroupId = orderTargetAgeGroup.AgeGroupId,
                AgeGroupName = orderTargetAgeGroup.AgeGroupName,
                Deleted = orderTargetAgeGroup.Deleted,
                CreatedBy = orderTargetAgeGroup.CreatedBy,
                CreatedDatetime = orderTargetAgeGroup.Created,
                LastModifiedBy = orderTargetAgeGroup.LastModifiedBy,
                LastModifiedDatetime = orderTargetAgeGroup.LastModified
            };
            return orderTargetAgeGroupDto;
        }

        public static OrderTargetGenderGroup Map(OrderTargetGenderGroupDto orderTargetGenderGroupDto)
        {
            if (orderTargetGenderGroupDto == null)
            {
                return null;
            }

            var orderTargetGenderGroup = new OrderTargetGenderGroup()
            {
                OrderId = orderTargetGenderGroupDto.OrderId,
                GenderId = orderTargetGenderGroupDto.GenderId,
                GenderName = orderTargetGenderGroupDto.GenderName,
                Deleted = orderTargetGenderGroupDto.Deleted,
                CreatedBy = orderTargetGenderGroupDto.CreatedBy,
                Created = orderTargetGenderGroupDto.CreatedDatetime,
                LastModifiedBy = orderTargetGenderGroupDto.LastModifiedBy,
                LastModified = orderTargetGenderGroupDto.LastModifiedDatetime
            };
            return orderTargetGenderGroup;
        }

        public static OrderTargetGenderGroupDto Map(OrderTargetGenderGroup orderTargetGenderGroup)
        {
            if (orderTargetGenderGroup == null)
            {
                return null;
            }

            var orderTargetGenderGroupDto = new OrderTargetGenderGroupDto()
            {
                OrderId = orderTargetGenderGroup.OrderId,
                GenderId = orderTargetGenderGroup.GenderId,
                GenderName = orderTargetGenderGroup.GenderName,
                Deleted = orderTargetGenderGroup.Deleted,
                CreatedBy = orderTargetGenderGroup.CreatedBy,
                CreatedDatetime = orderTargetGenderGroup.Created,
                LastModifiedBy = orderTargetGenderGroup.LastModifiedBy,
                LastModifiedDatetime = orderTargetGenderGroup.LastModified
            };
            return orderTargetGenderGroupDto;
        }

        public static OrderParentalStatusGroup Map(OrderParentalStatusGroupDto orderParentalStatusGroupDto)
        {
            if (orderParentalStatusGroupDto == null)
            {
                return null;
            }

            var orderParentalStatusGroup = new OrderParentalStatusGroup()
            {
                OrderId = orderParentalStatusGroupDto.OrderId,
                ParentalStatusId = orderParentalStatusGroupDto.ParentalStatusId,
                ParentalStatusName = orderParentalStatusGroupDto.ParentalStatusName,
                Deleted = orderParentalStatusGroupDto.Deleted,
                CreatedBy = orderParentalStatusGroupDto.CreatedBy,
                Created = orderParentalStatusGroupDto.CreatedDatetime,
                LastModifiedBy = orderParentalStatusGroupDto.LastModifiedBy,
                LastModified = orderParentalStatusGroupDto.LastModifiedDatetime
            };
            return orderParentalStatusGroup;
        }

        public static OrderParentalStatusGroupDto Map(OrderParentalStatusGroup orderParentalStatusGroup)
        {
            if (orderParentalStatusGroup == null)
            {
                return null;
            }

            var orderParentalStatusGroupDto = new OrderParentalStatusGroupDto()
            {
                OrderId = orderParentalStatusGroup.OrderId,
                ParentalStatusId = orderParentalStatusGroup.ParentalStatusId,
                ParentalStatusName = orderParentalStatusGroup.ParentalStatusName,
                Deleted = orderParentalStatusGroup.Deleted,
                CreatedBy = orderParentalStatusGroup.CreatedBy,
                CreatedDatetime = orderParentalStatusGroup.Created,
                LastModifiedBy = orderParentalStatusGroup.LastModifiedBy,
                LastModifiedDatetime = orderParentalStatusGroup.LastModified
            };
            return orderParentalStatusGroupDto;
        }

        public static OrderHouseholdIncomeGroup Map(OrderHouseholdIncomeGroupDto orderHouseholdIncomeGroupDto)
        {
            if (orderHouseholdIncomeGroupDto == null)
            {
                return null;
            }

            var orderHouseholdIncomeGroup = new OrderHouseholdIncomeGroup()
            {
                OrderId = orderHouseholdIncomeGroupDto.OrderId,
                HouseholdIncomeId = orderHouseholdIncomeGroupDto.HouseholdIncomeGroupId,
                HouseholdIncomeName = orderHouseholdIncomeGroupDto.GroupName,
                Deleted = orderHouseholdIncomeGroupDto.Deleted,
                CreatedBy = orderHouseholdIncomeGroupDto.CreatedBy,
                Created = orderHouseholdIncomeGroupDto.CreatedDatetime,
            };
            return orderHouseholdIncomeGroup;
        }

        public static OrderHouseholdIncomeGroupDto Map(OrderHouseholdIncomeGroup orderHouseholdIncomeGroup)
        {
            if (orderHouseholdIncomeGroup == null)
            {
                return null;
            }

            var orderHouseholdIncomeGroupDto = new OrderHouseholdIncomeGroupDto()
            {
                OrderId = orderHouseholdIncomeGroup.OrderId,
                HouseholdIncomeGroupId = orderHouseholdIncomeGroup.HouseholdIncomeId,
                GroupName = orderHouseholdIncomeGroup.HouseholdIncomeName,
                Deleted = orderHouseholdIncomeGroup.Deleted,
                CreatedBy = orderHouseholdIncomeGroup.CreatedBy,
                CreatedDatetime = orderHouseholdIncomeGroup.Created,
            };
            return orderHouseholdIncomeGroupDto;
        }
    }
}
