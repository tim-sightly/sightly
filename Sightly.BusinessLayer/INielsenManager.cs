using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;
using NielsenKpiStatsData = Sightly.BusinessLayer.DomainObjects.NielsenKpiStatsData;

namespace Sightly.BusinessLayer
{
    public interface INielsenManager
    {
        List<NielsenKpiStatsData> GetNielsenKpiStatsDataByAccount(Guid accountId);
        List<NielsenKpiStatsData> GetNielsenKpiStatsDataByAdvertiser(Guid advertiserId);
        List<NielsenKpiStatsData> GetNielsenKpiStatsDataByCampaign(Guid campaignId);
    }
}