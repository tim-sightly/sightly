﻿using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessLayer
{
    public interface IKpiManager
    {
        List<KpiCondition> GetKpiConditions();
        List<KpiMetric> GetKpiMetrics();

        List<KpiEditorData> GetKpiEditorDataByCustomerId(long awCustomerId);

        int SaveKpiEditorData(
            long adwordsCustomerId,
            int kpiMetricId,
            int equationTypeId,
            string equationValue,
            int sequence);

        void RemoveKpiEditorData(int adwordsCustomerKpiId);
    }
}