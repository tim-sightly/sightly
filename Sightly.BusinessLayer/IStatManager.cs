﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessLayer
{
    public interface IStatManager
    {
        List<DailyStats> GetDailyDeviceStatsForAdWordsCustomerId(long adWordsCustomerId);
        List<DailyStats> GetDailyDeviceStatsForCampaign(Guid campaignId);
        List<DailyStats> GetDailyDeviceStatsForAccount(Guid accountId);
        List<DailyStats> GetDailyDeviceStatsForAdvertiser(Guid advertiserId);
    }
}