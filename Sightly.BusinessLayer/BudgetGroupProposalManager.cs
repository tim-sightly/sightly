﻿using System;
using Sightly.BusinessLayer.Mapping;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;

using BudgetGroupDto = Sightly.DAL.DTO.BudgetGroupStates.BudgetGroup;
using BudgetGroupProposalDto = Sightly.DAL.DTO.BudgetGroupStates.BudgetGroupProposal;
using BudgetGroupHistoryDto = Sightly.DAL.DTO.BudgetGroupStates.BudgetGroupHistory;
using BudgetGroupLedgerDto = Sightly.DAL.DTO.BudgetGroupLedger;
using OrderDto = Sightly.DAL.DTO.Order;

using Sightly.BusinessLayer.Exceptions;
using Sightly.BusinessLayer.Common;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupAdStates;
using System.Linq;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupLocationStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupConversionActionStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupPlacementStates;
using Sightly.DAL;
using Sightly.DAL.DTO;
using Sightly.Logging;
using BudgetGroupLedger = Sightly.BusinessLayer.DomainObjects.BudgetGroupLedger;
using Order = Sightly.BusinessLayer.DomainObjects.Order;

namespace Sightly.BusinessLayer
{
    public class BudgetGroupProposalManager : IBudgetGroupProposalManager
    {
        private readonly IBudgetGroupProposalStore _budgetGroupProposalStore;
        private readonly IBudgetGroupLedgerStore _budgetGroupLedgerStore;
        private readonly ILoggingWrapper _loggingWrapper;
        public BudgetGroupProposalManager(IBudgetGroupProposalStore budgetGroupProposalStore, IBudgetGroupLedgerStore budgetGroupLedgerStore, ILoggingWrapper loggingWrapper)
        {
            _budgetGroupProposalStore = budgetGroupProposalStore;
            _budgetGroupLedgerStore = budgetGroupLedgerStore;
            _loggingWrapper = loggingWrapper;
        }

        public BudgetGroupProposal Create(BudgetGroupProposal budgetGroupProposal, Order order, Guid createdBy)
        {
            //check business rules and throw error if a violation is found

            //CheckRulesFor(budgetGroupProposal, createdBy);

            //DateTime now = DateTime.UtcNow;
            //budgetGroupProposal.BudgetGroupProposalId = budgetGroupProposal.BudgetGroupProposalId ?? Guid.NewGuid();
            //budgetGroupProposal.BudgetGroupStatusId = 1;
            //budgetGroupProposal.CreatedBy = createdBy;
            //budgetGroupProposal.Created = now;

            ////if (budgetGroupProposal.BudgetGroupAdProposals != null)
            //budgetGroupProposal.BudgetGroupAdProposals?.ForEach(a =>
            //{
            //    a.BudgetGroupAdProposalId = a.BudgetGroupAdProposalId ?? Guid.NewGuid();
            //    a.BudgetGroupProposalId = budgetGroupProposal.BudgetGroupProposalId;
            //    a.CreatedBy = createdBy;
            //    a.Created = now;
            //});

            ////if (budgetGroupProposal.BudgetGroupLocationProposals != null)
            //budgetGroupProposal.BudgetGroupLocationProposals?.ForEach(l => {
            //    l.BudgetGroupLocationProposalId = l.BudgetGroupLocationProposalId ?? Guid.NewGuid();
            //    l.BudgetGroupProposalId = budgetGroupProposal.BudgetGroupProposalId;
            //    l.CreatedBy = createdBy;
            //    l.Created = now;
            //});

            //budgetGroupProposal.BudgetGroupTimedBudgetProposal.BudgetGroupTimedBudgetProposalId = budgetGroupProposal.BudgetGroupTimedBudgetProposal.BudgetGroupTimedBudgetProposalId ?? Guid.NewGuid();
            //budgetGroupProposal.BudgetGroupTimedBudgetProposal.BudgetGroupProposalId = budgetGroupProposal.BudgetGroupProposalId;
            //budgetGroupProposal.BudgetGroupTimedBudgetProposal.CreatedBy = createdBy;
            //budgetGroupProposal.BudgetGroupTimedBudgetProposal.Created = now;           

            ////if (budgetGroupProposal.BudgetGroupPlacementProposals != null)
            //budgetGroupProposal.BudgetGroupPlacementProposals?.ForEach(p => {
            //    p.BudgetGroupPlacementProposalId = p.BudgetGroupPlacementProposalId ?? Guid.NewGuid();
            //    p.BudgetGroupProposalId = budgetGroupProposal.BudgetGroupProposalId;
            //    p.AssignedBy = createdBy;
            //    p.AssignedDate = now;
            //});

            ////if (budgetGroupProposal.BudgetGroupConversionActionProposals != null)
            //budgetGroupProposal.BudgetGroupConversionActionProposals?.ForEach(ca => {
            //    ca.BudgetGroupConversionActionProposalId = ca.BudgetGroupConversionActionProposalId ?? Guid.NewGuid();
            //    ca.BudgetGroupProposalId = budgetGroupProposal.BudgetGroupProposalId;
            //    ca.CreatedBy = createdBy;
            //    ca.Created = now;
            //});
            
            //BudgetGroup budgetGroup = new BudgetGroup()
            //{
            //    BudgetGroupId = Guid.NewGuid(),
            //    BudgetGroupStatusId = (int)BudgetGroupStatus.Draft,
            //    BudgetGroupName = budgetGroupProposal.BudgetGroupName,
            //    BudgetGroupBudget = budgetGroupProposal.BudgetGroupBudget,
            //    Deleted = false,
            //    Created = budgetGroupProposal.Created,
            //    CreatedBy = createdBy
            //};

            //OrderBudgetGroup orderBudgetGroup = new OrderBudgetGroup()
            //{
            //    OrderBudgetGroupId = Guid.NewGuid(),
            //    OrderId = order.OrderId,
            //    BudgetGroupId = budgetGroup.BudgetGroupId.Value,
            //    Deleted = false,
            //    CreatedBy = createdBy.ToString(),
            //    Created = now,
            //    Modified = now,
            //    ModifiedBy = createdBy.ToString(),
            //};

            //BudgetGroupLedger budgetGroupLedger = new BudgetGroupLedger()
            //{
            //    BudgetGroupProposalId = budgetGroupProposal.BudgetGroupProposalId,
            //    BudgetGroupId = budgetGroup.BudgetGroupId,
            //    BudgetGroupEventId = BudgetGroupEvent.ProposalCreated,
            //    Notes = null,
            //    CreatedBy = createdBy,
            //    Created = now
            //};        

            //OrderDto orderDto = Mapper.Map(order);
            //OrderXrefBudgetGroup orderXrefBudgetGroup = Mapper.Map(orderBudgetGroup);
            //BudgetGroupProposalDto budgetGroupProposalDto = Mapper.Map(budgetGroupProposal);
            //BudgetGroupDto budgetGroupDto = Mapper.Map(budgetGroup);
            //BudgetGroupLedgerDto budgetGroupLedgerDto = Mapper.Map(budgetGroupLedger);

            //BudgetGroupProposalDto newBudgetGroupProposalDto = _budgetGroupProposalStore.CreateBudgetGroupProposal(budgetGroupProposalDto, budgetGroupDto, orderDto, orderXrefBudgetGroup, budgetGroupLedgerDto);
            //BudgetGroupProposal newBudgetGroupProposal = Mapper.Map(newBudgetGroupProposalDto);
            //return newBudgetGroupProposal;
            return null;
        }

        public BudgetGroupProposal UpdateStatus(Guid budgetGroupProposalId, BudgetGroupStatus newStatus, Guid user)
        {
            //BudgetGroupProposal budgetGroupProposal = Mapper.Map(_budgetGroupProposalStore.GetBudgetGroupProposalById(budgetGroupProposalId));

            //if (budgetGroupProposal == null)
            //    throw new EntityNotFoundException();

            //BudgetGroupStatus currentStatus = (BudgetGroupStatus)budgetGroupProposal.BudgetGroupStatusId;

            //if (ProposalStatusTransition.IsValid(currentStatus, newStatus))
            //{
            //    BudgetGroupProposalDto updatedBudgetGroupProposalDto = _budgetGroupProposalStore.UpdateBudgetGroupProposalStatus(budgetGroupProposalId, (int)newStatus, user);
            //    BudgetGroupProposal updatedBudgetGroupProposal = Mapper.Map(updatedBudgetGroupProposalDto);
            //    return updatedBudgetGroupProposal;
            //}
            //else
            //{
            //    throw new DataConditionException(string.Format("Invalid status transition occured. Cannot go from {0} to {1}", currentStatus, newStatus));
            //}
            return null;
        }

        public BudgetGroupProposal Update(BudgetGroupProposal newBudgetGroupProposal, Guid user)
        {
            //BudgetGroupProposalDto oldBudgetGroupProposalDto = _budgetGroupProposalStore.GetBudgetGroupProposalById(newBudgetGroupProposal.BudgetGroupProposalId.Value);

            //if (oldBudgetGroupProposalDto == null)
            //    throw new EntityNotFoundException();

            ////if (oldBudgetGroupProposalDto.BudgetGroupStatusId == (int)BudgetGroupStatus.Locked)
            ////    throw new DataConditionException("Cannot UPDATE a LOCKED BudgetGroupProposal.");

            //BudgetGroupProposal oldBudgetGroupProposal = Mapper.Map(oldBudgetGroupProposalDto);
            //DateTime now = DateTime.UtcNow;

            ////MERGE old and new
            //BudgetGroupProposal mergedBudgetGroupProposal = MergeBudgetGroupProposal(oldBudgetGroupProposal, newBudgetGroupProposal, user, now);
            //mergedBudgetGroupProposal.BudgetGroupAdProposals = MergeAds(oldBudgetGroupProposal.BudgetGroupAdProposals, newBudgetGroupProposal.BudgetGroupAdProposals, newBudgetGroupProposal.BudgetGroupProposalId.Value, user, now);
            //mergedBudgetGroupProposal.BudgetGroupLocationProposals = MergeLocations(oldBudgetGroupProposal.BudgetGroupLocationProposals, newBudgetGroupProposal.BudgetGroupLocationProposals, newBudgetGroupProposal.BudgetGroupProposalId.Value, user, now);
            //mergedBudgetGroupProposal.BudgetGroupTimedBudgetProposal = oldBudgetGroupProposal.BudgetGroupTimedBudgetProposal;
            //mergedBudgetGroupProposal.BudgetGroupConversionActionProposals = MergeConversionActions(oldBudgetGroupProposal.BudgetGroupConversionActionProposals, newBudgetGroupProposal.BudgetGroupConversionActionProposals, newBudgetGroupProposal.BudgetGroupProposalId.Value, user, now);
            //mergedBudgetGroupProposal.BudgetGroupAdProposals = MergeAds(oldBudgetGroupProposal.BudgetGroupAdProposals, newBudgetGroupProposal.BudgetGroupAdProposals, newBudgetGroupProposal.BudgetGroupProposalId.Value, user, now);

            ////DELETE children that are flagged for deletion
            //Dictionary<string, List<Guid>> itemsToDelete = BuildDeleteDictionaryAndStripDeletedItems(mergedBudgetGroupProposal);

            ////SAVE to db and return result
            //BudgetGroupProposalDto mergedBudgetGroupProposalDto = Mapper.Map(mergedBudgetGroupProposal);
            //var savedBudgetGroup =
            //    _budgetGroupProposalStore.UpdateBudgetGroupProposal(mergedBudgetGroupProposalDto, itemsToDelete);
            //return Mapper.Map(savedBudgetGroup);
            throw new NotImplementedException();
        }

        BudgetGroupProposal IBudgetGroupProposalManager.GetById(Guid budgetGroupProposalId)
        {
            BudgetGroupProposal budgetGroupProposal = Mapper.Map(_budgetGroupProposalStore.GetBudgetGroupProposalById(budgetGroupProposalId));
            return budgetGroupProposal;
        }

        List<BudgetGroupProposal> IBudgetGroupProposalManager.GetBy(BudgetGroupProposalColumn column, string value)
        {
            List<BudgetGroupProposalDto> budgetGroupProposalDtos = _budgetGroupProposalStore.GetBudgetGroupProposalsBy(column.ToString());
            List<BudgetGroupProposal> budgetGroupProposals = budgetGroupProposalDtos.Select(bgp => Mapper.Map(bgp)).ToList();
            return budgetGroupProposals;
        }

        public BudgetGroupProposal MediaBriefPulled(Guid budgetGroupProposalId, Guid user, string notes = null)
        {
            BudgetGroupProposalDto budgetGroupProposalDto = _budgetGroupProposalStore.GetBudgetGroupProposalById(budgetGroupProposalId);

            if (budgetGroupProposalDto == null)
                throw new EntityNotFoundException();

            //if (budgetGroupProposalDto.BudgetGroupStatusId != (int)BudgetGroupStatus.Submitted)
            //    throw new DataConditionException("You cannot pull a Media Brief on a Budget Group Proposal that is not in Submitted state.");

            BudgetGroupLedgerDto budgetGroupLedgerDto = new BudgetGroupLedgerDto()
            {
                BudgetGroupProposalId = budgetGroupProposalId,
                CreatedBy = user,
                Created = DateTime.UtcNow,
                BudgetGroupEventId = (int)BudgetGroupEvent.MediaBriefPulled,
                Notes = string.IsNullOrEmpty(notes) ? null : notes
            };

            BudgetGroupProposalDto updatedBudgetGroupProposalDto = _budgetGroupProposalStore.UpdateBudgetGroupProposalStatus(budgetGroupProposalId, (int)BudgetGroupStatus.Locked, user, budgetGroupLedgerDto);            

            return Mapper.Map(updatedBudgetGroupProposalDto);
        }

        public BudgetGroupProposal MediaBriefProvisioned(Guid budgetGroupProposalId, Guid user, string notes = null)
        {
            BudgetGroupProposalDto budgetGroupProposalDto = _budgetGroupProposalStore.GetBudgetGroupProposalById(budgetGroupProposalId);

            if (budgetGroupProposalDto == null)
                throw new EntityNotFoundException();

            //if (budgetGroupProposalDto.BudgetGroupStatusId != (int)BudgetGroupStatus.Locked)
            //    throw new DataConditionException("You cannot provision a Media Brief that is not in Locked state.");

            BudgetGroupLedgerDto budgetGroupLedgerDto = new BudgetGroupLedgerDto()
            {
                BudgetGroupProposalId = budgetGroupProposalId,
                CreatedBy = user,
                Created = DateTime.UtcNow,
                BudgetGroupEventId = (int)BudgetGroupEvent.MediaBriefProvisioned,
                Notes = string.IsNullOrEmpty(notes) ? null : notes
            };

            BudgetGroupProposalDto updatedBudgetGroupProposalDto = _budgetGroupProposalStore.UpdateBudgetGroupProposalStatus(budgetGroupProposalId, (int)BudgetGroupStatus.Ready, user, budgetGroupLedgerDto);
            return Mapper.Map(updatedBudgetGroupProposalDto);
        }
        


        /// <summary>
        /// This is technically only viable on the initial move to live that has since been discontinued. We needed to create the budgetGroupId from the creation so this method wont work.
        /// </summary>
        /// <param name="budgetGroupProposalId"></param>
        /// <param name="user"></param>
        /// <param name="notes"></param>
        /// <returns></returns>
        //public BudgetGroup MoveToLive(Guid budgetGroupProposalId, Guid user, string notes = null)
        //{
        //    BudgetGroupProposal budgetGroupProposal = Mapper.Map(_budgetGroupProposalStore.GetBudgetGroupProposalById(budgetGroupProposalId));

        //    if (budgetGroupProposal == null)
        //        throw new EntityNotFoundException($"Could not find BudgetGroupProposal with BudgetGroupProposalId={budgetGroupProposalId}.");

        //    if (budgetGroupProposal.BudgetGroupStatusId != (int)BudgetGroupStatus.Ready)
        //        throw new DataConditionException("BudgetGroupProposal must be in Ready State before it can be made live.");


        //    var budgetGroupId = _budgetGroupLedgerStore.GetBudgetGroupLedgerByBudgetGroupProposal(budgetGroupProposalId).BudgetGroupId ?? new Guid();

        //    BudgetGroup budgetGroup = ProposalToLive.Map(budgetGroupProposal, budgetGroupId, user);
        //    //set budget group proposal status back to draft
        //    budgetGroupProposal.BudgetGroupStatusId = (int) BudgetGroupStatus.Draft;
        //    //bump buget group status to Live
        //    budgetGroup.BudgetGroupStatusId = (int)BudgetGroupStatus.Live;
        //    BudgetGroupHistory budgetGroupHistory = LiveToHistory.Map(budgetGroup);

        //    BudgetGroupLedger budgetGroupLedger = new BudgetGroupLedger()
        //    {
        //        BudgetGroupId = budgetGroup.BudgetGroupId,
        //        BudgetGroupProposalId = budgetGroupProposal.BudgetGroupProposalId,
        //        BudgetGroupHistoryId = budgetGroupHistory.BudgetGroupHistoryId,
        //        CreatedBy = user,
        //        Created = DateTime.UtcNow,
        //        BudgetGroupEventId = BudgetGroupEvent.StatsAvailable,
        //        Notes = string.IsNullOrEmpty(notes) ? null : notes
        //    };

        //    //Back to dto's to pass off to DAL and save to DB   
        //    BudgetGroupProposalDto budgetGroupProposalDto = Mapper.Map(budgetGroupProposal);
        //    BudgetGroupDto budgetGroupDto = Mapper.Map(budgetGroup);
        //    BudgetGroupHistoryDto budgetGroupHistoryDto = Mapper.Map(budgetGroupHistory);
        //    BudgetGroupLedgerDto budgetGroupLedgerDto = Mapper.Map(budgetGroupLedger);

        //    BudgetGroupDto createBudgetGroup = _budgetGroupProposalStore.ProcessAllBudgetGroupChanges(budgetGroupDto, budgetGroupProposalDto, budgetGroupHistoryDto, budgetGroupLedgerDto);
        //    return Mapper.Map(createBudgetGroup);             
        //}

        #region Helpers

        private void CheckRulesFor(BudgetGroupProposal bgp, Guid createdBy)
        {
            /* ADS */
            if (bgp.BudgetGroupAdProposals == null || bgp.BudgetGroupAdProposals.Count == 0)
                throw new DataConditionException("You must provide at least one BudgetGroupAdProposal.");

            bgp.BudgetGroupAdProposals.ForEach(a => {
                if (!a.AdId.HasValue)
                    throw new DataConditionException("You must provide a valid AdId for BudgetGroupAdProposal.");
            });

            /* LOCATIONS */
            if (bgp.BudgetGroupLocationProposals == null || bgp.BudgetGroupLocationProposals.Count == 0)
                throw new DataConditionException("You must provide at least one BudgetGroupLocationProposal.");

            bgp.BudgetGroupLocationProposals.ForEach(l => {
                if (l.LocationId == null)
                    throw new DataConditionException("You must provide a valid LocationId for BudgetGroupLocationProposal.");
            });

            /* BudgetGroupTimedBudgetProposal */
            if (bgp.BudgetGroupTimedBudgetProposals == null || bgp.BudgetGroupTimedBudgetProposals.Count == 0)
                throw new DataConditionException("You must provide one BudgetGroupTimedBudgetProposal.");

            bgp.BudgetGroupTimedBudgetProposals.ForEach(b =>
            {
                if(b.BudgetAmount == null)
                throw new DataConditionException("You must provide a BudgetAmount for BudgetGroupTimedBudgetProposal.");

                if (b.StartDate == null)
                    throw new DataConditionException("You must provide a StartDate for BudgetGroupTimedBudgetProposal.");

                if (b.EndDate == null)
                    throw new DataConditionException("You must provide an EndDate for BudgetGroupTimedBudgetProposal.");

                if (b.StartDate > b.EndDate)
                    throw new DataConditionException("Start Date cannot be after End Date.");

        });
            bgp.BudgetGroupPlacementProposals?.ForEach(p => {
                if (p.PlacementId == null)
                    throw new DataConditionException("You must provide a valid PlacementId for BudgetGroupPlacementProposal.");
            });

            bgp.BudgetGroupConversionActionProposals?.ForEach(ca =>
            {
                if (ca.ConversionActionId == null)
                    throw new DataConditionException("You must provide a valid ConversionActionId for BudgetGroupConversionActionProposal.");
            });
        }

        private Dictionary<string, List<Guid>> BuildDeleteDictionaryAndStripDeletedItems(BudgetGroupProposal budgetGroupProposal)
        {
            //Dictionary<string, List<Guid>> itemsToDelete = new Dictionary<string, List<Guid>>()
            //{
            //    {"ads", new List<Guid>()},
            //    {"locations", new List<Guid>()},
            //    {"timedBudget", new List<Guid>()},
            //    {"conversionActions", new List<Guid>()},
            //    {"placements", new List<Guid>()},
            //};

            //if (budgetGroupProposal.BudgetGroupAdProposals != null)
            //{
            //    for (int i = budgetGroupProposal.BudgetGroupAdProposals.Count - 1; i >= 0; i--)
            //    {
            //        if (budgetGroupProposal.BudgetGroupAdProposals[i].DeleteMe.HasValue && budgetGroupProposal.BudgetGroupAdProposals[i].DeleteMe.Value)
            //        {
            //            itemsToDelete["ads"].Add(budgetGroupProposal.BudgetGroupAdProposals[i].BudgetGroupAdProposalId.Value);
            //            budgetGroupProposal.BudgetGroupAdProposals.RemoveAt(i);
            //        }
            //    }
            //}

            //if (budgetGroupProposal.BudgetGroupLocationProposals != null) {
            //    for (int i = budgetGroupProposal.BudgetGroupLocationProposals.Count - 1; i >= 0; i--)
            //    {
            //        if (budgetGroupProposal.BudgetGroupLocationProposals[i].DeleteMe.HasValue && budgetGroupProposal.BudgetGroupLocationProposals[i].DeleteMe.Value)
            //        {
            //            itemsToDelete["locations"].Add(budgetGroupProposal.BudgetGroupLocationProposals[i].BudgetGroupLocationProposalId.Value);
            //            budgetGroupProposal.BudgetGroupLocationProposals.RemoveAt(i);
            //        }
            //    }
            //}

            //if (budgetGroupProposal.BudgetGroupTimedBudgetProposal != null && budgetGroupProposal.BudgetGroupTimedBudgetProposal.DeleteMe.HasValue)
            //{
            //    itemsToDelete["timedBudget"].Add(budgetGroupProposal.BudgetGroupTimedBudgetProposal.BudgetGroupTimedBudgetProposalId.Value);
            //}

            //if (budgetGroupProposal.BudgetGroupConversionActionProposals != null)
            //{
            //    for (int i = budgetGroupProposal.BudgetGroupConversionActionProposals.Count - 1; i >= 0; i--)
            //    {
            //        if (budgetGroupProposal.BudgetGroupConversionActionProposals[i].DeleteMe.HasValue && budgetGroupProposal.BudgetGroupConversionActionProposals[i].DeleteMe.Value)
            //        {
            //            itemsToDelete["conversionActions"].Add(budgetGroupProposal.BudgetGroupConversionActionProposals[i].BudgetGroupConversionActionProposalId.Value);
            //            budgetGroupProposal.BudgetGroupConversionActionProposals.RemoveAt(i);
            //        }
            //    }
            //}

            //if (budgetGroupProposal.BudgetGroupPlacementProposals != null)
            //{
            //    for (int i = budgetGroupProposal.BudgetGroupPlacementProposals.Count - 1; i >= 0; i--)
            //    {
            //        if (budgetGroupProposal.BudgetGroupPlacementProposals[i].DeleteMe.HasValue && budgetGroupProposal.BudgetGroupPlacementProposals[i].DeleteMe.Value)
            //        {
            //            itemsToDelete["placements"].Add(budgetGroupProposal.BudgetGroupPlacementProposals[i].BudgetGroupPlacementProposalId.Value);
            //            budgetGroupProposal.BudgetGroupPlacementProposals.RemoveAt(i);
            //        }
            //    }
            //}

            //return itemsToDelete;
            return null;
        }

        private BudgetGroupProposal MergeBudgetGroupProposal(BudgetGroupProposal oldBgp, BudgetGroupProposal newBgp, Guid user, DateTime now)
        {
            //var mergedBudgetGroupProposal = new BudgetGroupProposal()
            //{
            //    BudgetGroupProposalId = oldBgp.BudgetGroupProposalId,
            //    BudgetGroupStatusId = oldBgp.BudgetGroupStatusId,
            //    BudgetGroupName = newBgp.BudgetGroupName ?? oldBgp.BudgetGroupName,
            //    BudgetGroupBudget = newBgp.BudgetGroupBudget ?? oldBgp.BudgetGroupBudget,
            //    CreatedBy = oldBgp.CreatedBy,
            //    Created = oldBgp.Created,
            //    ModifiedBy = (!string.IsNullOrEmpty(newBgp.BudgetGroupName) || newBgp.BudgetGroupBudget.HasValue) ? user : oldBgp.ModifiedBy,
            //    Modified = (!string.IsNullOrEmpty(newBgp.BudgetGroupName) || newBgp.BudgetGroupBudget.HasValue) ? now : oldBgp.Modified,
            //};
            //return mergedBudgetGroupProposal;
            throw new NotImplementedException();
        }

        //private List<BudgetGroupAdProposal> MergeAds(List<BudgetGroupAdProposal> oldAds, List<BudgetGroupAdProposal> newAds, Guid budgetGroupProposalId, Guid user, DateTime now)
        //{
        //    var mergedBgp = (from n in newAds
        //                     join o in oldAds on n.BudgetGroupAdProposalId equals o.BudgetGroupAdProposalId into m
        //                     from o in m.DefaultIfEmpty()
        //                     select new BudgetGroupAdProposal() {
        //                         BudgetGroupAdProposalId = n.BudgetGroupAdProposalId.HasValue ? o.BudgetGroupAdProposalId : Guid.NewGuid(),
        //                         BudgetGroupProposalId = budgetGroupProposalId,
        //                         AdId = n.AdId,
        //                         CreatedBy = !n.BudgetGroupAdProposalId.HasValue ? user : o.CreatedBy,
        //                         Created = !n.BudgetGroupAdProposalId.HasValue ? now : o.Created,
        //                         ModifiedBy = n.BudgetGroupAdProposalId.HasValue ? user : (Guid?)null,
        //                         Modified = n.BudgetGroupAdProposalId.HasValue ? now : (DateTime?)null,
        //                         DeleteMe = (n.DeleteMe.HasValue && n.DeleteMe.Value) ? n.DeleteMe.Value : (bool?)null
        //        }).ToList();

        //    return mergedBgp;
        //}

        //private List<BudgetGroupLocationProposal> MergeLocations(List<BudgetGroupLocationProposal> oldLocations, List<BudgetGroupLocationProposal> newLocations, Guid budgetGroupProposalId, Guid user, DateTime now)
        //{
        //    var mergedBgp = (from n in newLocations
        //                     join o in oldLocations on n.BudgetGroupLocationProposalId equals o.BudgetGroupLocationProposalId into m
        //                     from o in m.DefaultIfEmpty()
        //                     select new BudgetGroupLocationProposal()
        //                     {
        //                         BudgetGroupLocationProposalId = n.BudgetGroupLocationProposalId.HasValue ? o.BudgetGroupLocationProposalId : Guid.NewGuid(),
        //                         BudgetGroupProposalId = budgetGroupProposalId,
        //                         LocationId = n.LocationId,
        //                         CreatedBy = !n.BudgetGroupLocationProposalId.HasValue ? user : o.CreatedBy,
        //                         Created = !n.BudgetGroupLocationProposalId.HasValue ? now : o.Created,
        //                         ModifiedBy = n.BudgetGroupLocationProposalId.HasValue ? user : (Guid?)null,
        //                         Modified = n.BudgetGroupLocationProposalId.HasValue ? now : (DateTime?)null,
        //                         DeleteMe = (n.DeleteMe.HasValue && n.DeleteMe.Value) ? n.DeleteMe.Value : (bool?)null
        //                     }).ToList();

        //    return mergedBgp;
        //}

        private List<BudgetGroupConversionActionProposal> MergeConversionActions(List<BudgetGroupConversionActionProposal> oldConversionActions, List<BudgetGroupConversionActionProposal> newConversionActions, Guid budgetGroupProposalId, Guid user, DateTime now)
        {
            var mergedBgp = (from n in newConversionActions
                             join o in oldConversionActions on n.BudgetGroupConversionActionProposalId equals o.BudgetGroupConversionActionProposalId into m
                             from o in m.DefaultIfEmpty()
                             select new BudgetGroupConversionActionProposal()
                             {
                                 BudgetGroupConversionActionProposalId = n.BudgetGroupConversionActionProposalId.HasValue ? o.BudgetGroupConversionActionProposalId : Guid.NewGuid(),
                                 BudgetGroupProposalId = budgetGroupProposalId,
                                 ConversionActionId = n.ConversionActionId,
                                 CreatedBy = !n.BudgetGroupConversionActionProposalId.HasValue ? user : o.CreatedBy,
                                 Created = !n.BudgetGroupConversionActionProposalId.HasValue ? now : o.Created,
                                 ModifiedBy = n.BudgetGroupConversionActionProposalId.HasValue ? user : (Guid?)null,
                                 Modified = n.BudgetGroupConversionActionProposalId.HasValue ? now : (DateTime?)null,
                                 DeleteMe = (n.DeleteMe.HasValue && n.DeleteMe.Value) ? n.DeleteMe.Value : (bool?)null
                             }).ToList();

            return mergedBgp;
        }

        //private List<BudgetGroupPlacementProposal> MergePlacements(List<BudgetGroupPlacementProposal> oldConversionActions, List<BudgetGroupPlacementProposal> newConversionActions, Guid budgetGroupProposalId, Guid user, DateTime now)
        //{
        //    var mergedBgp = (from n in newConversionActions
        //                     join o in oldConversionActions on n.BudgetGroupPlacementProposalId equals o.BudgetGroupPlacementProposalId into m
        //                     from o in m.DefaultIfEmpty()
        //                     select new BudgetGroupPlacementProposal()
        //                     {
        //                         BudgetGroupPlacementProposalId = n.BudgetGroupPlacementProposalId.HasValue ? o.BudgetGroupPlacementProposalId : Guid.NewGuid(),
        //                         BudgetGroupProposalId = budgetGroupProposalId,
        //                         PlacementId = n.PlacementId,
        //                         AssignedBy = !n.BudgetGroupPlacementProposalId.HasValue ? user : o.AssignedBy,
        //                         AssignedDate = !n.BudgetGroupPlacementProposalId.HasValue ? now : o.AssignedDate,
        //                         DeleteMe = (n.DeleteMe.HasValue && n.DeleteMe.Value) ? n.DeleteMe.Value : (bool?)null
        //                     }).ToList();

        //    return mergedBgp;
        //}
        #endregion
    }
}
