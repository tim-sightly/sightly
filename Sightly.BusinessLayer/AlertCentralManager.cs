﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;
using Sightly.DAL.DTO;

using AdwordCustomer = Sightly.BusinessLayer.DomainObjects.AdwordCustomer;
using AdwordCustomerDto = Sightly.DAL.DTO.AdwordCustomer;
using AdwordsKpiStatsData = Sightly.BusinessLayer.DomainObjects.AdwordsKpiStatsData;
using KpiDefinitionDataDto = Sightly.DAL.DTO.KpiDefinitionData;
using AdwordsKpiStatsDataDto = Sightly.DAL.DTO.AdwordsKpiStatsData;
using DoubleVerifyKpiStatsData = Sightly.BusinessLayer.DomainObjects.DoubleVerifyKpiStatsData;
using MoatKpiStatsDataDto = Sightly.DAL.DTO.MoatKpiStatsData;
using NielsenKpiStatsDataDto = Sightly.DAL.DTO.NielsenKpiStatsData;
using DoubleVerifyKpiStatsDataDto = Sightly.DAL.DTO.DoubleVerifyKpiStatsData;
using KpiDefinitionData = Sightly.BusinessLayer.DomainObjects.KpiDefinitionData;
using MoatKpiStatsData = Sightly.BusinessLayer.DomainObjects.MoatKpiStatsData;
using NielsenKpiStatsData = Sightly.BusinessLayer.DomainObjects.NielsenKpiStatsData;


namespace Sightly.BusinessLayer
{
    public class AlertCentralManager : IAlertCentralManager
    {
        private readonly IAlertCentralStore _alertCentralStore;

        public AlertCentralManager(IAlertCentralStore alertCentralStore)
        {
            _alertCentralStore = alertCentralStore;
        }

        public List<AdwordCustomer> GetAwCustomersThatAreCurrentlyActive(DateTime statDate)
        {
            List<AdwordCustomerDto> adwordCustomerDto = _alertCentralStore.GetAwCustomersThatAreCurrentlyActive(statDate);
            return adwordCustomerDto.Select(Mapper.Map).ToList();
        }

        public List<AdwordCustomer> GetAwCustomersThatAreCurrentlyActiveByFlight(DateTime statDate)
        {
            List<AdwordCustomerDto> adwordCustomerDto = _alertCentralStore.GetAwCustomersThatAreCurrentlyActiveByFlight(statDate);
            return adwordCustomerDto.Select(Mapper.Map).ToList();
        }

        public List<KpiDefinitionData> GetKpiDefinitionData(long customerId)
        {
            List<KpiDefinitionDataDto> kpiDefinitionDataDto = _alertCentralStore.GetKpiDefinitionData(customerId);
            return kpiDefinitionDataDto.Select(Mapper.Map).ToList();
        }

        public AdwordsKpiStatsData GetAdwordsKpiStatsData(long customerId)
        {
            AdwordsKpiStatsDataDto adwordsKpiStatsDataDto = _alertCentralStore.GetAdwordsKpiStatsData(customerId);
            return Mapper.Map(adwordsKpiStatsDataDto);
        }

        public AdwordsKpiStatsData GetAdwordsKpiStatsDataCampaignAndDate(long customerId, DateTime startDate, DateTime endDate)
        {
            AdwordsKpiStatsDataDto adwordsKpiStatsDataDto = _alertCentralStore.GetAdwordsKpiStatsDataByCustomerAndDates(customerId, startDate, endDate);
            return Mapper.Map(adwordsKpiStatsDataDto);
        }

        public MoatKpiStatsData GetMoatKpiStatsData(long customerId)
        {
            MoatKpiStatsDataDto moatKpiStatsDataDto = _alertCentralStore.GetMoatKpiStatsData(customerId);
            return Mapper.Map(moatKpiStatsDataDto);
        }

        public MoatKpiStatsData GetMoatKpiStatsDataCampaignAndDate(long customerId, DateTime startDate, DateTime endDate)
        {
            MoatKpiStatsDataDto moatKpiStatsDataDto = _alertCentralStore.GetMoatKpiStatsDataByCustomerAndDates(customerId, startDate, endDate);
            return Mapper.Map(moatKpiStatsDataDto);
        }

        public NielsenKpiStatsData GetNielsenKpiStatsData(long customerId)
        {
            NielsenKpiStatsDataDto nielsenKpiStatsDataDto = _alertCentralStore.GetNielsenKpiStatsData(customerId);
            return Mapper.Map(nielsenKpiStatsDataDto);
        }

        public NielsenKpiStatsData GetNielsenKpiStatsDataCampaignAndDate(long customerId, DateTime startDate, DateTime endDate)
        {
            NielsenKpiStatsDataDto nielsenKpiStatsDataDto = _alertCentralStore.GetNielsenKpiStatsDataByCustomerAndDates(customerId, startDate, endDate);
            return Mapper.Map(nielsenKpiStatsDataDto);
        }

        public DoubleVerifyKpiStatsData GetDoubleVerifyKpiStatsData(long customerId)
        {
            DoubleVerifyKpiStatsDataDto doubleVerifyKpiStatsDataDto =_alertCentralStore.GetDoubleVerifyKpiStatsData(customerId);
            return Mapper.Map(doubleVerifyKpiStatsDataDto);
        }

        public DoubleVerifyKpiStatsData GetDoubleVerifyKpiStatsDataCampaignAndDate(long customerId, DateTime startDate, DateTime endDate)
        {
            DoubleVerifyKpiStatsDataDto doubleVerifyKpiStatsDataDto = _alertCentralStore.GetDoubleVerifyKpiStatsDataByCustomerAndDates(customerId, startDate, endDate);
            return Mapper.Map(doubleVerifyKpiStatsDataDto);
        }
    }
}