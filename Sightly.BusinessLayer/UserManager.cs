﻿using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;
using UserDto = Sightly.DAL.DTO.User;

namespace Sightly.BusinessLayer
{
    public class UserManager : IUserManager
    {
        private readonly IUserStore _userStore;

        public UserManager(IUserStore userStore)
        {
            _userStore = userStore;
        }

        public User GetUserByEmail(string userEmail)
        {
            UserDto user = _userStore.GetUserByEmail(userEmail);
            return Mapper.Map(user);
        }
    }
}
