﻿using Sightly.BusinessLayer.Common;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessLayer
{
    public interface IBudgetGroupProposalManager
    {
        BudgetGroupProposal Create(BudgetGroupProposal budgetGroupProposal, Order order, Guid createdBy);
        BudgetGroupProposal UpdateStatus(Guid budgetGroupProposalId, BudgetGroupStatus newStatus, Guid user);
        BudgetGroupProposal Update(BudgetGroupProposal budgetGroupProposal, Guid user);
        BudgetGroupProposal GetById(Guid budgetGroupProposalId);
        List<BudgetGroupProposal> GetBy(BudgetGroupProposalColumn column, string value);
        BudgetGroupProposal MediaBriefPulled(Guid budgetGroupProposalId, Guid user, string notes = null);
        BudgetGroupProposal MediaBriefProvisioned(Guid budgetGroupProposalId, Guid user, string notes = null);
    }
}
