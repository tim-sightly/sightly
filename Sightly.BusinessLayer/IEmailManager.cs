﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.BusinessLayer
{
    public interface IEmailManager
    {
        OrderEmailData GetOrderEmailByOrder(Guid orderId);
        List<OrderChangeInfo> GetOrderChangesByOrder(Guid orderId);
    }
}
