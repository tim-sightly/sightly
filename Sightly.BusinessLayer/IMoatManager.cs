﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;
using MoatKpiStatsData = Sightly.BusinessLayer.DomainObjects.MoatKpiStatsData;

namespace Sightly.BusinessLayer
{
    public interface IMoatManager
    {
        List<MoatKpiStatsData> GetMoatKpiStatsDataByAccount(Guid accountId);
        List<MoatKpiStatsData> GetMoatKpiStatsDataByAdvertiser(Guid advertiserId);
        List<MoatKpiStatsData> GetMoatKpiStatsDataByCampaign(Guid campaignId);
    }
}