using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;

namespace Sightly.BusinessLayer
{
    public class NielsenManager : INielsenManager
    {
        private readonly INielsenStore _nielsenStore;

        public NielsenManager(INielsenStore nielsenStore)
        {
            _nielsenStore = nielsenStore;
        }

        public List<NielsenKpiStatsData> GetNielsenKpiStatsDataByAccount(Guid accountId)
        {
            List<DAL.DTO.NielsenKpiStatsData> nielsenKpiStatsDataDto = _nielsenStore.GetNielsenKpiStatsDataByAccount(accountId);
            return nielsenKpiStatsDataDto.Select(Mapper.Map).ToList();
        }

        public List<NielsenKpiStatsData> GetNielsenKpiStatsDataByAdvertiser(Guid advertiserId)
        {
            List<DAL.DTO.NielsenKpiStatsData> nielsenKpiStatsDataDto = _nielsenStore.GetNielsenKpiStatsDataByAdvertiser(advertiserId);
            return nielsenKpiStatsDataDto.Select(Mapper.Map).ToList();
        }

        public List<NielsenKpiStatsData> GetNielsenKpiStatsDataByCampaign(Guid campaignId)
        {
            List<DAL.DTO.NielsenKpiStatsData> nielsenKpiStatsDataDto = _nielsenStore.GetNielsenKpiStatsDataByCampaign(campaignId);
            return nielsenKpiStatsDataDto.Select(Mapper.Map).ToList();
        }
    }
}