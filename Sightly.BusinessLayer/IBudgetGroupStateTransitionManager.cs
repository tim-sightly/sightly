﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;

namespace Sightly.BusinessLayer
{
    public interface IBudgetGroupStateTransitionManager
    {
        BudgetGroup MakeBudgetGroupProposalReadyForLive(Guid budgetGroupProposalId, Guid createdBy, string notes = null, bool isMediaAgency = false);
        List<BudgetGroup> MakeBudgetGroupProposalReadyForLiveByOrderId(Guid budgetGroupProposalId, Guid createdBy, string notes = null);
        List<BudgetGroup> MakeBudgetGroupLiveByOrderId(Guid orderId, Guid createdBy, string notes = null);
    }
}