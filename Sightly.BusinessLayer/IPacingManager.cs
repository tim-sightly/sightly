using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;

namespace Sightly.BusinessLayer
{
    public interface IPacingManager
    {
        List<CampaignPacingData> GetMediaAgencyPacingDataByDate(DateTime pacingDate);
        List<PlacementAssociation> getMediaAgencyPlacementByDate(DateTime pacingDate);
        int SaveProposedBudgetPacing(long customerId, long campaignId, decimal recomendedBudget, float overPacingRate, DateTime startDate, decimal totalBudget, int daysRemaining, Guid budgetGroupTimedBudgetId, DateTime pacingDate, Guid userId);
        List<CustomerPacingDate> GetLastPaceDateForAllCustomers();
        List<Manager> GetManagers();
    }
}