﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;

namespace Sightly.BusinessLayer
{
    public class AccountManager : IAccountManager
    {
        private readonly IAccountStore _accountStore;

        public AccountManager(IAccountStore accountStore)
        {
            _accountStore = accountStore;
        }

        public List<Account> ListAllAccounts()
        {
            List<Sightly.DAL.DTO.Account> accounts = _accountStore.ListAllAccounts();
            return accounts.Select(acc => Mapper.Map(acc)).ToList();
        }

        public List<Advertiser> ListAllAdvertisers()
        {
            List<Sightly.DAL.DTO.Advertiser> advertisers = _accountStore.ListAllAdvertisers();
            return advertisers.Select(adv => Mapper.Map(adv)).ToList();
        }
    }
}