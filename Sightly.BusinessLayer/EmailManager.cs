﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;
using OrderEmailDto = Sightly.DAL.DTO.OrderEmail;
using OrderChangesDto = Sightly.DAL.DTO.OrderChangeInfo;
using Sightly.Models;

namespace Sightly.BusinessLayer
{
    public class EmailManager : IEmailManager
    {
        private readonly IOrderStore _orderStore;

        public EmailManager(IOrderStore orderStore)
        {
            _orderStore = orderStore;
        }


        public OrderEmailData GetOrderEmailByOrder(Guid orderId)
        {
            OrderEmailDto orderEmail = _orderStore.GetOrderEmailByOrder(orderId);

            return Mapper.Map(orderEmail);
        }

        public List<OrderChangeInfo> GetOrderChangesByOrder(Guid orderId)
        {
            List<OrderChangesDto> orderChangeDtos = _orderStore.GetOrderChangeInfoByOrder(orderId);

            return orderChangeDtos.Select(Mapper.Map).ToList();

        }
    }
}