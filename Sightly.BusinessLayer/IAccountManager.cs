﻿using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessLayer
{
    public interface IAccountManager
    {
        List<Account> ListAllAccounts();
        List<Advertiser> ListAllAdvertisers();
    }
}