﻿using Sightly.BusinessLayer.DomainObjects.BudgetGroupTimedBudgetStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupAdStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupConversionActionStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupLocationStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupPlacementStates;
using System;
using System.Runtime.Serialization;
using System.Collections.Generic;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupStates
{
    [DataContract]
    public class BudgetGroupProposal : ABudgetGroup
    {
        [DataMember(Order = 11)]
        public Guid? AdId { get; set; }
        [DataMember(Order = 12)]
        public Guid? AudienceId { get; set; }
        [DataMember(Order = 13)]
        public decimal? BudgetGroupBudget { get; set; }
        [DataMember(Order = 14)]
        public string CreatedBy { get; set; }
        [DataMember(Order = 15)]
        public DateTime? CreatedDatetime { get; set; }
        [DataMember(Order = 16)]
        public string LastModifiedBy { get; set; }
        [DataMember(Order = 17)]
        public DateTime? LastModifiedDatetime { get; set; }


        public List<BudgetGroupAdProposal> BudgetGroupAdProposals { get; set; }
        [DataMember(Order = 20)]
        public List<BudgetGroupLocationProposal> BudgetGroupLocationProposals { get; set; }
        [DataMember(Order = 21)]
        public List<BudgetGroupTimedBudgetProposal> BudgetGroupTimedBudgetProposals { get; set; }
        [DataMember(Order = 22)]
        public List<BudgetGroupPlacementProposal> BudgetGroupPlacementProposals { get; set; }
        [DataMember(Order = 23)]
        public List<BudgetGroupConversionActionProposal> BudgetGroupConversionActionProposals { get; set; }
    }
}
