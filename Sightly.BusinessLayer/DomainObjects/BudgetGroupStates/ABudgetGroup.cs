﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupStates
{
    [DataContract]
    public abstract class ABudgetGroup
    {
        [DataMember(Order = 1)]
        public Guid? BudgetGroupId { get; set; }
        [DataMember(Order = 2)]
        public string BudgetGroupName { get; set; }
    }
}
