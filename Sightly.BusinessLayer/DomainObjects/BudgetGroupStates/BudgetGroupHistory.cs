﻿using Sightly.BusinessLayer.DomainObjects.BudgetGroupTimedBudgetStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupAdStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupConversionActionStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupLocationStates;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupPlacementStates;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupStates
{
    public class BudgetGroupHistory : ABudgetGroup
    {
        [DataMember(Order = 10)]
        public int? BudgetGroupStatusId { get; set; }
        [DataMember(Order = 11)]
        public Guid? CreatedBy { get; set; }
        [DataMember(Order = 12)]
        public DateTime? Created { get; set; }
        [DataMember(Order = 13)]
        public Guid? ModifiedBy { get; set; }
        [DataMember(Order = 14)]
        public DateTime? Modified { get; set; }



        [DataMember(Order = 20)]
        public List<BudgetGroupAdHistory> BudgetGroupAdHistories { get; set; }
        [DataMember(Order = 21)]
        public List<BudgetGroupLocationHistory> BudgetGroupLocationHistories { get; set; }
        [DataMember(Order = 22)]
        public List<BudgetGroupTimedBudgetHistory> BudgetGroupTimedBudgetHistories { get; set; }
        [DataMember(Order = 23)]
        public List<BudgetGroupPlacementHistory> BudgetGroupPlacementHistories { get; set; }
        [DataMember(Order = 24)]
        public List<BudgetGroupConversionActionHistory> BudgetGroupConversionActionHistories { get; set; }
    }
}
