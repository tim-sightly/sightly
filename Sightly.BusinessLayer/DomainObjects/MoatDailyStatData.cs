﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects
{
    [DataContract]
    public class MoatDailyStatData
    {
        [DataMember(Order = 1)]
        public Guid? Id { get; set; }

        [DataMember(Order = 2)]
        public string Type { get; set; }

        [DataMember(Order = 5)]
        public DateTime StatDate { get; set; }

        [DataMember(Order = 10)]
        public decimal Two_Sec_In_View_Rate_Unfiltered { get; set; }

        [DataMember(Order = 15)]
        public decimal ReachCompletePercent { get; set; }

        public decimal CustomerQuality { get; set; }

        [DataMember(Order = 25)]
        public decimal HumanAndAvocRate { get; set; }

        [DataMember(Order = 30)]
        public decimal HumanRate { get; set; }

        [DataMember(Order = 35)]
        public long AwCustomerId { get; set; }
    }
}
