﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.BusinessLayer.DomainObjects
{
    public class CustomerPacingDate
    {
        public long CustomerId { get; set; }
        public DateTime PacingDate { get; set; }
    }
}
