﻿using System;

namespace Sightly.BusinessLayer.DomainObjects
{
    public class DoubleVerifyKpiStatsData
    {
        public long CustomerId { get; set; }
        public decimal BrandSafetyOnTargetPercent_1x1 { get; set; }
        public decimal FraudSivtRate { get; set; }
        public decimal FraudSivtRate_1x1 { get; set; }
        public decimal InGeoRate_1x1 { get; set; }
        public decimal VideoViewableRate { get; set; }
        public DateTime? StatDate { get; set; }
        public long AwCustomerId { get; set; }

    }
}