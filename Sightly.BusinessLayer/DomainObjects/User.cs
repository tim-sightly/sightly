﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects
{
    [DataContract]
    public class User
    {
        [DataMember(Order = 1)]
        public Guid? UserId { get; set; }
        [DataMember(Order = 2)]
        public string FirstName { get; set; }
        [DataMember(Order = 3)]
        public string LastName { get; set; }
        [DataMember(Order = 4)]
        public string Email { get; set; }
        [DataMember(Order = 5)]
        public Guid? AccountId { get; set; }
    }
}
