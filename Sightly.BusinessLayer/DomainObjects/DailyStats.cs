﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects
{
    [DataContract]
    public class DailyStats
    {
        [DataMember(Order = 1)]
        public Guid? Id { get; set; }
        
        [DataMember(Order = 2)]
        public string Type { get; set; }
        
        [DataMember(Order = 5)]
        public DateTime StatDate { get; set; }
        
        [DataMember(Order = 10)]
        public long Impressions { get; set; }
        
        [DataMember(Order = 15)]
        public long Views { get; set; }
        
        [DataMember(Order = 20)]
        public long Clicks { get; set; }
        
        [DataMember(Order = 25)]
        public decimal Cost { get; set; }
        
        [DataMember(Order = 30)]
        public decimal PacingRate { get; set; }
        
        [DataMember(Order = 35)]
        public decimal Margin { get; set; }

        [DataMember(Order = 40)]
        public long AwCustomerId { get; set; }
    }

    
}