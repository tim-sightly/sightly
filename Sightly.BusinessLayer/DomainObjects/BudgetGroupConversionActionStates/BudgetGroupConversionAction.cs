﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupConversionActionStates
{
    [DataContract]
    public class BudgetGroupConversionAction : ABudgetGroupConversionAction
    {
        [DataMember(Order = 1)]
        public Guid? BudgetGroupConversionActionId { get; set; }
        [DataMember(Order = 2)]
        public Guid BudgetGroupId { get; set; }
        [DataMember(Order = 8)]
        public bool? DeleteMe { get; set; }
    }
}
