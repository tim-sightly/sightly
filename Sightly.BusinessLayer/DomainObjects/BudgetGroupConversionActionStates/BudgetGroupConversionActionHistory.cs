﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupConversionActionStates
{
    [DataContract]
    public class BudgetGroupConversionActionHistory : ABudgetGroupConversionAction
    {
        [DataMember(Order = 1)]
        public Guid BudgetGroupConversionActionHistoryId { get; set; }
        [DataMember(Order = 2)]
        public Guid BudgetGroupHistoryId { get; set; }
    }
}
