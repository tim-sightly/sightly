﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupConversionActionStates
{
    [DataContract]
    public class BudgetGroupConversionActionProposal : ABudgetGroupConversionAction
    {
        [DataMember(Order = 1)]
        public Guid? BudgetGroupConversionActionProposalId { get; set; }
        [DataMember(Order = 2)]
        public Guid? BudgetGroupProposalId { get; set; }
        [DataMember(Order = 8)]
        public bool? DeleteMe { get; set; }
    }
}
