﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupConversionActionStates
{
    [DataContract]
    public abstract class ABudgetGroupConversionAction
    {
        [DataMember(Order = 3)]
        public Guid? ConversionActionId { get; set; }
        [DataMember(Order = 4)]
        public Guid? CreatedBy { get; set; }
        [DataMember(Order = 5)]
        public DateTime? Created { get; set; }
        [DataMember(Order = 6)]
        public Guid? ModifiedBy { get; set; }
        [DataMember(Order = 7)]
        public DateTime? Modified { get; set; }
    }
}
