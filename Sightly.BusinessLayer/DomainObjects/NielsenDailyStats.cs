﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.BusinessLayer.DomainObjects
{
    [DataContract]
    public class NielsenDailyStats
    {
        [DataMember(Order = 1)]
        public Guid? Id { get; set; }

        [DataMember(Order = 2)]
        public string Type { get; set; }

        [DataMember(Order = 5)]
        public DateTime StatDate { get; set; }

        [DataMember(Order = 10)]
        public decimal DarComputer { get; set; }

        [DataMember(Order = 20)]
        public decimal DarMobile { get; set; }

        [DataMember(Order = 30)]
        public decimal DarTotalDigital { get; set; }

        [DataMember(Order = 40)]
        public long AwCustomerId { get; set; }
    }
}
