﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects
{
    [DataContract]
    public class DoubleVerifyStats
    {
        [DataMember(Order = 1)]
        public Guid? Id { get; set; }

        [DataMember(Order = 2)]
        public string Type { get; set; }

        [DataMember(Order = 5)]
        public DateTime StatDate { get; set; }


        [DataMember(Order = 10)]
        public decimal BrandSafetyOnTargetPercent_1x1 { get; set; }

        [DataMember(Order = 15)]
        public decimal FraudSivtRate { get; set; }

        [DataMember(Order = 20)]
        public decimal FraudSivtRate_1x1 { get; set; }

        [DataMember(Order = 25)]
        public decimal InGeoRate_1x1 { get; set; }

        [DataMember(Order = 30)]
        public decimal VideoViewableRate { get; set; }
        [DataMember(Order = 35)]
        public long AwCustomerId { get; set; }
    }

}
