﻿using System;

namespace Sightly.BusinessLayer.DomainObjects
{
    public class AdwordCustomer
    {
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int TotalDays { get; set; }
        public int DaysSoFar { get; set; }
        public decimal PercentComplete { get; set; }
        public int DaysLeft { get; set; }
    }
}