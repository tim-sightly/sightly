﻿namespace Sightly.BusinessLayer.DomainObjects
{
    public class KpiCondition
    {
        public int EquationTypeId { get; set; }
        public string ConditionName { get; set; }
        public string ConditionDisplay { get; set; }
    }
}