﻿namespace Sightly.BusinessLayer.DomainObjects
{
    public class KpiMetric
    {
        public int KpiMetricId { get; set; }
        public string KpiMetricName { get; set; }
        public string KpiAbbreviation { get; set; }
    }
}