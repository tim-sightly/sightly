﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sightly.BusinessLayer.Common;

namespace Sightly.BusinessLayer.DomainObjects
{
    public class BudgetGroupLedger
    {
        public long? BudgetGroupLedgerId { get; set; }
        public Guid? BudgetGroupId { get; set; }
        public Guid? BudgetGroupProposalId { get; set; }
        public Guid? BudgetGroupHistoryId { get; set; }
        public Guid? CreatedBy { get; set; }
        public DateTime? Created { get; set; }
        public BudgetGroupEvent? BudgetGroupEventId { get; set; }
        public string Notes { get; set; }
    }
}
