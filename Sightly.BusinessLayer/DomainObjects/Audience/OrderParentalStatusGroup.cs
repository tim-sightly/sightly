﻿using System;

namespace Sightly.BusinessLayer.DomainObjects.Audience
{
    public class OrderParentalStatusGroup
    {
        public Guid OrderId { get; set; }
        public Guid ParentalStatusId { get; set; }
        public string ParentalStatusName { get; set; }
        public Boolean Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModified { get; set; }
    }
}
