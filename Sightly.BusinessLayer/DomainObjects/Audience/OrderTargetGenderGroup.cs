﻿using System;

namespace Sightly.BusinessLayer.DomainObjects.Audience
{
    public class OrderTargetGenderGroup
    {
        public Guid OrderId { get; set; }
        public Guid GenderId { get; set; }
        public string GenderName { get; set; }
        public Boolean Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModified { get; set; }
    }
}
