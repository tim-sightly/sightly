﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.BusinessLayer.DomainObjects.Audience
{
    public class OrderHouseholdIncomeGroup
    {
        public Guid OrderId { get; set; }
        public int HouseholdIncomeId { get; set; }
        public string HouseholdIncomeName { get; set; }
        public Boolean Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }
    }
}
