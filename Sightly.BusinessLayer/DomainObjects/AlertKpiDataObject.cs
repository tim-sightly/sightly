﻿using System;
using System.Collections.Generic;

namespace Sightly.BusinessLayer.DomainObjects
{
    public class AlertKpiDataObject
    {
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int TotalDays { get; set; }
        public int DaysSoFar { get; set; }
        public decimal PercentComplete { get; set; }
        public int DaysLeft { get; set; }

        public List<KpiDefinitionData> KpiDefinitions { get; set; }

        public AdwordsKpiStatsData AdwordsKpiStats { get; set; }
        public DoubleVerifyKpiStatsData DoubleVerifyKpiStats { get; set; }
        public MoatKpiStatsData MoatKpiStats { get; set; }
        public NielsenKpiStatsData NielsenKpiStats { get; set; }

        public AlertKpiDataObject(long customerId, string customerName, DateTime startDate, DateTime endDate, int totalDays, int daysSoFar, decimal percentComplete, int daysLeft)
        {
            CustomerId = customerId;
            CustomerName = customerName;
            StartDate = startDate;
            EndDate = endDate;
            TotalDays = totalDays;
            DaysSoFar = daysSoFar;
            PercentComplete = percentComplete;
            DaysLeft = daysLeft;
            KpiDefinitions = new List<KpiDefinitionData>();
        }
    }
}
