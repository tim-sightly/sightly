﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects
{
    [DataContract]
    public class Advertiser
    {
        [DataMember(Order = 1)]
        public Guid AdvertiserId { get; set; }
        
        [DataMember(Order = 2)]
        public Guid AccountId { get; set; }
        
        [DataMember(Order = 3)]
        public string AdvertiserName { get; set; }
        
        [DataMember(Order = 4)]
        public Guid AdvertiserCategoryId { get; set; }
        
        [DataMember(Order = 5)]
        public decimal AdvertiserMargin { get; set; }
        
        [DataMember(Order = 6)]
        public string AdvertiserRefCode { get; set; }

        [DataMember(Order = 7)]
        public Guid AdvertiserSubCategoryId { get; set; }
    }
}