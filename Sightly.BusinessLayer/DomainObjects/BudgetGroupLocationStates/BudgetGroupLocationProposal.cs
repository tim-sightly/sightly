﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupLocationStates
{
    [DataContract]
    public class BudgetGroupLocationProposal : ABudgetGroupLocation
    {
        [DataMember(Order = 10)]
        public bool Deleted { get; set; }
        [DataMember(Order = 11)]
        public int SequenceId { get; set; }
        [DataMember(Order = 12)]
        public string CreatedBy { get; set; }
        [DataMember(Order = 13)]
        public DateTime CreatedDatetime { get; set; }
        [DataMember(Order = 14)]
        public string LastModifiedBy { get; set; }
        [DataMember(Order = 15)]
        public DateTime LastModifiedDatetime { get; set; }
    }
}
