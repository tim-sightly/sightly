﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupLocationStates
{
    [DataContract]
    public abstract class ABudgetGroupLocation
    {
        [DataMember(Order = 1)]
        public Guid BudgetGroupLocationId { get; set; }
        [DataMember(Order = 2)]
        public Guid BudgetGroupId { get; set; }
        [DataMember(Order = 3)]
        public Guid LocationId { get; set; }
    }
}
