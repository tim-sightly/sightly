﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupLocationStates
{
    public class BudgetGroupLocationHistory : ABudgetGroupLocation
    {
        [DataMember(Order = 10)]
        public Guid CreatedBy { get; set; }
        [DataMember(Order = 11)]
        public DateTime Created { get; set; }
    }
}
