﻿namespace Sightly.BusinessLayer.DomainObjects
{
    public class KpiDefinitionData
    {
        public int Sequence { get; set; }
        public long AdwordsCustomerId { get; set; }
        public string AdwordsCustomerName { get; set; }
        public string KpiSourceName { get; set; }
        public string OutcomeTypeName { get; set; }
        public string KpiMetricName { get; set; }
        public string KpiMetricAbbreviation { get; set; }
        public string ConditionDisplay { get; set; }
        public string EquationValue { get; set; }
        public string KpiUnitTypeName { get; set; }

        public string ActualValue { get; set; }
        public int Status { get; set; }
        public string Email { get; set; }
    }
}
