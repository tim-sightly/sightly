﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects
{
    [DataContract]
    public class Campaign
    {
        [DataMember(Order = 1)]
        public Guid CampaignId { get; set; }
        [DataMember(Order = 2)]
        public Guid AccountId { get; set; }
        [DataMember(Order = 3)]
        public Guid? AdvertiserId { get; set; }
        [DataMember(Order = 4)]
        public string CampaignName { get; set; }
        [DataMember(Order = 5)]
        public string CampaignRefCode { get; set; }
        [DataMember(Order = 6)]
        public string ParentAccountNames { get; set; }
        [DataMember(Order = 7)]
        public long AdWordsCustomerId { get; set; }
    }
}
