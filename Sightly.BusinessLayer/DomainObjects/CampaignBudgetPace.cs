using System;

namespace Sightly.BusinessLayer.DomainObjects
{
    public class CampaignBudgetPace
    {
        public Guid UserId { get; set; }
        public int? PacingId { get; set; }
        public DateTime? PacingDate { get; set; }
        public long CustomerId { get; set; }
        public long CampaignId { get; set; }
        public decimal RecommendedBudget { get; set; }
        public float OverPacingRate { get; set; }
        public DateTime StartDate { get; set; }
        public decimal TotalBudget { get; set; }
        public int DaysRemaining { get; set; }
        public Guid BudgetGroupTimedBudgetId { get; set; }

    }
}