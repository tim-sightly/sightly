using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupAdStates
{
    public class BudgetGroupAdHistory : ABudgetGroupAd
    {
        [DataMember(Order = 10)]
        public DateTime? Created { get; set; }
        [DataMember(Order = 11)]
        public Guid? CreatedBy { get; set; }
    }
}
