using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupAdStates
{
    [DataContract]
    public class BudgetGroupAdProposal : ABudgetGroupAd
    {
        [DataMember(Order = 10)]
        public int SequenceNo { get; set; }
        [DataMember(Order = 11)]
        public bool Paused { get; set; }
        [DataMember(Order = 12)]
        public bool Deleted { get; set; }
        [DataMember(Order = 13)]
        public string CreatedBy { get; set; }
        [DataMember(Order = 14)]
        public DateTime CreatedDatetime { get; set; }
        [DataMember(Order = 15)]
        public string LastModifiedBy { get; set; }
        [DataMember(Order = 16)]
        public DateTime LastModifiedDatetime { get; set; }
    }
}
