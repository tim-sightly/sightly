using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupAdStates
{
    [DataContract]
    public abstract class ABudgetGroupAd
    {
        [DataMember(Order = 1)]
        public Guid BudgetGroupAdId { get; set; }
        [DataMember(Order = 2)]
        public Guid BudgetGroupId { get; set; }
        [DataMember(Order = 3)]
        public Guid? AdId { get; set; }       
    }
}
