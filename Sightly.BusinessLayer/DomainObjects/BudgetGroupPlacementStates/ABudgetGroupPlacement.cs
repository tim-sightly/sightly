﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupPlacementStates
{
    [DataContract]
    public abstract class ABudgetGroupPlacement
    {
        [DataMember(Order = 1)]
        public Guid? BudgetGroupPlacementId { get; set; }
        [DataMember(Order = 2)]
        public Guid? BudgetGroupId { get; set; }
        [DataMember(Order = 3)]
        public Guid? PlacementId { get; set; }
    }
}
