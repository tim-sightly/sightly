﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupPlacementStates
{
    [DataContract]
    public class BudgetGroupPlacementHistory : ABudgetGroupPlacement
    {
        [DataMember(Order = 10)]
        public Guid? CreatedBy { get; set; }

        [DataMember(Order = 11)]
        public DateTime? Created { get; set; }
    }
}
