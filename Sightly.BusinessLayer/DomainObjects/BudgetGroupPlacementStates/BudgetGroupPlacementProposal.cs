﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupPlacementStates
{
    [DataContract]
    public class BudgetGroupPlacementProposal : ABudgetGroupPlacement
    {
        [DataMember(Order = 10)]
        public string AssignedBy { get; set; }
        [DataMember(Order = 11)]
        public DateTime AssignedDate { get; set; }
    }
}
