﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects
{
    [DataContract]
    public class Account
    {
        [DataMember(Order = 1)]
        public Guid? AccountId { get; set; }
        
        [DataMember(Order = 2)]
        public string AccountName { get; set; }
        
        [DataMember(Order = 3)]
        public string ShortCode { get; set; }
        
        [DataMember(Order = 4)]
        public Guid AccountTypeId { get; set; }
        
        [DataMember(Order = 5)]
        public decimal AccountMargin { get; set; }

        [DataMember(Order = 6)]
        public Guid ParentId { get; set; }
    }
}