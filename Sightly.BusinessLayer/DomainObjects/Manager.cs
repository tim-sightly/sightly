﻿using System;

namespace Sightly.BusinessLayer.DomainObjects
{
    public class Manager
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }

    }
}

