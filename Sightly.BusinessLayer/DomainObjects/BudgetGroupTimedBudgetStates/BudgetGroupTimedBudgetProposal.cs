﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupTimedBudgetStates
{
    [DataContract]
    public class BudgetGroupTimedBudgetProposal : ABudgetGroupTimedBudget
    {
        [DataMember(Order = 10)]
        public string CreatedBy { get; set; }
        [DataMember(Order = 11)]
        public DateTime CreatedDatetime { get; set; }
    }
}
