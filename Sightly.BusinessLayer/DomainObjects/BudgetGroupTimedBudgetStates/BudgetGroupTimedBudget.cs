﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupTimedBudgetStates
{
    [DataContract]
    public class BudgetGroupTimedBudget : ABudgetGroupTimedBudget
    {
        [DataMember(Order = 10)]
        public Guid? CreatedBy { get; set; }
        [DataMember(Order = 11)]
        public DateTime? Created { get; set; }
    }
}
