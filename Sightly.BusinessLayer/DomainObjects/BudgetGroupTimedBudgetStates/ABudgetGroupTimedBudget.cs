﻿using System;
using System.Runtime.Serialization;

namespace Sightly.BusinessLayer.DomainObjects.BudgetGroupTimedBudgetStates
{
    [DataContract]
    public abstract class ABudgetGroupTimedBudget
    {
        [DataMember(Order = 1)]
        public Guid? BudgetGroupTimedBudgetId { get; set; }
        [DataMember(Order = 2)]
        public Guid? BudgetGroupId { get; set; }
        [DataMember(Order = 3)]
        public decimal? BudgetAmount { get; set; }
        [DataMember(Order = 4)]
        public decimal? Margin { get; set; }
        [DataMember(Order = 5)]
        public DateTime? StartDate { get; set; }
        [DataMember(Order = 6)]
        public DateTime? EndDate { get; set; }
        [DataMember(Order = 7)]
        public Guid? BudgetGroupXrefPlacementId { get; set; }
        [DataMember(Order = 8)]
        public Guid? OrderId { get; set; }
    }
}
