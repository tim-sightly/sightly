﻿using System;

namespace Sightly.BusinessLayer.Exceptions
{
    [Serializable]
    public class BudgetException : Exception
    {
        public BudgetException()
        { }

        public BudgetException(string message) : base(message)
        { }

        public BudgetException(string message, Exception innerException) : base(message, innerException)
        { }
    }
}
