﻿using System;

namespace Sightly.BusinessLayer.Exceptions
{
    [Serializable]
    public class DataConditionException : Exception
    {
        public DataConditionException()
        { }

        public DataConditionException(string message) : base(message)
        { }

        public DataConditionException(string message, Exception innerException) : base (message, innerException)
        { }
    }
}
