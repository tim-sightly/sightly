﻿using System.Collections.Generic;
using System.Linq;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;

using KpiMetric = Sightly.BusinessLayer.DomainObjects.KpiMetric;
using KpiMetricDto = Sightly.DAL.DTO.KpiMetric;
using KpiCondition = Sightly.BusinessLayer.DomainObjects.KpiCondition;
using KpiConditionDto = Sightly.DAL.DTO.KpiCondition;
using KpiEditorData = Sightly.BusinessLayer.DomainObjects.KpiEditorData;
using KpiEditorDataDto = Sightly.DAL.DTO.KpiEditorData;


namespace Sightly.BusinessLayer
{
    public class KpiManager : IKpiManager
    {
        private readonly IKpiStore _kpiStore;

        public KpiManager(IKpiStore kpiStore)
        {
            _kpiStore = kpiStore;
        }

        public List<KpiCondition> GetKpiConditions()
        {
            List<KpiConditionDto> kpiConditions = _kpiStore.GetKpiConditions();
            return kpiConditions.Select(Mapper.Map).ToList();
        }

        public List<KpiMetric> GetKpiMetrics()
        {
            List<KpiMetricDto> kpiMetricDto = _kpiStore.GetKpiMetrics();
            return kpiMetricDto.Select(Mapper.Map).ToList();
        }

        public List<KpiEditorData> GetKpiEditorDataByCustomerId(long awCustomerId)
        {
            List<KpiEditorDataDto> kpiEditorDataDto = _kpiStore.GetKpiEditorDataByCustomerId(awCustomerId);
            return kpiEditorDataDto.Select(Mapper.Map).ToList();
        }

        public int SaveKpiEditorData(long adwordsCustomerId, int kpiMetricId, int equationTypeId, string equationValue,
            int sequence)
        {
            return _kpiStore.SaveKpiEditorData(
                adwordsCustomerId,
                kpiMetricId,
                equationTypeId,
                equationValue,
                sequence);
        }

        public void RemoveKpiEditorData(int adwordsCustomerKpiId)
        {
            _kpiStore.RemoveKpiEditorData(adwordsCustomerKpiId);
        }
    }
}