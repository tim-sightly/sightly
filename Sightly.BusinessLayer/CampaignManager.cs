﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;
using CampaignDto = Sightly.DAL.DTO.Campaign;

namespace Sightly.BusinessLayer
{
    public class CampaignManager : ICampaignManager
    {
        private readonly ICampaignStore _campaignStore;

        public CampaignManager(ICampaignStore campaignStore)
        {
            _campaignStore = campaignStore;
        }

        public List<Campaign> GetAllCampaigns()
        {
            List<CampaignDto> campaigns = _campaignStore.GetAllCampaigns();
            
            //convert DTOs to DomainObjects
            return campaigns.Select(c => Mapper.Map(c)).ToList();
        }

        public List<Campaign> GetAllMediaAgencyCampaigns()
        {
            List<CampaignDto> campaigns = _campaignStore.GetAllMediaAgencyCampaigns();

            //convert DTOs to DomainObjects
            return campaigns.Select(c => Mapper.Map(c)).ToList();
        }

        public List<Campaign> GetAllCampaignsForAdWordsCustomerId(long adWordsCustomerId)
        {
            List<CampaignDto> campaigns = _campaignStore.GetAllCampaignsForAdWordsCustomerId(adWordsCustomerId);
            return campaigns.Select(c => Mapper.Map(c)).ToList();
        }
    }
}
