﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.Common;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using Sightly.BusinessLayer.Exceptions;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;
using Sightly.Logging;
using BudgetGroupLedgerDto = Sightly.DAL.DTO.BudgetGroupLedger;
using BudgetGroupProposalDto = Sightly.DAL.DTO.BudgetGroupStates.BudgetGroupProposal;
using BudgetGroupTimedBudgetProposalDto = Sightly.DAL.DTO.BudgetGroupTimedBudgetStates.BudgetGroupTimedBudgetProposal;
using BudgetGroupDto = Sightly.DAL.DTO.BudgetGroupStates.BudgetGroup;
using BudgetGroupHistoryDto = Sightly.DAL.DTO.BudgetGroupStates.BudgetGroupHistory;
using OrderDto = Sightly.DAL.DTO.Order;

namespace Sightly.BusinessLayer
{
    public class BudgetGroupStateTransitionManager : IBudgetGroupStateTransitionManager
    {
        private readonly IBudgetGroupLedgerStore _budgetGroupLedgerStore;
        private readonly IBudgetGroupManager _budgetGroupManager;
        private readonly IBudgetGroupHistoryManager _budgetGroupHistoryManager;
        private readonly IBudgetGroupProposalManager _budgetGroupProposalManager;
        private readonly IBudgetGroupProposalStore _budgetGroupProposalStore;
        private readonly IBudgetGroupStore _budgetGroupStore;
        private readonly IStateTransitionStore _stateTransitionStore;
        private readonly IOrderStore _orderStore;
        private readonly ILoggingWrapper _loggingWrapper;


        public BudgetGroupStateTransitionManager(IBudgetGroupLedgerStore budgetGroupLedgerStore, 
                                                 IBudgetGroupManager budgetGroupManager, 
                                                 IBudgetGroupHistoryManager budgetGroupHistoryManager, 
                                                 IBudgetGroupProposalManager budgetGroupProposalManager, 
                                                 IBudgetGroupProposalStore budgetGroupProposalStore,
                                                 IBudgetGroupStore budgetGroupStore,
                                                 IStateTransitionStore stateTransitionStore,
                                                 IOrderStore orderStore,
                                                 ILoggingWrapper loggingWrapper)
        {
            _budgetGroupLedgerStore = budgetGroupLedgerStore;
            _budgetGroupManager = budgetGroupManager;
            _budgetGroupHistoryManager = budgetGroupHistoryManager;
            _budgetGroupProposalManager = budgetGroupProposalManager;
            _budgetGroupProposalStore = budgetGroupProposalStore;
            _budgetGroupStore = budgetGroupStore;
            _stateTransitionStore = stateTransitionStore;
            _orderStore = orderStore;
            _loggingWrapper = loggingWrapper;
        }

        public BudgetGroup MakeBudgetGroupProposalReadyForLive(Guid budgetGroupProposalId, Guid createdBy, string notes = null, bool isMediaAgency = false)
        {
            bool hasExistingLiveBudget = false;

            BudgetGroupProposalDto budgetGroupProposalDto = _budgetGroupProposalStore.GetBudgetGroupProposalById(budgetGroupProposalId);

            if (budgetGroupProposalDto == null)
                throw new EntityNotFoundException($"Unable to find BudgetGroupProposal with BudgetGroupId = {budgetGroupProposalId}.");

            if (budgetGroupProposalDto.BudgetGroupTimedBudgetProposals.Count == 0)
            {
                OrderDto orderDto = _orderStore.GetByBudgetGroupId(budgetGroupProposalId);

                if (orderDto == null)
                    throw new EntityNotFoundException($"Unable to find Order corresponding with BudgetGroupId = {budgetGroupProposalId}.");



                budgetGroupProposalDto.BudgetGroupTimedBudgetProposals.Add(new BudgetGroupTimedBudgetProposalDto()
                {
                    BudgetGroupTimedBudgetId = Guid.NewGuid(),
                    BudgetGroupId = budgetGroupProposalId,
                    BudgetAmount = orderDto.CampaignBudget ?? orderDto.MonthlyBudget.Value * orderDto.MonthCount,
                    Margin = orderDto.SightlyMargin,
                    StartDate = orderDto.StartDate,
                    EndDate = orderDto.EndDate,
                    CreatedBy = createdBy.ToString(),
                    CreatedDatetime = orderDto.CreatedDatetime,
                    BudgetGroupXrefPlacementId = null
                });

            }


            /* check for matching live budgetgroup */
            BudgetGroupLedgerDto liveEvent = _budgetGroupLedgerStore.GetLiveEventByProposalId(budgetGroupProposalId);

            if (liveEvent != null)
            {
                if (!liveEvent.BudgetGroupId.HasValue)
                    throw new EntityNotFoundException($"BudgetGroupLedgerId = {liveEvent.BudgetGroupLedgerId} is missing a BudgetGroupId. Unable to continue.");

                BudgetGroupDto budgetGroup = _budgetGroupStore.GetById(liveEvent.BudgetGroupId.Value);

                if (budgetGroup != null)
                    hasExistingLiveBudget = true;
                if(!isMediaAgency && budgetGroup.IsTheSame(budgetGroupProposalDto))
                    throw new BudgetException($"There were no changes detected between BudgetGroupProposalId = '{budgetGroupProposalId}' and BudgetGroup = '{budgetGroup.BudgetGroupId}'. Therefore you cannot move to Live.");
            }

            //map from proposal to live and historical states
            DateTime now = DateTime.UtcNow;
            BudgetGroupDto liveBudgetGroup = ProposalToLiveDto.Map(budgetGroupProposalDto, createdBy);
            BudgetGroupHistoryDto histBudgetGroup = LiveToHistoryDto.Map(liveBudgetGroup, Guid.NewGuid(), now);

            //create ledger entry to record this event
            var budgetGroupLedgerDto = new BudgetGroupLedgerDto()
            {
                BudgetGroupEventId = (int)BudgetGroupEvent.MediaBriefProvisioned,
                Notes = notes,
                BudgetGroupProposalId = budgetGroupProposalId,
                BudgetGroupId = liveBudgetGroup.BudgetGroupId,
                BudgetGroupHistoryId = histBudgetGroup.BudgetGroupId,
                CreatedBy = createdBy,
                Created = now
            };

            //SAVE
            BudgetGroupDto newBudgetGroupDto = _stateTransitionStore.MoveToLive(liveBudgetGroup, histBudgetGroup, budgetGroupLedgerDto, hasExistingLiveBudget);
            return Mapper.Map(newBudgetGroupDto);
        }

        public List<BudgetGroup> MakeBudgetGroupProposalReadyForLiveByOrderId(Guid orderId, Guid createdBy, string notes = null)
        {
            var budgetGroupList = new List<BudgetGroup>();
            List<Guid> budgetGroupProposalId = _budgetGroupProposalStore.GetBudgetGroupProposalIdByOrderId(orderId);
            budgetGroupProposalId.ForEach(brProp =>
            {
                budgetGroupList.Add(MakeBudgetGroupProposalReadyForLive(brProp, createdBy, notes));
            });
            return budgetGroupList;
        }

        public List<BudgetGroup> MakeBudgetGroupLiveByOrderId(Guid orderId, Guid createdBy, string notes = null)
        {
            var budgetGroupList = new List<BudgetGroup>();
            List<Guid> orderList = _orderStore.GetBudgetGroupsByOrderId(orderId);
            orderList.ForEach(bg =>
            {
                budgetGroupList.Add(MakeBudgetGroupProposalReadyForLive(bg, createdBy, notes, true));
            });
            return budgetGroupList;
        }
    }
}
