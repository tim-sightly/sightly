using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;

namespace Sightly.BusinessLayer
{
    public class MoatManager : IMoatManager
    {
        private readonly IMoatStore _moatStore;

        public MoatManager(IMoatStore moatStore)
        {
            _moatStore = moatStore;
        }

        public List<MoatKpiStatsData> GetMoatKpiStatsDataByAccount(Guid accountId)
        {
            List<DAL.DTO.MoatKpiStatsData> moatKpiStatsDataDto = _moatStore.GetMoatKpiStatsDataByAccount(accountId);
            return moatKpiStatsDataDto.Select(Mapper.Map).ToList();

        }

        public List<MoatKpiStatsData> GetMoatKpiStatsDataByAdvertiser(Guid advertiserId)
        {
            List<DAL.DTO.MoatKpiStatsData> moatKpiStatsDataDto = _moatStore.GetMoatKpiStatsDataByAdvertiser(advertiserId);
            return moatKpiStatsDataDto.Select(Mapper.Map).ToList();
        }

        public List<MoatKpiStatsData> GetMoatKpiStatsDataByCampaign(Guid campaignId)
        {
            List<DAL.DTO.MoatKpiStatsData> moatKpiStatsDataDto = _moatStore.GetMoatKpiStatsDataByCampaign(campaignId);
            return moatKpiStatsDataDto.Select(Mapper.Map).ToList();
        }
    }
}