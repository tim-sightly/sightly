﻿using System;
using System.Collections.Generic;
using Sightly.DAL.DTO;
using AdwordCustomer = Sightly.BusinessLayer.DomainObjects.AdwordCustomer;
using AdwordsKpiStatsData = Sightly.BusinessLayer.DomainObjects.AdwordsKpiStatsData;
using DoubleVerifyKpiStatsData = Sightly.BusinessLayer.DomainObjects.DoubleVerifyKpiStatsData;
using KpiDefinitionData = Sightly.BusinessLayer.DomainObjects.KpiDefinitionData;
using MoatKpiStatsData = Sightly.BusinessLayer.DomainObjects.MoatKpiStatsData;
using NielsenKpiStatsData = Sightly.BusinessLayer.DomainObjects.NielsenKpiStatsData;


namespace Sightly.BusinessLayer
{
    public interface IAlertCentralManager
    {
        List<AdwordCustomer> GetAwCustomersThatAreCurrentlyActive(DateTime statDate);
        List<AdwordCustomer> GetAwCustomersThatAreCurrentlyActiveByFlight(DateTime statDate);
        List<KpiDefinitionData> GetKpiDefinitionData(long customerId);
        AdwordsKpiStatsData GetAdwordsKpiStatsData(long customerId);
        AdwordsKpiStatsData GetAdwordsKpiStatsDataCampaignAndDate(long customerId, DateTime startDate, DateTime endDate);
        MoatKpiStatsData GetMoatKpiStatsData(long customerId);
        MoatKpiStatsData GetMoatKpiStatsDataCampaignAndDate(long customerId, DateTime startDate, DateTime endDate);
        NielsenKpiStatsData GetNielsenKpiStatsData(long customerId);
        NielsenKpiStatsData GetNielsenKpiStatsDataCampaignAndDate(long customerId, DateTime startDate, DateTime endDate);
        DoubleVerifyKpiStatsData GetDoubleVerifyKpiStatsData(long customerId);
        DoubleVerifyKpiStatsData GetDoubleVerifyKpiStatsDataCampaignAndDate(long customerId, DateTime startDate, DateTime endDate);
    }
}
