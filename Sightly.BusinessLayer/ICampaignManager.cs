﻿using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessLayer
{
    public interface ICampaignManager
    {
        List<Campaign> GetAllCampaigns();
        List<Campaign> GetAllMediaAgencyCampaigns();
        List<Campaign> GetAllCampaignsForAdWordsCustomerId(long adWordsCustomerId);
    }
}
