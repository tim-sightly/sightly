using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;
using CampaignPacingDto = Sightly.DAL.DTO.CampaignPacingData;
using PlacementAssociationDto = Sightly.DAL.DTO.PlacementAssociation;

namespace Sightly.BusinessLayer
{
    public class PacingManager : IPacingManager
    {
        private readonly IPacingStore _pacingStore;

        public PacingManager(IPacingStore pacingStore)
        {
            _pacingStore = pacingStore;
        }
        public List<CampaignPacingData> GetMediaAgencyPacingDataByDate(DateTime pacingDate)
        {
            List<CampaignPacingDto> campaignPacingDto = _pacingStore.GetMediaAgencyPacingDataByDate(pacingDate);

            return campaignPacingDto.Select(Mapper.Map).ToList();
        }

        public List<PlacementAssociation> getMediaAgencyPlacementByDate(DateTime pacingDate)
        {
            List<PlacementAssociationDto> placementAssociationDto = _pacingStore.GetMediaAgencyPlacementByDate(pacingDate);
            return placementAssociationDto.Select(Mapper.Map).ToList();
        }

        public int SaveProposedBudgetPacing(
            long customerId, 
            long campaignId, 
            decimal recomendedBudget, 
            float overPacingRate,
            DateTime startDate, 
            decimal totalBudget, 
            int daysRemaining, 
            Guid budgetGroupTimedBudgetId, 
            DateTime pacingDate,
            Guid userId)
        {
            return _pacingStore.SaveProposedBudgetPacing(
                    customerId, 
                    campaignId, 
                    recomendedBudget, 
                    overPacingRate,
                    startDate, 
                    totalBudget, 
                    daysRemaining, 
                    budgetGroupTimedBudgetId, 
                    pacingDate,
                    userId);
        }

        public List<CustomerPacingDate> GetLastPaceDateForAllCustomers()
        {
            return _pacingStore.GetLastPaceDateForAllCustomers().Select(cpd => Mapper.Map(cpd)).ToList();
        }

        public List<Manager> GetManagers()
        {
            return _pacingStore.GetManagers().Select(m => Mapper.Map(m)).ToList();
        }

    }
}