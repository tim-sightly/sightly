﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.Mapping;
using Sightly.DAL;
using DoubleVerifyKpiStatsData = Sightly.BusinessLayer.DomainObjects.DoubleVerifyKpiStatsData;
using DoubleVerifyKpiStatsDataDto = Sightly.DAL.DTO.DoubleVerifyKpiStatsData;

namespace Sightly.BusinessLayer
{
    public interface IDoubleVerifyManager
    {
        List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyAccount(Guid accountId);
        List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyAdvertiser(Guid advertiserId);
        List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyCampaign(Guid campaignId);
    }

    public class DoubleVerifyManager : IDoubleVerifyManager
    {
        private readonly IDoubleVerifyStore _doubleVerifyStore;

        public DoubleVerifyManager(IDoubleVerifyStore doubleVerifyStore)
        {
            _doubleVerifyStore = doubleVerifyStore;
        }

        public List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyAccount(Guid accountId)
        {
            List<DoubleVerifyKpiStatsDataDto> doubleVerifyKpiStatsDataDto = _doubleVerifyStore.GetDoubleVerifyKpiStatsDatabyAccount(accountId);
            return doubleVerifyKpiStatsDataDto.Select(Mapper.Map).ToList();
        }

        public List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyAdvertiser(Guid advertiserId)
        {
            List<DoubleVerifyKpiStatsDataDto> doubleVerifyKpiStatsDataDto = _doubleVerifyStore.GetDoubleVerifyKpiStatsDatabyAdvertiser(advertiserId);
            return doubleVerifyKpiStatsDataDto.Select(Mapper.Map).ToList();
        }

        public List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyCampaign(Guid campaignId)
        {
            List<DoubleVerifyKpiStatsDataDto> doubleVerifyKpiStatsDataDto = _doubleVerifyStore.GetDoubleVerifyKpiStatsDatabyCampaign(campaignId);
            return doubleVerifyKpiStatsDataDto.Select(Mapper.Map).ToList();
        }
    }
}