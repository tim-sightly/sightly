﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessLayer
{
    public interface IUserManager
    {
        User GetUserByEmail(string userEmail);
    }
}
