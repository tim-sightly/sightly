﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;

namespace Sightly.Youtube.Utility
{
    public class YoutubeServiceGetter : IYoutubeServiceGetter
    {
        public async Task<YouTubeService> GetYouTubeServiceAsync()
        {
            UserCredential credential;
            var assembly = this.GetType().Assembly;
//            string[] arr = this.GetType().Assembly.GetManifestResourceNames();
//            string[] arr2 = Assembly.GetExecutingAssembly().GetManifestResourceNames(); 
            var resName = "Sightly.Youtube.Utility.Permissions.json";
            
            using (var stream = assembly.GetManifestResourceStream(resName)) 
            {
                
                // google sux 
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,

                    new[] { YouTubeService.Scope.YoutubeUpload, YouTubeService.Scope.YoutubeForceSsl, YouTubeService.Scope.Youtubepartner },
                    "Sightly Videos",
                    CancellationToken.None
                );
                
            }

            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
            });

            return youtubeService;

        }


    }
}
