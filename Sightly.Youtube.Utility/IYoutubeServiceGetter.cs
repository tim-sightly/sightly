﻿using System.Threading.Tasks;
using Google.Apis.YouTube.v3;

namespace Sightly.Youtube.Utility
{
    internal interface IYoutubeServiceGetter
    {
        Task<YouTubeService> GetYouTubeServiceAsync();
    }
}