﻿using System.Collections.Generic;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;

namespace Sightly.Youtube.Utility
{
    public interface IYoutubeIdValidator
    {
        VideoListResponse GetYtInfoFromId(string part, string id, YouTubeService ytService);
        List<string> CreateIdChunksFromList(List<string> list);

        SearchListResponse GetYtInfoFromQuery(string part, string query, YouTubeService ytService,
            string maxResults = "", bool myVids = false);

        void InitWebDriver();
        void DisposeWebDriver();
        Dictionary<string, string> GetOriginalIds(List<string> dupeIds, bool webRunning);
        Dictionary<string, string> FindDuplicateIdsFromListOfIds(List<string> allIds, YouTubeService service, bool printStatus=false);
    }
}