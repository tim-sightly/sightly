# Read Me
the ffprofile folder must be in the same folder as the controller file so that it can be copied 
to the bin/Debug folder

### In The Case of a Password Change:
**You will have to create a new firefox profile!**

To create a new firefox profile: 
1. Win+R (run command) and type firefox.exe -P
2. Click "Create Profile" 
3. Follow the Profile Wizard:
	* when prompted to choose folder, place the folder in easy to reach directory.
	* enter "ffprofile" for the name of the profile
4. Start firefox with the newly created profile and sign in to youtube. Select the "Sightly Videos" account with 8 subscribers
5. Close firefox and copy the entire ffprofile folder to the folder of the relevant project
6. Once the profile is copied to another directory, delete the profile in the profile creator window.


