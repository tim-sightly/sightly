﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Web.UI;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using Newtonsoft.Json.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace Sightly.Youtube.Utility
{
    public class YoutubeIdValidator : IYoutubeIdValidator
    {
        
        private FirefoxDriver browser;
        private const string Username = "sightlyvideos@sightly.com";
        private const string YtUrl = "https://www.youtube.com/my_videos?o=U&sq=video_id%3A";

        //send id and part to youtube and get a response of videos
        public VideoListResponse GetYtInfoFromId(string part, string id, YouTubeService ytService)
        {    
            var parameters = new Dictionary<string, string>
            {
                { "part", part },
                { "id", id },
                
            };
            
            var  videosListByIdRequest = ytService.Videos.List(parameters["part"]);
            
            if (parameters.ContainsKey("id") && parameters["id"] != "")
            {
                videosListByIdRequest.Id = parameters["id"];
            }

           

            return videosListByIdRequest.Execute();
        }

        public SearchListResponse GetYtInfoFromQuery(string part, string query, YouTubeService ytService, string maxResults="", bool myVids = false)
        {
            var parameters = new Dictionary<string, string>
            {
                {"forMine", "true"},
                {"part", part},
                {"q", query},
                {"maxResults", maxResults},
                {"type", "video"}

            };

            var videosListByQueryRequest = ytService.Search.List(parameters["part"]);

            if (myVids)
            {
                videosListByQueryRequest.ForMine = parameters["forMine"] == "true" ? true : false;
                videosListByQueryRequest.Type = parameters["type"];
            }

            if (parameters.ContainsKey("q") && parameters["q"] != "")
            {
                videosListByQueryRequest.Q = parameters["q"];
            }
            if (parameters.ContainsKey("maxResults") && parameters["maxResults"] != "")
            {
                videosListByQueryRequest.MaxResults = long.Parse(parameters["maxResults"]);
            }

            return videosListByQueryRequest.Execute();



        }
        
        
        //expose things
        //in LIST of all ids, and getting a list of bad ids with the corresponding ids

        public Dictionary<string, string> FindDuplicateIdsFromListOfIds(List<string> allIds, YouTubeService service, bool printStatus=false)
        {
            var result = new Dictionary<string, string>();
            var duplicateIds = new List<string>();
            
            
            var idChunks = CreateIdChunksFromList(allIds);

            if (printStatus) { Console.WriteLine("Start Parsing..."); }
            foreach (var idChunk in idChunks)
            {
                if (printStatus) { Console.WriteLine("..."); }

                var vidListResponse = GetYtInfoFromId("snippet,status", idChunk, service);
                var videosItems = vidListResponse.Items;


                foreach (var video in videosItems)
                {
                    var isDuplicate = video.Status.RejectionReason == "duplicate" ;

                    if (isDuplicate)
                    {
                        duplicateIds.Add(video.Id);
                    }
                }

            }
            if (printStatus) { Console.WriteLine("Id Parsing Complete"); }

            var webRunning = false;
            if (duplicateIds.Count > 0)
            {
                if (printStatus) { Console.WriteLine("Init Webscraping"); }

                try
                {
                    InitWebDriver();
                    webRunning = true;
                }
                catch (Exception e)
                {
                    Debug.WriteLine("Cannot start WebDriver! " + e);
                }
                result = GetOriginalIds(duplicateIds, webRunning);
                if (webRunning)
                {
                    DisposeWebDriver();
                }

            }


            if (printStatus) { Console.WriteLine("Done!"); }

            return result;
            
           
        }

        //chop id list by 50 and put strings into list
        public List<string> CreateIdChunksFromList(List<string> list)
        {
            var result = new List<string>();
            
            var remainder = list.ToList().Count % 50;
            var loops = (int) (list.ToList().Count / 50);

            var index = 0;
            var block = "";

            if (list.Count > 50)
            {
                for (var l = 0; l < loops; l++)
                {
                    block = "";


                    for (var i = index; i < 50 + index; i++)
                    {
                        var item = list.ElementAt(i);
                        block += item + ",";
                    }

                    block = block.Substring(0, block.Length - 1);
                    result.Add(block);
                    index += 50;

                }

                block = "";
                for (var r = list.ToList().Count - 2 - remainder; r <= list.ToList().Count - 1; r++)
                {
                    var item = list.ElementAt(r);
                    block += item + ",";
                }
            }
            else
            {
                block = "";
                for (var r = 0; r < list.Count(); r++)
                {
                    var item = list.ElementAt(r);
                    block += item + ",";
                }
            }

            block = block.Substring(0, block.Length - 1);
            result.Add(block);

            return result;

        }

        public void InitWebDriver()
        {
           

            browser = new FirefoxDriver();
            browser.Manage().Window.Size = new System.Drawing.Size(480, 320);

            browser.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
        }

        public string RetrivePassword()
        {
//            var passw = "";
//            var assembly = this.GetType().Assembly;
//            var arr1 = this.GetType().Assembly.GetManifestResourceNames();
//            var arr2 = Assembly.GetExecutingAssembly().GetManifestResourceNames();
//            var resName = "Sightly.Youtube.Utility.password.json";
//
//            using (var stream = assembly.GetManifestResourceStream(resName))
//            {
//                using (var reader = new StreamReader(stream))
//                {
//
//                    var json = reader.ReadToEnd();
//                    var jObj = JObject.Parse(json);
//                    foreach (var item in jObj.Properties())
//                    {
//                        passw = item.Value.ToString();
//                    }
//                }
//            }

            return ConfigurationManager.AppSettings["YoutubePassword"];
         
        }
        public void DisposeWebDriver()
        {
            browser.Quit();
        }
        public void SignIn()
        {
            browser.Navigate().GoToUrl(YtUrl);
            var userNameIn = browser.FindElementByXPath("//*[@id=\"identifierId\"]");
            userNameIn.Click();
            userNameIn.SendKeys(Username);
            userNameIn.SendKeys(Keys.Enter);

            System.Threading.Thread.Sleep(2000);

            var passwIn = browser.FindElementByXPath("//*[@id=\"password\"]/div[1]/div/div[1]/input");
            passwIn.Click();
            var passw = RetrivePassword();
            passwIn.SendKeys(passw);
            passwIn.SendKeys(Keys.Enter);

            System.Threading.Thread.Sleep(3000);

            var radioBtn = browser.FindElementByXPath("/html/body/div[1]/div[3]/div/div/div/div/div/div[1]/div[2]/div/form/div[1]/ul/label[1]/li/span/span[1]/input");
            radioBtn.Click();
            radioBtn.SendKeys(Keys.Enter);

            System.Threading.Thread.Sleep(5000);

        }
        public Dictionary<string, string> GetOriginalIds(List<string> dupeIds, bool isWebRunning)
        {
            var result = new Dictionary<string, string>();
            
            
            
            
            //Tester Data:
            //Dupe:NtfgUNjoAcQ   Orig:LgHh9LqpAhs


            if (isWebRunning)
            {
                SignIn();
            }

            foreach (var dId in dupeIds)
            {
                var oId = dId;
                if (isWebRunning)
                {
                    browser.Navigate().GoToUrl(YtUrl + dId);
                    try
                    {
                        oId = browser
                            .FindElementByXPath(
                                "/html/body/div[2]/div[4]/div/div[5]/div/div[2]/div[9]/ol/li/div/div/div[2]/div[2]/div/span[4]/a")
                            .Text;
                        result.Add(dId, oId);
                    }
                    catch
                    {
                        Debug.WriteLine("Couldn't find real id....");
                        result.Add(dId, oId);
                    }

                }
                else
                {
                    result.Add(dId, oId);
                }
            }

            return result;
        }
    }
      
    
}