﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Autofac;
using Newtonsoft.Json;
using Sightly.Business.MediaAgency;
using Sightly.Business.StatsGather;
using Sightly.Models;
using Sightly.Test.Common;
using static System.Configuration.ConfigurationSettings;
using System.Configuration;

namespace Sightly.MediaAgency.ImportTool
{
    class Program
    {
        private static IContainer Container { get; set; }
        private static IDomainManager _domainManager;
        private static IGatherStats _gatherStats;

        static void Initialize()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _domainManager = Container.Resolve<IDomainManager>();
            _gatherStats = Container.Resolve<IGatherStats>();
        }

        static void Main(string[] args)
        {
            Console.WriteLine($"Started: {DateTime.Now}");

            Initialize();

            var mediaAgencyImprtList = File.ReadAllLines("..\\..\\CSV\\4225312901.csv")
                .Skip(1)
                .Select(MediaAgencyImport.FromCsv)
                .ToList();
            
            //figure out what the total budget is
            mediaAgencyImprtList.GroupBy(x => x.AwCustomerId).ToList().ForEach(ma =>
            {
                decimal totalBudget = mediaAgencyImprtList.Where(x => x.AwCustomerId == ma.Key)
                    .Sum(y => y.CampaignBudget);
                mediaAgencyImprtList.Where(x => x.AwCustomerId == ma.Key).ToList().ForEach(mai =>
                {
                    mai.TotalBudget = totalBudget;
                });
            });

            Console.WriteLine($"{DateTime.Now} - Starting Import");
            //import data and get a list of orders 
            var orderIdList = _domainManager.CreateTvMediaAgencyDomain(mediaAgencyImprtList, false);
            
            Console.WriteLine($"{DateTime.Now} - Import Complete");

            try
            {
                //Set the order To Live in the State Machine
                orderIdList.ForEach(order =>
                {

                    Console.WriteLine($"{DateTime.Now} - Setting Order {order.OrderId} to Live");
                    var orderRequest = new OrdersProposalRequest
                    {
                        OrderProposalId = order.OrderId,
                        UserId = Guid.Parse("779eaf81-f372-497b-b2d4-a5b900975170")
                    };
                    SendOrderForLiveSubmission(orderRequest);
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Console.WriteLine($"{DateTime.Now} - Order allready set to live.");
            }
            


            Console.WriteLine($"{DateTime.Now} - Starting Data gather");
            var today = DateTime.Now;
            mediaAgencyImprtList.GroupBy(x => x.AwCustomerId).ToList().ForEach(ma =>
            {

                var startDate = mediaAgencyImprtList.Where(x => x.AwCustomerId == ma.Key).Min(m => m.StartDate);
                var endDate = mediaAgencyImprtList.Where(x => x.AwCustomerId == ma.Key).Max(m => m.EndDate);
                if (endDate.Date >= today.Date) endDate = today;

                for (var countDate = startDate; countDate.Date <= endDate.Date; countDate = countDate.AddDays(1))
                {
                    Console.WriteLine($"{DateTime.Now} - Gather Stats for : {ma.Key} on {countDate}");
                    _gatherStats.Execute(ma.Key, countDate);
                }

            });




            Console.WriteLine($"Ended: {DateTime.Now}");
            Console.ReadKey();
        }

        public static void SendOrderForLiveSubmission(OrdersProposalRequest request)
        {
            try
            {
                var requestUrl = "api/transition/OrderSetToLive";
                SendPostRequest(requestUrl, request);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        protected static void SendPostRequest(string actionUrl, BaseRequest request)
        {
            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(5);
                var baseApiUrl = ConfigurationManager.AppSettings.Get("SightlyApi");
                var serviceId = ConfigurationManager.AppSettings.Get("ServiceId");
                var jwt = ConfigurationManager.AppSettings.Get("JwtPass");
                request.ServiceId = Guid.Parse(serviceId);

                client.BaseAddress = new Uri(baseApiUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", jwt);

                var jsonRequest = JsonConvert.SerializeObject(request);
                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var response = client.PostAsync(actionUrl, content).Result;

               // if (response.StatusCode == HttpStatusCode.BadRequest || response.StatusCode == HttpStatusCode.InternalServerError)
                if (response.StatusCode != HttpStatusCode.OK)
                {
                    var httpErrorString = response.Content.ReadAsStringAsync().Result;
                    var httpErrorTemplateObject = new { message = "" };
                    var httpErrorObject = JsonConvert.DeserializeAnonymousType(httpErrorString, httpErrorTemplateObject);
                    throw new HttpRequestException($"Failed POST Request @ {baseApiUrl + actionUrl}. {httpErrorObject.message}");
                }
            }

        }
    }



    public class BaseRequest
    {
        public Guid ServiceId { get; set; }
    }
    public class OrdersProposalRequest : BaseRequest
    {
        public Guid? OrderProposalId { get; set; }
        public string Notes { get; set; }
        public Guid? UserId { get; set; }
        public Guid? BudgetGroupXrefPlacementId { get; set; }
    }
}
