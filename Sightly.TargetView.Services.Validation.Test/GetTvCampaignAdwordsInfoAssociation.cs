﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.Interfaces;
using Sightly.TargetView.AdwordsValidation.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;

namespace Sightly.TargetView.Services.Validation.Test
{
    [TestClass]
    public class GetTvCampaignAdwordsInfoAssociation
    {
        private  DateTime today = DateTime.Now;
        private static IContainer Container { get; set; }
        private IAdwordsToTargetview _adwordsToTargetview;
        private IAdRepository _adRepository;
        private IAdValidation _adValidation;
        private IPlacementValidation _placementValidation;
        private IOrderRepository _orderRepository;
        private ITargetViewAdwordsValidation _targetViewAdwordsValidation;
        private ITargetViewRepository _targetViewRepository;
        private IConnectionStringStore _connectionStringStore;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _adwordsToTargetview = Container.Resolve<IAdwordsToTargetview>();
            _adRepository = Container.Resolve<IAdRepository>();
            _adValidation = Container.Resolve<IAdValidation>();
            _placementValidation = Container.Resolve<IPlacementValidation>();
            _orderRepository = Container.Resolve<IOrderRepository>();
            _targetViewAdwordsValidation = Container.Resolve<ITargetViewAdwordsValidation>();
            _targetViewRepository = Container.Resolve<ITargetViewRepository>();
            _connectionStringStore = Container.Resolve<IConnectionStringStore>();
        }

        [TestMethod]
        public void GetTvCampaignsToAssociateAndDoIt()
        {
            try
            {
                _adwordsToTargetview.GetCampaignsAdWordsInfosAndUpate(today);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
            


        }
    }
}
