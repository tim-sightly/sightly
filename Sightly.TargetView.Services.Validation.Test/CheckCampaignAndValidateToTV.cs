﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.Interfaces;
using Sightly.TargetView.AdwordsValidation.Interfaces;
using Sightly.TargetView.Services.Validation;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;

namespace Sightly.TargetView.Services.Validation.Test
{
    [TestClass]
    public class CheckCampaignAndValidateToTV
    {

        private const string customer = "9522014060";
        private static IContainer Container  { get;set; }

        private IAdRepository _adRepository;
        private IAdValidation _adValidation;
        private IAdwordsToTargetview _adwordsToTargetview;
        private IConnectionStringStore _connectionStringStore;
        private IPlacementValidation _placementValidation;
        private ITargetViewRepository _targetViewRepository;
        private ITargetViewAdwordsValidation _targetViewAdwordsValidation;
        private ITvAdDataStore _adDataStore;
        private ITvOrderDataStore _orderDataStore;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _connectionStringStore = Container.Resolve<IConnectionStringStore>();
            _targetViewAdwordsValidation = Container.Resolve<ITargetViewAdwordsValidation>();
            _adDataStore = Container.Resolve<ITvAdDataStore>();
            _orderDataStore = Container.Resolve<ITvOrderDataStore>();
             _adValidation = Container.Resolve<IAdValidation>();
            _adRepository = Container.Resolve<IAdRepository>();
            _targetViewRepository = Container.Resolve<ITargetViewRepository>();
            _adwordsToTargetview = Container.Resolve<IAdwordsToTargetview>();
            _placementValidation = Container.Resolve<IPlacementValidation>();
        }

        [TestMethod]
        public void PullAdWordsToTargetViewWithCustomerTest()
        {
            try
            {
                _adwordsToTargetview.CheckAndPushCampaignOrder(Convert.ToInt64(customer));
            }
            catch (Exception e)
            {
                Assert.Fail("Testing of the AdwordsToTargetview Service has failed : {0}", e.ToString());
            }
            
        }
    }
}
