﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.Interfaces;
using Sightly.TargetView.AdwordsValidation.Interfaces;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;

namespace Sightly.TargetView.Services.Validation.Test
{
    [TestClass]
    public class PlacementValidationTest
    {
        private const string customer = "5426470786";
        private static IContainer Container { get; set; }

        private IAdRepository _adRepository;
        private IAdValidation _adValidation;
        private IAdwordsToTargetview _adwordsToTargetview;
        private IConnectionStringStore _connectionStringStore;
        private IPlacementValidation _placementValidation;
        private ITargetViewRepository _targetViewRepository;
        private ITargetViewAdwordsValidation _targetViewAdwordsValidation;
        private ITvAdDataStore _adDataStore;
        private ITvOrderDataStore _orderDataStore;


        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _connectionStringStore = Container.Resolve<IConnectionStringStore>();
            _targetViewAdwordsValidation = Container.Resolve<ITargetViewAdwordsValidation>();
            _adDataStore = Container.Resolve<ITvAdDataStore>();
            _orderDataStore = Container.Resolve<ITvOrderDataStore>();
            _adValidation = Container.Resolve<IAdValidation>();
            _adRepository = Container.Resolve<IAdRepository>();
            _targetViewRepository = Container.Resolve<ITargetViewRepository>();
            _adwordsToTargetview = Container.Resolve<IAdwordsToTargetview>();
            _placementValidation = Container.Resolve<IPlacementValidation>();
        }
        [TestMethod]
        public void GetPlacementDataFromAdwords()
        {
            try
            {
                _adwordsToTargetview.AssignPlacementWithAwCustomerId(Convert.ToInt64(customer), "PlacementValidation");
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
    }
}
