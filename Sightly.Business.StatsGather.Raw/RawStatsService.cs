using System;
using System.Collections.Generic;
using System.Data.Services.Client;
using Sightly.Adwords.Utility.Interface;
using Sightly.DoubleVerify.Utility;
using Sightly.Moat.Utility;
using Sightly.Models;
using Sightly.Nielsen.Utility;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.StatsGather.Raw
{
    public class RawStatsService : IRawStatsService
    {
        private readonly IAdDevicePerformanceFree _adDevicePerformanceFree;
        private readonly IAdDevicePerformance _adDevicePerformance;
        private readonly IAdGroupPerformanceFree _adGroupPerformanceFree;
        private readonly IAgeRangePerformanceFree _ageRangePerformance;
        private readonly IDoubleVerifyDataService _doubleVerifyService;
        private readonly IGenderPerformanceFree _genderPerformance;
        private readonly IGeoPerformanceFree _geoPerformanceFree;
        private readonly IMoatDataService _moatService;
        private readonly INielsenDataService _nielsenService;
        private readonly IRawStatsRepository _rawStatsRepository;

        public RawStatsService(IAdDevicePerformanceFree adDevicePerformanceFree, IAdDevicePerformance adDevicePerformance, IAdGroupPerformanceFree adGroupPerformanceFree,
            IAgeRangePerformanceFree ageRangePerformance, IDoubleVerifyDataService doubleVerifyService, IGenderPerformanceFree genderPerformance, 
            IGeoPerformanceFree geoPerformance, IMoatDataService moatService, INielsenDataService nielsenService, IRawStatsRepository rawStatsRepository)
        {
            _adDevicePerformanceFree = adDevicePerformanceFree;
            _adDevicePerformance = adDevicePerformance;
            _adGroupPerformanceFree = adGroupPerformanceFree;
            _ageRangePerformance = ageRangePerformance;
            _doubleVerifyService = doubleVerifyService;
            _genderPerformance = genderPerformance;
            _geoPerformanceFree = geoPerformance;
            _moatService = moatService;
            _nielsenService = nielsenService;
            _rawStatsRepository = rawStatsRepository;
        }

        public RawSingleDayFreeStatsData GatherCustomerStatsPerDay(long customerId, DateTime statDate)
        {
            var singleDayStats =
                new RawSingleDayFreeStatsData
                {
                    AdDeviceStats = GetAndSaveAdDeviceStats(customerId, statDate),
                    AgeRangeStats = GetAndSaveAgeRangeStats(customerId, statDate),
                    GenderStats = GetAndSaveGenderStats(customerId, statDate)
                };

            return singleDayStats;
        }

        public List<AdDevicePerformanceFreeData> GetAndSaveFreeAdDeviceStats(long customerId, DateTime statDate)
        {
            var adDeviceStats = _adDevicePerformanceFree.GetAdDevicePerformanceFreeData(customerId.ToString(), statDate);

            adDeviceStats.ForEach(stat =>
            {
                _rawStatsRepository.InsertRawAdDeviceStat(stat);
            });

            return adDeviceStats;
        }

        public List<AdDevicePerformanceData> GetAndSaveAdDeviceStats(long customerId, DateTime statDate)
        {
            var adDeviceStats = _adDevicePerformance.GetAdDevicePerformanceData(customerId.ToString(), statDate);

            adDeviceStats.ForEach(stat =>
            {
                stat.CustomerId = customerId;
                _rawStatsRepository.InsertRawAdDeviceStat(stat);
            });
            return adDeviceStats;
        }

        public List<AdGroupPerformanceFreeData> GetAndSaveAdGroupStats(long customerId, DateTime statDate)
        {
            var adGroupStats = _adGroupPerformanceFree.GetAdGroupPerformanceFreeData(customerId.ToString(), statDate);
            adGroupStats.ForEach(stat =>
            {
                stat.CustomerId = customerId;
                _rawStatsRepository.InsertRawAdGroupStat(stat);
            });
            return adGroupStats;
        }

        public List<AgeRangePerformanceFreeData> GetAndSaveAgeRangeStats(long customerId, DateTime statDate)
        {
            var ageRangeStats = _ageRangePerformance.GetAgeRangePerformanceFreeData(customerId.ToString(), statDate);

            ageRangeStats.ForEach(stat =>
            {
                stat.CustomerId = customerId;
                _rawStatsRepository.InsertRawAgeRangeStat(stat);
            });

            return ageRangeStats;
        }

        public List<GenderPerformanceFreeData> GetAndSaveGenderStats(long customerId, DateTime statDate)
        {
            var genderStats = _genderPerformance.GetGenderPerformanceFreeData(customerId.ToString(), statDate);

            genderStats.ForEach(stat =>
            {
                stat.CustomerId = customerId;
                _rawStatsRepository.InsertRawGenderStat(stat);
            });

            return genderStats;
        }

        public List<GeoPerformanceFreeData> GetAndSaveGeoStats(long customerId, DateTime statDate)
        {
            var geoStats = _geoPerformanceFree.GetGeoPerformanceFreeData(customerId.ToString(), statDate);

            geoStats.ForEach(stat =>
            {
                stat.CustomerId = customerId;
                _rawStatsRepository.InsertRawGeoStat(stat);
            });
            return geoStats;
        }

        public List<MoatDailyStatData> GetAndSaveMoatStats(DateTime statDate)
        {
            var moatStats = _moatService.GetDailyMoatStats(statDate).Result;
            
            moatStats.ForEach(dailyStat =>
            {
                _rawStatsRepository.InsertRawMoatStats(dailyStat);
            });

            return moatStats;
        }


        public NielsenCombinedData GetAndSaveNielsenStats(List<NielsenCampaignReference> campaignReferences, DateTime statDate)
        {
            //if (nielsenCampaigns.Count < 1)
            //    throw new DataServiceRequestException("No Campaigns were identified");

            //var campaignsToProcess = new List<NielsenCampaignAvailability>();
            
            //nielsenCampaigns.ForEach(campaign =>
            //{
            //    campaignsToProcess.Add(new NielsenCampaignAvailability(campaign.ToString()));
            //});

            var nielsenStats = _nielsenService.GetNielsenData(campaignReferences, statDate).Result;

            nielsenStats.NielsenStatsExposure.ForEach(stat =>
            {
                _rawStatsRepository.InsertNielsenStatsExposure(stat);
            });
        
            return nielsenStats;
        }

        public NielsenCombinedData GetAndSaveNielsenStatsByCustomerId(long customerId, DateTime statDate)
        {
            var nielsenStats = _nielsenService.GetNielsenData(customerId, statDate).Result;

            nielsenStats.NielsenStatsExposure.ForEach(stat =>
            {
                _rawStatsRepository.InsertNielsenStatsExposure(stat);
            });

            return nielsenStats;
        }

        public List<NielsenCampaignReference> VerifyNielsenCampaignStructure(DateTime date)
        {
            var nielsenCampaigns = _nielsenService.GetNielsenCampaignReferenceData(date).Result;

            nielsenCampaigns.ForEach(camp =>
            {
                _rawStatsRepository.CreateOrUpdateNielsenCampaignAndReference(camp);
            });

            return nielsenCampaigns;
        }

        public List<DoubleVerifyStats> SaveOffDoubleVerify(List<DoubleVerifyImport> importData)
        {
            var dbStats = _doubleVerifyService.GetDoubleVerifyStatsFromImport(importData);

            dbStats.ForEach(stat =>
            {
                _rawStatsRepository.InsertDoubleVerifyStats(stat);
            });

            return dbStats;
        }

        public void RunNielsenDarUpdate(List<long> nielsenCiDs, DateTime yesterday)
        {
            if(nielsenCiDs.Count < 1)
                throw new DataServiceRequestException("No CustomerId's were identified");
            nielsenCiDs.ForEach(cid =>
            {
                var dailyNielsenDarStat = _rawStatsRepository.GetDailyNielsenDarStat(cid, yesterday);
                if(dailyNielsenDarStat != null)
                    _rawStatsRepository.InsertDailyNielsenDarStat(dailyNielsenDarStat);
            });
        }
    }
}