﻿using System.Collections.Generic;
using System.Linq;
using Sightly.Models;

namespace Sightly.Business.StatsGather.Raw
{
    public class RawStatsAssociator : IRawStatsAssociator
    {
        public List<TargetViewAdwordsAssociatedData> AssociateRawCampaigns(List<TargetViewAdwordsAssociatedData> tvAwData, List<CampaignData> rawAdDeviceStats)
        {
            var newCampaigns = rawAdDeviceStats.Where(camp =>
                tvAwData.All(camp2 =>
                    camp2.AdwordsCampaignId != camp.CampaignId)).ToList();
            if (newCampaigns.Any())
            {
               tvAwData.Where(tv => tv.AdwordsCampaignId == null).ToList().ForEach(tvRecord =>
               {
                   newCampaigns.ForEach(newCamp =>
                   {
                       if (!newCamp.CampaignName.Contains(tvRecord.BudgetGroupId.ToString())) return;
                       tvRecord.AdwordsCampaignId = newCamp.CampaignId;
                       tvRecord.AdwordsCampaignName = newCamp.CampaignName;
                       tvRecord.UpdateCampaignId = true;
                   });
               });
            }

            return tvAwData;

        }

        public List<TargetViewAdwordsAssociatedData>  AssociateRawAds(List<TargetViewAdwordsAssociatedData> tvAwData, List<AdData> rawAdDeviceStats)
        {
            var newAds = rawAdDeviceStats.Where(ad =>
                tvAwData.All(ad2 =>
                    ad2.AdwordsAdId != ad.AdId)).ToList();
            if (newAds.Any())
            {
                tvAwData.Where(tv => tv.AdwordsAdId == null).ToList().ForEach(tvRecord =>
                {
                    newAds.ForEach(newAd =>
                    {
                        if (!newAd.AdName.Contains(tvRecord.AdId.ToString())) return;
                        tvRecord.AdwordsAdId = newAd.AdId;
                        tvRecord.AdwordsAdName = newAd.AdName;
                        tvRecord.UpdateAdId = true;
                    });
                });
            }
            return tvAwData;
        }
    }
}