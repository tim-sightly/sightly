﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.StatsGather.Raw
{
    public interface IRawStatsService
    {
        RawSingleDayFreeStatsData GatherCustomerStatsPerDay(long customerId, DateTime statDate);

        List<AdDevicePerformanceFreeData> GetAndSaveFreeAdDeviceStats(long customerId, DateTime statDate);

        List<AdDevicePerformanceData> GetAndSaveAdDeviceStats(long customerId, DateTime statDate);

        List<AdGroupPerformanceFreeData> GetAndSaveAdGroupStats(long customerId, DateTime statDate);
        List<AgeRangePerformanceFreeData> GetAndSaveAgeRangeStats(long customerId, DateTime statDate);

        List<GenderPerformanceFreeData> GetAndSaveGenderStats(long customerId, DateTime statDate);

        List<GeoPerformanceFreeData> GetAndSaveGeoStats(long customerId, DateTime statDate);

        List<MoatDailyStatData> GetAndSaveMoatStats(DateTime statDate);

        NielsenCombinedData GetAndSaveNielsenStats(List<NielsenCampaignReference> campaignReferences, DateTime statDate);
        NielsenCombinedData GetAndSaveNielsenStatsByCustomerId(long customerId, DateTime statDate);
        void RunNielsenDarUpdate(List<long> nielsenCiDs, DateTime yesterday);
        List<NielsenCampaignReference> VerifyNielsenCampaignStructure(DateTime date);

        List<DoubleVerifyStats> SaveOffDoubleVerify(List<DoubleVerifyImport> importData);
    }
}
