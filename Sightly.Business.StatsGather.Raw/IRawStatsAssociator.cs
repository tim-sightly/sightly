﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.StatsGather.Raw
{
    public interface IRawStatsAssociator
    {
        List<TargetViewAdwordsAssociatedData> AssociateRawCampaigns(List<TargetViewAdwordsAssociatedData> tvAwData, List<CampaignData> rawAdDeviceStats);

        List<TargetViewAdwordsAssociatedData> AssociateRawAds(List<TargetViewAdwordsAssociatedData> tvAwData, List<AdData> rawAdDeviceStats);
    }
}
