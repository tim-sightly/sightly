﻿using System;
using System.Data.SqlTypes;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Reporting.Common;
using Reporting.DAL;
using Reporting.DAL.DTO;
using Sightly.ReportGeneration.Generators;
using Sightly.ReportGeneration.Generators.Interface;
using Sightly.ReportGeneration.Models;

namespace Reporting.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("reports/pub")]
    public class PubController : ApiController
    {
        private readonly IReportStore _reportStore;
        private readonly IAccountStore _accountStore;

        public PubController()
        {
            _reportStore = new ReportStore();
            _accountStore = new AccountStore();
        }


        [HttpGet]
        [Route("pubreport")]
        public HttpResponseMessage PubReport(string customerId)
        {
            try
            {
                var prCustomerId = long.Parse(customerId);
                var PubReportByCustomerId = new CampaignPubReportCollection();
                var PubReportList = _reportStore.GetPubReports(prCustomerId);
                PubReportByCustomerId.CampaignPubReports.AddRange(PubReportList);

                IExcelReport pubReportExcel = new PubReportExportExcel(PubReportByCustomerId);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(pubReportExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"PubReport-{customerId}.xlsx"
                };
                return result;
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPost]
        [Route("pubreportByDateRange")]
        public HttpResponseMessage PubReport(CustomerDateRangeRequest customerDateRange)
        {
            try
            {
                var PubReportByCustomerId = new CampaignPubReportCollection();
                var PubReportList = _reportStore.GetPubReportsByDateRange(customerDateRange.Customers.First(), customerDateRange.StartDate.Value, customerDateRange.EndDate.Value);
                PubReportByCustomerId.CampaignPubReports.AddRange(PubReportList);

                IExcelReport pubReportExcel = new PubReportExportExcel(PubReportByCustomerId);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(pubReportExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"PubReport-{customerDateRange.Customers.First()} from {customerDateRange.StartDate.Value.ToShortDateString()} to {customerDateRange.EndDate.Value.ToShortDateString()}.xlsx"
                };
                return result;
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("customerPubReport")]
        public HttpResponseMessage CustomerPubReport(CustomerDateRangeRequest customerDateRange)
        {
            try
            {
                bool IsSummaryReport = customerDateRange.IsSummary;

                DateTime startDate = customerDateRange.StartDate ?? (DateTime)SqlDateTime.MinValue;
                DateTime endDate = customerDateRange.EndDate ?? (DateTime)SqlDateTime.MaxValue;

                var pubReportByCustomerCollection = new CampaignPubReportCollection();
                var pubReportData = _reportStore.GetAggregatedPubReportData(
                                                    customerDateRange.Customers,
                                                    startDate, 
                                                    endDate, 
                                                    customerDateRange.IsSummary);
                pubReportByCustomerCollection.CampaignPubReports.AddRange(pubReportData);

                if (IsSummaryReport)
                {
                    IExcelReport pubReportExcel = new PubReportSummaryExportExcel(pubReportByCustomerCollection);

                    var result = Request.CreateResponse(HttpStatusCode.OK);
                    result.Content = new StreamContent(pubReportExcel.ToStream());
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = $"PubReport-{customerDateRange.Customers.First()} from {startDate.ToShortDateString()} to {endDate.ToShortDateString()}.xlsx"
                    };

                    return result;
                }
                else
                {
                    IExcelReport pubReportExcel = new PubReportExportExcel(pubReportByCustomerCollection);

                    var result = Request.CreateResponse(HttpStatusCode.OK);
                    result.Content = new StreamContent(pubReportExcel.ToStream());
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = $"PubReport-{customerDateRange.Customers.First()} from {startDate.ToShortDateString()} to {endDate.ToShortDateString()}.xlsx"
                    };

                    return result;
                }
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }

        }

        [HttpPost]
        [Route("internalPubReport")]
        public HttpResponseMessage InternalPubReport(CustomerDateRangeRequest customerDateRange)
        {
            try
            {
                bool IsSummaryReport = customerDateRange.IsSummary;

                DateTime startDate = customerDateRange.StartDate ?? (DateTime)SqlDateTime.MinValue;
                DateTime endDate = customerDateRange.EndDate ?? (DateTime)SqlDateTime.MaxValue;

                var pubReportByCustomerCollection = new CampaignPubReportCollection();
                var pubReportData = _reportStore.GetInternalAggregatedPubReportData(
                                                    customerDateRange.Customers,
                                                    startDate,
                                                    endDate,
                                                    customerDateRange.IsSummary);
                pubReportByCustomerCollection.CampaignPubReports.AddRange(pubReportData);

                if (IsSummaryReport)
                {
                    IExcelReport pubReportExcel = new PubReportInternalSummaryExportExcel(pubReportByCustomerCollection);

                    var result = Request.CreateResponse(HttpStatusCode.OK);
                    result.Content = new StreamContent(pubReportExcel.ToStream());
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = $"PubReport-{customerDateRange.Customers.First()} from {startDate.ToShortDateString()} to {endDate.ToShortDateString()}.xlsx"
                    };

                    return result;
                }
                else
                {
                    IExcelReport pubReportExcel = new PubReportInternalExportExcel(pubReportByCustomerCollection);

                    var result = Request.CreateResponse(HttpStatusCode.OK);
                    result.Content = new StreamContent(pubReportExcel.ToStream());
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = $"PubReport-{customerDateRange.Customers.First()} from {startDate.ToShortDateString()} to {endDate.ToShortDateString()}.xlsx"
                    };

                    return result;
                }
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }

        }
    }
}
