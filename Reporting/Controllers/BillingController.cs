﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.CompilerServices;
using System.Web.Http;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using Reporting.Common;
using Reporting.DAL;
using Reporting.DAL.DTO;
using Sightly.ReportGeneration.Generators;

namespace Reporting.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("reports/billing")]
    public class BillingController : ApiController
    {
        private readonly IReportStore _reportStore;
        private readonly IAccountStore _accountStore;

        public BillingController()
        {
            _reportStore = new ReportStore();
            _accountStore = new AccountStore();
        }

        [HttpGet]
        [Route("localResellers")]
        public HttpResponseMessage LocalResellersBillingReport(string lastDayOfReport)
        {
            try
            {
                var lastDay = DateTime.Parse(lastDayOfReport);
                List<BillingData> resellersData = _reportStore.GetResellersBillingReport(lastDay);

                //split the data into ADI and others and generate both the reports then zip together

                //ADI Data
                var adiData = resellersData.Where(x => x.TopParent == "Advance Local").ToList();
                ResellersBillingReportExcel report = new ResellersBillingReportExcel(adiData);

                //Others 
                var othersData = resellersData.Where(x => x.TopParent != "Advance Local").ToList();
                ResellersBillingReportExcel otherReport = new ResellersBillingReportExcel(othersData);


                var ms = new MemoryStream();
                var zipOutputStream = new ZipOutputStream(ms);
                zipOutputStream.SetLevel(3);

                var adiEntry = new ZipEntry($"BillingReport-ADI:{lastDay.ToString("MMMM", CultureInfo.InvariantCulture)}.xlsx");
                adiEntry.DateTime = DateTime.Now;
                zipOutputStream.PutNextEntry(adiEntry);

                MemoryStream adiFileStream = report.ToStream();
                StreamUtils.Copy(adiFileStream, zipOutputStream, new byte[4096]);
                adiFileStream.Close();
                zipOutputStream.CloseEntry();

                var otherEntry = new ZipEntry($"BillingReport-Others:{lastDay.ToString("MMMM", CultureInfo.InvariantCulture)}.xlsx");
                otherEntry.DateTime = DateTime.Now;
                zipOutputStream.PutNextEntry(otherEntry);

                MemoryStream otherFileStream = otherReport.ToStream();
                StreamUtils.Copy(otherFileStream, zipOutputStream, new byte[4096]);
                otherFileStream.Close();
                zipOutputStream.CloseEntry();



                zipOutputStream.IsStreamOwner = false;    // False stops the Close also Closing the underlying stream.
                zipOutputStream.Close();          // Must finish the ZipOutputStream before using outputMemStream.

                ms.Position = 0;
                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(ms);
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"BillingReports-{Guid.NewGuid()}.zip"
                };
                return result;



            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
    }
}
