﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Newtonsoft.Json;
using Reporting.Common;
using Reporting.DAL;
using Reporting.DAL.DTO;
using Reporting.DAL.DTO.NewTargetview;
using Sightly.ReportGeneration.Generators;
using Sightly.ReportGeneration.Generators.Interface;
using Sightly.ReportGeneration.Models;

namespace Reporting.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("reports/excel")]
    public partial class ExcelController : ApiController
    {
        private readonly IReportStore _reportStore;
        private readonly IAccountStore _accountStore;

        public ExcelController()
        {
            _reportStore = new ReportStore();
            _accountStore = new AccountStore();
        }

        
        [HttpGet]
        [Route("performance")]
        public HttpResponseMessage PerformanceReport(string campaignId)
        {
            try
            {                
                var campaignGuid = Guid.Parse(campaignId);
                PerformanceReport performanceReport = _reportStore.GetPerformanceReport(campaignGuid);
                PerformanceReportExcel performanceReportExcel = new PerformanceReportExcel(performanceReport);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(performanceReportExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"PerformanceReport-{performanceReportExcel.CampaignName}.xlsx"
                };

                return result;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }




        [HttpGet]
        [Route("performance_view")]
        public HttpResponseMessage PerformanceViewReport(string customerId)
        {
            try
            {
                //var campaignGuid = Guid.Parse(campaignId);
                var customerLong = long.Parse(customerId);
                DateTime startDate = (DateTime)SqlDateTime.MinValue;
                DateTime endDate = (DateTime)SqlDateTime.MaxValue;
                PerformanceReport performanceReport = _reportStore.GetPerformanceReport(customerLong, startDate, endDate);
                Performance_ViewExcel performanceReportExcel = new Performance_ViewExcel(performanceReport);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(performanceReportExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"PerformanceViewReport-{performanceReportExcel.CampaignName}.xlsx"
                };

                return result;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("performanceViewByDateRange")]
        public HttpResponseMessage PerformanceViewReport(CustomerDateRangeRequest customerDateRange)
        {
            try
            {
                PerformanceReport performanceReport = _reportStore.GetPerformanceReport(customerDateRange.Customers.First(), customerDateRange.StartDate.Value, customerDateRange.EndDate.Value);
                Performance_ViewExcel performanceReportExcel = new Performance_ViewExcel(performanceReport);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(performanceReportExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"PerformanceViewReport-{performanceReportExcel.CampaignName}.xlsx"
                };

                return result;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }


        [HttpGet]
        [Route("performance_1kImpression")]
        public HttpResponseMessage Performance1KImpressionReport(string customerId)
        {
            try
            {
                //var campaignGuid = Guid.Parse(campaignId);
                var customerLong = long.Parse(customerId);
                DateTime startDate = (DateTime)SqlDateTime.MinValue;
                DateTime endDate = (DateTime)SqlDateTime.MaxValue;
                PerformanceReport performanceReport = _reportStore.GetPerformanceReport(customerLong, startDate, endDate);
                Performance_1KImpressionExcel performanceReportExcel = new Performance_1KImpressionExcel(performanceReport);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(performanceReportExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"PerformanceViewReport-{performanceReportExcel.CampaignName}.xlsx"
                };

                return result;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("performance_1kImpressionByDateRange")]
        public HttpResponseMessage Performance1KImpressionReport(CustomerDateRangeRequest customerDateRange)
        {
            try
            {
                PerformanceReport performanceReport = _reportStore.GetPerformanceReport(customerDateRange.Customers.First(), customerDateRange.StartDate.Value, customerDateRange.EndDate.Value);
                Performance_1KImpressionExcel performanceReportExcel = new Performance_1KImpressionExcel(performanceReport);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(performanceReportExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"PerformanceViewReport-{performanceReportExcel.CampaignName}.xlsx"
                };

                return result;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("MediaBriefReport")]
        public HttpResponseMessage MediaBriefReport(string orderId)
        {
            try
            {
                var orderGuid = Guid.Parse(orderId);
                MediaBrief mediaBrief = _reportStore.GetMediaBrief(orderGuid);
                IExcelReport mediaBriefExcel = new MediaBriefExcel(mediaBrief);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(mediaBriefExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"MediaBrief-{mediaBrief.OrderData.OrderName}.xlsx"
                };
                return result;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("NewMediaBriefReport")]
        public HttpResponseMessage NewMediaBriefReport(string orderId)
        {
            try
            {
                var orderGuid = Guid.Parse(orderId);
                MediaBrief mediaBrief = _reportStore.GetNewMediaBrief(orderGuid);
                IExcelReport mediaBriefExcel = new ExtendedMediaBriefExcel(mediaBrief);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(mediaBriefExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"MediaBrief-{mediaBrief.OrderData.OrderName}.xlsx"
                };
                return result;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("ProposedNewMediaBrief")]
        public HttpResponseMessage ProposedNewMediaBrief(string orderId)
        {
            try
            {
                var gOrderId = Guid.Parse(orderId);
                var jsonData = _reportStore.GetJsonOfSavedOrder(gOrderId);

                var order = JsonConvert.DeserializeObject<Order>(jsonData);
                MediaBrief mb = MakeMediaBriefFromOrder(order);
                IExcelReport mediaBriefExcel = new ExtendedMediaBriefExcel(mb);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(mediaBriefExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"MediaBrief-{mb.OrderData.OrderName}.xlsx"
                };
                return result;
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }

        }

        [HttpGet]
        [Route("mediaagencyplacement")]
        public HttpResponseMessage MediaAgencyPlacementReport(string customerId)
        {
            try
            {
                var lCustomerId = long.Parse(customerId);
                var campaignPlacementReport = new CampaignPlacementReport();
                var campaignPlacementList = _reportStore.GetMediaAgencyCampaignPlacements(lCustomerId);
                campaignPlacementReport.CampaignName = _accountStore.GetCampaignName(lCustomerId);
                campaignPlacementReport.MediaAgencyCampaignPlacements.AddRange(campaignPlacementList);

                IExcelReport maPlacementExcel = new MediaAgencyCampaignPlacementExportExcel(campaignPlacementReport);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(maPlacementExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"MediaAgencyPlacement-{campaignPlacementReport.CampaignName}.xlsx"
                };

                return result;
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }
        [HttpGet]
        [Route("mediaagencyorderplacement")]
        public HttpResponseMessage MediaAgencyOrderPlacementReport(string customerId)
        {
            try
            {
                var lCustomerId = long.Parse(customerId);
                var orderPlacementReport = new OrderPlacementReport();
                var orderPlacementList = _reportStore.GetMediaAgencyOrderPlacements(lCustomerId);
                orderPlacementReport.OrderName = orderPlacementList.Select(o => o.OrderName).FirstOrDefault();
                orderPlacementReport.MediaAgencyOrderPlacements.AddRange(orderPlacementList);
                
                IExcelReport maPlacementExcel = new MediaAgencyOrderPlacementExportExcel(orderPlacementReport);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(maPlacementExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"MediaAgencyPlacement-{orderPlacementReport.OrderName}.xlsx"
                };

                return result;
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("ResellerUploadTemplateByOrder")]
        public HttpResponseMessage ResellerUploadTemplateByOrder(string orderId)
        {
            try
            {

                var orderGuid = Guid.Parse(orderId);
                var orderPlacementReport = new OrderPlacementReport();
                var orderPlacementList = new List<MediaAgencyOrderPlacement>();
                var orderTemplate = _reportStore.GetResellerCampaignUploaderData(orderGuid);
                orderPlacementList.Add(new MediaAgencyOrderPlacement
                {
                    AdwordsCampaignName = orderTemplate.BudgetGroupName,
                    CampaignBudget = orderTemplate.CampaignBudget,
                    StartDate = orderTemplate.StartDate,
                    EndDate = orderTemplate.EndDate,
                    Margin = orderTemplate.SightlyMargin,
                    OrderName = orderTemplate.TargetviewOrderName,
                    PlacementName = orderTemplate.BudgetGroupName,
                    PlacementValue = orderTemplate.BudgetGroupId.ToString(),
                    TargetViewCampaignName = orderTemplate.TargetviewCampaignName
                });
                orderPlacementReport.OrderName = orderPlacementList.Select(o => o.OrderName).FirstOrDefault();
                orderPlacementReport.MediaAgencyOrderPlacements.AddRange(orderPlacementList);

                IExcelReport maPlacementExcel = new MediaAgencyOrderPlacementExportExcel(orderPlacementReport);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(maPlacementExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"ResellerUploadTemplate-{orderPlacementReport.OrderName}-{DateTime.Now.Date}.xlsx"
                };

                return result;
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        

        [HttpGet]
        [Route("pubreport")]
        public HttpResponseMessage PubReport(string customerId)
        {
            try
            {
                var prCustomerId = long.Parse(customerId);
                var PubReportByCustomerId = new CampaignPubReportCollection();
                var PubReportList = _reportStore.GetPubReports(prCustomerId);
                PubReportByCustomerId.CampaignPubReports.AddRange(PubReportList);

                IExcelReport pubReportExcel = new PubReportExportExcel(PubReportByCustomerId);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(pubReportExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"PubReport-{customerId}.xlsx"
                };
                return result;
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }


        [HttpPost]
        [Route("pubreportByDateRange")]
        public HttpResponseMessage PubReport(CustomerDateRangeRequest customerDateRange)//string customerId, string start, string end)
        {
            try
            {
                //var prCustomerId = long.Parse(customerId);
                //var startDate = DateTime.Parse(start);
                //var endDate = DateTime.Parse(end);
                var PubReportByCustomerId = new CampaignPubReportCollection();
                var PubReportList = _reportStore.GetPubReportsByDateRange(customerDateRange.Customers.First(), customerDateRange.StartDate.Value, customerDateRange.EndDate.Value);
                PubReportByCustomerId.CampaignPubReports.AddRange(PubReportList);

                IExcelReport pubReportExcel = new PubReportExportExcel(PubReportByCustomerId);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(pubReportExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"PubReport-{customerDateRange.Customers.First()} from {customerDateRange.StartDate.Value.ToShortDateString()} to {customerDateRange.EndDate.Value.ToShortDateString()}.xlsx"
                };
                return result;
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [HttpGet]
        [Route("getNewOrderTemplate")]
        public HttpResponseMessage GetNewOrderTemplate(string orderName)
        {
            try
            {
            
                NewOrderTemplate template = new NewOrderTemplate(orderName);
                template.Build();

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(template.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"NewOrderTemplate-{orderName}.xlsx"
                };

                return result;
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }





        //        This was for the back part of a Lat/Long generator.

        [HttpPost]
        [Route("latlongreport")]

        public HttpResponseMessage LatLongReport(LatLongReportRequest latLongReportRequest)
        {
            try
            {
                IExcelReport longLatExcel = new LatLongExportExcel(latLongReportRequest);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(longLatExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"LatLongReport.xlsx"
                };
                return result;
            }

            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        private MediaBrief MakeMediaBriefFromOrder(Order order)
        {
            var mediaBrief = new MediaBrief();
            mediaBrief.OrderData = _reportStore.GetOrderDataFromOrderInfo(order.Info);
            mediaBrief.GenderAgeList = _reportStore.GetGenderAgeListFromOrderGenders(order.Audience.Genders, order.Audience.AgeRanges);
            mediaBrief.LocationList = MapLocationListFromOrderLocations(order.Geo.SelectedServiceAreas);
            mediaBrief.BudgetData = _reportStore.GetBudgetDataFromOrder(order.Info);
            mediaBrief.KeywordUrl = new KeywordUrl
            {
                KeyWords = order.Audience.KeyWords,
                CompetitorsUrls = order.Audience.CompetitorUrls,
                TargetingNotes = order.Audience.Notes
            };
            mediaBrief.AdList = MapAdsFromOrderAds(order.Ads);
            mediaBrief.HouseholdIncomeList = _reportStore.GetHHIFromOrderHHI(order.Audience.HouseholdIncomes);

            return mediaBrief;
        }

        private List<LocationData> MapLocationListFromOrderLocations(List<GeoData> serviceAreas)
        {
            var locationDataList = new List<LocationData>();
            serviceAreas.ForEach(sa =>
            {
                locationDataList.Add(new LocationData
                {
                    GeographyName = sa.DmaName,
                    LocationName = sa.DmaName
                });
            });

            return locationDataList;
        }

        private List<AdData> MapAdsFromOrderAds(List<Ad> orderAds)
        {
            var adDataList = new List<AdData>();
            orderAds.ForEach(oa =>
            {
                adDataList.Add(new AdData
                {
                    AdName = oa.AdName,
                    CompanionBannerUrl = oa.CampanionBannerUrl,
                    DestinationUrl = oa.LandingUrl,
                    DisplayUrl = oa.ClickableUrl,
                    VideoAdAsset = oa.AdName,
                    VideoUrl = oa.YoutubeUrl,
                    Paused = oa.Pause
                });
            });

            return adDataList;
        }
    }
}
