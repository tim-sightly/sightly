﻿using System;
using System.Data.SqlTypes;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;
using Reporting.Common;
using Reporting.DAL;
using Sightly.ReportGeneration.Generators;
using Sightly.ReportGeneration.Generators.Interface;
using Sightly.ReportGeneration.Models;

namespace Reporting.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("reports/budget")]
    public class BudgetController : ApiController
    {
        private readonly IReportStore _reportStore;
        private readonly IAccountStore _accountStore;

        public BudgetController()
        {
            _reportStore = new ReportStore();
            _accountStore = new AccountStore();
        }

        [HttpPost]
        [Route("budgetingViewReport")]
        public HttpResponseMessage BudgetingViewReport(CustomerDateRangeRequest customerDateRange)
        {
            try
            {
                var startDate = customerDateRange.StartDate ?? (DateTime)SqlDateTime.MinValue;
                var endDate = customerDateRange.EndDate ?? (DateTime)SqlDateTime.MaxValue;

                var campBudgReport = _reportStore.GetCampaignBudgetingReports(customerDateRange.Customers, startDate, endDate);

                IExcelReport BudgetViewExcel = new CampaginBudgetViewReport(campBudgReport);
                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(BudgetViewExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"BudgetViewReport_{string.Join("|", customerDateRange.Customers.Select(x => x.ToString()))}.xlsx"
                };
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }


        [HttpPost]
        [Route("budgetingImpressionReport")]
        public HttpResponseMessage BudgetingImpressionReport(CustomerDateRangeRequest customerDateRange)
        {
            try
            {
                var startDate = customerDateRange.StartDate ?? (DateTime)SqlDateTime.MinValue;
                var endDate = customerDateRange.EndDate ?? (DateTime)SqlDateTime.MaxValue;

                var campBudgReport = _reportStore.GetCampaignBudgetingReports(customerDateRange.Customers, startDate, endDate);

                IExcelReport BudgetImpressionExcel = new CampaignBudgetImpressionReport(campBudgReport);
                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(BudgetImpressionExcel.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"BudgetImpressionReport_{string.Join("|", customerDateRange.Customers.Select(x => x.ToString()))}.xlsx"
                };
                return result;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
