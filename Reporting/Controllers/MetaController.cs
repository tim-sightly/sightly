﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Reporting.Common;
using Reporting.DAL;
using Reporting.DAL.DTO;

namespace Reporting.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("meta")]    
    public class MetaController : ApiController
    {
        private readonly IAccountStore _accountStore;

        public MetaController()
        {
            _accountStore = new AccountStore();
        }

        [HttpGet]
        [Route("campaigns")]
        public IHttpActionResult GetCampaignList()
        {
            try
            {
                List<Campaign> campaigns = _accountStore.GetAllCampaigns().OrderBy(c => c.CampaignName).ToList();
                if(campaigns == null || campaigns.Count == 0)
                    throw new Exception("Something went wrong. Unable to retrieve campaigns.");

                return Ok(new { campaigns = campaigns });
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("meadiaagencycampaigns")]
        public IHttpActionResult GetMediaAgencyCampaignList()
        {
            try
            {
                List<CampaignAwCustomer> campaigns = _accountStore.GetAllMediaAgencyCampaigns().OrderBy(c => c.CampaignName).ToList();
                if (campaigns == null || campaigns.Count == 0)
                    throw new Exception("Something went wrong. Unable to retrieve campaigns.");

                return Ok(new { campaigns = campaigns });
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

    }
}
