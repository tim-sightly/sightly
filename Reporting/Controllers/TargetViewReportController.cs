﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using Reporting.Common;
using Reporting.DAL;
using Reporting.DAL.DTO.TargetView;
using Sightly.ReportGeneration.Generators;
using Sightly.ReportGeneration.Generators.Interface;
using Sightly.ReportGeneration.Generators.TargetView;
using Sightly.ReportGeneration.Models;
using Sightly.ReportGeneration.Storage;

namespace Reporting.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("reports/tv")]
    public partial class TargetViewReportController: ApiController
    {
        private readonly IReportStore _reportStore;

        public TargetViewReportController()
        {
            _reportStore = new ReportStore();
        }

        [HttpPost]
        [Route("performanceDetail")]
        public HttpResponseMessage PerformanceDetail(OrderVM[] orders)
        {
            try
            {
                DateTime startDate;
                DateTime endDate;
                if (orders.Length == 1)
                {
                    startDate = orders[0].startDate ?? (DateTime)SqlDateTime.MinValue;
                    endDate = orders[0].endDate ?? (DateTime)SqlDateTime.MaxValue;
                    var fileName = $"PerformanceDetail-{Guid.NewGuid()}.xlsx";
                    var reportParams = _reportStore.GetPerfomanceDetailReport(orders[0].orderId, startDate, endDate);
                    OrderStatGroups(reportParams);
             
                    IExcelTargetViewReport report = new PerformanceDetailReportExcel(reportParams, "performancedetailreports", fileName);
                    report.Build();

                    var result = Request.CreateResponse(HttpStatusCode.OK);
                    result.Content = new StreamContent(report.ToStream(fileName));
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = fileName
                    };
                    return result;
                }
                else
                {
                    List<Task> buildOrderReportTasks = new List<Task>();
                    List<string> fileNames = new List<string>();
                    char[] invalidFileNameChars = Path.GetInvalidFileNameChars();
                    List<PerformanceDetailReportParameters> perfDetailsList = new List<PerformanceDetailReportParameters>();
                    foreach (var order in orders)
                    {      
                        startDate = order.startDate ?? (DateTime)SqlDateTime.MinValue;
                        endDate = orders[0].endDate ?? (DateTime)SqlDateTime.MaxValue;
                        var reportParams = _reportStore.GetPerfomanceDetailReport(order.orderId, startDate, endDate);
                        OrderStatGroups(reportParams);
                        perfDetailsList.Add(reportParams);
                        var validOrderName = new string(reportParams.OrderName.Where(ch => !invalidFileNameChars.Contains(ch)).ToArray());
                        string fileName = $"{validOrderName}.xlsx";
                        fileNames.Add(fileName);

                        buildOrderReportTasks.Add(Task.Factory.StartNew(() =>
                        {                            
                            IExcelTargetViewReport report = new PerformanceDetailReportExcel(reportParams, "performancedetailreports", fileName);
                            report.Build();
                        }));
                    }
                    Task.WaitAll(buildOrderReportTasks.ToArray());

                    PerformanceDetailReportParameters aggregateReportParameter = GetAggregatePerformanceDetailsFromList(perfDetailsList);
                    OrderStatGroups(aggregateReportParameter);
                    var aggFileName = $"AggregateReport-{Guid.NewGuid()}.xlsx";
                    IExcelTargetViewReport aggReport = new PerformanceDetailReportExcel(aggregateReportParameter, "performancedetailreports", aggFileName);
                    aggReport.Build();
                    fileNames.Add(aggFileName);

                    //We have built all excel files so assemble them into zip...
                    var ms = new MemoryStream();
                    var zipOutputStream = new ZipOutputStream(ms);
                    zipOutputStream.SetLevel(3); //0-9, 9 being the highest level of compression
                    var fileManager = new BlobStorageManager("performancedetailreports");

                    foreach (var file in fileNames)
                    {
                        var newEntry = new ZipEntry(file);
                        newEntry.DateTime = DateTime.Now;
                        zipOutputStream.PutNextEntry(newEntry);

                        MemoryStream fileStream = fileManager.ReadFileAsStream(file);
                        StreamUtils.Copy(fileStream, zipOutputStream, new byte[4096]);
                        fileStream.Close();
                        zipOutputStream.CloseEntry();
                    }

                    zipOutputStream.IsStreamOwner = false;    // False stops the Close also Closing the underlying stream.
                    zipOutputStream.Close();          // Must finish the ZipOutputStream before using outputMemStream.

                    ms.Position = 0;
                    var result = Request.CreateResponse(HttpStatusCode.OK);
                    result.Content = new StreamContent(ms);
                    result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                    {
                        FileName = $"PerformanceDetail-{Guid.NewGuid()}.zip"
                    };
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }



        
        private PerformanceDetailReportParameters GetAggregatePerformanceDetailsFromList(List<PerformanceDetailReportParameters> perfDetailsList)
        {
            PerformanceDetailReportParameters aggregate = new PerformanceDetailReportParameters();
            aggregate.StartDate = perfDetailsList.Min(pd => pd.StartDate);
            aggregate.EndDate = perfDetailsList.Max(pd => pd.EndDate);
            aggregate.AdvertiserName = "All Advertisers";
            aggregate.CampaignName = "All Campaigns";
            aggregate.ReportPeriodText = "All Periods";
            aggregate.OrderAggregate = new OrderAggregate
            {
                Cost = perfDetailsList.Sum(x => x.OrderAggregate.Cost),
                Reach = perfDetailsList.Average(x => x.OrderAggregate.Reach),
                Impressions = perfDetailsList.Sum(x => x.OrderAggregate.Impressions),
                Views = perfDetailsList.Sum(x => x.OrderAggregate.Views),
                ViewRate = perfDetailsList.Average(x => x.OrderAggregate.ViewRate),
                ViewTime = perfDetailsList.Sum(x => x.OrderAggregate.ViewTime),
                CPV = perfDetailsList.Average(x => x.OrderAggregate.CPV),
                Clicks = perfDetailsList.Sum(x => x.OrderAggregate.Clicks),
                ClickThroughRate = perfDetailsList.Average(x => x.OrderAggregate.ClickThroughRate)
            };

            //aggregate.AggregatedCampaign = new AggregatedCampaignPerformanceSummary
            //{
            //    TotalSpend = perfDetailsList.Sum(x => x.AggregatedCampaign.TotalSpend),
            //    AvgReach = perfDetailsList.Average(x => x.AggregatedCampaign.AvgReach),
            //    TotalImpressions = perfDetailsList.Sum(x=> x.AggregatedCampaign.TotalImpressions),
            //    TotalCompletedViews = perfDetailsList.Sum(x => x.AggregatedCampaign.TotalCompletedViews),
            //    ViewRate = perfDetailsList.Average(x => x.AggregatedCampaign.ViewRate),
            //    TotalViewTime = perfDetailsList.Sum(x => x.AggregatedCampaign.TotalViewTime),
            //    AvgCPCV = perfDetailsList.Average(x => x.AggregatedCampaign.AvgCPCV),
            //    Clicks = perfDetailsList.Sum(x => x.AggregatedCampaign.Clicks),
            //    AvgCTR = perfDetailsList.Average(x => x.AggregatedCampaign.AvgCTR)

            //};

            aggregate.AgeGroupPerformanceDetails = GetAgePerformanceAggregate(perfDetailsList);

            aggregate.GenderPerformanceDetails = GetGenderPerformanceAggregate(perfDetailsList);

            aggregate.DevicePerformanceDetails = GetDevicePerformanceAggregate(perfDetailsList);

            aggregate.AdPerformanceDetails = new List<AdPerformanceDetail>();

            perfDetailsList.ForEach(perfDet =>
            {
                perfDet.AdPerformanceDetails.ForEach(adPerf =>
                {
                    aggregate.AdPerformanceDetails.Add(adPerf);
                });
            });

            return aggregate;
        }

        private List<AgeGroupPerformanceDetail> GetAgePerformanceAggregate(List<PerformanceDetailReportParameters> perfDetailsList)
        {
            List<AgeGroupPerformanceDetail> ageGroupPerformanceDetails = new List<AgeGroupPerformanceDetail>();

            List<AgeGroupPerformanceDetail> eighteenList = new List<AgeGroupPerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var details = perf.AgeGroupPerformanceDetails.FirstOrDefault(g => g.AgeGroupName == "18-24");
                if (details != null)
                {
                    eighteenList.Add(perf.AgeGroupPerformanceDetails.FirstOrDefault(g => g.AgeGroupName == "18-24"));
                }
            });

            if (eighteenList.Count > 0)
            {
                AgeGroupPerformanceDetail eighteen = new AgeGroupPerformanceDetail
                {
                    AgeGroupName = "18-24",
                    Impressions = eighteenList.Sum(e => e.Impressions),
                    CompletedViews = eighteenList.Sum(e => e.CompletedViews),
                    ViewRate = eighteenList.Average(e => e.ViewRate),
                    AvgCPCV = eighteenList.Average(e => e.AvgCPCV),
                    Clicks = eighteenList.Sum(e => e.Clicks)
                };

                ageGroupPerformanceDetails.Add(eighteen);
            }

            List<AgeGroupPerformanceDetail> twentyfiveList = new List<AgeGroupPerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var details = perf.AgeGroupPerformanceDetails.FirstOrDefault(g => g.AgeGroupName == "25-34");
                if (details != null)
                {
                    twentyfiveList.Add(perf.AgeGroupPerformanceDetails.FirstOrDefault(g => g.AgeGroupName == "25-34"));
                }
            });
            if (twentyfiveList.Count > 0)
            {
                AgeGroupPerformanceDetail twentyfive = new AgeGroupPerformanceDetail
                {
                    AgeGroupName = "25-34",
                    Impressions = twentyfiveList.Sum(e => e.Impressions),
                    CompletedViews = twentyfiveList.Sum(e => e.CompletedViews),
                    ViewRate = twentyfiveList.Average(e => e.ViewRate),
                    AvgCPCV = twentyfiveList.Average(e => e.AvgCPCV),
                    Clicks = twentyfiveList.Sum(e => e.Clicks)
                };

                ageGroupPerformanceDetails.Add(twentyfive);
            }
            


            List<AgeGroupPerformanceDetail> thirtyfiveList = new List<AgeGroupPerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var details = perf.AgeGroupPerformanceDetails.FirstOrDefault(g => g.AgeGroupName == "35-44");
                if (details != null)
                {
                    thirtyfiveList.Add(details);
                }
            });
            if (thirtyfiveList.Count > 0)
            {
                AgeGroupPerformanceDetail thirtyfive = new AgeGroupPerformanceDetail
                {
                    AgeGroupName = "35-44",
                    Impressions = thirtyfiveList.Sum(e => e.Impressions),
                    CompletedViews = thirtyfiveList.Sum(e => e.CompletedViews),
                    ViewRate = thirtyfiveList.Average(e => e.ViewRate),
                    AvgCPCV = thirtyfiveList.Average(e => e.AvgCPCV),
                    Clicks = thirtyfiveList.Sum(e => e.Clicks)
                };

                ageGroupPerformanceDetails.Add(thirtyfive);

            }

            List<AgeGroupPerformanceDetail> fourtyfiveList = new List<AgeGroupPerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var details = perf.AgeGroupPerformanceDetails.FirstOrDefault(g => g.AgeGroupName == "45-54");
                if (details != null)
                {
                    fourtyfiveList.Add(details);
                }
            });

            if (fourtyfiveList.Count > 0)
            {
                AgeGroupPerformanceDetail fourtyfive = new AgeGroupPerformanceDetail
                {
                    AgeGroupName = "45-54",
                    Impressions = fourtyfiveList.Sum(e => e.Impressions),
                    CompletedViews = fourtyfiveList.Sum(e => e.CompletedViews),
                    ViewRate = fourtyfiveList.Average(e => e.ViewRate),
                    AvgCPCV = fourtyfiveList.Average(e => e.AvgCPCV),
                    Clicks = fourtyfiveList.Sum(e => e.Clicks)
                };

                ageGroupPerformanceDetails.Add(fourtyfive);
            }

            List<AgeGroupPerformanceDetail> fiftyfiveList = new List<AgeGroupPerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var detail = perf.AgeGroupPerformanceDetails.FirstOrDefault(g => g.AgeGroupName == "55-64");
                if (detail != null)
                {
                    fiftyfiveList.Add(detail);
                }
            });

            if (fiftyfiveList.Count > 0)
            {
                AgeGroupPerformanceDetail fiftyfive = new AgeGroupPerformanceDetail
                {
                    AgeGroupName = "55-64",
                    Impressions = fiftyfiveList.Sum(e => e.Impressions),
                    CompletedViews = fiftyfiveList.Sum(e => e.CompletedViews),
                    ViewRate = fiftyfiveList.Average(e => e.ViewRate),
                    AvgCPCV = fiftyfiveList.Average(e => e.AvgCPCV),
                    Clicks = fiftyfiveList.Sum(e => e.Clicks)
                };

                ageGroupPerformanceDetails.Add(fiftyfive);
            }



            List<AgeGroupPerformanceDetail> sixtyfiveList = new List<AgeGroupPerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var detail = perf.AgeGroupPerformanceDetails.FirstOrDefault(g => g.AgeGroupName == "65 or more");
                if (detail != null)
                {
                    sixtyfiveList.Add(detail);
                }
            });

            if (sixtyfiveList.Count > 0)
            {
                AgeGroupPerformanceDetail sixtyfive = new AgeGroupPerformanceDetail
                {
                    AgeGroupName = "65 or more",
                    Impressions = sixtyfiveList.Sum(e => e.Impressions),
                    CompletedViews = sixtyfiveList.Sum(e => e.CompletedViews),
                    ViewRate = sixtyfiveList.Average(e => e.ViewRate),
                    AvgCPCV = sixtyfiveList.Average(e => e.AvgCPCV),
                    Clicks = sixtyfiveList.Sum(e => e.Clicks)
                };

                ageGroupPerformanceDetails.Add(sixtyfive);
            }


            List<AgeGroupPerformanceDetail> underterminedList = new List<AgeGroupPerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var detail = perf.AgeGroupPerformanceDetails.FirstOrDefault(g => g.AgeGroupName == "Undetermined");
                if (detail != null)
                {
                    underterminedList.Add(detail);
                }
            });

            if (underterminedList.Count > 0)
            {
                AgeGroupPerformanceDetail undertermined = new AgeGroupPerformanceDetail
                {
                    AgeGroupName = "Undetermined",
                    Impressions = underterminedList.Sum(e => e.Impressions),
                    CompletedViews = underterminedList.Sum(e => e.CompletedViews),
                    ViewRate = underterminedList.Average(e => e.ViewRate),
                    AvgCPCV = underterminedList.Average(e => e.AvgCPCV),
                    Clicks = underterminedList.Sum(e => e.Clicks)
                };

                ageGroupPerformanceDetails.Add(undertermined);

            }
            

            
            return ageGroupPerformanceDetails;
        }

        private List<GenderPerformanceDetail> GetGenderPerformanceAggregate(List<PerformanceDetailReportParameters> perfDetailsList)
        {
            List<GenderPerformanceDetail> genderPerformanceDetails = new List<GenderPerformanceDetail>();
            List<GenderPerformanceDetail> maleDetailList = new List<GenderPerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var detail = perf.GenderPerformanceDetails.FirstOrDefault(g => g.GenderName == "Male");
                if (detail != null)
                {
                    maleDetailList.Add(detail);
                }
            });
            if (maleDetailList.Count > 0)
            {
                GenderPerformanceDetail male = new GenderPerformanceDetail
                {
                    GenderName = "Male",
                    Impressions = maleDetailList.Sum(e => e.Impressions),
                    CompletedViews = maleDetailList.Sum(e => e.CompletedViews),
                    ViewRate = maleDetailList.Average(e => e.ViewRate),
                    AvgCPCV = maleDetailList.Average(e => e.AvgCPCV),
                    Clicks = maleDetailList.Sum(e => e.Clicks)
                };

                genderPerformanceDetails.Add(male);
            }

            List<GenderPerformanceDetail> femaleDetailList = new List<GenderPerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var detail = perf.GenderPerformanceDetails.FirstOrDefault(g => g.GenderName == "Female");
                if (detail != null)
                {
                    femaleDetailList.Add(detail);
                }
            });
            if (femaleDetailList.Count > 0)
            {
                GenderPerformanceDetail female = new GenderPerformanceDetail
                {
                    GenderName = "Female",
                    Impressions = femaleDetailList.Sum(e => e.Impressions),
                    CompletedViews = femaleDetailList.Sum(e => e.CompletedViews),
                    ViewRate = femaleDetailList.Average(e => e.ViewRate),
                    AvgCPCV = femaleDetailList.Average(e => e.AvgCPCV),
                    Clicks = femaleDetailList.Sum(e => e.Clicks)
                };

                genderPerformanceDetails.Add(female);
            }

            List<GenderPerformanceDetail> unknownDetailList = new List<GenderPerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var detail = perf.GenderPerformanceDetails.FirstOrDefault(g => g.GenderName == "Unknown");
                if (detail != null)
                {
                    unknownDetailList.Add(detail);
                }
            });
            if (unknownDetailList.Count > 0)
            {
                GenderPerformanceDetail unknown = new GenderPerformanceDetail
                {
                    GenderName = "Unknown",
                    Impressions = unknownDetailList.Sum(e => e.Impressions),
                    CompletedViews = unknownDetailList.Sum(e => e.CompletedViews),
                    ViewRate = unknownDetailList.Average(e => e.ViewRate),
                    AvgCPCV = unknownDetailList.Average(e => e.AvgCPCV),
                    Clicks = unknownDetailList.Sum(e => e.Clicks)
                };

                genderPerformanceDetails.Add(unknown);
            }


            return genderPerformanceDetails;
        }



        private List<DeviceTypePerformanceDetail> GetDevicePerformanceAggregate(List<PerformanceDetailReportParameters> perfDetailsList)
        {
            List<DeviceTypePerformanceDetail> devicePerformanceDetails = new List<DeviceTypePerformanceDetail>();

            List<DeviceTypePerformanceDetail> computerList = new List<DeviceTypePerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var details = perf.DevicePerformanceDetails.FirstOrDefault(g => g.DeviceTypeName == "Computers");
                if (details != null)
                {
                    computerList.Add(details);
                }
            });
            if (computerList.Count > 0)
            {
                DeviceTypePerformanceDetail computers = new DeviceTypePerformanceDetail
                {
                    DeviceTypeName = "Computers",
                    Impressions = computerList.Sum(e => e.Impressions),
                    CompletedViews = computerList.Sum(e => e.CompletedViews),
                    ViewRate = computerList.Average(e => e.ViewRate),
                    AvgCPCV = computerList.Average(e => e.AvgCPCV),
                    Clicks = computerList.Sum(e => e.Clicks)
                };
                devicePerformanceDetails.Add(computers);
            }

            List<DeviceTypePerformanceDetail> tabletList = new List<DeviceTypePerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var details = perf.DevicePerformanceDetails.FirstOrDefault(g => g.DeviceTypeName == "Tablets with full browsers");
                if (details != null)
                {
                    tabletList.Add(details);
                }
            });
            if (tabletList.Count > 0)
            {
                DeviceTypePerformanceDetail tablet = new DeviceTypePerformanceDetail
                {
                    DeviceTypeName = "Tablets with full browsers",
                    Impressions = tabletList.Sum(e => e.Impressions),
                    CompletedViews = tabletList.Sum(e => e.CompletedViews),
                    ViewRate = tabletList.Average(e => e.ViewRate),
                    AvgCPCV = tabletList.Average(e => e.AvgCPCV),
                    Clicks = tabletList.Sum(e => e.Clicks)
                };
                devicePerformanceDetails.Add(tablet);
            }


            List<DeviceTypePerformanceDetail> mobileList = new List<DeviceTypePerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var details = perf.DevicePerformanceDetails.FirstOrDefault(g => g.DeviceTypeName == "Mobile devices with full browsers");
                if (details != null)
                {
                    mobileList.Add(details);
                }
            });
            if (mobileList.Count > 0)
            {
                DeviceTypePerformanceDetail tablet = new DeviceTypePerformanceDetail
                {
                    DeviceTypeName = "Mobile devices with full browsers",
                    Impressions = mobileList.Sum(e => e.Impressions),
                    CompletedViews = mobileList.Sum(e => e.CompletedViews),
                    ViewRate = mobileList.Average(e => e.ViewRate),
                    AvgCPCV = mobileList.Average(e => e.AvgCPCV),
                    Clicks = mobileList.Sum(e => e.Clicks)
                };
                devicePerformanceDetails.Add(tablet);
            }


            List<DeviceTypePerformanceDetail> otherList = new List<DeviceTypePerformanceDetail>();
            perfDetailsList.ForEach(perf =>
            {
                var details = perf.DevicePerformanceDetails.FirstOrDefault(g => g.DeviceTypeName == "Other");
                if (details != null)
                {
                    otherList.Add(details);
                }
            });
            if (otherList.Count > 0)
            {
                DeviceTypePerformanceDetail other = new DeviceTypePerformanceDetail
                {
                    DeviceTypeName = "Other",
                    Impressions = mobileList.Sum(e => e.Impressions),
                    CompletedViews = mobileList.Sum(e => e.CompletedViews),
                    ViewRate = mobileList.Average(e => e.ViewRate),
                    AvgCPCV = mobileList.Average(e => e.AvgCPCV),
                    Clicks = mobileList.Sum(e => e.Clicks)
                };
                devicePerformanceDetails.Add(other);
            }
            

            return devicePerformanceDetails;
        }

        [HttpPost]
        [Route("performanceSummary")]
        public HttpResponseMessage PerformanceSummary(PerformanceSummaryVM vm)
        {
            try
            {
                DateTime startDate = vm.startDate ?? (DateTime) SqlDateTime.MinValue;
                DateTime endDate = vm.endDate ?? (DateTime) SqlDateTime.MaxValue;
                List<string> orderIdStrings = vm.orderIds.Split(',').ToList();
                List<Guid> orderIds = orderIdStrings.Select(orderId => Guid.Parse(orderId)).ToList();
                var reportParams = _reportStore.GetPerformanceSummary(orderIds, startDate, endDate);
                ICsvReport report = new PerformanceSummaryReportCsv(reportParams);
                report.Build();

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(report.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"PerformanceSummary-{Guid.NewGuid()}.csv"
                };
                return result;
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        [HttpPost]
        [Route("performanceSummary2")]
        public HttpResponseMessage PerformanceSummaryReport(PerformanceSummaryVM orderDateRange)
        {
            try
            {
                DateTime startDate = orderDateRange.startDate ?? (DateTime)SqlDateTime.MinValue;
                DateTime endDate = orderDateRange.endDate ?? (DateTime)SqlDateTime.MaxValue;
                var performanceSummaryData = _reportStore.GetPerformanceSummaryData(orderDateRange.orderIds, startDate, endDate);

                IExcelReport performanceSummary = new PerformanceSummaryReportExcel(performanceSummaryData);

                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(performanceSummary.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"PerformanceSummary_{startDate.ToShortDateString()}_{endDate.ToShortDateString()}.xlsx"
                };
                return result;
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }

        }


        [HttpGet]
        [Route("budgetPacing")]
        public HttpResponseMessage BudgetPacing(Guid orderId)
        {
            try
            {
                //TODO: parse long variable list
                var reportParams = new BudgetPacingReportParameters();

                ICsvReport report = new BudgetPacingReportCsv(reportParams);
                report.Build();
                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(report.ToStream());
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"BudgetPacing-{Guid.NewGuid()}.csv"
                };
                return result;
            }
            catch (Exception e)
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError);
            }
        }

        private void OrderStatGroups(PerformanceDetailReportParameters parameters)
        {
            parameters.AgeGroupPerformanceDetails.Sort((ag1, ag2) => ag1.AgeGroupName.CompareTo(ag2.AgeGroupName));
            AgeGroupPerformanceDetail unknown = parameters.AgeGroupPerformanceDetails.FirstOrDefault(ag => ag.AgeGroupName.ToLower() == "unknown");
            if (unknown != null)
            {
                //remove from middle
                parameters.AgeGroupPerformanceDetails.RemoveAll(ag => ag.AgeGroupName.ToLower() == "unknown");
                //add to end
                parameters.AgeGroupPerformanceDetails.Add(unknown);
            }


            parameters.DevicePerformanceDetails.Sort((d1, d2) => d1.DeviceTypeName.CompareTo(d2.DeviceTypeName));
            DeviceTypePerformanceDetail other = parameters.DevicePerformanceDetails.FirstOrDefault(d => d.DeviceTypeName.ToLower() == "other");
            if (other != null)
            {
                //remove from middle
                parameters.DevicePerformanceDetails.RemoveAll(d => d.DeviceTypeName.ToLower() == "other");
                //add to end
                parameters.DevicePerformanceDetails.Add(other);
            }

            parameters.GenderPerformanceDetails.Sort((g1, g2) => g1.GenderName.CompareTo(g2.GenderName));
        }
    }
}