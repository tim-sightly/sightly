﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using Reporting.Common;
using Reporting.DAL;

namespace Reporting.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("proc")]
    public class ProcController : ApiController
    {
        private readonly IReportStore _reportStore;

        public ProcController()
        {
            _reportStore = new ReportStore();
        }

        [HttpGet]
        [Route("do")]
        public IHttpActionResult Do()
        {
            try
            {
                var queryStringParams = Request.GetQueryNameValuePairs().ToList();
                bool procNameExists = queryStringParams.Exists(p => p.Key == "procName");
                if(!procNameExists)
                    return BadRequest("Must provide a value for 'procName'.");

                string procName = queryStringParams.First(p => p.Key == "procName").Value;
                queryStringParams.RemoveAll(p => p.Key == "procName");
                queryStringParams.RemoveAll(p => p.Key == "token");

                IEnumerable<dynamic> rows = _reportStore.ProcDo(procName, queryStringParams);
                string csvString = BuildCsvStringFromDapperRows(rows);
                byte[] responseBytes = System.Text.Encoding.UTF8.GetBytes(csvString);
                var result = Request.CreateResponse(HttpStatusCode.OK);
                result.Content = new StreamContent(new MemoryStream(responseBytes));
                result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
                {
                    FileName = $"{procName}-{DateTime.Now.ToString("MM-dd-yyyy")}.csv"
                };

                return ResponseMessage(result);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        private string BuildCsvStringFromDapperRows(IEnumerable<dynamic> rows)
        {
            StringBuilder sb = new StringBuilder();
            int count = 0;

            foreach (dynamic row in rows)
            {
                if (count == 0)
                {
                    foreach (var pair in row)
                    {
                        sb.Append(pair.Key + ",");
                    }
                    sb.Remove(sb.Length - 1, 1);
                    sb.AppendLine();
                }
                foreach (var pair in row)
                {
                    sb.Append(pair.Value + ",");
                }
                sb.Remove(sb.Length - 1, 1);
                sb.AppendLine();
                count++;
            }

            return sb.ToString();
        }
    }
}
