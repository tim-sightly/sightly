﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using JWT;
using JWT.Serializers;

namespace Reporting.Common
{
    public class JwtAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (AuthorizeRequest(actionContext))
                return;

            HandleUnauthorizedRequest(actionContext);
        }

        protected override void HandleUnauthorizedRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            //Code to handle unauthorized request
            actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
        }

        private bool AuthorizeRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            //Write your code here to perform authorization
            try
            {
                string token = GetTokenFromRequest(actionContext);

                if (token == null)
                    return false;

                JsonNetSerializer serializer = new JWT.Serializers.JsonNetSerializer();
                IDateTimeProvider provider = new UtcDateTimeProvider();
                IJwtValidator validator = new JwtValidator(serializer, provider);
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);

                string secret = ConfigurationManager.AppSettings.Get("JwtSecret");
                var payload = decoder.DecodeToObject<IDictionary<string, object>>(token, secret, true);

                object userId;
                if (payload.TryGetValue("userId", out userId))
                {
                    actionContext.ActionArguments["UserId"] = userId.ToString();
                }
            }
            catch (TokenExpiredException)
            {
                //Console.WriteLine("Token has expired");
                return false;
            }
            catch (SignatureVerificationException)
            {
                //Console.WriteLine("Token has invalid signature");
                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        private string GetTokenFromRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            //First look in headers for JWT
            if (!string.IsNullOrEmpty(actionContext.Request.Headers.Authorization?.Parameter))
            {
                return actionContext.Request.Headers.Authorization?.Parameter;
            }
            //Now look in query string for JWT
            else
            {
                var queryString = actionContext.Request
                    .GetQueryNameValuePairs()
                    .ToDictionary(x => x.Key, x => x.Value);

                if (queryString.ContainsKey("token") && queryString["token"] != null)
                {
                    return queryString["token"];
                }
                else
                {
                    return null;
                }
            }
        }
    }
}