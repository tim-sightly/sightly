﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.IOC.Testing;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Service.YoutubeUploader.Test
{
    [TestClass]
    public class UpDownLoaderTest
    {
        /*
B9C09DF1-2166-4AFE-9ACD-A86C01858ED7
         * */
        private Guid VideoAssetId = Guid.Parse("afc3016a-cf22-47e5-a622-a8e001338d65");
        private static IContainer Container { get; set; }
        private IUpDownLoader _upDownLoader;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _upDownLoader = Container.Resolve<IUpDownLoader>();
        }


        
        [TestMethod]
        public void GetDataTogetherAndDownloadaZureData()
        {
            var youTubeId = _upDownLoader.DownloadAzureBlobAndUploadToYoutube(VideoAssetId);

            Assert.IsNotNull(youTubeId);
        }
    }
}
