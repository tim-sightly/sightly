﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.TargetView.Services.Validation
{
    public interface IAdwordsToTargetview
    {
        void CheckAndPushCampaignOrder(long customerId);

        void AssignPlacementWithAwCustomerId(long awCustomerId, string lastModifiedBy);


        void GetCampaignsAdWordsInfosAndUpate(DateTime date);

        void UpdateTargetViewAdwordsAssociatedData(List<TargetViewAdwordsAssociatedData> tvAdwAssociatedData);
    }
}