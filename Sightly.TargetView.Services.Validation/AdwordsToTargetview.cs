﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Business.Interfaces;
using Sightly.Models;
using Sightly.TargetView.AdwordsValidation.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.TargetView.Services.Validation
{
    public class AdwordsToTargetview : IAdwordsToTargetview
    {

        private IAdRepository _adRepository;
        private IAdValidation _adValidation;
        //private IConnectionStringStore _connectionStringStore;
        private IPlacementValidation _placementValidation;
        private IOrderRepository _orderRepository;
        private ITargetViewAdwordsValidation _targetViewAdwordsValidation;
        private ITargetViewRepository _targetViewRepository;

        public AdwordsToTargetview(IAdRepository adRepository, IAdValidation adValidation, ITargetViewAdwordsValidation targetViewAdwordsValidation, ITargetViewRepository targetViewRepository, IPlacementValidation placementValidation, IOrderRepository orderRepository)
        {
            _adRepository = adRepository;
            _adValidation = adValidation;
          //  _connectionStringStore = connectionStringStore;
            _targetViewAdwordsValidation = targetViewAdwordsValidation;
            _targetViewRepository = targetViewRepository;
            _placementValidation = placementValidation;
            _orderRepository = orderRepository;
        }
        public void CheckAndPushCampaignOrder(long customerId)
        {
            var targetViewData = _targetViewRepository.GetTargetViewAdwordsAssociations(customerId);
            var awData = _targetViewAdwordsValidation.GetCustomerAndAllAdsFromAdwords(customerId.ToString());
            //The Video Data is needed for the Videos Present in TV
            var tvVideoData = _targetViewRepository.GetTargetViewVideoAssetsByAdvertiser(targetViewData.First().AdvertiserId);

            var areSimilar = _adValidation.TargetViewToAdwordsAdChecker(targetViewData, awData.First());
            //This test will identify any videos that are not in the system
            var doesVidExist = _adValidation.TargetViewAdVideoValidation(areSimilar.AdsInAw, tvVideoData);

            if (doesVidExist.Any(ad => ad.TvVideoAssetId == Guid.Empty))
            {
                doesVidExist = ManageTargetViewVideos(doesVidExist);
            }

            var doesAdExist = _adValidation.TargetViewAdUrlValidation(doesVidExist, targetViewData);

            if (doesAdExist.Any(ad => ad.TvAdId == Guid.Empty))
            {
                doesAdExist = ManageTargetViewAds(doesAdExist);
            }

            ManageTargetViewAdAssociations(doesAdExist);

        }

        private List<AwAdTvRef> ManageTargetViewVideos(List<AwAdTvRef> doesVidExist)
        {
            // Find the videos that don't exist and add them to targetview. then copy the VideoAssetId to the AwAdTvRef model
            throw new NotImplementedException();
        }

        private List<AwAdTvRef> ManageTargetViewAds(List<AwAdTvRef> doesAdExist)
        {
            doesAdExist.Where(ad => ad.TvAdId == Guid.Empty).ToList().ForEach(rec =>
            {
                
            });

            return doesAdExist;
        }

        private void ManageTargetViewAdAssociations(List<AwAdTvRef> doesAdExist)
        {
            var badGuid = new Guid("00000000-0000-0000-0000-000000000000");
            doesAdExist.ForEach(ad =>
            {
                if (ad.TvBudgetGroupId != badGuid &&
                    ad.TvOrderId != badGuid &&
                    ad.TvAdId != badGuid)
                {
                    _adRepository.CreateAdAdWordsInfo(new AdAdWordsInfoData
                    {
                        AdAdWordsInfoId = Guid.NewGuid(),
                        AdId = ad.TvAdId,
                        AdWordsAdId = ad.AwAd.AdId,
                        AdWordsAdName = ad.AwAd.AdName,
                        BudgetGroupId = ad.TvBudgetGroupId,
                        LastModified = "Adwords to Targetview Validator",
                        OrderId = ad.TvOrderId
                    });
                }

            });
        }

        public void AssignPlacementWithAwCustomerId(long awCustomerId, string lastModifiedBy)
        {
            var campLables = _placementValidation.GetCampaignLabelData(awCustomerId);

            campLables.ForEach(cl =>
                cl.Labels.ForEach(label =>
                {
                    if (label.Contains("Placement"))
                    {
                        //CALL SP FOR ASSIGNING PLACEMENT
                        _orderRepository.AssignPlacementWithAwCampaignId(cl.CampaignId, label, lastModifiedBy);

                    }
                })
            );
        }

        public void GetCampaignsAdWordsInfosAndUpate(DateTime date)
        {
            var tvCampaignAdwordInfoWithNoCids = _targetViewRepository.GetListWithoutAdwordsCustomerId(date);

            var tvAssociatedCampaigns =
                _targetViewAdwordsValidation.GetAdWordsCustomersInfo(tvCampaignAdwordInfoWithNoCids);

            tvAssociatedCampaigns.ToList().ForEach(camp =>
            {
                if (camp.AdWordsCustomerId.HasValue)
                {
                    _targetViewRepository.UpdateCampaignAdWordsInfoWithCustomerId(camp.CampaignAdWordsInfoId, camp.AdWordsCustomerId.Value, "Adwords To Targetview Assigner");
                }
            });
        }

        public void UpdateTargetViewAdwordsAssociatedData(List<TargetViewAdwordsAssociatedData> tvAdwAssociatedData)
        {
            var campaignUpdater = tvAdwAssociatedData.Where(tvCam => tvCam.UpdateCampaignId == true).ToList();
            campaignUpdater.ForEach(camp =>
            {
                _targetViewRepository.UpdateBudgetGroupAdWordsInfoWithCampaignId(camp.BudgetGroupId, camp.AdwordsCampaignId.Value, camp.AdwordsCampaignName, "Adwords To Targetview Assigner");
            });

            var adUpdater = tvAdwAssociatedData.Where(ad => ad.UpdateAdId == true).ToList();
            adUpdater.ForEach(ad =>
            {
                _targetViewRepository.AddAdWordsInfoWithAdId(ad.AdId, ad.AdwordsAdName, ad.AdwordsAdId.Value, ad.AdwordsCampaignId.Value, ad.BudgetGroupId, "Adwords To Targetview Assigner");
            });
        }
    }
}