﻿using System;
using System.Collections.Generic;
using System.Linq;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using Sightly.Tools;
using System.Net.Mail;
using System.IO;
using System.Reflection;

namespace MediaAgenciesChecker
{
    class Program
    {
        static void Main(string[] args)
        {
            using (IDbConnection db = new SqlConnection("Server=tcp:w5yha8r8ja.database.windows.net,1433; Database=TargetviewDb;User ID=sightlyadmin@w5yha8r8ja;Password = cDE3@1QAz;Trusted_Connection = False; Encrypt = True; Connection Timeout = 30;"))
            //using (IDbConnection db = new SqlConnection("Server=tcp:cg5h9ry6.database.windows.net,1433;Database=TargetViewDb;User ID=SightlyAdmin@cg5h9ry6;Password=mUMCFG0k;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;"))

            {
               // var outPutDirectory = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
                DateTime dateToCheck = new DateTime(2017, 11, 25);
                string dateToCheckStr = dateToCheck.ToString();

                //Get all of the missing ads per customerId by campaignID
                string strSql = @"SELECT DISTINCT		a.CustomerId, a.CampaignId, a.CampaignName, a.AdId, a.Adname, a.statdate " +
                                 "FROM	raw.AdDeviceStats a " +
                                 "LEFT JOIN Orders.AdAdWordsInfo aai on a.AdId = aai.AdWordsAdId " +
                                 "WHERE a.StatDate = " + "'" + dateToCheckStr + "' " +
                                 "and aai.AdAdWordsInfoId is NULL " +
                                 "ORDER BY				a.CustomerId";

                List<MissingAd> listOfMissingAds = db.Query<MissingAd>(strSql).ToList();


                int countOfMissingAds = listOfMissingAds.Count;

                if (countOfMissingAds > 0)
                {
                    System.Console.WriteLine("The following list of Ads are not associated yet in TargetView.");
                    for (int i = 0; i < countOfMissingAds; i++)
                    {
                        System.Console.WriteLine("{0}" + "   " + "{1}" + "  " + "{2}" + "   " + "{3}  " + "4", i, listOfMissingAds[i].CustomerId, listOfMissingAds[i].CampaignID, listOfMissingAds[i].CampaignName, listOfMissingAds[i].Adid, listOfMissingAds[i].Adname);
                    }
                }

                else
                { System.Console.WriteLine("There are no ads by CustomerID that are missing in Adwords..."); }

                CsvUtility.CreateCsvFromGenericList(listOfMissingAds, string.Format("MA_DailyAdsCheck_{0}.csv", dateToCheckStr));


                //create the path for the first attachment
                //var outPutDirectory1 = Path.GetDirectoryName(Assembly.GetExecutingAssembly().CodeBase);
                //var filepath = outPutDirectory1 + string.Format("MA_DailyAdsCheck_{ 0}.csv", dateToCheckStr);

                //Get all the missing campaigns in TargetViewDb
                string strSql1 = @"SELECT DISTINCT	a.CustomerID, a.CampaignId, a.CampaignName, a.StatDate " +
                                  "FROM				raw.AdDeviceStats a " +
                                  "LEFT JOIN		Orders.BudgetGroupAdWordsInfo bgai on a.campaignId = bgai.AdWordsCampaignId " +
                                  "WHERE			a.StatDate = " + "'" + dateToCheckStr + "'" + " and bgai.BudgetGroupAdWordsInfoId is NULL";

                List<MissingCampaign> listOfMissingCampaigns = db.Query<MissingCampaign>(strSql1).ToList();


                int countOfMissingCampaigns = listOfMissingCampaigns.Count;

                if (countOfMissingCampaigns > 0)
                {
                    System.Console.WriteLine("The following list of Campaigns are not asociated yet in TargetView.");
                    for (int i = 0; i < countOfMissingCampaigns; i++)
                    {
                        System.Console.WriteLine("{0}" + "   " + "{1}" + "  " + "{2}" + "   " + "{3}  ", i, listOfMissingCampaigns[i].CustomerId, listOfMissingCampaigns[i].CampaignID, listOfMissingCampaigns[i].CampaignName);
                    }
                }

                else
                {
                    Console.WriteLine("There are no campaigns by CustomerID that are not associated TargetView.");
                    
                }


                CsvUtility.CreateCsvFromGenericList(listOfMissingCampaigns, string.Format("MA_DailyCampaignsCheck_{0}.csv", dateToCheckStr));

                //send an email
                MailMessage mail = new MailMessage();
                SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
                mail.From = new MailAddress("dailychecker2017@gmai.com");
                mail.To.Add("grace@sightly.com");
                mail.Subject = "Media Agencies Daily Checker Result";
                
                
                mail.Body = "FYI - This email contains the missing Ads/Campaigns that are not associated in TargetView - please see attachment.  " + Environment.NewLine + Environment.NewLine;

                if (countOfMissingCampaigns == 0)
                {
                    mail.Body = Environment.NewLine + mail.Body + "As of today, there are no Campaigns that are not associated yet in TargetView.";
                }


                if (countOfMissingAds == 0)
                {
                    mail.Body = Environment.NewLine + mail.Body + "There are no Ads that are not associated yet in TargetView.";
                }

                smtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpServer.UseDefaultCredentials = false;
                smtpServer.Port = 587;
                smtpServer.Credentials = new System.Net.NetworkCredential("dailychecker2017@gmail.com", "CSGteam2017");
                smtpServer.EnableSsl = true;

                smtpServer.Send(mail);

                Console.ReadLine();
            }

        }

        public class MissingAd
        {
            public long CustomerId { get; set; }
            public long CampaignID { get; set; }
            public string CampaignName { get; set; }
            public long Adid { get; set; }
            public string Adname { get; set; }
            public DateTime StatDate { get; set; }
        }

        public class MissingCampaign
        {
            public long CustomerId { get; set; }
            public long CampaignID { get; set; }
            public string CampaignName { get; set; }
            public DateTime StatDate { get; set; }

        }
    }
}
