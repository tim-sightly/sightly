﻿using System;
using Sightly.Business.Interfaces;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;

namespace Sightly.TargetViewDb.DAL.DataStores
{
    public class TvExternalResourceStore : DapperDataStore, ITvExteralResourcesDataStore
    {
        public TvExternalResourceStore(IConnectionStringStore connectionStringStore) : base(connectionStringStore)
        {
        }

        public void InsertMoatStat(MoatDailyStatData moatStatData)
        {
            Execute(
                "raw.InsertMoatStatsData",
                new
                {
                    StatDate = moatStatData.StatDate,
                    AdvertiserId = moatStatData.AdvertiserId,
                    AdvertiserName = moatStatData.AdvertiserName,
                    Impressions = moatStatData.Impressions,
                    LoadsUnfiltered = moatStatData.LoadsUnfiltered,
                    StrictOrPx2SecConsecVideoOtsUnfiltered = moatStatData.StrictOrPx2SecConsecVideoOtsUnfiltered,
                    ISomehowMeasurableUnfiltered = moatStatData.ISomehowMeasurableUnfiltered,
                    HumanAndAvoc = moatStatData.HumanAndAvoc,
                    MeasurableImpressions = moatStatData.MeasurableImpressions,
                    ReachedCompleteSum = moatStatData.ReachedCompleteSum

                });
        }
    }
}
