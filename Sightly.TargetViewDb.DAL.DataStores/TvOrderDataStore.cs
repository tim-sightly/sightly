﻿using System;
using Sightly.Business.Interfaces;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;

namespace Sightly.TargetViewDb.DAL.DataStores
{
    public class TvOrderDataStore : DapperDataStore, ITvOrderDataStore
    {
        public TvOrderDataStore(IConnectionStringStore connectionStringStore) : base(connectionStringStore)
        {
        }

        public void CreateOrderRecordLog(Guid orderRecordLogId, Guid orderId, string lastModifiedBy)
        {
            Execute(
                "Orders.OrderRecordLog_InsertFromOrder",
                new
                {
                    OrderRecordLogId = orderRecordLogId,
                    OrderId = orderId,
                    LastModifiedBy = lastModifiedBy
                }
            );
        }

        public void AssignPlacementWithAwCampaignId(long awCampaignId, string placement, string lastModifiedBy)
        {
            Execute(
                "Orders.AssignPlacementWithAWCampaignId",
                new
                {
                    AwCampaignId = awCampaignId,
                    Placement = placement,
                    CreatedBy = lastModifiedBy
                }
            );
        }
    }
}