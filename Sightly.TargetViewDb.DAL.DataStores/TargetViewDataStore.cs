﻿using System;
using System.Collections.Generic;
using Sightly.Business.Interfaces;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;

namespace Sightly.TargetViewDb.DAL.DataStores
{
    public class TargetViewDataStore : DapperDataStore, ITargetViewDataStore
    {
        public TargetViewDataStore(IConnectionStringStore connectionStringStore) : base(connectionStringStore)
        {
        }

        public List<TargetViewAdwordsData> GetTargetViewAdwordsAssociations(long customerId)
        {
            return QueryList<TargetViewAdwordsData>("Orders.TargetViewAssociationsByAwCustomerId",
                new { customer = customerId });
        }

        public List<TargetViewAdwordsAssociatedData> GetAssociatedTargetViewAdwordsAssociations(DateTime statDate)
        {
            return QueryList<TargetViewAdwordsAssociatedData>("Orders.GetTVAdwordsAssociation",
                new {GatherDate = statDate});
        }

        public List<VideoAssetData> GetTargetViewVideoAssetsByAdvertiser(Guid advertiserId)
        {
            return QueryList<VideoAssetData>("Accounts.VideoAssetList_GetByAdvertiserId",
                new { AdvertiserId = advertiserId});
        }

        public AdvertiserEntityData GetAdvertiserEntityData(long campaignId)
        {
            return QueryScalar<AdvertiserEntityData>("Orders.AdvertiserAndAccountByAsCampaignId",
                new { CampaignId = campaignId });
        }

        public List<TvCampaignAdWordsInfo> GetListWithoutAdwordsCustomerId(DateTime liveOrderDate)
        {
            return QueryList<TvCampaignAdWordsInfo>("Accounts.CampaignListWithoutAdWordsCustomerId_Get",
                new { LiveOrderDate = liveOrderDate }
                );
        }

        public void UpdateCampaignAdWordsInfoWithCustomerId(Guid campaignAdWordsInfoId, long customerId, string lastModifedBy)
        {
            Execute(
                "Accounts.CampaignAdWordsInfo_UpdateCustomerId",
                new
                {
                    CampaignAdWordsInfoId = campaignAdWordsInfoId,
                    CustomerId = customerId,
                    LastModifiedBy = lastModifedBy
                }
            );
        }

        public void UpdateBudgetGroupAdWordsInfoWithCampaignId(Guid budgetGroupId, long campaignId, string campaignName, string lastModifiedBy)
        {
            Execute("Orders.BudgetGroupAdWordsInfo_UpdateCampaign",
                new
                {
                    BudgetGroupId  = budgetGroupId,
                    AdwordsCampaignId = campaignId,
                    AdWordsCampaignName  = campaignName,
                    LastModifiedBy = lastModifiedBy
                });
        }

        public void AddAdWordsInfoWithAdId(Guid adId, string awAdName, long awAdId, long awCampaignId, Guid budgetGroupId, string lastModifiedBy)
        {
            Execute("Orders.AdAdWordsInfo_Insert",
                new
                {
                    AdAdWordsInfoId = Guid.NewGuid(),
                    AdId = adId,
                    AdWordsAdName = awAdName,
                    AdWordsAdId = awAdId,
                    AdWordsCampaignId = awCampaignId,
                    BudgetGroupId = budgetGroupId,
                    LastModifiedBy = lastModifiedBy
                });
            
        }
    }
}
