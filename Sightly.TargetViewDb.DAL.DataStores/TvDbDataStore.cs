using System;
using System.Collections.Generic;
using System.Runtime.Remoting.Messaging;
using Newtonsoft.Json;
using Sightly.Business.Interfaces;
using Sightly.Models;
using Sightly.Models.tv;
using Sightly.Models.tv.extended;
using Sightly.Models.tv.extended.audiencegroups;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;

namespace Sightly.TargetViewDb.DAL.DataStores
{
    public class TvDbDataStore : DapperDataStore, ITvDbDataStore
    {
        public TvDbDataStore(IConnectionStringStore connectionStringStore) : base(connectionStringStore)
        {
        }

        #region Getters

        public AdMetaData GetAdMetaData(long customerId, long campaignId)
        {
            return QueryScalar<AdMetaData>(
                "tvdb.GetAdMetaData",
                new
                {
                    CustomerId = customerId,
                    CampaignId = campaignId
                });
        }

        public TvAccount GetAccountDataByName(string accountName)
        {
            return QueryScalar<TvAccount>(
                "tvdb.GetAccountByName",
                new
                {
                    AccountName = accountName
                });
        }

        public TvAccount GetAccountDataByCampaignName(string campaignName)
        {
            return QueryScalar<TvAccount>(
                "tvdb.GetAccountDataByCampaignName",
                new
                {
                    CampaignName = campaignName
                });
        }

        public TvAccount GetAccountDataByOrderName(string orderName)
        {
            return QueryScalar<TvAccount>(
                "tvdb.GetAccountDataByOrderName",
                new
                {
                    OrderName = orderName
                });
        }

        public TvAdvertiser GetAdvertiserDataByName(string advertiserName)
        {
            return QueryScalar<TvAdvertiser>(
                "tvdb.GetAdvertiserByName",
                new
                {
                    AdvertiserName = advertiserName
                });
        }

        public TvAdvertiser GetAdvertiserDataByCampaignName(string campaignName)
        {
            return QueryScalar<TvAdvertiser>(
                "tvdb.GetAdvertiserDataByCampaignName",
                new
                {
                    CampaignName = campaignName
                });
        }

        public TvAdvertiser GetAdvertiserDataByOrderName(string orderName)
        {
            return QueryScalar<TvAdvertiser>(
                "tvdb.GetAdvertiserDataByOrderName",
                new
                {
                    OrderName = orderName
                });
        }

        public TvAd GetAdByVideoAssetName(string videoAssetName)
        {
            return QueryScalar<TvAd>(
                "tvdb.GetAdByVideoAdName",
                new
                {
                    VideoAdName = videoAssetName
                });
        }

        public TvAdAdwordsInfo GetAdAdwordsInfo(long awAdId)
        {
            return QueryScalar<TvAdAdwordsInfo>(
                "tvdb.GetAdAdwordsInfoByAwAdId",
                new
                {
                    AwAdId = awAdId
                });
        }

        public TvBudgetGroupAdwordsInfo GetBudgetGroupAdwordsInfo(long campaignId)
        {
            return QueryScalar<TvBudgetGroupAdwordsInfo>(
                "tvdb.GetBudgetGroupAdwordsInfoByCampaign",
                new
                {
                    CampaignId = campaignId
                });
        }

        public TvBudgetGroup GetBudgetGroupByAWCampaignId(long awCampaignId)
        {
            return QueryScalar<TvBudgetGroup>(
                "tvdb.GetBudgetGroupByAWCampaignId",
                new
                {
                    AwCampaignId = awCampaignId
                });
        }

        public TvBudgetGroup GetBudgetGroupByNameAndBudget(string budgetGroupName, decimal budgetGroupBudget, Guid orderId)
        {
            return QueryScalar<TvBudgetGroup>(
                "tvdb.GetBudgetGroupByNameAndBudgetAndOrder",
                new
                {
                    BudgetGroupName = budgetGroupName,
                    BudgetGroupBudget = budgetGroupBudget,
                    OrderId = orderId
                });
        }

        public List<TvBudgetGroup> GetBudgetGroupByAwCustomerId(long customerId)
        {
            return QueryList<TvBudgetGroup>(
                "tvdb.GetBudgetGroupByAwCustomerId",
                new
                {
                    CustomerId = customerId
                });
        }

        public TvOrderXrefBudgetGroup GetOrderXrefBudgetGroup(Guid orderId, Guid budgetGroupId)
        {
            return QueryScalar<TvOrderXrefBudgetGroup>(
                "tvdb.GetOrderXrefBudgetGroup",
                new
                {
                    OrderId = orderId,
                    BudgetGroupId = budgetGroupId
                });
        }

        public TvBudgetGroupTimedBudget GetBudgetGroupTimedBudget(Guid budgetGroupId, DateTime startDate,
            Guid budgetGroupXrefPlacementId)
        {
            return QueryScalar<TvBudgetGroupTimedBudget>(
                "tvdb.GetBudgetGroupTimedBudget",
                new
                {
                    BudgetGroupId = budgetGroupId,
                    StartDate = startDate,
                    BudgetGroupXrefPlacementId = budgetGroupXrefPlacementId
                });
        }

        public TvBudgetGroupXrefAd GetBudgetGroupXrefAd(Guid budgetGroupId, Guid adId)
        {
            return QueryScalar<TvBudgetGroupXrefAd>(
                "tvdb.GetBudgetGroupXrefAd",
                new
                {
                    BudgetGroupId = budgetGroupId,
                    AdId = adId
                });
        }

        public TvBudgetGroupXrefLocation GetBudgetGroupXrefLocation(Guid budgetGroupId, Guid locationId)
        {
            return QueryScalar<TvBudgetGroupXrefLocation>(
                "tvdb.GetBudgetGroupXrefLocation",
                new
                {
                    BudgetGroupId = budgetGroupId,
                    LocationId = locationId
                });
        }

        public TvBudgetGroupXrefPlacement GetBudgetGroupXrefPlacement(Guid budgetGroupId, Guid placementId)
        {
            return QueryScalar<TvBudgetGroupXrefPlacement>(
                "tvdb.GetBudgetGroupXrefPlacement",
                new
                {
                    BudgetGroupId = budgetGroupId,
                    PlacementId = placementId
                });
        }

        public TvBudgetGroupAndPlacement GetBudgetGroupXrefPlacementIdByAwCampaignId(long awCampaignId)
        {
            return QueryScalar<TvBudgetGroupAndPlacement>(
                "tvdb.GetBudgetGroupXrefPlacementIdByAwCampaignId",
                new
                {
                    AwCampaignId = awCampaignId
                });
        }

        public TvCampaign GetCampaignDataByName(string campaignName)
        {
            return QueryScalar<TvCampaign>(
                "tvdb.GetCampaignByName",
                new
                {
                    CampaignName = campaignName
                });
        }

        public TvCampaign GetCampaignByOrderName(string orderName)
        {
            return QueryScalar<TvCampaign>(
                "tvdb.GetCampaignByOrderName", 
                new
                {
                    OrderName = orderName
                });
        }

        public TvCampaignAdWordsInfo GetCampaignAdwordsInfoDataByCustomerId(long customerId)
        {
            return QueryScalar<TvCampaignAdWordsInfo>(
                "tvdb.GetCampaignAdwordsInfoByCustomerId",
                new
                {
                    AdwordsCustomerId = customerId
                });
        }

        public bool IsCustomerLocked(long customerId)
        {
            return QueryScalar<bool>(
                "tvdb.IsCustomerLockedByCustomerId",
                new
                {
                    CustomerId = customerId
                });
        }

        public string GetTvCampaignNameByCustomerId(long customerId)
        {
            return QueryScalar<string>(
                "tvdb.GetTvCampaignNameByCustomerId",
                new
                {
                    CustomerId = customerId
                });
        }

        public TvLocation GetLocationByAccountAndName(Guid accountId, string locationName)
        {
            return QueryScalar<TvLocation>(
                "tvdb.GetLocationByAccountAndName",
                new
                {
                    AccountId = accountId,
                    LocationName = locationName
                });
        }

        public TvOrder GetOrderDataByName(string orderName)
        {
            return QueryScalar<TvOrder>(
                "tvdb.GetOrderByName",
                new
                {
                    OrderName = orderName
                });
        }

        public TvPlacement GetPlacementByNameAndValue(string placementName, string placementValue)
        {
            return QueryScalar<TvPlacement>(
                "tvdb.GetPlacementByNameAndValue",
                new
                {
                    PlacementName = placementName,
                    PlacementValue = placementValue
                });
        }

        public VideoAssetUpdateData GetVideoAssetDataByVideoAssetId(Guid videoAssetId)
        {
            return QueryScalar<VideoAssetUpdateData>(
                "tvdb.GetVideoAssetUpdateDataByVideoAssetId",
                new
                {
                    VideoAssetId = videoAssetId
                });
        }

        public TvVideoAsset GetVideoAssetDataByVideoAssetName(string videoAssetName)
        {
            return QueryScalar<TvVideoAsset>(
                "tvdb.GetVideoAssetByVideoAssetName",
                new
                {
                    VideoAssetName = videoAssetName
                });
        }

        public TvVideoAssetVersion GetVideoAssetVersionDataByYouTubeId(string youTubeId)
        {
            return QueryScalar<TvVideoAssetVersion>(
                "tvdb.GetVideoAssetVersionByYouTubeId",
                new
                {
                    YouTubeId = youTubeId
                });
        }

        public List<Guid> GetVideoAssetVersionsWithNoYoutubeId()
        {
            return QueryList<Guid>("tvdb.GetVideoAssetVersionsWithNoYoutubeId", null);
        }

        public string GetNewOrderBlobById(int id)
        {
            return QueryScalar<string>(
                "tvdb.NewOrderBlob_GetJson",
                new
                {
                    NewOrderBlobId = id
                });
        }

        public int SaveNewOrderBlob(string orderName, string orderRefCode, string orderJson, DateTime startDate, DateTime endDate)
        {
            return QueryScalar<int>(
                "tvdb.NewOrderBlob_Insert",
                new
                {
                    OrderName = orderName,
                    OrderRefCode = orderRefCode,
                    OrderBlob = orderJson,
                    StartDate = startDate,
                    EndDate = endDate,
                    Creator = "Create Order Service"
                });
        }

        public List<Order> SearchNewOrderBlob(string orderName, string orderRefCode, DateTime? startDate, DateTime? endDate)
        {
            return QueryList<Order>(
                "tvdb.NewOrderBlob_Search",
                new
                {
                    OrderName = orderName == string.Empty ? null: orderName,
                    OrderRefCode = orderRefCode == string.Empty ? null : orderRefCode,
                    StartDate = startDate,
                    EndDate = endDate
                });
        }

        public int SaveNewOrderBlob(string orderName, string orderRefCode, string orderJson, DateTime startDate, DateTime endDate,
            string accountName, string advertiserName, Guid advertiser, string campaignName, Guid campaign,
            decimal totalBudget)
        {
            return QueryScalar<int>(
                "tvdb.NewOrderBlob_InsertMore",
                new
                {
                    OrderName = orderName,
                    OrderRefCode = orderRefCode,
                    OrderBlob = orderJson,
                    StartDate = startDate,
                    EndDate = endDate,
                    Creator = "Create Order Service",
                    AccountName = accountName,
                    AdvertiserName = advertiserName,
                    AdvertiserId = advertiser,
                    CampaignName = campaignName,
                    CampaignId = campaign,
                    TotalBudget = totalBudget
                });
        }

        public Info GetOrderInfoByOrderId(Guid orderId)
        {
            return QueryScalar<Info>(
                "tvdb.GetOrderInfoByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public InfoExtended GetOrderInfoExtendedByOrderId(Guid orderId)
        {
            return QueryScalar<InfoExtended>("tvdb.GetOrderInfoExtendedByOrderId", new {OrderId = orderId});
        }

        public List<Ad> GetOrderAdsByOrderId(Guid orderId)
        {
            return QueryList<Ad>(
                "tvdb.GetOrderAdsByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public List<GeoData> GetOrderGeoSelectedServiceAreasByOrderId(Guid orderId)
        {
            return QueryList<GeoData>(
                "tvdb.GetOrderGeoSelectedServiceAreasByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public List<Guid> GetOrderAgeRangesByOrderId(Guid orderId)
        {
            return QueryList<Guid>(
                "tvdb.GetOrderAgeRangesByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public List<Guid> GetOrderAgeRangesExtendedByOrderId(Guid orderId)
        {
            return QueryList<Guid>(
                "tvdb.GetOrderAgeRangesByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public string GetOrdersCompetitorsUrlsByOrderId(Guid orderId)
        {
            return QueryScalar<string>(
                "tvdb.GetOrdersCompetitorsUrlsByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public List<Guid> GetOrdersGendersByOrderId(Guid orderId)
        {
            return QueryList<Guid>(
                "tvdb.GetOrdersGendersByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public List<short> GetOrdersHouseholdIncomeByOrderId(Guid orderId)
        {
            return QueryList<short>(
                "tvdb.GetOrdersHouseholdIncomeByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public string GetOrdersKeywordsByOrderId(Guid orderId)
        {
            return QueryScalar<string>(
                "tvdb.GetOrdersKeywordsByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public string GetOrderNotesByOrderId(Guid orderId)
        {
            return QueryScalar<string>(
                "tvdb.GetOrderNotesByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public List<Guid> GetOrderParentalStatusByOrderId(Guid orderId)
        {
            return QueryList<Guid>(
                "tvdb.GetOrderParentalStatusByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public List<OrderLite> SearchOrderLiteByString(string searchValue, Guid userId)
        {
            return QueryList<OrderLite>(
                "tvdb.GetOrder_SearchByStringAndUser",
                new
                {
                    Search = searchValue,
                    UserId = userId
                });
        }

        public OrderPauseData GetOrderPauseDataByOrderId(Guid orderId)
        {
            return QueryScalar<OrderPauseData>(
                "tvdb.GetOrderPauseDataByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public OrderCancelData GetOrderCancelDataByOrderId(Guid orderId)
        {
            return QueryScalar<OrderCancelData>(
                "tvdb.GetOrderCancelledDataByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public Guid? FindLiveBudgetGroupXrefPlacement(Guid budgetGroupXrefPlacementId)
        {
            return QueryScalar<Guid?>(
                "tvdb.FindLiveBudgetGroupXrefPlacement",
                new
                {
                    BudgetGroupXrefPlacementId = budgetGroupXrefPlacementId
                });
        }

        public List<OrderTargetAgeGroup> GetOrderAgeRangesExtended(Guid orderId)
        {
            return QueryList<OrderTargetAgeGroup>("[tvdb].[GetOrderAgeRangesExtendedByOrderId]", new { OrderId = orderId });
        }

        public List<OrderTargetGenderGroup> GetOrdersGendersExtended(Guid orderId)
        {
            return QueryList<OrderTargetGenderGroup>("[tvdb].[GetOrdersGendersExtendedByOrderId]", new { OrderId = orderId });
        }

        public List<OrderHouseholdIncomeGroup> GetOrdersHouseholdIncomeExtended(Guid orderId)
        {
            return QueryList<OrderHouseholdIncomeGroup>("[tvdb].[GetOrdersHouseholdIncomeExtendedByOrderId]", new { OrderId = orderId });
        }

        public List<OrderParentalStatusGroup> GetOrderParentalStatusExtended(Guid orderId)
        {
            return QueryList<OrderParentalStatusGroup>("[tvdb].[GetOrderParentalStatusExtendedByOrderId]", new { OrderId = orderId });
        }

        #endregion


        public Guid CreateLocationIfNotExisting(Guid orderId)
        {
            return QueryScalar<Guid>(
                "tvdb.CreateLocationIfNotExistingByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public void AddGeoIfNotExisting(Guid orderId, object locId, Guid geoId)
        {
            Execute(
                "tvdb.AddGeoIfNotExisting",
                new
                {
                    OrderId = orderId,
                    LocId = locId,
                    GeoId = geoId
                });
        }


        public void AddKeyWordsToOrderTargetKeyWords(Guid orderId, string keyWords)
        {
            Execute(
                "tvdb.AddKeywordsToOrderTargetKeyWord",
                new
                {
                    OrderId = orderId,
                    KeyWords = keyWords
                });
        }

        public void CreateProposedOrderAd(Guid orderId, int selectedAdFormat, string adNameInput, string youtubeUrlInput,
            string clickableUrlInput, string landingUrlInput, string campanionBannerUrl, bool pause)
        {
            Execute(
                "tvdb.CreateProposedOrderAds",
                new
                {
                    OrderId = orderId,
                    SelectedAdFormat = selectedAdFormat,
                    AdNameInput = adNameInput,
                    YoutubeUrlInput = youtubeUrlInput,
                    ClickableUrlInput = clickableUrlInput,
                    LandingUrlInput = landingUrlInput,
                    CampanionBannerUrl = campanionBannerUrl,
                    Pause = pause
                });
        }

        #region Setters

        public void InsertAccount(TvAccount account)
        {
            Execute(
                "tvdb.Account_Insert",
                new
                {
                    AccountId = account.AccountId,
                    AccountName = account.AccountName,
                    ShortCode = account.ShortCode,
                    AccountTypeId = account.AccountTypeId,
                    SightlyMargin = account.SightlyMargin,
                    ParentAccountId = account.ParentAccountId,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void InsertAdvertiser(TvAdvertiser advertiser)
        {
            Execute(
                "tvdb.Advertiser_Insert",
                new
                {
                    AdvertiserId = advertiser.AdvertiserId,
                    AccountId = advertiser.AccountId,
                    AdvertiserName = advertiser.AdvertiserName,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void InsertAd(TvAd ad)
        {
            Execute(
                "tvdb.Ad_Insert",
                new
                {
                    AdId = ad.AdId,
                    VideoAdName = ad.VideoAdName,
                    DestinationUrl = ad.DestinationUrl,
                    DisplayUrl = ad.DisplayUrl,
                    VideoAssetVersionId = ad.VideoAssetVersionId,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void InsertAdAdwordsInfo(TvAdAdwordsInfo tvAdAdwordsInfo)
        {
            Execute(
                "tvdb.AdAdWordsInfo_Insert",
                new
                {
                    AdAdWordsInfoId = tvAdAdwordsInfo.AdAdwordsInfoId,
                    AdId = tvAdAdwordsInfo.AdId,
                    BudgetGroupId = tvAdAdwordsInfo.BudgetGroupId,
                    AdWordsAdName = tvAdAdwordsInfo.AdWordsAdName,
                    AdWordsAdId = tvAdAdwordsInfo.AdWordsAdId,
                    AdWordsCampaignId = tvAdAdwordsInfo.AdWordsCampaignId,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void InsertBudgetGroup(TvBudgetGroup budgetGroup)
        {
            Execute(
                "tvdb.BudgetGroup_Insert",
                new
                {
                    BudgetGroupId = budgetGroup.BudgetGroupId,
                    BudgetGroupBudget = budgetGroup.BudgetGroupBudget,
                    BudgetGroupName = budgetGroup.BudgetGroupName,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void InsertBudgetGroupAdwordsInfo(TvBudgetGroupAdwordsInfo budgetGroupAdwordsInfo)
        {
            Execute(
                "tvdb.BudgetGroupAdwordsInfo_Insert",
                new
                {
                    BudgetGroupAdWordsInfoId = budgetGroupAdwordsInfo.BudgetGroupAdwordsInfoId,
                    BudgetGroupId = budgetGroupAdwordsInfo.BudgetGroupId,
                    AdwordsCustomerId = budgetGroupAdwordsInfo.AdwordsCustomerId,
                    AdwordsCampaignId = budgetGroupAdwordsInfo.AdwordsCampaignId,
                    AdWordsCampaignName = budgetGroupAdwordsInfo.AdwordsCampaignName,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void InsertBudgetGroupTimedBudget(TvBudgetGroupTimedBudget budgetGroupTimedBudget)
        {
            Execute(
                "tvdb.BudgetGroupTimedBudget_Insert",
                new
                {
                    BudgetGroupTimedBudgetId = budgetGroupTimedBudget.BudgetGroupTimedBudgetId,
                    BudgetGroupId = budgetGroupTimedBudget.BudgetGroupId,
                    BudgetAmount = budgetGroupTimedBudget.BudgetAmount,
                    StartDate = budgetGroupTimedBudget.StartDate,
                    EndDate = budgetGroupTimedBudget.EndDate,
                    Margin = budgetGroupTimedBudget.Margin,
                    BudgetGroupXrefPlacementId = budgetGroupTimedBudget.BudgetGroupXrefPlacementId,
                    OrderId = budgetGroupTimedBudget.OrderId,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void InsertBudgetGroupXrefAd(TvBudgetGroupXrefAd budgetGroupXrefAd)
        {
            Execute(
                "tvdb.BudgetGroupXrefAd_Insert",
                new
                {
                    BudgetGroupXrefAdId = budgetGroupXrefAd.BudgetGroupXrefAdId,
                    BudgetGroupId = budgetGroupXrefAd.BudgetGroupId,
                    AdId = budgetGroupXrefAd.AdId,
                    CreatedBy = budgetGroupXrefAd.CreatedBy
                });
        }

        public void InsertBudgetGroupXrefLocation(TvBudgetGroupXrefLocation budgetGroupXrefLocation)
        {
            Execute(
                "tvdb.BudgetGroupXrefLocation_Insert",
                new
                {
                    BudgetGroupXrefLocationId = budgetGroupXrefLocation.BudgetGroupXrefLocationId,
                    BudgetGroupId = budgetGroupXrefLocation.BudgetGroupId,
                    LocationId = budgetGroupXrefLocation.LocationId,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void InsertBudgetGroupXrefPlacement(TvBudgetGroupXrefPlacement budgetGroupXrefPlacement)
        {
            Execute(
                "tvdb.BudgetGroupXrefPlacement_Insert",
                new
                {
                    BudgetGroupXrefPlacementId = budgetGroupXrefPlacement.BudgetGroupXrefPlacementId,
                    BudgetGroupId = budgetGroupXrefPlacement.BudgetGroupId,
                    PlacementId = budgetGroupXrefPlacement.PlacementId,
                    AssignedBy = "Data Pipeline"
                });
        }

        public void InsertCampaign(TvCampaign campaign)
        {
            Execute(
                "tvdb.Campaign_Insert",
                new
                {
                    AccountId = campaign.AccountId,
                    AdvertiserId = campaign.AdvertiserId,
                    CampaignId = campaign.CampaignId,
                    CampaignName = campaign.CampaignName,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void InsertCampaignAdwordsInfo(TvCampaignAdWordsInfo campaignAdWordsInfo)
        {
            Execute(
                "tvdb.CampaignAdwordsInfoWithEmail_Insert",
                new
                {
                    CampaignAdWordsInfoId = campaignAdWordsInfo.CampaignAdWordsInfoId,
                    AdwordsCustomerId = campaignAdWordsInfo.AdWordsCustomerId,
                    AdvertiserId = campaignAdWordsInfo.AdvertiserId,
                    CampaignId = campaignAdWordsInfo.CampaignId,
                    CreatedBy = "Data Pipeline",
                    CampaignManagerEmail = campaignAdWordsInfo.CampaignMangerEmail
                });
        }

        public void InsertLocation(TvLocation location)
        {
            Execute(
                "tvdb.Location_Insert",
                new
                {
                    LocationId = location.LocationId,
                    AccountId = location.AccountId,
                    LocationName = location.LocationName,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void InsertOrder(TvOrder order)
        {
            Execute(
                "tvdb.Order_Insert_wObjCatId",
                new
                {
                    OrderId = order.OrderId,
                    OrderName = order.OrderName,
                    OrderRefCode = order.OrderRefCode,
                    AccountId = order.AccountId,
                    AdvertiserId = order.AdvertiserId,
                    CampaignId = order.CampaignId,
                    OrderStatusId = order.OrderStatusId,
                    CampaignBudget = order.CampaignBudget,
                    StartDate = order.StartDate,
                    EndDate = order.EndDate,
                    CreatedBy = "Data Pipeline",
                    ObjectiveCategoryId = order.ObjectiveCategoryId
                });
        }

        public void InsertOrderWithParent(TvOrder order)
        {
            Execute(
                "tvdb.Order_Insert_wParentObj",
                new
                {
                    OrderId = order.OrderId,
                    OrderName = order.OrderName,
                    OrderRefCode = order.OrderRefCode,
                    AccountId = order.AccountId,
                    AdvertiserId = order.AdvertiserId,
                    CampaignId = order.CampaignId,
                    OrderStatusId = order.OrderStatusId,
                    CampaignBudget = order.CampaignBudget,
                    StartDate = order.StartDate,
                    EndDate = order.EndDate,
                    CreatedBy = "Data Pipeline",
                    ObjectiveCategoryId = order.ObjectiveCategoryId,
                    Paused = order.Paused,
                    ParentOrderId = order.ParentOrderId
                });
        }

        public void InsertTargetAgeGroup(TvOrderTargetAgeGroup orderTargetAgeGroup)
        {
            Execute(
                "tvdb.OrderTargetAgeGroup_Insert",
                new
                {
                    OrderId = orderTargetAgeGroup.OrderId,
                    AgeGroupId = orderTargetAgeGroup.AgeGroupId,
                    LastModifiedBy = "Data Pipeline"
                });
        }

        public void InsertTargetGender(TvOrderTargetGender orderTargetGender)
        {
            Execute(
                "tvdb.OrderTargetGender_Insert",
                new
                {
                    OrderId = orderTargetGender.OrderId,
                    GenderId = orderTargetGender.GenderId,
                    LastModifiedBy = "Data Pipeline"
                });
        }

        public void InsertOrderXrefBudgetGroup(TvOrderXrefBudgetGroup orderXrefBudgetGroup)
        {
            Execute(
                "tvdb.OrderXrefBudgetGroup_Insert",
                new
                {
                    OrderXrefBudgetGroupId = orderXrefBudgetGroup.OrderXrefBudgetGroupId,
                    OrderId = orderXrefBudgetGroup.OrderId,
                    BudgetGroupId = orderXrefBudgetGroup.BudgetGroupId,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void InsertPlacement(TvPlacement placement)
        {
            Execute(
                "tvdb.Placement_Insert",
                new
                {
                    PlacementId = placement.PlacementId,
                    PlacementValue = placement.PlacementValue,
                    AdvertiserId = placement.AdvertiserId,
                    PlacementName = placement.PlacementName,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void InsertVideoAsset(TvVideoAsset videoAsset)
        {
            Execute(
                "tvdb.VideoAsset_Insert",
                new
                {
                    VideoAssetId = videoAsset.VideoAssetId,
                    AccountId = videoAsset.AccountId,
                    AdvertiserId = videoAsset.AdvertiserId,
                    VideoAssetFileName = videoAsset.VideoAssetName,
                    VideoAssetName = videoAsset.VideoAssetName,
                    VideoAssetRefCode = videoAsset.VideoAssetRefCode,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void InsertVideoAssetVersion(TvVideoAssetVersion videoAssetVersion)
        {
            Execute(
                "tvdb.VideoAssetVersion_Insert",
                new
                {
                    VideoAssetVersionId = videoAssetVersion.VideoAssetVersionId,
                    VideoAssetId = videoAssetVersion.VideoAssetId,
                    VideoAssetVersionName = videoAssetVersion.VideoAssetVersionName,
                    VideoAssetVersionRefCode = videoAssetVersion.VideoAssetVersionRefCode,
                    VideoLengthInSeconds = videoAssetVersion.VideoLengthInSeconds,
                    YouTubeId = videoAssetVersion.YouTubeId,
                    YouTubeUrl = videoAssetVersion.YouTubeUrl,
                    CreatedBy = "Data Pipeline"
                });
        }

        public void PrepareBudgetGroupTimedBudgetsIfReorder(long awcustomerId)
        {
            Execute(
                "tvdb.RemoveBudgetGroupTimedBudgetsForImportByAWCustomerId",
                new
                {
                    CustomerId = awcustomerId
                });
        }

        public void SaveVideoAssetIdToVideoAsset(Guid videoAssetId, string uploadResponse)
        {
            Execute(
                "[tvdb].[SaveVideoAssetIdToVideoAsset]",
                new
                {
                    VideoAssetId = videoAssetId,
                    YouTubeId = uploadResponse
                });
        }

        public TvBudgetGroupTimedBudget InsertHistoricalBudgetGroupTimedBudget(Guid budgetGroupTimedBudgetId,
            Guid budgetGroupId,
            decimal campaignBudget, DateTime startDate, DateTime endDate, decimal margin,
            Guid budgetGroupXrefPlacementId)
        {
            return QueryScalar<TvBudgetGroupTimedBudget>(
                "tvdb.BudgetGroupTimedBudgetHistory_Insert",
                new
                {
                    BudgetGroupTimedBudgetId = budgetGroupTimedBudgetId,
                    BudgetGroupId = budgetGroupId,
                    BudgetAmount = campaignBudget,
                    StartDate = startDate,
                    EndDate = endDate,
                    Margin = margin,
                    BudgetGroupXrefPlacementId = budgetGroupXrefPlacementId,
                    CreatedBy = "Sightly Updater"
                });
        }

        public void LockCustomer(long customerId, string email, string importType)
        {
            Execute("tvdb.LockCustomer",
                new
                {
                    CustomerId = customerId,
                    Email = email,
                    ImportType = importType
                });
        }

        public void UnlockCustomer(long customerId, string email)
        {
            Execute("tvdb.UnlockCustomer",
                new
                {
                    CustomerId = customerId,
                    Email = email
                });
        }

        public void SaveOrderVersionForHistory(Order order, Guid userId)
        {
            var orderJson = JsonConvert.SerializeObject(order);
            Execute("tvdb.OrderVersion_Insert",
            new
            {
                OrderId = order.Info.OrderId,
                OrderJson = orderJson,
                SavedBy = userId
            });
        }

        public void UpdateBudgetGroupStructureFromProposeToLive(Guid budgetGroupId)
        {
            Execute("tvdb.PushProposedToLive_StateMachine",
                new
                {
                    BudgetGroupId = budgetGroupId
                });
        }

        public void UpdateOrderInfoData(Guid orderId, DateTime startDate, DateTime endDate, decimal totalBudget)
        {
            Execute(
                "tvdb.UpdateOrderInfoData",
                new
                {
                    OrderId = orderId,
                    StartDate = startDate,
                    EndDate = endDate,
                    TotalBudget = totalBudget
                });
        }

        public bool SetOrderPausedStatus(Guid orderId)
        {
            return QueryScalar<bool>(
                "tvdb.SetOrderPausedStatusByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public bool SetOrderCancelled(Guid orderId)
        {
            return QueryScalar<bool>(
                "tvdb.SetOrderCancelledByOrderId",
                new
                {
                    OrderId = orderId
                });
        }

        public void AssociateNewOrderIdToParentWithBudgetGroup(Guid orderId, Guid parentOrderId)
        {
            Execute(
                "tvdb.AssociateNewOrderIdToParentWithBudgetGroup",
                new
                {
                    OrderId = orderId,
                    ParentOrderId = parentOrderId
                });
        }

        public void EnsureProperCampaignMangerAssignedToCustomer(long customerId, string campaignMangerEmail)
        {
            Execute(
                "tvdb.EnsureProperCampaignMangerAssignedToCustomer",
                new
                {
                    CustomerId = customerId,
                    CampaignManagerEmail = campaignMangerEmail
                });
        }

        public TvAdvertiser GetAdvertiserDataByCampaignId(Guid campaignId)
        {
            return QueryScalar<TvAdvertiser>(
                "tvdb.GetAdvertiserDataByCampaign",
                new
                {
                    CampaignId = campaignId
                });
        }

        public TvAccount GetAccountDataByCampaignId(Guid campaignId)
        {
            return QueryScalar<TvAccount>(
                "tvdb.GetAccountDataByCampaignId",
                new
                {
                    CampaignId = campaignId
                });
        }

        public TvCampaign GetCampaignByCampaignId(Guid campaignId)
        {
            return QueryScalar<TvCampaign>(
                "tvdb.GetCampaignByCampaign",
                new
                {
                    CampaignId = campaignId
                });
        }

        public AccountMetaData GetAdvertiserMetaData(Guid campaignId)
        {
            return QueryScalar<AccountMetaData>(
                "tvdb.GetAdvertiserMetaData",
                new
                {
                    CampaignId = campaignId
                });
        }

        public void RemoveLocationDetails(Guid orderId, Guid locId)
        {
            Execute("tvdb.LocationDetail_Delete", new { LocationId = locId });
        }

        public void DeleteOrderTargetAgeGroup(Guid orderId)
        {
            Execute("tvdb.DeleteOrderTargetAgeGroup", new { OrderId = orderId });
        }

        public void DeleteOrderTargetGender(Guid orderId)
        {
            Execute("tvdb.DeleteOrderTargetGender", new { OrderId = orderId });
        }

        public void DeleteOrderTargetParentalStatus(Guid orderId)
        {
            Execute("tvdb.DeleteOrderTargetParentalStatus", new { OrderId = orderId });
        }

        public void DeleteOrderTargetCompetitorsUrls(Guid orderId)
        {
            Execute("tvdb.DeleteOrderTargetCompetitorsUrls", new { OrderId = orderId });
        }

        public void DeleteOrderTargetHouseholdIncome(Guid orderId)
        {
            Execute("tvdb.DeleteOrderTargetHouseholdIncome", new { OrderId = orderId });
        }

        public void DeleteOrderTargetingNotes(Guid orderId)
        {
            Execute("tvdb.DeleteOrderTargetingNotes", new { OrderId = orderId });
        }

        public void DeleteOrderTargetingKeyWords(Guid orderId)
        {
            Execute("tvdb.DeleteOrderTargetingKeyWords", new { OrderId = orderId });
        }

        public TvPlacement GetPlacementByName(string placementName)
        {
            return QueryScalar<TvPlacement>(
                "tvdb.GetPlacementByName",
                new
                {
                    PlacementName = placementName
                });
        }

        public void AddAgeGroupToOrderTargetAgeGroups(Guid orderId, Guid age)
        {
            Execute(
                "tvdb.OrderTargetAgeGroup_Insert",
                new
                {
                    OrderId = orderId,
                    AgeGroupId =age,
                    LastModifiedBy = "Order Creation Service"
                });
        }

        public void AddGenderToOrderTargetGender(Guid orderId, Guid gender)
        {
            Execute(
                "tvdb.OrderTargetGender_Insert",
                new
                {
                    OrderId = orderId,
                    GenderId = gender,
                    LastModifiedBy = "Order Creation Service"
                });
        }

        public void AddParentalStatusToOrderTargetParentalStatus(Guid orderId, Guid par)
        {
            Execute(
                "tvdb.OrderTargetParentalStats_Insert",
                new
                {
                    OrderId = orderId,
                    ParentalStatusId = par,
                    CreatedBy = "Order Creation Service"
                });
        }

        public void AddUrlsToOrderCompetitorsUrl(Guid orderId, string competitorsUrls)
        {
            Execute(
                "tvdb.OrderTargetCompetitorsUrls_Insert",
                new
                {
                    OrderId = orderId,
                    CompetitorsUrls = competitorsUrls,
                    CreatedBy = "Order Creation Service"
                });
        }

        public void AddIncomeToOrderTargetIncome(Guid orderId, short hi)
        {
            Execute(
                "tvdb.OrderTargetHouseholdIncome_Insert",
                new
                {
                    OrderId  = orderId,
                    HouseholdIncomeGroupId = hi,
                    CreatedBy = "Order Creation Service"
                });
        }

        public void AddNoteToOrderTargetNote(Guid orderId, string note)
        {
            Execute(
                "tvdb.OrderTargetingNotes_Insert",
                new
                {
                    OrderId = orderId,
                    TargetingNotes = note
                });
        }
        #endregion

        #region Updater

        public TvBudgetGroupTimedBudget UpdateBudgetGroupTimedBudget(
            Guid budgetGroupTimedBudgetId,
            decimal campaignBudget, 
            DateTime startDate,
            DateTime endDate,
            decimal margin,
            Guid budgetGroupXrefPlacementId)
        {
            return QueryScalar<TvBudgetGroupTimedBudget>(
                "tvdb.UpdateBudgetGroupTimedBudget",
                new
                {
                    BudgetGroupTimedBudgetId = budgetGroupTimedBudgetId,
                    CampaignBudget = campaignBudget,
                    StartDate = startDate,
                    EndDate = endDate,
                    Margin = margin,
                    BudgetGroupXrefPlacementId = budgetGroupXrefPlacementId
                });
        }

        public void UpdateHistoricalBudgetGroupTimedBudget(
            Guid budgetGroupTimedBudgetId, 
            decimal campaignBudget,
            DateTime startDate,
            DateTime endDate,
            decimal margin,
            Guid budgetGroupXrefPlacementId)
        {
            Execute(
                "tvdb.UpdateHistoricalBudgetGroupTimedBudget",
                new
                {
                    LiveBudgetGroupTimedBudgetId = budgetGroupTimedBudgetId,
                    CampaignBudget = campaignBudget,
                    StartDate = startDate,
                    EndDate = endDate,
                    Margin = margin,
                    BudgetGroupXrefPlacementId = budgetGroupXrefPlacementId
                });
        }




        public void UpdateProposedBudgetGroupTimedBudget(
            Guid budgetGroupTimedBudgetId, 
            decimal campaignBudget, 
            DateTime startDate,
            DateTime endDate,
            decimal margin,
            Guid budgetGroupXrefPlacementId)
        {
            Execute(
                "tvdb.UpdateProposedBudgetGroupTimedBudget",
                new
                {
                    BudgetGroupTimedBudgetId = budgetGroupTimedBudgetId,
                    CampaignBudget = campaignBudget,
                    StartDate = startDate,
                    EndDate = endDate,
                    Margin = margin,
                    BudgetGroupXrefPlacementId = budgetGroupXrefPlacementId
                });
        }

        #endregion
    }
}