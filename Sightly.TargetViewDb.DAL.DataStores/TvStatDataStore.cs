using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using Sightly.Business.Interfaces;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;
using Sightly.TargetViewDb.DAL.Utilities;

namespace Sightly.TargetViewDb.DAL.DataStores
{
    public class TvStatDataStore : DapperDataStore,  ITvStatDataStore
    {
        private readonly IStatsXmlSerializer statsXmlSerializer;
        public TvStatDataStore(IConnectionStringStore connectionStringStore, IStatsXmlSerializer statsXmlSerializer) : base(connectionStringStore)
        {
            this.statsXmlSerializer = statsXmlSerializer;
        }

        public List<AccountCustomerData> GetChannelPartnerMccCustomerList()
        {
            return QueryList<AccountCustomerData>("stat.GetChannelPartnerMccCustomerList", null);
        }

        public List<BudgetGroupCampaignWithAdData> GetChannelPartnerCampaignList()
        {
            return QueryList<BudgetGroupCampaignWithAdData>("stat.GetChannelPartnerCampaignList", null);
        }

        public List<long> GetAllMediaAgencyMccCustomerList()
        {
            return QueryList<long>("tvdb.GetMediaAgencyMccCustomerList", null);
        }

        public List<long> GetAllRunningAdwordsCidsFromTvByDate(DateTime statDate)
        {
            return QueryList<long>("stat.RunningAdwordsOrder_GetList",
                new { GatherDate = statDate });
        }

        public List<AdwordsTvAssociationData> GetAdwordsTvAssociationData(long campaignId)
        {
            return QueryList<AdwordsTvAssociationData>("stat.GetAdwordsTvAssociations",
                new { CampaignId = campaignId });
        }

        public int? GetAdVideoLengthByAdId(Guid adId)
        {
            return QueryScalar<int?>(
                "Orders.AdLength_GetById",
                new
                {
                    AdId = adId
                }
            );
        }

        public List<long> GetCustomerIdsFromNielsenData(DateTime yesterday)
        {
            return QueryList<long>(
                "raw.GetCustomerIdsFromNielsenData",
                new
                {
                    StatDate = yesterday
                });
        }

        public List<long> GetDailyStatsRegatherByDate(DateTime statDate)
        {
            return QueryList<long>(
                "raw.DailyStatsRegatherByDate",
                new
                {
                    StatDate = statDate
                });
        }

        public List<long> GetMccCustomerList()
        {
            return QueryList<long>(
                "tvdb.GetMccCustomerList", null);
        }

        public List<long> GetNielsenCampaignsByCidAndDate(long customerId, DateTime date)
        {
            return QueryList<long>(
                "raw.GetNielsenCampaignsByCidAndDate",
                new
                {
                    CustomerId = customerId,
                    StatDate = date
                });
        }

        public List<long> GetNielsenCampaignsByDate(DateTime statDate)
        {
            return QueryList<long>(
                "raw.GetNielsenCampaignsByDate",
                new
                {
                    StatDate = statDate
                });
        }

        public CustomerStartEndData GetStartAndEndDateForCustomerById(long customerId)
        {
            return QueryScalar<CustomerStartEndData>(
                "tvdb.GetStartAndEndDateForCustomerById",
                new
                {
                    CustomerId = customerId
                });
        }

        public List<CustomerCampaignRawStatsByDayData> GetValidationForDailyRawDataByCustomerIdAndDate(long customerId, DateTime date)
        {
            return QueryList<CustomerCampaignRawStatsByDayData>(
                "tvdb.GetValidationForDailyRawDataByCustomerIdAndDate",
                new
                {
                    CustomerId = customerId,
                    StatDate = date
                });
        }

        public List<MissingCidStatDates> GetMissingCidAndStatsDates()
        {
            return QueryList<MissingCidStatDates>(
                "tvdb.GetMissingCidStatDates",
                null);
        }

        public List<MissingCidStatDates> GetMissingCidStatsByDate(DateTime statDate)
        {
            return QueryList<MissingCidStatDates>(
                "tvdb.GetMissingCidStatsByDate",
                new
                {
                    StatDate = statDate
                });
        }

        #region BudgetGroup Inserts

        public void InsertBudgetGroupStats(List<BudgetGroupStats> budgetGroupStats)
        {
            var statsXml = statsXmlSerializer.Execute(budgetGroupStats.ToArray());

            Execute("Stat.BudgetGroupStats_Save",
                new
                {
                    BudgetGroupStatsXml = statsXml,
                    CreatedBy = "Data Pitp"
                });
        }

        public void InsertBudgetGroupAdStats(List<BudgetGroupAdStats> budgetGroupAdStats)
        {
            //var statsXml = statsXmlSerializer.Execute(budgetGroupAdStats.ToArray());
            budgetGroupAdStats.ForEach(bgas =>
            {
                Execute(
                    "Stat.BudgetGroupAdStats_Insert",
                    new
                    {
                        BudgetGroupAdStatsId = bgas.BudgetGroupAdStatsId,
                        ActualSpend = bgas.ActualSpend,
                        AdId = bgas.AdId,
                        AudienceRetention25 = bgas.AudienceRetention25,
                        AudienceRetention50 = bgas.AudienceRetention50,
                        AudienceRetention75 = bgas.AudienceRetention75,
                        AudienceRetention100 = bgas.AudienceRetention100,
                        AverageCpv = bgas.AverageCpv,
                        BudgetGroupId = bgas.BudgetGroupId,
                        Clicks = bgas.Clicks,
                        ClickThroughRate = bgas.ClickThroughRate,
                        CompletedViews = bgas.CompletedViews,
                        Conversions = bgas.Conversions,
                        EstimatedCost = bgas.EstimatedCost,
                        Impressions = bgas.Impressions,
                        OrderId = bgas.OrderId,
                        PartialViews = bgas.PartialViews,
                        StatDate = bgas.StatDate,
                        ViewRate = bgas.ViewRate,
                        ViewThroughConversions = bgas.ViewThroughConversions,
                        ViewTime = bgas.ViewTime,
                        CreatedBy = "Data Pipeline"
                    }
                );
            });
            
        }

        public void InsertBudgetGroupAgeStats(List<BudgetGroupAgeStats> budgetGroupAgeStats)
        {
            //var statsXml = statsXmlSerializer.Execute(budgetGroupAgeStats.ToArray());
            budgetGroupAgeStats.ForEach(bgas =>
            {
                Execute("Stat.BudgetGroupAgeGroupStats_Insert",
                    new
                    {
                        BudgetGroupAgeGroupStatsId = bgas.BudgetGroupTargetAgeGroupStatsId,
                        ActualSpend = bgas.ActualSpend,
                        AgeGroupName = bgas.AdWordsAgeGroupName,
                        AudienceRetention25 = bgas.AudienceRetention25,
                        AudienceRetention50 = bgas.AudienceRetention50,
                        AudienceRetention75 = bgas.AudienceRetention75,
                        AudienceRetention100 = bgas.AudienceRetention100,
                        BudgetGroupId = bgas.BudgetGroupId,
                        Clicks = bgas.Clicks,
                        ClickThroughRate = bgas.ClickThroughRate,
                        CompletedViews = bgas.CompletedViews,
                        Conversions = bgas.Conversions,
                        EstimatedCost = bgas.EstimatedCost,
                        Impressions = bgas.Impressions,
                        OrderId = bgas.OrderId,
                        StatDate = bgas.StatDate,
                        ViewRate = bgas.ViewRate,
                        ViewThroughConversions = bgas.ViewThroughConversions,
                        CreatedBy = "Data Pipeline"
                    });
            });
            
        }

        public void InsertBudgetGroupDeviceStats(List<BudgetGroupDeviceStats> budgetGroupDeviceStats)
        {
            //var statsXml = statsXmlSerializer.Execute(budgetGroupDeviceStats.ToArray());
            budgetGroupDeviceStats.ForEach(bgds =>
            {
                //Console.WriteLine($"bgDevice: {bgds.AdWordsDeviceTypeName}: {bgds.BudgetGroupId}");
                Execute("Stat.BudgetGroupDeviceStats_Insert",
                new
                {
                    BudgetGroupDeviceStatsId = bgds.BudgetGroupDeviceStatsId,
                    ActualSpend = bgds.ActualSpend,
                    AudienceRetention25 = bgds.AudienceRetention25,
                    AudienceRetention50 = bgds.AudienceRetention50,
                    AudienceRetention75 = bgds.AudienceRetention75,
                    AudienceRetention100 = bgds.AudienceRetention100,
                    AverageCpv = bgds.AverageCpv,
                    BudgetGroupId = bgds.BudgetGroupId,
                    Clicks = bgds.Clicks,
                    ClickThroughRate = bgds.ClickThroughRate,
                    CompletedViews = bgds.CompletedViews,
                    Conversions = bgds.Conversions,
                    AdWordsDeviceTypeName = bgds.AdWordsDeviceTypeName,
                    EstimatedCost = bgds.EstimatedCost,
                    Impressions = bgds.Impressions,
                    OrderId = bgds.OrderId,
                    PartialViews = bgds.PartialViews,
                    StatDate = bgds.StatDate,
                    ViewRate = bgds.ViewRate,
                    ViewThroughConversions = bgds.ViewThroughConversions,
                    ViewTime = bgds.ViewTime,
                    CreatedBy = "Data Pipeline"
                });
            });
            
        }

        public void InsertBudgetGroupGenderStats(List<BudgetGroupGenderStats> budgetGroupGenderStats)
        {
            //var statsXml = statsXmlSerializer.Execute(budgetGroupGenderStats.ToArray());

            budgetGroupGenderStats.ForEach(bggs =>
            {
                Execute("Stat.BudgetGroupGenderStats_Insert",
                    new
                    {
                        BudgetGroupGenderStatsId = bggs.BudgetGroupTargetGenderStatsId,
                        ActualSpend = bggs.ActualSpend,
                        AudienceRetention25 = bggs.AudienceRetention25,
                        AudienceRetention50 = bggs.AudienceRetention50,
                        AudienceRetention75 = bggs.AudienceRetention75,
                        AudienceRetention100 = bggs.AudienceRetention100,
                        BudgetGroupId = bggs.BudgetGroupId,
                        Clicks = bggs.Clicks,
                        ClickThroughRate = bggs.ClickThroughRate,
                        CompletedViews = bggs.CompletedViews,
                        Conversions = bggs.Conversions,
                        EstimatedCost = bggs.EstimatedCost,
                        GenderName = bggs.AdwordsGenderName,
                        Impressions = bggs.Impressions,
                        OrderId = bggs.OrderId,
                        StatDate = bggs.StatDate,
                        ViewRate = bggs.ViewRate,
                        ViewThroughConversions = bggs.ViewThroughConversions,
                        CreatedBy = "Data Pipeline"
                    });
            });

        }

        public void InsertBudgetGroupStatsOneAtATime(BudgetGroupStats bgs)
        {
            Execute("Stat.BudgetGroupStats_Insert",
                new
                {
                    BudgetGroupStatsId = bgs.BudgetGroupStatsId,
                    BudgetGroupId = bgs.BudgetGroupId,
                    ActualSpend = bgs.ActualSpend,
                    Amount = bgs.Amount,
                    AudienceRetention25 = bgs.AudienceRetention25,
                    AudienceRetention50 = bgs.AudienceRetention50,
                    AudienceRetention75 = bgs.AudienceRetention75,
                    AudienceRetention100 = bgs.AudienceRetention100,
                    AverageCpc = bgs.AverageCpc,
                    AverageCpe = bgs.AverageCpe,
                    AverageCpm = bgs.AverageCpm,
                    AverageCpv = bgs.AverageCpv,
                    AveragePosition = bgs.AveragePosition,
                    Clicks = bgs.Clicks,
                    ClickThroughRate = bgs.ClickThroughRate,
                    CompletedViews = bgs.CompletedViews,
                    Conversions = bgs.Conversions,
                    CreatedBy = "Data Pipeline",
                    Engagements = bgs.Engagements,
                    Impressions = bgs.Impressions,
                    Interactions = bgs.Interactions,
                    OrderId = bgs.OrderId,
                    StatDate = bgs.StatDate,
                    ViewRate = bgs.ViewRate,
                    ViewThroughConversions = bgs.ViewThroughConversions
                });
        }



        #endregion

        #region Orders Insert

        public void InsertAdStats(List<AdStats> adStats)
        {
            //var statsXml = statsXmlSerializer.Execute(adStats.ToArray());
            adStats.ForEach(ads =>
            {
                 Execute(
                "Stat.AdStats_Insert",
                new
                {
                    AdStatsId = ads.AdStatsId,
                    ActualSpend = ads.ActualSpend,
                    AdId = ads.AdId,
                    AudienceRetention25 = ads.AudienceRetention25,
                    AudienceRetention50 = ads.AudienceRetention50,
                    AudienceRetention75 = ads.AudienceRetention75,
                    AudienceRetention100 = ads.AudienceRetention100,
                    AverageCpv = ads.AverageCpv,
                    Clicks = ads.Clicks,
                    ClickThroughRate = ads.ClickThroughRate,
                    CompletedViews = ads.CompletedViews,
                    Conversions = ads.Conversions,
                    EstimatedCost = ads.EstimatedCost,
                    Impressions = ads.Impressions,
                    OrderId = ads.OrderId,
                    PartialViews = ads.PartialViews,
                    StatDate = ads.StatDate,
                    ViewRate = ads.ViewRate,
                    ViewThroughConversions = ads.ViewThroughConversions,
                    ViewTime = ads.ViewTime,
                    CreatedBy = "Data Pipeline"
                }
            );
            });
           
        }

        public void InsertAgeGroupStats(List<AgeGroupStats> ageGroupStats)
        {
            //var statsXml = statsXmlSerializer.Execute(ageGroupStats.ToArray());

            ageGroupStats.ForEach(ags =>
            {
                Execute(
                    "Stat.OrderTargetAgeGroupStats_Insert",
                    new
                    {
                        AgeGroupStatsId = ags.OrderTargetAgeGroupStatsId,
                        ActualSpend = ags.ActualSpend,
                        AgeGroupName = ags.AgeGroupName,
                        AudienceRetention25 = ags.AudienceRetention25,
                        AudienceRetention50 = ags.AudienceRetention50,
                        AudienceRetention75 = ags.AudienceRetention75,
                        AudienceRetention100 = ags.AudienceRetention100,
                        Clicks = ags.Clicks,
                        ClickThroughRate = ags.ClickThroughRate,
                        CompletedViews = ags.CompletedViews,
                        Conversions = ags.Conversions,
                        EstimatedCost = ags.EstimatedCost,
                        Impressions = ags.Impressions,
                        OrderId = ags.OrderId,
                        StatDate = ags.StatDate,
                        ViewRate = ags.ViewRate,
                        ViewThroughConversions = ags.ViewThroughConversions,
                        CreatedBy = "Data Pipeline"
                    }
                );
            });

            
        }

        public void InsertDeviceStats(List<DeviceStats> deviceStats)
        {
            //var statsXml = statsXmlSerializer.Execute(deviceStats.ToArray());
            deviceStats.ForEach(ds =>
            {
                Execute(
                    "Stat.DeviceStats_Insert",
                    new
                    {
                        DeviceStatsId = ds.DeviceStatsId,
                        ActualSpend = ds.ActualSpend,
                        AudienceRetention25 = ds.AudienceRetention25,
                        AudienceRetention50 = ds.AudienceRetention50,
                        AudienceRetention75 = ds.AudienceRetention75,
                        AudienceRetention100 = ds.AudienceRetention100,
                        AverageCpv = ds.AverageCpv,
                        Clicks = ds.Clicks,
                        ClickThroughRate = ds.ClickThroughRate,
                        CompletedViews = ds.CompletedViews,
                        Conversions = ds.Conversions,
                        EstimatedCost = ds.EstimatedCost,
                        DeviceTypeName = ds.AdWordsDeviceTypeName,
                        Impressions = ds.Impressions,
                        OrderId = ds.OrderId,
                        StatDate = ds.StatDate,
                        ViewThroughConversions = ds.ViewThroughConversions,
                        ViewRate = ds.ViewRate,
                        ViewTime = ds.ViewTime,
                        PartialViews = ds.PartialViews,
                        CreatedBy = "Data Pipeline"
                    }
                );
            });
            
        }

        public void InsertGenderStats(List<GenderStats> newGenderStats)
        {
            //var statsXml = statsXmlSerializer.Execute(newGenderStats.ToArray());
            newGenderStats.ForEach(gs =>
            {
                Execute(
                    "Stat.OrderTargetGenderStats_Insert",
                    new
                    {
                        OrderTargetGenderStatsId = gs.OrderTargetGenderStatsId,
                        ActualSpend = gs.ActualSpend,
                        AudienceRetention25 = gs.AudienceRetention25,
                        AudienceRetention50 = gs.AudienceRetention50,
                        AudienceRetention75 = gs.AudienceRetention75,
                        AudienceRetention100 = gs.AudienceRetention100,
                        Clicks = gs.Clicks,
                        ClickThroughRate = gs.ClickThroughRate,
                        CompletedViews = gs.CompletedViews,
                        Conversions = gs.Conversions,
                        EstimatedCost = gs.EstimatedCost,
                        GenderName = gs.GenderName,
                        Impressions = gs.Impressions,
                        OrderId = gs.OrderId,
                        StatDate = gs.StatDate,
                        ViewRate = gs.ViewRate,
                        ViewThroughConversions = gs.ViewThroughConversions,
                        CreatedBy = "Data Pipeline"
                    }
                );
            });
            
        }

        public void InsertOrderStats(List<OrderStats> orderStats)
        {
            orderStats.ForEach(order =>
            {
                Execute("Stat.OrderStats_Insert",
                    new
                    {
                        OrderStatsId = order.OrderStatId,
                        OrderId = order.OrderId,
                        ActualSpend = order.ActualSpend,
                        Amount = order.Amount,
                        AudienceRetention25 = order.AudienceRetention25,
                        AudienceRetention50 = order.AudienceRetention50,
                        AudienceRetention75 = order.AudienceRetention75,
                        AudienceRetention100 = order.AudienceRetention100,
                        AverageCpc = order.AverageCpc,
                        AverageCpe = order.AverageCpe,
                        AverageCpm = order.AverageCpm,
                        AverageCpv = order.AverageCpv,
                        AveragePosition = order.AveragePosition,
                        Clicks = order.Clicks,
                        ClickThroughRate = order.ClickThroughRate,
                        CompletedViews = order.CompletedViews,
                        Conversions = order.Conversions,
                        CreatedBy = "Data Pipeline",
                        Engagements = order.Engagements,
                        Impressions = order.Impressions,
                        Interactions = order.Interactions,
                        StatDate = order.StatDate,
                        ViewRate = order.ViewRate,
                        ViewThroughConversions = order.ViewThroughConversions
                    });
            });
        }

        public void InsertDailyStatsValidation(CustomerRawStatsByDayDate reportedStats)
        {
            Execute("raw.DailyStatsValidation_Insert",
                new
                {
                    CustomerId = reportedStats.CustomerId,
                    StatDate = reportedStats.StatDate,
                    Clicks_AdoAge = Math.Round(reportedStats.Clicks_AdoAge,4),
                    Clicks_AdoGen = Math.Round(reportedStats.Clicks_AdoGen,4),
                    Clicks_AgeoGen = Math.Round(reportedStats.Clicks_AgeoGen,4),
                    Cost_AdoAge = Math.Round(reportedStats.Cost_AdoAge,4),
                    Cost_AdoGen = Math.Round(reportedStats.Cost_AdoGen,4),
                    Cost_AgeoGen = Math.Round(reportedStats.Cost_AgeoGen,4),
                    Impressions_AdoAge = Math.Round(reportedStats.Impressions_AdoAge,4),
                    Impressions_AdoGen = Math.Round(reportedStats.Impressions_AdoGen,4),
                    Impressions_AgeoGen = Math.Round(reportedStats.Impressions_AgeoGen,4),
                    Views_AdoAge = Math.Round(reportedStats.Views_AdoAge,4),
                    Views_AdoGen = Math.Round(reportedStats.Views_AdoGen,4),
                    Views_AgeoGen = Math.Round(reportedStats.Views_AgeoGen,4)
                });
        }

        public void InsertMissingCidAndDate(long customer, DateTime statDate)
        {
            Execute(
                "[raw].[MissingCidStatDate_Insert]",
                new
                {
                    CustomerId = customer,
                    StatDate = statDate
                });
        }

        public void SetAdwordsCustomerId(List<AdvertiserCustomerData> advertiserCustomerData)
        {
            Execute(
                "Stat.Campaign_SetAdWordsCustomerId",
                new { CampaignXml = GenerateCampaignXml(advertiserCustomerData) }
            );
        }

        public void SetAdwordsCampaignId(List<BudgetGroupCampaignData> budgetGroupCampaignData)
        {
            var orderXml = GenerateBudgetGroupAdWordsCampaignXml(budgetGroupCampaignData);
            Execute(
                "Stat.BudgetGroup_SetAdWordsCampaignId",
                new { OrderXml = orderXml }
            );
        }

        public void SetAdwrodsAdId(List<AdAwAdData> adAwAdData)
        {
            Execute(
                "Stat.Ad_SaveAdWordsInfo",
                new
                {
                    AdXml = GenerateAdMappingsXml(adAwAdData),
                    LastModifiedBy = "Data Pipeline"
                }
            );
        }

        public void DeleteMissingCidAndDate(long customerId, DateTime statDate)
        {
            Execute(
                "[raw].[MissingCidStatDate_Delete]",
                new
                {
                    CustomerId = customerId,
                    StatDate = statDate
                });
        }

        public void DeleteRawStatsData(long customer, DateTime statDate)
        {
            Execute(
                "[raw].[RawStats_Delete]",
                new
                {
                    CustomerId = customer,
                    StatDate = statDate
                }, 300);
        }

        public List<AdwordsTvAssociationData> GetAllRunningAdwordsOrdersFromTvByDate(DateTime statDate)
        {
            return QueryList<AdwordsTvAssociationData>(
                "tvdb.GetAllRunningAdwordsOrdersFromTvByDate",
                new
                {
                    StatDate = statDate
                });
        }

        public void DeleteRawAdGroupAndLocationStats(long customer, DateTime statdate)
        {
            Execute(
                "[raw].[RawAdGroupAndLocationStats_Delete]",
                new
                {
                    CustomerId = customer,
                    StatDate = statdate
                });
        }

        #endregion



        #region Private Methods
        private string GenerateAdMappingsXml(List<AdAwAdData> ads)
        {
            return GenerateXml(
                ads,
                "ads",
                "ad",
                new Dictionary<string, Func<AdAwAdData, object>>
                {
                    { "id", a => a.AdAdWordsInfoId },
                    { "aid", a => a.AdId },
                    { "awan", a => a.AdWordsAdName },
                    { "awaid", a => a.AdWordsAdId },
                    { "awcid", a => a.AdWordsCampaignId },
                    { "bgid", a => a.BudgetGroupId },
                }
            );
        }
        private string GenerateBudgetGroupAdWordsCampaignXml(List<BudgetGroupCampaignData> budgetGroupCampaignData)
        {
            return GenerateXml(
                budgetGroupCampaignData,
                "Orders",
                "Order",
                new Dictionary<string, Func<BudgetGroupCampaignData, object>>
                {
                    { "id", o => o.BudgetGroupId },
                    { "acid", o => o.AdWordsCampaignId },
                    { "lmb", o => o.LastModifiedBy },
                }
            );
        }

        private string GenerateCampaignXml(List<AdvertiserCustomerData> campaigns)
        {
            return GenerateXml(
                campaigns,
                "campaigns",
                "campaign",
                new Dictionary<string, Func<AdvertiserCustomerData, object>>
                {
                    { "id", c => c.AdvertiserId},
                    { "awcid", c => c.AdWordsCustomerId },
                    { "lmb", c => c.LastModifiedBy },
                }
            );
        }

        private string GenerateXml<T>(
            List<T> list,
            string parentElementName,
            string childElementName,
            Dictionary<string, Func<T, object>> attributeSelectors
        )
        {
            var parentElement = new XElement(parentElementName);

            foreach (var item in list)
            {
                var childElement = new XElement(childElementName);
                var attributes = attributeSelectors.Select(
                    a => new XAttribute(
                        a.Key,
                        a.Value(item) ?? string.Empty
                    )
                );

                childElement.Add(attributes);
                parentElement.Add(childElement);
            }

            return parentElement.ToString();
        }
        #endregion
    }
}