using System;
using Sightly.Business.Interfaces;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;

namespace Sightly.TargetViewDb.DAL.DataStores
{
    public class TvRawStatsDataStore : DapperDataStore, ITvRawStatsDataStore
    {
        public TvRawStatsDataStore(IConnectionStringStore connectionStringStore) : base(connectionStringStore)
        {
        }

        private int CalculateAudienceRetention(decimal value)
        {
            if (value > 0)
                return int.Parse(Math.Round(value).ToString());

            return 0;
        }

        public void InsertRawAdDeviceStat(AdDevicePerformanceFreeData adDeviceData)
        {
            Execute("[raw].[AdDeviceStats_Insert]",
                new
                {
                    AdGroupId = adDeviceData.AdGroupId,
                    AdId = adDeviceData.AdId,
                    AdName = adDeviceData.AdName,
                    CampaignId = adDeviceData.CampaignId,
                    CampaignName = adDeviceData.CampaignName,
                    Clicks = adDeviceData.Clicks,
                    Cost = adDeviceData.Cost,
                    Device = adDeviceData.DeviceName,
                    Impressions = adDeviceData.Impressions,
                    StatDate = adDeviceData.StatDate,
                    Views = adDeviceData.Views
                });
        }

        public void InsertRawAdDeviceStat(AdDevicePerformanceData adDeviceData)
        {
            Execute("[raw].[AdDeviceStatsWithPercentages_Insert]",
                new
                {
                    AdGroupId = adDeviceData.AdGroupId,
                    AdId = adDeviceData.AdId,
                    AdName = adDeviceData.AdName,
                    CampaignId = adDeviceData.CampaignId,
                    CampaignName = adDeviceData.CampaignName,
                    Clicks = adDeviceData.Clicks,
                    Cost = adDeviceData.Cost,
                    Device = adDeviceData.DeviceName,
                    Impressions = adDeviceData.Impressions,
                    StatDate = adDeviceData.StatDate,
                    Views = adDeviceData.Views,
                    AudienceRetention25 = adDeviceData.VideoQuartile25Rate,
                    AudienceRetention50 = adDeviceData.VideoQuartile50Rate,
                    AudienceRetention75 = adDeviceData.VideoQuartile75Rate,
                    AudienceRetention100 = adDeviceData.VideoQuartile100Rate,
                    CustomerId  = adDeviceData.CustomerId
                });
        }

        public void InsertRawAdGroupStat(AdGroupPerformanceFreeData adGroupData)
        {
            Execute("[raw].[AdGroupStats_Insert]",
                new
                {
                    AdGroupId = adGroupData.AdGroupId,
                    AdGroupName = adGroupData.AdGroupName,
                    CampaignId = adGroupData.CampaignId,
                    CampaignName = adGroupData.CampaignName,
                    StatDate = adGroupData.StatDate,
                    Clicks = adGroupData.Clicks,
                    Impressions = adGroupData.Impressions,
                    TotalViews = adGroupData.Views,
                    Cost = adGroupData.Cost,
                    CustomerId = adGroupData.CustomerId
                });
        }

        public void InsertRawAgeRangeStat(AgeRangePerformanceFreeData ageRangeData)
        {
            Execute("[raw].[AgeRangeStats_Insert]",
                new
                {
                    AdGroupId = ageRangeData.AdGroupId,
                    AgeRangeName = ageRangeData.AgeRangeName,
                    CampaignId = ageRangeData.CampaignId,
                    Clicks = ageRangeData.Clicks,
                    Cost = ageRangeData.Cost,
                    Impressions = ageRangeData.Impressions,
                    StatDate = ageRangeData.StatDate,
                    Views = ageRangeData.Views,
                    CustomerId = ageRangeData.CustomerId
                });
        }

        public void InsertRawGenderStat(GenderPerformanceFreeData genderData)
        {
            Execute("[raw].[GenderStats_Insert]",
                new
                {
                    AdGroupId = genderData.AdGroupId,
                    CampaignId = genderData.CampaignId,
                    Clicks = genderData.Clicks,
                    Cost = genderData.Cost,
                    Impressions = genderData.Impressions,
                    GenderName = genderData.GenderName,
                    StatDate = genderData.StatDate,
                    Views = genderData.Views,
                    CustomerId = genderData.CustomerId
                });
        }

        public void InsertRawGeoStat(GeoPerformanceFreeData geoData)
        {
            Execute("[raw].[GeoStats_Insert]",
                new
                {
                    AdGroupId = geoData.AdGroupId,
                    AdGroupName = geoData.AdGroupName,
                    CampaignId = geoData.CampaignId,
                    CampaignName = geoData.CampaignName,
                    CountryTerritory = geoData.CountryCriteriaId,
                    City = geoData.CityCriteriaId,
                    Region = geoData.RegionCriteriaId,
                    MetroArea = geoData.MetroCriteriaId,
                    MostSpecificLocation = geoData.MostSpecificCriteriaId,
                    StatDate = geoData.StatDate,
                    Impressions = geoData.Impressions,
                    TotalViews = geoData.VideoViews,
                    Clicks = geoData.Clicks,
                    Cost = geoData.Cost,
                    CustomerId = geoData.CustomerId
                });
        }

        public void InsertRawMoatDailyStat(MoatDailyStatData moatData)
        {
            Execute("[raw].[MoatStats_Insert]",
                new
                {
                    StatDate = moatData.StatDate,
                    AdvertiserId = moatData.AdvertiserId,
                    AdvertiserName = moatData.AdvertiserName,
                    ImpressionsAnalysed = moatData.Impressions,
                    LoadsUnfiltered = moatData.LoadsUnfiltered,
                    StrictOrPx2SecConsecVideoOtsUnfiltered = moatData.StrictOrPx2SecConsecVideoOtsUnfiltered,
                    ISomehowMeasurableUnfiltered = moatData.ISomehowMeasurableUnfiltered,
                    HumanAndAvoc = moatData.HumanAndAvoc,
                    MeasurableImpressions = moatData.MeasurableImpressions,
                    ReachedCompleteSum = moatData.ReachedCompleteSum
                });
        }

        public void CreateOrUpdateNielsenCampaignAndReference(NielsenCampaignReference camp)
        {
            Execute("[raw].[Nielsen_CreateOrUpdateNielsenCampaignAndReference]",
                new
                {
                    CampaignId = camp.CampaignId,
                    ParentCampaignId = camp.ParentCampaignId,
                    CampaignName = camp.CampaignName,
                    CampaignStartDate = camp.CampaignStartDate,
                    CampaignEndDate = camp.CampaignEndDate,
                    TargetDemo = camp.TargetDemo,
                    TargetStartAge = camp.TargetStartAge,
                    TargetEndAge = camp.TargetEndAge,
                    AdReferenceCampaignId = camp.AdReferenceCampaignId

                });
        }

        public void InsertNielsenCampaignData(NielsenCampaignReference campaignData)
        {
            Execute(
                "[raw].[Neilsen_Campaign_Insert]",
                new
                {
                    CampaignId = campaignData.CampaignId,
                    CampaignName = campaignData.CampaignName,
                    CampaignStartDate = campaignData.CampaignStartDate,
                    CampaignEndDate = campaignData.CampaignEndDate,
                    TargetDemo = campaignData.TargetDemo,
                    TargetStartAge = campaignData.TargetStartAge,
                    TargetEndAge = campaignData.TargetEndAge,
                    AdReferenceCampaignId = campaignData.AdReferenceCampaignId
                });
        }

        public void InsertNielsenCampaignPlacementData(NielsenCampaignSiteReference campaignPlacementData)
        {
            Execute(
                "[raw].[Neilsen_CampaignPlacement_Insert]",
                new
                {
                    CampaignId = campaignPlacementData.CampaignId,
                    SiteId = campaignPlacementData.SiteId,
                    SiteName = campaignPlacementData.SiteName,
                    PlacementId = campaignPlacementData.PlacementId,
                    TagPlacementId = campaignPlacementData.TagPlacementId,
                    PlacementName = campaignPlacementData.PlacementName,
                    PlacementStatus = campaignPlacementData.PlacementStatus
                });
        }

        public void InsertNielsenStatsExposure(NielsenCampaignPlacementExposure statsData)
        {
            Execute(
                "[raw].[Neilsen_Stats_Insert]",
                new
                {
                    StatDate = statsData.CampaignDataDate,
                    CampaignId = statsData.CampaignId,
                    PlatformId = statsData.PlatformId,
                    DemoId = statsData.DemoId,
                    PlacementId = statsData.PlacementId,
                    TagPlacementId = statsData.TagPlacementId,
                    Impressions = statsData.Impressions,
                    UniverseEstimate = statsData.UniverseEstimate
                });
        }

        public NielsenDailyDarStats GetDailyNielsenDarStat(long cid, DateTime yesterday)
        {
            return QueryScalar<NielsenDailyDarStats>(
                "raw.NielsenDarByCustomerAndDate",
                new
                {
                    CustomerId = cid,
                    StatDate = yesterday.Date
                });
        }

        public void InsertDailyNielsenDarStat(NielsenDailyDarStats dailyNielsenDarStat)
        {
            Execute(
                "[raw].[Nielsen_DailyDar_Insert]",
                new
                {
                    CustomerId = dailyNielsenDarStat.CustomerId,
                    StatDate = dailyNielsenDarStat.StatDate,
                    ComputerDar = dailyNielsenDarStat.ComputerDar,
                    MobileDar = dailyNielsenDarStat.MobileDar,
                    TotalDigitalDar = dailyNielsenDarStat.TotalDigitalDar
                });
            
        }


        public void InsertDoubleVerifyStats(DoubleVerifyStats statsData)
        {
            Execute(
                "[raw].[DoubleVerifyStats_Insert]",
                new
                {
                    PlacementId = statsData.PlacementId,
                    PlacementName = statsData.PlacementName,
                    StatDate = statsData.StatDate,
                    DeviceType = statsData.DeviceType,
                    MonitoredImpressions = statsData.MonitoredImpressions,
                    BrandSafetyOnTargetImpressions_1x1 = statsData.BrandSafetyOnTargetImpressions_1x1,
                    FraudSIVTImpressions = statsData.FraudSIVTImpressions,
                    FraudSIVTImpressions_1x1 = statsData.FraudSIVTImpressions_1x1,
                    InGeoImpressions_1x1 = statsData.InGeoImpressions_1x1,
                    VideoViewableImpressions = statsData.VideoViewableImpressions
                });
        }
    }
}