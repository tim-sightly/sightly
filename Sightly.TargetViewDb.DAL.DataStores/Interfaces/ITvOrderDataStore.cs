﻿using System;
using System.Collections.Generic;
using Sightly.Business.Enums;
using Sightly.Models;

namespace Sightly.TargetViewDb.DAL.DataStores.Interfaces
{
    public interface ITvOrderDataStore
    {
        void CreateOrderRecordLog(Guid orderRecordLogId, Guid orderId, string lastModifiedBy);

        void AssignPlacementWithAwCampaignId(long awCampaignId, string placement, string lastModifiedBy);
    }
}
