﻿using System;
using System.Collections.Generic;
using Sightly.Business.Enums;
using Sightly.Models;

namespace Sightly.TargetViewDb.DAL.DataStores.Interfaces
{
    public interface ITvStatDataStore
    {
        List<AccountCustomerData> GetChannelPartnerMccCustomerList();
        List<BudgetGroupCampaignWithAdData> GetChannelPartnerCampaignList();
        List<long> GetAllMediaAgencyMccCustomerList();
        List<long> GetAllRunningAdwordsCidsFromTvByDate(DateTime statDate);
        List<AdwordsTvAssociationData> GetAdwordsTvAssociationData(long campaignId);
        int? GetAdVideoLengthByAdId(Guid adId);
        List<long> GetCustomerIdsFromNielsenData(DateTime yesterday);
        List<long> GetDailyStatsRegatherByDate(DateTime statDate);
        List<long> GetMccCustomerList();
        List<long> GetNielsenCampaignsByCidAndDate(long customerId, DateTime date);
        List<long> GetNielsenCampaignsByDate(DateTime statDate);
        CustomerStartEndData GetStartAndEndDateForCustomerById(long customerId);
        List<CustomerCampaignRawStatsByDayData> GetValidationForDailyRawDataByCustomerIdAndDate(long customerId, DateTime date);

        List<MissingCidStatDates> GetMissingCidAndStatsDates();
        List<MissingCidStatDates> GetMissingCidStatsByDate(DateTime statDate);

        #region BudgetGroup Inserts

        void InsertBudgetGroupStats(List<BudgetGroupStats> budgetGroupStats);
        
        void InsertBudgetGroupAdStats(List<BudgetGroupAdStats> budgetGroupAdStats);

        void InsertBudgetGroupAgeStats(List<BudgetGroupAgeStats> budgetGroupAgeStats);

        void InsertBudgetGroupDeviceStats(List<BudgetGroupDeviceStats> budgetGroupDeviceStats);

        void InsertBudgetGroupGenderStats(List<BudgetGroupGenderStats> budgetGroupGenderStats);
        void InsertBudgetGroupStatsOneAtATime(BudgetGroupStats bgs);

        #endregion

        #region Order Inserts

        void InsertAdStats(List<AdStats> adStats);
        void InsertAgeGroupStats(List<AgeGroupStats> ageGroupStats);
        void InsertDeviceStats(List<DeviceStats> deviceStats);
        void InsertGenderStats(List<GenderStats> newGenderStats);
        void InsertOrderStats(List<OrderStats> orderStats);

        #endregion


        void InsertDailyStatsValidation(CustomerRawStatsByDayDate reportedStats);
        void InsertMissingCidAndDate(long customer, DateTime statDate);

        void SetAdwordsCustomerId(List<AdvertiserCustomerData> advertiserCustomerData);
        void SetAdwordsCampaignId(List<BudgetGroupCampaignData> budgetGroupCampaignData);
        void SetAdwrodsAdId(List<AdAwAdData> adAwAdData);


        void DeleteMissingCidAndDate(long customerId, DateTime statDate);
        void DeleteRawStatsData(long customer, DateTime statDate);
        List<AdwordsTvAssociationData> GetAllRunningAdwordsOrdersFromTvByDate(DateTime statDate);
		void DeleteRawAdGroupAndLocationStats(long customer, DateTime statdate);
    }
}