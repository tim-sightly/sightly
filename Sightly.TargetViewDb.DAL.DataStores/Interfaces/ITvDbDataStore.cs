using System;
using System.Collections.Generic;
using Sightly.Models;
using Sightly.Models.tv;
using Sightly.Models.tv.extended;
using Sightly.Models.tv.extended.audiencegroups;

namespace Sightly.TargetViewDb.DAL.DataStores.Interfaces
{
    public interface ITvDbDataStore
    {
        #region Getters
        AdMetaData GetAdMetaData(long customerId, long campaignId);
        TvAccount GetAccountDataByName(string accountName);
        TvAccount GetAccountDataByCampaignName(string campaignName);
        TvAccount GetAccountDataByOrderName(string orderName);
        TvAdvertiser GetAdvertiserDataByName(string advertiserName);
        TvAdvertiser GetAdvertiserDataByCampaignName(string campaignName);
        TvAdvertiser GetAdvertiserDataByOrderName(string campaignName);
        TvAd GetAdByVideoAssetName(string videoAssetName);
        TvAdAdwordsInfo GetAdAdwordsInfo(long awAdId);
        TvBudgetGroupAdwordsInfo GetBudgetGroupAdwordsInfo(long campaignId);
        TvBudgetGroup GetBudgetGroupByAWCampaignId(long awCampaignId);
        TvBudgetGroup GetBudgetGroupByNameAndBudget(string budgetGroupName, decimal budgetGroupBudget, Guid orderId);
        List<TvBudgetGroup> GetBudgetGroupByAwCustomerId(long customerId);
        TvOrderXrefBudgetGroup GetOrderXrefBudgetGroup(Guid orderId, Guid budgetGroupId);
        TvBudgetGroupTimedBudget GetBudgetGroupTimedBudget(Guid budgetGroupId, DateTime startDate, Guid budgetGroupXrefPlacementId);
        TvBudgetGroupXrefAd GetBudgetGroupXrefAd(Guid budgetGroupId, Guid adId);
        TvBudgetGroupXrefLocation GetBudgetGroupXrefLocation(Guid budgetGroupId, Guid locationId);
        TvBudgetGroupXrefPlacement GetBudgetGroupXrefPlacement(Guid budgetGroupId, Guid placementId);
        TvBudgetGroupAndPlacement GetBudgetGroupXrefPlacementIdByAwCampaignId(long awCampaignId);
        TvCampaign GetCampaignDataByName(string campaignName);
        TvCampaign GetCampaignByOrderName(string orderName);
        TvCampaignAdWordsInfo GetCampaignAdwordsInfoDataByCustomerId(long customerId);
        bool IsCustomerLocked(long customerId);
        string GetTvCampaignNameByCustomerId(long customerId);
        TvLocation GetLocationByAccountAndName(Guid accountId, string locationName);
        TvOrder GetOrderDataByName(string orderName);
        TvPlacement GetPlacementByNameAndValue(string placementName, string placementValue);
        VideoAssetUpdateData GetVideoAssetDataByVideoAssetId(Guid videoAssetId);
        TvVideoAsset GetVideoAssetDataByVideoAssetName(string videoAssetName);
        TvVideoAssetVersion GetVideoAssetVersionDataByYouTubeId(string youTubeId);
        List<Guid> GetVideoAssetVersionsWithNoYoutubeId();
        string GetNewOrderBlobById(int id);
        int SaveNewOrderBlob(string orderName, string orderRefCode, string orderJson, DateTime startDate, DateTime endDate);
        List<Order> SearchNewOrderBlob(string orderName, string orderRefCode, DateTime? startDate, DateTime? endDate);
        int SaveNewOrderBlob(string orderName, string orderRefCode, string orderJson, DateTime startDate, DateTime endDate, string accountName, string advertiserName, Guid advertiser, string campaignName, Guid campaign, decimal totalBudget);
        Info GetOrderInfoByOrderId(Guid orderId);
        InfoExtended GetOrderInfoExtendedByOrderId(Guid orderId);
        List<Ad> GetOrderAdsByOrderId(Guid orderId);
        List<GeoData> GetOrderGeoSelectedServiceAreasByOrderId(Guid orderId);
        List<Guid> GetOrderAgeRangesByOrderId(Guid orderId);
        string GetOrdersCompetitorsUrlsByOrderId(Guid orderId);
        List<Guid> GetOrdersGendersByOrderId(Guid orderId);
        List<short> GetOrdersHouseholdIncomeByOrderId(Guid orderId);
        string GetOrdersKeywordsByOrderId(Guid orderId);
        string GetOrderNotesByOrderId(Guid orderId);
        List<Guid> GetOrderParentalStatusByOrderId(Guid orderId);
        List<OrderLite> SearchOrderLiteByString(string searchValue, Guid userId);
        OrderPauseData GetOrderPauseDataByOrderId(Guid orderId);
        OrderCancelData GetOrderCancelDataByOrderId(Guid orderId);
        Guid? FindLiveBudgetGroupXrefPlacement(Guid budgetGroupXrefPlacementId);
        List<OrderTargetAgeGroup> GetOrderAgeRangesExtended(Guid orderId);
        List<OrderTargetGenderGroup> GetOrdersGendersExtended(Guid orderId);
        List<OrderHouseholdIncomeGroup> GetOrdersHouseholdIncomeExtended(Guid orderId);
        List<OrderParentalStatusGroup> GetOrderParentalStatusExtended(Guid orderId);

        #endregion


        Guid CreateLocationIfNotExisting(Guid orderId);
        void AddGeoIfNotExisting(Guid orderId, object locId, Guid geoId);

        #region Setters

        TvPlacement GetPlacementByName(string placementName);
        void AddAgeGroupToOrderTargetAgeGroups(Guid orderId, Guid age);
        void AddGenderToOrderTargetGender(Guid orderId, Guid gender);
        void AddParentalStatusToOrderTargetParentalStatus(Guid orderId, Guid par);
        void AddUrlsToOrderCompetitorsUrl(Guid orderId, string competitorsUrls);
        void AddIncomeToOrderTargetIncome(Guid orderId, short hi);
        void AddNoteToOrderTargetNote(Guid orderId, string note);
        void AddKeyWordsToOrderTargetKeyWords(Guid orderId, string keyWords);
        void CreateProposedOrderAd(Guid orderId, int selectedAdFormat, string adNameInput, string youtubeUrlInput, string clickableUrlInput, string landingUrlInput, string campanionBannerUrl, bool pause);
        void InsertAccount(TvAccount account);
        void InsertAdvertiser(TvAdvertiser advertiser);
        void InsertAd(TvAd ad);
        void InsertAdAdwordsInfo(TvAdAdwordsInfo tvAdAdwordsInfo);
        void InsertBudgetGroup(TvBudgetGroup budgetGroup);
        void InsertBudgetGroupAdwordsInfo(TvBudgetGroupAdwordsInfo budgetGroupAdwordsInfo);
        void InsertBudgetGroupTimedBudget(TvBudgetGroupTimedBudget budgetGroupTimedBudget);
        void InsertBudgetGroupXrefAd(TvBudgetGroupXrefAd budgetGroupXrefAd);
        void InsertBudgetGroupXrefLocation(TvBudgetGroupXrefLocation budgetGroupXrefLocation);
        void InsertBudgetGroupXrefPlacement(TvBudgetGroupXrefPlacement budgetGroupXrefPlacement);
        void InsertCampaign(TvCampaign campaign);
        void InsertCampaignAdwordsInfo(TvCampaignAdWordsInfo campaignAdWordsInfo);
        void InsertLocation(TvLocation location);
        void InsertOrder(TvOrder order);
        void InsertOrderWithParent(TvOrder order);
        void InsertTargetAgeGroup(TvOrderTargetAgeGroup orderTargetAgeGroup);
        void InsertTargetGender(TvOrderTargetGender orderTargetGender);

        void InsertOrderXrefBudgetGroup(TvOrderXrefBudgetGroup orderXrefBudgetGroup);

        void InsertPlacement(TvPlacement placement);
        void InsertVideoAsset(TvVideoAsset videoAsset);

        void InsertVideoAssetVersion(TvVideoAssetVersion videoAssetVersion);
        void PrepareBudgetGroupTimedBudgetsIfReorder(long awcustomerId);
        void SaveVideoAssetIdToVideoAsset(Guid videoAssetId, string uploadResponse);
        TvBudgetGroupTimedBudget InsertHistoricalBudgetGroupTimedBudget(Guid budgetGroupTimedBudgetId, Guid budgetGroupId, decimal campaignBudget, DateTime startDate, DateTime endDate, decimal margin, Guid budgetGroupXrefPlacementId);

        void LockCustomer(long customerId, string email, string importType);
        void UnlockCustomer(long customerId, string email);
        void SaveOrderVersionForHistory(Order order, Guid userId);
        #endregion

        #region Updater
        TvBudgetGroupTimedBudget UpdateBudgetGroupTimedBudget(Guid budgetGroupTimedBudgetId, decimal campaignBudget, DateTime startDate, DateTime endDate, decimal margin, Guid budgetGroupXrefPlacementId);

        void UpdateHistoricalBudgetGroupTimedBudget(Guid budgetGroupTimedBudgetId, decimal campaignBudget, DateTime startDate, DateTime endDate, decimal margin, Guid budgetGroupXrefPlacementId);
        void UpdateProposedBudgetGroupTimedBudget(Guid budgetGroupTimedBudgetId, decimal campaignBudget, DateTime startDate, DateTime endDate, decimal margin, Guid budgetGroupXrefPlacementId);

        void UpdateBudgetGroupStructureFromProposeToLive(Guid budgetGroupId);
        void UpdateOrderInfoData(Guid orderId, DateTime startDate, DateTime endDate, decimal totalBudget);
        bool SetOrderPausedStatus(Guid orderId);
        bool SetOrderCancelled(Guid orderId);
        void AssociateNewOrderIdToParentWithBudgetGroup(Guid orderId, Guid parentOrderId);
        void EnsureProperCampaignMangerAssignedToCustomer(long customerId, string campaignMangerEmail);
        #endregion

        TvAdvertiser GetAdvertiserDataByCampaignId(Guid campaignId);
        TvAccount GetAccountDataByCampaignId(Guid campaignId);
        TvCampaign GetCampaignByCampaignId(Guid campaignId);
        AccountMetaData GetAdvertiserMetaData(Guid campaignId);

        #region Deleters

        void RemoveLocationDetails(Guid orderId, Guid locId);
        void DeleteOrderTargetAgeGroup(Guid orderId);
        void DeleteOrderTargetGender(Guid orderId);
        void DeleteOrderTargetParentalStatus(Guid orderId);
        void DeleteOrderTargetCompetitorsUrls(Guid orderId);
        void DeleteOrderTargetHouseholdIncome(Guid orderId);
        void DeleteOrderTargetingNotes(Guid orderId);
        void DeleteOrderTargetingKeyWords(Guid orderId);


        #endregion

    }
}