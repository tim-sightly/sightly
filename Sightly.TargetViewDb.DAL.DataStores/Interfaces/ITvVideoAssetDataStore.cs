using Sightly.Models;

namespace Sightly.TargetViewDb.DAL.DataStores.Interfaces
{
    public interface ITvVideoAssetDataStore
    {
        void CreateVideoAsset(VideoAssetEntityData videoAsset);
    }
}