using Sightly.Models;

namespace Sightly.TargetViewDb.DAL.DataStores.Interfaces
{
    public interface ITvExteralResourcesDataStore
    {
        void InsertMoatStat(MoatDailyStatData moatStatData);
    }
}