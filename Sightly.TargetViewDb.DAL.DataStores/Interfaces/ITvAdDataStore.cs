﻿using Sightly.Models;

namespace Sightly.TargetViewDb.DAL.DataStores.Interfaces
{
    public interface ITvAdDataStore
    {
        void CreateAdAdWordsInfo(AdAdWordsInfoData adAdwordsInfo);
    }
}
