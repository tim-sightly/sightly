﻿using System;
using Sightly.Business.Enums;
using Sightly.Models;

namespace Sightly.TargetViewDb.DAL.DataStores.Interfaces
{
    public interface ITvRawStatsDataStore
    {
        #region Adwords
        void InsertRawAdDeviceStat(AdDevicePerformanceFreeData adDeviceData);
        void InsertRawAdDeviceStat(AdDevicePerformanceData adDeviceData);
        void InsertRawAdGroupStat(AdGroupPerformanceFreeData adGroupData);
        void InsertRawAgeRangeStat(AgeRangePerformanceFreeData ageRangeData);
        void InsertRawGenderStat(GenderPerformanceFreeData genderData);
        void InsertRawGeoStat(GeoPerformanceFreeData geoData);
        #endregion

        #region Moat
        void InsertRawMoatDailyStat(MoatDailyStatData moatData);
        #endregion

        #region Nielsen
        void CreateOrUpdateNielsenCampaignAndReference(NielsenCampaignReference camp);
        void InsertNielsenCampaignData(NielsenCampaignReference campaignData);
        void InsertNielsenCampaignPlacementData(NielsenCampaignSiteReference campaignPlacementData);
        void InsertNielsenStatsExposure(NielsenCampaignPlacementExposure statsData);
        NielsenDailyDarStats GetDailyNielsenDarStat(long cid, DateTime yesterday);
        void InsertDailyNielsenDarStat(NielsenDailyDarStats dailyNielsenDarStat);
        #endregion

        #region DoubleVerify
        void InsertDoubleVerifyStats(DoubleVerifyStats statsData);
        #endregion

    }
}
