﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.TargetViewDb.DAL.DataStores.Interfaces
{
    public interface ITargetViewDataStore
    {
        List<TargetViewAdwordsData> GetTargetViewAdwordsAssociations(long customerId);

        List<TargetViewAdwordsAssociatedData> GetAssociatedTargetViewAdwordsAssociations(DateTime statDate);
        List<VideoAssetData> GetTargetViewVideoAssetsByAdvertiser(Guid advertiserId);

        AdvertiserEntityData GetAdvertiserEntityData(long campaignId);

        List<TvCampaignAdWordsInfo> GetListWithoutAdwordsCustomerId(DateTime liveOrderDate);

        void UpdateCampaignAdWordsInfoWithCustomerId(Guid campaignAdWordsInfoId, long customerId, string lastModifedBy);

        void UpdateBudgetGroupAdWordsInfoWithCampaignId(Guid budgetGroupId, long campaignId, string campaignName, string lastModifiedBy);

        void AddAdWordsInfoWithAdId(Guid adId, string awAdName, long awAdId, long awCampaignId, Guid budgetGroupId, string lastModifiedBy);
    }
}