using Sightly.Business.Interfaces;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;

namespace Sightly.TargetViewDb.DAL.DataStores
{
    public class TvAdDataStore : DapperDataStore, ITvAdDataStore
    {
        public TvAdDataStore(IConnectionStringStore connectionStringStore) : base(connectionStringStore)
        {
        }

        public void CreateAdAdWordsInfo(AdAdWordsInfoData adAdwordsInfo)
        {
            Execute("Orders.AdAdWordsInfo_Insert",
                new
                {
                    AdId = adAdwordsInfo.AdId,
                    OrderId = adAdwordsInfo.OrderId,
                    BudgetGroupId = adAdwordsInfo.BudgetGroupId,
                    AdWordsAdName = adAdwordsInfo.AdWordsAdName,
                    AdWordsAdId = adAdwordsInfo.AdWordsAdId,
                    LastModified = adAdwordsInfo.LastModified
                }
            );
        }
    }
}