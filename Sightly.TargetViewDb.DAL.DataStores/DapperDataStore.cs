﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Sightly.Business.Enums;
using Sightly.Business.Interfaces;
using Sightly.Models;

namespace Sightly.TargetViewDb.DAL.DataStores
{
    public class DapperDataStore
    {
        //private const string ConnectionString = "data source=DESKTOP-F32JH43\\SQL7DE;initial catalog=TargetViewDb;integrated security=True;MultipleActiveResultSets=True;App=EntityFramework";

        private ConnectionStringKey? connectionStringKey;
        private IConnectionStringStore connectionStringStore;

        public DapperDataStore(IConnectionStringStore connectionStringStore)
        {
            this.connectionStringStore = connectionStringStore;
        }

        public DapperDataStore(IConnectionStringStore connectionStringStore, ConnectionStringKey connectionStringKey) : this(connectionStringStore)
        {
            this.connectionStringKey = connectionStringKey;
        }

        private DbConnection CreateDbConnection()
        {
            var key = connectionStringKey ?? ConnectionStringKey.TargetView;
            var connectionString = connectionStringStore.GetKeyValue(key);
            return new SqlConnection(connectionString);
        }

        protected async Task<T> WithConnection<T>(Func<IDbConnection, Task<T>> getData)
        {
            using (var connection = CreateDbConnection())
            {
                await connection.OpenAsync();
                return await getData(connection);
            }
        }

        protected async Task<TResult> WithConnection<TRead, TResult>(Func<IDbConnection, Task<TRead>> getData, Func<TRead, Task<TResult>> process)
        {
            using (var connection = CreateDbConnection())
            {
                await connection.OpenAsync();
                var data = await getData(connection);
                return await process(data);
            }
        }

        protected void AdInputParameter(DynamicParameters parameters, string name, object value)
        {
            parameters.Add("@"+ name, value, direction: ParameterDirection.Input);
        }

        protected void AddOutputParameter(DynamicParameters parameters, string name)
        {
            parameters.Add("@" + name, direction: ParameterDirection.Output);
        }

        protected void AddOutputParameter(DynamicParameters parameters, string name, DbType dbType)
        {
            parameters.Add("@" + name, direction: ParameterDirection.Output, dbType: dbType);
        }

        protected void Execute(string storedProcedureName)
        {
            Execute(storedProcedureName, null);
        }

        protected void Execute(string storedProcedureName, object parameterSet)
        {
            Execute(storedProcedureName, parameterSet, 30);
        }

        protected void Execute(string storedProcedureName, object parameterSet, int timeout)
        {
            using (var connection = CreateDbConnection())
            {
                connection.Execute(storedProcedureName, parameterSet, commandType: CommandType.StoredProcedure, commandTimeout: timeout, transaction: null);
                connection.Close();
            }
        }

        public T QueryScalar<T>(string storedProcedureName)
        {
            return QueryScalar<T>(storedProcedureName, null);
        }
        
        public T QueryScalar<T>(string storedProcedureName, object parameterSet)
        {
            using (var dbConnection = CreateDbConnection())
            {
                return dbConnection.Query<T>(
                           storedProcedureName,
                           parameterSet,
                           commandType: CommandType.StoredProcedure,
                           commandTimeout: 120
                       ).FirstOrDefault();
            }
        }

        public List<T> QueryList<T>(string storedProcedureName, object parameterSet)
        {
            using (var dbConnection = CreateDbConnection())
            {
                return dbConnection.Query<T>(
                           storedProcedureName,
                           parameterSet,
                           commandType: CommandType.StoredProcedure
                       ).ToList();
            }
        }

        public List<T> QueryList<T>(string storedProcedureName, DynamicParameters parameterSet)
        {
            using (var dbConnection = CreateDbConnection())
            {
                return dbConnection.Query<T>(
                           storedProcedureName,
                           parameterSet,
                           commandType: CommandType.StoredProcedure
                       ).ToList();
            }
        }

        
    }
}
