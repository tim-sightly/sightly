﻿using Google.Apis.YouTube.v3;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Google.Apis.Upload;
using Google.Apis.YouTube.v3.Data;
using System.Diagnostics;

namespace Sightly.Youtube.Uploader
{
    public class YoutubeVideoUploader : IYoutubeVideoUploader
    {
        private string id = "";
        public async Task<string> UploadVideo(YouTubeService service, HttpPostedFile file)
        {
            var video = new Google.Apis.YouTube.v3.Data.Video();
            video.Snippet = new VideoSnippet();
            video.Snippet.Title = file.FileName;
            video.Snippet.Description = "";
            video.Status = new VideoStatus();
            video.Status.PrivacyStatus = "unlisted";

            using (var videoFile = file.InputStream)
            {
                
                var videosInsertRequest = service.Videos.Insert(video, "snippet,status", videoFile, "video/*");
                videosInsertRequest.ProgressChanged += videosInsertRequest_ProgressChanged;
                videosInsertRequest.ResponseReceived += videosInsertRequest_ResponseReceived;

                videosInsertRequest.Upload();

                return id;
            }
                
            
        }

        void videosInsertRequest_ProgressChanged(Google.Apis.Upload.IUploadProgress progress)
        {
            switch (progress.Status)
            {
                case UploadStatus.Uploading:
                    Debug.WriteLine( progress.BytesSent + "bytes sent.");
                    break;

                case UploadStatus.Failed:
                    Debug.WriteLine("An error prevented the upload from completing.\n"+ progress.Exception);
                    break;
            }
        }

        void videosInsertRequest_ResponseReceived(Video video)
        {
            Debug.WriteLine("Video id " + video.Id+ " was successfully uploaded.");
            id = video.Id;
           
        }
        



    }


}

