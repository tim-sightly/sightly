﻿using System.Threading.Tasks;
using System.Web;
using Google.Apis.YouTube.v3;

namespace Sightly.Youtube.Uploader
{
    public interface IYoutubeVideoUploader
    {
        Task<string> UploadVideo(YouTubeService service, HttpPostedFile file);
    }
}