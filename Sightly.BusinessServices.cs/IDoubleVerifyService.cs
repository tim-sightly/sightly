﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public interface IDoubleVerifyService
    {
        List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyAccount(Guid accountId);
        List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyAdvertiser(Guid advertiserId);
        List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyCampaign(Guid campaignId);

    }
}