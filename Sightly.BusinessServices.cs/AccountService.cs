﻿using System.Collections.Generic;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public class AccountService : IAccountService
    {
        private readonly IAccountManager _accountManager;

        public AccountService(IAccountManager accountManager)
        {
            _accountManager = accountManager;
        }

        public List<Account> ListAllAccounts()
        {
            return _accountManager.ListAllAccounts();
        }
        
        public List<Advertiser> ListAllAdvertisers()
        {
            return _accountManager.ListAllAdvertisers();
        }
    }
}