﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;

namespace Sightly.BusinessServices.cs
{
    public class StateTransitionService : IStateTransitionService
    {

        private readonly IBudgetGroupStateTransitionManager _stateTransitionManager;

        public StateTransitionService(IBudgetGroupStateTransitionManager stateTransitionManager)
        {
            _stateTransitionManager = stateTransitionManager;
        }

        public BudgetGroup ToLive(Guid budgetGroupProposalId, Guid userId, string notes = null)
        {
            return _stateTransitionManager.MakeBudgetGroupProposalReadyForLive(budgetGroupProposalId, userId, notes);
        }

        public List<BudgetGroup> MakeReadyForLiveByOrderId(Guid orderId, Guid userId, string notes = null)
        {
            return _stateTransitionManager.MakeBudgetGroupProposalReadyForLiveByOrderId(orderId, userId, notes);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="orderId"></param>
        /// <param name="userId"></param>
        /// <param name="notes"></param>
        /// <returns></returns>
        public List<BudgetGroup> SetLiveByOrderId(Guid orderId, Guid userId, string notes = null)
        {
            return _stateTransitionManager.MakeBudgetGroupLiveByOrderId(orderId, userId, notes);
        }
    }
}
