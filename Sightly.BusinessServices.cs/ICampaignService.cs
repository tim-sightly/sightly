﻿using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public interface ICampaignService
    {
        List<Campaign> GetAllCampaigns();
        List<Campaign> GetAllMediaAgencyCampaigns();

        List<Campaign> GetAllCampaignsForAdWordsCustomerId(long adWordsCustomerId);
    }
}
