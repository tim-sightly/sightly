﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public interface IAccountService
    {
        List<Account> ListAllAccounts();
        List<Advertiser> ListAllAdvertisers();
    }
}