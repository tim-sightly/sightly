﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public interface IAlertCentralService
    {
        List<AdwordCustomer> GetAwCustomersThatAreCurrentlyActive(DateTime statDate);
        List<AdwordCustomer> GetAwCustomersThatAreCurrentlyActiveByFlight(DateTime statDate);
        List<KpiDefinitionData> GetKpiDefinitionData(long customerId);
        AdwordsKpiStatsData GetAdwordsKpiStatsData(long customerId);
        AdwordsKpiStatsData GetAdwordsKpiStatsDataByCampaignAndDates(long customerId, DateTime startDate, DateTime endDate);
        MoatKpiStatsData GetMoatKpiStatsData(long customerId);
        MoatKpiStatsData GetMoatKpiStatsDataByCampaignAndDates(long customerId, DateTime startDate, DateTime endDate);
        NielsenKpiStatsData GetNielsenKpiStatsData(long customerId);
        NielsenKpiStatsData GetNielsenKpiStatsDataByCampaignAndDates(long customerId, DateTime startDate, DateTime endDate); 
        DoubleVerifyKpiStatsData GetDoubleVerifyKpiStatsData(long customerId);
        DoubleVerifyKpiStatsData GetDoubleVerifyKpiStatsDataByCampaignAndDates(long customerId, DateTime startDate, DateTime endDate);
    }
}