﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public interface INielsenService
    {
        List<NielsenKpiStatsData> GetNielsenKpiStatsDataByAccount(Guid accountId);
        List<NielsenKpiStatsData> GetNielsenKpiStatsDataByAdvertiser(Guid advertiserId);
        List<NielsenKpiStatsData> GetNielsenKpiStatsDataByCampaign(Guid campaignId);
    }
}