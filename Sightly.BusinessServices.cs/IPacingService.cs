﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;

namespace Sightly.BusinessServices.cs
{
    public interface IPacingService
    {
        List<CampaignPacingData> GetMediaAgencyPacingDataByDate(DateTime pacingDate);
        List<PlacementAssociation> GetMediaAgencyPlacementByDate(DateTime pacingDate);
        int SaveProposedBudgetPacing(
            long customerId, 
            long campaignId, 
            decimal recomendedBudget, 
            float overPacingRate, 
            DateTime startDate, 
            decimal totalBudget, 
            int daysRemaining, 
            Guid budgetGroupTimedBudgetId, 
            DateTime pacingDate,
            Guid userId);

        List<CustomerPacingDate> GetLastPaceDateForAllCustomers();

        List<Manager> GetManagers();

    }
}

    