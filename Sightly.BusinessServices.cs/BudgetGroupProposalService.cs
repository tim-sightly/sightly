﻿using System;
using Sightly.BusinessServices.cs;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using Sightly.BusinessLayer.Common;
using System.Collections.Generic;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices
{
    public class BudgetGroupProposalService : ABusinessService, IBudgetGroupProposalService
    {
        private readonly IBudgetGroupProposalManager _budgetGroupProposalManager;
        
        public BudgetGroupProposalService(IBudgetGroupProposalManager budgetGroupProposalManager) : base()
        {
            _budgetGroupProposalManager = budgetGroupProposalManager;
        }

        public BudgetGroupProposal Create(BudgetGroupProposal budgetGroupProposal, Order order, Guid user)
        {
            return _budgetGroupProposalManager.Create(budgetGroupProposal, order, user);
        }

        public BudgetGroupProposal GetById(Guid id)
        {
            return _budgetGroupProposalManager.GetById(id);
        }

        public List<BudgetGroupProposal> GetByStatus(BudgetGroupStatus status)
        {
            throw new NotImplementedException();
        }

        public List<BudgetGroupProposal> GetAll()
        {
            return _budgetGroupProposalManager.GetBy(BudgetGroupProposalColumn.None, "");
        }

        public BudgetGroupProposal UpdateStatus(Guid budgetGroupProposalId, BudgetGroupStatus status, Guid user)
        {
            throw new NotImplementedException();
            //return _budgetGroupProposalManager.UpdateStatus(budgetGroupProposalId, status, user);
        }

        public BudgetGroupProposal Update(BudgetGroupProposal budgetGroupProposal, Guid user)
        {
            return _budgetGroupProposalManager.Update(budgetGroupProposal, user);
        }

        public BudgetGroupProposal MediaBriefPulled(Guid budgetGroupProposalId, Guid user, string notes = null)
        {
            return _budgetGroupProposalManager.MediaBriefPulled(budgetGroupProposalId, user, notes);
        }

        public BudgetGroupProposal MediaBriefProvisioned(Guid budgetGroupProposalId, Guid user, string notes = null)
        {
            return _budgetGroupProposalManager.MediaBriefProvisioned(budgetGroupProposalId, user, notes);
        }

        public BudgetGroup MoveProposalToLive(Guid id, Guid user, string notes = null)
        {
            throw new NotImplementedException();
            //    return _budgetGroupProposalManager.MoveToLive(id, user, notes);
        }
    }
}
