﻿using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.Common;

namespace Sightly.BusinessServices.cs
{
    public interface IBudgetGroupService
    {
        BudgetGroup GetById(Guid id, Guid user);
        List<BudgetGroup> GetAll(Guid user);
        List<BudgetGroup> GetByBudgetGroupStatus(BudgetGroupStatus status, Guid user);
        BudgetGroup UpdateStatus(Guid budgetGroupId, BudgetGroupStatus status, Guid user);
    }
}
