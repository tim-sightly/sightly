﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public class KpiService : IKpiService
    {
        private readonly IKpiManager _kpiManager;

        public KpiService(IKpiManager kpiManager)
        {
            _kpiManager = kpiManager;
        }

        public List<KpiCondition> GetKpiConditions()
        {
            return _kpiManager.GetKpiConditions();
        }

        public List<KpiMetric> GetKpiMetrics()
        {
            return _kpiManager.GetKpiMetrics();
        }

        public List<KpiEditorData> GetKpiEditorDataByCustomerId(long awCustomerId)
        {
            return _kpiManager.GetKpiEditorDataByCustomerId(awCustomerId);
        }

        public int SaveKpiEditorData(long adwordsCustomerId, int kpiMetricId, int equationTypeId, string equationValue,
            int sequence)
        {
            return _kpiManager.SaveKpiEditorData(
                adwordsCustomerId, 
                kpiMetricId, 
                equationTypeId, 
                equationValue,
                sequence);
        }

        public void RemoveKpiEditorData(int adwordsCustomerKpiId)
        {
            _kpiManager.RemoveKpiEditorData(adwordsCustomerKpiId);
        }
    }
}