﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public class AlertCentralService : IAlertCentralService
    {
        private readonly IAlertCentralManager _alertCentralManager;

        public AlertCentralService(IAlertCentralManager alertCentralManager)
        {
            _alertCentralManager = alertCentralManager;
        }

        public List<AdwordCustomer> GetAwCustomersThatAreCurrentlyActive(DateTime statDate)
        {
            return _alertCentralManager.GetAwCustomersThatAreCurrentlyActive(statDate);
        }

        public List<AdwordCustomer> GetAwCustomersThatAreCurrentlyActiveByFlight(DateTime statDate)
        {
            return _alertCentralManager.GetAwCustomersThatAreCurrentlyActiveByFlight(statDate);
        }

        public List<KpiDefinitionData> GetKpiDefinitionData(long customerId)
        {
            return _alertCentralManager.GetKpiDefinitionData(customerId);
        }

        public AdwordsKpiStatsData GetAdwordsKpiStatsData(long customerId)
        {
            return _alertCentralManager.GetAdwordsKpiStatsData(customerId);
        }

        public AdwordsKpiStatsData GetAdwordsKpiStatsDataByCampaignAndDates(long customerId, DateTime startDate, DateTime endDate)
        {
            return _alertCentralManager.GetAdwordsKpiStatsDataCampaignAndDate(customerId, startDate, endDate);
        }

        public MoatKpiStatsData GetMoatKpiStatsData(long customerId)
        {
            return _alertCentralManager.GetMoatKpiStatsData(customerId);
        }

        public MoatKpiStatsData GetMoatKpiStatsDataByCampaignAndDates(long customerId, DateTime startDate, DateTime endDate)
        {
            return _alertCentralManager.GetMoatKpiStatsDataCampaignAndDate(customerId, startDate, endDate);
        }

        public NielsenKpiStatsData GetNielsenKpiStatsData(long customerId)
        {
            return _alertCentralManager.GetNielsenKpiStatsData(customerId);
        }

        public NielsenKpiStatsData GetNielsenKpiStatsDataByCampaignAndDates(long customerId, DateTime startDate, DateTime endDate)
        {
            return _alertCentralManager.GetNielsenKpiStatsDataCampaignAndDate(customerId, startDate, endDate);
        }

        public DoubleVerifyKpiStatsData GetDoubleVerifyKpiStatsData(long customerId)
        {
            return _alertCentralManager.GetDoubleVerifyKpiStatsData(customerId);
        }

        public DoubleVerifyKpiStatsData GetDoubleVerifyKpiStatsDataByCampaignAndDates(long customerId, DateTime startDate, DateTime endDate)
        {
            return _alertCentralManager.GetDoubleVerifyKpiStatsDataCampaignAndDate(customerId, startDate, endDate);
        }
    }
}