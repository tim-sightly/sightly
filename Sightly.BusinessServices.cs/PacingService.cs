﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;

namespace Sightly.BusinessServices.cs
{
    public class PacingService : IPacingService
    {
        private readonly IPacingManager _pacingManager;

        public PacingService(IPacingManager pacingManager)
        {
            _pacingManager = pacingManager;
        }

        public List<CampaignPacingData> GetMediaAgencyPacingDataByDate(DateTime pacingDate)
        {
            return _pacingManager.GetMediaAgencyPacingDataByDate(pacingDate);
        }

        public List<PlacementAssociation> GetMediaAgencyPlacementByDate(DateTime pacingDate)
        {
            return _pacingManager.getMediaAgencyPlacementByDate(pacingDate);
        }

        public int SaveProposedBudgetPacing(
            long customerId, 
            long campaignId, 
            decimal recomendedBudget, 
            float overPacingRate,
            DateTime startDate, 
            decimal totalBudget, 
            int daysRemaining, 
            Guid budgetGroupTimedBudgetId, 
            DateTime pacingDate, 
            Guid userId)
        {
            return _pacingManager.SaveProposedBudgetPacing(
                customerId, 
                campaignId, 
                recomendedBudget, 
                overPacingRate,
                startDate, 
                totalBudget, 
                daysRemaining, 
                budgetGroupTimedBudgetId, 
                pacingDate,
                userId);
        }

        public List<CustomerPacingDate> GetLastPaceDateForAllCustomers()
        {
            return _pacingManager.GetLastPaceDateForAllCustomers();
        }

        public List<Manager> GetManagers()
        {
            return _pacingManager.GetManagers();
        }
    }
}