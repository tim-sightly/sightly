using System;
using System.Collections.Generic;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public class NielsenService : INielsenService
    {
        private readonly INielsenManager _nielsenManager;

        public NielsenService(INielsenManager nielsenManager)
        {
            _nielsenManager = nielsenManager;
        }

        public List<NielsenKpiStatsData> GetNielsenKpiStatsDataByAccount(Guid accountId)
        {
            return _nielsenManager.GetNielsenKpiStatsDataByAccount(accountId);
        }

        public List<NielsenKpiStatsData> GetNielsenKpiStatsDataByAdvertiser(Guid advertiserId)
        {
            return _nielsenManager.GetNielsenKpiStatsDataByAdvertiser(advertiserId);
        }

        public List<NielsenKpiStatsData> GetNielsenKpiStatsDataByCampaign(Guid campaignId)
        {
            return _nielsenManager.GetNielsenKpiStatsDataByCampaign(campaignId);
        }
    }
}