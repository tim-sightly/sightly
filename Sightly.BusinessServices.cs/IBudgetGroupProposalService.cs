﻿using Sightly.BusinessLayer.Common;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices
{
    public interface IBudgetGroupProposalService
    {
        BudgetGroupProposal Create(BudgetGroupProposal budgetGroupProposal, Order order, Guid user);
        BudgetGroupProposal GetById(Guid id);
        List<BudgetGroupProposal> GetByStatus(BudgetGroupStatus status);
        List<BudgetGroupProposal> GetAll();
        BudgetGroupProposal UpdateStatus (Guid budgetGroupProposalId, BudgetGroupStatus status, Guid user);
        BudgetGroupProposal Update(BudgetGroupProposal budgetGroupProposal, Guid user);
        BudgetGroupProposal MediaBriefPulled(Guid budgetGroupProposalId, Guid user, string notes = null);
        BudgetGroupProposal MediaBriefProvisioned(Guid budgetGroupProposalId, Guid user, string notes = null);

        BudgetGroup MoveProposalToLive(Guid id, Guid user, string notes = null);        
    }
}
