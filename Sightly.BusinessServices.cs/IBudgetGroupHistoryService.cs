﻿using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using System;

namespace Sightly.BusinessServices.cs
{
    public interface IBudgetGroupHistoryService
    {
        BudgetGroupHistory GetById(Guid id);
    }
}
