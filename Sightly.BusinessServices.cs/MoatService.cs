using System;
using System.Collections.Generic;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public class MoatService : IMoatService
    {
        private readonly IMoatManager _moatManager;

        public MoatService(IMoatManager moatManager)
        {
            _moatManager = moatManager;
        }

        public List<MoatKpiStatsData> GetMoatKpiStatsDataByAccount(Guid accountId)
        {
            return _moatManager.GetMoatKpiStatsDataByAccount(accountId);
        }

        public List<MoatKpiStatsData> GetMoatKpiStatsDataByAdvertiser(Guid advertiserId)
        {
            return _moatManager.GetMoatKpiStatsDataByAdvertiser(advertiserId);
        }

        public List<MoatKpiStatsData> GetMoatKpiStatsDataByCampaign(Guid campaignId)
        {
            return _moatManager.GetMoatKpiStatsDataByCampaign(campaignId);
        }
    }
}