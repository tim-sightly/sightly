﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Threading.Tasks;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.DomainObjects.Audience;

namespace Sightly.BusinessServices.cs
{
    public class OrderService : IOrderService
    {
        private IOrderManagerSm _orderManager;

        public OrderService(IOrderManagerSm orderManager)
        {
            _orderManager = orderManager;
        }

        public List<OrderTargetAgeGroup> GetTargetAgeGroupsForOrder(Guid orderId)
        {
            return _orderManager.GetTargetAgeGroupsForOrder(orderId);
        }

        public List<OrderTargetGenderGroup> GetTargetGenderGroupsForOrder(Guid orderId)
        {
            return _orderManager.GetTargetGenderGroupsForOrder(orderId);
        }

        public List<OrderParentalStatusGroup> GetParentalStatusGroupsForOrder(Guid orderId)
        {
            return _orderManager.GetParentalStatusGroupsForOrder(orderId);
        }

        public List<OrderHouseholdIncomeGroup> GetHouseholdIncomeGroupsForOrder(Guid orderId)
        {
            return _orderManager.GetHouseholdIncomeGroupsForOrder(orderId);
        }

    }
}
