using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public interface IMoatService
    {
        List<MoatKpiStatsData> GetMoatKpiStatsDataByAccount(Guid accountId);
        List<MoatKpiStatsData> GetMoatKpiStatsDataByAdvertiser(Guid advertiserId);
        List<MoatKpiStatsData> GetMoatKpiStatsDataByCampaign(Guid campaignId);
    }
}