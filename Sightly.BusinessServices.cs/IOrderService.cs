﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sightly.BusinessLayer.DomainObjects.Audience;

namespace Sightly.BusinessServices.cs
{
    public interface IOrderService
    {
        List<OrderTargetAgeGroup> GetTargetAgeGroupsForOrder(Guid orderId);
        List<OrderTargetGenderGroup> GetTargetGenderGroupsForOrder(Guid orderId);
        List<OrderParentalStatusGroup> GetParentalStatusGroupsForOrder(Guid orderId);
        List<OrderHouseholdIncomeGroup> GetHouseholdIncomeGroupsForOrder(Guid orderId);
    }
}
