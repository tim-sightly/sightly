﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer;
using Sightly.Models;

namespace Sightly.BusinessServices.cs
{
    public class EmailService : IEmailService
    {
        private readonly IEmailManager _emailManager;

        public EmailService(IEmailManager emailManager)
        {
            _emailManager = emailManager;
        }

        public OrderEmailData GetOrderEmailByOrder(Guid orderId)
        {
            return _emailManager.GetOrderEmailByOrder(orderId);
        }

        public List<OrderChangeInfo> GetOrderChangesByOrder(Guid orderId)
        {
            return _emailManager.GetOrderChangesByOrder(orderId);
        }
    }
}