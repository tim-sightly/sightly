﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public class DoubleVerifyService : IDoubleVerifyService
    {
        private readonly IDoubleVerifyManager _doubleVerifyManager;

        public DoubleVerifyService(IDoubleVerifyManager doubleVerifyManager)
        {
            _doubleVerifyManager = doubleVerifyManager;
        }

        public List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyAccount(Guid accountId)
        {
            return _doubleVerifyManager.GetDoubleVerifyKpiStatsDatabyAccount(accountId);
        }

        public List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyAdvertiser(Guid advertiserId)
        {
            return _doubleVerifyManager.GetDoubleVerifyKpiStatsDatabyAdvertiser(advertiserId);
        }

        public List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyCampaign(Guid campaignId)
        {
            return _doubleVerifyManager.GetDoubleVerifyKpiStatsDatabyCampaign(campaignId);
        }
    }
}