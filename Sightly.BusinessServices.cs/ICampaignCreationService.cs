﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.Models;

namespace Sightly.BusinessServices.cs
{
    public interface ICampaignCreationService
    {
        List<Account> GetAllAccounts();
        List<Advertiser> GetAdvertiserByAccountId(Guid accountId);
        List<Advertiser> GetAdvertiserByAccountIdAndUser(Guid accountId, Guid userId);

        Account InsertSubAccount(string accountName, Guid accountTypeId, Guid parentAccountId);

        Advertiser InsertAdvertiser(string advertiserName, Guid accountId);

        Campaign InsertCampaign(string campaignName, Guid advertiserId);

        OrderBasic InsertOrder(string orderName, string orderRefCode, Guid campaignId);
        Advertiser InsertAdvertiserExtended(string advertiserName, string advertiserRefCode, Guid accountId, Guid advertiserCategoryId, Guid advertiserSubCategoryId);
        List<Account> GetAllAccountsUnderUser(Guid userId);
        List<Advertiser> GetAllPermAdvertisers(Guid userId);
        List<CampaignAbbreviated> GetAllPermCampaigns(Guid userId);
    }
}