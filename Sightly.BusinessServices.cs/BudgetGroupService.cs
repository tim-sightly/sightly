﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.Common;

namespace Sightly.BusinessServices.cs
{
    public class BudgetGroupService : ABusinessService, IBudgetGroupService
    {
        private readonly IBudgetGroupManager _budgetGroupManager;

        public BudgetGroupService(IBudgetGroupManager budgetGroupManager) : base()
        {

            _budgetGroupManager = budgetGroupManager;
        }

        public BudgetGroup GetById(Guid id, Guid user)
        {
            return _budgetGroupManager.GetById(id, user);
        }

        public List<BudgetGroup> GetAll(Guid user)
        {
            return _budgetGroupManager.GetAll(user);
        }

        public List<BudgetGroup> GetByBudgetGroupStatus(BudgetGroupStatus status, Guid user)
        {
            return _budgetGroupManager.GetByBudgetGroupStatus(status, user);
        }

        public BudgetGroup UpdateStatus(Guid budgetGroupId, BudgetGroupStatus status, Guid user)
        {
            return _budgetGroupManager.UpdateStatus(budgetGroupId, status, user);
        }


    }
}
