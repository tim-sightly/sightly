﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.Models;

namespace Sightly.BusinessServices.cs
{
    public class CampaignCreationService : ICampaignCreationService
    {
        private readonly ICampaignCreationManager _campaignCreationManager;

        public CampaignCreationService(ICampaignCreationManager campaignCreationManager)
        {
            _campaignCreationManager = campaignCreationManager;
        }

        public List<Account> GetAllAccounts()
        {
            return _campaignCreationManager.GetAllAccounts();
        }

        public List<Advertiser> GetAdvertiserByAccountId(Guid accountId)
        {
            return _campaignCreationManager.GetAdvertiserByAccountId(accountId);
        }

        public List<Advertiser> GetAdvertiserByAccountIdAndUser(Guid accountId, Guid userId)
        {
            return _campaignCreationManager.GetAdvertiserByAccountIdAndUser(accountId, userId);
        }

        public Account InsertSubAccount(string accountName, Guid accountTypeId, Guid parentAccountId)
        {
            return _campaignCreationManager.InsertSubAccount(accountName, accountTypeId, parentAccountId);
        }

        public Advertiser InsertAdvertiser(string advertiserName, Guid accountId)
        {
            return _campaignCreationManager.InsertAdvertiser(advertiserName, accountId);
        }

        public Campaign InsertCampaign(string campaignName, Guid advertiserId)
        {
            return _campaignCreationManager.InsertCampaign(campaignName, advertiserId);
        }

        public OrderBasic InsertOrder(string orderName, string orderRefCode, Guid campaignId)
        {
            return _campaignCreationManager.InsertOrder(orderName, orderRefCode, campaignId);
        }

        public Advertiser InsertAdvertiserExtended(string advertiserName, string advertiserRefCode, Guid accountId,
            Guid advertiserCategoryId, Guid advertiserSubCategoryId)
        {
            return _campaignCreationManager.InsertAdvertiserExtended(
                advertiserName, 
                advertiserRefCode,
                accountId,
                advertiserCategoryId,
                advertiserSubCategoryId);
        }

        public List<Account> GetAllAccountsUnderUser(Guid userId)
        {
            return _campaignCreationManager.GetAllAccountsByUser(userId);
        }

        public List<Advertiser> GetAllPermAdvertisers(Guid userId)
        {
            return _campaignCreationManager.GetAllAdvertisersByUser(userId);
        }

        public List<CampaignAbbreviated> GetAllPermCampaigns(Guid userId)
        {
            return _campaignCreationManager.GetAllCampaignsByUser(userId);
        }
    }
}