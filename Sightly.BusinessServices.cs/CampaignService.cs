﻿using System.Collections.Generic;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public class CampaignService : ICampaignService
    {
        private readonly ICampaignManager _campaignManager;

        public CampaignService(ICampaignManager campaignManager)
        {
            _campaignManager = campaignManager;
        }

        public List<Campaign> GetAllCampaigns()
        {
            return _campaignManager.GetAllCampaigns();
        }

        public List<Campaign> GetAllMediaAgencyCampaigns()
        {
            return _campaignManager.GetAllMediaAgencyCampaigns();
        }

        public List<Campaign> GetAllCampaignsForAdWordsCustomerId(long adWordsCustomeId)
        {
            return _campaignManager.GetAllCampaignsForAdWordsCustomerId(adWordsCustomeId);
        }

    }
}
