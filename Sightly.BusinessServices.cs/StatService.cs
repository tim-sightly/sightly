﻿using System;
using System.Collections.Generic;
using Sightly.BusinessLayer;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.BusinessServices.cs
{
    public class StatService : IStatService
    {
        private readonly IStatManager _statManager;
        
        public StatService(IStatManager statManager)
        {
            _statManager = statManager;
        }

        public List<DailyStats> GetDailyDeviceStatsForAdWordsCustomerId(long adWordsCustomerId)
        {
            return _statManager.GetDailyDeviceStatsForAdWordsCustomerId(adWordsCustomerId);
        }

        public List<DailyStats> GetDailyDeviceStatsForCampaign(Guid campaignId)
        {
            return _statManager.GetDailyDeviceStatsForCampaign(campaignId);
        }

        public List<DailyStats> GetDailyDeviceStatsForAccount(Guid accountId)
        {
            return _statManager.GetDailyDeviceStatsForAccount(accountId);
        }

        public List<DailyStats> GetDailyDeviceStatsForAdvertiser(Guid advertiserId)
        {
            return _statManager.GetDailyDeviceStatsForAdvertiser(advertiserId);
        }
    }
}