﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;

namespace Sightly.BusinessServices.cs
{
    public interface IStateTransitionService
    {
        BudgetGroup ToLive(Guid budgetGroupProposalId, Guid userId, string notes = null);
        List<BudgetGroup> MakeReadyForLiveByOrderId(Guid orderId, Guid userId, string notes = null);
        List<BudgetGroup> SetLiveByOrderId(Guid orderId, Guid userId, string notes = null);
    }
}
