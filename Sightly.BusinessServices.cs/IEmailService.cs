﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.BusinessServices.cs
{
    public interface IEmailService
    {
        OrderEmailData GetOrderEmailByOrder(Guid orderId);
        List<OrderChangeInfo> GetOrderChangesByOrder(Guid orderId);
    }
}
