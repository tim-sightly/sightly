﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Sightly.ReportingClient.RequestModels;

namespace Sightly.ReportingClient
{
    public class JsonWebApiAgent
    {
        private ConfigurationStore _configurationStore { get; set; }

        public JsonWebApiAgent()
        {
            _configurationStore = new ConfigurationStore();
        }
        protected string GetServiceName()
        {
            return _configurationStore.InternalWebServiceName;
        }

        protected T SendGetRequest<T>(string actionUrl)
        {
            using (var client = new HttpClient())
            {
                var baseApiUrl = _configurationStore.ReportUrl;
                var serviceId = _configurationStore.ServiceId;

                client.BaseAddress = new Uri(baseApiUrl);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                if (actionUrl.Contains("?"))
                {
                    actionUrl += "&ServiceId=" + serviceId.ToString();
                }
                else
                {
                    actionUrl += "?ServiceId=" + serviceId.ToString();
                }

                var response = client.GetAsync(actionUrl).Result;

                if (response.IsSuccessStatusCode)
                {
                    var contentStream = response.Content.ReadAsStreamAsync().Result;
                    var reader = new StreamReader(contentStream);
                    var jsonData = reader.ReadToEnd();
                    contentStream.Position = 0;
                    if (!string.IsNullOrEmpty(jsonData))
                    {
                        return JsonConvert.DeserializeObject<T>(jsonData);
                    }
                }
                else
                {
                    string message = response.ReasonPhrase;
                }

                throw new HttpRequestException("Failed Get Request @ " + baseApiUrl + actionUrl);
            }
        }

        protected T SendPostRequest<T>(string actionUrl, BaseRequest request)
        {
            using (var client = new HttpClient())
            {
                var baseApiUrl = _configurationStore.ReportUrl;
                var serviceId = _configurationStore.ServiceId;

                request.ServiceId = Guid.Parse(serviceId);

                client.BaseAddress = new Uri(baseApiUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var jsonRequest = JsonConvert.SerializeObject(request);
                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var response = client.PostAsync(actionUrl, content).Result;

                if (response.IsSuccessStatusCode)
                {
                    var contentStream = response.Content.ReadAsStreamAsync().Result;
                    var reader = new StreamReader(contentStream);
                    var jsonData = reader.ReadToEnd();

                    contentStream.Position = 0;

                    if (!string.IsNullOrEmpty(jsonData))
                    {
                        return JsonConvert.DeserializeObject<T>(jsonData);
                    }
                }
                throw new HttpRequestException("Failed POST request @ " + baseApiUrl + actionUrl);
            }
        }

        protected void SendPostRequest(string actionUrl, BaseRequest request)
        {
            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(1);
                var baseApiUrl = _configurationStore.ReportUrl;
                var serviceId = _configurationStore.ServiceId;

                request.ServiceId = Guid.Parse(serviceId);

                client.BaseAddress = new Uri(baseApiUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var jsonRequest = JsonConvert.SerializeObject(request);
                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var response = client.PostAsync(actionUrl, content).Result;

                if (response.StatusCode == HttpStatusCode.BadRequest || response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    var httpErrorString = response.Content.ReadAsStringAsync().Result;
                    var httpErrorTemplateObject = new { message = "" };
                    var httpErrorObject = JsonConvert.DeserializeAnonymousType(httpErrorString, httpErrorTemplateObject);
                    throw new HttpRequestException($"Failed POST Request @ {baseApiUrl + actionUrl}. {httpErrorObject.message}");
                }
            }

        }
    }
}
