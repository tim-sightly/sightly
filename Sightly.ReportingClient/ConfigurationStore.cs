﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.ReportingClient
{
    public class ConfigurationStore
    {
        public string InternalWebServiceName { get; set; }

        public string ServiceId { get; set; }
        public string ReportUrl { get; set; }

        public string JwtSecret { get; set; }

        public ConfigurationStore()
        {
            ServiceId = System.Configuration.ConfigurationManager.AppSettings["ServiceId"];
            ReportUrl = System.Configuration.ConfigurationManager.AppSettings["ReportUrl"];
            JwtSecret = System.Configuration.ConfigurationManager.AppSettings["JwtSecret"];
        }
    }
}
