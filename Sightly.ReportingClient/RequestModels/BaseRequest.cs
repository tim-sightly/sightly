﻿using System;

namespace Sightly.ReportingClient.RequestModels
{
    public class BaseRequest
    {
        public Guid ServiceId { get; set; }
    }
}
