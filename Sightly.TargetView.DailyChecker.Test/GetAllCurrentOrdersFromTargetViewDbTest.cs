﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Sightly.TargetView.DailyChecker.Test
{
    [TestClass]
    public class GetAllCurrentOrdersFromTargetViewDbTest
    {
        private readonly DateTime _statDate = DateTime.Now;

        [TestMethod]
        public void GetOrderFromDatabase()
        {
            var context = new TargetViewDb.DAL.TargetViewDb();

            var currentOrders =
                context.Orders.Where(order => order.StartDate <= _statDate && order.EndDate >= _statDate && order.Deleted == false).ToList();

            Assert.IsNotNull(currentOrders);

        }
    }
}
