﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Sightly.DAL.Test
{
    [TestClass]
    public class AlertCentralObjectTest
    {
        private static IContainer Container;
        private IAlertCentralStore _alertCentralStore;
        private const long CustomerId = 7104344915;

        [TestInitialize]
        public void InitializeTest()
        {
            try
            {
                Container = IOC.Testing.DIConfiguration.GetConfiguredContainer();
                _alertCentralStore = Container.Resolve<IAlertCentralStore>();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        [TestMethod]
        public void GetData()
        {
            var data = _alertCentralStore.GetAdwordsKpiStatsData(CustomerId);
            Assert.IsNotNull(data);
        }
    }
}
