﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.DAL.DTO.BudgetGroupStates;
using Sightly.DAL.DTO.BudgetGroupXrefAdStates;
using System.Collections.Generic;
using Sightly.DAL.DTO.BudgetGroupTimedBudgetStates;
using Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates;
using Sightly.DAL.DTO.BudgetGroupXrefLocationStates;
using Sightly.DAL.DTO.BudgetGroupXrefPlacementStates;
using Autofac;

namespace Sightly.DAL.Test
{
    [TestClass]
    public class DapperDataStoreTest
    {
        private static IContainer Container { get; set; }
        private IBudgetGroupProposalStore _budgetGroupProposalStore;
        private IBudgetGroupLedgerStore _budgetGroupLedgerStore;
        

        [TestInitialize]
        public void InitializeTest()
        {
            Container = IOC.Testing.DIConfiguration.GetConfiguredContainer();
            _budgetGroupProposalStore = Container.Resolve<IBudgetGroupProposalStore>();
            _budgetGroupLedgerStore = Container.Resolve<IBudgetGroupLedgerStore>();
        }

        [Ignore]
        [TestMethod]
        public void CreateLiveAndHistory_HappyPath_RowsAffectedGreaterThanZero()
        {
            //Guid budgetGroupid = Guid.NewGuid();
            //Guid budgetGroupHistoryid = Guid.NewGuid();
            //string creator = "Billy Bob";
            //IBudgetGroupProposalStore dataStore = new BudgetGroupProposalStore();
            //BudgetGroup budgetGroup = new BudgetGroup()
            //{
            //    BudgetGroupId = budgetGroupid,
            //    BudgetGroupName = "TEST MOVE TO LIVE",
            //    BudgetGroupBudget = 50000.00m,
            //    CreatedBy = creator,
            //    CreatedDatetime = null,
            //    LastModifiedBy = creator,
            //    LastModifiedDatetime = null,
            //    BudgetGroupXrefAds = new List<BudgetGroupXrefAd>()
            //    {
            //        new BudgetGroupXrefAd
            //        {
            //            BudgetGroupXrefAdId = Guid.NewGuid(),
            //            BudgetGroupId = budgetGroupid,
            //            AdId = new Guid("6B5D7DD2-7A58-4005-AF1D-000DE031825C"),
            //            CreatedBy = creator,
            //            CreatedDatetime = null,
            //            LastModifiedBy = creator,
            //        },
            //        new BudgetGroupXrefAd
            //        {
            //            BudgetGroupXrefAdId = Guid.NewGuid(),
            //            BudgetGroupId = budgetGroupid,
            //            AdId = new Guid("6B5D7DD2-7A58-4005-AF1D-000DE031825C"),
            //            CreatedBy = creator,
            //            CreatedDatetime = DateTime.Now,
            //            LastModifiedBy = creator,
            //            LastModifiedDatetime = DateTime.Now
            //        }
            //    },
            //    BudgetGroupTimedBudget = new BudgetGroupTimedBudget()
            //    {
            //        BudgetGroupTimedBudgetId = Guid.NewGuid(),
            //        BudgetGroupId = budgetGroupid,
            //        BudgetAmount = 15001.25m,
            //        Margin = .2m,
            //        StartDate = DateTime.Now,
            //        EndDate = DateTime.Now,
            //        CreatedBy = creator,
            //        CreatedDatetime = null
            //    },
            //    BudgetGroupXrefConversionActions = new List<BudgetGroupXrefConversionAction>()
            //    {
            //        new BudgetGroupXrefConversionAction()
            //        {
            //            BudgetGroupXrefConversionActionId = Guid.NewGuid(),
            //            BudgetGroupId = budgetGroupid,
            //            ConversionActionId = null,
            //            SequenceId = 5,
            //            CreatedBy = creator,
            //            CreatedDatetime = null,
            //            LastModifiedBy = creator,
            //            LastModifiedDatetime = null
            //        },
            //        new BudgetGroupXrefConversionAction()
            //        {
            //            BudgetGroupXrefConversionActionId = Guid.NewGuid(),
            //            BudgetGroupId = budgetGroupid,
            //            ConversionActionId = null,
            //            SequenceId = 1,
            //            CreatedBy = creator,
            //            CreatedDatetime = DateTime.Now,
            //            LastModifiedBy = creator,
            //            LastModifiedDatetime = DateTime.Now
            //        }
            //    },
            //    BudgetGroupXrefLocations = new List<BudgetGroupXrefLocation>()
            //    {
            //        new BudgetGroupXrefLocation()
            //        {
            //            BudgetGroupXrefLocationId = Guid.NewGuid(),
            //            BudgetGroupId = budgetGroupid,
            //            LocationId = new Guid("A8952CFC-A8C4-4285-9E6F-006B5D1D6507"),
            //            CreatedBy = creator,
            //            CreatedDatetime = null,
            //            LastModifiedBy = creator,
            //            LastModifiedDatetime = null
            //        },
            //        new BudgetGroupXrefLocation()
            //        {
            //            BudgetGroupXrefLocationId = Guid.NewGuid(),
            //            BudgetGroupId = budgetGroupid,
            //            LocationId = new Guid("C1B1CCC7-D75C-48AD-B5DF-020A015CA93F"),
            //            CreatedBy = creator,
            //            CreatedDatetime = DateTime.Now,
            //            LastModifiedBy = creator,
            //            LastModifiedDatetime = DateTime.Now
            //        }
            //    },
            //    BudgetGroupXrefPlacements = new List<BudgetGroupXrefPlacement>()
            //    {
            //        new BudgetGroupXrefPlacement()
            //        {
            //            BudgetGroupXrefPlacementId = Guid.NewGuid(),
            //            BudgetGroupId = budgetGroupid,
            //            PlacementId = new Guid("BC878971-C818-4402-9B13-02A198E0139B"),
            //            CreatedBy = creator,
            //            CreatedDatetime = null
            //        },
            //        new BudgetGroupXrefPlacement()
            //        {
            //            BudgetGroupXrefPlacementId = Guid.NewGuid(),
            //            BudgetGroupId = budgetGroupid,
            //            PlacementId = new Guid("BC878971-C818-4402-9B13-02A198E0139B"),
            //            CreatedBy = creator,
            //            CreatedDatetime = DateTime.Now
            //        }
            //    },
            //};

            //BudgetGroupHistory budgetGroupHistory = new BudgetGroupHistory()
            //{
            //    BudgetGroupHistoryId = budgetGroupHistoryid,
            //    BudgetGroupId = budgetGroupid,
            //    HistoryDate = DateTime.Now,
            //    BudgetGroupName = "Trial Budget",
            //    BudgetGroupBudget = 40000.00m,
            //    CreatedBy = creator,
            //    CreatedDatetime = null,
            //    BudgetGroupXrefAdHistories = new List<BudgetGroupXrefAdHistory>()
            //    {
            //        new BudgetGroupXrefAdHistory
            //        {
            //            BudgetGroupXrefAdHistoryId = Guid.NewGuid(),
            //            BudgetGroupHistoryId = budgetGroupHistoryid,
            //            HistoryDate = DateTime.Now,
            //            AdId = new Guid("6B5D7DD2-7A58-4005-AF1D-000DE031825C"),
            //            CreatedBy = creator,
            //            CreatedDatetime = null,
            //        },
            //        new BudgetGroupXrefAdHistory
            //        {
            //            BudgetGroupXrefAdHistoryId = Guid.NewGuid(),
            //            BudgetGroupHistoryId = budgetGroupHistoryid,
            //            HistoryDate = DateTime.Now,
            //            AdId = new Guid("6B5D7DD2-7A58-4005-AF1D-000DE031825C"),
            //            CreatedBy = creator,
            //            CreatedDatetime = DateTime.Now,
            //        }
            //    },
            //    BudgetGroupTimedBudgetHistory = new BudgetGroupTimedBudgetHistory()
            //    {
            //        BudgetGroupTimedBudgetHistoryId = Guid.NewGuid(),
            //        BudgetGroupHistoryId = budgetGroupHistoryid,
            //        HistoryDate = DateTime.Now,
            //        BudgetAmount = 15001.25m,
            //        Margin = .2m,
            //        StartDate = DateTime.Now,
            //        EndDate = DateTime.Now,
            //        CreatedBy = creator,
            //        CreatedDatetime = null
            //    },
            //    BudgetGroupXrefConversionActionHistories = new List<BudgetGroupXrefConversionActionHistory>()
            //    {
            //        new BudgetGroupXrefConversionActionHistory()
            //        {
            //            BudgetGroupXrefConversionActionHistoryId = Guid.NewGuid(),
            //            BudgetGroupHistoryId = budgetGroupHistoryid,
            //            HistoryDate = DateTime.Now,
            //            ConversionActionId = null,
            //            CreatedBy = creator,
            //            CreatedDatetime = null
            //        },
            //        new BudgetGroupXrefConversionActionHistory()
            //        {
            //            BudgetGroupXrefConversionActionHistoryId = Guid.NewGuid(),
            //            BudgetGroupHistoryId = budgetGroupHistoryid,
            //            HistoryDate = DateTime.Now,
            //            ConversionActionId = null,
            //            CreatedBy = creator,
            //            CreatedDatetime = DateTime.Now
            //        }
            //    },
            //    BudgetGroupXrefLocationHistories = new List<BudgetGroupXrefLocationHistory>()
            //    {
            //        new BudgetGroupXrefLocationHistory()
            //        {
            //            BudgetGroupXrefLocationHistoryId = Guid.NewGuid(),
            //            BudgetGroupHistoryId = budgetGroupHistoryid,
            //            HistoryDate = DateTime.Now,
            //            LocationId = new Guid("C1B1CCC7-D75C-48AD-B5DF-020A015CA93F"),
            //            CreatedBy = creator,
            //            CreatedDatetime = null,
            //        },
            //        new BudgetGroupXrefLocationHistory()
            //        {
            //            BudgetGroupXrefLocationHistoryId = Guid.NewGuid(),
            //            BudgetGroupHistoryId = budgetGroupHistoryid,
            //            HistoryDate = DateTime.Now,
            //            LocationId = new Guid("C1B1CCC7-D75C-48AD-B5DF-020A015CA93F"),
            //            CreatedBy = creator,
            //            CreatedDatetime = DateTime.Now,
            //        }
            //    },
            //    BudgetGroupXrefPlacementHistories = new List<BudgetGroupXrefPlacementHistory>()
            //    {
            //        new BudgetGroupXrefPlacementHistory()
            //        {
            //            BudgetGroupXrefPlacementHistoryId = Guid.NewGuid(),
            //            BudgetGroupHistoryId = budgetGroupHistoryid,
            //            HistoryDate = DateTime.Now,
            //            PlacementId = new Guid("BC878971-C818-4402-9B13-02A198E0139B"),
            //            CreatedBy = creator,
            //            CreatedDatetime = null
            //        },
            //        new BudgetGroupXrefPlacementHistory()
            //        {
            //            BudgetGroupXrefPlacementHistoryId = Guid.NewGuid(),
            //            BudgetGroupHistoryId = budgetGroupHistoryid,
            //            HistoryDate = DateTime.Now,
            //            PlacementId = new Guid("BC878971-C818-4402-9B13-02A198E0139B"),
            //            CreatedBy = creator,
            //            CreatedDatetime = DateTime.Now
            //        }
            //    },
            //};

            //int rowsAffected = dataStore.MoveToLiveBudgetGroupAndBudgetGroupHistory(budgetGroup, budgetGroupHistory);
            Assert.AreEqual(20, 0);
        }

        //[TestMethod]
        //public void CreateBudgetGroup_HappyPath_RowsAffectedGreaterThanZero()
        //{
        //    Guid id = Guid.NewGuid();
        //    string creator = "Chevy Chase";
        //    DateTime now = DateTime.Now;
        //    DateTime endDate = now.AddMonths(2);
        //    IBudgetGroupProposalStore dataStore = new BudgetGroupProposalStore();
        //    BudgetGroup budgetGroup = new BudgetGroup()
        //    {
        //        BudgetGroupId = id,
        //        BudgetGroupName = "Lampoons National Vacation Budget",
        //        BudgetGroupBudget = 4500.00m,
        //        CreatedBy = creator,
        //        CreatedDatetime = now,
        //        LastModifiedBy = creator,
        //        LastModifiedDatetime = now,
        //        BudgetGroupXrefAds = new List<BudgetGroupXrefAd>()
        //        {
        //            new BudgetGroupXrefAd
        //            {
        //                BudgetGroupXrefAdId = Guid.NewGuid(),
        //                BudgetGroupId = id,
        //                AdId = new Guid("6B5D7DD2-7A58-4005-AF1D-000DE031825C"),
        //                CreatedBy = creator,
        //                CreatedDatetime = now,
        //                LastModifiedBy = creator,
        //                LastModifiedDatetime = now
        //            },
        //            new BudgetGroupXrefAd
        //            {
        //                BudgetGroupXrefAdId = Guid.NewGuid(),
        //                BudgetGroupId = id,
        //                AdId = new Guid("6B5D7DD2-7A58-4005-AF1D-000DE031825C"),
        //                CreatedBy = creator,
        //                CreatedDatetime = now,
        //                LastModifiedBy = creator,
        //                LastModifiedDatetime = now
        //            }
        //        },
        //        BudgetGroupXrefLocations = new List<BudgetGroupXrefLocation>()
        //        {
        //            new BudgetGroupXrefLocation()
        //            {
        //                BudgetGroupXrefLocationId = Guid.NewGuid(),
        //                BudgetGroupId = id,
        //                LocationId = new Guid("A8952CFC-A8C4-4285-9E6F-006B5D1D6507"),
        //                CreatedBy = creator,
        //                CreatedDatetime = null,
        //                LastModifiedBy = creator,
        //                LastModifiedDatetime = null
        //            },
        //            new BudgetGroupXrefLocation()
        //            {
        //                BudgetGroupXrefLocationId = Guid.NewGuid(),
        //                BudgetGroupId = id,
        //                LocationId = new Guid("C1B1CCC7-D75C-48AD-B5DF-020A015CA93F"),
        //                CreatedBy = creator,
        //                CreatedDatetime = DateTime.Now,
        //                LastModifiedBy = creator,
        //                LastModifiedDatetime = DateTime.Now
        //            }
        //        },
        //        BudgetGroupTimedBudget = new BudgetGroupTimedBudget()
        //        {
        //            BudgetGroupTimedBudgetId = Guid.NewGuid(),
        //            BudgetGroupId = id,
        //            BudgetAmount = 15001.25m,
        //            Margin = .2m,
        //            StartDate = DateTime.Now,
        //            EndDate = endDate,
        //            CreatedBy = creator,
        //            CreatedDatetime = now
        //        },
        //        BudgetGroupXrefPlacements = new List<BudgetGroupXrefPlacement>()
        //        {
        //            new BudgetGroupXrefPlacement()
        //            {
        //                BudgetGroupXrefPlacementId = Guid.NewGuid(),
        //                BudgetGroupId = id,
        //                PlacementId = new Guid("D0587739-FC9E-43AA-AD5D-003B9F9E009A"),
        //                CreatedBy = creator,
        //                CreatedDatetime = null
        //            },
        //            new BudgetGroupXrefPlacement()
        //            {
        //                BudgetGroupXrefPlacementId = Guid.NewGuid(),
        //                BudgetGroupId = id,
        //                PlacementId = new Guid("D0587739-FC9E-43AA-AD5D-003B9F9E009A"),
        //                CreatedBy = creator,
        //                CreatedDatetime = DateTime.Now
        //            }
        //        },
        //        BudgetGroupXrefConversionActions = new List<BudgetGroupXrefConversionAction>()
        //        {
        //            new BudgetGroupXrefConversionAction()
        //            {
        //                BudgetGroupXrefConversionActionId = Guid.NewGuid(),
        //                BudgetGroupId = id,
        //                ConversionActionId = null,
        //                SequenceId = 5,
        //                CreatedBy = creator,
        //                CreatedDatetime = null,
        //                LastModifiedBy = creator,
        //                LastModifiedDatetime = null
        //            },
        //            new BudgetGroupXrefConversionAction()
        //            {
        //                BudgetGroupXrefConversionActionId = Guid.NewGuid(),
        //                BudgetGroupId = id,
        //                ConversionActionId = null,
        //                SequenceId = 1,
        //                CreatedBy = creator,
        //                CreatedDatetime = DateTime.Now,
        //                LastModifiedBy = creator,
        //                LastModifiedDatetime = DateTime.Now
        //            }
        //        }                             
        //    };
        //    int rowsAffected = dataStore.CreateBudgetGroup(budgetGroup);
        //    Assert.AreEqual(10, rowsAffected);
        //}
        //[Ignore]
        //[TestMethod]
        //public void CreateBudgetGroupProposal_HappyPath_RowsAffectedGreaterThanZero()
        //{
        //    Guid id = Guid.NewGuid();
        //    Guid creator = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");
        //    DateTime now = DateTime.UtcNow;
        //    DateTime endDate = now.AddMonths(6);

        //    BudgetGroupProposal budgetGroupProposal = new BudgetGroupProposal()
        //    {
        //        BudgetGroupProposalId = id,
        //        BudgetGroupStatusId = 1,
        //        BudgetGroupName = "The Wild Bunch",
        //        BudgetGroupBudget = 1963.25m,
        //        CreatedBy = creator,
        //        Created = now,
        //        ModifiedBy = null,
        //        Modified = null,
        //        BudgetGroupXrefAdProposals = new List<BudgetGroupXrefAdProposal>()
        //        {
        //            new BudgetGroupXrefAdProposal
        //            {
        //                BudgetGroupXrefAdProposalId = Guid.NewGuid(),
        //                BudgetGroupProposalId = id,
        //                AdId = new Guid("53046EBC-D129-490A-A260-00184A563304"),
        //                CreatedBy = creator,
        //                Created = now
        //            },
        //            new BudgetGroupXrefAdProposal
        //            {
        //                BudgetGroupXrefAdProposalId = Guid.NewGuid(),
        //                BudgetGroupProposalId = id,
        //                AdId = new Guid("53046EBC-D129-490A-A260-00184A563304"),
        //                CreatedBy = creator,
        //                Created = now,
        //            }
        //        },
        //        BudgetGroupXrefLocationProposals = new List<BudgetGroupXrefLocationProposal>()
        //        {
        //            new BudgetGroupXrefLocationProposal()
        //            {
        //                BudgetGroupXrefLocationProposalId = Guid.NewGuid(),
        //                BudgetGroupProposalId = id,
        //                LocationId = new Guid("3DD617F4-709D-461C-A600-02AF14A5495C"),
        //                CreatedBy = creator,
        //            },
        //            new BudgetGroupXrefLocationProposal()
        //            {
        //                BudgetGroupXrefLocationProposalId = Guid.NewGuid(),
        //                BudgetGroupProposalId = id,
        //                LocationId = new Guid("3DD617F4-709D-461C-A600-02AF14A5495C"),
        //                CreatedBy = creator,
        //            }
        //        },
        //        BudgetGroupTimedBudgetProposal = new BudgetGroupTimedBudgetProposal()
        //        {
        //            BudgetGroupTimedBudgetProposalId = Guid.NewGuid(),
        //            BudgetGroupProposalId = id,
        //            BudgetAmount = 1000.50m,
        //            Margin = .22m,
        //            StartDate = DateTime.UtcNow,
        //            EndDate = endDate,
        //            CreatedBy = creator
        //        },
        //        BudgetGroupXrefPlacementProposals = new List<BudgetGroupXrefPlacementProposal>()
        //        {
        //            new BudgetGroupXrefPlacementProposal()
        //            {
        //                BudgetGroupXrefPlacementProposalId = Guid.NewGuid(),
        //                BudgetGroupProposalId = id,
        //                PlacementId = new Guid("D0587739-FC9E-43AA-AD5D-003B9F9E009A"),
        //                AssignedBy = creator,
        //            },
        //                new BudgetGroupXrefPlacementProposal()
        //            {
        //                BudgetGroupXrefPlacementProposalId = Guid.NewGuid(),
        //                BudgetGroupProposalId = id,
        //                PlacementId = new Guid("D0587739-FC9E-43AA-AD5D-003B9F9E009A"),
        //                AssignedBy = creator
        //            }
        //        },
        //        BudgetGroupXrefConversionActionProposals = new List<BudgetGroupXrefConversionActionProposal>()
        //        {
        //            new BudgetGroupXrefConversionActionProposal()
        //            {
        //                BudgetGroupXrefConversionActionProposalId = Guid.NewGuid(),
        //                BudgetGroupProposalId = id,
        //                ConversionActionId = new Guid("A04A9314-9E3F-4978-A9DA-461101B8AF23"),
        //                CreatedBy = creator,
        //            },
        //            new BudgetGroupXrefConversionActionProposal()
        //            {
        //                BudgetGroupXrefConversionActionProposalId = Guid.NewGuid(),
        //                BudgetGroupProposalId = id,
        //                ConversionActionId = new Guid("A04A9314-9E3F-4978-A9DA-461101B8AF23"),
        //                CreatedBy = creator
        //            }
        //        }
        //    };
        //    //TODO: add new parameters to this method: order, orderxrefbudgetgroup, budgetgroupledger
        //    BudgetGroupProposal createdBudgetGroupProposal = _budgetGroupProposalStore.CreateBudgetGroupProposal(budgetGroupProposal);
        //    Assert.IsNotNull(createdBudgetGroupProposal);
        //}
        [Ignore]
        [TestMethod]
        public void UpdateBudgetGroupProposalStatus_HappyPath_RowsAffectedGreaterThanZero()
        {
            Guid user = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");
            BudgetGroupProposal updatedBudgetGroup = _budgetGroupProposalStore.UpdateBudgetGroupProposalStatus(new Guid("0037CDF4-76F8-4C96-8A4F-CA8842B4FAC0"), 2, user);
            Assert.IsNotNull(updatedBudgetGroup);
        }

        [Ignore]
        [TestMethod]
        public void GetBudgetGroupProposal_BudgetGroupProposalExists_ReturnBudgetGroupProposal()
        {
            BudgetGroupProposal budgetGroupProposal = _budgetGroupProposalStore.GetBudgetGroupProposalById(new Guid("53570873-FCBE-4B63-B151-D0DFCE02348C"));
            Assert.IsNotNull(budgetGroupProposal);
        }

        [Ignore]
        [TestMethod]
        public void UpdateBudgetGroupProposal_BudgetGroupProposalExists_ReturnUpdatedBudgetGroupProposal()
        {
            //BudgetGroupProposal budgetGroupProposal = _budgetGroupProposalStore.GetBudgetGroupProposalById(new Guid("57F77718-BD4A-43FF-ACFB-23D86C5F421F"));
            ////make changes
            //budgetGroupProposal.BudgetGroupName = "Police Academy IX Budget";
            //budgetGroupProposal.BudgetGroupXrefAdProposals[1].AdId = new Guid("1647F054-4EFD-424E-AB99-000BA231370E");
            //budgetGroupProposal.BudgetGroupXrefAdProposals.Add(new BudgetGroupXrefAdProposal()
            //{
            //    BudgetGroupXrefAdProposalId = Guid.NewGuid(),
            //    BudgetGroupProposalId = budgetGroupProposal.BudgetGroupProposalId,
            //    AdId = new Guid("1647F054-4EFD-424E-AB99-000BA231370E"),
            //    Created = DateTime.UtcNow,
            //    CreatedBy = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE")
            //});

            //budgetGroupProposal.BudgetGroupXrefLocationProposals[0].LocationId = new Guid("C1B1CCC7-D75C-48AD-B5DF-020A015CA93F");

            //budgetGroupProposal.BudgetGroupXrefLocationProposals.Add(new BudgetGroupXrefLocationProposal()
            //{
            //    BudgetGroupXrefLocationProposalId = Guid.NewGuid(),
            //    BudgetGroupProposalId = budgetGroupProposal.BudgetGroupProposalId,
            //    LocationId = new Guid("C1B1CCC7-D75C-48AD-B5DF-020A015CA93F"),
            //    Created = DateTime.UtcNow,
            //    CreatedBy = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE")
            //});

            //budgetGroupProposal.BudgetGroupTimedBudgetProposal.Margin = .0222m;
            //budgetGroupProposal.BudgetGroupTimedBudgetProposal.StartDate = DateTime.UtcNow.AddDays(5);
            //budgetGroupProposal.BudgetGroupTimedBudgetProposal.StartDate = DateTime.UtcNow.AddMonths(4);

            //budgetGroupProposal.BudgetGroupXrefConversionActionProposals[1].ConversionActionId = new Guid("C248D9C7-2626-4498-B550-9F4C8778BEE7");
            //budgetGroupProposal.BudgetGroupXrefConversionActionProposals[1].ModifiedBy = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");
            //budgetGroupProposal.BudgetGroupXrefConversionActionProposals[1].Modified = DateTime.UtcNow;

            //budgetGroupProposal.BudgetGroupXrefConversionActionProposals.Add(new BudgetGroupXrefConversionActionProposal()
            //{
            //    BudgetGroupXrefConversionActionProposalId = Guid.NewGuid(),
            //    BudgetGroupProposalId = budgetGroupProposal.BudgetGroupProposalId,
            //    ConversionActionId = new Guid("C248D9C7-2626-4498-B550-9F4C8778BEE7"),
            //    Created = DateTime.UtcNow,
            //    CreatedBy = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE")
            //});


            //budgetGroupProposal.BudgetGroupXrefPlacementProposals[0].PlacementId = new Guid("499CEDA1-0560-4B4C-995E-A653318D1BBD");
            //budgetGroupProposal.BudgetGroupXrefPlacementProposals[0].AssignedBy = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");
            //budgetGroupProposal.BudgetGroupXrefPlacementProposals[0].AssignedDate = DateTime.UtcNow.AddDays(5);

            //budgetGroupProposal.BudgetGroupXrefPlacementProposals.Add(new BudgetGroupXrefPlacementProposal()
            //{
            //    BudgetGroupXrefPlacementProposalId = Guid.NewGuid(),
            //    BudgetGroupProposalId = budgetGroupProposal.BudgetGroupProposalId,
            //    PlacementId = new Guid("499CEDA1-0560-4B4C-995E-A653318D1BBD"),
            //    AssignedDate = DateTime.UtcNow,
            //    AssignedBy = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE")
            //});

            //Dictionary<string, List<Guid>> itemsToDelete = new Dictionary<string, List<Guid>>()
            //{
            //    {"ads", new List<Guid>() { } },
            //    {"locations", new List<Guid>() { } },
            //    {"timedBudgets", new List<Guid>() { } },
            //    {"conversionActions", new List<Guid>() },
            //    {"placements", new List<Guid>() { } }
            //};
            //BudgetGroupProposal updatedBudgetGroupProposal = _budgetGroupProposalStore.UpdateBudgetGroupProposal(budgetGroupProposal, itemsToDelete);
            //Assert.IsNotNull(updatedBudgetGroupProposal);
        }

        //[Ignore]
        //public void CreateBudgetGroupHistory_HappyPath_RowsAffectedGreaterThanZero()
        //{
        //    Guid id = Guid.NewGuid();
        //    string creator = "George";
        //    IBudgetGroupProposalStore dataStore = new BudgetGroupProposalStore();
        //    BudgetGroupHistory budgetGroupHistory = new BudgetGroupHistory()
        //    {
        //        BudgetGroupHistoryId = id,
        //        BudgetGroupId = new Guid("29A3A357-9FA4-485C-A682-010F4FBE76A5"),
        //        HistoryDate = DateTime.Now,
        //        BudgetGroupName = "Trial Budget",
        //        BudgetGroupBudget = 40000.00m,
        //        CreatedBy = creator,
        //        CreatedDatetime = null,
        //        BudgetGroupXrefAdHistories = new List<BudgetGroupXrefAdHistory>()
        //        {
        //            new BudgetGroupXrefAdHistory
        //            {
        //                BudgetGroupXrefAdHistoryId = Guid.NewGuid(),
        //                BudgetGroupHistoryId = id,
        //                HistoryDate = DateTime.Now,
        //                AdId = new Guid("6B5D7DD2-7A58-4005-AF1D-000DE031825C"),
        //                CreatedBy = creator,
        //                CreatedDatetime = null,
        //            },
        //            new BudgetGroupXrefAdHistory
        //            {
        //                BudgetGroupXrefAdHistoryId = Guid.NewGuid(),
        //                BudgetGroupHistoryId = id,
        //                HistoryDate = DateTime.Now,
        //                AdId = new Guid("6B5D7DD2-7A58-4005-AF1D-000DE031825C"),
        //                CreatedBy = creator,
        //                CreatedDatetime = DateTime.Now,
        //            }
        //        },
        //        BudgetGroupTimedBudgetHistory = new BudgetGroupTimedBudgetHistory()
        //        {
        //            BudgetGroupTimedBudgetHistoryId = Guid.NewGuid(),
        //            BudgetGroupHistoryId = id,
        //            HistoryDate = DateTime.Now,
        //            BudgetAmount = 15001.25m,
        //            Margin = .2m,
        //            StartDate = DateTime.Now,
        //            EndDate = DateTime.Now,
        //            CreatedBy = creator,
        //            CreatedDatetime = null
        //        },
        //        BudgetGroupXrefConversionActionHistories = new List<BudgetGroupXrefConversionActionHistory>()
        //        {
        //            new BudgetGroupXrefConversionActionHistory()
        //            {
        //                BudgetGroupXrefConversionActionHistoryId = Guid.NewGuid(),
        //                BudgetGroupHistoryId = id,
        //                HistoryDate = DateTime.Now,
        //                ConversionActionId = null,
        //                CreatedBy = creator,
        //                CreatedDatetime = null
        //            },
        //            new BudgetGroupXrefConversionActionHistory()
        //            {
        //                BudgetGroupXrefConversionActionHistoryId = Guid.NewGuid(),
        //                BudgetGroupHistoryId = id,
        //                HistoryDate = DateTime.Now,
        //                ConversionActionId = null,
        //                CreatedBy = creator,
        //                CreatedDatetime = DateTime.Now
        //            }
        //        },
        //        BudgetGroupXrefLocationHistories = new List<BudgetGroupXrefLocationHistory>()
        //        {
        //            new BudgetGroupXrefLocationHistory()
        //            {
        //                BudgetGroupXrefLocationHistoryId = Guid.NewGuid(),
        //                BudgetGroupHistoryId = id,
        //                HistoryDate = DateTime.Now,
        //                LocationId = new Guid("C1B1CCC7-D75C-48AD-B5DF-020A015CA93F"),
        //                CreatedBy = creator,
        //                CreatedDatetime = null,
        //            },
        //            new BudgetGroupXrefLocationHistory()
        //            {
        //                BudgetGroupXrefLocationHistoryId = Guid.NewGuid(),
        //                BudgetGroupHistoryId = id,
        //                HistoryDate = DateTime.Now,
        //                LocationId = new Guid("C1B1CCC7-D75C-48AD-B5DF-020A015CA93F"),
        //                CreatedBy = creator,
        //                CreatedDatetime = DateTime.Now,
        //            }
        //        },
        //        BudgetGroupXrefPlacementHistories = new List<BudgetGroupXrefPlacementHistory>()
        //        {
        //            new BudgetGroupXrefPlacementHistory()
        //            {
        //                BudgetGroupXrefPlacementHistoryId = Guid.NewGuid(),
        //                BudgetGroupHistoryId = id,
        //                HistoryDate = DateTime.Now,
        //                PlacementId = new Guid("BC878971-C818-4402-9B13-02A198E0139B"),
        //                CreatedBy = creator,
        //                CreatedDatetime = null
        //            },
        //            new BudgetGroupXrefPlacementHistory()
        //            {
        //                BudgetGroupXrefPlacementHistoryId = Guid.NewGuid(),
        //                BudgetGroupHistoryId = id,
        //                HistoryDate = DateTime.Now,
        //                PlacementId = new Guid("BC878971-C818-4402-9B13-02A198E0139B"),
        //                CreatedBy = creator,
        //                CreatedDatetime = DateTime.Now
        //            }
        //        },
        //    };
        //    int rowsAffected = dataStore.CreateBudgetGroupHistory(budgetGroupHistory);
        //    Assert.AreEqual(10, rowsAffected);
        //}

        //[TestMethod]
        //public void GetBudgetGroupById_BudgetGroupExists_ReturnBudgetGroup()
        //{
        //    IBudgetGroupProposalStore dataStore = new BudgetGroupProposalStore();
        //    BudgetGroup budgetGroup = dataStore.GetBudgetGroupById(new Guid("61F24DD3-B377-4BA4-A340-78664B69BD79"));
        //    Assert.IsNotNull(budgetGroup);
        //}

        //[TestMethod]
        //public void GetBudgetGroupById_BudgetGroupDoesNotExist_ReturnNull()
        //{
        //    IBudgetGroupProposalStore dataStore = new BudgetGroupProposalStore();
        //    BudgetGroup budgetGroup = dataStore.GetBudgetGroupById(new Guid("21F24DD3-B377-4BA4-A340-78664B69BD79"));
        //    Assert.IsNull(budgetGroup);
        //}

        //[Ignore]
        //[TestMethod]
        //public void GetBudgetGroupById_BudgetGroupExistSomeOrAllChildrentDoNotExist_ReturnBudgetGroup()
        //{
        //    throw new NotImplementedException();
        //}

        //[TestMethod]
        //public void GetBudgetGroupProposalById_BudgetGroupProposalExists_ReturnBudgetGroupProposal()
        //{
        //    IBudgetGroupProposalStore dataStore = new BudgetGroupProposalStore();
        //    BudgetGroupProposal budgetGroupProposal = dataStore.GetBudgetGroupProposalById(new Guid("90FD9112-CB00-45C1-834A-15D163559710"));
        //    Assert.IsNotNull(budgetGroupProposal);
        //}

        //[TestMethod]
        //public void GetBudgetGroupProposalById_BudgetGroupProposalDoesNotExist_ReturnNull()
        //{
        //    IBudgetGroupProposalStore dataStore = new BudgetGroupProposalStore();
        //    BudgetGroupProposal budgetGroupProposal = dataStore.GetBudgetGroupProposalById(new Guid("EB933B38-8A27-48BA-9C77-C880C3D5A5A2"));
        //    Assert.IsNull(budgetGroupProposal);
        //}

        //[Ignore]
        //[TestMethod]
        //public void GetBudgetGroupProposalById_BudgetGroupProposalExistSomeOrAllChildrentDoNotExist_ReturnBudgetGroupProposal()
        //{
        //    throw new NotImplementedException();
        //}

        //[Ignore]
        //[TestMethod]
        //public void GetBudgetGroupHistoryById_BudgetGroupHistoryExists_ReturnBudgetGroupHistory()
        //{
        //    IBudgetGroupProposalStore dataStore = new BudgetGroupProposalStore();
        //    BudgetGroupHistory budgetGroupHistory = dataStore.GetBudgetGroupHistoryById(new Guid("623CA7A5-0698-42BE-B955-777273F99555"));
        //    Assert.IsNotNull(budgetGroupHistory);
        //}

        //[Ignore]
        //[TestMethod]
        //public void GetBudgetGroupHistoryById_BudgetGroupHistoryDoesNotExist_ReturnNull()
        //{
        //    IBudgetGroupProposalStore dataStore = new BudgetGroupProposalStore();
        //    BudgetGroupProposal budgetGroupHistory = dataStore.GetBudgetGroupProposalById(new Guid("22933B38-8A27-48BA-9C77-C880C3D5A5A2"));
        //    Assert.IsNull(budgetGroupHistory);
        //}

        //[Ignore]
        //[TestMethod]
        //public void GetBudgetGroupHistoryById_BudgetGroupHistoryExistSomeOrAllChildrentDoNotExist_ReturnBudgetGroupHistory()
        //{
        //    throw new NotImplementedException();
        //}

        //[TestMethod]
        //public void UpdateBudgetGroupProposal_BudgetGroupProposalExists_ReturnRowsAffected()
        //{
        //    IBudgetGroupProposalStore dataStore = new BudgetGroupProposalStore();
        //    BudgetGroupProposal budgetGroupProposal = dataStore.GetBudgetGroupProposalById(new Guid("128C5291-7006-4D8D-A31B-C00C4D7C2D77"));
        //    budgetGroupProposal.BudgetGroupId = new Guid("6326DCDF-3728-4EAA-880F-F8C3C5B1942D");
        //    budgetGroupProposal.BudgetGroupName = "Testing Update";
        //    int rowsAffected = dataStore.UpdateBudgetGroupProposal(budgetGroupProposal);
        //    Assert.IsTrue(rowsAffected > 0);
        //}

        //[TestMethod]
        //public void GetOrderXrefBudgetGroup_OrderXrefBudgetGroupExists_ReturnOrderXrefBudgetGroup()
        //{
        //    IBudgetGroupProposalStore dataStore = new BudgetGroupProposalStore();
        //    OrderXrefBudgetGroup orderXrefBudgetGroup = dataStore.GetOrderXrefBudgetGroup(new Guid("4DA004C5-8C0D-4158-9014-00702C395DF1"));
        //    Assert.IsNotNull(orderXrefBudgetGroup);
        //}

        [Ignore]
        [TestMethod]
        public void GetBudgetGroupLedger()
        {
            var bgId = Guid.Parse("A55C19D8-06F4-4C8D-86F9-593154E3E41B");
            var returedVal = _budgetGroupLedgerStore.GetBudgetGroupLedgerByBudgetGroup(bgId);

            Assert.IsNotNull(returedVal);
        }
    }
}
