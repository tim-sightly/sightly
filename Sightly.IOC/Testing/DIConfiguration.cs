﻿using System;
using Autofac;

namespace Sightly.IOC.Testing
{
    public class DIConfiguration
    {
        private static readonly Lazy<IContainer> Container = new Lazy<IContainer>(() =>
        {
            var builder = new ContainerBuilder();
            (new AutofacConfiguration()).RegisterServices(builder);
            var container = builder.Build();
            return container;
        });
        public static IContainer GetConfiguredContainer()
        {
            return Container.Value;
        }
    }
}
