﻿using Autofac;
using Sightly.Adwords.Utility;
using Sightly.Adwords.Utility.Interface;
using Sightly.Business.Association.Ad;
using Sightly.Business.Association.Campaign;
using Sightly.Business.Association.Customer;
using Sightly.Business.Interfaces;
using Sightly.Business.Utilities;
using Sightly.Business.MediaAgency;
using Sightly.Business.MediaAgency.Crud;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Business.MediaAgency.AwModel;
using Sightly.Business.OrderManager;
using Sightly.Business.OrderManager.Crud;
using Sightly.Business.OrderManager.Crud.Interfaces;
using Sightly.Business.StatsGather;
using Sightly.Business.StatsGather.Raw;
using Sightly.Business.StatsTransformer;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.BusinessLayer;
using Sightly.BusinessServices;
using Sightly.BusinessServices.cs;
using Sightly.DAL;
using Sightly.Logging;
using Sightly.TargetView.AdwordsValidation;
using Sightly.TargetView.AdwordsValidation.Interfaces;
using Sightly.TargetView.Services.Validation;
using Sightly.TargetViewDb.DAL.DataStores;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.TargetViewDb.DAL.Utilities;
using Sightly.Service.YoutubeUploader;
using Sightly.DoubleVerify.Utility;
using Sightly.Moat.Utility;
using Sightly.Nielsen.Utility;
using Sightly.Email;
using Sightly.Order;
using Sightly.ReportGeneration.Generators.Internal;
using Sightly.ReportGeneration.Internal.Generators;
using Sightly.ReportGeneration.Internal.Generators.Interface;
using CampaignHelper = Sightly.Business.MediaAgency.Crud.CampaignHelper;
using ICampaignHelper = Sightly.Business.MediaAgency.Crud.Interface.ICampaignHelper;

namespace Sightly.IOC
{
    public class AutofacConfiguration
    {
        private ContainerBuilder _builder;

        public ContainerBuilder Builder
        {
            get { return _builder ?? new ContainerBuilder(); }
        }

        public void RegisterServices(ContainerBuilder builder)
        {
            //Adword Types
            builder.RegisterType<AccountHierarchy>().As<IAccountHierarchy>().InstancePerLifetimeScope();
            builder.RegisterType<AccountBudget>().As<IAccountBudget>().InstancePerLifetimeScope();
            builder.RegisterType<AdwordAd>().As<IAd>().InstancePerLifetimeScope();
            builder.RegisterType<AdwordAdGroup>().As<IAdGroup>().InstancePerLifetimeScope();
            builder.RegisterType<AdwordBudget>().As<IBudget>().InstancePerLifetimeScope();
            builder.RegisterType<AdwordCampaign>().As<ICampaign>().InstancePerLifetimeScope();
            builder.RegisterType<AdwordVideo>().As<IVideo>().InstancePerLifetimeScope();
            builder.RegisterType<CustomerAgent>().As<ICustomerAgent>().InstancePerLifetimeScope();

            //Adwords free Stats

            builder.RegisterType<AdDevicePerformanceFree>().As<IAdDevicePerformanceFree>().InstancePerLifetimeScope();
            builder.RegisterType<AdGroupPerformanceFree>().As<IAdGroupPerformanceFree>().InstancePerLifetimeScope();
            builder.RegisterType<AgeRangePerformanceFree>().As<IAgeRangePerformanceFree>().InstancePerLifetimeScope();
            builder.RegisterType<GenderPerformanceFree>().As<IGenderPerformanceFree>().InstancePerLifetimeScope();
            builder.RegisterType<GeoPerformanceFree>().As<IGeoPerformanceFree>().InstancePerLifetimeScope();


            //Adword Reports
            builder.RegisterType<AdGroupPerformance>().As<IAdGroupPerformance>().InstancePerLifetimeScope();
            builder.RegisterType<AdPerformance>().As<IAdPerformance>().InstancePerLifetimeScope();
            builder.RegisterType<AdDevicePerformance>().As<IAdDevicePerformance>().InstancePerLifetimeScope();
            builder.RegisterType<AgeRangePerformance>().As<IAgeRangePerformance>().InstancePerLifetimeScope();
            builder.RegisterType<CampaignPerformance>().As<ICampaignPerformance>().InstancePerLifetimeScope();
            builder.RegisterType<CampaignLabel>().As<ICampaignLabel>().InstancePerLifetimeScope();
            builder.RegisterType<DevicePerformance>().As<IDevicePerformance>().InstancePerLifetimeScope();
            builder.RegisterType<GenderPerformance>().As<IGenderPerformance>().InstancePerLifetimeScope();
            builder.RegisterType<GeoPerformance>().As<IGeoPerformance>().InstancePerLifetimeScope();
            builder.RegisterType<ParentalPerformance>().As<IParentalPerformance>().InstancePerLifetimeScope();


            //DAL Data Stores
            builder.RegisterType<TargetViewDataStore>().As<ITargetViewDataStore>().InstancePerLifetimeScope();
            builder.RegisterType<TvAdDataStore>().As<ITvAdDataStore>().InstancePerLifetimeScope();
            builder.RegisterType<TvOrderDataStore>().As<ITvOrderDataStore>().InstancePerLifetimeScope();
            builder.RegisterType<TvStatDataStore>().As<ITvStatDataStore>().InstancePerLifetimeScope();
            builder.RegisterType<TvRawStatsDataStore>().As<ITvRawStatsDataStore>().InstancePerLifetimeScope();
            builder.RegisterType<WebConnectionStore>().As<IConnectionStringStore>().InstancePerLifetimeScope();
            builder.RegisterType<TvDbDataStore>().As<ITvDbDataStore>().InstancePerLifetimeScope();

            //DAL
            builder.RegisterType<AccountStore>().As<IAccountStore>().InstancePerLifetimeScope();
            builder.RegisterType<AlertCentralStore>().As<IAlertCentralStore>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupLedgerStore>().As<IBudgetGroupLedgerStore>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupHistoryStore>().As<IBudgetGroupHistoryStore>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupProposalStore>().As<IBudgetGroupProposalStore>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupStore>().As<IBudgetGroupStore>().InstancePerLifetimeScope();
            builder.RegisterType<OrderStore>().As<IOrderStore>().InstancePerLifetimeScope();
            builder.RegisterType<PacingStore>().As<IPacingStore>().InstancePerLifetimeScope();
            builder.RegisterType<StateTransitionStore>().As<IStateTransitionStore>().InstancePerLifetimeScope();
            builder.RegisterType<CampaignStore>().As<ICampaignStore>().InstancePerLifetimeScope();
            builder.RegisterType<CampaignCreationStore>().As<ICampaignCreationStore>().InstancePerLifetimeScope();
            builder.RegisterType<UserStore>().As<IUserStore>().InstancePerLifetimeScope();
            builder.RegisterType<StatStore>().As<IStatStore>().InstancePerLifetimeScope();
            builder.RegisterType<KpiStore>().As<IKpiStore>().InstancePerLifetimeScope();

            //External Stores
            builder.RegisterType<DoubleVerifyStore>().As<IDoubleVerifyStore>().InstancePerLifetimeScope();
            builder.RegisterType<NielsenStore>().As<INielsenStore>().InstancePerLifetimeScope();
            builder.RegisterType<MoatStore>().As<IMoatStore>().InstancePerLifetimeScope();



            //DAL Repositories
            builder.RegisterType<AdRepository>().As<IAdRepository>().InstancePerLifetimeScope();
            builder.RegisterType<OrderRepository>().As<IOrderRepository>().InstancePerLifetimeScope();
            builder.RegisterType<RawStatsRepository>().As<IRawStatsRepository>().InstancePerLifetimeScope();
            builder.RegisterType<StatRepository>().As<IStatRepository>().InstancePerLifetimeScope();
            builder.RegisterType<TargetViewRepository>().As<ITargetViewRepository>().InstancePerLifetimeScope();
            builder.RegisterType<TvDbRepository>().As<ITvDbRepository>().InstancePerLifetimeScope();

            //DAL Utilities
            builder.RegisterType<StatsXmlSerializer>().As<IStatsXmlSerializer>().InstancePerLifetimeScope();


            //Business Layer
            builder.RegisterType<AccountManager>().As<IAccountManager>().InstancePerLifetimeScope();
            builder.RegisterType<AlertCentralManager>().As<IAlertCentralManager>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupHistoryManager>().As<IBudgetGroupHistoryManager>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupManager>().As<IBudgetGroupManager>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupProposalManager>().As<IBudgetGroupProposalManager>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupStateTransitionManager>().As<IBudgetGroupStateTransitionManager>().InstancePerLifetimeScope();
            builder.RegisterType<CampaignManager>().As<ICampaignManager>().InstancePerLifetimeScope();
            builder.RegisterType<CampaignCreationManager>().As<ICampaignCreationManager>().InstancePerLifetimeScope();
            builder.RegisterType<PacingManager>().As<IPacingManager>().InstancePerLifetimeScope();
            builder.RegisterType<UserManager>().As<IUserManager>().InstancePerLifetimeScope();
            builder.RegisterType<StatManager>().As<IStatManager>().InstancePerLifetimeScope();
            builder.RegisterType<AccountManager>().As<IAccountManager>().InstancePerLifetimeScope();
            builder.RegisterType<EmailManager>().As<IEmailManager>().InstancePerLifetimeScope();
            builder.RegisterType<KpiManager>().As<IKpiManager>().InstancePerLifetimeScope();
            builder.RegisterType<OrderManager>().As<IOrderManager>().InstancePerLifetimeScope();
            builder.RegisterType<OrderManagerSm>().As<IOrderManagerSm>().InstancePerLifetimeScope();

            //Service
            builder.RegisterType<UpDownLoader>().As<IUpDownLoader>().InstancePerLifetimeScope();
            builder.RegisterType<DoubleVerifyManager>().As<IDoubleVerifyManager>().InstancePerLifetimeScope();
            builder.RegisterType<MoatManager>().As<IMoatManager>().InstancePerLifetimeScope();
            builder.RegisterType<NielsenManager>().As<INielsenManager>().InstancePerLifetimeScope();
            builder.RegisterType<EmailClient>().As<IEmailClient>().InstancePerLifetimeScope();

            //External Services
            builder.RegisterType<DoubleVerifyDataService>().As<IDoubleVerifyDataService>().InstancePerLifetimeScope();
            builder.RegisterType<MoatDataService>().As<IMoatDataService>().InstancePerLifetimeScope();
            builder.RegisterType<NielsenDataService>().As<INielsenDataService>().InstancePerLifetimeScope();

            //Business Service
            builder.RegisterType<BudgetGroupProposalService>().As<IBudgetGroupProposalService>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupService>().As<IBudgetGroupService>().InstancePerLifetimeScope();
            builder.RegisterType<StateTransitionService>().As<IStateTransitionService>().InstancePerLifetimeScope();
            builder.RegisterType<CampaignService>().As<ICampaignService>().InstancePerLifetimeScope();
            builder.RegisterType<CampaignCreationService>().As<ICampaignCreationService>().InstancePerLifetimeScope();
            builder.RegisterType<PacingService>().As<IPacingService>().InstancePerLifetimeScope();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerLifetimeScope();
            builder.RegisterType<RawStatsService>().As<IRawStatsService>().InstancePerLifetimeScope();
            builder.RegisterType<StatService>().As<IStatService>().InstancePerLifetimeScope();
            builder.RegisterType<AccountService>().As<IAccountService>().InstancePerLifetimeScope();
            builder.RegisterType<AlertCentralService>().As<IAlertCentralService>().InstancePerLifetimeScope();
            builder.RegisterType<EmailService>().As<IEmailService>().InstancePerLifetimeScope();
            builder.RegisterType<KpiService>().As<IKpiService>().InstancePerLifetimeScope();
            builder.RegisterType<OrderService>().As<IOrderService>().InstancePerLifetimeScope();

            builder.RegisterType<DoubleVerifyService>().As<IDoubleVerifyService>().InstancePerLifetimeScope();
            builder.RegisterType<MoatService>().As<IMoatService>().InstancePerLifetimeScope();
            builder.RegisterType<NielsenService>().As<INielsenService>().InstancePerLifetimeScope();

            //Logging
            builder.RegisterType<LoggingService>().As<ILoggingService>().InstancePerLifetimeScope();
            builder.RegisterType<LoggingWrapper>().As<ILoggingWrapper>().InstancePerLifetimeScope();
            //            builder.RegisterType<>().As<>().InstancePerLifetimeScope();


            //Business Logic
            builder.RegisterType<PlacementValidation>().As<IPlacementValidation>().InstancePerLifetimeScope();
            builder.RegisterType<TargetViewAdValidation>().As<IAdValidation>().InstancePerLifetimeScope();
            builder.RegisterType<TargetViewAdwordsValidation>().As<ITargetViewAdwordsValidation>().InstancePerLifetimeScope();

            //Associations
            builder.RegisterType<CustomerAssociation>().As<ICustomerAssociation>().InstancePerLifetimeScope();
            builder.RegisterType<CampaignAssociation>().As<ICampaignAssociation>().InstancePerLifetimeScope();
            builder.RegisterType<AdAssociation>().As<IAdAssociation>().InstancePerLifetimeScope();

            //Media Agency
            builder.RegisterType<AwDataAssociation>().As<IAwDataAssociation>().InstancePerLifetimeScope();
            builder.RegisterType<DomainManager>().As<IDomainManager>().InstancePerLifetimeScope();

            //Crud Helpers
            builder.RegisterType<AccountHelper>().As<IAccountHelper>().InstancePerLifetimeScope();
            builder.RegisterType<AdAdwordsInfoHelper>().As<IAdAdwordsInfoHelper>().InstancePerLifetimeScope();
            builder.RegisterType<TvAdHelper>().As<ITvAdHelper>().InstancePerLifetimeScope();
            builder.RegisterType<AdvertiserHelper>().As<IAdvertiserHelper>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupAdWordsInfoHelper>().As<IBudgetGroupAdWordsInfoHelper>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupHelper>().As<IBudgetGroupHelper>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupTimedBudgetHelper>().As<IBudgetGroupTimedBudgetHelper>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupXrefAdHelper>().As<IBudgetGroupXrefAdHelper>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupXrefLocationHelper>().As<IBudgetGroupXrefLocationHelper>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupXrefPlacementHelper>().As<IBudgetGroupXrefPlacementHelper>().InstancePerLifetimeScope();
            builder.RegisterType<CampaignAdWordsInfoHelper>().As<ICampaignAdWordsInfoHelper>().InstancePerLifetimeScope();
            builder.RegisterType<CampaignHelper>().As<ICampaignHelper>().InstancePerLifetimeScope();
            builder.RegisterType<FileUploadHistoryHelper>().As<IFileUploadHistoryHelper>().InstancePerLifetimeScope();
            builder.RegisterType<LocationHelper>().As<ILocationHelper>().InstancePerLifetimeScope();
            builder.RegisterType<TvOrderHelper>().As<ITvOrderHelper>().InstancePerLifetimeScope();
            builder.RegisterType<OrderTargetAgeGroupHelper>().As<IOrderTargetAgeGroupHelper>().InstancePerLifetimeScope();
            builder.RegisterType<OrderTargetGenderHelper>().As<IOrderTargetGenderHelper>().InstancePerLifetimeScope();
            builder.RegisterType<OrderXrefBudgetGroupHelper>().As<IOrderXrefBudgetGroupHelper>().InstancePerLifetimeScope();
            builder.RegisterType<PlacementHelper>().As<IPlacementHelper>().InstancePerLifetimeScope();
            builder.RegisterType<VideoAssetHelper>().As<IVideoAssetHelper>().InstancePerLifetimeScope();
            builder.RegisterType<VideoAssetVersionHelper>().As<IVideoAssetVersionHelper>().InstancePerLifetimeScope();

            builder.RegisterType<AdHelper>().As<IAdHelper>().InstancePerLifetimeScope();
            builder.RegisterType<AudienceHelper>().As<IAudienceHelper>().InstancePerLifetimeScope();
            builder.RegisterType<CampaignHelper>().As<ICampaignHelper>().InstancePerLifetimeScope();
            builder.RegisterType<GeoHelper>().As<IGeoHelper>().InstancePerLifetimeScope();
            builder.RegisterType<InfoHelper>().As<IInfoHelper>().InstancePerLifetimeScope();
            builder.RegisterType<NewOrderBlobHelper>().As<INewOrderBlobHelper>().InstancePerLifetimeScope();
            builder.RegisterType<OrderHelper>().As<IOrderHelper>().InstancePerLifetimeScope();


            //Transforms
            builder.RegisterType<AdStatsTransform>().As<IAdStatsTransform>().InstancePerLifetimeScope();
            builder.RegisterType<AgeGroupStatsTransform>().As<IAgeGroupStatsTransform>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupStatsTransform>().As<IBudgetGroupStatsTransform>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupAdStatsTransform>().As<IBudgetGroupAdStatsTransform>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupAgeStatsTransform>().As<IBudgetGroupAgeStatsTransform>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupDeviceStatsTransform>().As<IBudgetGroupDeviceStatsTransform>().InstancePerLifetimeScope();
            builder.RegisterType<BudgetGroupGenderStatsTransform>().As<IBudgetGroupGenderStatsTransform>().InstancePerLifetimeScope();
            builder.RegisterType<DeviceStatsTransform>().As<IDeviceStatsTransform>().InstancePerLifetimeScope();
            builder.RegisterType<GenderStatsTransform>().As<IGenderStatsTransform>().InstancePerLifetimeScope();
            builder.RegisterType<OrderStatsTransform>().As<IOrderStatsTransform>().InstancePerLifetimeScope();

            //Services
            builder.RegisterType<AdwordsToTargetview>().As<IAdwordsToTargetview>().InstancePerLifetimeScope();
            builder.RegisterType<RawStatsService>().As<IRawStatsService>().InstancePerLifetimeScope();

            builder.RegisterType<OrderComparison>().As<IOrderComparison>().InstancePerLifetimeScope();

            builder.RegisterType<GatherStats>().As<IGatherStats>().InstancePerLifetimeScope();
            

            //Reporting
            builder.RegisterType<ChangeOrderDetailExcel>().As<IExcelReport>();

            _builder = builder;
        }
    }
}
