using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Azure.WebJobs;
using Sightly.Adwords.Utility.Interface;
using Sightly.Business.Association.Ad;
using Sightly.Business.Association.Campaign;
using Sightly.Business.Association.Customer;
using Sightly.Business.StatsGather;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Pipeline
{
    public class PipelineControl
    {
        private readonly IAdAssociation _adAssociation;
        private readonly IAccountHierarchy _accountHierarchy;
        private readonly ICampaignAssociation _campaignAssociation;
        private readonly ICustomerAssociation _customerAssociation;
        private readonly IGatherStats _gatherStats;
        private readonly IStatRepository _statRepository;

        public PipelineControl(
                               IAdAssociation adAssociation, 
                               IAccountHierarchy accountHierarchy,
                               ICampaignAssociation campaignAssociation,
                               ICustomerAssociation customerAssociation,
                               IGatherStats gatherStats,
                               IStatRepository statRepository)
        {
            _adAssociation = adAssociation;
            _accountHierarchy = accountHierarchy;
            _campaignAssociation = campaignAssociation;
            _customerAssociation = customerAssociation;
            _gatherStats = gatherStats;
            _statRepository = statRepository;
        }



        [NoAutomaticTrigger]
        public void Execute()
        {
            Console.WriteLine($"Started: {DateTime.Now}");


            //DailyStatsGather();

            //Regather3DaysAgo();

            //RunDailyMissingCidAndStatDates();
            var customerIds = new List<long>
            {
                7238689194


            };

            customerIds.ForEach(GatherCustomerStatsForTimeRange);


            Console.WriteLine($"Completed: {DateTime.Now}");
        }

        private void GatherCustomerStatsForTimeRange(long cid)
        {
            var startDate = new DateTime(2018, 10, 16);
            var endDate = new DateTime(2018, 10, 22);


            Console.WriteLine($"{DateTime.Now} -  {cid}");
            for (var countDate = startDate; countDate.Date <= endDate.Date; countDate = countDate.AddDays(1))
            {

                _statRepository.DeleteRawStatsData(cid, countDate);
                Console.WriteLine($"{DateTime.Now} -  {countDate}");
                _gatherStats.Execute(cid, countDate);
                

                SaveOffValidation(cid, countDate);
            }
        }

        private void GatherNielseStatsOverTimeRange()
        {
            var startDate = new DateTime(2017, 06, 16);
            var endDate = new DateTime(2017, 06, 30);

            for (var countDate = startDate; countDate.Date <= endDate.Date; countDate = countDate.AddDays(1))
            {
                try
                { 
                    Console.WriteLine($"{DateTime.Now} - Nielsen for {countDate}");
                    var campRefernce = ValidateNielsenCampaignStructure(countDate);
                    GetNielsenStats(campRefernce, countDate);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
            }
        }

        [NoAutomaticTrigger]
        public void  GatherCustomerStatsForTimeRange()
        {
            var startDate = new DateTime(2018, 08, 08);
            var endDate = new DateTime(2018, 08, 09);
             var customerId = 8506065947;


            for (var countDate = startDate; countDate.Date <= endDate.Date; countDate = countDate.AddDays(1))
            {

                _statRepository.DeleteRawStatsData(customerId, countDate);
                Console.WriteLine($"{DateTime.Now} -  {countDate}");
                _gatherStats.Execute(customerId, countDate);

                //_gatherStats.RunMoat(countDate);

                SaveOffValidation(customerId, countDate);
            }
        }


        private void GatherAdGroupGeoStatsForTimeRange()
        {
            var startDate = new DateTime(2017, 12, 10);
            var endDate = new DateTime(2017, 12, 12);
            var customerId = 1285494141;


            for (var countDate = startDate; countDate.Date <= endDate.Date; countDate = countDate.AddDays(1))
            {

                //_statRepository.DeleteRawStatsData(customerId, countDate);
                Console.WriteLine($"{DateTime.Now} -  {countDate}");
                _gatherStats.RunAdGroupGeoStats(customerId, countDate);

                //_gatherStats.RunMoat(countDate);

                SaveOffValidation(customerId, countDate);
            }
        }
        private void Regather3DaysAgo()
        {
            GetAllStatsFromAdwordsByDate(Get3DaysAgoDate());
        }

        private void DailyStatsGather()
        {
            var yesterday = GetPreviousDate();
           
            try
            {
                GetAllStatsFromTvByDate(yesterday);
                _gatherStats.RunMoat(yesterday);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            try
            {

                var campRefernce = ValidateNielsenCampaignStructure(yesterday);
                GetNielsenStats(campRefernce, yesterday);
            }
            catch (Exception e)
            {
               Console.WriteLine(e);
            }
        }

        private void TestNielsenStats()
        {
            var yesterday = new DateTime(2018, 4, 25);
            
            try
            {
                var campRefernce  = ValidateNielsenCampaignStructure(yesterday);
                GetNielsenStats(campRefernce, yesterday);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void GetNielsenStatsForCidOverTime(long customerId, DateTime startDate, DateTime endDate)
        {
            for (var countDate = startDate; countDate.Date <= endDate.Date; countDate = countDate.AddDays(1))
            {
                Console.WriteLine($"{DateTime.Now} - CustomerId: {customerId} for {countDate}");
                GetNielsenStatsByCidAndDate(customerId, countDate);
            }
        }

        private void GetNielsenStatsByCidAndDate(long customerId, DateTime date)
        {
            _gatherStats.RunNielsen(customerId, date);
            var campList = new List<long> {customerId};
            _gatherStats.RunNielseDarUpdate(campList, date );
        }

        private void GetNielsenStats(List<NielsenCampaignReference> campaignReferences, DateTime yesterday)
        {
            _gatherStats.RunNielsen(campaignReferences, yesterday);

            var nielsenCiDs = _statRepository.GetCustomerIdsFromNielsenData(yesterday);

            _gatherStats.RunNielseDarUpdate(nielsenCiDs, yesterday);

        }

        private List<NielsenCampaignReference> ValidateNielsenCampaignStructure(DateTime date)
        {
             return _gatherStats.VerifyNielsenCampaignStructure(date);
        }

        private void GetAllStatsFromAdwordsByDate(DateTime statDate)
        {

            var customers = GetAllCustomerIdsFromAwAndTv();

            customers.ForEach(customer =>
            {
             Console.WriteLine($"{DateTime.Now} - CustomerId: {customer} for {statDate}");
                try
                {
                    _statRepository.DeleteRawStatsData(customer, statDate);
                    _gatherStats.Execute(customer, statDate);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"---------Issue Gathering {customer} for {statDate}--------------");
                    Console.WriteLine(e);
                    _statRepository.InsertMissingCidAndDate(customer, statDate);
                }
                finally
                {
                    SaveOffValidation(customer, statDate);
                }
            });
            try
            {
                _gatherStats.RunMoat(statDate);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }


            try
            {

                var campRefernce = ValidateNielsenCampaignStructure(statDate);
                GetNielsenStats(campRefernce, statDate);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }

        private void ValidateDayOfStats()
        {
            var startDate = new DateTime(2017, 11, 1);
            var endDate = new DateTime(2017, 11, 29);
            var customers = GetAllCustomerIdsFromAwAndTv();


            for (var countDate = startDate; countDate.Date <= endDate.Date; countDate = countDate.AddDays(1))
            {
                customers.ForEach(customer =>
                {
                    Console.WriteLine($"{customer} - {countDate}");
                    SaveOffValidation(customer, countDate);
                });
            }
        }

        private void RunDailyMissingCidAndStatDates()
        {
            var customerStatDate = _statRepository.GetMissingCidStatsByDate(Get3DaysAgoDate());

            customerStatDate.ForEach(cs =>
            {
                try
                {
                    _statRepository.DeleteRawStatsData(cs.CustomerId, cs.StatDate);
                    _gatherStats.Execute(cs.CustomerId, cs.StatDate);

                    Console.WriteLine($"{DateTime.Now} - CustomerId: {cs.CustomerId} for {cs.StatDate}");
                    _statRepository.DeleteMissingCidAndDate(cs.CustomerId, cs.StatDate);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"---------Issue Gathering {cs.CustomerId} for {cs.StatDate}--------------");
                    Console.WriteLine(e);
                    _statRepository.InsertMissingCidAndDate(cs.CustomerId, cs.StatDate);
                }
            });
        }

        private void GetAllStatsFromTvByDate(DateTime statdate)
        {
            //Get All CustomerIds that were running yesterday from TargetVeiw
            var customers = _statRepository.GetAllRunningAdWordsCidsFromTvByDate(statdate);

            //Remove all Mcc ID's from list
            var mccList = _statRepository.GetMccCustomerList();

            customers.RemoveAll(s => mccList.Contains(s));

            customers.GroupBy(c => c).ToList().ForEach(customer =>
            {
                Console.WriteLine($"{DateTime.Now} - CustomerId: {customer.Key} for {statdate}");
                try
                {
                    _statRepository.DeleteRawStatsData(customer.Key, statdate);
                    _gatherStats.Execute(customer.Key, statdate);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"---------Issue Gathering {customer.Key} for {statdate}--------------");
                    Console.WriteLine(e);
                    _statRepository.InsertMissingCidAndDate(customer.Key, statdate);
                }
                finally
                {
                    SaveOffValidation(customer.Key, statdate);
                }
            });

            _gatherStats.RunMoat(statdate);
        }



        private List<long> GetAllCustomerIdsFromAwAndTv()
        {
            var customerList = new List<long>();

            //Get list of all the Media Agency account we should track
            var mccCustomers = _statRepository.GetMediaAgencyMccCustomerList();

            mccCustomers.ForEach(customer =>
            {
                try
                {
                    customerList.AddRange(_accountHierarchy.GetAllCustomersFromMCC(customer));
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error gathering hierarchy for {customer} : {ex}");
                }
            });

            //Add all of the ChannelPartner CID's too
            var channelParters = _statRepository.GetChannelPartnerMccCustomerList();

            channelParters.ForEach(cp =>
            {
                if (cp.AdwordsCustomerId != null)
                    customerList.Add(cp.AdwordsCustomerId.Value);
            });

            //Remove all Mcc ID's from list
            var mccList = _statRepository.GetMccCustomerList();

            return customerList.Except(mccList).ToList();
        }

        private static DateTime GetPreviousDate()
        {
            var currentDate = DateTime.UtcNow.AddDays(-1);
            return new DateTime(currentDate.Ticks, DateTimeKind.Utc);
        }

        private static DateTime Get3DaysAgoDate()
        {

            var currentDate = DateTime.UtcNow.AddDays(-3);
            return new DateTime(currentDate.Ticks, DateTimeKind.Utc);
        }

        private void SaveOffValidation(long customerId, DateTime statDate)
        {
            var dailyChecker =
                _statRepository.GetValidationForDailyRawDataByCustomerIdAndDate(customerId, statDate);
            var reportedStats = new CustomerRawStatsByDayDate { CustomerId = customerId, StatDate = statDate };
            dailyChecker.ForEach(campaign =>
            {
                reportedStats.AdClicks += campaign.AdClicks;
                reportedStats.AgeClicks += campaign.AgeClicks;
                reportedStats.GenderClicks += campaign.GenderClicks;
                reportedStats.AdCost += campaign.AdCost;
                reportedStats.AgeCost += campaign.AgeCost;
                reportedStats.GenderCost += campaign.GenderCost;
                reportedStats.AdImpressions += campaign.AdImpressions;
                reportedStats.AgeImpressions += campaign.AgeImpressions;
                reportedStats.GenderImpressions += campaign.GenderImpressions;
                reportedStats.AdViews += campaign.AdViews;
                reportedStats.AgeViews += campaign.AgeViews;
                reportedStats.GenderViews += campaign.GenderViews;
            });
            _statRepository.InsertDailyStatsValidation(reportedStats);
        }

        private static bool ValidateRawData(List<CustomerCampaignRawStatsByDayData> dailyChecker)
        {
            bool validated = true;

            dailyChecker.ForEach(row =>
            {
                if (
                    (row.AdClicks != row.AgeClicks || row.AdClicks != row.GenderClicks || row.AgeClicks != row.GenderClicks) ||
                    (row.AdCost != row.AgeCost || row.AdCost != row.GenderCost || row.AgeCost != row.GenderCost) ||
                    (row.AdImpressions != row.AgeImpressions || row.AdImpressions != row.GenderImpressions || row.AgeImpressions != row.GenderImpressions) ||
                    (row.AdViews != row.AgeViews || row.AdViews != row.GenderViews || row.AgeViews != row.GenderViews)
                )
                    validated = false;
            });

            return validated;
        }

        

    }
}