﻿using Autofac;
using Microsoft.Azure.WebJobs;
using Sightly.IOC;


namespace Sightly.Pipeline
{
    public class Program
    {
        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            (new AutofacConfiguration()).RegisterServices(builder);
            builder.RegisterType<PipelineControl>();
            var container = builder.Build();
            

            var jobHostConfig = new JobHostConfiguration
            {
                JobActivator = new DiJobActivator(container)
            };

            var host = new JobHost(jobHostConfig);
            host.Call(typeof(PipelineControl).GetMethod("Execute"));
        }

    }


}
