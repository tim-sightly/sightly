using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Sightly.Models;
using Sightly.Tools;

namespace Sightly.Moat.Utility
{
    public class MoatDataService : IMoatDataService
    {
        public async Task<List<MoatDailyStatData>> GetDailyMoatStats(DateTime statDate)
        {
            var dailyMoatDetailList = new List<MoatDailyStatData>();
            var userName = "sightly_youtube@moat.com";
            var password = "Twcgh73435#";
            var statDateFormated = statDate.ToString("yyyyMMdd");

            var url = $"https://api.moat.com/1/stats.json?start={statDateFormated}&end={statDateFormated}&columns=date,level1,impressions_analyzed,loads_unfiltered,strict_or_px_2sec_consec_video_ots_unfiltered,l_somehow_measurable_unfiltered,human_and_avoc,measurable_impressions,reached_complete_sum";
            try
            {

                //var jsonReturnedData = await API.Moat.GetMoatCampaignData(userName, password, url);
                var jsonReturnedData = await API.Moat.GetMoatCampaignData(userName, password, url);

                JsonUtility.GetJTokenValueAsString(json: jsonReturnedData, path: "data_available");
                if (jsonReturnedData != null)
                {
                    string summaryJson = JsonUtility.GetJTokenValueAsString(json: jsonReturnedData, path: "results.summary");
                    string detailsJson = JsonUtility.GetJTokenValueAsString(json: jsonReturnedData, path: "results.details");
                    if (summaryJson != "[]")
                    {
                        var rawData = JsonConvert.DeserializeObject<List<MoatDailyStatData>>(detailsJson);
                        dailyMoatDetailList = (from moat in rawData
                                                select moat)
                                                .ToList();
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
           
            return dailyMoatDetailList;
        }
    }
}