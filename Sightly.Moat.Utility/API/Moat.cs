﻿using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Sightly.Tools;

namespace Sightly.Moat.Utility.API
{
    public class Moat
    {
        public static async Task<string> GetMoatCampaignData(string username, string password, string url)
        {
            var credentials = Convert.ToBase64String(Encoding.ASCII.GetBytes($"{username}:{password}"));
            using (var httpClient = new HttpClient())
            {
                try
                {
                    httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", credentials);
                    HttpResponseMessage response = httpClient.GetAsync(url).Result;
                    string responseContent = await response.Content.ReadAsStringAsync();
                    return responseContent;
                }
                catch (Exception ex)
                {
                    throw ExceptionUtility.ThrowException(ex);
                }
            }
        }
    }
}
