﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sightly.Models;

namespace Sightly.Moat.Utility
{
    public interface IMoatDataService
    {
        Task<List<MoatDailyStatData>> GetDailyMoatStats(DateTime statDate);
    }
}
