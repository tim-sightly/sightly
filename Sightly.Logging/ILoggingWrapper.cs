﻿namespace Sightly.Logging
{
    public interface ILoggingWrapper
    {
        ILoggingService Logger { get; }
    }
}