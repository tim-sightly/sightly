﻿using System.Globalization;
using System.Text;
using NLog;
using NLog.Config;
using NLog.LayoutRenderers;

namespace Sightly.Logging.Renderer
{
    [LayoutRenderer("utc_date")]
    public class UtcDateRenderer : LayoutRenderer
    {
        public CultureInfo Culture { get; set; }
        [DefaultParameter]
        public string Format { get; set; }


        public UtcDateRenderer()
        {
            this.Format = "MM/dd/yy H:mm:ss";  //"G"
            this.Culture = CultureInfo.InvariantCulture;
        }

        protected override void Append(StringBuilder builder, LogEventInfo logEvent)
        {
            builder.Append(logEvent.TimeStamp.ToUniversalTime().ToString(this.Format, this.Culture));
        }
    }
}
