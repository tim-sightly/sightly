﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.Logging
{
    public class LoggingWrapper : ILoggingWrapper
    {
        private static ILoggingService _logger; 

       

        public ILoggingService Logger
        {
            get
            {
                if (_logger != null)
                    return _logger;
                else
                {
                    _logger = LoggingService.GetLoggingService();
                    return _logger;

                }
            }
        }
    }
}
