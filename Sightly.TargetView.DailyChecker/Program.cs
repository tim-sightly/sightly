﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Sightly.Adwords.Utility.Interface;
using Sightly.IOC;
using Sightly.Models;
using Sightly.TargetView.AdwordsValidation;
using Sightly.Tools;
using System.Net.Mail;
using System.Text;
using Sightly.TargetView.AdwordsValidation.Interfaces;

namespace Sightly.TargetView.DailyChecker
{
    class Program
    {
        static void Main()
        {
            InitializeDi();

            var mccCustomers = new Dictionary<long, string>{
                //9217729477, Moore and Scarry MCC
               { long.Parse("9217729477"), "Moore and Scarry MCC" },
               // //6622029363, REACH LOCAL MCC
               { long.Parse("6622029363"), "REACH LOCAL MCC" },
               // //1256155692, KMFB STATIONS
               { long.Parse("1256155692"), "KMFB STATIONS" },
               // //3470698112, MINDSTREAM
               { long.Parse("3470698112"), "MINDSTREAM" },
                // 7742181127, NJAM
               { long.Parse("7742181127"), "ADVANCE DIGITAL (NJAM)" },
               { long.Parse("3017157839"), "ADVANCE DIGITAL (LVL)" },
               { long.Parse("8158057652"), "ADVANCE DIGITAL (MassLive)" },
               { long.Parse("9786528386"), "ADVANCE DIGITAL (SMG)" },
               { long.Parse("5570545417"), "ADVANCE DIGITAL (PAMG)" },
               { long.Parse("7929513994"), "ADVANCE DIGITAL (AMG)" },
               { long.Parse("7528459433"), "ADVANCE DIGITAL (OMG)" },
               { long.Parse("9660276423"), "ADVANCE DIGITAL (AO)" },
               { long.Parse("7978460615"), "ADVANCE DIGITAL (MMG)" },
                //5325696208, WSI
               { long.Parse("5325696208"), "WSI" },
               //{ long.Parse("4716165769"), "Horizon Media MCC"},

               //{ long.Parse("8409054131"), "Upwardly Group" }

             };


             //var statDate = DateTime.Parse("2017-08-29"); 
           var statDate = DateTime.Now.AddDays(-1);

            var dailyStats = GetDailyCampaignStats(statDate, mccCustomers);

            //Temp Fix for Horizon Media
           // dailyStats.RemoveAll(ds => ds.AccountName == "Horizon Media MCC");


            //    //Console.ReadLine();
            CsvUtility.CreateCsvFromGenericList(dailyStats, string.Format("DailyCheck_{0}.csv", statDate.ToString("u")));


            //var statDate = DateTime.Parse("2016-11-01");
            //var endDate = DateTime.Parse("2016-11-30");
            //while (statDate <= endDate)
            //{

            //    var dailyStats = GetDailyCampaignStats(statDate, mccCustomers);

            //    //Console.ReadLine();
            //    CsvUtility.CreateCsvFromGenericList(dailyStats, string.Format("DailyCheck_{0}.csv", statDate.ToString("u")));
            //    statDate = statDate.AddDays(1);
            //}

        }


        private static IContainer Container { get; set; }
        private static IAccountHierarchy _accountHierarchy;
        private static ITargetViewAdwordsValidation _targetViewAdwordsValidation;

        public static void InitializeDi()
        {
            var builder = new ContainerBuilder();
            (new AutofacConfiguration()).RegisterServices(builder);
            Container = builder.Build();
            _accountHierarchy = Container.Resolve<IAccountHierarchy>();
            _targetViewAdwordsValidation = Container.Resolve<ITargetViewAdwordsValidation>();
        }

        public static List<AwCampaignStatics> GetDailyCampaignStats(DateTime statDate, Dictionary<long, string> customers)
        {

            #region campStats section
            var campStats = new List<AwCampaignStatics>();


            foreach (var customer in customers)
            {
                Console.WriteLine($"Getting customer: {customer.Value}");
                var customerHierarchy = _accountHierarchy.GetAdwordsCustomerByMcc(customer.Key);
                var campaignStats = _targetViewAdwordsValidation.GetCampaignStatisticsForMccCustomer(statDate,
                                                                                                     customerHierarchy.First().ChildCustomers);
                campaignStats.ToList().ForEach(stat => { stat.AccountName = customer.Value; });
                campStats.AddRange(campaignStats);
            }

            var context = new TargetViewDb.DAL.TargetViewDb();

            campStats.ForEach(tvCamp =>
            {
                var stat = (from o in context.OrderBudgetGroupImpressionsViewsByDates
                            where o.statdate.Value.Month == statDate.Month && o.statdate.Value.Year == statDate.Year && o.statdate.Value.Day == statDate.Day && o.adwordscampaignid == tvCamp.CampaignId
                            select new
                            {
                                AccountName = o.accountName,
                                OrderId = o.orderid,
                                BudgetGroupId = o.BudgetGroupId,
                                OrderStatus = o.OrderStatus,
                                AdImpressions = o.AdsImpressions,
                                AdCompletedViews = o.AdsCompletedViews,
                                o.OrderImpressions,
                                o.OrderCompletedViews,
                                o.BudgetGroupImpressions,
                                o.BudgetGroupCompletedViews
                            }).FirstOrDefault();

                if (stat == null) return;

                tvCamp.AccountName = stat.AccountName;
                tvCamp.TvOrderId = stat.OrderId;
                tvCamp.TvBudgetGroupId = stat.BudgetGroupId.Value;
                tvCamp.TvOrderStatus = stat.OrderStatus;
                tvCamp.TvVideoImpressions = stat.AdImpressions ?? 0L;
                tvCamp.TvVideoViews = stat.AdCompletedViews ?? 0L;
                tvCamp.TvOrderImpressions = stat.OrderImpressions ?? 0L;
                tvCamp.TvOrderViews = stat.OrderCompletedViews ?? 0L;
                tvCamp.TvBudgetGroupImpressions = stat.BudgetGroupImpressions ?? 0L;
                tvCamp.TvBudgetGroupViews = stat.BudgetGroupCompletedViews ?? 0L;
                //Adding formatted Response logic
                if (tvCamp.AccountName != "Horizon Media MCC")
                {
                    tvCamp.VideoImpressionsDiff = tvCamp.Impressions - tvCamp.TvVideoImpressions;
                    tvCamp.VideoViewsDiff = tvCamp.VideoViews - tvCamp.TvVideoViews;
                    tvCamp.OrderImpressionsDiff = tvCamp.TvVideoImpressions - tvCamp.TvOrderImpressions;
                    tvCamp.OrderVideoViewsDiff = tvCamp.TvVideoViews - tvCamp.TvOrderViews;
                    if (tvCamp.Impressions > 0L)
                    {
                        tvCamp.PercentImpressionChange = (Convert.ToDecimal(tvCamp.VideoImpressionsDiff) / Convert.ToDecimal(tvCamp.Impressions)) * 100M;
                    }
                    else
                    {
                        tvCamp.PercentImpressionChange = 0M;
                    }

                    if (tvCamp.VideoViews > 0L)
                    {
                        tvCamp.PercentVideoChange = (Convert.ToDecimal(tvCamp.VideoViewsDiff) / Convert.ToDecimal(tvCamp.VideoViews)) * 100M;
                    }
                    else
                    {
                        tvCamp.PercentVideoChange = 0M;
                    }

                    if (Math.Abs(tvCamp.PercentImpressionChange) >= 10 || Math.Abs(tvCamp.PercentVideoChange) >= 10)
                    {
                        tvCamp.ChangeGreaterThan10Percent = true;
                    }
                    else
                    {
                        tvCamp.ChangeGreaterThan10Percent = false;
                    }
                }

                else
                {
                    tvCamp.BudgetGroupImpressionsDiff = tvCamp.TvBudgetGroupImpressions - tvCamp.Impressions;
                    tvCamp.BudgetGroupVideoViewsDiff = tvCamp.TvBudgetGroupViews - tvCamp.VideoViews;
                    if (tvCamp.BudgetGroupImpressionsDiff > 0L)
                    {
                        tvCamp.PercentImpressionChange = (Convert.ToDecimal(tvCamp.BudgetGroupImpressionsDiff) / Convert.ToDecimal(tvCamp.Impressions)) * 100M;
                    }
                    else
                    {
                        tvCamp.PercentImpressionChange = 0M;
                    }

                    if (tvCamp.BudgetGroupVideoViewsDiff> 0L)
                    {
                        tvCamp.PercentVideoChange = (Convert.ToDecimal(tvCamp.BudgetGroupVideoViewsDiff) / Convert.ToDecimal(tvCamp.VideoViews)) * 100M;
                    }

                }
            });


            var specificAccountsToCheck = new List<Guid>
            {
                Guid.Parse("B4C7397E-3C0C-4350-EAAF-A4FB00B677AF"),
                Guid.Parse("DD51D01F-FE7D-4E01-8F24-A53400FC9E85"),
                Guid.Parse("C252851B-7844-47DD-A20C-A664011EC51A"),
                Guid.Parse("70BE35A4-2E79-4B32-AAB6-A6A401437479"),
                Guid.Parse("07CCCA20-AD4E-4F63-AAF9-A658013085F8"),
                Guid.Parse("2AAE05DD-51B7-47AC-8716-E6E9C1965ABF")
            };

            //var getAllTvOrders = context.Orders.Where(ord => specificAccountsToCheck.Contains(ord.AccountId) && ord.StartDate <= statDate && ord.EndDate >= statDate).ToList();


            //var missingOrders = getAllTvOrders.Where(o => campStats.All(o2 => o2.TvOrderId != o.OrderId));
            //missingOrders.ToList().ForEach(tvOnly => campStats.Add(new AwCampaignStatics
            //{
            //    AccountName = tvOnly.Account.AccountName,
            //    CampaignName = tvOnly.Campaign.CampaignName,
            //    TvOrderId = tvOnly.OrderId,
            //    TvOrderStatus = tvOnly.OrderStatu.OrderStatusName
            //}));

            #endregion campstats section

            campStats.ForEach(cs =>
            {
                cs.OrderIsEmpty = cs.TvOrderId.Equals(Guid.Parse("00000000-0000-0000-0000-000000000000"));
            });

            SendEmail(campStats);

            return campStats;
        }

        private static void SendEmail(List<AwCampaignStatics> ListOfAwCampaign)
        {
            #region send mail test

            //Loop through the list and find data with Diff > 10 %
            int counter = -1;
            List<AwCampaignStatics> LocalList = new List<AwCampaignStatics>();

            foreach (AwCampaignStatics campaign in ListOfAwCampaign)
            {
                counter++;
                if (campaign.ChangeGreaterThan10Percent || campaign.OrderIsEmpty)
                {
                    //save results into a new list
                    if (campaign.AccountName != "Horizon Media MCC")
                    {
                        LocalList.Add(campaign);
                    }
                }

            }

            MailMessage mail = new MailMessage();
            SmtpClient smtpServer = new SmtpClient("smtp.gmail.com");
            mail.From = new MailAddress("dailychecker2017@gmai.com");
            mail.To.Add("grace@sightly.com,dailychecker2017@gmail.com");
            mail.Subject = "Channel Partners Daily Checker Result";
            mail.Body = "The following OrderIDs have more than 10% of difference between Adwords and TargetView Stats or OrderID is empty:  " + Environment.NewLine + Environment.NewLine;

            int countOfOrderIds = 0;

            var sb = new StringBuilder();
            foreach (AwCampaignStatics x in LocalList)
            {
                sb.Append(x.AccountName.ToString() + "  Customer:    " + x.CustomerName.ToString() + "  CampaignID:   " + x.CampaignId.ToString() + " OrderID:   " + x.TvOrderId.ToString() + " Ten Percent Change:   " + x.ChangeGreaterThan10Percent + Environment.NewLine);
                countOfOrderIds++;
            }


            mail.Body = countOfOrderIds == 0 ? "There are no data issues today." : sb.ToString();

            //Attachment attachment;
            //attachment = new Attachment("@C:\xxxx");
            //mail.Attachments.Add(attachment);

            smtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtpServer.UseDefaultCredentials = false;
            smtpServer.Port = 587;
            smtpServer.Credentials = new System.Net.NetworkCredential("dailychecker2017@gmail.com", "CSGteam2017");
            smtpServer.EnableSsl = true;

            smtpServer.Send(mail);


            #endregion send mail test
        }

    }


}
