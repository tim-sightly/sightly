﻿using System;
using System.Collections.Generic;
using Autofac;
using Sightly.Business.MediaAgency;
using Sightly.Business.StatsGather;
using Sightly.Test.Common;

namespace Sightly.MediaAgency.AdSwap
{
    class Program
    {
        private static IContainer Container { get; set; }
        private static IDomainManager _domainManager;

        static void Initialize()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _domainManager = Container.Resolve<IDomainManager>();
        }
       
        static void Main(string[] args)
        {
            Console.WriteLine($"Started: {DateTime.Now}");

            Initialize();



            var cids = new List<long>()
                {
                    3070400739L

                };

            cids.ForEach(cid =>
            {
                Console.WriteLine($"{DateTime.Now} - Updating ads for {cid}");
                _domainManager.UpdateTvMediaAgencyAds(cid);
            });

            Console.WriteLine($"Ended: {DateTime.Now}");
            Console.ReadKey();
        }
    }
}
