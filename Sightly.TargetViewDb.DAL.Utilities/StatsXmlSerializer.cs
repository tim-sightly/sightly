﻿using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace Sightly.TargetViewDb.DAL.Utilities
{
    public class StatsXmlSerializer : IStatsXmlSerializer
    {
        #region Private Classes
        [XmlRoot(ElementName = "Stats")]
        // NOTE: Do not ever reference this class
        //       outside of this utility to avoid
        //       maintainability issues in the future.
        //       This was only set to public for the XmlSerializer
        //       class to have access to it when generating XML.
        public class SerializableCollection<T>
        {
            public T[] Items { get; set; }
        }
        #endregion

        #region Private Methods
        private string SerializeStats<T>(T[] stats)
        {
            var serializableCollection = new SerializableCollection<T>()
            {
                Items = stats
            };

            var xmlserializer = new XmlSerializer(typeof(SerializableCollection<T>));
            var options = new XmlWriterSettings { OmitXmlDeclaration = true, Indent = true };
            var stringWriter = new StringWriter();
            using (var writer = XmlWriter.Create(stringWriter, options))
            {
                xmlserializer.Serialize(writer, serializableCollection);
                return stringWriter.ToString();
            }
        }
        #endregion

        #region Public Methods
        /// <summary>
        /// Method used to initiate the serialization.
        /// </summary>
        /// <typeparam name="T">
        ///     Type of collection.
        /// </typeparam>
        /// <param name="collection">
        ///     Collection to be serialized.
        /// </param>
        /// <returns>
        ///     Serialized result of the collection as string.
        /// </returns>
        public string Execute<T>(T[] collection)
        {
            return SerializeStats(collection);
        }
        #endregion
    }
}