﻿namespace Sightly.TargetViewDb.DAL.Utilities
{
    public interface IStatsXmlSerializer
    {
        string Execute<T>(T[] collection);
    }
}
