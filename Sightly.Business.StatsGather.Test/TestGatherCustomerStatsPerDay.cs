﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.StatsGather.Raw;
using Sightly.Test.Common;

namespace Sightly.Business.StatsGather.Test
{
    [TestClass]
    public class TestGatherCustomerStatsPerDay
    {
        private const long CustomerId = 1285494141L;
        private static readonly DateTime StatDate = new DateTime(2017, 09, 26);
        private static IContainer Container { get; set; }
        private IRawStatsService _rawStatsService;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _rawStatsService = Container.Resolve<IRawStatsService>();
            
        }

        [TestMethod]
        public void TestGettingAndSavingFreeAdDeviceStats()
        {
            var adDevices = _rawStatsService.GetAndSaveFreeAdDeviceStats(CustomerId, StatDate);

            Assert.AreNotEqual(0, adDevices.Count);
        }

        [TestMethod]
        public void TestGettingAndSavingAdDeviceWithPercentagesStats()
        {
            var adDevices = _rawStatsService.GetAndSaveAdDeviceStats(CustomerId, StatDate);

            Assert.AreNotEqual(0, adDevices.Count);
        }

        [TestMethod]
        public void TestGettingAndSavingAgeRangeStats()
        {
            var ageRanges = _rawStatsService.GetAndSaveAgeRangeStats(CustomerId, StatDate);
            Assert.AreNotEqual(0, ageRanges.Count);
        }

        [TestMethod]
        public void TestGettingAndSavingGenderStats()
        {
            var genderStats = _rawStatsService.GetAndSaveGenderStats(CustomerId, StatDate);
            Assert.AreNotEqual(0, genderStats.Count);
        }

        [TestMethod]
        public void GetAndSaveAllStats()
        {
            var singleDayStats = _rawStatsService.GatherCustomerStatsPerDay(CustomerId, StatDate);

            Assert.AreNotEqual(0, singleDayStats.AdDeviceStats);
            Assert.AreNotEqual(0, singleDayStats.AgeRangeStats);
            Assert.AreNotEqual(0, singleDayStats.GenderStats);
        }
    }
}
