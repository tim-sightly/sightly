﻿using System;
using System.Collections.Generic;
using System.Data;
using Sightly.Models;

namespace Sightly.DoubleVerify.Utility
{
    public class DoubleVerifyDataService : IDoubleVerifyDataService
    {
        public DoubleVerifyDataService()
        { }


        public List<DoubleVerifyStats> GetDoubleVerifyStatsFromImport(List<DoubleVerifyImport> importData)
        {
            if(importData.Count < 1)
                throw new DataException("Double Verify Import data is empty");
            List<DoubleVerifyStats> stats = new List<DoubleVerifyStats>();

            importData.ForEach(data =>
            {
                stats.Add(new DoubleVerifyStats
                {
                    PlacementId = data.PlacementId,
                    PlacementName = data.PlacementName,
                    StatDate = data.StatDate,
                    DeviceType = data.DeviceType,
                    MonitoredImpressions = data.MonitoredImpressions,
                    BrandSafetyOnTargetImpressions_1x1 = data.BrandSafeImpression_1x1,
                    FraudSIVTImpressions = data.FraudSivtFreeImpression,
                    FraudSIVTImpressions_1x1 = data.FraudSivtFreeImpression_1x1,
                    InGeoImpressions_1x1 = data.InGeoImpression_1x1,
                    VideoViewableImpressions = data.VideoViewableImpression
                });
            });

            return stats;
        }
    }
}