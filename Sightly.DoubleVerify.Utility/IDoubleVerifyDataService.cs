﻿using System.Collections.Generic;
using Sightly.Models;
namespace Sightly.DoubleVerify.Utility
{
    public interface IDoubleVerifyDataService
    {
        List<DoubleVerifyStats> GetDoubleVerifyStatsFromImport(List<DoubleVerifyImport> importData);
    }
}
