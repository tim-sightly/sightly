﻿using Autofac;
using Microsoft.Azure.WebJobs;
using Sightly.Business.StatsGather;
using Sightly.IOC;
using Sightly.Pipeline.Location;


namespace Sightly.Pipeline.Location
{
    public class Program
    {
        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            (new AutofacConfiguration()).RegisterServices(builder);
            builder.RegisterType<LocationControl>();
            var container = builder.Build();


            var jobHostConfig = new JobHostConfiguration
            {
                JobActivator = new DiJobActivator(container)
            };

            var host = new JobHost(jobHostConfig);
            host.Call(typeof(LocationControl).GetMethod("Execute"));
        }

    }


}