﻿using System;
using System.Linq;
using Microsoft.Azure.WebJobs;
using Sightly.Business.StatsGather;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Pipeline.Location
{
    public class LocationControl
    {
        private readonly IGatherStats _gatherStats;
        private readonly IStatRepository _statsRepository;

        public LocationControl(IGatherStats gatherStats, IStatRepository statsRepository)
        {
            _gatherStats = gatherStats;
            _statsRepository = statsRepository;
        }
        [NoAutomaticTrigger]
        public void Execute()
        {

            Console.WriteLine($"Started: {DateTime.Now}");

            DailyStatsGather();
            //GatherAdGroupGeoStatsForTimeRange();

            Console.WriteLine($"Completed: {DateTime.Now}");
        }

     
        private void DailyStatsGather()
        {
            var yesterday = Get3DaysAgoDate();

            GetAllAgGroupAndLocationStatsFromTvByDate(yesterday);
        }

        private void GatherAdGroupGeoStatsForTimeRange()
        {
            var startDate = new DateTime(2017, 10, 24);
            var endDate = new DateTime(2017, 10, 30);
            var customerId = 9519864476;


            for (var countDate = startDate; countDate.Date <= endDate.Date; countDate = countDate.AddDays(1))
            {

                //_statRepository.DeleteRawStatsData(customerId, countDate);
                Console.WriteLine($"{DateTime.Now} -  {countDate}");
                _gatherStats.RunAdGroupGeoStats(customerId, countDate);

            }
        }

        private void GetAllAgGroupAndLocationStatsFromTvByDate(DateTime statdate)
        {
            //Get All CustomerIds that were running yesterday from TargetVeiw
            var customers = _statsRepository.GetAllRunningAdWordsCidsFromTvByDate(statdate);

            //Remove all Mcc ID's from list
            var mccList = _statsRepository.GetMccCustomerList();

            customers.RemoveAll(s => mccList.Contains(s));

            customers.GroupBy(c => c).ToList().ForEach(customer =>
            {
                Console.WriteLine($"{DateTime.Now} - CustomerId: {customer.Key} for {statdate}");
                try
                {
                    _statsRepository.DeleteRawAdGroupAndLocationStats(customer.Key, statdate);
                    _gatherStats.RunAdGroupGeoStats(customer.Key, statdate);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"---------Issue Gathering {customer.Key} for {statdate}--------------");
                    Console.WriteLine(e);
                    _statsRepository.InsertMissingCidAndDate(customer.Key, statdate);
                }
            });
            
        }

        private static DateTime GetPreviousDate()
        {
            var currentDate = DateTime.UtcNow.AddDays(-1);
            //var currentDate = new DateTime(2017, 11, 19);   
            return new DateTime(currentDate.Ticks, DateTimeKind.Utc);
        }

        private static DateTime Get3DaysAgoDate()
        {

            var currentDate = DateTime.UtcNow.AddDays(-3);
            //var currentDate = new DateTime(2017, 11, 11);   
            return new DateTime(currentDate.Ticks, DateTimeKind.Utc);
        }
    }
}
