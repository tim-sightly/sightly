﻿using Autofac;
using Microsoft.Azure.WebJobs.Host;

namespace Sightly.Pipeline.Location
{
    public class DiJobActivator : IJobActivator
    {
        private readonly IContainer _container;

        public DiJobActivator(IContainer container)
        {
            _container = container;
        }


        public T CreateInstance<T>()
        {
            return _container.Resolve<T>();
        }
    }
}
