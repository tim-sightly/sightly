﻿using System;
using Sightly.Models.tv;
using Sightly.ReportGeneration.Internal.Generators;
using Sightly.ReportGeneration.Internal.Generators.Interface;

namespace Sightly.ReportGeneration.Generators.Internal
{
    public class ChangeOrderDetailExcel : AExcelReport, IExcelReport
    {
        #region constants

        private readonly string INT_FORMAT = "#,##0";
        private readonly string PERCENTAGE_FORMAT = "#0.00%";
        private readonly string MONEY_FORMAT = "$0.00";

        #endregion

        #region variables

        private OrderChanges _data;
        private DateTime _now;
        private int _rowOffset;
        private readonly string _dateFormat = "MM-dd-yyyy";


        #endregion

        public ChangeOrderDetailExcel(OrderChanges orderChangesData) 
            : base()
         //   : base("Sightly.ReportGeneration.Templates.Excel.changeOrderDetails.xlsx")
        {
            _data = orderChangesData;
            _now = DateTime.Now;
            _rowOffset = 8;

            Build();
        }

        private void Build()
        {
            BuildHeader();
            if (_data.InfoChanges.HasChanges)
            {
                BuildInfoChanges();
            }

            if (_data.HasAdChanges)
            {
                BuildCreativeChanges();
            }

            if (_data.AudienceChanges.HasChanges)
            {
                BuildAudienceChanges();
            }

            if (_data.GeoChanges.HasChanges)
            {
                BuildGeoChanges();
            }
        }

        private void BuildHeader()
        {
            WorkSheet.Cells[1, 1].Style.Font.Bold = true;
            WorkSheet.Cells[1, 1].Value = "Changes";

            WorkSheet.Cells[2, 1].Value = "Submitted By:";
            WorkSheet.Cells[2, 2].Value = _data.SubmittedBy;
            WorkSheet.Cells[3, 1].Value = "Submitted Date:";
            WorkSheet.Cells[3, 2].Value = _data.SubmittedDate.ToString(_dateFormat);
            WorkSheet.Cells[4, 1].Value = "Account:";
            WorkSheet.Cells[4, 2].Value = _data.AccountName;
            WorkSheet.Cells[5, 1].Value = "Advertiser:";
            WorkSheet.Cells[5, 2].Value = _data.AdvertiserName;
            WorkSheet.Cells[6, 1].Value = "Order Name:";
            WorkSheet.Cells[6, 2].Value = _data.OrderName;
            WorkSheet.Cells[7, 1].Value = "Change Date:";
            WorkSheet.Cells[7, 2].Value = _data.SubmittedDate.ToString(_dateFormat);

        }

        private void BuildInfoChanges()
        {
            var currentRow = _rowOffset + 1;
            WorkSheet.Cells[currentRow, 1].Style.Font.Bold = true;
            WorkSheet.Cells[currentRow, 1].Value = "INFO";
            currentRow++;
            if (!string.IsNullOrEmpty(_data.InfoChanges.OrderRefCodeChange))
            {
                WorkSheet.Cells[currentRow, 1].Value = _data.InfoChanges.OrderRefCodeChange;
                currentRow++;
            }

            if (!string.IsNullOrEmpty(_data.InfoChanges.TotalBudgetChange))
            {
                WorkSheet.Cells[currentRow, 1].Value = _data.InfoChanges.TotalBudgetChange;
                currentRow++;
            }

            if (!string.IsNullOrEmpty(_data.InfoChanges.EndDateChange))
            {
                WorkSheet.Cells[currentRow, 1].Value = _data.InfoChanges.EndDateChange;
                currentRow++;
            }

            _rowOffset = currentRow;
        }

        private void BuildCreativeChanges()
        {
            var currentRow = _rowOffset + 1;
            WorkSheet.Cells[currentRow, 1].Style.Font.Bold = true;
            WorkSheet.Cells[currentRow, 1].Value = "CREATIVE";
            currentRow++;
            _data.AdChanges.ForEach(ad =>
            {
                if (!string.IsNullOrEmpty(ad.AdPausedChange))
                {
                    WorkSheet.Cells[currentRow, 1].Value = ad.AdPausedChange;
                    currentRow++;
                }

                if (!string.IsNullOrEmpty(ad.DisplayUrlChange))
                {
                    WorkSheet.Cells[currentRow, 1].Value = ad.DisplayUrlChange;
                    currentRow++;
                }

                if (!string.IsNullOrEmpty(ad.DestinationUrlChange))
                {
                    WorkSheet.Cells[currentRow, 1].Value = ad.DestinationUrlChange;
                    currentRow++;
                }

                if (!string.IsNullOrEmpty(ad.CompanionBannerChange))
                {
                    WorkSheet.Cells[currentRow, 1].Value = ad.CompanionBannerChange;
                    currentRow++;
                }

                if (!string.IsNullOrEmpty(ad.NewAdChange))
                {
                    WorkSheet.Cells[currentRow, 1].Value = ad.NewAdChange;
                    currentRow++;
                }
            });

            _rowOffset = currentRow;
        }

        private void BuildAudienceChanges()
        {
            var currentRow = _rowOffset + 1;
            WorkSheet.Cells[currentRow, 1].Style.Font.Bold = true;
            WorkSheet.Cells[currentRow, 1].Value = "AUDIENCE";
            currentRow++;

            if (!string.IsNullOrEmpty(_data.AudienceChanges.AgeRangeChanges))
            {
                WorkSheet.Cells[currentRow, 1].Value = _data.AudienceChanges.AgeRangeChanges;
                WorkSheet.Cells[currentRow, 2].Value = _data.AudienceChanges.SpecificAgeRanges;
                currentRow++;
            }

            if (!string.IsNullOrEmpty(_data.AudienceChanges.GenderChanges))
            {
                WorkSheet.Cells[currentRow, 1].Value = _data.AudienceChanges.GenderChanges;
                WorkSheet.Cells[currentRow, 2].Value = _data.AudienceChanges.SpecificGenders;
                currentRow++;
            }
            if (!string.IsNullOrEmpty(_data.AudienceChanges.ParentalStatusChanges))
            {
                WorkSheet.Cells[currentRow, 1].Value = _data.AudienceChanges.ParentalStatusChanges;
                WorkSheet.Cells[currentRow, 2].Value = _data.AudienceChanges.SpecificParentalStatus;
                currentRow++;
            }
            if (!string.IsNullOrEmpty(_data.AudienceChanges.HouseholdIncomeChanges))
            {
                WorkSheet.Cells[currentRow, 1].Value = _data.AudienceChanges.HouseholdIncomeChanges;
                WorkSheet.Cells[currentRow, 2].Value = _data.AudienceChanges.SpecificHouseholdIncomes;
                currentRow++;
            }
            if (!string.IsNullOrEmpty(_data.AudienceChanges.KeywordChanges))
            {
                WorkSheet.Cells[currentRow, 1].Value = "Keywords";
                WorkSheet.Cells[currentRow, 2].Value = _data.AudienceChanges.KeywordChanges;
                currentRow++;
            }
            if (!string.IsNullOrEmpty(_data.AudienceChanges.CompetitorUrlChanges))
            {
                WorkSheet.Cells[currentRow, 1].Value = "Customer Interest URLs";
                WorkSheet.Cells[currentRow, 2].Value = _data.AudienceChanges.CompetitorUrlChanges;
                currentRow++;
            }
            if (!string.IsNullOrEmpty(_data.AudienceChanges.NoteChanges))
            {
                WorkSheet.Cells[currentRow, 1].Value = "Notes";
                WorkSheet.Cells[currentRow, 2].Value = _data.AudienceChanges.NoteChanges;
                currentRow++;
            }

            _rowOffset = currentRow;
        }

        private void BuildGeoChanges()
        {
            var currentRow = _rowOffset + 1;
            WorkSheet.Cells[currentRow, 1].Style.Font.Bold = true;
            WorkSheet.Cells[currentRow, 1].Value = "GEO";
            currentRow++;

            WorkSheet.Cells[currentRow, 1].Value = _data.GeoChanges.ChangedGeos;
            currentRow++;


            _rowOffset = currentRow;
        }
    }
}