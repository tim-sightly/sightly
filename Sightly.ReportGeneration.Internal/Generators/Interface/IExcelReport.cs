﻿using System.IO;

namespace Sightly.ReportGeneration.Internal.Generators.Interface
{
    public interface IExcelReport
    {
        MemoryStream ToStream();
    }
}