﻿using System;
using System.Collections.Generic;
using Sightly.Models;
using Sightly.Models.tv;
using Sightly.Models.tv.extended;
using Sightly.Models.tv.extended.audiencegroups;

namespace Sightly.TargetViewDb.DAL.Repositories.Interfaces
{
    public interface ITvDbRepository
    {
        #region Getters
        AdMetaData GetAdMetaData(long customerId, long campaignId);
        TvAccount GetAccountDataByName(string accountName);
        TvAccount GetAccountDataByCampaignName(string campaignName);
        TvAccount GetAccountDataByOrderName(string orderName);
        TvAccount GetAccountDateByCampaignId(Guid campaignId);
        TvAd GetAdByVideoAssetName(string videoAssetName);
        TvAdAdwordsInfo GetAdAdwordsInfo(long awAdId);
        TvAdvertiser GetAdvertiserDataByName(string advertiserName);
        TvAdvertiser GetAdvertiserDataByCampaignName(string campaignName);
        TvAdvertiser GetAdvertiserDataByOrderName(string orderName);
        TvBudgetGroupAdwordsInfo GetBudgetGroupAdwordsInfo(long campaignId);
        TvBudgetGroup GetBudgetGrouByAWCampaignId(long awCampaignId);
        List<TvBudgetGroup> GetBudgetGroupByAwCustomerId(long customerId);
        int SaveNewOrderBlob(string orderName, string orderRefCode, string orderJson, DateTime startDate, DateTime endDate, string accountName, string advertiserName, Guid advertiser, string campaignName, Guid campaign, decimal totalBudget);
        TvAdvertiser GetAdvertiserDataByCampaignId(Guid campaignId);
        TvBudgetGroupAdwordsInfo GetBudgetGroupAdwordsInfo(Guid budgetGroupId, string campaignName, long campaignId);
        TvBudgetGroup GetBudgetGroupByNameAndBudget(string budgetGroupName, decimal budgetGroupBudget, Guid orderId);
        TvBudgetGroupTimedBudget GetBudgetGroupTimedBudget(Guid budgetGroupId, DateTime startDate, Guid budgetGroupXrefPlacementId);
        TvBudgetGroupXrefAd GetBudgetXrefAd(Guid budgetGroupId, Guid adId);
        TvBudgetGroupXrefLocation GetBudgetGroupXrefLocation(Guid budgetGroupId, Guid locationId);
        TvBudgetGroupXrefPlacement GetBudgetGroupXrefPlacement(Guid budgetGroupId, Guid placementId);
        TvBudgetGroupAndPlacement GetBudgetGroupXrefPlacementByAWCampaignId(long awCampaignId);
        TvCampaign GetCampaignDataByName(string campaignName);
        TvCampaign GetCampaignByCampaignId(Guid campaignId);
        TvCampaign GetCampaignByOrderName(string orderName);
        TvCampaignAdWordsInfo GetCampaignAdwordsInfoDataByCustomerId(long customerId);
        string GetTvCampaignNameByCustomerId(long customerId);
        bool IsCustomerLocked(long customerId);
        TvLocation GetlocationByAccountAndName(Guid accountId, string locationName);
        TvOrder GetOrderDataByName(string orderName);
        TvOrderXrefBudgetGroup GetOrderXrefBudgetGroup(Guid orderId, Guid budgetGroupId);
        TvPlacement GetPlacementByNameAndValue(string placementName, string placementValue);
        TvPlacement GetPlacementByName(string placementName);
        VideoAssetUpdateData GetVideoAssetDataByVideoAssetId(Guid videoAssetId);
        TvVideoAsset GetVideoAssetDataByVideoAssetName(string videoAssetName);
        TvVideoAssetVersion GetVideoAssetVersionDataByYouTubeId(string youTubeId);
        List<Guid> GetVideoAssetVersionsWithNoYoutubeId();


        Guid CreateLocationIfNotExisting(Guid orderId);
        void AddGeoIfNotExisting(Guid orderId, object locId, Guid geoId);
        string GetNewOrderBlobById(int id);
        int SaveNewOrderBlob(string orderName, string orderRefCode, string orderJson, DateTime startDate, DateTime endDate);
        List<Order> SearchNewOrderBlob(string orderName, string orderRefCode, DateTime? startDate, DateTime? endDate);
        Info GetOrderInfoByOrderId(Guid orderId);
        InfoExtended GetOrderInfoExtendedByOrderId(Guid orderId);
        List<Ad> GetOrderAds(Guid orderId);
        List<GeoData> GetOrderGeoSelectedServiceAreas(Guid orderId);
        List<Guid> GetOrderAgeRanges(Guid orderId);
        string GetOrdersCompetitorsUrls(Guid orderId);
        List<Guid> GetOrdersGenders(Guid orderId);
        List<short> GetOrdersHouseholdIncome(Guid orderId);
        string GetOrdersKeywords(Guid orderId);
        string GetOrderNotes(Guid orderId);
        List<Guid> GetOrderParentalStatus(Guid orderId);
        List<OrderLite> SearchOrderLiteByString(string searchValue, Guid userId);
        AccountMetaData GetAdvertiserMetaData(Guid campaignId);
        OrderPauseData GetOrderPauseDataByOrderId(Guid orderId);
        OrderCancelData GetOrderCancelDataByOrderId(Guid orderId);
        Guid? FindLiveBudgetGroupXrefPlacement(Guid budgetGroupXrefPlacementId);
        List<OrderTargetAgeGroup> GetOrderAgeRangesExtended(Guid orderId);
        List<OrderTargetGenderGroup> GetOrdersGendersExtended(Guid orderId);
        List<OrderHouseholdIncomeGroup> GetOrdersHouseholdIncomeExtended(Guid orderId);
        List<OrderParentalStatusGroup> GetOrderParentalStatusExtended(Guid orderId);
        #endregion


        #region Setters

        void AddAgeGroupToOrderTargetAgeGroups(Guid orderId, Guid age);
        void AddGenderToOrderTargetGender(Guid orderId, Guid gender);
        void AddParentalStatusToOrderTargetParentalStatus(Guid orderId, Guid par);
        void AddUrlsToOrderCompetitorsUrl(Guid orderId, string competitorsUrls);
        void AddIncomeToOrderTargetIncome(Guid orderId, short hi);
        void AddNoteToOrderTargetNote(Guid orderId, string note);
        void AddKeyWordsToOrderTargetKeyworks(Guid orderId, string keyWords);
        void CreateProposedOrderAd(Guid orderId, int selectedAdFormat, string adNameInput, string youtubeUrlInput, string clickableUrlInput, string landingUrlInput, string campanionBannerUrl, bool pause);
        void InsertAccount(TvAccount account);
        void InsertAd(TvAd ad);
        void InsertAdAdwordsInfo(TvAdAdwordsInfo tvAdAdwordsInfo);
        void InsertAdvertiser(TvAdvertiser advertiser);
        void InsertBudgetGroup(TvBudgetGroup budgetGroup);
        void InsertBudgetGroupAdwordsInfo(TvBudgetGroupAdwordsInfo budgetGroupAdwordsInfo);
        void InsertBudgetGroupTimedBudget(TvBudgetGroupTimedBudget budgetGroupTimedBudget);
        void InsertBudgetGroupXrefAd(TvBudgetGroupXrefAd budgetGroupXrefAd);
        void InsertBudgetGroupXrefLocation(TvBudgetGroupXrefLocation budgetGroupXrefLocation);
        void InsertBudgetGroupXrefPlacement(TvBudgetGroupXrefPlacement budgetGroupXrefPlacement);
        void InsertCampaign(TvCampaign campaign);
        void InsertCampaignAdwordsInfo(TvCampaignAdWordsInfo campaignAdWordsInfo);
        void InsertLocation(TvLocation location);
        void InsertOrder(TvOrder order);
        void InsertOrderWithParent(TvOrder order);
        void InsertTargetAgeGroup(TvOrderTargetAgeGroup orderTargetAgeGroup);
        void InsertTargetGender(TvOrderTargetGender orderTargetGender);

        void InsertOrderXrefBudgetGroup(TvOrderXrefBudgetGroup orderXrefBudgetGroup);

        void InsertPlacement(TvPlacement placement);
        void InsertVideoAsset(TvVideoAsset videoAsset);

        void InsertVideoAssetVersion(TvVideoAssetVersion videoAssetVersion);

        void PrepareBudgetGroupTimedBudgetsIfReorder(long awcustomerId);
        void SaveVideoAssetIdToVideoAsset(Guid videoAssetId, string uploadResponse);


        void LockCustomer(long customerId, string email, string importType);
        void UnlockCustomer(long customerId, string email);

        void SaveOrderVersionForHistory(Order order, Guid userId);
        void AssociateNewOrderIdToParentWithBudgetGroup(Guid orderId, Guid parentOrderId);
        #endregion

        #region Updater
        TvBudgetGroupTimedBudget UpdateBudgetGroupTimedBudget(Guid budgetGroupTimedBudgetId, decimal campaignBudget, DateTime startDate, DateTime endDate, decimal margin, Guid budgetGroupXrefPlacementId);
        void UpdateHistoricalBudgetGroupTimedBudget(Guid budgetGroupTimedBudgetId, decimal campaignBudget, DateTime startDate, DateTime endDate, decimal margin, Guid budgetGroupXrefPlacementId);
        void UpdateProposedBudgetGroupTimedBudget(Guid budgetGroupTimedBudgetId, decimal campaignBudget, DateTime startDate, DateTime endDate, decimal margin, Guid budgetGroupXrefPlacementId);
        void UpdateBudgetGroupStructureFromProposeToLive(Guid budgetGroupId);
        void UpdateOrderInfoData(Guid orderId, DateTime startDate, DateTime endDate, decimal totalBudget);

        bool SetOrderPausedStatus(Guid orderId);
        bool SetOrderCancelled(Guid orderId);
        void EnsureProperCampaignMangerAssignedToCustomer(long customerId, string campaignMangerEmail);
        #endregion

        #region Deleters

        void RemoveLocationDetails(Guid orderId, Guid locId);

        void DeleteOrderTargetAgeGroup(Guid orderId);
        void DeleteOrderTargetGender(Guid orderId);
        void DeleteOrderTargetParentalStatus(Guid orderId);
        void DeleteOrderTargetCompetitorsUrls(Guid orderId);
        void DeleteOrderTargetHouseholdIncome(Guid orderId);
        void DeleteOrderTargetingNotes(Guid orderId);
        void DeleteOrderTargetingKeyWords(Guid orderId);

        #endregion

    }
}