﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.TargetViewDb.DAL.Repositories.Interfaces
{
    public interface IStatRepository
    {
        List<AccountCustomerData> GetChannelPartnerMccCustomerList();
        List<BudgetGroupCampaignWithAdData> GetChannelPartnerCampaignList();
        List<long> GetCustomerIdsFromNielsenData(DateTime yesterday);
        List<long> GetMediaAgencyMccCustomerList();
        void SetAdwordsCustomerId(List<AdvertiserCustomerData> advertiserCustomerData);
        void SetAdwordsCampaignId(List<BudgetGroupCampaignData> budgetGroupCampaignData);
        void SetAdwordsAdId(List<AdAwAdData> adAwAdData);
        List<long> GetAllRunningAdWordsCidsFromTvByDate(DateTime statDate); 
        List<AdwordsTvAssociationData> GetAllRunningAdWordsOrdersFromTvByDate(DateTime statDate); 
        List<AdwordsTvAssociationData> GetAdwordsTvAssociationData(long campaignId);
        List<long> GetDailyStatsRegatherByDate(DateTime statDate);
        List<long> GetMccCustomerList();
        List<MissingCidStatDates> GetMissingCidAndStatDates();
        List<MissingCidStatDates> GetMissingCidStatsByDate(DateTime statDate);
        List<long> GetNielsenCampaignsByCidAndDate(long customerId, DateTime date);
        List<long> GetNielsenCampaignsByDate(DateTime statDate);
        CustomerStartEndData GetStartAndEndDateForCustomerById(long customerId);
        List<CustomerCampaignRawStatsByDayData> GetValidationForDailyRawDataByCustomerIdAndDate(long customerId, DateTime date);

        int? GetAdVideoLengthByAdId(Guid adId);

        void InsertBudgetGroupStats(List<BudgetGroupStats> budgetGroupStats);
        void InsertBudgetGroupStatsOneAtATime(List<BudgetGroupStats> budgetGroupStats);
        void InsertBudgetGroupAdStats(List<BudgetGroupAdStats> budgetGroupAdStats);
        void InsertBudgetGroupAgeStats(List<BudgetGroupAgeStats> budgetGroupAgeStats);
        void InsertBudgetGroupDeviceStats(List<BudgetGroupDeviceStats> budgetGroupDeviceStats);
        void InsertBudgetGroupGenderStats(List<BudgetGroupGenderStats> budgetGroupGenderStats);
        void InsertAdStats(List<AdStats> adStats);
        void InsertAgeGroupStats(List<AgeGroupStats> ageGroupStats);
        void InsertDailyStatsValidation(CustomerRawStatsByDayDate reportedStats);
        void InsertDeviceStats(List<DeviceStats> deviceStats);
        void InsertGenderStats(List<GenderStats> newGenderStats);
        void InsertMissingCidAndDate(long customer, DateTime statDate);
        void InsertOrderStats(List<OrderStats> orderStats);
        void DeleteMissingCidAndDate(long customerId, DateTime statDate);
        void DeleteRawStatsData(long customer, DateTime statDate);
        void DeleteRawAdGroupAndLocationStats(long customer, DateTime statdate);
    }
}