﻿using System;
using Sightly.Models;

namespace Sightly.TargetViewDb.DAL.Repositories.Interfaces
{
    public interface IRawStatsRepository
    {
        void InsertRawAdDeviceStat(AdDevicePerformanceFreeData adDeviceData);

        void InsertRawAdDeviceStat(AdDevicePerformanceData adDeviceData);

        void InsertRawAdGroupStat(AdGroupPerformanceFreeData adGroupData);

        void InsertRawAgeRangeStat(AgeRangePerformanceFreeData ageRangeData);

        void InsertRawGenderStat(GenderPerformanceFreeData genderData);

        void InsertRawGeoStat(GeoPerformanceFreeData geoData);

        void InsertRawMoatStats(MoatDailyStatData moatData);

        void InsertNielsenCampaignData(NielsenCampaignReference campaignData);

        void InsertNielsenCampaignPlacementData(NielsenCampaignSiteReference campaignPlacementData);

        void InsertNielsenStatsExposure(NielsenCampaignPlacementExposure statsData);

        void InsertDoubleVerifyStats(DoubleVerifyStats statsData);
        NielsenDailyDarStats GetDailyNielsenDarStat(long cid, DateTime yesterday);
        void InsertDailyNielsenDarStat(NielsenDailyDarStats dailyNielsenDarStat);
        void CreateOrUpdateNielsenCampaignAndReference(NielsenCampaignReference camp);
    }
}
