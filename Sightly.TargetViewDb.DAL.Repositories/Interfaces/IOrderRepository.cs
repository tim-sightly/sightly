﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.TargetViewDb.DAL.Repositories.Interfaces
{
    public interface IOrderRepository
    {
        void CreateOrderRecordLog(Guid orderRecordLogId, Guid orderId, string lastModifiedBy);

        void AssignPlacementWithAwCampaignId(long awCampaignId, string placement, string lastModifiedBy);
    }
}
