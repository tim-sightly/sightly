﻿using Sightly.Models;

namespace Sightly.TargetViewDb.DAL.Repositories.Interfaces
{
    public interface IAdRepository
    {
        void CreateAdAdWordsInfo(AdAdWordsInfoData adAdwordsInfo);
    }
}
