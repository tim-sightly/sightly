﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Sightly.Models;
using Sightly.Models.tv;
using Sightly.Models.tv.extended;
using Sightly.Models.tv.extended.audiencegroups;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.TargetViewDb.DAL.Repositories
{
    public class TvDbRepository : ITvDbRepository
    {
        private readonly ITvDbDataStore _tvDbDataStore;

        public TvDbRepository(ITvDbDataStore tvDbDataStore)
        {
            _tvDbDataStore = tvDbDataStore;
        }


        #region Getters

        public AdMetaData GetAdMetaData(long customerId, long campaignId)
        {
            return _tvDbDataStore.GetAdMetaData(customerId, campaignId);
        }

        public TvAccount GetAccountDataByName(string accountName)
        {
            return _tvDbDataStore.GetAccountDataByName(accountName);
        }

        public TvAccount GetAccountDataByCampaignName(string campaignName)
        {
            return _tvDbDataStore.GetAccountDataByCampaignName(campaignName);
        }

        public TvAccount GetAccountDataByOrderName(string orderName)
        {
            return _tvDbDataStore.GetAccountDataByOrderName(orderName);
        }

        public TvAccount GetAccountDateByCampaignId(Guid campaignId)
        {
            return _tvDbDataStore.GetAccountDataByCampaignId(campaignId);
        }

        public TvAd GetAdByVideoAssetName(string videoAssetName)
        {
            return _tvDbDataStore.GetAdByVideoAssetName(videoAssetName);
        }

        public TvAdAdwordsInfo GetAdAdwordsInfo(long awAdId)
        {
            return _tvDbDataStore.GetAdAdwordsInfo(awAdId);
        }

        public TvAdvertiser GetAdvertiserDataByName(string advertiserName)
        {
            return _tvDbDataStore.GetAdvertiserDataByName(advertiserName);
        }

        public TvAdvertiser GetAdvertiserDataByCampaignName(string campaignName)
        {
            return _tvDbDataStore.GetAdvertiserDataByCampaignName(campaignName);
        }

        public TvAdvertiser GetAdvertiserDataByOrderName(string orderName)
        {
            return _tvDbDataStore.GetAdvertiserDataByOrderName(orderName);
        }

        public TvBudgetGroupAdwordsInfo GetBudgetGroupAdwordsInfo(long campaignId)
        {
            return _tvDbDataStore.GetBudgetGroupAdwordsInfo(campaignId);
        }

        public TvBudgetGroup GetBudgetGrouByAWCampaignId(long awCampaignId)
        {
            return _tvDbDataStore.GetBudgetGroupByAWCampaignId(awCampaignId);
        }

        public List<TvBudgetGroup> GetBudgetGroupByAwCustomerId(long customerId)
        {
            return _tvDbDataStore.GetBudgetGroupByAwCustomerId(customerId);
        }

        public int SaveNewOrderBlob(string orderName, string orderRefCode, string orderJson, DateTime startDate, DateTime endDate,
            string accountName, string advertiserName, Guid advertiser, string campaignName, Guid campaign,
            decimal totalBudget)
        {
            return _tvDbDataStore.SaveNewOrderBlob(orderName, orderRefCode, orderJson, startDate, endDate, accountName,
                advertiserName, advertiser, campaignName, campaign, totalBudget);
        }

        public TvAdvertiser GetAdvertiserDataByCampaignId(Guid campaignId)
        {
            return _tvDbDataStore.GetAdvertiserDataByCampaignId(campaignId);
        }

        public TvBudgetGroupAdwordsInfo GetBudgetGroupAdwordsInfo(Guid budgetGroupId, string campaignName, long campaignId)
        {
            return _tvDbDataStore.GetBudgetGroupAdwordsInfo(campaignId);
        }

        public TvBudgetGroup GetBudgetGroupByNameAndBudget(string budgetGroupName, decimal budgetGroupBudget, Guid orderId)
        {
            return _tvDbDataStore.GetBudgetGroupByNameAndBudget(budgetGroupName, budgetGroupBudget, orderId);
        }

        public TvBudgetGroupTimedBudget GetBudgetGroupTimedBudget(Guid budgetGroupId, DateTime startDate,
            Guid budgetGroupXrefPlacementId)
        {
            return _tvDbDataStore.GetBudgetGroupTimedBudget(budgetGroupId, startDate, budgetGroupXrefPlacementId);
        }

        public TvBudgetGroupXrefAd GetBudgetXrefAd(Guid budgetGroupId, Guid adId)
        {
            return _tvDbDataStore.GetBudgetGroupXrefAd(budgetGroupId, adId);
        }

        public TvBudgetGroupXrefLocation GetBudgetGroupXrefLocation(Guid budgetGroupId, Guid locationId)
        {
            return _tvDbDataStore.GetBudgetGroupXrefLocation(budgetGroupId, locationId);
        }

        public TvBudgetGroupXrefPlacement GetBudgetGroupXrefPlacement(Guid budgetGroupId, Guid placementId)
        {
            return _tvDbDataStore.GetBudgetGroupXrefPlacement(budgetGroupId, placementId);
        }

        TvBudgetGroupAndPlacement ITvDbRepository.GetBudgetGroupXrefPlacementByAWCampaignId(long awCampaignId)
        {
            return _tvDbDataStore.GetBudgetGroupXrefPlacementIdByAwCampaignId(awCampaignId);
        }

        public TvCampaign GetCampaignDataByName(string campaignName)
        {
            return _tvDbDataStore.GetCampaignDataByName(campaignName);
        }

        public TvCampaign GetCampaignByCampaignId(Guid campaignId)
        {
            return _tvDbDataStore.GetCampaignByCampaignId(campaignId);
        }

        public TvCampaign GetCampaignByOrderName(string orderName)
        {
            return _tvDbDataStore.GetCampaignByOrderName(orderName);
        }

        public TvCampaignAdWordsInfo GetCampaignAdwordsInfoDataByCustomerId(long customerId)
        {
            return _tvDbDataStore.GetCampaignAdwordsInfoDataByCustomerId(customerId);
        }

        public string GetTvCampaignNameByCustomerId(long customerId)
        {
            return _tvDbDataStore.GetTvCampaignNameByCustomerId(customerId);
        }

        public bool IsCustomerLocked(long customerId)
        {
            return _tvDbDataStore.IsCustomerLocked(customerId);
        }

        public TvLocation GetlocationByAccountAndName(Guid accountId, string locationName)
        {
            return _tvDbDataStore.GetLocationByAccountAndName(accountId, locationName);
        }

        public TvOrder GetOrderDataByName(string orderName)
        {
            return _tvDbDataStore.GetOrderDataByName(orderName);
        }

        public TvOrderXrefBudgetGroup GetOrderXrefBudgetGroup(Guid orderId, Guid budgetGroupId)
        {
            return _tvDbDataStore.GetOrderXrefBudgetGroup(orderId, budgetGroupId);
        }


        public TvPlacement GetPlacementByNameAndValue(string placementName, string placementValue)
        {
            return _tvDbDataStore.GetPlacementByNameAndValue(placementName, placementValue);
        }
		
        public TvPlacement GetPlacementByName(string placementName)
        {
            return _tvDbDataStore.GetPlacementByName(placementName);
        }

        public VideoAssetUpdateData GetVideoAssetDataByVideoAssetId(Guid videoAssetId)
        {
            return _tvDbDataStore.GetVideoAssetDataByVideoAssetId(videoAssetId);
        }

        public TvVideoAsset GetVideoAssetDataByVideoAssetName(string videoAssetName)
        {
            return _tvDbDataStore.GetVideoAssetDataByVideoAssetName(videoAssetName);
        }

        public TvVideoAssetVersion GetVideoAssetVersionDataByYouTubeId(string youTubeId)
        {
            return _tvDbDataStore.GetVideoAssetVersionDataByYouTubeId(youTubeId);
        }

        public List<Guid> GetVideoAssetVersionsWithNoYoutubeId()
        {
            return _tvDbDataStore.GetVideoAssetVersionsWithNoYoutubeId();
        }

        public Guid CreateLocationIfNotExisting(Guid orderId)
        {
            return _tvDbDataStore.CreateLocationIfNotExisting(orderId);
        }

        public void AddGeoIfNotExisting(Guid orderId, object locId, Guid geoId)
        {
            _tvDbDataStore.AddGeoIfNotExisting(orderId, locId, geoId);
        }

        public string GetNewOrderBlobById(int id)
        {
            return _tvDbDataStore.GetNewOrderBlobById(id);
        }

        public int SaveNewOrderBlob(string orderName, string orderRefCode, string orderJson, DateTime startDate, DateTime endDate)
        {
            return _tvDbDataStore.SaveNewOrderBlob(orderName, orderRefCode, orderJson, startDate, endDate);
        }

        public List<Order> SearchNewOrderBlob(string orderName, string orderRefCode, DateTime? startDate, DateTime? endDate)
        {
            return _tvDbDataStore.SearchNewOrderBlob(orderName, orderRefCode, startDate, endDate);
        }

        public Info GetOrderInfoByOrderId(Guid orderId)
        {
            return _tvDbDataStore.GetOrderInfoByOrderId(orderId);
        }

        public InfoExtended GetOrderInfoExtendedByOrderId(Guid orderId)
        {
            return _tvDbDataStore.GetOrderInfoExtendedByOrderId(orderId);
        }

        public List<Ad> GetOrderAds(Guid orderId)
        {
            return _tvDbDataStore.GetOrderAdsByOrderId(orderId);
        }

        public List<GeoData> GetOrderGeoSelectedServiceAreas(Guid orderId)
        {
            return _tvDbDataStore.GetOrderGeoSelectedServiceAreasByOrderId(orderId);
        }

        public List<Guid> GetOrderAgeRanges(Guid orderId)
        {
            return _tvDbDataStore.GetOrderAgeRangesByOrderId(orderId);
        }

        public string GetOrdersCompetitorsUrls(Guid orderId)
        {
            return _tvDbDataStore.GetOrdersCompetitorsUrlsByOrderId(orderId);
        }

        public List<Guid> GetOrdersGenders(Guid orderId)
        {
            return _tvDbDataStore.GetOrdersGendersByOrderId(orderId);
        }

        public List<short> GetOrdersHouseholdIncome(Guid orderId)
        {
            return _tvDbDataStore.GetOrdersHouseholdIncomeByOrderId(orderId);
        }

        public string GetOrdersKeywords(Guid orderId)
        {
            return _tvDbDataStore.GetOrdersKeywordsByOrderId(orderId);
        }

        public string GetOrderNotes(Guid orderId)
        {
            return _tvDbDataStore.GetOrderNotesByOrderId(orderId);
        }

        public List<Guid> GetOrderParentalStatus(Guid orderId)
        {
            return _tvDbDataStore.GetOrderParentalStatusByOrderId(orderId);
        }

        public List<OrderLite> SearchOrderLiteByString(string searchValue, Guid userId)
        {
            var orders = _tvDbDataStore.SearchOrderLiteByString(searchValue, userId);
            return orders;
        }

        public AccountMetaData GetAdvertiserMetaData(Guid campaignId)
        {
            return _tvDbDataStore.GetAdvertiserMetaData(campaignId);
        }

        public OrderPauseData GetOrderPauseDataByOrderId(Guid orderId)
        {
            return _tvDbDataStore.GetOrderPauseDataByOrderId(orderId);
        }

        public OrderCancelData GetOrderCancelDataByOrderId(Guid orderId)
        {
            return _tvDbDataStore.GetOrderCancelDataByOrderId(orderId);
        }

        public Guid? FindLiveBudgetGroupXrefPlacement(Guid budgetGroupXrefPlacementId)
        {
            return _tvDbDataStore.FindLiveBudgetGroupXrefPlacement(budgetGroupXrefPlacementId);
        }

        public void AddAgeGroupToOrderTargetAgeGroups(Guid orderId, Guid age)
        {
            _tvDbDataStore.AddAgeGroupToOrderTargetAgeGroups(orderId, age);
        }

        public void AddGenderToOrderTargetGender(Guid orderId, Guid gender)
        {
            _tvDbDataStore.AddGenderToOrderTargetGender(orderId, gender);
        }

        public void AddParentalStatusToOrderTargetParentalStatus(Guid orderId, Guid par)
        {
            _tvDbDataStore.AddParentalStatusToOrderTargetParentalStatus(orderId, par);
        }

        public void AddUrlsToOrderCompetitorsUrl(Guid orderId, string competitorsUrls)
        {
            _tvDbDataStore.AddUrlsToOrderCompetitorsUrl(orderId, competitorsUrls);
        }

        public void AddIncomeToOrderTargetIncome(Guid orderId, short hi)
        {
            _tvDbDataStore.AddIncomeToOrderTargetIncome(orderId, hi);
        }

        public void AddNoteToOrderTargetNote(Guid orderId, string note)
        {
            _tvDbDataStore.AddNoteToOrderTargetNote(orderId, note);
        }

        public void AddKeyWordsToOrderTargetKeyworks(Guid orderId, string keyWords)
        {
            _tvDbDataStore.AddKeyWordsToOrderTargetKeyWords(orderId, keyWords);
        }

        public void CreateProposedOrderAd(Guid orderId, int selectedAdFormat, string adNameInput, string youtubeUrlInput,
            string clickableUrlInput, string landingUrlInput, string campanionBannerUrl, bool pause)
        {
            _tvDbDataStore.CreateProposedOrderAd(orderId, selectedAdFormat, adNameInput, youtubeUrlInput, clickableUrlInput, landingUrlInput, campanionBannerUrl, pause);
        }

        #endregion

        #region Setters

        public void InsertAccount(TvAccount account)
        {
            _tvDbDataStore.InsertAccount(account);
        }

        public void InsertAd(TvAd ad)
        {
            _tvDbDataStore.InsertAd(ad);
        }

        public void InsertAdAdwordsInfo(TvAdAdwordsInfo tvAdAdwordsInfo)
        {
            _tvDbDataStore.InsertAdAdwordsInfo(tvAdAdwordsInfo);
        }

        public void InsertAdvertiser(TvAdvertiser advertiser)
        {
            _tvDbDataStore.InsertAdvertiser(advertiser);
        }

        public void InsertBudgetGroup(TvBudgetGroup budgetGroup)
        {
            _tvDbDataStore.InsertBudgetGroup(budgetGroup);
        }

        public void InsertBudgetGroupAdwordsInfo(TvBudgetGroupAdwordsInfo budgetGroupAdwordsInfo)
        {
            _tvDbDataStore.InsertBudgetGroupAdwordsInfo(budgetGroupAdwordsInfo);
        }

        public void InsertBudgetGroupTimedBudget(TvBudgetGroupTimedBudget budgetGroupTimedBudget)
        {
            _tvDbDataStore.InsertBudgetGroupTimedBudget(budgetGroupTimedBudget);
        }

        public void InsertBudgetGroupXrefAd(TvBudgetGroupXrefAd budgetGroupXrefAd)
        {
            _tvDbDataStore.InsertBudgetGroupXrefAd(budgetGroupXrefAd);
        }

        public void InsertBudgetGroupXrefLocation(TvBudgetGroupXrefLocation budgetGroupXrefLocation)
        {
            _tvDbDataStore.InsertBudgetGroupXrefLocation(budgetGroupXrefLocation);
        }

        public void InsertBudgetGroupXrefPlacement(TvBudgetGroupXrefPlacement budgetGroupXrefPlacement)
        {
            _tvDbDataStore.InsertBudgetGroupXrefPlacement(budgetGroupXrefPlacement);
        }

        public void InsertCampaign(TvCampaign campaign)
        {
            _tvDbDataStore.InsertCampaign(campaign);
        }

        public void InsertCampaignAdwordsInfo(TvCampaignAdWordsInfo campaignAdWordsInfo)
        {
            _tvDbDataStore.InsertCampaignAdwordsInfo(campaignAdWordsInfo);
        }

        public void InsertLocation(TvLocation location)
        {
            _tvDbDataStore.InsertLocation(location);
        }

        public void InsertOrder(TvOrder order)
        {
            _tvDbDataStore.InsertOrder(order);
        }

        public void InsertOrderWithParent(TvOrder order)
        {
            _tvDbDataStore.InsertOrderWithParent(order);
        }

        public void InsertTargetAgeGroup(TvOrderTargetAgeGroup orderTargetAgeGroup)
        {
            _tvDbDataStore.InsertTargetAgeGroup(orderTargetAgeGroup);
        }

        public void InsertTargetGender(TvOrderTargetGender orderTargetGender)
        {
            _tvDbDataStore.InsertTargetGender(orderTargetGender);
        }

        public void InsertOrderXrefBudgetGroup(TvOrderXrefBudgetGroup orderXrefBudgetGroup)
        {
            _tvDbDataStore.InsertOrderXrefBudgetGroup(orderXrefBudgetGroup);
        }

        public void InsertPlacement(TvPlacement placement)
        {
            _tvDbDataStore.InsertPlacement(placement);
        }

        public void InsertVideoAsset(TvVideoAsset videoAsset)
        {
            _tvDbDataStore.InsertVideoAsset(videoAsset);
        }

        public void InsertVideoAssetVersion(TvVideoAssetVersion videoAssetVersion)
        {
            _tvDbDataStore.InsertVideoAssetVersion(videoAssetVersion);
        }

        public void PrepareBudgetGroupTimedBudgetsIfReorder(long awcustomerId)
        {
            _tvDbDataStore.PrepareBudgetGroupTimedBudgetsIfReorder(awcustomerId);
        }

        public void SaveVideoAssetIdToVideoAsset( Guid videoAssetId, string uploadResponse)
        {
            _tvDbDataStore.SaveVideoAssetIdToVideoAsset(videoAssetId, uploadResponse);
        }

        public void LockCustomer(long customerId, string email, string importType)
        {
            _tvDbDataStore.LockCustomer(customerId, email, importType);
        }

        public void UnlockCustomer(long customerId, string email)
        {
            _tvDbDataStore.UnlockCustomer(customerId, email);
        }

        public void SaveOrderVersionForHistory(Order order, Guid userId)
        {
            _tvDbDataStore.SaveOrderVersionForHistory(order, userId);
        }

        public void AssociateNewOrderIdToParentWithBudgetGroup(Guid orderId, Guid parentOrderId)
        {
            _tvDbDataStore.AssociateNewOrderIdToParentWithBudgetGroup(orderId, parentOrderId);
        }

        #endregion

        #region Updater

        public TvBudgetGroupTimedBudget UpdateBudgetGroupTimedBudget(
            Guid budgetGroupTimedBudgetId, 
            decimal campaignBudget, 
            DateTime startDate,
            DateTime endDate, 
            decimal margin,
            Guid budgetGroupXrefPlacementId)
        {
            return _tvDbDataStore.UpdateBudgetGroupTimedBudget(
                budgetGroupTimedBudgetId, 
                campaignBudget, 
                startDate, 
                endDate, 
                margin,
                budgetGroupXrefPlacementId);
        }

        public void UpdateHistoricalBudgetGroupTimedBudget(
            Guid budgetGroupTimedBudgetId, 
            decimal campaignBudget, 
            DateTime startDate,
            DateTime endDate, 
            decimal margin,
            Guid budgetGroupXrefPlacementId)
        {
            _tvDbDataStore.UpdateHistoricalBudgetGroupTimedBudget(
                budgetGroupTimedBudgetId, 
                campaignBudget, 
                startDate, 
                endDate, 
                margin,
                budgetGroupXrefPlacementId);
        }

        public void UpdateProposedBudgetGroupTimedBudget(
            Guid budgetGroupTimedBudgetId, 
            decimal campaignBudget, 
            DateTime startDate,
            DateTime endDate, 
            decimal margin,
            Guid budgetGroupXrefPlacementId)
        {
            _tvDbDataStore.UpdateProposedBudgetGroupTimedBudget(
                budgetGroupTimedBudgetId, 
                campaignBudget, 
                startDate, 
                endDate, 
                margin,
                budgetGroupXrefPlacementId);
        }

        public void UpdateBudgetGroupStructureFromProposeToLive(Guid budgetGroupId)
        {
            _tvDbDataStore.UpdateBudgetGroupStructureFromProposeToLive(budgetGroupId);
        }

        public void UpdateOrderInfoData(Guid orderId, DateTime startDate, DateTime endDate, decimal totalBudget)
        {
            _tvDbDataStore.UpdateOrderInfoData(orderId, startDate, endDate, totalBudget);
        }

        public bool SetOrderPausedStatus(Guid orderId)
        {
            return _tvDbDataStore.SetOrderPausedStatus(orderId);
        }

        public bool SetOrderCancelled(Guid orderId)
        {
            return _tvDbDataStore.SetOrderCancelled(orderId);
        }

        public void EnsureProperCampaignMangerAssignedToCustomer(long customerId, string campaignMangerEmail)
        {
            _tvDbDataStore.EnsureProperCampaignMangerAssignedToCustomer(customerId, campaignMangerEmail);
        }

        #endregion

        #region Deleters
        public void RemoveLocationDetails(Guid orderId, Guid locId)
        {
            _tvDbDataStore.RemoveLocationDetails(orderId, locId);
        }

        public void DeleteOrderTargetAgeGroup(Guid orderId)
        {
            _tvDbDataStore.DeleteOrderTargetAgeGroup(orderId);
        }

        public void DeleteOrderTargetGender(Guid orderId)
        {
            _tvDbDataStore.DeleteOrderTargetGender(orderId);
        }

        public void DeleteOrderTargetParentalStatus(Guid orderId)
        {
            _tvDbDataStore.DeleteOrderTargetParentalStatus(orderId);
        }

        public void DeleteOrderTargetCompetitorsUrls(Guid orderId)
        {
            _tvDbDataStore.DeleteOrderTargetCompetitorsUrls(orderId);
        }

        public void DeleteOrderTargetHouseholdIncome(Guid orderId)
        {
            _tvDbDataStore.DeleteOrderTargetHouseholdIncome(orderId);
        }

        public void DeleteOrderTargetingNotes(Guid orderId)
        {
            _tvDbDataStore.DeleteOrderTargetingNotes(orderId);
        }

        public void DeleteOrderTargetingKeyWords(Guid orderId)
        {
            _tvDbDataStore.DeleteOrderTargetingKeyWords(orderId);
        }

        public List<OrderTargetAgeGroup> GetOrderAgeRangesExtended(Guid orderId)
        {
            return _tvDbDataStore.GetOrderAgeRangesExtended(orderId);
        }

        public List<OrderTargetGenderGroup> GetOrdersGendersExtended(Guid orderId)
        {
            return _tvDbDataStore.GetOrdersGendersExtended(orderId);
        }

        public List<OrderHouseholdIncomeGroup> GetOrdersHouseholdIncomeExtended(Guid orderId)
        {
            return _tvDbDataStore.GetOrdersHouseholdIncomeExtended(orderId);
        }

        public List<OrderParentalStatusGroup> GetOrderParentalStatusExtended(Guid orderId)
        {
            return _tvDbDataStore.GetOrderParentalStatusExtended(orderId);
        }
        #endregion
    }
}