﻿using System;
using System.Collections.Generic;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.TargetViewDb.DAL.Repositories
{
    public class TargetViewRepository : ITargetViewRepository
    {
        private readonly ITargetViewDataStore _targetViewDataStore;

        public TargetViewRepository(ITargetViewDataStore targetViewDataStore)
        {
            _targetViewDataStore = targetViewDataStore;
        }

        public List<TargetViewAdwordsData> GetTargetViewAdwordsAssociations(long customerId)
        {
            return _targetViewDataStore.GetTargetViewAdwordsAssociations(customerId);
        }

        public List<TargetViewAdwordsAssociatedData> GetAssociatedTargetViewAdwordsAssociations(DateTime statDate)
        {
            return _targetViewDataStore.GetAssociatedTargetViewAdwordsAssociations(statDate);
        }

        public List<VideoAssetData> GetTargetViewVideoAssetsByAdvertiser(Guid advertiserId)
        {
            return _targetViewDataStore.GetTargetViewVideoAssetsByAdvertiser(advertiserId);
        }

        public List<TvCampaignAdWordsInfo> GetListWithoutAdwordsCustomerId(DateTime liveOrderDate)
        {
            return _targetViewDataStore.GetListWithoutAdwordsCustomerId(liveOrderDate);
        }

        public void UpdateCampaignAdWordsInfoWithCustomerId(Guid campaignAdWordsInfoId, long customerId, string lastModifedBy)
        {
            _targetViewDataStore.UpdateCampaignAdWordsInfoWithCustomerId(campaignAdWordsInfoId, customerId, lastModifedBy);
        }

        public void UpdateBudgetGroupAdWordsInfoWithCampaignId(Guid budgetGroupId, long campaignId, string campaignName, string lastModifiedBy)
        {
            _targetViewDataStore.UpdateBudgetGroupAdWordsInfoWithCampaignId(budgetGroupId, campaignId, campaignName, lastModifiedBy);
        }

        public void UpdateAdAdWordsInfoWithAdId(Guid adId, string awAdName, long awAdId, long awCampaignId, Guid budgetGroupId,
            string lastModifiedBy)
        {
            throw new NotImplementedException();
        }

        public void AddAdWordsInfoWithAdId(Guid adId, string awAdName, long awAdId, long awCampaignId, Guid budgetGroupId, string lastModifiedBy)
        {
            _targetViewDataStore.AddAdWordsInfoWithAdId(adId, awAdName, awAdId,awCampaignId, budgetGroupId, lastModifiedBy);
        }
    }
}