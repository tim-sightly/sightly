﻿using Sightly.Models;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.TargetViewDb.DAL.Repositories
{
    public class AdRepository : IAdRepository
    {
        private ITvAdDataStore _adDataStore;

        public AdRepository(ITvAdDataStore tvAdDataStore)
        {
            _adDataStore = tvAdDataStore;
        }

        public void CreateAdAdWordsInfo(AdAdWordsInfoData adAdwordsInfo)
        {
            _adDataStore.CreateAdAdWordsInfo(adAdwordsInfo);
        }
    }
}