﻿using System;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.TargetViewDb.DAL.Repositories
{
    public class RawStatsRepository : IRawStatsRepository
    {
        private readonly ITvRawStatsDataStore _tvRawStatsStore;

        public RawStatsRepository(ITvRawStatsDataStore rawStatsDataStore)
        {
            _tvRawStatsStore = rawStatsDataStore;
        }
        public void InsertRawAdDeviceStat(AdDevicePerformanceFreeData adDeviceData)
        {
            _tvRawStatsStore.InsertRawAdDeviceStat(adDeviceData);
        }

        public void InsertRawAdDeviceStat(AdDevicePerformanceData adDeviceData)
        {
            _tvRawStatsStore.InsertRawAdDeviceStat(adDeviceData);
        }

        public void InsertRawAdGroupStat(AdGroupPerformanceFreeData adGroupData)
        {
            _tvRawStatsStore.InsertRawAdGroupStat(adGroupData);
        }

        public void InsertRawAgeRangeStat(AgeRangePerformanceFreeData ageRangeData)
        {
            _tvRawStatsStore.InsertRawAgeRangeStat(ageRangeData);
        }

        public void InsertRawGenderStat(GenderPerformanceFreeData genderData)
        {
            _tvRawStatsStore.InsertRawGenderStat(genderData);
        }

        public void InsertRawGeoStat(GeoPerformanceFreeData geoData)
        {
            _tvRawStatsStore.InsertRawGeoStat(geoData);
        }

        public void InsertRawMoatStats(MoatDailyStatData moatData)
        {
            _tvRawStatsStore.InsertRawMoatDailyStat(moatData);
        }

        public void InsertNielsenCampaignData(NielsenCampaignReference campaignData)
        {
            _tvRawStatsStore.InsertNielsenCampaignData(campaignData);
        }

        public void InsertNielsenCampaignPlacementData(NielsenCampaignSiteReference campaignPlacementData)
        {
            _tvRawStatsStore.InsertNielsenCampaignPlacementData(campaignPlacementData);
        }

        public void InsertNielsenStatsExposure(NielsenCampaignPlacementExposure statsData)
        {
            _tvRawStatsStore.InsertNielsenStatsExposure(statsData);
        }

        public void InsertDoubleVerifyStats(DoubleVerifyStats statsData)
        {
            _tvRawStatsStore.InsertDoubleVerifyStats(statsData);
        }

        public NielsenDailyDarStats GetDailyNielsenDarStat(long cid, DateTime yesterday)
        {
            return _tvRawStatsStore.GetDailyNielsenDarStat(cid, yesterday);
        }

        public void InsertDailyNielsenDarStat(NielsenDailyDarStats dailyNielsenDarStat)
        {
            _tvRawStatsStore.InsertDailyNielsenDarStat(dailyNielsenDarStat);
        }

        public void CreateOrUpdateNielsenCampaignAndReference(NielsenCampaignReference camp)
        {
            _tvRawStatsStore.CreateOrUpdateNielsenCampaignAndReference(camp);
        }
    }
}