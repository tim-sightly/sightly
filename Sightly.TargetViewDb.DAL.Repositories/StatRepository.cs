﻿using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.TargetViewDb.DAL.Repositories
{
    public class StatRepository : IStatRepository
    {
        private readonly ITvStatDataStore _tvStatDataStore;

        public StatRepository(ITvStatDataStore tvStatDataStore)
        {
            _tvStatDataStore = tvStatDataStore;
        }

        public List<AccountCustomerData> GetChannelPartnerMccCustomerList()
        {
            return _tvStatDataStore.GetChannelPartnerMccCustomerList();
        }

        public List<BudgetGroupCampaignWithAdData> GetChannelPartnerCampaignList()
        {
            return _tvStatDataStore.GetChannelPartnerCampaignList();
        }

        public List<long> GetCustomerIdsFromNielsenData(DateTime yesterday)
        {
            return _tvStatDataStore.GetCustomerIdsFromNielsenData(yesterday);
        }

        public List<long> GetMediaAgencyMccCustomerList()
        {
            return _tvStatDataStore.GetAllMediaAgencyMccCustomerList();
        }

        public void SetAdwordsCustomerId(List<AdvertiserCustomerData> advertiserCustomerData)
        {
            _tvStatDataStore.SetAdwordsCustomerId(advertiserCustomerData);
        }

        public void SetAdwordsCampaignId(List<BudgetGroupCampaignData> budgetGroupCampaignData)
        {
            _tvStatDataStore.SetAdwordsCampaignId(budgetGroupCampaignData);
        }

        public void SetAdwordsAdId(List<AdAwAdData> adAwAdData)
        {
            _tvStatDataStore.SetAdwrodsAdId(adAwAdData);
        }

        public List<long> GetAllRunningAdWordsCidsFromTvByDate(DateTime statDate)
        {
            return _tvStatDataStore.GetAllRunningAdwordsCidsFromTvByDate(statDate);
        }

        public List<AdwordsTvAssociationData> GetAllRunningAdWordsOrdersFromTvByDate(DateTime statDate)
        {
            return _tvStatDataStore.GetAllRunningAdwordsOrdersFromTvByDate(statDate);
        }

        public List<AdwordsTvAssociationData> GetAdwordsTvAssociationData(long campaignId)
        {
            return _tvStatDataStore.GetAdwordsTvAssociationData(campaignId);
        }

        public List<long> GetDailyStatsRegatherByDate(DateTime statDate)
        {
            return _tvStatDataStore.GetDailyStatsRegatherByDate(statDate);
        }

        public List<long> GetMccCustomerList()
        {
            return _tvStatDataStore.GetMccCustomerList();
        }

        public List<MissingCidStatDates> GetMissingCidAndStatDates()
        {
            return _tvStatDataStore.GetMissingCidAndStatsDates();
        }

        public List<MissingCidStatDates> GetMissingCidStatsByDate(DateTime statDate)
        {
            return _tvStatDataStore.GetMissingCidStatsByDate(statDate);
        }

        public List<long> GetNielsenCampaignsByCidAndDate(long customerId, DateTime date)
        {
            return _tvStatDataStore.GetNielsenCampaignsByCidAndDate(customerId, date);
        }

        public List<long> GetNielsenCampaignsByDate(DateTime statDate)
        {
            return _tvStatDataStore.GetNielsenCampaignsByDate(statDate);
        }

        public CustomerStartEndData GetStartAndEndDateForCustomerById(long customerId)
        {
            return _tvStatDataStore.GetStartAndEndDateForCustomerById(customerId);
        }

        public List<CustomerCampaignRawStatsByDayData> GetValidationForDailyRawDataByCustomerIdAndDate(long customerId, DateTime date)
        {
            return _tvStatDataStore.GetValidationForDailyRawDataByCustomerIdAndDate(customerId, date);
        }

        public int? GetAdVideoLengthByAdId(Guid adId)
        {
            return _tvStatDataStore.GetAdVideoLengthByAdId(adId);
        }

        public void InsertBudgetGroupStats(List<BudgetGroupStats> budgetGroupStats)
        {
            budgetGroupStats.ForEach(bgs =>
            {
                bgs.BudgetGroupStatsId = Guid.NewGuid();
            });
            _tvStatDataStore.InsertBudgetGroupStats(budgetGroupStats);
        }

        public void InsertBudgetGroupStatsOneAtATime(List<BudgetGroupStats> budgetGroupStats)
        {
            budgetGroupStats.ForEach(bgs =>
            {
                bgs.BudgetGroupStatsId = Guid.NewGuid();
                _tvStatDataStore.InsertBudgetGroupStatsOneAtATime(bgs);
            });
        }

        public void InsertBudgetGroupAdStats(List<BudgetGroupAdStats> budgetGroupAdStats)
        {
            budgetGroupAdStats.ForEach(bgas =>
            {
                bgas.BudgetGroupAdStatsId = Guid.NewGuid();
            });
            _tvStatDataStore.InsertBudgetGroupAdStats(budgetGroupAdStats);
        }

        public void InsertBudgetGroupAgeStats(List<BudgetGroupAgeStats> budgetGroupAgeStats)
        {
            budgetGroupAgeStats.ForEach(bgas =>
            {
                bgas.BudgetGroupTargetAgeGroupStatsId = Guid.NewGuid();
            });

            _tvStatDataStore.InsertBudgetGroupAgeStats(budgetGroupAgeStats);
        }

        public void InsertBudgetGroupDeviceStats(List<BudgetGroupDeviceStats> budgetGroupDeviceStats)
        {
            budgetGroupDeviceStats.ForEach(bgds =>
            {
                bgds.BudgetGroupDeviceStatsId = Guid.NewGuid();
            });
            _tvStatDataStore.InsertBudgetGroupDeviceStats(budgetGroupDeviceStats);
        }

        public void InsertBudgetGroupGenderStats(List<BudgetGroupGenderStats> budgetGroupGenderStats)
        {
            budgetGroupGenderStats.ForEach(bggs =>
            {
                bggs.BudgetGroupTargetGenderStatsId = Guid.NewGuid();
            });

            _tvStatDataStore.InsertBudgetGroupGenderStats(budgetGroupGenderStats);
        }

        public void InsertAdStats(List<AdStats> adStats)
        {
            adStats.ForEach(adStat =>
            {
                adStat.AdStatsId = Guid.NewGuid();
            });
            _tvStatDataStore.InsertAdStats(adStats);
        }

        public void InsertAgeGroupStats(List<AgeGroupStats> ageGroupStats)
        {
            ageGroupStats.ForEach(ageStat =>
            {
                ageStat.OrderTargetAgeGroupStatsId = Guid.NewGuid();
            });
            _tvStatDataStore.InsertAgeGroupStats(ageGroupStats);
        }

        public void InsertDailyStatsValidation(CustomerRawStatsByDayDate reportedStats)
        {
            _tvStatDataStore.InsertDailyStatsValidation(reportedStats);
        }

        public void InsertDeviceStats(List<DeviceStats> deviceStats)
        {
            deviceStats.ForEach(deviceStat =>
            {
                deviceStat.DeviceStatsId = Guid.NewGuid();
            });
            _tvStatDataStore.InsertDeviceStats(deviceStats);
        }

        public void InsertGenderStats(List<GenderStats> newGenderStats)
        {
            newGenderStats.ForEach(genderStat =>
            {
                genderStat.OrderTargetGenderStatsId = Guid.NewGuid();
            });
            _tvStatDataStore.InsertGenderStats(newGenderStats);
        }

        public void InsertMissingCidAndDate(long customer, DateTime statDate)
        {
            _tvStatDataStore.InsertMissingCidAndDate(customer, statDate);
        }

        public void InsertOrderStats(List<OrderStats> orderStats)
        {
            orderStats.ForEach(orderStat =>
            {
                orderStat.OrderStatId = Guid.NewGuid();
            });
            _tvStatDataStore.InsertOrderStats(orderStats);
        }

        public void DeleteMissingCidAndDate(long customerId, DateTime statDate)
        {
            _tvStatDataStore.DeleteMissingCidAndDate(customerId, statDate);
        }

        public void DeleteRawStatsData(long customer, DateTime statDate)
        {
            _tvStatDataStore.DeleteRawStatsData(customer, statDate);
        }

        public void DeleteRawAdGroupAndLocationStats(long customer, DateTime statdate)
        {
            _tvStatDataStore.DeleteRawAdGroupAndLocationStats(customer, statdate);
        }
    }
}