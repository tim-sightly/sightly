﻿using System;
using Sightly.TargetViewDb.DAL.DataStores.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.TargetViewDb.DAL.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private ITvOrderDataStore _orderDataStore;

        public OrderRepository(ITvOrderDataStore orderDataStore)
        {
            _orderDataStore = orderDataStore;
        }

        public void CreateOrderRecordLog(Guid orderRecordLogId, Guid orderId, string lastModifiedBy)
        {
            _orderDataStore.CreateOrderRecordLog(orderRecordLogId, orderId, lastModifiedBy);
        }

        public void AssignPlacementWithAwCampaignId(long awCampaignId, string placement, string lastModifiedBy)
        {
            _orderDataStore.AssignPlacementWithAwCampaignId(awCampaignId, placement, lastModifiedBy);
        }
    }
}