﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Models.tv;

namespace Sightly.Order
{
    public class OrderComparison : IOrderComparison
    {
        public  OrderChanges GetOrderDifferences(Models.tv.Order initialOrder, Models.tv.Order secondOrder)
        {
            try
            {
                OrderChanges changes = new OrderChanges
                {
                    InfoChanges = GetOrderInfoChanges(initialOrder.Info, secondOrder.Info),
                    AdChanges = GetOrderAdChanges(initialOrder.Ads, secondOrder.Ads),
                    AudienceChanges = GetOrderAudienceChanges(initialOrder.Audience, secondOrder.Audience),
                    GeoChanges = GetOrderGeoChanges(initialOrder.Geo, secondOrder.Geo)
                };

                return changes;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private InfoChanges GetOrderInfoChanges(Info initialOrderInfo, Info secondOrderInfo)
        {
            var infoChanges = new InfoChanges();

            if (initialOrderInfo.OrderName != secondOrderInfo.OrderName)
            {
                infoChanges.OrderNameChange = $"Order name was changed to {secondOrderInfo.OrderName}";
            }

            if (initialOrderInfo.OrderRefCode != secondOrderInfo.OrderRefCode)
            {
                infoChanges.OrderRefCodeChange = $"Order ref code was changed to {secondOrderInfo.OrderRefCode}";
            }

            if (initialOrderInfo.TotalBudget != secondOrderInfo.TotalBudget)
            {
                infoChanges.TotalBudgetChange = initialOrderInfo.TotalBudget < secondOrderInfo.TotalBudget ?
                        $"Total budget was increased to {secondOrderInfo.TotalBudget}" :
                        $"Total budget was decreased to {secondOrderInfo.TotalBudget}";
            }

            if (initialOrderInfo.EndDate != secondOrderInfo.EndDate)
            {
                infoChanges.EndDateChange = $"End date was changed to {secondOrderInfo.EndDate}";
            }

            return infoChanges;
        }

        private List<AdChanges> GetOrderAdChanges(List<Ad> initialOrderAds, List<Ad> secondOrderAds)
        {
            var adChanges = new List<AdChanges>();

           
            secondOrderAds.ForEach(ad =>
            {
                var adChange = new AdChanges();
                if (ad.Pause)
                {
                    adChange.AdPausedChange = $"{ad.AdName} is Paused";
                }
                //Find the corresponding ad from the initial list
                var originalAd = initialOrderAds.FirstOrDefault(x => x.YoutubeUrl == ad.YoutubeUrl);
                if (originalAd == null && ad.AdName != null)
                {
                    adChange.NewAdChange = $"{ad.AdName} is a new Ad";
                }
                else
                {
                    if (originalAd.ClickableUrl.Trim() != ad.ClickableUrl.Trim())
                    {
                        adChange.DisplayUrlChange = $"Change Display URL for {ad.AdName} to {ad.ClickableUrl}";
                    }

                    if (originalAd.LandingUrl.Trim() != ad.LandingUrl.Trim())
                    {
                        adChange.DestinationUrlChange = $"Change Destination URL for {ad.AdName} to {ad.LandingUrl}";
                    }

                    if (!string.IsNullOrEmpty(originalAd.CampanionBannerUrl))
                    {
                        if (!string.IsNullOrEmpty(ad.CampanionBannerUrl))
                        {
                            if (originalAd.CampanionBannerUrl != ad.CampanionBannerUrl)
                            {

                                adChange.CompanionBannerChange = $"Replace Companion Banner for {ad.AdName}";
                            }
                        }
                        else
                        {
                            adChange.CompanionBannerChange = $"Remove Companion Banner for {ad.AdName}";
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(ad.CampanionBannerUrl))
                        {
                            adChange.CompanionBannerChange = $"Add Companion Banner for {ad.AdName}";
                        }
                    }
                }
                if (adChange.IsChangeExisting)
                {
                    adChanges.Add(adChange);
                }


            });


            return adChanges;
        }


        private AudienceChanges GetOrderAudienceChanges(Audience initialOrderAudience, Audience secondOrderAudience)
        {
            var audienceChange = new AudienceChanges();

            //Age
            if (secondOrderAudience.AgeRanges.Except(initialOrderAudience.AgeRanges).ToList().Count > 0)
            {
                audienceChange.AgeRangeChanges = "Changes to the Age Range";
            }
            //Gender
            if (secondOrderAudience.Genders.Except(initialOrderAudience.Genders).ToList().Count > 0)
            {
                audienceChange.GenderChanges = "Changes to the Genders";
            }
            //Parental
            if (secondOrderAudience.ParentalStatuses.Except(initialOrderAudience.ParentalStatuses).ToList().Count > 0)
            {
                audienceChange.ParentalStatusChanges = "Changes to the Parental Status";
            }
            //HHI
            if (secondOrderAudience.HouseholdIncomes.Except(initialOrderAudience.HouseholdIncomes).ToList().Count > 0)
            {
                audienceChange.HouseholdIncomeChanges = "Changes to the Household Income";
            }
            //Keywords
            if (secondOrderAudience.KeyWords != initialOrderAudience.KeyWords)
            {
                audienceChange.KeywordChanges = $"Change Keywords to {secondOrderAudience.KeyWords}";
            }

            //Custom Interest URL
            if (secondOrderAudience.CompetitorUrls != initialOrderAudience.CompetitorUrls)
            {
                audienceChange.CompetitorUrlChanges = $"Change Custom Interest URLS to {secondOrderAudience.CompetitorUrls}";
            }
            //Notes
            if (secondOrderAudience.Notes != initialOrderAudience.Notes)
            {
                audienceChange.NoteChanges = $"Change Notes to {secondOrderAudience.Notes}";
            }

            return audienceChange;
        }

        private GeoChanges GetOrderGeoChanges(Geo initialOrderGeo, Geo secondOrderGeo)
        {
            var geoChange = new GeoChanges();
            if (!secondOrderGeo.SelectedServiceAreas.SequenceEqual(initialOrderGeo.SelectedServiceAreas, new DefaultGeoComparer()))
            {
                geoChange.ChangedGeos = $"Change Service areas to {string.Join(",", secondOrderGeo.SelectedServiceAreas.Select(g => g.DmaName))}";
            }

            return geoChange;
        }

        public class DefaultGeoComparer : IEqualityComparer<GeoData>
        {
            public bool Equals(GeoData x, GeoData y)
            {
                return x.Id == y.Id;
            }

            public int GetHashCode(GeoData obj)
            {
                return obj.Id.GetHashCode();
            }
        }
    }
}
