﻿
using Sightly.Models.tv;

namespace Sightly.Order
{
    public interface IOrderComparison
    {
        OrderChanges GetOrderDifferences(Models.tv.Order initialOrder, Models.tv.Order secondOrder);

        
    }
}