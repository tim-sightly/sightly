﻿using System;
using System.Collections.Generic;
using Sightly.Models;
using Sightly.Models.tv;
using Sightly.Models.tv.extended;

namespace Sightly.Business.OrderManager
{
    public interface IOrderManager
    {
        Guid CreateOrder(Order order);
        Guid CreateReorder(Order order);

        string GetNewOrderBlobById(int id);

        int SaveNewOrderBlob(Order order);

        List<Order> SearchNewOrderBlob(
            string orderName = null, 
            string orderRefCode = null, 
            DateTime? startDate = null,
            DateTime? endDate = null);
        
        Order GetExistingOrder(Guid orderId);
        OrderExtended GetExistingOrderExtended(Guid orderId);
        List<OrderLite> SearchOrderLiteByString(string searchValue, Guid userId);
        bool SetOrderPauseStatus(Guid orderId);
        void SaveOrderVersionForHistory(Order order, Guid userId);
        void UpdateOrder(Order order);
        bool SetOrderCancelled(Guid orderId);
        OrderPauseData GetOrderPauseDataByOrderId(Guid orderId);
        OrderCancelData GetOrderCancelDataByOrderId(Guid orderId);
    }
}
