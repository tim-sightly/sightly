﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Business.OrderManager.Crud.Interfaces;
using Sightly.Models;
using Sightly.Models.tv;
using Sightly.Models.tv.extended;

namespace Sightly.Business.OrderManager
{
    public class OrderManager : IOrderManager
    {
        private IAdvertiserHelper _advertiserHelper;
        private IInfoHelper _infoHelper;
        private IGeoHelper _geoHelper;
        private IAdHelper _adHelper;
        private IAudienceHelper _audienceHelper;
        private INewOrderBlobHelper _newOrderBlobHelper;
        private IOrderHelper _orderHelper;

        public OrderManager(IAdvertiserHelper advertiserHelper, IInfoHelper infoHelper, IGeoHelper geoHelper, IAdHelper adHelper, IAudienceHelper audienceHelper, INewOrderBlobHelper newOrderBlobHelper, IOrderHelper orderHelper)
        {
            _advertiserHelper = advertiserHelper;
            _infoHelper = infoHelper;
            _geoHelper = geoHelper;
            _adHelper = adHelper;
            _audienceHelper = audienceHelper;
            _newOrderBlobHelper = newOrderBlobHelper;
            _orderHelper = orderHelper;
        }

        public Guid CreateOrder(Order order)
        {
            //Manager order information
            order.Info = _infoHelper.CreateOrderInfo(
                    order.Info.OrderName, 
                    order.Info.OrderRefCode,
                    order.Info.Account, 
                    order.Info.Advertiser,
                    order.Info.Campaign, 
                    order.Info.TotalBudget, 
                    order.Info.Objective, 
                    order.Info.StartDate,
                    order.Info.EndDate);

            //Manage Infrastucture to a
            _geoHelper.AssociateGeographyToOrder(order.Info.OrderId.Value, order.Geo.SelectedServiceAreas);

            //loop throught the ads and save to proposedOrderAds
            order.Ads.ForEach(ad =>
            {
                _adHelper.CreateProposedOrderAd(order.Info.OrderId.Value, ad);
            });

            _audienceHelper.AddAudienceObjects(order.Info.OrderId.Value, order.Audience);

            return order.Info.OrderId.Value;
        }

        public Guid CreateReorder(Order order)
        {
            var originalOrderId = order.Info.OrderId;
            //Manager order information
            order.Info = _infoHelper.CreateOrderInfo(
                order.Info.OrderName,
                order.Info.OrderRefCode,
                order.Info.Account,
                order.Info.Advertiser,
                order.Info.Campaign,
                order.Info.TotalBudget,
                order.Info.Objective,
                order.Info.StartDate,
                order.Info.EndDate,
                order.Info.Paused,
                order.Info.ParentOrderId);

            if (!originalOrderId.HasValue && order.Info.ParentOrderId.HasValue)
            {
                _geoHelper.AssociateNewOrderIdToParentWithBudgetGroup(order.Info.OrderId.Value, order.Info.ParentOrderId.Value);
            }
            else
            {
                //Manage Infrastucture to a Location and Geography
                _geoHelper.AssociateGeographyToOrder(order.Info.OrderId.Value, order.Geo.SelectedServiceAreas);
            }

            //loop throught the ads and save to proposedOrderAds
            order.Ads.ForEach(ad =>
            {
                _adHelper.CreateProposedOrderAd(order.Info.OrderId.Value, ad);
            });

            _audienceHelper.AddAudienceObjects(order.Info.OrderId.Value, order.Audience);

            return order.Info.OrderId.Value;
        }

        public string GetNewOrderBlobById(int id)
        {
            return _newOrderBlobHelper.GetNewOrderBlobById(id);
        }

        public int SaveNewOrderBlob(Order order)
        {
            var orderJson = JsonConvert.SerializeObject(order);
            AccountMetaData accountMetaData = _advertiserHelper.GetAdvertiserMetaData(order.Info.Campaign);
            return _newOrderBlobHelper.SaveNewOrderBlob(
                order.Info.OrderName, 
                order.Info.OrderRefCode,
                orderJson, 
                order.Info.StartDate, 
                order.Info.EndDate,
                accountMetaData.AccountName,
                accountMetaData.AdvertiserName,
                order.Info.Advertiser,
                accountMetaData.CampaignName,
                order.Info.Campaign,
                order.Info.TotalBudget
                );
        }

        public List<Order> SearchNewOrderBlob(
            string orderName = null, 
            string orderRefCode = null, 
            DateTime? startDate = null,
            DateTime? endDate = null)
        {
            return _newOrderBlobHelper.SearchNewOrderBlob(orderName, orderRefCode, startDate, endDate);
        }

        public List<Order> SearchOrderInfoByString(string orderString)
        {
            var orderData = new List<Order>();
            return orderData;
        }

        public Order GetExistingOrder(Guid orderId)
        {
            var order = new Order
            {
                Info = _infoHelper.GetOrderInfo(orderId),
                Ads = _adHelper.GetOrderAds(orderId),
                Audience = _audienceHelper.GetOrderAudience(orderId),
                Geo = _geoHelper.GetOrderGeo(orderId)
            };

            return order;
        }

        public OrderExtended GetExistingOrderExtended(Guid orderId)
        {
            var order = new OrderExtended
            {
                Info = _infoHelper.GetOrderInfoExtended(orderId),
                Ads = _adHelper.GetOrderAds(orderId),
                Audience = _audienceHelper.GetOrderAudienceExtended(orderId),
                Geo = _geoHelper.GetOrderGeo(orderId)
            };
            return order;
        }

        public List<OrderLite> SearchOrderLiteByString(string searchValue, Guid userId)
        {
            return _orderHelper.SearchOrderLiteByString(searchValue, userId);
        }

        public bool SetOrderPauseStatus(Guid orderId)
        {
            return _orderHelper.SetOrderPausedStatus(orderId);
        }

        public void SaveOrderVersionForHistory(Order order, Guid userId)
        {
            _orderHelper.SaveOrderVersionForHistory(order, userId);
        }

        public void UpdateOrder(Order order)
        {
            // Update Location
            _geoHelper.UpdateLocationData(order.Info.OrderId.Value, order.Geo);
            // Update Audience
            _audienceHelper.UpdateAudienceData(order.Info.OrderId.Value, order.Audience);
            //
            // add Ads
            order.Ads.ForEach(ad =>
            {
                _adHelper.CreateProposedOrderAd(order.Info.OrderId.Value, ad);
            });
            // Update Order info - just Budget, Dates and paused field
            _orderHelper.UpdateOrderInfoData(order.Info.OrderId.Value, order.Info);
        }

        public bool SetOrderCancelled(Guid orderId)
        {
            return _orderHelper.SetOrderCancelled(orderId);
        }

        public OrderPauseData GetOrderPauseDataByOrderId(Guid orderId)
        {
            return _orderHelper.GetOrderPauseDataByOrderId(orderId);
        }

        public OrderCancelData GetOrderCancelDataByOrderId(Guid orderId)
        {
            return _orderHelper.GetOrderCancelDataByOrderId(orderId);
        }
    }
}