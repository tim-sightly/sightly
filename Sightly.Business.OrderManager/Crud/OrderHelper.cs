﻿using System;
using System.Collections.Generic;
using Sightly.Business.OrderManager.Crud.Interfaces;
using Sightly.Models;
using Sightly.Models.tv;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.OrderManager.Crud
{
    public class OrderHelper : IOrderHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public OrderHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public List<OrderLite> SearchOrderLiteByString(string searchValue, Guid userId)
        {
            return _tvDbRepository.SearchOrderLiteByString(searchValue, userId);
        }

        public bool SetOrderPausedStatus(Guid orderId)
        {
            return _tvDbRepository.SetOrderPausedStatus(orderId);
        }

        public void SaveOrderVersionForHistory(Order order, Guid userId)
        {
             _tvDbRepository.SaveOrderVersionForHistory(order, userId);
        }

        public void UpdateOrderInfoData(Guid orderId, Info orderInfo)
        {
            _tvDbRepository.UpdateOrderInfoData(orderId, orderInfo.StartDate, orderInfo.EndDate, orderInfo.TotalBudget);
        }

        public bool SetOrderCancelled(Guid orderId)
        {
            return _tvDbRepository.SetOrderCancelled(orderId);
        }

        public OrderPauseData GetOrderPauseDataByOrderId(Guid orderId)
        {
            return _tvDbRepository.GetOrderPauseDataByOrderId(orderId);
        }

        public OrderCancelData GetOrderCancelDataByOrderId(Guid orderId)
        {
            return _tvDbRepository.GetOrderCancelDataByOrderId(orderId);
        }
    }
}