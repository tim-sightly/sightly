﻿using System;
using System.Collections.Generic;
using Sightly.Business.OrderManager.Crud.Interfaces;
using Sightly.Models.tv;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.OrderManager.Crud
{
    public class GeoHelper : IGeoHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public GeoHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public bool AssociateGeographyToOrder(Guid orderId, List<GeoData> geographies)
        {
            try
            {
                var locId = _tvDbRepository.CreateLocationIfNotExisting(orderId);
                geographies.ForEach(geo =>
                {
                    _tvDbRepository.AddGeoIfNotExisting(orderId, locId, geo.Id);
                });
            }
            catch
            {
                return false;
            }

            return true;
        }

        public Geo GetOrderGeo(Guid orderId)
        {
            var geo = new Geo
            {
                SelectedServiceAreas = _tvDbRepository.GetOrderGeoSelectedServiceAreas(orderId)
            };

            return geo;
        }

        public void UpdateLocationData(Guid orderId, Geo geo)
        {
            var locId = _tvDbRepository.CreateLocationIfNotExisting(orderId);

            //Delete all of the locId to geoIds OR LocationDetails
            _tvDbRepository.RemoveLocationDetails(orderId, locId);



            geo.SelectedServiceAreas.ForEach(g =>
            {
                _tvDbRepository.AddGeoIfNotExisting(orderId, locId, g.Id);
            });

        }

        public void AssociateNewOrderIdToParentWithBudgetGroup(Guid orderId, Guid parentOrderId)
        {
            _tvDbRepository.AssociateNewOrderIdToParentWithBudgetGroup(orderId, parentOrderId);
        }
    }
}