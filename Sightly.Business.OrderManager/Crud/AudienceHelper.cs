﻿using System;
using System.Collections.Generic;
using Sightly.Business.OrderManager.Crud.Interfaces;
using Sightly.Models.tv;
using Sightly.Models.tv.extended;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.OrderManager.Crud
{
    public class AudienceHelper : IAudienceHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public AudienceHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public void AddOrderTargetAgeGroup(Guid orderId, List<Guid> ageGroups)
        {
            ageGroups.ForEach(age =>
            {
                _tvDbRepository.AddAgeGroupToOrderTargetAgeGroups(orderId, age);
            });
        }

        private void DeleteOrderTargetAgeGroup(Guid orderId)
        {
            _tvDbRepository.DeleteOrderTargetAgeGroup(orderId);
        }

        public void AddOrderTargetGender(Guid orderId, List<Guid> genderGroups)
        {
            genderGroups.ForEach(gender =>
            {
                _tvDbRepository.AddGenderToOrderTargetGender(orderId, gender);
            });
        }

        private void DeleteOrderTargetGender(Guid orderId)
        {
            _tvDbRepository.DeleteOrderTargetGender(orderId);
        }

        public void AddOrderTargetParentalStatus(Guid orderId, List<Guid> parentalStatus)
        {
            parentalStatus.ForEach(par =>
                {
                    _tvDbRepository.AddParentalStatusToOrderTargetParentalStatus(orderId, par);
                });
        }

        private void DeleteOrderTargetParentalStatus(Guid orderId)
        {
            _tvDbRepository.DeleteOrderTargetParentalStatus(orderId);
        }

        public void AddOrderTargetCompetitorsUrls(Guid orderId, string competitorsUrls)
        {
            _tvDbRepository.AddUrlsToOrderCompetitorsUrl(orderId, competitorsUrls);
        }
        private void DeleteOrderTargetCompetitorsUrls(Guid orderId)
        {
            _tvDbRepository.DeleteOrderTargetCompetitorsUrls(orderId);
        }

        public void AddOrderTargetHouseholdIncome(Guid orderId, List<short> householdIncomes)
        {
            householdIncomes.ForEach(hi =>
            {
                _tvDbRepository.AddIncomeToOrderTargetIncome(orderId, hi);
            });
        }

        private void DeleteOrderTargetHouseholdIncome(Guid orderId)
        {
            _tvDbRepository.DeleteOrderTargetHouseholdIncome(orderId);
        }

        public void AddOrderTargetingNotes(Guid orderId, string note)
        {
            _tvDbRepository.AddNoteToOrderTargetNote(orderId, note);
        }

        private void DeleteOrderTargetingNotes(Guid orderId)
        {
            _tvDbRepository.DeleteOrderTargetingNotes(orderId);
        }


        private void AddOrderTargetingKeyWords(Guid orderId, string keyWords)
        {
            _tvDbRepository.AddKeyWordsToOrderTargetKeyworks(orderId, keyWords);
        }

        private void DeleteOrderTargetingKeyWords(Guid orderId)
        {
            _tvDbRepository.DeleteOrderTargetingKeyWords(orderId);
        }
        public void AddAudienceObjects(Guid orderId, Audience audience)
        {
            //Add ages
            if (audience.AgeRanges.Count > 0)
            {
                AddOrderTargetAgeGroup(orderId, audience.AgeRanges);
            }
            //Add Genders
            if (audience.Genders.Count > 0)
            {
                AddOrderTargetGender(orderId, audience.Genders);
            }
            //Add Parentage
            if (audience.ParentalStatuses.Count > 0)
            {
                AddOrderTargetParentalStatus(orderId, audience.ParentalStatuses);
            }

            //Add Income
            if (audience.HouseholdIncomes.Count > 0)
            {
                AddOrderTargetHouseholdIncome(orderId, audience.HouseholdIncomes);
            }
            //Add Urls
            if (!String.IsNullOrEmpty(audience.CompetitorUrls))
            {
                AddOrderTargetCompetitorsUrls(orderId, audience.CompetitorUrls);
            }
            //Add Notes
            if (!String.IsNullOrEmpty(audience.Notes))
            {
                AddOrderTargetingNotes(orderId, audience.Notes);
            }
            //Add KeyWords
            if (!String.IsNullOrEmpty(audience.KeyWords))
            {
                AddOrderTargetingKeyWords(orderId, audience.KeyWords);
            }
        }

        public void RemoveAudienceObjects(Guid orderId)
        {
            DeleteOrderTargetAgeGroup(orderId);
            DeleteOrderTargetGender(orderId);
            DeleteOrderTargetParentalStatus(orderId);
            DeleteOrderTargetHouseholdIncome(orderId);
            DeleteOrderTargetCompetitorsUrls(orderId);
            DeleteOrderTargetingNotes(orderId);
            DeleteOrderTargetingKeyWords(orderId);

        }


        public Audience GetOrderAudience(Guid orderId)
        {
            var audience = new Audience
            {
                AgeRanges = _tvDbRepository.GetOrderAgeRanges(orderId),
                CompetitorUrls = _tvDbRepository.GetOrdersCompetitorsUrls(orderId),
                Genders = _tvDbRepository.GetOrdersGenders(orderId),
                HouseholdIncomes = _tvDbRepository.GetOrdersHouseholdIncome(orderId),
                KeyWords = _tvDbRepository.GetOrdersKeywords(orderId),
                Notes = _tvDbRepository.GetOrderNotes(orderId),
                ParentalStatuses = _tvDbRepository.GetOrderParentalStatus(orderId)
            };

            return audience;
        }

        public AudienceExtended GetOrderAudienceExtended(Guid orderId)
        {
            var audience = new AudienceExtended
            {
                AgeRanges = _tvDbRepository.GetOrderAgeRangesExtended(orderId),
                CompetitorUrls = _tvDbRepository.GetOrdersCompetitorsUrls(orderId),
                Genders = _tvDbRepository.GetOrdersGendersExtended(orderId),
                HouseholdIncomes = _tvDbRepository.GetOrdersHouseholdIncomeExtended(orderId),
                KeyWords = _tvDbRepository.GetOrdersKeywords(orderId),
                Notes = _tvDbRepository.GetOrderNotes(orderId),
                ParentalStatuses = _tvDbRepository.GetOrderParentalStatusExtended(orderId)
            };

            return audience;
        }

        public void UpdateAudienceData(Guid orderId, Audience audience)
        {
            RemoveAudienceObjects(orderId);
            AddAudienceObjects(orderId, audience);
        }
    }
}