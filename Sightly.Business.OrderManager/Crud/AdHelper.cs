﻿using System;
using System.Collections.Generic;
using Sightly.Business.OrderManager.Crud.Interfaces;
using Sightly.Models.tv;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.OrderManager.Crud
{
    public class AdHelper : IAdHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public AdHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public void CreateProposedOrderAd(
            Guid orderId, 
            int selectedAdFormat, 
            string adNameInput, 
            string youtubeUrlInput,
            string clickableUrlInput, 
            string landingUrlInput, 
            string campanionBannerUrl, 
            bool pause)
        {
            _tvDbRepository.CreateProposedOrderAd(
                orderId, 
                selectedAdFormat, 
                adNameInput, 
                youtubeUrlInput, 
                clickableUrlInput, 
                landingUrlInput,
                campanionBannerUrl, 
                pause);
        }

        public void CreateProposedOrderAd(Guid orderId, Ad ad)
        {
            _tvDbRepository.CreateProposedOrderAd(
                orderId,
                ad.AdFormat,
                ad.AdName,
                ad.YoutubeUrl,
                ad.ClickableUrl,
                ad.LandingUrl,
                ad.CampanionBannerUrl,
                ad.Pause);
        }

        public List<Ad> GetOrderAds(Guid orderId)
        {
            return _tvDbRepository.GetOrderAds(orderId);
        }
    }
}