﻿using System;
using Sightly.Business.OrderManager.Crud.Interfaces;
using Sightly.Models;
using Sightly.Models.tv;
using Sightly.Models.tv.extended;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.OrderManager.Crud
{
    public class InfoHelper : IInfoHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public InfoHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public Info CheckExistanceOfOrderByName(string orderName)
        {
            var order = _tvDbRepository.GetOrderDataByName(orderName);
            return MapOrderToInfo(order);
        }

        public Info CreateOrderInfo(
            string orderName, 
            string orderRefCode,
            Guid accountId, 
            Guid advertiserId, 
            Guid campaignId, 
            decimal campaignBudget,
            Guid? ObjectiveId,
            DateTime startDate, 
            DateTime endDate)
        {
            var order = new TvOrder
            {
                OrderId = Guid.NewGuid(),
                AccountId = accountId,
                AdvertiserId = advertiserId,
                CampaignId = campaignId,
                OrderName = orderName,
                OrderRefCode = orderRefCode,
                CampaignBudget = campaignBudget,
                StartDate = startDate,
                EndDate = endDate,
                ObjectiveCategoryId = ObjectiveId,
                OrderStatusId = Guid.Parse("1FE1D4D8-48ED-4E89-9787-88784B090A78") // Submitted status
            };
            _tvDbRepository.InsertOrder(order);

            return MapOrderToInfo(order);
        }

        public Info CreateOrderInfo(string orderName, string orderRefCode, Guid account, Guid advertiser, Guid campaign,
            decimal totalBudget, Guid? objective, DateTime startDate, DateTime endDate, bool paused, Guid? parentOrderId)
        {
            var order = new TvOrder
            {
                OrderId = Guid.NewGuid(),
                AccountId = account,
                AdvertiserId = advertiser,
                CampaignId = campaign,
                OrderName = orderName,
                OrderRefCode = orderRefCode,
                CampaignBudget = totalBudget,
                StartDate = startDate,
                EndDate = endDate,
                ObjectiveCategoryId = objective,
                OrderStatusId = Guid.Parse("1FE1D4D8-48ED-4E89-9787-88784B090A78"), // ready status
                Paused = paused,
                ParentOrderId = parentOrderId.Value
            };
            _tvDbRepository.InsertOrderWithParent(order);

            return MapOrderToInfo(order);
        }

        public Info GetOrderInfo(Guid orderId)
        {
            return _tvDbRepository.GetOrderInfoByOrderId(orderId);
        }

        public InfoExtended GetOrderInfoExtended(Guid orderId)
        {
            return _tvDbRepository.GetOrderInfoExtendedByOrderId(orderId);
        }

        private Info MapOrderToInfo(TvOrder order)
        {
            return new Info
            {
                Account = order.AccountId,
                Advertiser = order.AdvertiserId,
                Campaign = order.CampaignId,
                EndDate = order.EndDate,            
                Objective = order.ObjectiveCategoryId,
                OrderId = order.OrderId,
                OrderName = order.OrderName,
                StartDate = order.StartDate,
                TotalBudget = order.CampaignBudget
            };
        }
    }
}