﻿using System;
using System.Collections.Generic;
using Sightly.Models.tv;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.OrderManager.Crud.Interfaces
{
    public class NewOrderBlobHelper : INewOrderBlobHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public NewOrderBlobHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public string GetNewOrderBlobById(int id)
        {
            return _tvDbRepository.GetNewOrderBlobById(id);
        }

        public int SaveNewOrderBlob(string orderName, string orderRefCode, string orderJson, DateTime startDate, DateTime endDate,
            string accountName, string advertiserName, Guid advertiser, string campaignName, Guid campaign,
            decimal totalBudget)
        {
            return _tvDbRepository.SaveNewOrderBlob(
                orderName, 
                orderRefCode, 
                orderJson, 
                startDate, 
                endDate,
                accountName,
                advertiserName,
                advertiser,
                campaignName,
                campaign,
                totalBudget);
        }

        public int SaveNewOrderBlob(string orderName, string orderRefCode, string orderJson, DateTime startDate, DateTime endDate)
        {
            return _tvDbRepository.SaveNewOrderBlob(orderName, orderRefCode, orderJson, startDate, endDate);
        }

        public List<Order> SearchNewOrderBlob(string orderName, string orderRefCode, DateTime? startDate, DateTime? endDate)
        {
            return _tvDbRepository.SearchNewOrderBlob(orderName, orderRefCode, startDate, endDate);
        }
    }
}