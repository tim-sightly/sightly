﻿using System;
using Sightly.Models.tv;

namespace Sightly.Business.OrderManager.Crud.Interfaces
{
    public interface ICampaignHelper
    {
        Campaign CheckExistanceOfCampaignByName(string campaignName);
        Campaign CreateCampaignIfNotExisting(Guid AdvertiserId, string campaignName);
        Campaign GetCampaignByCampaignId(Guid campaignId);
    }
}
