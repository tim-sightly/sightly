﻿using System;
using System.Collections.Generic;
using Sightly.Models.tv;

namespace Sightly.Business.OrderManager.Crud.Interfaces
{
    public interface IAdHelper
    {
        void CreateProposedOrderAd(Guid orderId, Ad ad);
        List<Ad> GetOrderAds(Guid orderId);
    }
}