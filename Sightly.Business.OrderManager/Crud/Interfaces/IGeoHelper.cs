﻿using System;
using System.Collections.Generic;
using Sightly.Models;
using Sightly.Models.tv;

namespace Sightly.Business.OrderManager.Crud.Interfaces
{
    public interface IGeoHelper
    {
        bool AssociateGeographyToOrder(Guid orderId, List<GeoData> geographies);
        Geo GetOrderGeo(Guid orderId);
        void UpdateLocationData(Guid orderId, Geo geo);
        void AssociateNewOrderIdToParentWithBudgetGroup(Guid orderId, Guid parentOrderId);
    }
}