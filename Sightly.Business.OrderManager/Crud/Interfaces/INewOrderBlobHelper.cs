﻿using System;
using System.Collections.Generic;
using Sightly.Models.tv;

namespace Sightly.Business.OrderManager.Crud.Interfaces
{
    public interface INewOrderBlobHelper
    {
        string GetNewOrderBlobById(int id);
        int SaveNewOrderBlob(string orderName, string orderRefCode, string orderJson, DateTime startDate, DateTime endDate, string accountName, string advertiserName, Guid advertiser, string campaignName, Guid campaign, decimal totalBudget);
        List<Order> SearchNewOrderBlob(string orderName, string orderRefCode, DateTime? startDate, DateTime? endDate);
    }
}