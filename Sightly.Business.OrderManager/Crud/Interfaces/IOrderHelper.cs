﻿using System;
using System.Collections.Generic;
using Sightly.Models;
using Sightly.Models.tv;

namespace Sightly.Business.OrderManager.Crud.Interfaces
{
    public interface IOrderHelper
    {
        List<OrderLite> SearchOrderLiteByString(string searchValue, Guid userId);
        bool SetOrderPausedStatus(Guid orderId);
        void SaveOrderVersionForHistory(Order order, Guid userId);
        void UpdateOrderInfoData(Guid orderId, Info orderInfo);
        bool SetOrderCancelled(Guid orderId);
        OrderPauseData GetOrderPauseDataByOrderId(Guid orderId);
        OrderCancelData GetOrderCancelDataByOrderId(Guid orderId);
    }
}

