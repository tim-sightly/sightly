﻿using System;
using Sightly.Models.tv;
using Sightly.Models.tv.extended;

namespace Sightly.Business.OrderManager.Crud.Interfaces
{
    public interface IInfoHelper
    {
        Info CheckExistanceOfOrderByName(string orderName);
        Info CreateOrderInfo(
            string orderName,
            string orderRefCode,
            Guid accountId,
            Guid advertiserId,
            Guid campaignId,
            decimal campaignBudget,
            Guid? ObjectiveId,
            DateTime startDate,
            DateTime endDate);

        Info CreateOrderInfo(
            string orderName, 
            string orderRefCode, 
            Guid account, 
            Guid advertiser, 
            Guid campaign, 
            decimal totalBudget, 
            Guid? objective, 
            DateTime startDate, 
            DateTime endDate, 
            bool paused, 
            Guid? parentOrderId);


        Info GetOrderInfo(Guid orderId);
        InfoExtended GetOrderInfoExtended(Guid orderId);
    }
}