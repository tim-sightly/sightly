﻿using System;
using System.Collections.Generic;
using Sightly.Models.tv;
using Sightly.Models.tv.extended;

namespace Sightly.Business.OrderManager.Crud.Interfaces
{
    public interface IAudienceHelper
    {
        void AddOrderTargetAgeGroup(Guid orderId, List<Guid> ageGroups);
        void AddOrderTargetGender(Guid orderId, List<Guid> genderGroups);
        void AddOrderTargetParentalStatus(Guid orderId, List<Guid> parentalStatus);
        void AddOrderTargetCompetitorsUrls(Guid orderId, string competitorsUrls);
        void AddOrderTargetHouseholdIncome(Guid orderId, List<Int16> householdIncomes);
        void AddOrderTargetingNotes(Guid orderId, string notes);
        void AddAudienceObjects(Guid orderId, Audience audience);

        Audience GetOrderAudience(Guid orderId);
        void UpdateAudienceData(Guid orderId, Audience audience);
        AudienceExtended GetOrderAudienceExtended(Guid orderId);
    }
}