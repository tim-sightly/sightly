﻿using System;
using Sightly.Models;
using Sightly.Models.tv;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Business.OrderManager.Crud.Interfaces;

namespace Sightly.Business.OrderManager.Crud
{
    public class CampaignHelper : ICampaignHelper
    {
        private readonly ITvDbRepository _tvDbRepository;

        public CampaignHelper(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public Campaign CheckExistanceOfCampaignByName(string campaignName)
        {
            var campaign = _tvDbRepository.GetCampaignDataByName(campaignName);
            if(campaign == null) return new Campaign();
            return MapCampagin(campaign);
        }

        public Campaign CreateCampaignIfNotExisting(Guid AdvertiserId, string campaignName)
        {
            throw new NotImplementedException();
        }

        public Campaign GetCampaignByCampaignId(Guid campaignId)
        {
            var campaign =  _tvDbRepository.GetCampaignByCampaignId(campaignId);
            return MapCampagin(campaign);
        }

        private Campaign MapCampagin(TvCampaign tvCampaign)
        {
            return new Campaign
            {
                AccountId = tvCampaign.AccountId,
                AdvertiserId = tvCampaign.AdvertiserId,
                CampaignId = tvCampaign.CampaignId,
                CampaignName = tvCampaign.CampaignName
            };
        }
    }
}