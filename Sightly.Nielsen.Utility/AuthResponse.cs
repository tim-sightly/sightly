﻿using Newtonsoft.Json;

namespace Sightly.Nielsen.Utility
{
    public class AuthResponse
    {
        #region Public Properties
        [JsonProperty("token_type")]
        public string TokenType { get; set; }

        [JsonProperty("expires_in")]
        public string ExpiresIn { get; set; }

        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }

        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        #endregion Public Properties
    }
}
