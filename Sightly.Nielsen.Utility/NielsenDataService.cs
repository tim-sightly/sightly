﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Sightly.Models;

namespace Sightly.Nielsen.Utility
{
    public class NielsenDataService : INielsenDataService
    {
        public async Task<NielsenCombinedData> GetNielsenData(List<NielsenCampaignReference> campaignReferences, DateTime statDate)
        {
            var nielsenCombinedData = new NielsenCombinedData();
            var nielsenSettings = GetNielsenSettings();
            var formattedEndDate = statDate.ToString("MM/dd/yyyy");

            //Authenticate login
            await API.Nielsen.GetAuthenticationToken(nielsenSettings);
            if (string.IsNullOrEmpty(nielsenSettings.AccessToken))
            {
                throw new AuthenticationException();
            }

            List<Task<IEnumerable<NielsenCampaignPlacementExposure>>> statsTasks = new List<Task<IEnumerable<NielsenCampaignPlacementExposure>>>();
            campaignReferences.ForEach(campaign =>
            {
                //Check campaign against the SiteReference and find the Sightly.com placements
                Console.WriteLine($"Checking CampaignSiteReferenc for {campaign.CampaignId} - {campaign.CampaignName}");

                statsTasks.Add(GetCampaignPlacementExposureByDate(
                    nielsenSettings, 
                    campaign.CampaignId.ToString(), 
                    formattedEndDate));
            });
            

            while (statsTasks.Count > 0)
            {
                int index = Task.WaitAny(statsTasks.ToArray());
                try
                {
                    var campStats = statsTasks[index].Result.ToList();
                    //get the SiteReferences to exclude non-sightly results
                    List<NielsenCampaignSiteReference> campSites = GetCampaignSiteReferenceByDate(
                        nielsenSettings,
                        campStats.First().CampaignId.ToString(), 
                        formattedEndDate).Result;
                    var sightlyPlacements = campSites.Where(x => x.SiteURL == "www.sightly.com").ToList();
                    nielsenCombinedData.NielsenStatsExposure.AddRange(campStats.Where(cs => sightlyPlacements.Any(sp => Convert.ToInt64(cs.PlacementId) == sp.PlacementId)));
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    statsTasks.RemoveAt(index);
                }
                statsTasks.RemoveAt(index);
            }

            return nielsenCombinedData;
        }

        public async Task<NielsenCombinedData> GetNielsenData(long customerId, DateTime statDate)
        {
            var nielsenCombinedData = new NielsenCombinedData();
            var nielsenSettings = GetNielsenSettings();
            var formattedEndDate = statDate.ToString("MM/dd/yyyy");

            //Authenticate login
            await API.Nielsen.GetAuthenticationToken(nielsenSettings);
            if (string.IsNullOrEmpty(nielsenSettings.AccessToken))
            {
                throw new AuthenticationException();
            }

            List<Task<IEnumerable<NielsenCampaignPlacementExposure>>> statsTasks = new List<Task<IEnumerable<NielsenCampaignPlacementExposure>>>();
            statsTasks.Add(GetCampaignPlacementExposureByDate(nielsenSettings, customerId.ToString(), formattedEndDate));
            

            while (statsTasks.Count > 0)
            {
                int index = Task.WaitAny(statsTasks.ToArray());
                try
                {
                    nielsenCombinedData.NielsenStatsExposure.AddRange(statsTasks[index].Result);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                }
                statsTasks.RemoveAt(index);
            }

            return nielsenCombinedData;
        }

        public async Task<List<NielsenCampaignReference>> GetNielsenCampaignReferenceData(DateTime statDate)
        {
            var nielsenSettings = GetNielsenSettings();

            //Authenticate login
            await API.Nielsen.GetAuthenticationToken(nielsenSettings);
            if (string.IsNullOrEmpty(nielsenSettings.AccessToken))
            {
                throw new AuthenticationException();
            }
            return GetNielsenCampaignReferenceData(nielsenSettings, statDate).Result;
        }

        public async Task<List<NielsenCampaignReference>> GetNielsenCampaignReferenceData(NielsenSettings nielsenSettings, DateTime statDate)
        {

            var formattedEndDate = statDate.ToString("MM/dd/yyyy");
            Console.WriteLine($"Executing DownloadCampaignAvailabilityData for the report date {formattedEndDate}");
            List<NielsenCampaignAvailability> campaignList = await DownloadCampaignAvailabilityData(nielsenSettings, formattedEndDate);

            List<NielsenCampaignAvailability> campaignsToProcess = campaignList.Where(c => c.DataReleaseId == "1" && c.ReportStatus != "Re-Stated").ToList();
            Console.WriteLine($"Executing GetCampaignReference for the report date {formattedEndDate}");
            List<NielsenCampaignReference> campaignReferenceList = await GetCampaignReference(nielsenSettings, campaignsToProcess, formattedEndDate);

            return campaignReferenceList;
        }

        private static NielsenSettings GetNielsenSettings()
        {
            var nielsenSettings = new NielsenSettings
            {
                ApiKey = "RUZOWTFnaTMyNWEzM0dmd0FqS2F5Um9PYTh0UmNYN2I6RE80bTBXQjZMd0xlOWtCaA==",
                AuthTokenUrl = "https://api.developer.nielsen.com/watch/oauth/token",
                CampaignDataAvailabilityUrl =
                    "https://api.developer.nielsen.com/watch/dar/campaignratings/v2/CampaignDataAvailability",
                CampaignPlacementExpCustomRangeUrl =
                    "https://api.developer.nielsen.com/watch/dar/campaignratings/v3/CampaignPlacementExpCustomRange",
                CampaignReferenceUrl = "https://api.developer.nielsen.com/watch/dar/campaignratings/v3/CampaignReference",
                CampaignSiteReferenceUrl = "https://api.developer.nielsen.com/watch/dar/campaignratings/v3/CampaignSiteReference",
                CountryCode = "US",
                Password = "S1@htLyAp1",
                Username = "apiuser.sightly@sightly.com"
                //PlatformReferenceUrl = "https://api.developer.nielsen.com/watch/dar/campaignratings/v1/PlatformReference",
                //DemographicReferenceUrl = "https://api.developer.nielsen.com/watch/dar/campaignratings/v1/DemographicReference",
            };
            return nielsenSettings;
        }

        private async Task<IEnumerable<NielsenCampaignPlacementExposure>> GetCampaignPlacementExposureByDate(NielsenSettings nielsenSettings, string campaignId, string formattedEndDate)
        {
            Console.WriteLine($"Stats for {campaignId}");
            List<NielsenCampaignPlacementExposure> campaignPlacementExposureList = await API.Nielsen.GetCampaignPlacementExposureCustomRangeData(nielsenSettings, campaignId, formattedEndDate, formattedEndDate);
            return campaignPlacementExposureList;
        }

        private async Task<List<NielsenCampaignAvailability>> DownloadCampaignAvailabilityData(NielsenSettings nielsenSettings, string formattedEndDate)
        {
            List<NielsenCampaignAvailability> campaignAvailabilityList = await API.Nielsen.GetCampaignDataAvailability(nielsenSettings, formattedEndDate);
            if (campaignAvailabilityList == null || campaignAvailabilityList.Count == 0)
                return new List<NielsenCampaignAvailability>();
            else
            {
                return campaignAvailabilityList;
            }
        }

        private static async Task<List<NielsenCampaignSiteReference>> GetCampaignSiteReferenceByDate(NielsenSettings nielsenSettings, string campaignId, string reportDate)
        {
            List<NielsenCampaignSiteReference> campaignSiteReferenceList = await API.Nielsen.GetCampaignSiteReferenceData(nielsenSettings, campaignId, reportDate);
            return campaignSiteReferenceList;
        }

        private static async Task<List<NielsenCampaignReference>> GetCampaignReference(NielsenSettings nielsenSettings, List<NielsenCampaignAvailability> campaignsToProcess, string formattedEndDate)
        {
            var concurrentStack = new ConcurrentStack<NielsenCampaignReference>();
            var block = new ActionBlock<NielsenCampaignAvailability>(async campaignAvailability =>
            {
                NielsenCampaignReference campaignReference = await API.Nielsen.GetCampaignReferenceData(nielsenSettings, campaignAvailability.CampaignId, formattedEndDate);
                if (campaignReference.CampaignId != 0)
                    concurrentStack.Push(campaignReference);

            }, new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = 32 });

            foreach (NielsenCampaignAvailability campaign in campaignsToProcess)
            {
                block.Post(campaign);
            }

            block.Complete();
            block.Completion.Wait();
            return concurrentStack.ToList();
        }
    }
}