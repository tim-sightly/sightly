﻿using Newtonsoft.Json;

namespace Sightly.Nielsen.Utility
{
    public class NielsenSettings
    {
        #region Public Properties
        [JsonProperty("Password")]
        public string Password { get; set; }

        [JsonProperty("Username")]
        public string Username { get; set; }

        [JsonProperty("ApiKey")]
        public string ApiKey { get; set; }

        [JsonProperty("AuthTokenUrl")]
        public string AuthTokenUrl { get; set; }
        
        [JsonProperty("CampaignDataAvailabilityUrl")]
        public string CampaignDataAvailabilityUrl { get; set; }

        [JsonProperty("CampaignReferenceUrl")]
        public string CampaignReferenceUrl { get; set; }
        
        [JsonProperty("CampaignPlacementExpCustomRangeUrl")]
        public string CampaignPlacementExpCustomRangeUrl { get; set; }

        [JsonProperty("CampaignSiteReferenceUrl")]
        public string CampaignSiteReferenceUrl { get; set; }

        [JsonProperty("CountryCode")]
        public string CountryCode { get; set; }
        
        public string AccessToken { get; set; } = null;
        #endregion Public Properties
    }
}
