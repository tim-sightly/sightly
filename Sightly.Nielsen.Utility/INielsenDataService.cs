﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Sightly.Models;

namespace Sightly.Nielsen.Utility
{
    public interface INielsenDataService
    {
        Task<NielsenCombinedData> GetNielsenData(List<NielsenCampaignReference> campaignReferences, DateTime statDate); 
        Task<NielsenCombinedData> GetNielsenData(long customerId, DateTime statDate);

        Task<List<NielsenCampaignReference>> GetNielsenCampaignReferenceData(DateTime statdate);
        Task<List<NielsenCampaignReference>> GetNielsenCampaignReferenceData(NielsenSettings nielsenSettings, DateTime statdate);
    }
}
