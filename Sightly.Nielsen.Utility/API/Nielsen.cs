﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Sightly.Models;
using Sightly.Tools;

namespace Sightly.Nielsen.Utility.API
{
    public class Nielsen
    {
        #region Authentication
        public static async Task GetAuthenticationToken(NielsenSettings nielsenSettings)
        {
            using (var client = new HttpClient())
            {
                var message = new HttpRequestMessage(new HttpMethod(method: "POST"), requestUri: nielsenSettings.AuthTokenUrl);
                message.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType: "application/x-www-form-urlencoded"));
                message.Headers.Authorization = new AuthenticationHeaderValue(scheme: "Basic", parameter: nielsenSettings.ApiKey);
                var nameValueCollection = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>(key: "grant_type", value: "password"),
                    new KeyValuePair<string, string>(key: "username", value: nielsenSettings.Username),
                    new KeyValuePair<string, string>(key: "password", value: nielsenSettings.Password)
                };

                message.Content = new FormUrlEncodedContent(nameValueCollection);
                try
                {
                    HttpResponseMessage httpResponseMessage = await client.SendAsync(message);
                    httpResponseMessage.EnsureSuccessStatusCode();
                    HttpContent httpContent = httpResponseMessage.Content;
                    string responseString = await httpContent.ReadAsStringAsync();
                    AuthResponse authResponse = JsonConvert.DeserializeObject<AuthResponse>(responseString);
                    nielsenSettings.AccessToken = authResponse.AccessToken;
                }
                catch (Exception ex)
                {
                    throw ExceptionUtility.ThrowException(ex);
                }
            }
        }
        #endregion Authentication
        
        #region CampaignAvailability
        
        public static async Task<List<NielsenCampaignAvailability>> GetCampaignDataAvailability(NielsenSettings nielsenSettings, string reportDate)
        {
            using (var client = new HttpClient())
            {
                var message = new HttpRequestMessage(new HttpMethod(method: "POST"), requestUri: nielsenSettings.CampaignDataAvailabilityUrl);
                message.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType: "application/json"));
                message.Headers.Authorization = new AuthenticationHeaderValue(scheme: "Bearer", parameter: nielsenSettings.AccessToken);

                var nameValueCollection = new List<KeyValuePair<string, string>>();
                nameValueCollection.Add(new KeyValuePair<string, string>(key: "date", value: reportDate));
                nameValueCollection.Add(new KeyValuePair<string, string>(key: "countryCode", value: nielsenSettings.CountryCode));

                message.Content = new FormUrlEncodedContent(nameValueCollection);
                try
                {
                    HttpResponseMessage httpResponseMessage = await client.SendAsync(message);
                    httpResponseMessage.EnsureSuccessStatusCode();
                    HttpContent httpContent = httpResponseMessage.Content;
                    string responseString = await httpContent.ReadAsStringAsync();
                    List<NielsenCampaignAvailability> campaignAvailabilityList = JsonConvert.DeserializeObject<List<NielsenCampaignAvailability>>(responseString);
                    return campaignAvailabilityList;
                }
                catch (Exception ex)
                {
                    throw ExceptionUtility.ThrowException(ex);
                }
            }
        }
        #endregion CampaignAvailability

        #region NielsenCampaignReference
        public static async Task<NielsenCampaignReference> GetCampaignReferenceData(NielsenSettings nielsenSettings, string campaignId, string date, string childCampaignId = null)
        {
            using (var client = new HttpClient())
            {
                client.Timeout = new TimeSpan(0, 15, 0);

                var campaingReferenceDictionary = new Dictionary<string, NielsenCampaignReference>();
                var message = new HttpRequestMessage(new HttpMethod(method: "POST"), requestUri: nielsenSettings.CampaignReferenceUrl);
                message.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType: "application/json"));
                message.Headers.Authorization = new AuthenticationHeaderValue(scheme: "Bearer", parameter: nielsenSettings.AccessToken);
                var nameValueCollection = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>(key: "date", value: date),
                    new KeyValuePair<string, string>(key: "countryCode", value: nielsenSettings.CountryCode),
                    new KeyValuePair<string, string>(key: "campaignId", value: campaignId)
                };

                message.Content = new FormUrlEncodedContent(nameValueCollection);
                try
                {
                    HttpResponseMessage httpResponseMessage = await client.SendAsync(message);
                    httpResponseMessage.EnsureSuccessStatusCode();
                    HttpContent httpContent = httpResponseMessage.Content;
                    string responseString = await httpContent.ReadAsStringAsync();
                    if (responseString == "")
                        return new NielsenCampaignReference();

                    List<NielsenCampaignReference> campaignReferenceList = JsonConvert.DeserializeObject<List<NielsenCampaignReference>>(responseString);
                    if (campaignReferenceList.Count == 1)
                    {
                        NielsenCampaignReference nielsenCampaignReference = campaignReferenceList[0];
                        
                        return nielsenCampaignReference;
                    }
                    else
                    {
                        throw ExceptionUtility.ThrowException(new Exception(string.Format(format: "Campaing Id: {0} error in getting campaign reference object.", arg0: campaignId)));
                    }
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("Unauthorized"))
                        return new NielsenCampaignReference();

                    throw ExceptionUtility.ThrowException(ex);
                }
            }
        }
        #endregion NielsenCampaignReference
        

        #region CampaignPlacementExposureCustomRange
        public static async Task<List<NielsenCampaignPlacementExposure>> GetCampaignPlacementExposureCustomRangeData(NielsenSettings nielsenSettings, string campaignId, string startDateString, string endDateString)
        {
            using (var client = new HttpClient())
            {
                client.Timeout = new TimeSpan(0, 1, 0);

                var message = new HttpRequestMessage(new HttpMethod(method: "POST"), requestUri: nielsenSettings.CampaignPlacementExpCustomRangeUrl);
                message.Headers.Authorization = new AuthenticationHeaderValue(scheme: "Bearer", parameter: nielsenSettings.AccessToken);
                message.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType: "application/json"));

                var nameValueCollection = new List<KeyValuePair<string, string>>
                {
                    new KeyValuePair<string, string>(key: "countryCode", value: nielsenSettings.CountryCode),
                    new KeyValuePair<string, string>(key: "campaignId", value: campaignId),
                    new KeyValuePair<string, string>(key: "startDate", value: startDateString),
                    new KeyValuePair<string, string>(key: "endDate", value: endDateString)
                };
                message.Content = new FormUrlEncodedContent(nameValueCollection);

                try
                {
                    HttpResponseMessage httpResponseMessage = await client.SendAsync(message);
                    httpResponseMessage.EnsureSuccessStatusCode();
                    HttpContent httpContent = httpResponseMessage.Content;
                    string responseString = await httpContent.ReadAsStringAsync();

                    if (responseString == "")
                        return new List<NielsenCampaignPlacementExposure>();

                    List<NielsenCampaignPlacementExposure> campaignPlacementExposureList = JsonConvert.DeserializeObject<List<NielsenCampaignPlacementExposure>>(responseString);
                    return campaignPlacementExposureList;
                }
                catch (Exception ex)
                {
                    throw ExceptionUtility.ThrowException(ex);
                }
            }
        }
        #endregion CampaignPlacementExposureCustomRange

        #region CampaignSiteReference
        public static async Task<List<NielsenCampaignSiteReference>> GetCampaignSiteReferenceData(NielsenSettings nielsenSettings, string campaignId, string reportDate)
        {
            using (var client = new HttpClient())
            {
                var message = new HttpRequestMessage(new HttpMethod(method: "POST"), requestUri: nielsenSettings.CampaignSiteReferenceUrl);
                message.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue(mediaType: "application/json"));
                message.Headers.Authorization = new AuthenticationHeaderValue(scheme: "Bearer", parameter: nielsenSettings.AccessToken);

                var nameValueCollection = new List<KeyValuePair<string, string>>();
                nameValueCollection.Add(new KeyValuePair<string, string>(key: "date", value: reportDate));
                nameValueCollection.Add(new KeyValuePair<string, string>(key: "campaignId", value: campaignId));
                nameValueCollection.Add(new KeyValuePair<string, string>(key: "countryCode", value: nielsenSettings.CountryCode));
                message.Content = new FormUrlEncodedContent(nameValueCollection);

                try
                {
                    HttpResponseMessage httpResponseMessage = await client.SendAsync(message);
                    httpResponseMessage.EnsureSuccessStatusCode();
                    HttpContent httpContent = httpResponseMessage.Content;
                    string responseString = await httpContent.ReadAsStringAsync();

                    if (responseString == "")
                        return new List<NielsenCampaignSiteReference>();


                    List<NielsenCampaignSiteReference> campaignSiteReferenceList = JsonConvert.DeserializeObject<List<NielsenCampaignSiteReference>>(responseString);
                    return campaignSiteReferenceList;
                }
                catch (Exception ex)
                {
                    throw ExceptionUtility.ThrowException(ex);
                }
            }
        }
        #endregion CampaignSiteReference
    }
}
