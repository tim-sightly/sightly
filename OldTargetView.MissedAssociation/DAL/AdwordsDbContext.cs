namespace OldTargetView.MissedAssociation.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class AdwordsDbContext : DbContext
    {
        public AdwordsDbContext()
            : base("name=AdwordsDb")
        {
        }

        public virtual DbSet<AccountMcc> AccountMccs { get; set; }
        public virtual DbSet<AdGroup> AdGroups { get; set; }
        public virtual DbSet<AdGroupAd> AdGroupAds { get; set; }
        public virtual DbSet<AdWordsCampaign> AdWordsCampaigns { get; set; }
        public virtual DbSet<Customer> Customers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AdGroup>()
                .Property(e => e.Name)
                .IsUnicode(false);
        }
    }
}
