namespace OldTargetView.MissedAssociation.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AccountMcc")]
    public partial class AccountMcc
    {
        [Key]
        public Guid AccountId { get; set; }

        public long AdWordsMccId { get; set; }

        public bool IsActive { get; set; }

        public DateTime CreateDate { get; set; }
    }
}
