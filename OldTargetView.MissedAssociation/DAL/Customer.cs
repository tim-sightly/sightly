namespace OldTargetView.MissedAssociation.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Customer")]
    public partial class Customer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long CustomerId { get; set; }

        public Guid AccountId { get; set; }

        public bool IsPrimary { get; set; }

        public long? ObfuscatedCustomerId { get; set; }

        public Guid LocationId { get; set; }

        public Guid ClientId { get; set; }
    }
}
