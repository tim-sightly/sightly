namespace OldTargetView.MissedAssociation.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("AdGroup")]
    public partial class AdGroup
    {
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long AdGroupId { get; set; }

        public Guid CampaignId { get; set; }

        public Guid LocationId { get; set; }

        public Guid CampaignAudienceId { get; set; }

        public Guid CampaignAudienceSegmentId { get; set; }

        public long CustomerId { get; set; }

        public long AdWordsCampaignId { get; set; }

        [Required]
        [StringLength(500)]
        public string Name { get; set; }

        [Column(TypeName = "date")]
        public DateTime FromDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime ToDate { get; set; }

        public bool Ignore { get; set; }
    }
}
