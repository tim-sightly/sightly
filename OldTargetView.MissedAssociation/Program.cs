﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Sightly.Adwords.Utility.Interface;
using Sightly.IOC;
using Sightly.Models;
using Sightly.TargetView.AdwordsValidation;
using Sightly.TargetView.AdwordsValidation.Interfaces;
using Sightly.Tools;

namespace OldTargetView.MissedAssociation
{
    class Program
    {
        static void Main(string[] args)
        {
            InitializeDi();

            var mccCustomers = new Dictionary<long, string>{
		        { long.Parse("3017157839"), "Advance Digital (LVL)" },
                { long.Parse("9660276423"), "Advance Digital (AO)" },
                { long.Parse("7978460615"), "Advance Digital (MMG)" },
                { long.Parse("9786528386"), "Advance Digital (SMG)" },
                { long.Parse("7929513994"), "Advance Digital (AMG)" },
                { long.Parse("7528459433"), "Advance Digital (OMG)" },
                { long.Parse("7742181127"), "Advance Digital (NJAM)" },
                { long.Parse("8158057652"), "Advance Digital (MassLive)" },
                { long.Parse("5570545417"), "Advance Digital (PAMG)" },
		       // { long.Parse("3554989310"), "Advanced Digital MCC" },
		        
	        };
            // var statDate = DateTime.Parse("2016-10-08"); 
            var statDate = DateTime.Now.AddDays(-1);


            var dailyStats = GetDailyCampaignStats(statDate, mccCustomers);
            
            CsvUtility.CreateCsvFromGenericList(dailyStats, string.Format("DailyCheck_{0}.csv", statDate.ToString("u")));

        }

        public static List<AwCampaignOrder> GetDailyCampaignStats(DateTime statDate, Dictionary<long, string> customers)
        {
            var campOrders = new List<AwCampaignOrder>();

            foreach (var customer in customers)
            {

                var customerHierarchy = _accountHierarchy.GetAdwordsCustomerByMcc(customer.Key);
                var campaignOrders = _targetViewAdwordsValidation.GetCampaignOrderForMccCustomer(statDate,
                                                                                                     customerHierarchy.First().ChildCustomers);
                var awCampaignOrders = campaignOrders as AwCampaignOrder[] ?? campaignOrders.ToArray();
                awCampaignOrders.ToList().ForEach(stat => { stat.AccountName = customer.Value; });
                campOrders.AddRange(awCampaignOrders);
            }

            var context = new DAL.AdwordsDbContext();

            campOrders.ForEach(tvCamp =>
            {
                var awCampaignId =
                    context.AdWordsCampaigns.FirstOrDefault(
                        adwords =>
                            adwords.AdWordsCampaignId == tvCamp.CampaignId && adwords.CustomerId == tvCamp.CustomerId)
                        .CampaignId;
                tvCamp.TvCampaignId = awCampaignId;
            });

            
            return campOrders;
        }


        private static IContainer Container { get; set; }
        private static IAccountHierarchy _accountHierarchy;
        private static ITargetViewAdwordsValidation _targetViewAdwordsValidation;

        public static void InitializeDi()
        {
            var builder = new ContainerBuilder();
            (new AutofacConfiguration()).RegisterServices(builder);
            Container = builder.Build();
            _accountHierarchy = Container.Resolve<IAccountHierarchy>();
            _targetViewAdwordsValidation = Container.Resolve<ITargetViewAdwordsValidation>();
        }
    }
}
