﻿using System;
using System.Collections.Generic;
using Sightly.Business.StatsGather.Raw;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.StatsGather
{
    public class GatherStats : IGatherStats
    {
        private readonly IAdStatsTransform _adStatsTransform;
        private readonly IAgeGroupStatsTransform _ageGroupStatsTransform;
        private readonly IBudgetGroupStatsTransform _budgetGroupStatsTransform;
        private readonly IBudgetGroupAdStatsTransform _budgetGroupAdStatsTransform;
        private readonly IBudgetGroupAgeStatsTransform _budgetGroupAgeStatsTransform;
        private readonly IBudgetGroupDeviceStatsTransform _budgetGroupDeviceStatsTransform;
        private readonly IBudgetGroupGenderStatsTransform _budgetGroupGenderStatsTransform;
        private readonly IDeviceStatsTransform _deviceStatsTransform;
        private readonly IGenderStatsTransform _genderStatsTransform;
        private readonly IOrderStatsTransform _orderStatsTransform;
        private readonly IRawStatsService _rawStatsService;
        private readonly IStatRepository _statRepository;
        public GatherStats(IAdStatsTransform adStatsTransform, 
                           IAgeGroupStatsTransform ageGroupStatsTransform, 
                           IBudgetGroupStatsTransform budgetGroupStatsTransform, 
                           IBudgetGroupAdStatsTransform budgetGroupAdStatsTransform, 
                           IBudgetGroupAgeStatsTransform budgetGroupAgeStatsTransform, 
                           IBudgetGroupDeviceStatsTransform budgetGroupDeviceStatsTransform, 
                           IBudgetGroupGenderStatsTransform budgetGroupGenderStatsTransform, 
                           IDeviceStatsTransform deviceStatsTransform, 
                           IGenderStatsTransform genderStatsTransform, 
                           IOrderStatsTransform orderStatsTransform, 
                           IRawStatsService rawStatsService, 
                           IStatRepository statRepository)
        {
            _adStatsTransform = adStatsTransform;
            _ageGroupStatsTransform = ageGroupStatsTransform;
            _budgetGroupStatsTransform = budgetGroupStatsTransform;
            _budgetGroupAdStatsTransform = budgetGroupAdStatsTransform;
            _budgetGroupAgeStatsTransform = budgetGroupAgeStatsTransform;
            _budgetGroupDeviceStatsTransform = budgetGroupDeviceStatsTransform;
            _budgetGroupGenderStatsTransform = budgetGroupGenderStatsTransform;
            _deviceStatsTransform = deviceStatsTransform;
            _genderStatsTransform = genderStatsTransform;
            _orderStatsTransform = orderStatsTransform;
            _rawStatsService = rawStatsService;
            _statRepository = statRepository;
        }

        public void Execute(long customerId, DateTime statDate)
        {

            var adDevStats = _rawStatsService.GetAndSaveAdDeviceStats(customerId, statDate);
            var ageStats = _rawStatsService.GetAndSaveAgeRangeStats(customerId, statDate);
            var genderStats = _rawStatsService.GetAndSaveGenderStats(customerId, statDate);


            //ManageAdDevStats(adDevStats);

            //MangeAgeStats(ageStats);

            //ManageGenderStats(genderStats);

        }

        public void RunAdGroupGeoStats(long customerId, DateTime statDate)
        {

            var adGroupStats = _rawStatsService.GetAndSaveAdGroupStats(customerId, statDate);
            var geostats = _rawStatsService.GetAndSaveGeoStats(customerId, statDate);
        }


        public void RunMoat(DateTime statDate)
        {
            var moatStats = _rawStatsService.GetAndSaveMoatStats(statDate);
        }

        public void RunNielsen(List<NielsenCampaignReference> campaignReferences, DateTime statDate)
        {
            var nielseStats = _rawStatsService.GetAndSaveNielsenStats(campaignReferences, statDate);
        }

        public void RunNielsen(long customerId, DateTime statDate)
        {
            var nielseStats = _rawStatsService.GetAndSaveNielsenStatsByCustomerId(customerId, statDate);
        }

        public List<NielsenCampaignReference> VerifyNielsenCampaignStructure(DateTime date)
        {
             return _rawStatsService.VerifyNielsenCampaignStructure(date);
        }

        public void RunNielseDarUpdate(List<long> nielsenCiDs, DateTime yesterday)
        {
            if (nielsenCiDs.Count > 0)
            {
                _rawStatsService.RunNielsenDarUpdate(nielsenCiDs, yesterday);
            }
        }

        public void RunDoubleVerify(List<DoubleVerifyImport> importData)
        {
            var dbData = _rawStatsService.SaveOffDoubleVerify(importData);
        }

        private void ManageGenderStats(List<GenderPerformanceFreeData> genderStats)
        {
            if (genderStats.Count > 0)
            {
                TransformGenderStats(genderStats);
            }
        }

        private void MangeAgeStats(List<AgeRangePerformanceFreeData> ageStats)
        {
            if (ageStats.Count > 0)
            {
                TransformAgeStats(ageStats);
            }
        }

        private void ManageAdDevStats(List<AdDevicePerformanceData> adDevStats)
        {
            if (adDevStats.Count > 0)
            {
                TransformAdStats(adDevStats);

                TransforDeviceStats(adDevStats);

                TransformStats(adDevStats);
            }
        }

        private void TransformGenderStats(List<GenderPerformanceFreeData> genderStats)
        {
            try
            {
                var budgetGroupGenderStats = _budgetGroupGenderStatsTransform.Execute(genderStats);
                _statRepository.InsertBudgetGroupGenderStats(budgetGroupGenderStats);
                var newGenderStats = _genderStatsTransform.Execute(budgetGroupGenderStats);
                _statRepository.InsertGenderStats(newGenderStats);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void TransformAgeStats(List<AgeRangePerformanceFreeData> ageStats)
        {
            try
            {
                var budgetGroupAgeStats = _budgetGroupAgeStatsTransform.Execute(ageStats);
                _statRepository.InsertBudgetGroupAgeStats(budgetGroupAgeStats);
                var ageGroupStats = _ageGroupStatsTransform.Execute(budgetGroupAgeStats);
                _statRepository.InsertAgeGroupStats(ageGroupStats);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void TransformStats(List<AdDevicePerformanceData> adDevStats)
        {
            try
            {
                var budgetGroupStats = _budgetGroupStatsTransform.Execute(adDevStats);
                _statRepository.InsertBudgetGroupStatsOneAtATime(budgetGroupStats);
                var orderStats = _orderStatsTransform.Execute(budgetGroupStats);
                _statRepository.InsertOrderStats(orderStats);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void TransforDeviceStats(List<AdDevicePerformanceData> adDevStats)
        {
            try
            {
                var budgetGroupDeviceStats = _budgetGroupDeviceStatsTransform.Execute(adDevStats);
                _statRepository.InsertBudgetGroupDeviceStats(budgetGroupDeviceStats);
                var deviceStats = _deviceStatsTransform.Execute(budgetGroupDeviceStats);
                _statRepository.InsertDeviceStats(deviceStats);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        private void TransformAdStats(List<AdDevicePerformanceData> adDevStats)
        {
            try
            {
                var budgetGroupAdStats = _budgetGroupAdStatsTransform.Execute(adDevStats);
                _statRepository.InsertBudgetGroupAdStats(budgetGroupAdStats);
                var adStats = _adStatsTransform.Execute(budgetGroupAdStats);
                _statRepository.InsertAdStats(adStats);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }
}
