﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.StatsGather
{
    public interface IGatherStats
    {
        void Execute(long customerId, DateTime statDate);

        void RunMoat(DateTime statDate);

        void RunNielsen(List<NielsenCampaignReference> campaignReferences, DateTime statDate);
        void RunNielsen(long customerId, DateTime statDate);
        void RunNielseDarUpdate(List<long> nielsenCiDs, DateTime yesterday);
        void RunDoubleVerify(List<DoubleVerifyImport> importData);
        void RunAdGroupGeoStats(long customerId, DateTime statDate);
        List<NielsenCampaignReference> VerifyNielsenCampaignStructure(DateTime date);
    }
}