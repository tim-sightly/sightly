﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Sightly.Business.OrderManager;
using Sightly.Models.tv;
using Sightly.Test.Common;
using IContainer = Autofac.IContainer;

namespace Sightly.Order.Test
{
    [TestClass]
    public class GetOrdersAndCompareTest
    {
        private static IContainer Container { get; set; }

        private IOrderComparison _orderComparison;
        //private IOrderManager _orderManager;

        private string Order1 =
            "{\"Info\":{\"Account\":\"399165a7-ab06-4a44-b63f-a65900531214\",\"Advertiser\":\"60d22f18-3ee4-4be8-ae49-a65900fb4baf\",\"Campaign\":\"b3e0fd32-3fa0-4552-af39-a659017327fd\",\"OrderId\":\"1b4a381d-54be-4edd-afb0-66e533297e9b\",\"OrderName\":\"ASI Solar 071618\",\"OrderRefCode\":null,\"Objective\":null,\"TotalBudget\":2500.0,\"StartDate\":\"2018-07-16T00:00:00\",\"EndDate\":\"2018-08-17T00:00:00\",\"Paused\":false,\"ParentOrderId\":null},\"Ads\":[],\"Audience\":{\"AgeRanges\":[\"e6174fe8-98b9-40b4-a75b-b12326dce1cd\",\"8517f6b4-f892-4631-8b8c-6e05eb128322\",\"fc26e52c-c104-4ee4-9005-a5537b822ff4\",\"cdb93241-a734-4978-828a-8ca72e8e835d\",\"aff1e62c-be36-4543-8c63-916cfc52cb99\",\"ff1641bc-50bd-4788-8456-bc4fbd2a9126\",\"f3e31a02-ebe2-4d07-9d7f-3e5ffb03d909\"],\"CompetitorUrls\":\"actionair.com\",\"Genders\":[\"6d4e82b0-7873-4434-a1f5-6ca299b203f4\",\"80e65313-aa65-429e-a4e4-ce48edc7c66c\",\"62ba835d-5af0-49ec-aec3-8492b936bd89\"],\"HouseholdIncomes\":[1,2,3,4,5,6,7],\"KeyWords\":\"air conditioning\\nair conditioner\\nac air conditioning\\nfurnace\\nair conditioners\\nhvac\\nair condition\\ntrane\\nair con\\nhvac air conditioning\\nheat pump\\nheating air\\nheating & air\\nheating and air\\nair conditioning units\\nair conditioning unit\\nair conditioner unit\\nair conditioner units\\nheating & cooling\\ncooling and heating\\nheating and cooling\\nair conditioning cooling\\nheat and air\\nair conditioning and heating\\nheating and air conditioning\\nheating air conditioning\\nheating & air conditioning\\nair conditioning heating\\nportable air conditioner\\nair conditioner portable\\ncentral air\\nportable air conditioning\\nair conditioning portable\\nportable air conditioners\\nair conditioners portable\\nair conditioning system\\nair conditioning repair\\nrepair air conditioning\\nair conditioner price\\nwindow air conditioner\\nair conditioner repair\\nportable air conditioning units\\nrepair air conditioner\",\"Notes\":\"ASI Hastings, Heating, Air and Solar is a San Diego HVAC and Solar contractor. For more information call us at 800-481-2665 or visit us online at asiheatingandair.com\",\"ParentalStatuses\":[\"e72fa1bc-ef09-4c4d-bffa-1e10a92bcd08\",\"09beb3c6-f702-4f8c-a895-3151f693f35e\",\"1d98120e-546d-489e-89aa-ffb47893f336\"]},\"Geo\":{\"SelectedServiceAreas\":[]}}";

        private string Order2 =
            "{\"Info\":{\"Account\":\"399165a7-ab06-4a44-b63f-a65900531214\",\"Advertiser\":\"60d22f18-3ee4-4be8-ae49-a65900fb4baf\",\"AdvertiserSubCategory\":\"9519b96e-48bf-41bd-8a81-a54c00ec1124\",\"Campaign\":\"b3e0fd32-3fa0-4552-af39-a659017327fd\",\"OrderId\":\"1b4a381d-54be-4edd-afb0-66e533297e9b\",\"OrderName\":\"ASI Solar 071618\",\"OrderRefCode\":\"Agen-7375\",\"Objective\":\"666a1fd2-a058-4515-a45d-cb007d505393\",\"TotalBudget\":3500.0,\"StartDate\":\"2018-08-10T23:24:24.356Z\",\"EndDate\":\"2018-09-28T00:00:00\",\"Paused\":false,\"ParentOrderId\":null},\"Ads\":[{\"AdId\":\"00000000-0000-0000-0000-000000000000\",\"AdName\":\"ASI Change Order Ad 444\",\"ClickableUrl\":\"https://asihastings.com\",\"Index\":0,\"LandingUrl\":\"https://asihastings.com\",\"AdFormat\":0,\"YoutubeUrl\":\"https://youtu.be/AJP9F4f4GhY\",\"VideoAssetVersionId\":\"00000000-0000-0000-0000-000000000000\",\"CampanionBannerUrl\":\"https://tv2prodfiles.blob.core.windows.net/bannerassets/Sightly-companion-banner-300x60-MBV.jpg\",\"Pause\":false,\"Deleted\":false}],\"Audience\":{\"AgeRanges\":[\"f3e31a02-ebe2-4d07-9d7f-3e5ffb03d909\",\"8517f6b4-f892-4631-8b8c-6e05eb128322\",\"cdb93241-a734-4978-828a-8ca72e8e835d\",\"aff1e62c-be36-4543-8c63-916cfc52cb99\",\"fc26e52c-c104-4ee4-9005-a5537b822ff4\",\"ff1641bc-50bd-4788-8456-bc4fbd2a9126\"],\"CompetitorUrls\":\"andersonheating.com\",\"Genders\":[\"6d4e82b0-7873-4434-a1f5-6ca299b203f4\",\"62ba835d-5af0-49ec-aec3-8492b936bd89\"],\"HouseholdIncomes\":[1,2,3,4,5,6,7],\"KeyWords\":\"air conditioning cooling\\nheat and air\\nair conditioning and heating\\nheating and air conditioning\\nheating air conditioning\\nheating & air conditioning\\nair conditioning heating\\nportable air conditioner\\nair conditioner portable\\ncentral air\",\"Notes\":\"Spark’s video editing capabilities are not too complex, they are simple enough for novices to acquire necessary skills to up\\ntheir game. One can comfortably incorporate text and photos into varied videos, themes, transition elements, and music. Branded Stories are set to help out those who prefer a\\nhands-on approach when it comes to their marketed\\nmaterial. The latest addition will also serve those\\nwho spend most of their time on social media and\\nvideo platforms.\",\"ParentalStatuses\":[\"e72fa1bc-ef09-4c4d-bffa-1e10a92bcd08\",\"09beb3c6-f702-4f8c-a895-3151f693f35e\",\"1d98120e-546d-489e-89aa-ffb47893f336\"]},\"Geo\":{\"SelectedServiceAreas\":[{\"DmaName\":\"92009, California\",\"Id\":\"83dd5bbb-a2bd-419a-8a4b-a54800f5705b\",\"OriginalPopulation\":42649},{\"DmaName\":\"92014, California\",\"Id\":\"41543b90-b29e-4ba9-8ed9-a54800f57066\",\"OriginalPopulation\":13403},{\"DmaName\":\"92024, California\",\"Id\":\"ccb0bbd6-b306-456c-8ee5-a54800f57078\",\"OriginalPopulation\":49048},{\"DmaName\":\"92025, California\",\"Id\":\"241a405d-62a4-4d6a-9a2a-a54800f57084\",\"OriginalPopulation\":49110},{\"DmaName\":\"92027, California\",\"Id\":\"da4a47e0-1902-4eaa-8cf3-a54800f5708e\",\"OriginalPopulation\":55461},{\"DmaName\":\"92029, California\",\"Id\":\"1711ccd6-79f2-4e17-9f69-a54800f5709f\",\"OriginalPopulation\":20198},{\"DmaName\":\"92037, California\",\"Id\":\"b6c1705d-f83a-4c53-836a-a54800f570ab\",\"OriginalPopulation\":40114},{\"DmaName\":\"92040, California\",\"Id\":\"89f1838d-8184-431b-8cd2-a54800f570b3\",\"OriginalPopulation\":41214},{\"DmaName\":\"92064, California\",\"Id\":\"bb30a099-85c9-4a7c-bc8e-a54800f570e2\",\"OriginalPopulation\":48622},{\"DmaName\":\"92065, California\",\"Id\":\"9971f487-7186-4670-89dc-a54800f570e5\",\"OriginalPopulation\":37299},{\"DmaName\":\"92067, California\",\"Id\":\"3f2ac27a-eb2c-42ca-bab8-a54800f570ef\",\"OriginalPopulation\":8068},{\"DmaName\":\"92071, California\",\"Id\":\"8354787c-0fc8-4648-af10-a54800f570fa\",\"OriginalPopulation\":54469},{\"DmaName\":\"92078, California\",\"Id\":\"29f852bc-793e-4181-ba7c-a54800f57106\",\"OriginalPopulation\":44007},{\"DmaName\":\"92091, California\",\"Id\":\"8be21d7f-58d2-41dd-8d8d-a54800f57128\",\"OriginalPopulation\":1383},{\"DmaName\":\"92121, California\",\"Id\":\"4abc002e-7d3c-46a0-a408-a54800f5718f\",\"OriginalPopulation\":4580},{\"DmaName\":\"92126, California\",\"Id\":\"c95e7c67-89ad-4a9e-97b5-a54800f571ab\",\"OriginalPopulation\":77860},{\"DmaName\":\"92127, California\",\"Id\":\"c8db741c-d41b-4cc7-85c4-a54800f571ae\",\"OriginalPopulation\":40246},{\"DmaName\":\"92128, California\",\"Id\":\"4047eee0-b246-4139-9866-a54800f571b2\",\"OriginalPopulation\":49332},{\"DmaName\":\"92129, California\",\"Id\":\"5c1175bc-5dcc-4672-bd9e-a54800f571b5\",\"OriginalPopulation\":53387},{\"DmaName\":\"92130, California\",\"Id\":\"fbf5523f-45a7-4ebc-b315-a54800f571bc\",\"OriginalPopulation\":48721},{\"DmaName\":\"92131, California\",\"Id\":\"78045d01-f2bf-4ad5-81a3-a54800f571c1\",\"OriginalPopulation\":33672}]}}";


        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _orderComparison = Container.Resolve<IOrderComparison>();
            //_orderManager = Container.Resolve<IOrderManager>();
        }

        [TestMethod]
        public void GetOrderDifferences()
        {
            var orderEarly = JsonConvert.DeserializeObject<Models.tv.Order>(Order1);
            var orderLate = JsonConvert.DeserializeObject<Models.tv.Order>(Order2);

            OrderChanges orderDifferences = _orderComparison.GetOrderDifferences(orderEarly, orderLate);

            Assert.IsNotNull(orderDifferences);
        }
        
    }
}
