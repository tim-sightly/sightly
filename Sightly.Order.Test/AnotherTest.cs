﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Sightly.Models.tv;
using Sightly.Test.Common;

namespace Sightly.Order.Test
{
    [TestClass]
    public class AnotherTest
    {
        private static IContainer Container { get; set; }

        private IOrderComparison _orderComparison;
        //private IOrderManager _orderManager;

        private string Order1 =
            "{\"Info\":{\"Account\":\"c0261124-3042-4c9d-be75-a65801312d4e\",\"Advertiser\":\"c47fa68c-5cdb-4339-bfd5-a78c01450ccc\",\"AdvertiserSubCategory\":\"00000000-0000-0000-0000-000000000000\",\"Campaign\":\"c3d63764-649a-4a99-a1c7-a78c01453d24\",\"OrderId\":\"0ce6c0e8-77b7-4b0b-aca8-a8f10020a593\",\"OrderName\":\"AMG - USA Children\'s and Women\'s - 05/30/2018\",\"OrderRefCode\":\"ALA - 1733007SBump\",\"Objective\":\"666a1fd2-a058-4515-a45d-cb007d505393\",\"TotalBudget\":30000.0,\"StartDate\":\"2018-07-24T00:00:00\",\"EndDate\":\"2018-11-27T00:00:00\",\"Paused\":false,\"ParentOrderId\":null},\"Ads\":[{\"AdId\":\"73e2059d-012f-4ff6-916e-a78c01461f07\",\"AdName\":\"USAPrenatalCampaign\",\"ClickableUrl\":\"http://www.usahealthsystem.com/usacwh?utm_source=AMG&utm_medium=Display&utm_campaign=Prenatal\",\"Index\":1,\"LandingUrl\":\"http://www.usahealthsystem.com/usacwh?utm_source=AMG&utm_medium=Display&utm_campaign=Prenatal\",\"AdFormat\":0,\"YoutubeUrl\":\"https://www.youtube.com/watch?v=yxBfHpUkUMI\",\"VideoAssetVersionId\":\"88c33e17-7833-47f0-bbe4-a78c0146017f\",\"CampanionBannerUrl\":\"A1584769-0001_V3.jpg\",\"Pause\":false,\"Deleted\":false},{\"AdId\":\"39504733-43ef-45be-8400-a8f10021b94c\",\"AdName\":\"USA Children\'s and Women\'s\",\"ClickableUrl\":\"http://www.usahealthsystem.com/Labor-and-Delivery?utm_source=amg&utm_medium=video&utm_campaign=womenshealthbirth\",\"Index\":2,\"LandingUrl\":\"http://www.usahealthsystem.com/Labor-and-Delivery?utm_source=amg&utm_medium=video&utm_campaign=womenshealthbirth\",\"AdFormat\":0,\"YoutubeUrl\":\"https://www.youtube.com/watch?v=rHP7hLzRXiQ\",\"VideoAssetVersionId\":\"e61f9dee-fe2c-4350-991e-a8f100219796\",\"CampanionBannerUrl\":\"A1584769-0001_V3.jpg\",\"Pause\":false,\"Deleted\":false}],\"Audience\":{\"AgeRanges\":[\"8517f6b4-f892-4631-8b8c-6e05eb128322\",\"fc26e52c-c104-4ee4-9005-a5537b822ff4\",\"e6174fe8-98b9-40b4-a75b-b12326dce1cd\",\"cdb93241-a734-4978-828a-8ca72e8e835d\"],\"CompetitorUrls\":\"www.yipee.com\",\"Genders\":[\"6d4e82b0-7873-4434-a1f5-6ca299b203f4\",\"80e65313-aa65-429e-a4e4-ce48edc7c66c\"],\"HouseholdIncomes\":[1,2,3,4,5],\"KeyWords\":\"blah blah blah\",\"Notes\":\"how about them apples\",\"ParentalStatuses\":[\"e72fa1bc-ef09-4c4d-bffa-1e10a92bcd08\",\"09beb3c6-f702-4f8c-a895-3151f693f35e\"]},\"Geo\":{\"SelectedServiceAreas\":[{\"DmaName\":\"Mobile, Alabama-Pensacola-Ft. Walton Beach, Florida DMA\",\"Id\":\"dc96cadc-88c7-43fb-b161-a5770120bb02\",\"OriginalPopulation\":1298536}]}}";


        private string Order2 =
            "{\"Info\":{\"Account\":\"c0261124-3042-4c9d-be75-a65801312d4e\",\"Advertiser\":\"c47fa68c-5cdb-4339-bfd5-a78c01450ccc\",\"AdvertiserSubCategory\":\"00000000-0000-0000-0000-000000000000\",\"Campaign\":\"c3d63764-649a-4a99-a1c7-a78c01453d24\",\"OrderId\":\"0ce6c0e8-77b7-4b0b-aca8-a8f10020a593\",\"OrderName\":\"AMG - USA Children\'s and Women\'s - 05/30/2018\",\"OrderRefCode\":\"ALA - 1733007SBump\",\"Objective\":\"666a1fd2-a058-4515-a45d-cb007d505393\",\"TotalBudget\":40000.0,\"StartDate\":\"2018-07-24T00:00:00\",\"EndDate\":\"2018-11-27T00:00:00\",\"Paused\":false,\"ParentOrderId\":null},\"Ads\":[{\"AdId\":\"73e2059d-012f-4ff6-916e-a78c01461f07\",\"AdName\":\"USAPrenatalCampaign\",\"ClickableUrl\":\"http://www.usahealthsystem.com/usacwh?utm_source=AMG&utm_medium=Display&utm_campaign=Prenatal\",\"Index\":1,\"LandingUrl\":\"http://www.usahealthsystem.com/usacwh?utm_source=AMG&utm_medium=Display&utm_campaign=Prenatal\",\"AdFormat\":0,\"YoutubeUrl\":\"https://www.youtube.com/watch?v=yxBfHpUkUMI\",\"VideoAssetVersionId\":\"88c33e17-7833-47f0-bbe4-a78c0146017f\",\"CampanionBannerUrl\":\"A1584769-0001_V3.jpg\",\"Pause\":false,\"Deleted\":false},{\"AdId\":\"39504733-43ef-45be-8400-a8f10021b94c\",\"AdName\":\"USA Children\'s and Women\'s\",\"ClickableUrl\":\"http://www.usahealthsystem.com/Labor-and-Delivery?utm_source=amg&utm_medium=video&utm_campaign=womenshealthbirth\",\"Index\":2,\"LandingUrl\":\"http://www.usahealthsystem.com/Labor-and-Delivery?utm_source=amg&utm_medium=video&utm_campaign=womenshealthbirth\",\"AdFormat\":0,\"YoutubeUrl\":\"https://www.youtube.com/watch?v=rHP7hLzRXiQ\",\"VideoAssetVersionId\":\"e61f9dee-fe2c-4350-991e-a8f100219796\",\"CampanionBannerUrl\":\"A1584769-0001_V3.jpg\",\"Pause\":false,\"Deleted\":false}],\"Audience\":{\"AgeRanges\":[\"8517f6b4-f892-4631-8b8c-6e05eb128322\",\"fc26e52c-c104-4ee4-9005-a5537b822ff4\",\"e6174fe8-98b9-40b4-a75b-b12326dce1cd\",\"cdb93241-a734-4978-828a-8ca72e8e835d\"],\"CompetitorUrls\":\"www.yipee.com\",\"Genders\":[\"6d4e82b0-7873-4434-a1f5-6ca299b203f4\",\"80e65313-aa65-429e-a4e4-ce48edc7c66c\"],\"HouseholdIncomes\":[1,2,3,4,5],\"KeyWords\":\"blah blah blah\",\"Notes\":\"how about them apples\",\"ParentalStatuses\":[\"e72fa1bc-ef09-4c4d-bffa-1e10a92bcd08\",\"09beb3c6-f702-4f8c-a895-3151f693f35e\"]},\"Geo\":{\"SelectedServiceAreas\":[{\"DmaName\":\"Mobile, Alabama-Pensacola-Ft. Walton Beach, Florida DMA\",\"Id\":\"dc96cadc-88c7-43fb-b161-a5770120bb02\",\"OriginalPopulation\":1298536}]}}";


        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _orderComparison = Container.Resolve<IOrderComparison>();
            //_orderManager = Container.Resolve<IOrderManager>();
        }


        [TestMethod]
        public void BudgetChange()
        {
            var orderEarly = JsonConvert.DeserializeObject<Models.tv.Order>(Order1);
            var orderLate = JsonConvert.DeserializeObject<Models.tv.Order>(Order2);

            OrderChanges orderDifferences = _orderComparison.GetOrderDifferences(orderEarly, orderLate);

            Assert.IsNotNull(orderDifferences);
        }
    }
}
