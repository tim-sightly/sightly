namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Live.BudgetGroupXrefPlacement")]
    public partial class BudgetGroupXrefPlacement1
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BudgetGroupXrefPlacement1()
        {
            BudgetGroupTimedBudget1 = new HashSet<BudgetGroupTimedBudget1>();
        }

        [Key]
        public Guid BudgetGroupXrefPlacementId { get; set; }

        public Guid BudgetGroupId { get; set; }

        public Guid PlacementId { get; set; }

        public Guid CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Created { get; set; }

        public virtual BudgetGroup1 BudgetGroup1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTimedBudget1> BudgetGroupTimedBudget1 { get; set; }

        public virtual Placement Placement { get; set; }

        public virtual User User { get; set; }
    }
}
