namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.Gender")]
    public partial class Gender
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Gender()
        {
            EstimateTargetGenders = new HashSet<EstimateTargetGender>();
            BudgetGroupTargetGenderStats = new HashSet<BudgetGroupTargetGenderStat>();
            OrderTargetGenders = new HashSet<OrderTargetGender>();
            OrderTargetGenderStats = new HashSet<OrderTargetGenderStat>();
            RawBudgetGroupGenderConversionStats = new HashSet<RawBudgetGroupGenderConversionStat>();
            RawBudgetGroupGenderStats = new HashSet<RawBudgetGroupGenderStat>();
            RawGenderConversionStats = new HashSet<RawGenderConversionStat>();
            RawGenderStats = new HashSet<RawGenderStat>();
            BudgetGroupTargetGenderStats1 = new HashSet<BudgetGroupTargetGenderStats1>();
            OrderTargetGenderStats1 = new HashSet<OrderTargetGenderStats1>();
        }

        public Guid GenderId { get; set; }

        [Required]
        [StringLength(16)]
        public string GenderName { get; set; }

        [Required]
        [StringLength(16)]
        public string AdWordsGenderName { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public decimal PopulationPercentage { get; set; }

        public int TargetViewGenderId { get; set; }

        [StringLength(8)]
        public string Color { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EstimateTargetGender> EstimateTargetGenders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTargetGenderStat> BudgetGroupTargetGenderStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetGender> OrderTargetGenders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetGenderStat> OrderTargetGenderStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupGenderConversionStat> RawBudgetGroupGenderConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupGenderStat> RawBudgetGroupGenderStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawGenderConversionStat> RawGenderConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawGenderStat> RawGenderStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTargetGenderStats1> BudgetGroupTargetGenderStats1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetGenderStats1> OrderTargetGenderStats1 { get; set; }
    }
}
