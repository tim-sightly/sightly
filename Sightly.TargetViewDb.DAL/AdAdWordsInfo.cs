namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.AdAdWordsInfo")]
    public partial class AdAdWordsInfo
    {
        public Guid AdAdWordsInfoId { get; set; }

        public Guid AdId { get; set; }

        public Guid? BudgetGroupId { get; set; }

        [Required]
        public string AdWordsAdName { get; set; }

        public long? AdWordsAdId { get; set; }

        public long? AdWordsCampaignId { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Ad Ad { get; set; }

        public virtual BudgetGroup2 BudgetGroup2 { get; set; }
    }
}
