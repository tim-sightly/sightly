namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.BudgetGroupXrefConversionAction")]
    public partial class BudgetGroupXrefConversionAction2
    {
        [Key]
        public Guid BudgetGroupXrefConversionActionId { get; set; }

        public Guid BudgetGroupId { get; set; }

        public Guid? ConversionActionId { get; set; }

        public int SequenceId { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual ConversionAction ConversionAction { get; set; }

        public virtual BudgetGroup2 BudgetGroup2 { get; set; }
    }
}
