namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.Order")]
    public partial class Order
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Order()
        {
            Estimates = new HashSet<Estimate>();
            AdStats = new HashSet<AdStat>();
            BudgetGroupAdStats = new HashSet<BudgetGroupAdStat>();
            BudgetGroupDeviceStats = new HashSet<BudgetGroupDeviceStat>();
            BudgetGroupTargetAgeGroupStats = new HashSet<BudgetGroupTargetAgeGroupStat>();
            BudgetGroupTargetGenderStats = new HashSet<BudgetGroupTargetGenderStat>();
            DeviceStats = new HashSet<DeviceStat>();
            AdStats1 = new HashSet<AdStats1>();
            BudgetGroupAdStats1 = new HashSet<BudgetGroupAdStats1>();
            BudgetGroupDeviceStats1 = new HashSet<BudgetGroupDeviceStats1>();
            BudgetGroupTargetAgeGroupStats1 = new HashSet<BudgetGroupTargetAgeGroupStats1>();
            BudgetGroupTargetGenderStats1 = new HashSet<BudgetGroupTargetGenderStats1>();
            DeviceStats1 = new HashSet<DeviceStats1>();
            Order1 = new HashSet<Order>();
            OrderBudgetPacings = new HashSet<OrderBudgetPacing>();
            OrderCompetitorUrls = new HashSet<OrderCompetitorUrl>();
            OrderConversionGatheringDurations = new HashSet<OrderConversionGatheringDuration>();
            OrderKeyTerms = new HashSet<OrderKeyTerm>();
            OrderRecordLogs = new HashSet<OrderRecordLog>();
            OrderRecordLogs1 = new HashSet<OrderRecordLog>();
            OrderSelectedChangeTypes = new HashSet<OrderSelectedChangeType>();
            OrderStats = new HashSet<OrderStat>();
            OrderStats1 = new HashSet<OrderStats1>();
            OrderTargetAgeGroups = new HashSet<OrderTargetAgeGroup>();
            OrderTargetAgeGroupStats = new HashSet<OrderTargetAgeGroupStat>();
            OrderTargetAgeGroupStats1 = new HashSet<OrderTargetAgeGroupStats1>();
            OrderTargetGenders = new HashSet<OrderTargetGender>();
            OrderTargetGenderStats = new HashSet<OrderTargetGenderStat>();
            OrderTargetGenderStats1 = new HashSet<OrderTargetGenderStats1>();
            OrderTargetParentalStatus = new HashSet<OrderTargetParentalStatu>();
            OrderXrefBudgetGroups = new HashSet<OrderXrefBudgetGroup>();
            OrderXrefConversionActions = new HashSet<OrderXrefConversionAction>();
            OrderXrefDeliveryKpis = new HashSet<OrderXrefDeliveryKpi>();
            OrderXrefObjectives = new HashSet<OrderXrefObjective>();
            RawAdConversionStats = new HashSet<RawAdConversionStat>();
            RawAdDeviceConversionStats = new HashSet<RawAdDeviceConversionStat>();
            RawAdStats = new HashSet<RawAdStat>();
            RawAgeGroupConversionStats = new HashSet<RawAgeGroupConversionStat>();
            RawAgeGroupStats = new HashSet<RawAgeGroupStat>();
            RawBudgetGroupAdStats = new HashSet<RawBudgetGroupAdStat>();
            RawBudgetGroupAgeGroupStats = new HashSet<RawBudgetGroupAgeGroupStat>();
            RawBudgetGroupDeviceStats = new HashSet<RawBudgetGroupDeviceStat>();
            RawBudgetGroupGenderStats = new HashSet<RawBudgetGroupGenderStat>();
            RawDeviceConversionStats = new HashSet<RawDeviceConversionStat>();
            RawDeviceStats = new HashSet<RawDeviceStat>();
            RawGenderConversionStats = new HashSet<RawGenderConversionStat>();
            RawGenderStats = new HashSet<RawGenderStat>();
            RawGeoConversionStats = new HashSet<RawGeoConversionStat>();
            RawOrderConversionStats = new HashSet<RawOrderConversionStat>();
            RawOrderStats = new HashSet<RawOrderStat>();
        }

        public Guid OrderId { get; set; }

        [Required]
        [StringLength(128)]
        public string OrderName { get; set; }

        [StringLength(128)]
        public string OrderRefCode { get; set; }

        public Guid AccountId { get; set; }

        public Guid? CampaignId { get; set; }

        public Guid? AdvertiserId { get; set; }

        public Guid? AdvertiserSubCategoryId { get; set; }

        public Guid? TargetingTypeId { get; set; }

        public Guid? BudgetTypeId { get; set; }

        [Column(TypeName = "money")]
        public decimal? CampaignBudget { get; set; }

        [Column(TypeName = "money")]
        public decimal? MonthlyBudget { get; set; }

        public int? MonthCount { get; set; }

        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }

        public bool? StartDateHard { get; set; }

        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }

        public bool? EndDateHard { get; set; }

        public decimal? CustomerAccountMargin { get; set; }

        public decimal? SightlyMargin { get; set; }

        public Guid OrderTypeId { get; set; }

        public Guid? OrderStatusId { get; set; }

        public int? AdDurationInSeconds { get; set; }

        [Column(TypeName = "money")]
        public decimal? EstimatedCpcv { get; set; }

        public decimal? AssumedViewRate { get; set; }

        public Guid? ParentOrderId { get; set; }

        [StringLength(1024)]
        public string PauseReason { get; set; }

        [Column(TypeName = "date")]
        public DateTime? PauseDate { get; set; }

        public bool SpendFlex { get; set; }

        public bool Deleted { get; set; }

        public Guid CreatorId { get; set; }

        public Guid? CancelerId { get; set; }

        public Guid? SubmitterId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? SubmittedDatetime { get; set; }

        public Guid? BudgetAllocationId { get; set; }

        public bool? RollBudgetBetweenLocations { get; set; }

        [Column(TypeName = "money")]
        public decimal? MinimumLocationBudget { get; set; }

        public Guid? DeliveryPriorityId { get; set; }

        public Guid? MediaBriefEmailStatusId { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CanceledDatetime { get; set; }

        public virtual Account Account { get; set; }

        public virtual Advertiser Advertiser { get; set; }

        public virtual Campaign Campaign { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Estimate> Estimates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdStat> AdStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupAdStat> BudgetGroupAdStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupDeviceStat> BudgetGroupDeviceStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTargetAgeGroupStat> BudgetGroupTargetAgeGroupStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTargetGenderStat> BudgetGroupTargetGenderStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeviceStat> DeviceStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdStats1> AdStats1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupAdStats1> BudgetGroupAdStats1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupDeviceStats1> BudgetGroupDeviceStats1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTargetAgeGroupStats1> BudgetGroupTargetAgeGroupStats1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTargetGenderStats1> BudgetGroupTargetGenderStats1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeviceStats1> DeviceStats1 { get; set; }

        public virtual AdvertiserSubCategory AdvertiserSubCategory { get; set; }

        public virtual BudgetAllocation BudgetAllocation { get; set; }

        public virtual BudgetType BudgetType { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        public virtual DeliveryPriority DeliveryPriority { get; set; }

        public virtual OrderStatu OrderStatu { get; set; }

        public virtual OrderType OrderType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order> Order1 { get; set; }

        public virtual Order Order2 { get; set; }

        public virtual User User2 { get; set; }

        public virtual TargetingType TargetingType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderBudgetPacing> OrderBudgetPacings { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderCompetitorUrl> OrderCompetitorUrls { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderConversionGatheringDuration> OrderConversionGatheringDurations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderKeyTerm> OrderKeyTerms { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderRecordLog> OrderRecordLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderRecordLog> OrderRecordLogs1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderSelectedChangeType> OrderSelectedChangeTypes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderStat> OrderStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderStats1> OrderStats1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetAgeGroup> OrderTargetAgeGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetAgeGroupStat> OrderTargetAgeGroupStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetAgeGroupStats1> OrderTargetAgeGroupStats1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetGender> OrderTargetGenders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetGenderStat> OrderTargetGenderStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetGenderStats1> OrderTargetGenderStats1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetParentalStatu> OrderTargetParentalStatus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderXrefBudgetGroup> OrderXrefBudgetGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderXrefConversionAction> OrderXrefConversionActions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderXrefDeliveryKpi> OrderXrefDeliveryKpis { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderXrefObjective> OrderXrefObjectives { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAdConversionStat> RawAdConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAdDeviceConversionStat> RawAdDeviceConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAdStat> RawAdStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAgeGroupConversionStat> RawAgeGroupConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAgeGroupStat> RawAgeGroupStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAdStat> RawBudgetGroupAdStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAgeGroupStat> RawBudgetGroupAgeGroupStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupDeviceStat> RawBudgetGroupDeviceStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupGenderStat> RawBudgetGroupGenderStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawDeviceConversionStat> RawDeviceConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawDeviceStat> RawDeviceStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawGenderConversionStat> RawGenderConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawGenderStat> RawGenderStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawGeoConversionStat> RawGeoConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawOrderConversionStat> RawOrderConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawOrderStat> RawOrderStats { get; set; }
    }
}
