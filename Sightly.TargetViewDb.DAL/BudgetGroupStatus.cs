namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.BudgetGroupStatuses")]
    public partial class BudgetGroupStatus
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BudgetGroupStatus()
        {
            BudgetGroups = new HashSet<BudgetGroup>();
            BudgetGroup1 = new HashSet<BudgetGroup1>();
        }

        public byte BudgetGroupStatusId { get; set; }

        [Required]
        [StringLength(20)]
        public string BudgetGroupStatusName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroup> BudgetGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroup1> BudgetGroup1 { get; set; }
    }
}
