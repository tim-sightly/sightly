namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Accounts.VideoAssetVersion")]
    public partial class VideoAssetVersion
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VideoAssetVersion()
        {
            Ads = new HashSet<Ad>();
        }

        public Guid VideoAssetVersionId { get; set; }

        public bool Deleted { get; set; }

        public Guid VideoAssetId { get; set; }

        [Required]
        [StringLength(1024)]
        public string VideoAssetVersionName { get; set; }

        public int VideoAssetVersionNo { get; set; }

        [StringLength(32)]
        public string VideoAssetVersionRefCode { get; set; }

        public int? VideoLengthInSeconds { get; set; }

        [StringLength(1024)]
        public string YouTubeId { get; set; }

        [StringLength(1024)]
        public string YouTubeUrl { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastModifiedDatetime { get; set; }

        public virtual VideoAsset VideoAsset { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Ad> Ads { get; set; }
    }
}
