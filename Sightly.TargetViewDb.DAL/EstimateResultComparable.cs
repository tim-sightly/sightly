namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Estimates.EstimateResultComparable")]
    public partial class EstimateResultComparable
    {
        public Guid EstimateResultComparableId { get; set; }

        public Guid EstimateId { get; set; }

        public decimal? Reach { get; set; }

        [Column(TypeName = "money")]
        public decimal? Budget { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Estimate Estimate { get; set; }
    }
}
