namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class TargetViewDb : DbContext
    {
        public TargetViewDb()
            : base("name=TargetViewDb")
        {
        }

        public virtual DbSet<Account> Accounts { get; set; }
        public virtual DbSet<AccountBillingAddress> AccountBillingAddresses { get; set; }
        public virtual DbSet<AccountBillingContact> AccountBillingContacts { get; set; }
        public virtual DbSet<AccountCreditCard> AccountCreditCards { get; set; }
        public virtual DbSet<Advertiser> Advertisers { get; set; }
        public virtual DbSet<AdvertiserXrefConversionAction> AdvertiserXrefConversionActions { get; set; }
        public virtual DbSet<BannerAsset> BannerAssets { get; set; }
        public virtual DbSet<Campaign> Campaigns { get; set; }
        public virtual DbSet<CampaignAdWordsInfo> CampaignAdWordsInfoes { get; set; }
        public virtual DbSet<ConversionAction> ConversionActions { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<LocationDetail> LocationDetails { get; set; }
        public virtual DbSet<VideoAsset> VideoAssets { get; set; }
        public virtual DbSet<VideoAssetVersion> VideoAssetVersions { get; set; }
        public virtual DbSet<DailyPacing> DailyPacings { get; set; }
        public virtual DbSet<Estimate> Estimates { get; set; }
        public virtual DbSet<EstimateResultComparable> EstimateResultComparables { get; set; }
        public virtual DbSet<EstimateTargetAgeGroup> EstimateTargetAgeGroups { get; set; }
        public virtual DbSet<EstimateTargetGender> EstimateTargetGenders { get; set; }
        public virtual DbSet<EstimateTargetParentalStatu> EstimateTargetParentalStatus { get; set; }
        public virtual DbSet<BudgetGroup> BudgetGroups { get; set; }
        public virtual DbSet<BudgetGroupTimedBudget> BudgetGroupTimedBudgets { get; set; }
        public virtual DbSet<BudgetGroupXrefAd> BudgetGroupXrefAds { get; set; }
        public virtual DbSet<BudgetGroupXrefConversionAction> BudgetGroupXrefConversionActions { get; set; }
        public virtual DbSet<BudgetGroupXrefLocation> BudgetGroupXrefLocations { get; set; }
        public virtual DbSet<BudgetGroupXrefPlacement> BudgetGroupXrefPlacements { get; set; }
        public virtual DbSet<BudgetGroup1> BudgetGroup1 { get; set; }
        public virtual DbSet<BudgetGroupTimedBudget1> BudgetGroupTimedBudget1 { get; set; }
        public virtual DbSet<BudgetGroupTimedBudgetProperty> BudgetGroupTimedBudgetProperties { get; set; }
        public virtual DbSet<BudgetGroupXrefAd1> BudgetGroupXrefAd1 { get; set; }
        public virtual DbSet<BudgetGroupXrefConversionAction1> BudgetGroupXrefConversionAction1 { get; set; }
        public virtual DbSet<BudgetGroupXrefLocation1> BudgetGroupXrefLocation1 { get; set; }
        public virtual DbSet<BudgetGroupXrefPlacement1> BudgetGroupXrefPlacement1 { get; set; }
        public virtual DbSet<Ad> Ads { get; set; }
        public virtual DbSet<AdAdWordsInfo> AdAdWordsInfoes { get; set; }
        public virtual DbSet<AdStat> AdStats { get; set; }
        public virtual DbSet<BudgetGroup2> BudgetGroup2 { get; set; }
        public virtual DbSet<BudgetGroupAdStat> BudgetGroupAdStats { get; set; }
        public virtual DbSet<BudgetGroupAdWordsInfo> BudgetGroupAdWordsInfoes { get; set; }
        public virtual DbSet<BudgetGroupDeviceStat> BudgetGroupDeviceStats { get; set; }
        public virtual DbSet<BudgetGroupLedger> BudgetGroupLedgers { get; set; }
        public virtual DbSet<BudgetGroupStat> BudgetGroupStats { get; set; }
        public virtual DbSet<BudgetGroupTargetAgeGroupStat> BudgetGroupTargetAgeGroupStats { get; set; }
        public virtual DbSet<BudgetGroupTargetGenderStat> BudgetGroupTargetGenderStats { get; set; }
        public virtual DbSet<BudgetGroupTimedBudget2> BudgetGroupTimedBudget2 { get; set; }
        public virtual DbSet<BudgetGroupXrefAd2> BudgetGroupXrefAd2 { get; set; }
        public virtual DbSet<BudgetGroupXrefConversionAction2> BudgetGroupXrefConversionAction2 { get; set; }
        public virtual DbSet<BudgetGroupXrefLocation2> BudgetGroupXrefLocation2 { get; set; }
        public virtual DbSet<BudgetGroupXrefPlacement2> BudgetGroupXrefPlacement2 { get; set; }
        public virtual DbSet<ChangeOrderLog> ChangeOrderLogs { get; set; }
        public virtual DbSet<DailyPacing1> DailyPacing1 { get; set; }
        public virtual DbSet<DeviceStat> DeviceStats { get; set; }
        public virtual DbSet<LockedOrder> LockedOrders { get; set; }
        public virtual DbSet<Order> Orders { get; set; }
        public virtual DbSet<OrderAdWordsInfo> OrderAdWordsInfoes { get; set; }
        public virtual DbSet<OrderBudgetPacing> OrderBudgetPacings { get; set; }
        public virtual DbSet<OrderCompetitorUrl> OrderCompetitorUrls { get; set; }
        public virtual DbSet<OrderConversionGatheringDuration> OrderConversionGatheringDurations { get; set; }
        public virtual DbSet<OrderKeyTerm> OrderKeyTerms { get; set; }
        public virtual DbSet<OrderRecordLog> OrderRecordLogs { get; set; }
        public virtual DbSet<OrderSelectedChangeType> OrderSelectedChangeTypes { get; set; }
        public virtual DbSet<OrderStat> OrderStats { get; set; }
        public virtual DbSet<OrderTargetAgeGroup> OrderTargetAgeGroups { get; set; }
        public virtual DbSet<OrderTargetAgeGroupStat> OrderTargetAgeGroupStats { get; set; }
        public virtual DbSet<OrderTargetGender> OrderTargetGenders { get; set; }
        public virtual DbSet<OrderTargetGenderStat> OrderTargetGenderStats { get; set; }
        public virtual DbSet<OrderTargetParentalStatu> OrderTargetParentalStatus { get; set; }
        public virtual DbSet<OrderXrefBudgetGroup> OrderXrefBudgetGroups { get; set; }
        public virtual DbSet<OrderXrefConversionAction> OrderXrefConversionActions { get; set; }
        public virtual DbSet<OrderXrefDeliveryKpi> OrderXrefDeliveryKpis { get; set; }
        public virtual DbSet<OrderXrefObjective> OrderXrefObjectives { get; set; }
        public virtual DbSet<Placement> Placements { get; set; }
        public virtual DbSet<RawAdConversionStat> RawAdConversionStats { get; set; }
        public virtual DbSet<RawAdDeviceConversionStat> RawAdDeviceConversionStats { get; set; }
        public virtual DbSet<RawAdStat> RawAdStats { get; set; }
        public virtual DbSet<RawAgeGroupConversionStat> RawAgeGroupConversionStats { get; set; }
        public virtual DbSet<RawAgeGroupStat> RawAgeGroupStats { get; set; }
        public virtual DbSet<RawBudgetGroupAdConversionStat> RawBudgetGroupAdConversionStats { get; set; }
        public virtual DbSet<RawBudgetGroupAdDeviceConversionStat> RawBudgetGroupAdDeviceConversionStats { get; set; }
        public virtual DbSet<RawBudgetGroupAdStat> RawBudgetGroupAdStats { get; set; }
        public virtual DbSet<RawBudgetGroupAgeGroupConversionStat> RawBudgetGroupAgeGroupConversionStats { get; set; }
        public virtual DbSet<RawBudgetGroupAgeGroupStat> RawBudgetGroupAgeGroupStats { get; set; }
        public virtual DbSet<RawBudgetGroupConversionStat> RawBudgetGroupConversionStats { get; set; }
        public virtual DbSet<RawBudgetGroupDeviceConversionStat> RawBudgetGroupDeviceConversionStats { get; set; }
        public virtual DbSet<RawBudgetGroupDeviceStat> RawBudgetGroupDeviceStats { get; set; }
        public virtual DbSet<RawBudgetGroupGenderConversionStat> RawBudgetGroupGenderConversionStats { get; set; }
        public virtual DbSet<RawBudgetGroupGenderStat> RawBudgetGroupGenderStats { get; set; }
        public virtual DbSet<RawBudgetGroupGeoConversionStat> RawBudgetGroupGeoConversionStats { get; set; }
        public virtual DbSet<RawBudgetGroupStat> RawBudgetGroupStats { get; set; }
        public virtual DbSet<RawDeviceConversionStat> RawDeviceConversionStats { get; set; }
        public virtual DbSet<RawDeviceStat> RawDeviceStats { get; set; }
        public virtual DbSet<RawGenderConversionStat> RawGenderConversionStats { get; set; }
        public virtual DbSet<RawGenderStat> RawGenderStats { get; set; }
        public virtual DbSet<RawGeoConversionStat> RawGeoConversionStats { get; set; }
        public virtual DbSet<RawOrderConversionStat> RawOrderConversionStats { get; set; }
        public virtual DbSet<RawOrderStat> RawOrderStats { get; set; }
        public virtual DbSet<RegatheredStatLog> RegatheredStatLogs { get; set; }
        public virtual DbSet<StatsGatheringLog> StatsGatheringLogs { get; set; }
        public virtual DbSet<AdDeviceStat> AdDeviceStats { get; set; }
        public virtual DbSet<AgeRangeStat> AgeRangeStats { get; set; }
        public virtual DbSet<GenderStat> GenderStats { get; set; }
        public virtual DbSet<AdStats1> AdStats1 { get; set; }
        public virtual DbSet<BudgetGroupAdStats1> BudgetGroupAdStats1 { get; set; }
        public virtual DbSet<BudgetGroupDeviceStats1> BudgetGroupDeviceStats1 { get; set; }
        public virtual DbSet<BudgetGroupStats1> BudgetGroupStats1 { get; set; }
        public virtual DbSet<BudgetGroupTargetAgeGroupStats1> BudgetGroupTargetAgeGroupStats1 { get; set; }
        public virtual DbSet<BudgetGroupTargetGenderStats1> BudgetGroupTargetGenderStats1 { get; set; }
        public virtual DbSet<DeviceStats1> DeviceStats1 { get; set; }
        public virtual DbSet<OrderStats1> OrderStats1 { get; set; }
        public virtual DbSet<OrderTargetAgeGroupStats1> OrderTargetAgeGroupStats1 { get; set; }
        public virtual DbSet<OrderTargetGenderStats1> OrderTargetGenderStats1 { get; set; }
        public virtual DbSet<AgencyMcc> AgencyMccs { get; set; }
        public virtual DbSet<PropertyType> PropertyTypes { get; set; }
        public virtual DbSet<ApiSession> ApiSessions { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<RoleClaim> RoleClaims { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserActivity> UserActivities { get; set; }
        public virtual DbSet<UserXrefAccount> UserXrefAccounts { get; set; }
        public virtual DbSet<UserXrefAdvertiser> UserXrefAdvertisers { get; set; }
        public virtual DbSet<AccountLogoFileType> AccountLogoFileTypes { get; set; }
        public virtual DbSet<AccountType> AccountTypes { get; set; }
        public virtual DbSet<AdLength> AdLengths { get; set; }
        public virtual DbSet<AdvertiserCategory> AdvertiserCategories { get; set; }
        public virtual DbSet<AdvertiserSubCategory> AdvertiserSubCategories { get; set; }
        public virtual DbSet<AgeGroup> AgeGroups { get; set; }
        public virtual DbSet<BillingType> BillingTypes { get; set; }
        public virtual DbSet<BudgetAllocation> BudgetAllocations { get; set; }
        public virtual DbSet<BudgetGroupEvent> BudgetGroupEvents { get; set; }
        public virtual DbSet<BudgetGroupStatus> BudgetGroupStatuses { get; set; }
        public virtual DbSet<BudgetPacingDateRange> BudgetPacingDateRanges { get; set; }
        public virtual DbSet<BudgetPacingField> BudgetPacingFields { get; set; }
        public virtual DbSet<BudgetType> BudgetTypes { get; set; }
        public virtual DbSet<CalculationSetting> CalculationSettings { get; set; }
        public virtual DbSet<ChangeType> ChangeTypes { get; set; }
        public virtual DbSet<ConversionActionTrackingStatu> ConversionActionTrackingStatus { get; set; }
        public virtual DbSet<ConversionCount> ConversionCounts { get; set; }
        public virtual DbSet<ConversionTrackingType> ConversionTrackingTypes { get; set; }
        public virtual DbSet<ConversionType> ConversionTypes { get; set; }
        public virtual DbSet<ConversionWindowsFromClick> ConversionWindowsFromClicks { get; set; }
        public virtual DbSet<ConversionWindowsFromImpression> ConversionWindowsFromImpressions { get; set; }
        public virtual DbSet<DeliveryKpi> DeliveryKpis { get; set; }
        public virtual DbSet<DeliveryPriority> DeliveryPriorities { get; set; }
        public virtual DbSet<DeviceType> DeviceTypes { get; set; }
        public virtual DbSet<EmailTemplate> EmailTemplates { get; set; }
        public virtual DbSet<EstimateFilterType> EstimateFilterTypes { get; set; }
        public virtual DbSet<EstimateResultComparableView> EstimateResultComparableViews { get; set; }
        public virtual DbSet<ExecutivePerformanceSortableField> ExecutivePerformanceSortableFields { get; set; }
        public virtual DbSet<Gender> Genders { get; set; }
        public virtual DbSet<Geography> Geographies { get; set; }
        public virtual DbSet<GeographyType> GeographyTypes { get; set; }
        public virtual DbSet<MediaBriefEmailStatu> MediaBriefEmailStatus { get; set; }
        public virtual DbSet<Objective> Objectives { get; set; }
        public virtual DbSet<ObjectiveCategory> ObjectiveCategories { get; set; }
        public virtual DbSet<OrderStatu> OrderStatus { get; set; }
        public virtual DbSet<OrderType> OrderTypes { get; set; }
        public virtual DbSet<PacingStatu> PacingStatus { get; set; }
        public virtual DbSet<ParentalStatu> ParentalStatus { get; set; }
        public virtual DbSet<PerformanceSummaryReportType> PerformanceSummaryReportTypes { get; set; }
        public virtual DbSet<ProfilePhotoFileType> ProfilePhotoFileTypes { get; set; }
        public virtual DbSet<Service> Services { get; set; }
        public virtual DbSet<SubAccountType> SubAccountTypes { get; set; }
        public virtual DbSet<TargetingType> TargetingTypes { get; set; }
        public virtual DbSet<ValuePerConversion> ValuePerConversions { get; set; }
        public virtual DbSet<YouTubeChannelLevel> YouTubeChannelLevels { get; set; }
        public virtual DbSet<BudgetProcessingLog> BudgetProcessingLogs { get; set; }
        public virtual DbSet<AggregatedCampaignAgeGroupStatsView> AggregatedCampaignAgeGroupStatsViews { get; set; }
        public virtual DbSet<AggregatedCampaignDeviceStatsView> AggregatedCampaignDeviceStatsViews { get; set; }
        public virtual DbSet<AggregatedCampaignGenderStatsView> AggregatedCampaignGenderStatsViews { get; set; }
        public virtual DbSet<AwTargetViewRelationDataView> AwTargetViewRelationDataViews { get; set; }
        public virtual DbSet<OrderBudgetGroupImpressionsViewsByDate> OrderBudgetGroupImpressionsViewsByDates { get; set; }
        public virtual DbSet<OrderImpressionsViewsByDate> OrderImpressionsViewsByDates { get; set; }
        public virtual DbSet<OrderStatsView_20170829> OrderStatsView_20170829 { get; set; }
        public virtual DbSet<RL_Daily> RL_Daily { get; set; }
        public virtual DbSet<AdStatsByDate> AdStatsByDates { get; set; }
        public virtual DbSet<AgeStatsByDate> AgeStatsByDates { get; set; }
        public virtual DbSet<CampaignStatsByDate> CampaignStatsByDates { get; set; }
        public virtual DbSet<DeviceStatsByDate> DeviceStatsByDates { get; set; }
        public virtual DbSet<GenderStatsByDate> GenderStatsByDates { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>()
                .Property(e => e.AccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<Account>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.AccountBillingAddresses)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.AccountBillingContacts)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.AccountCreditCards)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Account1)
                .WithOptional(e => e.Account2)
                .HasForeignKey(e => e.ParentAccountId);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Advertisers)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.BannerAssets)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Campaigns)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Locations)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.VideoAssets)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Users)
                .WithRequired(e => e.Account)
                .HasForeignKey(e => e.AccountId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.UserXrefAccounts)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.ConversionActions)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Estimates)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Account>()
                .HasMany(e => e.OrderRecordLogs)
                .WithRequired(e => e.Account)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Advertiser>()
                .Property(e => e.AdvertiserMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<Advertiser>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<Advertiser>()
                .HasMany(e => e.UserXrefAdvertisers)
                .WithRequired(e => e.Advertiser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Advertiser>()
                .HasMany(e => e.AdvertiserXrefConversionActions)
                .WithRequired(e => e.Advertiser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Advertiser>()
                .HasMany(e => e.ConversionActions)
                .WithRequired(e => e.Advertiser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Advertiser>()
                .HasMany(e => e.Estimates)
                .WithRequired(e => e.Advertiser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Advertiser>()
                .HasMany(e => e.OrderRecordLogs)
                .WithRequired(e => e.Advertiser)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Campaign>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<Campaign>()
                .HasMany(e => e.CampaignAdWordsInfoes)
                .WithRequired(e => e.Campaign)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Campaign>()
                .HasMany(e => e.OrderRecordLogs)
                .WithRequired(e => e.Campaign)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .Property(e => e.ValuePerConversion)
                .HasPrecision(19, 4);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.AdvertiserXrefConversionActions)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.BudgetGroupXrefConversionActions)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.BudgetGroupXrefConversionAction1)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.OrderConversionGatheringDurations)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.OrderXrefConversionActions)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawAdConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawAdDeviceConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawAgeGroupConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawBudgetGroupGenderConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawBudgetGroupAdConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawBudgetGroupAdDeviceConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawBudgetGroupAgeGroupConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawBudgetGroupConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawBudgetGroupDeviceConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawBudgetGroupGeoConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawDeviceConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawGenderConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawGeoConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionAction>()
                .HasMany(e => e.RawOrderConversionStats)
                .WithRequired(e => e.ConversionAction)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Location>()
                .HasMany(e => e.LocationDetails)
                .WithRequired(e => e.Location)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Location>()
                .HasMany(e => e.BudgetGroupXrefLocations)
                .WithRequired(e => e.Location)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Location>()
                .HasMany(e => e.BudgetGroupXrefLocation1)
                .WithRequired(e => e.Location)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Location>()
                .HasMany(e => e.BudgetGroupXrefLocation2)
                .WithRequired(e => e.Location)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Location>()
                .HasMany(e => e.Estimates)
                .WithRequired(e => e.Location)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<VideoAsset>()
                .HasMany(e => e.VideoAssetVersions)
                .WithRequired(e => e.VideoAsset)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DailyPacing>()
                .Property(e => e.RecomendedBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DailyPacing>()
                .Property(e => e.TotalBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Estimate>()
                .Property(e => e.Budget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Estimate>()
                .Property(e => e.EstimatedCpcv)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Estimate>()
                .Property(e => e.AssumedViewRate)
                .HasPrecision(5, 4);

            modelBuilder.Entity<Estimate>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<Estimate>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<Estimate>()
                .HasMany(e => e.EstimateResultComparables)
                .WithRequired(e => e.Estimate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Estimate>()
                .HasMany(e => e.EstimateTargetAgeGroups)
                .WithRequired(e => e.Estimate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Estimate>()
                .HasMany(e => e.EstimateTargetGenders)
                .WithRequired(e => e.Estimate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Estimate>()
                .HasMany(e => e.EstimateTargetParentalStatus)
                .WithRequired(e => e.Estimate)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EstimateResultComparable>()
                .Property(e => e.Reach)
                .HasPrecision(5, 4);

            modelBuilder.Entity<EstimateResultComparable>()
                .Property(e => e.Budget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroup>()
                .HasMany(e => e.BudgetGroupLedgers)
                .WithOptional(e => e.BudgetGroup)
                .HasForeignKey(e => e.BudgetGroupHistoryId);

            modelBuilder.Entity<BudgetGroupTimedBudget>()
                .Property(e => e.BudgetAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTimedBudget>()
                .Property(e => e.Margin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupTimedBudget1>()
                .Property(e => e.BudgetAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTimedBudget1>()
                .Property(e => e.Margin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupTimedBudget1>()
                .HasMany(e => e.BudgetGroupTimedBudgetProperties)
                .WithRequired(e => e.BudgetGroupTimedBudget1)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ad>()
                .HasMany(e => e.BudgetGroupXrefAds)
                .WithRequired(e => e.Ad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ad>()
                .HasMany(e => e.BudgetGroupXrefAd1)
                .WithRequired(e => e.Ad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ad>()
                .HasMany(e => e.AdAdWordsInfoes)
                .WithRequired(e => e.Ad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ad>()
                .HasMany(e => e.BudgetGroupAdStats)
                .WithRequired(e => e.Ad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ad>()
                .HasMany(e => e.BudgetGroupAdStats1)
                .WithRequired(e => e.Ad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ad>()
                .HasMany(e => e.BudgetGroupXrefAd2)
                .WithRequired(e => e.Ad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ad>()
                .HasMany(e => e.RawAdConversionStats)
                .WithRequired(e => e.Ad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ad>()
                .HasMany(e => e.RawAdStats)
                .WithRequired(e => e.Ad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ad>()
                .HasMany(e => e.RawBudgetGroupAdConversionStats)
                .WithRequired(e => e.Ad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ad>()
                .HasMany(e => e.RawBudgetGroupAdDeviceConversionStats)
                .WithRequired(e => e.Ad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Ad>()
                .HasMany(e => e.RawBudgetGroupAdStats)
                .WithRequired(e => e.Ad)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AdStat>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdStat>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<AdStat>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdStat>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdStat>()
                .Property(e => e.PartialViews)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdStat>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<AdStat>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdStat>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdStat>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroup2>()
                .Property(e => e.BudgetGroupBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.BudgetGroupAdStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.BudgetGroupAdWordsInfoes)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.BudgetGroupDeviceStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.BudgetGroupLedgers)
                .WithOptional(e => e.BudgetGroup2)
                .HasForeignKey(e => e.BudgetGroupProposalId);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.BudgetGroupStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.BudgetGroupTimedBudget2)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.BudgetGroupXrefAd2)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.BudgetGroupXrefConversionAction2)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.BudgetGroupXrefLocation2)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.BudgetGroupXrefPlacement2)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.OrderXrefBudgetGroups)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.RawBudgetGroupGenderConversionStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.RawBudgetGroupAdConversionStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.RawBudgetGroupAdDeviceConversionStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.RawBudgetGroupAdStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.RawBudgetGroupAgeGroupConversionStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.RawBudgetGroupAgeGroupStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.RawBudgetGroupConversionStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.RawBudgetGroupDeviceConversionStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.RawBudgetGroupDeviceStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.RawBudgetGroupGenderStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.RawBudgetGroupGeoConversionStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroup2>()
                .HasMany(e => e.RawBudgetGroupStats)
                .WithRequired(e => e.BudgetGroup2)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroupAdStat>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupAdStat>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupAdStat>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupAdStat>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupAdStat>()
                .Property(e => e.PartialViews)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupAdStat>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupAdStat>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupAdStat>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupAdStat>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStat>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStat>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupDeviceStat>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStat>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStat>()
                .Property(e => e.PartialViews)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStat>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupDeviceStat>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStat>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStat>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStat>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStat>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStat>()
                .Property(e => e.AverageCpc)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStat>()
                .Property(e => e.AverageCpm)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStat>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupStat>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStat>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupStat>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStat>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStat>()
                .Property(e => e.Reach)
                .HasPrecision(38, 6);

            modelBuilder.Entity<BudgetGroupStat>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStat>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStat>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStat>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStat>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStat>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStat>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStat>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStat>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStat>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStat>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStat>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStat>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStat>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStat>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStat>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStat>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTimedBudget2>()
                .Property(e => e.BudgetAmount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTimedBudget2>()
                .Property(e => e.Margin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.BudgetGroupDeleted)
                .IsFixedLength();

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.LocationDeleted)
                .IsFixedLength();

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.CustomerMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.GrossBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.SightlyBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.NetBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.GrossSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.NetSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.MTDGrossSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.MTDSightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.MTDNetSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.YesterdayNetSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.RemainingNetDailyBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.PacingPercent)
                .HasPrecision(38, 6);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.TrendingPacingPercent)
                .HasPrecision(38, 6);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.RemainingSightlyBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.ViewRate)
                .HasPrecision(38, 6);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.NetCPV)
                .HasPrecision(18, 6);

            modelBuilder.Entity<DailyPacing1>()
                .Property(e => e.SightlySpendToPreviousMonthsEnd)
                .HasPrecision(38, 6);

            modelBuilder.Entity<DeviceStat>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStat>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<DeviceStat>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStat>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStat>()
                .Property(e => e.PartialViews)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStat>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<DeviceStat>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStat>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStat>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Order>()
                .Property(e => e.CampaignBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Order>()
                .Property(e => e.MonthlyBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Order>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<Order>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<Order>()
                .Property(e => e.EstimatedCpcv)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Order>()
                .Property(e => e.AssumedViewRate)
                .HasPrecision(5, 4);

            modelBuilder.Entity<Order>()
                .Property(e => e.MinimumLocationBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.BudgetGroupAdStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.BudgetGroupDeviceStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.BudgetGroupTargetAgeGroupStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.BudgetGroupTargetGenderStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.BudgetGroupAdStats1)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.BudgetGroupDeviceStats1)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.BudgetGroupTargetAgeGroupStats1)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.BudgetGroupTargetGenderStats1)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.Order1)
                .WithOptional(e => e.Order2)
                .HasForeignKey(e => e.ParentOrderId);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderCompetitorUrls)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderConversionGatheringDurations)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderKeyTerms)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderRecordLogs)
                .WithRequired(e => e.Order)
                .HasForeignKey(e => e.OrderId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderRecordLogs1)
                .WithOptional(e => e.Order1)
                .HasForeignKey(e => e.ParentOrderId);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderSelectedChangeTypes)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderTargetAgeGroups)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderTargetGenders)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderTargetParentalStatus)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderXrefBudgetGroups)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderXrefConversionActions)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderXrefDeliveryKpis)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.OrderXrefObjectives)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawAdConversionStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawAdDeviceConversionStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawAdStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawAgeGroupConversionStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawAgeGroupStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawBudgetGroupAdStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawBudgetGroupAgeGroupStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawBudgetGroupDeviceStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawBudgetGroupGenderStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawDeviceConversionStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawDeviceStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawGenderConversionStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawGenderStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawGeoConversionStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawOrderConversionStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Order>()
                .HasMany(e => e.RawOrderStats)
                .WithRequired(e => e.Order)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrderBudgetPacing>()
                .Property(e => e.PacingRate)
                .HasPrecision(38, 6);

            modelBuilder.Entity<OrderBudgetPacing>()
                .Property(e => e.SpendToDate)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderBudgetPacing>()
                .Property(e => e.ProjectedFinalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderBudgetPacing>()
                .Property(e => e.RemainingBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderRecordLog>()
                .Property(e => e.CampaignBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderRecordLog>()
                .Property(e => e.MonthlyBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderRecordLog>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderRecordLog>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderRecordLog>()
                .Property(e => e.EstimatedCpcv)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderRecordLog>()
                .Property(e => e.AssumedViewRate)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderRecordLog>()
                .HasMany(e => e.ChangeOrderLogs)
                .WithRequired(e => e.OrderRecordLog)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrderStat>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStat>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStat>()
                .Property(e => e.AverageCpc)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStat>()
                .Property(e => e.AverageCpm)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStat>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderStat>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStat>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderStat>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStat>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStat>()
                .Property(e => e.Reach)
                .HasPrecision(38, 6);

            modelBuilder.Entity<OrderStat>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStat>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStat>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStat>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStat>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStat>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStat>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStat>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStat>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetGenderStat>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetGenderStat>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderTargetGenderStat>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetGenderStat>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetGenderStat>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderTargetGenderStat>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetGenderStat>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetGenderStat>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<Placement>()
                .Property(e => e.PlacementValue)
                .IsUnicode(false);

            modelBuilder.Entity<Placement>()
                .HasMany(e => e.BudgetGroupXrefPlacements)
                .WithRequired(e => e.Placement)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Placement>()
                .HasMany(e => e.BudgetGroupXrefPlacement1)
                .WithRequired(e => e.Placement)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Placement>()
                .HasMany(e => e.BudgetGroupXrefPlacement2)
                .WithRequired(e => e.Placement)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<RawAdStat>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawAgeGroupStat>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawBudgetGroupAdStat>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawBudgetGroupAgeGroupStat>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawBudgetGroupDeviceStat>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawBudgetGroupGenderStat>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawBudgetGroupStat>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawBudgetGroupStat>()
                .Property(e => e.AvgCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawBudgetGroupStat>()
                .Property(e => e.AvgCPC)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawBudgetGroupStat>()
                .Property(e => e.AvgCPM)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawBudgetGroupStat>()
                .Property(e => e.Budget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawDeviceStat>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawGenderStat>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawOrderStat>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawOrderStat>()
                .Property(e => e.AvgCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawOrderStat>()
                .Property(e => e.AvgCPC)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawOrderStat>()
                .Property(e => e.AvgCPM)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RawOrderStat>()
                .Property(e => e.Budget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdDeviceStat>()
                .Property(e => e.AdName)
                .IsUnicode(false);

            modelBuilder.Entity<AdDeviceStat>()
                .Property(e => e.CampaignName)
                .IsUnicode(false);

            modelBuilder.Entity<AdDeviceStat>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdDeviceStat>()
                .Property(e => e.Device)
                .IsUnicode(false);

            modelBuilder.Entity<AdDeviceStat>()
                .Property(e => e.AudienceRetention25)
                .HasPrecision(18, 4);

            modelBuilder.Entity<AdDeviceStat>()
                .Property(e => e.AudienceRetention50)
                .HasPrecision(18, 4);

            modelBuilder.Entity<AdDeviceStat>()
                .Property(e => e.AudienceRetention75)
                .HasPrecision(18, 4);

            modelBuilder.Entity<AdDeviceStat>()
                .Property(e => e.AudienceRetention100)
                .HasPrecision(18, 4);

            modelBuilder.Entity<AdDeviceStat>()
                .Property(e => e.Aud25xImpressions)
                .HasPrecision(18, 4);

            modelBuilder.Entity<AdDeviceStat>()
                .Property(e => e.Aud50xImpressions)
                .HasPrecision(18, 4);

            modelBuilder.Entity<AdDeviceStat>()
                .Property(e => e.Aud75xImpressions)
                .HasPrecision(18, 4);

            modelBuilder.Entity<AdDeviceStat>()
                .Property(e => e.Aud100xImpressions)
                .HasPrecision(18, 4);

            modelBuilder.Entity<AgeRangeStat>()
                .Property(e => e.AgeRangeName)
                .IsUnicode(false);

            modelBuilder.Entity<AgeRangeStat>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<GenderStat>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<GenderStat>()
                .Property(e => e.GenderName)
                .IsUnicode(false);

            modelBuilder.Entity<AdStats1>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdStats1>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<AdStats1>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdStats1>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdStats1>()
                .Property(e => e.PartialViews)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdStats1>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<AdStats1>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdStats1>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdStats1>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupAdStats1>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupAdStats1>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupAdStats1>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupAdStats1>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupAdStats1>()
                .Property(e => e.PartialViews)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupAdStats1>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupAdStats1>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupAdStats1>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupAdStats1>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStats1>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStats1>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupDeviceStats1>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStats1>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStats1>()
                .Property(e => e.PartialViews)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStats1>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupDeviceStats1>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStats1>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupDeviceStats1>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStats1>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStats1>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStats1>()
                .Property(e => e.AverageCpc)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStats1>()
                .Property(e => e.AverageCpm)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStats1>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupStats1>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStats1>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupStats1>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStats1>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupStats1>()
                .Property(e => e.Reach)
                .HasPrecision(38, 6);

            modelBuilder.Entity<BudgetGroupStats1>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStats1>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStats1>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStats1>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStats1>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStats1>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStats1>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStats1>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetAgeGroupStats1>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStats1>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStats1>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStats1>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStats1>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStats1>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStats1>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStats1>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<BudgetGroupTargetGenderStats1>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStats1>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStats1>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<DeviceStats1>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStats1>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStats1>()
                .Property(e => e.PartialViews)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStats1>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<DeviceStats1>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStats1>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStats1>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStats1>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStats1>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStats1>()
                .Property(e => e.AverageCpc)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStats1>()
                .Property(e => e.AverageCpm)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStats1>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderStats1>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStats1>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderStats1>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStats1>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStats1>()
                .Property(e => e.Reach)
                .HasPrecision(38, 6);

            modelBuilder.Entity<OrderStats1>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStats1>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStats1>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStats1>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStats1>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStats1>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStats1>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStats1>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetAgeGroupStats1>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetGenderStats1>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetGenderStats1>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderTargetGenderStats1>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetGenderStats1>()
                .Property(e => e.EstimatedCost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetGenderStats1>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderTargetGenderStats1>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetGenderStats1>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderTargetGenderStats1>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AgencyMcc>()
                .Property(e => e.AgencyMccName)
                .IsUnicode(false);

            modelBuilder.Entity<PropertyType>()
                .HasMany(e => e.BudgetGroupTimedBudgetProperties)
                .WithRequired(e => e.PropertyType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<Role>()
                .Property(e => e.Description)
                .IsUnicode(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.RoleClaims)
                .WithRequired(e => e.Role)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Role>()
                .HasMany(e => e.Users)
                .WithMany(e => e.Roles)
                .Map(m => m.ToTable("UserRole", "Users").MapLeftKey("RoleId").MapRightKey("UserId"));

            modelBuilder.Entity<RoleClaim>()
                .Property(e => e.ClaimType)
                .IsUnicode(false);

            modelBuilder.Entity<RoleClaim>()
                .Property(e => e.ClaimValue)
                .IsUnicode(false);

            modelBuilder.Entity<RoleClaim>()
                .Property(e => e.ClaimValueType)
                .IsUnicode(false);

            modelBuilder.Entity<RoleClaim>()
                .Property(e => e.ClaimTarget)
                .IsUnicode(false);

            modelBuilder.Entity<RoleClaim>()
                .Property(e => e.ClaimTargetType)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Accounts)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.AdminId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Estimates)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroups)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroups1)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.ModifiedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroupTimedBudgets)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroupXrefAds)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroupXrefConversionActions)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroupXrefLocations)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroupXrefPlacements)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroup1)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroup11)
                .WithOptional(e => e.User1)
                .HasForeignKey(e => e.ModifiedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroupTimedBudget1)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroupXrefAd1)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroupXrefConversionAction1)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroupXrefLocation1)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CreatedBy);

            modelBuilder.Entity<User>()
                .HasMany(e => e.BudgetGroupXrefPlacement1)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatedBy)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Ads)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Orders)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.CancelerId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Orders1)
                .WithRequired(e => e.User1)
                .HasForeignKey(e => e.CreatorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.Orders2)
                .WithOptional(e => e.User2)
                .HasForeignKey(e => e.SubmitterId);

            modelBuilder.Entity<User>()
                .HasMany(e => e.OrderRecordLogs)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.CreatorId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserXrefAccounts)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserXrefAdvertisers)
                .WithRequired(e => e.User)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AdvertiserCategory>()
                .Property(e => e.Cpcv)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdvertiserCategory>()
                .HasMany(e => e.Advertisers)
                .WithRequired(e => e.AdvertiserCategory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AdvertiserCategory>()
                .HasMany(e => e.AdvertiserSubCategories)
                .WithRequired(e => e.AdvertiserCategory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AdvertiserSubCategory>()
                .Property(e => e.Cpcv)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AgeGroup>()
                .Property(e => e.PopulationPercentage)
                .HasPrecision(5, 4);

            modelBuilder.Entity<AgeGroup>()
                .HasMany(e => e.EstimateTargetAgeGroups)
                .WithRequired(e => e.AgeGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgeGroup>()
                .HasMany(e => e.BudgetGroupTargetAgeGroupStats)
                .WithRequired(e => e.AgeGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgeGroup>()
                .HasMany(e => e.OrderTargetAgeGroups)
                .WithRequired(e => e.AgeGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgeGroup>()
                .HasMany(e => e.OrderTargetAgeGroupStats)
                .WithRequired(e => e.AgeGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgeGroup>()
                .HasMany(e => e.RawAgeGroupConversionStats)
                .WithRequired(e => e.AgeGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgeGroup>()
                .HasMany(e => e.RawAgeGroupStats)
                .WithRequired(e => e.AgeGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgeGroup>()
                .HasMany(e => e.RawBudgetGroupAgeGroupConversionStats)
                .WithRequired(e => e.AgeGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgeGroup>()
                .HasMany(e => e.RawBudgetGroupAgeGroupStats)
                .WithRequired(e => e.AgeGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgeGroup>()
                .HasMany(e => e.BudgetGroupTargetAgeGroupStats1)
                .WithRequired(e => e.AgeGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<AgeGroup>()
                .HasMany(e => e.OrderTargetAgeGroupStats1)
                .WithRequired(e => e.AgeGroup)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroupStatus>()
                .HasMany(e => e.BudgetGroups)
                .WithRequired(e => e.BudgetGroupStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetGroupStatus>()
                .HasMany(e => e.BudgetGroup1)
                .WithRequired(e => e.BudgetGroupStatus)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetPacingDateRange>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<BudgetType>()
                .HasMany(e => e.OrderRecordLogs)
                .WithRequired(e => e.BudgetType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ChangeType>()
                .HasMany(e => e.ChangeOrderLogs)
                .WithRequired(e => e.ChangeType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ChangeType>()
                .HasMany(e => e.OrderSelectedChangeTypes)
                .WithRequired(e => e.ChangeType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionActionTrackingStatu>()
                .HasMany(e => e.ConversionActions)
                .WithRequired(e => e.ConversionActionTrackingStatu)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionCount>()
                .HasMany(e => e.ConversionActions)
                .WithRequired(e => e.ConversionCount)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionTrackingType>()
                .HasMany(e => e.ConversionActions)
                .WithRequired(e => e.ConversionTrackingType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionType>()
                .HasMany(e => e.ConversionActions)
                .WithRequired(e => e.ConversionType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionWindowsFromClick>()
                .HasMany(e => e.ConversionActions)
                .WithRequired(e => e.ConversionWindowsFromClick)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ConversionWindowsFromImpression>()
                .HasMany(e => e.ConversionActions)
                .WithRequired(e => e.ConversionWindowsFromImpression)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeliveryKpi>()
                .HasMany(e => e.OrderXrefDeliveryKpis)
                .WithRequired(e => e.DeliveryKpi)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeviceType>()
                .HasMany(e => e.BudgetGroupDeviceStats)
                .WithRequired(e => e.DeviceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeviceType>()
                .HasMany(e => e.DeviceStats)
                .WithRequired(e => e.DeviceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeviceType>()
                .HasMany(e => e.RawAdDeviceConversionStats)
                .WithRequired(e => e.DeviceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeviceType>()
                .HasMany(e => e.RawAdStats)
                .WithRequired(e => e.DeviceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeviceType>()
                .HasMany(e => e.RawBudgetGroupAdDeviceConversionStats)
                .WithRequired(e => e.DeviceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeviceType>()
                .HasMany(e => e.RawBudgetGroupAdStats)
                .WithRequired(e => e.DeviceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeviceType>()
                .HasMany(e => e.RawBudgetGroupDeviceConversionStats)
                .WithRequired(e => e.DeviceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeviceType>()
                .HasMany(e => e.RawBudgetGroupDeviceStats)
                .WithRequired(e => e.DeviceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeviceType>()
                .HasMany(e => e.RawDeviceConversionStats)
                .WithRequired(e => e.DeviceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeviceType>()
                .HasMany(e => e.RawDeviceStats)
                .WithRequired(e => e.DeviceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeviceType>()
                .HasMany(e => e.BudgetGroupDeviceStats1)
                .WithRequired(e => e.DeviceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<DeviceType>()
                .HasMany(e => e.DeviceStats1)
                .WithRequired(e => e.DeviceType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<EstimateResultComparableView>()
                .HasMany(e => e.Estimates)
                .WithRequired(e => e.EstimateResultComparableView)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gender>()
                .Property(e => e.PopulationPercentage)
                .HasPrecision(5, 4);

            modelBuilder.Entity<Gender>()
                .HasMany(e => e.EstimateTargetGenders)
                .WithRequired(e => e.Gender)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gender>()
                .HasMany(e => e.BudgetGroupTargetGenderStats)
                .WithRequired(e => e.Gender)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gender>()
                .HasMany(e => e.OrderTargetGenders)
                .WithRequired(e => e.Gender)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gender>()
                .HasMany(e => e.OrderTargetGenderStats)
                .WithRequired(e => e.Gender)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gender>()
                .HasMany(e => e.RawBudgetGroupGenderConversionStats)
                .WithRequired(e => e.Gender)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gender>()
                .HasMany(e => e.RawBudgetGroupGenderStats)
                .WithRequired(e => e.Gender)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gender>()
                .HasMany(e => e.RawGenderConversionStats)
                .WithRequired(e => e.Gender)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gender>()
                .HasMany(e => e.RawGenderStats)
                .WithRequired(e => e.Gender)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gender>()
                .HasMany(e => e.BudgetGroupTargetGenderStats1)
                .WithRequired(e => e.Gender)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Gender>()
                .HasMany(e => e.OrderTargetGenderStats1)
                .WithRequired(e => e.Gender)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Geography>()
                .HasMany(e => e.LocationDetails)
                .WithRequired(e => e.Geography)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<GeographyType>()
                .HasMany(e => e.Geographies)
                .WithRequired(e => e.GeographyType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Objective>()
                .HasMany(e => e.OrderXrefObjectives)
                .WithRequired(e => e.Objective)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ObjectiveCategory>()
                .HasMany(e => e.DeliveryKpis)
                .WithRequired(e => e.ObjectiveCategory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ObjectiveCategory>()
                .HasMany(e => e.Objectives)
                .WithRequired(e => e.ObjectiveCategory)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrderType>()
                .HasMany(e => e.Orders)
                .WithRequired(e => e.OrderType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<OrderType>()
                .HasMany(e => e.OrderRecordLogs)
                .WithRequired(e => e.OrderType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ParentalStatu>()
                .HasMany(e => e.EstimateTargetParentalStatus)
                .WithRequired(e => e.ParentalStatu)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ParentalStatu>()
                .HasMany(e => e.OrderTargetParentalStatus)
                .WithRequired(e => e.ParentalStatu)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TargetingType>()
                .Property(e => e.CpcvMultiplier)
                .HasPrecision(19, 4);

            modelBuilder.Entity<TargetingType>()
                .HasMany(e => e.Estimates)
                .WithRequired(e => e.TargetingType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TargetingType>()
                .HasMany(e => e.OrderRecordLogs)
                .WithRequired(e => e.TargetingType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ValuePerConversion>()
                .HasMany(e => e.ConversionActions)
                .WithRequired(e => e.ValuePerConversion1)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<BudgetProcessingLog>()
                .Property(e => e.CID)
                .IsFixedLength();

            modelBuilder.Entity<BudgetProcessingLog>()
                .Property(e => e.DailyBudget)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AggregatedCampaignAgeGroupStatsView>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AggregatedCampaignDeviceStatsView>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AggregatedCampaignGenderStatsView>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStatsView_20170829>()
                .Property(e => e.ActualSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStatsView_20170829>()
                .Property(e => e.Amount)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStatsView_20170829>()
                .Property(e => e.AverageCpc)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStatsView_20170829>()
                .Property(e => e.AverageCpm)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStatsView_20170829>()
                .Property(e => e.CustomerAccountMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderStatsView_20170829>()
                .Property(e => e.CustomerAccountSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStatsView_20170829>()
                .Property(e => e.SightlyMargin)
                .HasPrecision(5, 4);

            modelBuilder.Entity<OrderStatsView_20170829>()
                .Property(e => e.SightlySpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStatsView_20170829>()
                .Property(e => e.TotalSpend)
                .HasPrecision(19, 4);

            modelBuilder.Entity<OrderStatsView_20170829>()
                .Property(e => e.Reach)
                .HasPrecision(38, 6);

            modelBuilder.Entity<OrderStatsView_20170829>()
                .Property(e => e.AvgCPCV)
                .HasPrecision(19, 4);

            modelBuilder.Entity<RL_Daily>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AdStatsByDate>()
                .Property(e => e.AdName)
                .IsUnicode(false);

            modelBuilder.Entity<AdStatsByDate>()
                .Property(e => e.CampaignName)
                .IsUnicode(false);

            modelBuilder.Entity<AdStatsByDate>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<AgeStatsByDate>()
                .Property(e => e.AgeRangeName)
                .IsUnicode(false);

            modelBuilder.Entity<AgeStatsByDate>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<CampaignStatsByDate>()
                .Property(e => e.CampaignName)
                .IsUnicode(false);

            modelBuilder.Entity<CampaignStatsByDate>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<DeviceStatsByDate>()
                .Property(e => e.CampaignName)
                .IsUnicode(false);

            modelBuilder.Entity<DeviceStatsByDate>()
                .Property(e => e.Device)
                .IsUnicode(false);

            modelBuilder.Entity<DeviceStatsByDate>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);

            modelBuilder.Entity<GenderStatsByDate>()
                .Property(e => e.GenderName)
                .IsUnicode(false);

            modelBuilder.Entity<GenderStatsByDate>()
                .Property(e => e.Cost)
                .HasPrecision(19, 4);
        }
    }
}
