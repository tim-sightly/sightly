namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.ObjectiveCategory")]
    public partial class ObjectiveCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ObjectiveCategory()
        {
            DeliveryKpis = new HashSet<DeliveryKpi>();
            DeliveryPriorities = new HashSet<DeliveryPriority>();
            Objectives = new HashSet<Objective>();
        }

        public Guid ObjectiveCategoryId { get; set; }

        [Required]
        [StringLength(256)]
        public string ObjectiveCategoryName { get; set; }

        public int SortOrder { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeliveryKpi> DeliveryKpis { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeliveryPriority> DeliveryPriorities { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Objective> Objectives { get; set; }
    }
}
