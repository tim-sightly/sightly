namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Hist.BudgetGroupXrefLocation")]
    public partial class BudgetGroupXrefLocation
    {
        [Key]
        [Column(Order = 0)]
        public Guid BudgetGroupXrefLocationId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid BudgetGroupId { get; set; }

        public Guid LocationId { get; set; }

        public Guid? CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Created { get; set; }

        public virtual Location Location { get; set; }

        public virtual BudgetGroup BudgetGroup { get; set; }

        public virtual User User { get; set; }
    }
}
