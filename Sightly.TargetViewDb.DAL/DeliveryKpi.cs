namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.DeliveryKpi")]
    public partial class DeliveryKpi
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DeliveryKpi()
        {
            OrderXrefDeliveryKpis = new HashSet<OrderXrefDeliveryKpi>();
        }

        public Guid DeliveryKpiId { get; set; }

        [Required]
        [StringLength(512)]
        public string DeliveryKpiName { get; set; }

        public Guid ObjectiveCategoryId { get; set; }

        public int SortOrder { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderXrefDeliveryKpi> OrderXrefDeliveryKpis { get; set; }

        public virtual ObjectiveCategory ObjectiveCategory { get; set; }
    }
}
