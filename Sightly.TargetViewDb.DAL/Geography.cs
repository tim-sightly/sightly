namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.Geography")]
    public partial class Geography
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Geography()
        {
            LocationDetails = new HashSet<LocationDetail>();
        }

        public Guid GeographyId { get; set; }

        public Guid GeographyTypeId { get; set; }

        [Required]
        [StringLength(128)]
        public string GeographyName { get; set; }

        [StringLength(64)]
        public string ZipCode { get; set; }

        [StringLength(128)]
        public string CanonicalName { get; set; }

        [StringLength(128)]
        public string CanonicalShortName { get; set; }

        public long? Population { get; set; }

        public Guid? ParentGeographyId { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastModifiedDatetime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LocationDetail> LocationDetails { get; set; }

        public virtual GeographyType GeographyType { get; set; }
    }
}
