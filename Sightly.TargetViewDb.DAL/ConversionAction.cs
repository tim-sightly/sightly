namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Accounts.ConversionAction")]
    public partial class ConversionAction
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ConversionAction()
        {
            AdvertiserXrefConversionActions = new HashSet<AdvertiserXrefConversionAction>();
            BudgetGroupXrefConversionActions = new HashSet<BudgetGroupXrefConversionAction>();
            BudgetGroupXrefConversionAction1 = new HashSet<BudgetGroupXrefConversionAction1>();
            BudgetGroupXrefConversionAction2 = new HashSet<BudgetGroupXrefConversionAction2>();
            OrderConversionGatheringDurations = new HashSet<OrderConversionGatheringDuration>();
            OrderXrefConversionActions = new HashSet<OrderXrefConversionAction>();
            RawAdConversionStats = new HashSet<RawAdConversionStat>();
            RawAdDeviceConversionStats = new HashSet<RawAdDeviceConversionStat>();
            RawAgeGroupConversionStats = new HashSet<RawAgeGroupConversionStat>();
            RawBudgetGroupGenderConversionStats = new HashSet<RawBudgetGroupGenderConversionStat>();
            RawBudgetGroupAdConversionStats = new HashSet<RawBudgetGroupAdConversionStat>();
            RawBudgetGroupAdDeviceConversionStats = new HashSet<RawBudgetGroupAdDeviceConversionStat>();
            RawBudgetGroupAgeGroupConversionStats = new HashSet<RawBudgetGroupAgeGroupConversionStat>();
            RawBudgetGroupConversionStats = new HashSet<RawBudgetGroupConversionStat>();
            RawBudgetGroupDeviceConversionStats = new HashSet<RawBudgetGroupDeviceConversionStat>();
            RawBudgetGroupGeoConversionStats = new HashSet<RawBudgetGroupGeoConversionStat>();
            RawDeviceConversionStats = new HashSet<RawDeviceConversionStat>();
            RawGenderConversionStats = new HashSet<RawGenderConversionStat>();
            RawGeoConversionStats = new HashSet<RawGeoConversionStat>();
            RawOrderConversionStats = new HashSet<RawOrderConversionStat>();
        }

        public Guid ConversionActionId { get; set; }

        public Guid AccountId { get; set; }

        public Guid AdvertiserId { get; set; }

        public bool AvailableForAllAdvertisers { get; set; }

        [Required]
        public string ConversionActionName { get; set; }

        public string ConversionActionTag { get; set; }

        public Guid ConversionActionTrackingStatusId { get; set; }

        public Guid ConversionCountId { get; set; }

        public Guid ConversionTypeId { get; set; }

        public Guid ConversionTrackingTypeId { get; set; }

        public Guid ConversionWindowsFromClicksId { get; set; }

        public int? ConversionWindowsFromClicksCustom { get; set; }

        public Guid ConversionWindowsFromImpressionsId { get; set; }

        public int? ConversionWindowsFromImpressionsCustom { get; set; }

        public bool Deleted { get; set; }

        public string Description { get; set; }

        [Column(TypeName = "date")]
        public DateTime? LastConversionRecorded { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastSeenDatetime { get; set; }

        [Column(TypeName = "money")]
        public decimal? ValuePerConversion { get; set; }

        public Guid ValuePerConversionId { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Account Account { get; set; }

        public virtual Advertiser Advertiser { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdvertiserXrefConversionAction> AdvertiserXrefConversionActions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefConversionAction> BudgetGroupXrefConversionActions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefConversionAction1> BudgetGroupXrefConversionAction1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefConversionAction2> BudgetGroupXrefConversionAction2 { get; set; }

        public virtual ConversionActionTrackingStatu ConversionActionTrackingStatu { get; set; }

        public virtual ConversionCount ConversionCount { get; set; }

        public virtual ConversionTrackingType ConversionTrackingType { get; set; }

        public virtual ConversionType ConversionType { get; set; }

        public virtual ConversionWindowsFromClick ConversionWindowsFromClick { get; set; }

        public virtual ConversionWindowsFromImpression ConversionWindowsFromImpression { get; set; }

        public virtual ValuePerConversion ValuePerConversion1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderConversionGatheringDuration> OrderConversionGatheringDurations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderXrefConversionAction> OrderXrefConversionActions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAdConversionStat> RawAdConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAdDeviceConversionStat> RawAdDeviceConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAgeGroupConversionStat> RawAgeGroupConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupGenderConversionStat> RawBudgetGroupGenderConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAdConversionStat> RawBudgetGroupAdConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAdDeviceConversionStat> RawBudgetGroupAdDeviceConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAgeGroupConversionStat> RawBudgetGroupAgeGroupConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupConversionStat> RawBudgetGroupConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupDeviceConversionStat> RawBudgetGroupDeviceConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupGeoConversionStat> RawBudgetGroupGeoConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawDeviceConversionStat> RawDeviceConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawGenderConversionStat> RawGenderConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawGeoConversionStat> RawGeoConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawOrderConversionStat> RawOrderConversionStats { get; set; }
    }
}
