namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Hist.BudgetGroupXrefPlacement")]
    public partial class BudgetGroupXrefPlacement
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BudgetGroupXrefPlacement()
        {
            BudgetGroupTimedBudgets = new HashSet<BudgetGroupTimedBudget>();
        }

        public Guid BudgetGroupXrefPlacementId { get; set; }

        public Guid BudgetGroupId { get; set; }

        public Guid PlacementId { get; set; }

        public Guid CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Created { get; set; }

        public virtual BudgetGroup BudgetGroup { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTimedBudget> BudgetGroupTimedBudgets { get; set; }

        public virtual Placement Placement { get; set; }

        public virtual User User { get; set; }
    }
}
