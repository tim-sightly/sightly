namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.AdvertiserCategory")]
    public partial class AdvertiserCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AdvertiserCategory()
        {
            Advertisers = new HashSet<Advertiser>();
            AdvertiserSubCategories = new HashSet<AdvertiserSubCategory>();
        }

        public Guid AdvertiserCategoryId { get; set; }

        [Required]
        [StringLength(128)]
        public string AdvertiserCategoryName { get; set; }

        [Column(TypeName = "money")]
        public decimal Cpcv { get; set; }

        [Required]
        [StringLength(256)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(256)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Advertiser> Advertisers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdvertiserSubCategory> AdvertiserSubCategories { get; set; }
    }
}
