namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.AggregatedCampaignAgeGroupStatsView")]
    public partial class AggregatedCampaignAgeGroupStatsView
    {
        public Guid? CampaignId { get; set; }

        [Key]
        public Guid AgeGroupId { get; set; }

        public long? CompletedViews { get; set; }

        [Column(TypeName = "money")]
        public decimal? AvgCPCV { get; set; }

        public double? ViewRate { get; set; }

        public long? Impressions { get; set; }

        public long? Clicks { get; set; }
    }
}
