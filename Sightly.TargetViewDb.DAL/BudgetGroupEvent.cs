namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.BudgetGroupEvents")]
    public partial class BudgetGroupEvent
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BudgetGroupEvent()
        {
            BudgetGroupLedgers = new HashSet<BudgetGroupLedger>();
        }

        public byte BudgetGroupEventId { get; set; }

        [Required]
        [StringLength(32)]
        public string BudgetGroupName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupLedger> BudgetGroupLedgers { get; set; }
    }
}
