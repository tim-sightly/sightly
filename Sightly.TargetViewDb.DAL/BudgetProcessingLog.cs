namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.BudgetProcessingLog")]
    public partial class BudgetProcessingLog
    {
        [Key]
        [Column(Order = 0)]
        public int BudgetProcessingLogID { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(12)]
        public string CID { get; set; }

        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long CampaignID { get; set; }

        [Key]
        [Column(Order = 3, TypeName = "money")]
        public decimal DailyBudget { get; set; }

        public int? Status { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastModifiedDatetime { get; set; }
    }
}
