namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Live.BudgetGroup")]
    public partial class BudgetGroup1
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BudgetGroup1()
        {
            BudgetGroupLedgers = new HashSet<BudgetGroupLedger>();
            BudgetGroupTimedBudget1 = new HashSet<BudgetGroupTimedBudget1>();
            BudgetGroupXrefAd1 = new HashSet<BudgetGroupXrefAd1>();
            BudgetGroupXrefConversionAction1 = new HashSet<BudgetGroupXrefConversionAction1>();
            BudgetGroupXrefLocation1 = new HashSet<BudgetGroupXrefLocation1>();
            BudgetGroupXrefPlacement1 = new HashSet<BudgetGroupXrefPlacement1>();
        }

        [Key]
        public Guid BudgetGroupId { get; set; }

        public byte BudgetGroupStatusId { get; set; }

        [StringLength(256)]
        public string BudgetGroupName { get; set; }

        public Guid? CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Created { get; set; }

        public Guid? ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? Modified { get; set; }

        public virtual BudgetGroupStatus BudgetGroupStatus { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupLedger> BudgetGroupLedgers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTimedBudget1> BudgetGroupTimedBudget1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefAd1> BudgetGroupXrefAd1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefConversionAction1> BudgetGroupXrefConversionAction1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefLocation1> BudgetGroupXrefLocation1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefPlacement1> BudgetGroupXrefPlacement1 { get; set; }
    }
}
