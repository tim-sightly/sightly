namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Accounts.AdvertiserXrefConversionAction")]
    public partial class AdvertiserXrefConversionAction
    {
        public Guid AdvertiserXrefConversionActionId { get; set; }

        public Guid AdvertiserId { get; set; }

        public Guid ConversionActionId { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Advertiser Advertiser { get; set; }

        public virtual ConversionAction ConversionAction { get; set; }
    }
}
