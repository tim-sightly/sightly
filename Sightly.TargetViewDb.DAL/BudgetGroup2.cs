namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.BudgetGroup")]
    public partial class BudgetGroup2
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BudgetGroup2()
        {
            AdAdWordsInfoes = new HashSet<AdAdWordsInfo>();
            BudgetGroupAdStats = new HashSet<BudgetGroupAdStat>();
            BudgetGroupAdWordsInfoes = new HashSet<BudgetGroupAdWordsInfo>();
            BudgetGroupDeviceStats = new HashSet<BudgetGroupDeviceStat>();
            BudgetGroupLedgers = new HashSet<BudgetGroupLedger>();
            BudgetGroupStats = new HashSet<BudgetGroupStat>();
            BudgetGroupTargetAgeGroupStats = new HashSet<BudgetGroupTargetAgeGroupStat>();
            BudgetGroupTargetGenderStats = new HashSet<BudgetGroupTargetGenderStat>();
            BudgetGroupTimedBudget2 = new HashSet<BudgetGroupTimedBudget2>();
            BudgetGroupXrefAd2 = new HashSet<BudgetGroupXrefAd2>();
            BudgetGroupXrefConversionAction2 = new HashSet<BudgetGroupXrefConversionAction2>();
            BudgetGroupXrefLocation2 = new HashSet<BudgetGroupXrefLocation2>();
            BudgetGroupXrefPlacement2 = new HashSet<BudgetGroupXrefPlacement2>();
            OrderXrefBudgetGroups = new HashSet<OrderXrefBudgetGroup>();
            RawBudgetGroupGenderConversionStats = new HashSet<RawBudgetGroupGenderConversionStat>();
            RawBudgetGroupAdConversionStats = new HashSet<RawBudgetGroupAdConversionStat>();
            RawBudgetGroupAdDeviceConversionStats = new HashSet<RawBudgetGroupAdDeviceConversionStat>();
            RawBudgetGroupAdStats = new HashSet<RawBudgetGroupAdStat>();
            RawBudgetGroupAgeGroupConversionStats = new HashSet<RawBudgetGroupAgeGroupConversionStat>();
            RawBudgetGroupAgeGroupStats = new HashSet<RawBudgetGroupAgeGroupStat>();
            RawBudgetGroupConversionStats = new HashSet<RawBudgetGroupConversionStat>();
            RawBudgetGroupDeviceConversionStats = new HashSet<RawBudgetGroupDeviceConversionStat>();
            RawBudgetGroupDeviceStats = new HashSet<RawBudgetGroupDeviceStat>();
            RawBudgetGroupGenderStats = new HashSet<RawBudgetGroupGenderStat>();
            RawBudgetGroupGeoConversionStats = new HashSet<RawBudgetGroupGeoConversionStat>();
            RawBudgetGroupStats = new HashSet<RawBudgetGroupStat>();
        }

        [Key]
        public Guid BudgetGroupId { get; set; }

        public Guid? AdId { get; set; }

        public Guid? AudienceId { get; set; }

        [Column(TypeName = "money")]
        public decimal? BudgetGroupBudget { get; set; }

        [StringLength(256)]
        public string BudgetGroupName { get; set; }

        public bool Deleted { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Ad Ad { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdAdWordsInfo> AdAdWordsInfoes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupAdStat> BudgetGroupAdStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupAdWordsInfo> BudgetGroupAdWordsInfoes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupDeviceStat> BudgetGroupDeviceStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupLedger> BudgetGroupLedgers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupStat> BudgetGroupStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTargetAgeGroupStat> BudgetGroupTargetAgeGroupStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTargetGenderStat> BudgetGroupTargetGenderStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTimedBudget2> BudgetGroupTimedBudget2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefAd2> BudgetGroupXrefAd2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefConversionAction2> BudgetGroupXrefConversionAction2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefLocation2> BudgetGroupXrefLocation2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefPlacement2> BudgetGroupXrefPlacement2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderXrefBudgetGroup> OrderXrefBudgetGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupGenderConversionStat> RawBudgetGroupGenderConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAdConversionStat> RawBudgetGroupAdConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAdDeviceConversionStat> RawBudgetGroupAdDeviceConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAdStat> RawBudgetGroupAdStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAgeGroupConversionStat> RawBudgetGroupAgeGroupConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAgeGroupStat> RawBudgetGroupAgeGroupStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupConversionStat> RawBudgetGroupConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupDeviceConversionStat> RawBudgetGroupDeviceConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupDeviceStat> RawBudgetGroupDeviceStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupGenderStat> RawBudgetGroupGenderStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupGeoConversionStat> RawBudgetGroupGeoConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupStat> RawBudgetGroupStats { get; set; }
    }
}
