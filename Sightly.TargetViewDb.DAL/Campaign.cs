namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Accounts.Campaign")]
    public partial class Campaign
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Campaign()
        {
            CampaignAdWordsInfoes = new HashSet<CampaignAdWordsInfo>();
            Orders = new HashSet<Order>();
            OrderRecordLogs = new HashSet<OrderRecordLog>();
        }

        public Guid CampaignId { get; set; }

        public Guid AccountId { get; set; }

        public Guid? AdvertiserId { get; set; }

        [Required]
        [StringLength(512)]
        public string CampaignName { get; set; }

        [StringLength(128)]
        public string CampaignRefCode { get; set; }

        public decimal? SightlyMargin { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? SightlyMarginLastModifiedDatetime { get; set; }

        public bool Deleted { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastModifiedDatetime { get; set; }

        public virtual Account Account { get; set; }

        public virtual Advertiser Advertiser { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CampaignAdWordsInfo> CampaignAdWordsInfoes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order> Orders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderRecordLog> OrderRecordLogs { get; set; }
    }
}
