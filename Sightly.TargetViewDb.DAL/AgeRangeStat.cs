namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("raw.AgeRangeStats")]
    public partial class AgeRangeStat
    {
        [Key]
        public long AgeRangeStatsId { get; set; }

        public long AdGroupId { get; set; }

        [Required]
        [StringLength(50)]
        public string AgeRangeName { get; set; }

        public long CampaignId { get; set; }

        public long Clicks { get; set; }

        [Column(TypeName = "money")]
        public decimal Cost { get; set; }

        public long Impressions { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime PulledDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StatDate { get; set; }

        public long TotalViews { get; set; }
    }
}
