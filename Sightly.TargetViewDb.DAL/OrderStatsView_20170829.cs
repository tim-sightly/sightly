namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.OrderStatsView_20170829")]
    public partial class OrderStatsView_20170829
    {
        public Guid? OrderId { get; set; }

        [Column(TypeName = "money")]
        public decimal? ActualSpend { get; set; }

        [Column(TypeName = "money")]
        public decimal? Amount { get; set; }

        [Column(TypeName = "money")]
        public decimal? AverageCpc { get; set; }

        public double? AverageCpe { get; set; }

        [Column(TypeName = "money")]
        public decimal? AverageCpm { get; set; }

        public double? AverageCpv { get; set; }

        public double? AveragePosition { get; set; }

        public int? AudienceRetention25 { get; set; }

        public int? AudienceRetention50 { get; set; }

        public int? AudienceRetention75 { get; set; }

        public int? AudienceRetention100 { get; set; }

        public long? Clicks { get; set; }

        public double? ClickThroughRate { get; set; }

        public long? CompletedViews { get; set; }

        public double? Conversions { get; set; }

        public decimal? CustomerAccountMargin { get; set; }

        [Column(TypeName = "money")]
        public decimal? CustomerAccountSpend { get; set; }

        public long? Engagements { get; set; }

        public long? Impressions { get; set; }

        public long? Interactions { get; set; }

        public decimal? SightlyMargin { get; set; }

        [Column(TypeName = "money")]
        public decimal? SightlySpend { get; set; }

        [Key]
        [Column(TypeName = "date")]
        public DateTime StatDate { get; set; }

        [Column(TypeName = "money")]
        public decimal? TotalSpend { get; set; }

        public double? ViewRate { get; set; }

        public long? ViewThroughConversions { get; set; }

        public decimal? Reach { get; set; }

        public long? ViewTime { get; set; }

        [Column(TypeName = "money")]
        public decimal? AvgCPCV { get; set; }
    }
}
