namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Accounts.AccountBillingAddress")]
    public partial class AccountBillingAddress
    {
        public Guid AccountBillingAddressId { get; set; }

        public Guid AccountId { get; set; }

        [Required]
        [StringLength(256)]
        public string CompanyName { get; set; }

        [Required]
        [StringLength(256)]
        public string AddressLine1 { get; set; }

        [StringLength(256)]
        public string AddressLine2 { get; set; }

        [Required]
        [StringLength(64)]
        public string City { get; set; }

        [Required]
        [StringLength(64)]
        public string State { get; set; }

        [Required]
        [StringLength(64)]
        public string ZipCode { get; set; }

        [StringLength(64)]
        public string MainPhone { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastModifiedDatetime { get; set; }

        public virtual Account Account { get; set; }
    }
}
