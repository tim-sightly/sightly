namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Users.ApiSession")]
    public partial class ApiSession
    {
        public Guid ApiSessionId { get; set; }

        public Guid UserId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LoginTime { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LogoutTime { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastRequestTime { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime ExpirationTime { get; set; }
    }
}
