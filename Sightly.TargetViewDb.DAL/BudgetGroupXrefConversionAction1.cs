namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Live.BudgetGroupXrefConversionAction")]
    public partial class BudgetGroupXrefConversionAction1
    {
        [Key]
        public Guid BudgetGroupXrefConversionActionId { get; set; }

        public Guid BudgetGroupId { get; set; }

        public Guid ConversionActionId { get; set; }

        public Guid CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Created { get; set; }

        public virtual ConversionAction ConversionAction { get; set; }

        public virtual BudgetGroup1 BudgetGroup1 { get; set; }

        public virtual User User { get; set; }
    }
}
