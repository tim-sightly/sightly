namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("adw.DailyPacing")]
    public partial class DailyPacing
    {
        [Key]
        public int PacingId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime PacingDate { get; set; }

        public long CustomerId { get; set; }

        public long CampaignId { get; set; }

        [Column(TypeName = "money")]
        public decimal RecomendedBudget { get; set; }

        public double OverPaceRate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "money")]
        public decimal TotalBudget { get; set; }

        public int DaysRemaining { get; set; }

        public Guid BudgetGroupTimedBudgetId { get; set; }
    }
}
