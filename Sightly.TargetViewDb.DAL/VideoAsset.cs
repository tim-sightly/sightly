namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Accounts.VideoAsset")]
    public partial class VideoAsset
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public VideoAsset()
        {
            VideoAssetVersions = new HashSet<VideoAssetVersion>();
        }

        public Guid VideoAssetId { get; set; }

        public Guid AccountId { get; set; }

        public Guid? AdvertiserId { get; set; }

        public bool Deleted { get; set; }

        [Required]
        [StringLength(1024)]
        public string VideoAssetFileName { get; set; }

        [Required]
        [StringLength(1024)]
        public string VideoAssetName { get; set; }

        [StringLength(32)]
        public string VideoAssetRefCode { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastModifiedDatetime { get; set; }

        public virtual Account Account { get; set; }

        public virtual Advertiser Advertiser { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VideoAssetVersion> VideoAssetVersions { get; set; }
    }
}
