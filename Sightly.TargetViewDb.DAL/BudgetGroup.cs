namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Hist.BudgetGroup")]
    public partial class BudgetGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BudgetGroup()
        {
            BudgetGroupLedgers = new HashSet<BudgetGroupLedger>();
            BudgetGroupTimedBudgets = new HashSet<BudgetGroupTimedBudget>();
            BudgetGroupXrefAds = new HashSet<BudgetGroupXrefAd>();
            BudgetGroupXrefConversionActions = new HashSet<BudgetGroupXrefConversionAction>();
            BudgetGroupXrefLocations = new HashSet<BudgetGroupXrefLocation>();
            BudgetGroupXrefPlacements = new HashSet<BudgetGroupXrefPlacement>();
        }

        public Guid BudgetGroupId { get; set; }

        public byte BudgetGroupStatusId { get; set; }

        [StringLength(256)]
        public string BudgetGroupName { get; set; }

        public Guid? CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Created { get; set; }

        public Guid? ModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? Modified { get; set; }

        public virtual BudgetGroupStatus BudgetGroupStatus { get; set; }

        public virtual User User { get; set; }

        public virtual User User1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupLedger> BudgetGroupLedgers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTimedBudget> BudgetGroupTimedBudgets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefAd> BudgetGroupXrefAds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefConversionAction> BudgetGroupXrefConversionActions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefLocation> BudgetGroupXrefLocations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefPlacement> BudgetGroupXrefPlacements { get; set; }
    }
}
