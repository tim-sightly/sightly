namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.ChangeOrderLog")]
    public partial class ChangeOrderLog
    {
        public Guid ChangeOrderLogId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime ChangeDate { get; set; }

        [Required]
        public string ChangeDetails { get; set; }

        public Guid ChangeTypeId { get; set; }

        public Guid OrderRecordLogId { get; set; }

        public Guid? AdId { get; set; }

        [Required]
        [StringLength(256)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(256)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Ad Ad { get; set; }

        public virtual ChangeType ChangeType { get; set; }

        public virtual OrderRecordLog OrderRecordLog { get; set; }
    }
}
