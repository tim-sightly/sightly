namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.DeviceType")]
    public partial class DeviceType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DeviceType()
        {
            BudgetGroupDeviceStats = new HashSet<BudgetGroupDeviceStat>();
            DeviceStats = new HashSet<DeviceStat>();
            RawAdDeviceConversionStats = new HashSet<RawAdDeviceConversionStat>();
            RawAdStats = new HashSet<RawAdStat>();
            RawBudgetGroupAdDeviceConversionStats = new HashSet<RawBudgetGroupAdDeviceConversionStat>();
            RawBudgetGroupAdStats = new HashSet<RawBudgetGroupAdStat>();
            RawBudgetGroupDeviceConversionStats = new HashSet<RawBudgetGroupDeviceConversionStat>();
            RawBudgetGroupDeviceStats = new HashSet<RawBudgetGroupDeviceStat>();
            RawDeviceConversionStats = new HashSet<RawDeviceConversionStat>();
            RawDeviceStats = new HashSet<RawDeviceStat>();
            BudgetGroupDeviceStats1 = new HashSet<BudgetGroupDeviceStats1>();
            DeviceStats1 = new HashSet<DeviceStats1>();
        }

        public Guid DeviceTypeId { get; set; }

        [Required]
        [StringLength(128)]
        public string DeviceTypeName { get; set; }

        [StringLength(128)]
        public string AdWordsDeviceTypeName { get; set; }

        [StringLength(8)]
        public string Color { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupDeviceStat> BudgetGroupDeviceStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeviceStat> DeviceStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAdDeviceConversionStat> RawAdDeviceConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAdStat> RawAdStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAdDeviceConversionStat> RawBudgetGroupAdDeviceConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAdStat> RawBudgetGroupAdStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupDeviceConversionStat> RawBudgetGroupDeviceConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupDeviceStat> RawBudgetGroupDeviceStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawDeviceConversionStat> RawDeviceConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawDeviceStat> RawDeviceStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupDeviceStats1> BudgetGroupDeviceStats1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DeviceStats1> DeviceStats1 { get; set; }
    }
}
