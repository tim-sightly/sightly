namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tvdb.PropertyType")]
    public partial class PropertyType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PropertyType()
        {
            BudgetGroupTimedBudgetProperties = new HashSet<BudgetGroupTimedBudgetProperty>();
        }

        public int PropertyTypeId { get; set; }

        [Required]
        [StringLength(128)]
        public string PropertyName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTimedBudgetProperty> BudgetGroupTimedBudgetProperties { get; set; }
    }
}
