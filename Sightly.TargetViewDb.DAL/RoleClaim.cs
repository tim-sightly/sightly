namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Users.RoleClaim")]
    public partial class RoleClaim
    {
        public int RoleClaimId { get; set; }

        public Guid RoleId { get; set; }

        [Required]
        [StringLength(256)]
        public string ClaimType { get; set; }

        [Required]
        [StringLength(50)]
        public string ClaimValue { get; set; }

        [Required]
        [StringLength(100)]
        public string ClaimValueType { get; set; }

        [StringLength(50)]
        public string ClaimTarget { get; set; }

        [StringLength(100)]
        public string ClaimTargetType { get; set; }

        public virtual Role Role { get; set; }
    }
}
