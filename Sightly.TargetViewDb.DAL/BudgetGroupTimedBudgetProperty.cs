namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Live.BudgetGroupTimedBudgetProperty")]
    public partial class BudgetGroupTimedBudgetProperty
    {
        public int Id { get; set; }

        public Guid BudgetGroupTimedBudgetId { get; set; }

        public int PropertyTypeId { get; set; }

        [Required]
        [StringLength(128)]
        public string PropertyValue { get; set; }

        public Guid CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Created { get; set; }

        public virtual BudgetGroupTimedBudget1 BudgetGroupTimedBudget1 { get; set; }

        public virtual PropertyType PropertyType { get; set; }
    }
}
