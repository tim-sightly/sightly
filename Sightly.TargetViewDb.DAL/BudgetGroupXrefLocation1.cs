namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Live.BudgetGroupXrefLocation")]
    public partial class BudgetGroupXrefLocation1
    {
        [Key]
        public Guid BudgetGroupXrefLocationId { get; set; }

        public Guid BudgetGroupId { get; set; }

        public Guid LocationId { get; set; }

        public Guid? CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Created { get; set; }

        public virtual Location Location { get; set; }

        public virtual BudgetGroup1 BudgetGroup1 { get; set; }

        public virtual User User { get; set; }
    }
}
