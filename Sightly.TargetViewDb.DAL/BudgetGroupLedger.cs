namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.BudgetGroupLedger")]
    public partial class BudgetGroupLedger
    {
        public long BudgetGroupLedgerId { get; set; }

        public Guid? BudgetGroupId { get; set; }

        public Guid? BudgetGroupProposalId { get; set; }

        public Guid? BudgetGroupHistoryId { get; set; }

        public Guid CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Created { get; set; }

        public byte? BudgetGroupEventId { get; set; }

        [StringLength(512)]
        public string Notes { get; set; }

        public virtual BudgetGroup BudgetGroup { get; set; }

        public virtual BudgetGroup1 BudgetGroup1 { get; set; }

        public virtual BudgetGroup2 BudgetGroup2 { get; set; }

        public virtual BudgetGroupEvent BudgetGroupEvent { get; set; }
    }
}
