namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.AdvertiserSubCategory")]
    public partial class AdvertiserSubCategory
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AdvertiserSubCategory()
        {
            Advertisers = new HashSet<Advertiser>();
            Estimates = new HashSet<Estimate>();
            Orders = new HashSet<Order>();
            OrderRecordLogs = new HashSet<OrderRecordLog>();
        }

        public Guid AdvertiserSubCategoryId { get; set; }

        public Guid AdvertiserCategoryId { get; set; }

        [Required]
        [StringLength(128)]
        public string AdvertiserSubCategoryName { get; set; }

        [Column(TypeName = "money")]
        public decimal Cpcv { get; set; }

        [Required]
        [StringLength(256)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(256)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Advertiser> Advertisers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Estimate> Estimates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order> Orders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderRecordLog> OrderRecordLogs { get; set; }

        public virtual AdvertiserCategory AdvertiserCategory { get; set; }
    }
}
