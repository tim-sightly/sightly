namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.OrderBudgetGroupImpressionsViewsByDate")]
    public partial class OrderBudgetGroupImpressionsViewsByDate
    {
        [Column(TypeName = "date")]
        public DateTime? statdate { get; set; }

        [Key]
        [Column(Order = 0)]
        [StringLength(256)]
        public string accountName { get; set; }

        public long? adwordscampaignid { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid orderid { get; set; }

        public Guid? BudgetGroupId { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(32)]
        public string OrderStatus { get; set; }

        public long? OrderImpressions { get; set; }

        public long? OrderCompletedViews { get; set; }

        public long? BudgetGroupImpressions { get; set; }

        public long? BudgetGroupCompletedViews { get; set; }

        public long? AdsImpressions { get; set; }

        public long? AdsCompletedViews { get; set; }
    }
}
