namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Accounts.CampaignAdWordsInfo")]
    public partial class CampaignAdWordsInfo
    {
        public Guid CampaignAdWordsInfoId { get; set; }

        public Guid CampaignId { get; set; }

        public Guid? AdvertiserId { get; set; }

        public long? AdWordsCustomerId { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Advertiser Advertiser { get; set; }

        public virtual Campaign Campaign { get; set; }
    }
}
