namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.AccountType")]
    public partial class AccountType
    {
        public Guid AccountTypeId { get; set; }

        [Required]
        [StringLength(256)]
        public string AccountTypeName { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastModifiedDatetime { get; set; }
    }
}
