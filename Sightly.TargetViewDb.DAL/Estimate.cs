namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Estimates.Estimate")]
    public partial class Estimate
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Estimate()
        {
            EstimateResultComparables = new HashSet<EstimateResultComparable>();
            EstimateTargetAgeGroups = new HashSet<EstimateTargetAgeGroup>();
            EstimateTargetGenders = new HashSet<EstimateTargetGender>();
            EstimateTargetParentalStatus = new HashSet<EstimateTargetParentalStatu>();
        }

        public Guid EstimateId { get; set; }

        [Required]
        [StringLength(128)]
        public string EstimateName { get; set; }

        public Guid EstimateResultComparableViewId { get; set; }

        [Column(TypeName = "money")]
        public decimal Budget { get; set; }

        public Guid CreatorId { get; set; }

        public Guid AccountId { get; set; }

        public Guid AdvertiserId { get; set; }

        public Guid? AdvertiserSubCategoryId { get; set; }

        public int? AdDurationInSeconds { get; set; }

        public Guid LocationId { get; set; }

        public Guid TargetingTypeId { get; set; }

        [Column(TypeName = "money")]
        public decimal EstimatedCpcv { get; set; }

        public decimal? AssumedViewRate { get; set; }

        public decimal? CustomerAccountMargin { get; set; }

        public decimal? SightlyMargin { get; set; }

        public Guid? OrderId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? ConvertedDatetime { get; set; }

        public bool Archived { get; set; }

        public bool Deleted { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Account Account { get; set; }

        public virtual Advertiser Advertiser { get; set; }

        public virtual Location Location { get; set; }

        public virtual User User { get; set; }

        public virtual EstimateResultComparableView EstimateResultComparableView { get; set; }

        public virtual Order Order { get; set; }

        public virtual AdvertiserSubCategory AdvertiserSubCategory { get; set; }

        public virtual TargetingType TargetingType { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EstimateResultComparable> EstimateResultComparables { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EstimateTargetAgeGroup> EstimateTargetAgeGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EstimateTargetGender> EstimateTargetGenders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EstimateTargetParentalStatu> EstimateTargetParentalStatus { get; set; }
    }
}
