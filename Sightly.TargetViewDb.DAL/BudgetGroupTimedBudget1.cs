namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Live.BudgetGroupTimedBudget")]
    public partial class BudgetGroupTimedBudget1
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BudgetGroupTimedBudget1()
        {
            BudgetGroupTimedBudgetProperties = new HashSet<BudgetGroupTimedBudgetProperty>();
        }

        [Key]
        public Guid BudgetGroupTimedBudgetId { get; set; }

        public Guid BudgetGroupId { get; set; }

        [Column(TypeName = "money")]
        public decimal BudgetAmount { get; set; }

        public decimal? Margin { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime EndDate { get; set; }

        public Guid? CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Created { get; set; }

        public Guid? BudgetGroupXrefPlacementId { get; set; }

        public virtual BudgetGroup1 BudgetGroup1 { get; set; }

        public virtual BudgetGroupXrefPlacement1 BudgetGroupXrefPlacement1 { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTimedBudgetProperty> BudgetGroupTimedBudgetProperties { get; set; }
    }
}
