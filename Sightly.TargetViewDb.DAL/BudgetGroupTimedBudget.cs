namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Hist.BudgetGroupTimedBudget")]
    public partial class BudgetGroupTimedBudget
    {
        [Key]
        [Column(Order = 0)]
        public Guid BudgetGroupTimedBudgetId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid BudgetGroupId { get; set; }

        [Column(TypeName = "money")]
        public decimal BudgetAmount { get; set; }

        public decimal? Margin { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime EndDate { get; set; }

        public Guid? CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Created { get; set; }

        public Guid? BudgetGroupXrefPlacementId { get; set; }

        public virtual BudgetGroup BudgetGroup { get; set; }

        public virtual BudgetGroupXrefPlacement BudgetGroupXrefPlacement { get; set; }

        public virtual User User { get; set; }
    }
}
