namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("tvdb.AgencyMcc")]
    public partial class AgencyMcc
    {
        public int Id { get; set; }

        [Required]
        [StringLength(128)]
        public string AgencyMccName { get; set; }

        public long CustomerId { get; set; }
    }
}
