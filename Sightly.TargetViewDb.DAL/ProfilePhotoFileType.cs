namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.ProfilePhotoFileType")]
    public partial class ProfilePhotoFileType
    {
        public Guid ProfilePhotoFileTypeId { get; set; }

        [Required]
        [StringLength(32)]
        public string ProfilePhotoFileTypeName { get; set; }

        [Required]
        [StringLength(8)]
        public string FileExtension { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }
    }
}
