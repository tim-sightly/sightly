namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.BudgetGroupTimedBudget")]
    public partial class BudgetGroupTimedBudget2
    {
        [Key]
        public Guid BudgetGroupTimedBudgetId { get; set; }

        public Guid BudgetGroupId { get; set; }

        [Column(TypeName = "money")]
        public decimal BudgetAmount { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StartDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime EndDate { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        public decimal? Margin { get; set; }

        public Guid? BudgetGroupXrefPlacementId { get; set; }

        public virtual BudgetGroup2 BudgetGroup2 { get; set; }

        public virtual BudgetGroupXrefPlacement2 BudgetGroupXrefPlacement2 { get; set; }
    }
}
