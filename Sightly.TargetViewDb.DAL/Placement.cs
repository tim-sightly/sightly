namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.Placement")]
    public partial class Placement
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Placement()
        {
            BudgetGroupXrefPlacements = new HashSet<BudgetGroupXrefPlacement>();
            BudgetGroupXrefPlacement1 = new HashSet<BudgetGroupXrefPlacement1>();
            BudgetGroupXrefPlacement2 = new HashSet<BudgetGroupXrefPlacement2>();
        }

        public Guid PlacementId { get; set; }

        [StringLength(128)]
        public string PlacementValue { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        public Guid AdvertiserId { get; set; }

        [StringLength(512)]
        public string PlacementName { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefPlacement> BudgetGroupXrefPlacements { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefPlacement1> BudgetGroupXrefPlacement1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefPlacement2> BudgetGroupXrefPlacement2 { get; set; }
    }
}
