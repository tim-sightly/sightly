namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Accounts.Advertiser")]
    public partial class Advertiser
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Advertiser()
        {
            UserXrefAdvertisers = new HashSet<UserXrefAdvertiser>();
            BannerAssets = new HashSet<BannerAsset>();
            VideoAssets = new HashSet<VideoAsset>();
            AdvertiserXrefConversionActions = new HashSet<AdvertiserXrefConversionAction>();
            Campaigns = new HashSet<Campaign>();
            CampaignAdWordsInfoes = new HashSet<CampaignAdWordsInfo>();
            ConversionActions = new HashSet<ConversionAction>();
            Estimates = new HashSet<Estimate>();
            Orders = new HashSet<Order>();
            OrderRecordLogs = new HashSet<OrderRecordLog>();
        }

        public Guid AdvertiserId { get; set; }

        public Guid AccountId { get; set; }

        [Required]
        [StringLength(256)]
        public string AdvertiserName { get; set; }

        public Guid AdvertiserCategoryId { get; set; }

        public Guid? AdvertiserSubCategoryId { get; set; }

        [StringLength(128)]
        public string AdvertiserRefCode { get; set; }

        public decimal? AdvertiserMargin { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? AdvertiserMarginLastModifiedDatetime { get; set; }

        public decimal? SightlyMargin { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? SightlyMarginLastModifiedDatetime { get; set; }

        public string YouTubeChannelId { get; set; }

        public bool Deleted { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Account Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserXrefAdvertiser> UserXrefAdvertisers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BannerAsset> BannerAssets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VideoAsset> VideoAssets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdvertiserXrefConversionAction> AdvertiserXrefConversionActions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Campaign> Campaigns { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CampaignAdWordsInfo> CampaignAdWordsInfoes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConversionAction> ConversionActions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Estimate> Estimates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order> Orders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderRecordLog> OrderRecordLogs { get; set; }

        public virtual AdvertiserCategory AdvertiserCategory { get; set; }

        public virtual AdvertiserSubCategory AdvertiserSubCategory { get; set; }
    }
}
