namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.AwTargetViewRelationDataView")]
    public partial class AwTargetViewRelationDataView
    {
        public Guid? AdvertiserId { get; set; }

        public long? AdWordsCustomerId { get; set; }

        [Key]
        [Column(Order = 0)]
        public Guid OrderId { get; set; }

        [Column(TypeName = "date")]
        public DateTime? StartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? EndDate { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid BudgetGroupId { get; set; }

        public long? AdWordsCampaignId { get; set; }

        [Key]
        [Column(Order = 2)]
        public Guid AdId { get; set; }

        public long? AdWordsAdId { get; set; }
    }
}
