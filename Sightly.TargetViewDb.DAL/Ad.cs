namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.Ad")]
    public partial class Ad
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Ad()
        {
            BudgetGroupXrefAds = new HashSet<BudgetGroupXrefAd>();
            BudgetGroupXrefAd1 = new HashSet<BudgetGroupXrefAd1>();
            AdAdWordsInfoes = new HashSet<AdAdWordsInfo>();
            AdStats = new HashSet<AdStat>();
            AdStats1 = new HashSet<AdStats1>();
            BudgetGroup2 = new HashSet<BudgetGroup2>();
            BudgetGroupAdStats = new HashSet<BudgetGroupAdStat>();
            BudgetGroupAdStats1 = new HashSet<BudgetGroupAdStats1>();
            BudgetGroupXrefAd2 = new HashSet<BudgetGroupXrefAd2>();
            ChangeOrderLogs = new HashSet<ChangeOrderLog>();
            RawAdConversionStats = new HashSet<RawAdConversionStat>();
            RawAdStats = new HashSet<RawAdStat>();
            RawBudgetGroupAdConversionStats = new HashSet<RawBudgetGroupAdConversionStat>();
            RawBudgetGroupAdDeviceConversionStats = new HashSet<RawBudgetGroupAdDeviceConversionStat>();
            RawBudgetGroupAdStats = new HashSet<RawBudgetGroupAdStat>();
        }

        public Guid AdId { get; set; }

        public Guid? BannerAssetId { get; set; }

        public Guid CreatorId { get; set; }

        public bool? Deleted { get; set; }

        [StringLength(1024)]
        public string DestinationUrl { get; set; }

        [StringLength(1024)]
        public string DisplayUrl { get; set; }

        public Guid? ParentAdId { get; set; }

        [Required]
        [StringLength(256)]
        public string VideoAdName { get; set; }

        public Guid? VideoAssetVersionId { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual BannerAsset BannerAsset { get; set; }

        public virtual VideoAssetVersion VideoAssetVersion { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefAd> BudgetGroupXrefAds { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefAd1> BudgetGroupXrefAd1 { get; set; }

        public virtual User User { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdAdWordsInfo> AdAdWordsInfoes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdStat> AdStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AdStats1> AdStats1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroup2> BudgetGroup2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupAdStat> BudgetGroupAdStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupAdStats1> BudgetGroupAdStats1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefAd2> BudgetGroupXrefAd2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChangeOrderLog> ChangeOrderLogs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAdConversionStat> RawAdConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAdStat> RawAdStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAdConversionStat> RawBudgetGroupAdConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAdDeviceConversionStat> RawBudgetGroupAdDeviceConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAdStat> RawBudgetGroupAdStats { get; set; }
    }
}
