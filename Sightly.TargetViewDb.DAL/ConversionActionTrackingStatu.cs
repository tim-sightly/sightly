namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.ConversionActionTrackingStatus")]
    public partial class ConversionActionTrackingStatu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ConversionActionTrackingStatu()
        {
            ConversionActions = new HashSet<ConversionAction>();
        }

        [Key]
        public Guid ConversionActionTrackingStatusId { get; set; }

        [Required]
        [StringLength(64)]
        public string ConversionActionTrackingStatusName { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        public int SortOrder { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConversionAction> ConversionActions { get; set; }
    }
}
