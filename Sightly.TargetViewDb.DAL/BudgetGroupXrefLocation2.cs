namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.BudgetGroupXrefLocation")]
    public partial class BudgetGroupXrefLocation2
    {
        [Key]
        public Guid BudgetGroupXrefLocationId { get; set; }

        public Guid BudgetGroupId { get; set; }

        public bool Deleted { get; set; }

        public Guid LocationId { get; set; }

        public int SequenceId { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Location Location { get; set; }

        public virtual BudgetGroup2 BudgetGroup2 { get; set; }
    }
}
