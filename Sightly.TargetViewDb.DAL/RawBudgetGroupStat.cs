namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.RawBudgetGroupStats")]
    public partial class RawBudgetGroupStat
    {
        [Key]
        public Guid RawBudgetGroupStatsId { get; set; }

        public Guid BudgetGroupId { get; set; }

        [Column(TypeName = "date")]
        public DateTime StatDate { get; set; }

        public long Clicks { get; set; }

        public double Conversions { get; set; }

        [Column(TypeName = "money")]
        public decimal Cost { get; set; }

        public long Impressions { get; set; }

        public double VideoPlayedTo25 { get; set; }

        public double VideoPlayedTo50 { get; set; }

        public double VideoPlayedTo75 { get; set; }

        public double VideoPlayedTo100 { get; set; }

        public long Views { get; set; }

        public long ViewThroughConversions { get; set; }

        [Column(TypeName = "money")]
        public decimal AvgCost { get; set; }

        [Column(TypeName = "money")]
        public decimal AvgCPC { get; set; }

        public double AvgCPE { get; set; }

        [Column(TypeName = "money")]
        public decimal AvgCPM { get; set; }

        public double AvgCPV { get; set; }

        public double AvgPosition { get; set; }

        [Column(TypeName = "money")]
        public decimal Budget { get; set; }

        public long Engagements { get; set; }

        public long Interactions { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        public virtual BudgetGroup2 BudgetGroup2 { get; set; }
    }
}
