namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Estimates.EstimateTargetAgeGroup")]
    public partial class EstimateTargetAgeGroup
    {
        [Key]
        [Column(Order = 0)]
        public Guid EstimateId { get; set; }

        [Key]
        [Column(Order = 1)]
        public Guid AgeGroupId { get; set; }

        public bool Deleted { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Estimate Estimate { get; set; }

        public virtual AgeGroup AgeGroup { get; set; }
    }
}
