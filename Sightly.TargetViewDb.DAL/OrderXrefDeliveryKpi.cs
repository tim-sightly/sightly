namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.OrderXrefDeliveryKpi")]
    public partial class OrderXrefDeliveryKpi
    {
        public Guid OrderXrefDeliveryKpiId { get; set; }

        public Guid OrderId { get; set; }

        public Guid DeliveryKpiId { get; set; }

        public bool IsPrimeDeliveryKpi { get; set; }

        public int SequenceNo { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Order Order { get; set; }

        public virtual DeliveryKpi DeliveryKpi { get; set; }
    }
}
