namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.ParentalStatus")]
    public partial class ParentalStatu
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ParentalStatu()
        {
            EstimateTargetParentalStatus = new HashSet<EstimateTargetParentalStatu>();
            OrderTargetParentalStatus = new HashSet<OrderTargetParentalStatu>();
        }

        [Key]
        public Guid ParentalStatusId { get; set; }

        [Required]
        [StringLength(16)]
        public string ParentalStatusName { get; set; }

        [StringLength(512)]
        public string Description { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EstimateTargetParentalStatu> EstimateTargetParentalStatus { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetParentalStatu> OrderTargetParentalStatus { get; set; }
    }
}
