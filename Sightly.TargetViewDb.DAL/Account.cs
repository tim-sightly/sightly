namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Accounts.Account")]
    public partial class Account
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Account()
        {
            AccountBillingAddresses = new HashSet<AccountBillingAddress>();
            AccountBillingContacts = new HashSet<AccountBillingContact>();
            AccountCreditCards = new HashSet<AccountCreditCard>();
            Account1 = new HashSet<Account>();
            Advertisers = new HashSet<Advertiser>();
            BannerAssets = new HashSet<BannerAsset>();
            Campaigns = new HashSet<Campaign>();
            Locations = new HashSet<Location>();
            VideoAssets = new HashSet<VideoAsset>();
            Users = new HashSet<User>();
            UserXrefAccounts = new HashSet<UserXrefAccount>();
            ConversionActions = new HashSet<ConversionAction>();
            Estimates = new HashSet<Estimate>();
            Orders = new HashSet<Order>();
            OrderRecordLogs = new HashSet<OrderRecordLog>();
        }

        public Guid AccountId { get; set; }

        [Required]
        [StringLength(256)]
        public string AccountName { get; set; }

        [Required]
        [StringLength(3)]
        public string ShortCode { get; set; }

        public Guid? AccountTypeId { get; set; }

        public Guid? SubAccountTypeId { get; set; }

        public Guid? BillingTypeId { get; set; }

        public decimal? AccountMargin { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? AccountMarginLastModifiedDatetime { get; set; }

        public decimal? SightlyMargin { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? SightlyMarginLastModifiedDatetime { get; set; }

        public bool? SightlyUniversity { get; set; }

        public bool ShowAdvancedTacticsTab { get; set; }

        public bool? ApproverWorkflow { get; set; }

        public long? AdwordsMccAccountId { get; set; }

        public long? FacebookCredentials { get; set; }

        [StringLength(256)]
        public string ManagerName { get; set; }

        public Guid? AdminId { get; set; }

        public Guid? YouTubeChannelLevelId { get; set; }

        public string YouTubeChannelId { get; set; }

        public Guid? ParentAccountId { get; set; }

        public bool ActivateSubAccounts { get; set; }

        public bool DisplayWhiteLabelVersion { get; set; }

        public bool DisplayAdvertiserReferenceNo { get; set; }

        public bool DisplayVideoAdReferenceNo { get; set; }

        public bool DisplayLocationReferenceNo { get; set; }

        public Guid? ProfilePhotoFileTypeId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? ProfilePhotoUploadDatetime { get; set; }

        public Guid? AccountLogoFileTypeId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? AccountLogoUploadDatetime { get; set; }

        public bool? Deleted { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastModifiedDatetime { get; set; }

        [StringLength(128)]
        public string AccountRefCode { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountBillingAddress> AccountBillingAddresses { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountBillingContact> AccountBillingContacts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<AccountCreditCard> AccountCreditCards { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Account> Account1 { get; set; }

        public virtual Account Account2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Advertiser> Advertisers { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BannerAsset> BannerAssets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Campaign> Campaigns { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Location> Locations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<VideoAsset> VideoAssets { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<User> Users { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserXrefAccount> UserXrefAccounts { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ConversionAction> ConversionActions { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Estimate> Estimates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Order> Orders { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderRecordLog> OrderRecordLogs { get; set; }

        public virtual User User { get; set; }
    }
}
