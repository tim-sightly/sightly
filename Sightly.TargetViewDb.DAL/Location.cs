namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Accounts.Location")]
    public partial class Location
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Location()
        {
            LocationDetails = new HashSet<LocationDetail>();
            BudgetGroupXrefLocations = new HashSet<BudgetGroupXrefLocation>();
            BudgetGroupXrefLocation1 = new HashSet<BudgetGroupXrefLocation1>();
            BudgetGroupXrefLocation2 = new HashSet<BudgetGroupXrefLocation2>();
            Estimates = new HashSet<Estimate>();
            OrderRecordLogs = new HashSet<OrderRecordLog>();
        }

        public Guid LocationId { get; set; }

        public Guid AccountId { get; set; }

        [Required]
        [StringLength(128)]
        public string LocationName { get; set; }

        [StringLength(32)]
        public string LocationRefCode { get; set; }

        public bool Deleted { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastModifiedDatetime { get; set; }

        public virtual Account Account { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<LocationDetail> LocationDetails { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefLocation> BudgetGroupXrefLocations { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefLocation1> BudgetGroupXrefLocation1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupXrefLocation2> BudgetGroupXrefLocation2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Estimate> Estimates { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderRecordLog> OrderRecordLogs { get; set; }
    }
}
