namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("raw.AdDeviceStats")]
    public partial class AdDeviceStat
    {
        [Key]
        public long AdDeviceStatsId { get; set; }

        public long AdGroupId { get; set; }

        public long AdId { get; set; }

        [Required]
        [StringLength(256)]
        public string AdName { get; set; }

        public long CampaignId { get; set; }

        [Required]
        [StringLength(512)]
        public string CampaignName { get; set; }

        public long Clicks { get; set; }

        [Column(TypeName = "money")]
        public decimal Cost { get; set; }

        [Required]
        [StringLength(50)]
        public string Device { get; set; }

        public long Impressions { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime PulledDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StatDate { get; set; }

        public long TotalViews { get; set; }

        public decimal AudienceRetention25 { get; set; }

        public decimal AudienceRetention50 { get; set; }

        public decimal AudienceRetention75 { get; set; }

        public decimal AudienceRetention100 { get; set; }

        public decimal Aud25xImpressions { get; set; }

        public decimal Aud50xImpressions { get; set; }

        public decimal Aud75xImpressions { get; set; }

        public decimal Aud100xImpressions { get; set; }

        public long? CustomerId { get; set; }
    }
}
