namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.ExecutivePerformanceSortableField")]
    public partial class ExecutivePerformanceSortableField
    {
        public Guid ExecutivePerformanceSortableFieldId { get; set; }

        [Required]
        [StringLength(32)]
        public string Name { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDate { get; set; }

        [Required]
        [StringLength(256)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastEditDate { get; set; }

        [Required]
        [StringLength(256)]
        public string LastEditBy { get; set; }
    }
}
