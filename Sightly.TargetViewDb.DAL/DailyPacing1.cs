namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.DailyPacing")]
    public partial class DailyPacing1
    {
        [Key]
        public Guid DailyPacingId { get; set; }

        [Column(TypeName = "date")]
        public DateTime StatDate { get; set; }

        [StringLength(12)]
        public string CustomerId { get; set; }

        [Required]
        [StringLength(128)]
        public string TopAccountName { get; set; }

        [StringLength(128)]
        public string AccountHeirarchy { get; set; }

        [Required]
        [StringLength(256)]
        public string AccountName { get; set; }

        [Required]
        [StringLength(256)]
        public string AdvertiserName { get; set; }

        public Guid AdvertiserId { get; set; }

        [Required]
        [StringLength(512)]
        public string CampaignName { get; set; }

        [StringLength(128)]
        public string CampaignRefCode { get; set; }

        [StringLength(10)]
        public string BudgetGroupDeleted { get; set; }

        [Required]
        [StringLength(128)]
        public string LocationName { get; set; }

        [StringLength(10)]
        public string LocationDeleted { get; set; }

        [Required]
        [StringLength(128)]
        public string OrderName { get; set; }

        public Guid OrderId { get; set; }

        [Required]
        [StringLength(128)]
        public string OrderRefCode { get; set; }

        [StringLength(32)]
        public string OrderStatus { get; set; }

        [Column(TypeName = "date")]
        public DateTime IOStartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime IOEndDate { get; set; }

        public decimal SightlyMargin { get; set; }

        public decimal CustomerMargin { get; set; }

        [Column(TypeName = "money")]
        public decimal GrossBudget { get; set; }

        [Column(TypeName = "money")]
        public decimal SightlyBudget { get; set; }

        [Column(TypeName = "money")]
        public decimal NetBudget { get; set; }

        [Column(TypeName = "money")]
        public decimal GrossSpend { get; set; }

        [Column(TypeName = "money")]
        public decimal SightlySpend { get; set; }

        [Column(TypeName = "money")]
        public decimal NetSpend { get; set; }

        [Column(TypeName = "money")]
        public decimal MTDGrossSpend { get; set; }

        [Column(TypeName = "money")]
        public decimal MTDSightlySpend { get; set; }

        [Column(TypeName = "money")]
        public decimal MTDNetSpend { get; set; }

        [Column(TypeName = "money")]
        public decimal YesterdayNetSpend { get; set; }

        [Column(TypeName = "money")]
        public decimal RemainingNetDailyBudget { get; set; }

        public decimal PacingPercent { get; set; }

        public decimal TrendingPacingPercent { get; set; }

        public int DaysInOrder { get; set; }

        public int DaysSoFar { get; set; }

        public int ReportedDates { get; set; }

        public int RemainingDays { get; set; }

        [Column(TypeName = "money")]
        public decimal RemainingSightlyBudget { get; set; }

        public long CompletedViews { get; set; }

        public long Impressions { get; set; }

        public decimal ViewRate { get; set; }

        public decimal NetCPV { get; set; }

        public decimal SightlySpendToPreviousMonthsEnd { get; set; }
    }
}
