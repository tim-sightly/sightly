namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.OrderBudgetPacing")]
    public partial class OrderBudgetPacing
    {
        public Guid OrderBudgetPacingId { get; set; }

        public Guid? OrderId { get; set; }

        public decimal? PacingRate { get; set; }

        [Column(TypeName = "money")]
        public decimal? SpendToDate { get; set; }

        [Column(TypeName = "money")]
        public decimal? ProjectedFinalSpend { get; set; }

        [Column(TypeName = "money")]
        public decimal? RemainingBudget { get; set; }

        public int? DaysLeft { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Order Order { get; set; }
    }
}
