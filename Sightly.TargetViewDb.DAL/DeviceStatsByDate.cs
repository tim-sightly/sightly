namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("raw.DeviceStatsByDate")]
    public partial class DeviceStatsByDate
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long CampaignId { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(512)]
        public string CampaignName { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string Device { get; set; }

        [Key]
        [Column(Order = 3, TypeName = "datetime2")]
        public DateTime StatDate { get; set; }

        public long? Impressions { get; set; }

        public long? Clicks { get; set; }

        [Column(TypeName = "money")]
        public decimal? Cost { get; set; }

        public long? TotalViews { get; set; }
    }
}
