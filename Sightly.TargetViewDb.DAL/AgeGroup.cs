namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.AgeGroup")]
    public partial class AgeGroup
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public AgeGroup()
        {
            EstimateTargetAgeGroups = new HashSet<EstimateTargetAgeGroup>();
            BudgetGroupTargetAgeGroupStats = new HashSet<BudgetGroupTargetAgeGroupStat>();
            OrderTargetAgeGroups = new HashSet<OrderTargetAgeGroup>();
            OrderTargetAgeGroupStats = new HashSet<OrderTargetAgeGroupStat>();
            RawAgeGroupConversionStats = new HashSet<RawAgeGroupConversionStat>();
            RawAgeGroupStats = new HashSet<RawAgeGroupStat>();
            RawBudgetGroupAgeGroupConversionStats = new HashSet<RawBudgetGroupAgeGroupConversionStat>();
            RawBudgetGroupAgeGroupStats = new HashSet<RawBudgetGroupAgeGroupStat>();
            BudgetGroupTargetAgeGroupStats1 = new HashSet<BudgetGroupTargetAgeGroupStats1>();
            OrderTargetAgeGroupStats1 = new HashSet<OrderTargetAgeGroupStats1>();
        }

        public Guid AgeGroupId { get; set; }

        [Required]
        [StringLength(16)]
        public string AgeGroupName { get; set; }

        [Required]
        [StringLength(16)]
        public string AdWordsAgeGroupName { get; set; }

        public int? StartAge { get; set; }

        public int? EndAge { get; set; }

        public bool IsStartAgeUpwards { get; set; }

        public decimal PopulationPercentage { get; set; }

        public int TargetViewAgeGroupId { get; set; }

        [StringLength(8)]
        public string Color { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<EstimateTargetAgeGroup> EstimateTargetAgeGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTargetAgeGroupStat> BudgetGroupTargetAgeGroupStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetAgeGroup> OrderTargetAgeGroups { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetAgeGroupStat> OrderTargetAgeGroupStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAgeGroupConversionStat> RawAgeGroupConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawAgeGroupStat> RawAgeGroupStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAgeGroupConversionStat> RawBudgetGroupAgeGroupConversionStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<RawBudgetGroupAgeGroupStat> RawBudgetGroupAgeGroupStats { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTargetAgeGroupStats1> BudgetGroupTargetAgeGroupStats1 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<OrderTargetAgeGroupStats1> OrderTargetAgeGroupStats1 { get; set; }
    }
}
