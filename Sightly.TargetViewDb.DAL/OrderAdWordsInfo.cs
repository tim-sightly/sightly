namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.OrderAdWordsInfo")]
    public partial class OrderAdWordsInfo
    {
        public Guid OrderAdWordsInfoId { get; set; }

        public Guid OrderId { get; set; }

        [Required]
        public string AdWordsCampaignName { get; set; }

        public long? AdWordsCampaignId { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public long? AdWordsCustomerId { get; set; }
    }
}
