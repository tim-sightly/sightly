namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.OrderRecordLog")]
    public partial class OrderRecordLog
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public OrderRecordLog()
        {
            ChangeOrderLogs = new HashSet<ChangeOrderLog>();
        }

        public Guid OrderRecordLogId { get; set; }

        public Guid OrderId { get; set; }

        [Required]
        [StringLength(128)]
        public string OrderName { get; set; }

        [Required]
        [StringLength(128)]
        public string OrderRefCode { get; set; }

        public Guid AccountId { get; set; }

        public Guid CampaignId { get; set; }

        public Guid AdvertiserId { get; set; }

        public Guid? AdvertiserSubCategoryId { get; set; }

        public Guid? LocationId { get; set; }

        public Guid TargetingTypeId { get; set; }

        public Guid BudgetTypeId { get; set; }

        [Column(TypeName = "money")]
        public decimal? CampaignBudget { get; set; }

        [Column(TypeName = "money")]
        public decimal? MonthlyBudget { get; set; }

        public int? MonthCount { get; set; }

        [Column(TypeName = "date")]
        public DateTime StartDate { get; set; }

        public bool StartDateHard { get; set; }

        [Column(TypeName = "date")]
        public DateTime EndDate { get; set; }

        public bool EndDateHard { get; set; }

        public decimal? CustomerAccountMargin { get; set; }

        public decimal? SightlyMargin { get; set; }

        public Guid OrderTypeId { get; set; }

        public Guid? OrderStatusId { get; set; }

        public int? AdDurationInSeconds { get; set; }

        [Column(TypeName = "money")]
        public decimal? EstimatedCpcv { get; set; }

        public decimal? AssumedViewRate { get; set; }

        public Guid? ParentOrderId { get; set; }

        public bool Deleted { get; set; }

        public bool SpendFlex { get; set; }

        public Guid CreatorId { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Account Account { get; set; }

        public virtual Advertiser Advertiser { get; set; }

        public virtual Campaign Campaign { get; set; }

        public virtual Location Location { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ChangeOrderLog> ChangeOrderLogs { get; set; }

        public virtual Order Order { get; set; }

        public virtual Order Order1 { get; set; }

        public virtual AdvertiserSubCategory AdvertiserSubCategory { get; set; }

        public virtual BudgetType BudgetType { get; set; }

        public virtual User User { get; set; }

        public virtual OrderStatu OrderStatu { get; set; }

        public virtual OrderType OrderType { get; set; }

        public virtual TargetingType TargetingType { get; set; }
    }
}
