namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("zLookups.GeographyType")]
    public partial class GeographyType
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GeographyType()
        {
            Geographies = new HashSet<Geography>();
        }

        public Guid GeographyTypeId { get; set; }

        [Required]
        [StringLength(128)]
        public string GeographyTypeName { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastModifiedDatetime { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Geography> Geographies { get; set; }
    }
}
