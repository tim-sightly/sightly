namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.BudgetGroupXrefAd")]
    public partial class BudgetGroupXrefAd2
    {
        [Key]
        public Guid BudgetGroupXrefAdId { get; set; }

        public Guid BudgetGroupId { get; set; }

        public Guid AdId { get; set; }

        public int SequenceNo { get; set; }

        public bool Paused { get; set; }

        public bool Deleted { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastModifiedDatetime { get; set; }

        public virtual Ad Ad { get; set; }

        public virtual BudgetGroup2 BudgetGroup2 { get; set; }
    }
}
