namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.BudgetGroupAdStats")]
    public partial class BudgetGroupAdStat
    {
        [Key]
        public Guid BudgetGroupAdStatsId { get; set; }

        [Column(TypeName = "money")]
        public decimal? ActualSpend { get; set; }

        public Guid AdId { get; set; }

        public int? AudienceRetention25 { get; set; }

        public int? AudienceRetention50 { get; set; }

        public int? AudienceRetention75 { get; set; }

        public int? AudienceRetention100 { get; set; }

        public double? AverageCpv { get; set; }

        public Guid BudgetGroupId { get; set; }

        public long? Clicks { get; set; }

        public double? ClickThroughRate { get; set; }

        public long? CompletedViews { get; set; }

        public double? Conversions { get; set; }

        public decimal? CustomerAccountMargin { get; set; }

        [Column(TypeName = "money")]
        public decimal? CustomerAccountSpend { get; set; }

        [Column(TypeName = "money")]
        public decimal? EstimatedCost { get; set; }

        public long? Impressions { get; set; }

        public Guid OrderId { get; set; }

        [Column(TypeName = "money")]
        public decimal? PartialViews { get; set; }

        public decimal? SightlyMargin { get; set; }

        [Column(TypeName = "money")]
        public decimal? SightlySpend { get; set; }

        [Column(TypeName = "date")]
        public DateTime StatDate { get; set; }

        [Column(TypeName = "money")]
        public decimal? TotalSpend { get; set; }

        public double? ViewRate { get; set; }

        public long? ViewThroughConversions { get; set; }

        public long? ViewTime { get; set; }

        [Column(TypeName = "money")]
        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal? AvgCPCV { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual Ad Ad { get; set; }

        public virtual BudgetGroup2 BudgetGroup2 { get; set; }

        public virtual Order Order { get; set; }
    }
}
