namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.RawBudgetGroupGenderStats")]
    public partial class RawBudgetGroupGenderStat
    {
        [Key]
        public Guid RawBudgetGroupGenderStatsId { get; set; }

        public long AdGroupId { get; set; }

        public long AdWordsCampaignId { get; set; }

        public Guid BudgetGroupId { get; set; }

        public long Clicks { get; set; }

        public double Conversions { get; set; }

        [Column(TypeName = "money")]
        public decimal Cost { get; set; }

        public Guid GenderId { get; set; }

        public long Impressions { get; set; }

        public Guid OrderId { get; set; }

        [Column(TypeName = "date")]
        public DateTime StatDate { get; set; }

        public double VideoPlayedTo25 { get; set; }

        public double VideoPlayedTo50 { get; set; }

        public double VideoPlayedTo75 { get; set; }

        public double VideoPlayedTo100 { get; set; }

        public long Views { get; set; }

        public long ViewThroughConversions { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        public virtual BudgetGroup2 BudgetGroup2 { get; set; }

        public virtual Order Order { get; set; }

        public virtual Gender Gender { get; set; }
    }
}
