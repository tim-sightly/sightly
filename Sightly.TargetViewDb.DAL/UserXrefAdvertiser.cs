namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Users.UserXrefAdvertiser")]
    public partial class UserXrefAdvertiser
    {
        public Guid UserXrefAdvertiserId { get; set; }

        public Guid UserId { get; set; }

        public Guid AdvertiserId { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime? LastModifiedDatetime { get; set; }

        public virtual Advertiser Advertiser { get; set; }

        public virtual User User { get; set; }
    }
}
