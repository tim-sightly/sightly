namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.BudgetGroupXrefPlacement")]
    public partial class BudgetGroupXrefPlacement2
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public BudgetGroupXrefPlacement2()
        {
            BudgetGroupTimedBudget2 = new HashSet<BudgetGroupTimedBudget2>();
        }

        [Key]
        public Guid BudgetGroupXrefPlacementId { get; set; }

        public Guid BudgetGroupId { get; set; }

        public Guid PlacementId { get; set; }

        [Required]
        [StringLength(512)]
        public string AssignedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime AssignedDate { get; set; }

        public virtual BudgetGroup2 BudgetGroup2 { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<BudgetGroupTimedBudget2> BudgetGroupTimedBudget2 { get; set; }

        public virtual Placement Placement { get; set; }
    }
}
