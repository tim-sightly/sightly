namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Live.BudgetGroupXrefAd")]
    public partial class BudgetGroupXrefAd1
    {
        [Key]
        public Guid BudgetGroupXrefAdId { get; set; }

        public Guid BudgetGroupId { get; set; }

        public Guid AdId { get; set; }

        public Guid? CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime Created { get; set; }

        public virtual BudgetGroup1 BudgetGroup1 { get; set; }

        public virtual Ad Ad { get; set; }

        public virtual User User { get; set; }
    }
}
