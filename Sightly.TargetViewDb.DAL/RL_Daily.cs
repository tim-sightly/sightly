namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.RL_Daily")]
    public partial class RL_Daily
    {
        [Key]
        [Column(Order = 0, TypeName = "date")]
        public DateTime Date { get; set; }

        [Column("Client Name")]
        [StringLength(128)]
        public string Client_Name { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(512)]
        public string campaignname { get; set; }

        public long? Impressions { get; set; }

        public long? Views { get; set; }

        [Column(TypeName = "money")]
        public decimal? Cost { get; set; }
    }
}
