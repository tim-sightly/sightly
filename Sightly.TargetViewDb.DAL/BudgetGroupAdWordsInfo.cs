namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.BudgetGroupAdWordsInfo")]
    public partial class BudgetGroupAdWordsInfo
    {
        public Guid BudgetGroupAdWordsInfoId { get; set; }

        public Guid BudgetGroupId { get; set; }

        public long? AdWordsCampaignId { get; set; }

        public string AdWordsCampaignName { get; set; }

        public long? AdWordsCustomerId { get; set; }

        [Required]
        [StringLength(256)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(256)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual BudgetGroup2 BudgetGroup2 { get; set; }
    }
}
