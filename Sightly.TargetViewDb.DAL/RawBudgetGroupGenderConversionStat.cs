namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.RawBudgetGroupGenderConversionStats")]
    public partial class RawBudgetGroupGenderConversionStat
    {
        [Key]
        public Guid RawBudgetGroupGenderConversionStatsId { get; set; }

        public long AdGroupId { get; set; }

        public long AdWordsCampaignId { get; set; }

        public Guid BudgetGroupId { get; set; }

        public Guid ConversionActionId { get; set; }

        [Required]
        [StringLength(512)]
        public string ConversionCategory { get; set; }

        [Required]
        [StringLength(512)]
        public string ConversionName { get; set; }

        public double Conversions { get; set; }

        public long ConversionTrackerId { get; set; }

        public double ConversionValue { get; set; }

        public double CrossDeviceConversions { get; set; }

        public Guid GenderId { get; set; }

        [Column(TypeName = "date")]
        public DateTime StatDate { get; set; }

        public double ValuePerConversion { get; set; }

        public long ViewThroughConversions { get; set; }

        [Required]
        [StringLength(512)]
        public string CreatedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime CreatedDatetime { get; set; }

        [Required]
        [StringLength(512)]
        public string LastModifiedBy { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime LastModifiedDatetime { get; set; }

        public virtual ConversionAction ConversionAction { get; set; }

        public virtual BudgetGroup2 BudgetGroup2 { get; set; }

        public virtual Gender Gender { get; set; }
    }
}
