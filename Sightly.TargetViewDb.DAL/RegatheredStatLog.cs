namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.RegatheredStatLog")]
    public partial class RegatheredStatLog
    {
        [Key]
        public int RegatheredStatId { get; set; }

        public Guid OrderId { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime StatDate { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime RegatheredDate { get; set; }
    }
}
