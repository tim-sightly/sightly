namespace Sightly.TargetViewDb.DAL
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("Orders.OrderImpressionsViewsByDate")]
    public partial class OrderImpressionsViewsByDate
    {
        [Key]
        [Column(Order = 0, TypeName = "date")]
        public DateTime statdate { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(256)]
        public string accountName { get; set; }

        public long? adwordscampaignid { get; set; }

        public Guid? orderid { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(32)]
        public string OrderStatus { get; set; }

        public long? OrderImpressions { get; set; }

        public long? AdsImpressions { get; set; }

        public long? AdsCompletedViews { get; set; }

        public long? OrderCompletedViews { get; set; }
    }
}
