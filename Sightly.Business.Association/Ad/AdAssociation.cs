using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.Association.Ad
{
    public class AdAssociation : IAdAssociation
    {
        private readonly IStatRepository _statRepository;
        private readonly IAd _adwordAd;

        public AdAssociation(IStatRepository statRepository, IAd adwordAd)
        {
            _statRepository = statRepository;
            _adwordAd = adwordAd;
        }

        public void AssociateAwAdToTv()
        {
            var awAdList = new List<AdAwAdData>();
            var adData = _statRepository.GetChannelPartnerCampaignList().Where(bg => bg.AdWordsAdId == null && bg.AdId != Guid.Empty).ToList();

            if (adData.Count > 0)
            {
                adData.ForEach(ad =>
                {
                    var adRecords = _adwordAd.GetAdData(ad.AdWordsCustomerId.ToString());

                    adRecords.ForEach(awAd =>
                    {
                        var awAdId = GetAdIdFromAdName(awAd.AdName);

                        if (ad.AdId != awAdId) return;

                        awAdList.Add(new AdAwAdData
                        {
                            AdAdWordsInfoId = Guid.NewGuid(),
                            AdId = ad.AdId,
                            AdWordsAdId = awAd.AdId,
                            AdWordsAdName = awAd.AdName,
                            AdWordsCampaignId = awAd.AwCampaignId,
                            BudgetGroupId = ad.BudgetGroupId
                        });

                    });
                });

            }
            if(awAdList.Count > 0)
                _statRepository.SetAdwordsAdId(awAdList);
        }
        private Guid GetAdIdFromAdName(string adName)
        {
            var counter = adName.LastIndexOf(" - ", StringComparison.Ordinal) + 3;
            Guid result;
            Guid.TryParse(adName.Substring(counter, adName.Length - counter), out result);
            return result;
        }
    }
}