﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.Association.Campaign
{
    public class CampaignAssociation : ICampaignAssociation
    {
        private readonly IStatRepository _statRepository;
        private readonly ICampaign _adwordCampaign;

        public CampaignAssociation(IStatRepository statRepository, ICampaign adwordCampaign)
        {
            _statRepository = statRepository;
            _adwordCampaign = adwordCampaign;
        }

        public void AssociationAwCustomerToTv()
        {
            var campaignList = new List<BudgetGroupCampaignData>();

            var budgetGroupData = _statRepository.GetChannelPartnerCampaignList().Where(bg => bg.AdWordsCampaignId == null).ToList();
            if (budgetGroupData.Count <= 0) return;
            budgetGroupData.ToList().ForEach(campaign =>
            {
                var campRecord = _adwordCampaign.GetCampaignData(campaign.AdWordsCustomerId.ToString());
                campRecord.ForEach(camp =>
                {
                    var awBudgetGroupId = GetCampaignIdFromCampaignName(camp.AwCampaignName);
                    if (!awBudgetGroupId.HasValue || campaign.BudgetGroupId != awBudgetGroupId) return;

                    campaignList.Add(new BudgetGroupCampaignData
                    {
                        AdWordsCampaignId = camp.AwCampaignId,
                        BudgetGroupId = campaign.BudgetGroupId,
                        LastModifiedBy = "Data Pipeline"
                    });
                });
            });
            _statRepository.SetAdwordsCampaignId(campaignList);
        }


        private Guid? GetCampaignIdFromCampaignName(string campaignName)
        {
            try
            {
                var counter = campaignName.LastIndexOf(" - ", StringComparison.Ordinal) + 3;
                return Guid.Parse(campaignName.Substring(counter, campaignName.Length - counter));
            }
            catch (Exception)
            {
                return (Guid?) null;
            }
        }
    }
}