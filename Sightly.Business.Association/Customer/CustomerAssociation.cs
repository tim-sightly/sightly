﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.Association.Customer
{
    public class CustomerAssociation : ICustomerAssociation
    {
        private readonly IStatRepository _statRepository;
        private readonly IAccountHierarchy _accountHierarchy;

        public CustomerAssociation(IStatRepository statRepository, IAccountHierarchy accountHierarchy)
        {
            _statRepository = statRepository;
            _accountHierarchy = accountHierarchy;
        }


        public void AssociationAwCustomerToTv()
        {
            var customerIdUpdates = new List<AdvertiserCustomerData>();
            var accountData = _statRepository.GetChannelPartnerMccCustomerList().Where(cust => cust.AdwordsCustomerId == null).ToList();

            accountData.GroupBy(ad => ad.AdwordsMccAccountId).Select(grp => grp.ToList()).ToList().ForEach(customer =>
            {
                var adwordsCustomers = _accountHierarchy.GetAdwordsCustomerByMcc(customer.First().AdwordsMccAccountId).First().ChildCustomers.ToList();
                customer.ForEach(rec =>
                {
                    var awCustomer = adwordsCustomers.FirstOrDefault(ac => String.Equals(ac.Customer.AwCustomerName, rec.CustomerId, StringComparison.CurrentCultureIgnoreCase));
                    if (awCustomer != null)
                    {
                        customerIdUpdates.Add(new AdvertiserCustomerData
                        {
                            AdvertiserId = rec.AdvertiserId,
                            AdvertiserName = rec.AdvertiserName,
                            AdWordsCustomerId = awCustomer.Customer.AwCustomerId,
                            LastModifiedBy = "Data Pipeline"
                        });
                    }
                });
            });

            _statRepository.SetAdwordsCustomerId(customerIdUpdates);
        }
    }
}