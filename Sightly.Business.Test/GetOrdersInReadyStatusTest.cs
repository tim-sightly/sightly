﻿using System;
using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Sightly.Business.Test
{
    [TestClass]
    public class GetOrdersInReadyStatusTest
    {

        [TestMethod]
        public void GetOrdersInReadyStatus()
        {
            var context = new TargetViewDb.DAL.TargetViewDb();

            var readyOrders = context.Orders.Where(o => o.OrderStatu.OrderStatusName == "Ready").ToList();

            Assert.IsNotNull(readyOrders);
        }
    }
}
