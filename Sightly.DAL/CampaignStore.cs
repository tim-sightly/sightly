﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;
using Dapper;
using Sightly.DAL.DTO;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class CampaignStore : ADataStore, ICampaignStore
    {
        private readonly ILoggingWrapper _loggingWrapper;

        public CampaignStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }

        public List<Campaign> GetAllCampaigns(IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    string sql = @"SELECT cai.AdWordsCustomerId, c.* FROM Accounts.Campaign c
                                INNER JOIN [Accounts].[CampaignAdWordsInfo] cai ON c.CampaignId = cai.CampaignId;";

                    return db.Query<Campaign>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }

        public List<Campaign> GetAllMediaAgencyCampaigns(IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    string sql = @"SELECT 
                                    c.CampaignId AS CampaignId, 
                                    c.CampaignName AS CampaignName, 
                                    c.CampaignRefCode AS CampaignRefCode, 
                                    [dbo].[GetParentAccountNames](c.AccountId) AS ParentAccountNames 
                                    FROM Accounts.Campaign c
                                    INNER JOIN Accounts.Account a ON c.AccountId = a.AccountId
                                    WHERE a.AccountTypeId = 'D600FA41-FFC0-4B03-8272-CAE6E093082F';";

                    return db.Query<Campaign>(sql).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }

        public List<Campaign> GetAllCampaignsForAdWordsCustomerId(long adWordsCustomerId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    string sql = @"SELECT c.* FROM Accounts.Campaign c
                                INNER JOIN [Accounts].[CampaignAdWordsInfo] cai on c.CampaignId = cai.CampaignId
                                WHERE cai.AdwordsCustomerId = @AdWordsCustomerId;";

                    return db.Query<Campaign>(sql, new { AdWordsCustomerId = adWordsCustomerId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }
    }
}
