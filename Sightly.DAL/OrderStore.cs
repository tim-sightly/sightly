﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Sightly.DAL.DTO;
using Sightly.DAL.DTO.Audience;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class OrderStore : ADataStore, IOrderStore
    {
        private readonly ILoggingWrapper _loggingWrapper;
        private const int COMMAND_TIMEOUT = 300;

        public OrderStore(ILoggingWrapper logginWrapper)
        {
            _loggingWrapper = logginWrapper;
        }

        public int Insert(Order order, IDbConnection connection = null)
        {
            using (IDbConnection db = connection ?? new SqlConnection())
            {
                int rowsChanged = 0;
                string sql = @"INSERT INTO Orders.[Order] 
                       (OrderId,
                        OrderTypeId,
                        OrderName, 
                        OrderRefCode, 
                        AccountId, 
                        CampaignId, 
                        AdvertiserId,
                        AdvertiserSubCategoryId,
                        BudgetTypeId,
                        CampaignBudget,
                        MonthlyBudget,
                        MonthCount,
                        StartDate,
                        StartDateHard,
                        EndDate,
                        EndDateHard,
                        SpendFlex,
                        DeliveryPriorityId,
                        Deleted,
                        CreatedDatetime,
                        CreatorId,
                        CreatedBy,
                        LastModifiedBy,
                        LastModifiedDateTime) VALUES 
                       (@OrderId,
                        @OrderTypeId,
                        @OrderName, 
                        @OrderRefCode, 
                        @AccountId, 
                        @CampaignId, 
                        @AdvertiserId,
                        @AdvertiserSubCategoryId,
                        @BudgetTypeId,
                        @CampaignBudget,
                        @MonthlyBudget,
                        @MonthCount,
                        @StartDate,
                        @StartDateHard,
                        @EndDate,
                        @EndDateHard,
                        @SpendFlex,
                        @DeliveryPriorityId,
                        @Deleted,
                        @CreatedDatetime,
                        @CreatorId,
                        @CreatedBy,
                        @LastModifiedBy,
                        @LastModifiedDateTime);";

                rowsChanged = db.Execute(sql, order);

                if (rowsChanged == 0)
                {
                    throw new Exception("INSERT Order Failed.");
                }

                return rowsChanged;
            }
        }

        public int InsertOrderXrefBudgetGroup(OrderXrefBudgetGroup orderXrefBudgetGroup,
            IDbConnection connection = null)
        {
            using (IDbConnection db = connection ?? new SqlConnection())
            {
                int rowsChanged = 0;
                string sql = @"INSERT INTO Orders.OrderXrefBudgetGroup 
                       (OrderXrefBudgetGroupId, 
                        OrderId, 
                        BudgetGroupId, 
                        Deleted, 
                        CreatedBy, 
                        CreatedDatetime,
                        LastModifiedBy,
                        LastModifiedDatetime)
                        VALUES(@OrderXrefBudgetGroupId, 
                        @OrderId, 
                        @BudgetGroupId, 
                        @Deleted, 
                        @CreatedBy, 
                        @CreatedDatetime,
                        @LastModifiedBy,
                        @LastModifiedDatetime);";

                rowsChanged = db.Execute(sql, orderXrefBudgetGroup);

                if (rowsChanged == 0)
                {
                    throw new Exception("INSERT OrderXrefBudgetGroup Failed.");
                }

                return rowsChanged;
            }
        }

        public Order GetByBudgetGroupId(Guid budgetGroupId, IDbConnection connection = null)
        {
            var sql = @"SELECT * FROM Orders.[Order] o
                        INNER JOIN Orders.OrderXrefBudgetGroup oxbg ON o.OrderId = oxbg.OrderId
                        WHERE oxbg.BudgetGroupId = @BudgetGroupId
                        ORDER BY o.CreatedDatetime desc;";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<Order>(sql, new {BudgetGroupId = budgetGroupId}, commandTimeout: COMMAND_TIMEOUT).First();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public List<Guid> GetBudgetGroupsByOrderId(Guid orderId, IDbConnection connection = null)
        {
            var sql = @"SELECT oxbg.BudgetGroupId FROM Orders.[Order] o
                        INNER JOIN Orders.OrderXrefBudgetGroup oxbg ON o.OrderId = oxbg.OrderId
                        WHERE o.OrderId = @OrderId
                        ORDER BY o.CreatedDatetime desc;";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<Guid>(sql, new {OrderId = orderId}, commandTimeout: COMMAND_TIMEOUT).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public OrderEmail GetOrderEmailByOrder(Guid orderId, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE tvdb.GetOrderEmailInformationByOrder @OrderId";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<OrderEmail>(sql, new { OrderId = orderId }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw; ;
            }
        }

        public List<OrderChangeInfo> GetOrderChangeInfoByOrder(Guid orderId, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE tvdb.GetTargetviewChangesForChangeOrder @OrderId";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<OrderChangeInfo>(sql, new { OrderId = orderId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw; ;
            }
        }

        public List<OrderTargetAgeGroup> GetTargetAgeGroupsForOrder(Guid orderId, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE tvdb.GetOrderAgeRangesExtendedByOrderId @OrderId";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<OrderTargetAgeGroup>(sql, new { OrderId = orderId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw; ;
            }
        }

        public List<OrderTargetGenderGroup> GetTargetGenderGroupsForOrder(Guid orderId, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE tvdb.GetOrdersGendersExtendedByOrderId @OrderId";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<OrderTargetGenderGroup>(sql, new { OrderId = orderId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw; ;
            }
        }

        public List<OrderParentalStatusGroup> GetParentalStatusGroupsForOrder(Guid orderId, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE tvdb.GetOrderParentalStatusExtendedByOrderId @OrderId";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<OrderParentalStatusGroup>(sql, new { OrderId = orderId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw; ;
            }
        }

        public List<OrderHouseholdIncomeGroup> GetHouseholdIncomeGroupsForOrder(Guid orderId, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE [tvdb].[GetOrdersHouseholdIncomeExtendedByOrderId] @OrderId";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<OrderHouseholdIncomeGroup>(sql, new { OrderId = orderId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw; ;
            }
        }
    }
}
