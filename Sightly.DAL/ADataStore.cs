﻿using System;
using System.Configuration;

namespace Sightly.DAL
{
    public abstract class ADataStore
    {
        protected static string ConnString { get; private set; }
        protected static int DapperTimeOut { get; private set; }

        public  ADataStore()
        {
            ConnString = ConfigurationManager.ConnectionStrings["TargetView"].ConnectionString;
            DapperTimeOut = Convert.ToInt32(ConfigurationManager.AppSettings["DapperTimeOut"]);            
        }
    }
}
