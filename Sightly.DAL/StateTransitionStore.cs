﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Transactions;
using Sightly.DAL.DTO;
using Sightly.DAL.DTO.BudgetGroupStates;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class StateTransitionStore : ADataStore, IStateTransitionStore
    {
        private ILoggingWrapper _loggingWrapper;
        private readonly IBudgetGroupStore _budgetGroupStore;
        private readonly IBudgetGroupHistoryStore _budgetGroupHistoryStore;
        private readonly IBudgetGroupLedgerStore _budgetGroupLedgerStore;

        public StateTransitionStore(ILoggingWrapper loggingWrapper, IBudgetGroupStore budgetGroupStore, IBudgetGroupHistoryStore budgetGroupHistoryStore, IBudgetGroupLedgerStore budgetGroupLedgerStore)
        {
            _loggingWrapper = loggingWrapper;
            _budgetGroupStore = budgetGroupStore;
            _budgetGroupHistoryStore = budgetGroupHistoryStore;
            _budgetGroupLedgerStore = budgetGroupLedgerStore;
        }

        public BudgetGroup MoveToLive(BudgetGroup budgetGroup, BudgetGroupHistory budgetGroupHistory, BudgetGroupLedger budgetGroupLedger, bool liveBudgetGroupExists)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                {
                    using (var transaction = new TransactionScope())
                    {
                        BudgetGroup newBudgetGroup;

                        if (liveBudgetGroupExists)
                        {
                            _budgetGroupStore.DeleteChildrenByBudgetGroupId(budgetGroup.BudgetGroupId, db);
                            db.ConnectionString = ConnString;
                            newBudgetGroup = _budgetGroupStore.UpdateBudgetGroupInsertChildren(budgetGroup, db);
                        }
                        else
                        {
                            newBudgetGroup = _budgetGroupStore.CreateBudgetGroup(budgetGroup);
                        }

                        db.ConnectionString = ConnString;
                        _budgetGroupHistoryStore.CreateBudgetGroupHistory(budgetGroupHistory, db);
                        db.ConnectionString = ConnString;
                        _budgetGroupLedgerStore.InsertIntoBudgetGroupLedger(budgetGroupLedger, db);

                        transaction.Complete();
                        return newBudgetGroup;
                    }                   
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
