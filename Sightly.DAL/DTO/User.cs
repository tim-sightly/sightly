﻿using System;

namespace Sightly.DAL.DTO
{
    public class User
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public Guid AccountId { get; set; }
    }
}
