namespace Sightly.DAL.DTO
{
    public class KpiEditorData
    {
        public int AdwordsCustomerKpiId { get; set; }
        public long AdwordsCustomerId { get; set; }
        public string KpiMetricName { get; set; }
        public string KpiAbbreviation { get; set; }
        public string ConditionDisplay { get; set; }
        public string EquationValue { get; set; }
        public int Sequence { get; set; }
        public string OutcomeTypeName { get; set; }
    }
}