﻿using System;

namespace Sightly.DAL.DTO
{
    public class Order
    {
        public Guid OrderId { get; set; }
        public Guid OrderTypeId { get; set; }
        public string OrderName { get; set; }
        public string OrderRefCode { get; set; }
        public Guid AccountId { get; set; }
        public Guid CampaignId { get; set; }
        public Guid AdvertiserId { get; set; }
        public Guid AdvertiserSubCategoryId { get; set; }

        public Guid BudgetTypeId { get; set; }

        public decimal? CampaignBudget { get; set; }
        public decimal? MonthlyBudget { get; set; }
        public decimal? SightlyMargin { get; set; }

        public int MonthCount { get; set; }
        public DateTime StartDate { get; set; }
        public bool StartDateHard { get; set; }
        public DateTime EndDate { get; set; }
        public bool EndDateHard { get; set; }
        public bool SpendFlex { get; set; }
        public Guid? DeliveryPriorityId { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedDatetime { get; set; }

        public Guid CreatorId { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDateTime { get; set; }
    }
}
