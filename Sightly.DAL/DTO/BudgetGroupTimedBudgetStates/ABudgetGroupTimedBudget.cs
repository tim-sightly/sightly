﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupTimedBudgetStates
{
    public abstract class ABudgetGroupTimedBudget
    {
        public Guid BudgetGroupTimedBudgetId { get;set; }
        public Guid BudgetGroupId { get; set; }
        public decimal BudgetAmount { get; set; }
        public decimal? Margin { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }      
        public Guid? BudgetGroupXrefPlacementId { get; set; } 
        public Guid? OrderId { get; set; }
    }
}
