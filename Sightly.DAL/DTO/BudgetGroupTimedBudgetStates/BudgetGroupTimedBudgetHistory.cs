﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupTimedBudgetStates
{
    public class BudgetGroupTimedBudgetHistory : ABudgetGroupTimedBudget
    {
        public Guid CreatedBy { get; set; }
        public DateTime Created { get; set; }
    }
}
