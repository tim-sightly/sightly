﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupTimedBudgetStates
{
    public class BudgetGroupTimedBudgetProposal : ABudgetGroupTimedBudget
    {
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public Guid? CreatedById { get; set; }

        public decimal NetBudgetGroup => BudgetAmount * (1 - Margin) ?? BudgetAmount;
    }
}
