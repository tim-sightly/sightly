﻿using System;

namespace Sightly.DAL.DTO
{
    public class DailyStats
    {
        public DateTime StatDate { get; set; }
        public long Impressions { get; set; }
        public long Clicks { get; set; }
        public long Views { get; set; }
        public decimal Cost { get; set; }
        public decimal PacingRate { get; set; }
        public decimal Margin { get; set; }
        public long AwCustomerId { get; set; }
    }
}