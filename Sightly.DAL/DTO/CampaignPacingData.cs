﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.DAL.DTO
{
    public class CampaignPacingData
    {
        public string CampaignName { get; set; }
        public string AdWordsCampaignName { get; set; }
        public long AdWordsCustomerId { get; set; }
        public long AdWordsCampaignId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int TotalDays { get; set; }
        public int DaysSoFar { get; set; }
        public int DaysLeft { get; set; }
        public decimal Margin { get; set; }
        public decimal BudgetAmount { get; set; }
        public decimal CostSoFar { get; set; }
        public decimal RemainingBudget { get; set; }
        public decimal ProposedDailyBudget { get; set; }
        public decimal PacingRate { get; set; }
        public Guid BudgetGroupTimedBudgetId { get; set; }
        public float YesterdaysOverPacingRate { get; set; }
        public string CampaignManagerEmail { get; set; }
    }
}
