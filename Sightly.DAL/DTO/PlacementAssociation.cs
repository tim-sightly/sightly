﻿using System;

namespace Sightly.DAL.DTO
{
    public class PlacementAssociation
    {
        public string PlacementValue { get; set; }
        public long AdwordsCampaignId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public string CampaignManagerEmail { get; set; }
    }
}