﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefPlacementStates
{
    public abstract class ABudgetGroupXrefPlacement
    {
        public Guid BudgetGroupXrefPlacementId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public Guid PlacementId { get; set; }
    }
}
