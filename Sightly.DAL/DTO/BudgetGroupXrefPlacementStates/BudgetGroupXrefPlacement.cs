﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefPlacementStates
{
    public class BudgetGroupXrefPlacement : ABudgetGroupXrefPlacement
    {
        public Guid CreatedBy { get; set; }
        public DateTime Created { get; set; }
    }
}
