﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefPlacementStates
{
    public class BudgetGroupXrefPlacementHistory : ABudgetGroupXrefPlacement
    {
        public Guid CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public DateTime AddedToHistory { get; set; }
    }
}
