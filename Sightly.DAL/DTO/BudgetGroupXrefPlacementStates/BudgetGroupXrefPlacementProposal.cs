﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefPlacementStates
{
    public class BudgetGroupXrefPlacementProposal : ABudgetGroupXrefPlacement
    {
        public string AssignedBy { get; set; }
        public DateTime AssignedDate { get; set; }
        public Guid? AssignedById { get; set; }
    }
}
