﻿using Sightly.DAL.DTO.BudgetGroupTimedBudgetStates;
using Sightly.DAL.DTO.BudgetGroupXrefAdStates;
using Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates;
using Sightly.DAL.DTO.BudgetGroupXrefLocationStates;
using Sightly.DAL.DTO.BudgetGroupXrefPlacementStates;
using System;
using System.Collections.Generic;

namespace Sightly.DAL.DTO.BudgetGroupStates
{
    public class BudgetGroupProposal : ABudgetGroup
    {
        public Guid? AdId { get; set; }
        public Guid? AudienceId { get; set; }
        public decimal? BudgetGroupBudget { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }

        public Guid? CreatedById { get; set; }
        public Guid? ModifiedById { get; set; }

        public List<BudgetGroupXrefAdProposal> BudgetGroupXrefAdProposals { get; set; }
        public List<BudgetGroupTimedBudgetProposal> BudgetGroupTimedBudgetProposals { get; set; }
        public List<BudgetGroupXrefConversionActionProposal> BudgetGroupXrefConversionActionProposals { get; set; }
        public List<BudgetGroupXrefLocationProposal> BudgetGroupXrefLocationProposals { get; set; }
        public List<BudgetGroupXrefPlacementProposal> BudgetGroupXrefPlacementProposals { get; set; }
    }
}
