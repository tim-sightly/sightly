﻿using Sightly.DAL.DTO.BudgetGroupTimedBudgetStates;
using Sightly.DAL.DTO.BudgetGroupXrefAdStates;
using Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates;
using Sightly.DAL.DTO.BudgetGroupXrefLocationStates;
using Sightly.DAL.DTO.BudgetGroupXrefPlacementStates;
using System;
using System.Collections.Generic;

namespace Sightly.DAL.DTO.BudgetGroupStates
{
    public class BudgetGroupHistory : ABudgetGroup
    {
        public int BudgetGroupStatusId { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? Modified { get; set; }

        public List<BudgetGroupXrefAdHistory> BudgetGroupXrefAdHistories { get; set; }
        public List<BudgetGroupXrefLocationHistory> BudgetGroupXrefLocationHistories { get; set; }
        public List<BudgetGroupXrefConversionActionHistory> BudgetGroupXrefConversionActionHistories { get; set; }
        public List<BudgetGroupXrefPlacementHistory> BudgetGroupXrefPlacementHistories { get; set; }
        public List<BudgetGroupTimedBudgetHistory> BudgetGroupTimedBudgetHistories { get; set; }
    }
}
