﻿using System;
using System.Collections.Generic;
using Sightly.DAL.DTO.BudgetGroupTimedBudgetStates;
using Sightly.DAL.DTO.BudgetGroupXrefAdStates;
using Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates;
using Sightly.DAL.DTO.BudgetGroupXrefLocationStates;
using Sightly.DAL.DTO.BudgetGroupXrefPlacementStates;

namespace Sightly.DAL.DTO.BudgetGroupStates
{
    public class BudgetGroup : ABudgetGroup
    {      
        public int BudgetGroupStatusId { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? Modified { get; set; }

        public List<BudgetGroupXrefAd> BudgetGroupXrefAds { get; set; }
        public List<BudgetGroupXrefLocation> BudgetGroupXrefLocations { get; set; }
        public List<BudgetGroupXrefConversionAction> BudgetGroupXrefConversionActions { get; set; }
        public List<BudgetGroupXrefPlacement> BudgetGroupXrefPlacements { get; set; }
        public List<BudgetGroupTimedBudget> BudgetGroupTimedBudgets { get; set; }
    }
}
