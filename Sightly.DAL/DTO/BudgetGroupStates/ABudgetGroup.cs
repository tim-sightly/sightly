﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupStates
{
    public abstract class ABudgetGroup
    {
        public Guid BudgetGroupId { get; set; }
        public string BudgetGroupName { get; set; }
    }
}
