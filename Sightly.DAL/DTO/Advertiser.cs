﻿using System;

namespace Sightly.DAL.DTO
{
    public class Advertiser
    {
        public Guid AdvertiserId { get; set; }
        public Guid AccountId { get; set; }
        public string AdvertiserName { get; set; }
        public Guid AdvertiserCategoryId { get; set; }
        public decimal AdvertiserMargin { get; set; }
        public string AdvertiserRefCode { get; set; }
        public Guid AdvertiserSubCategoryId { get; set; }
    }
}