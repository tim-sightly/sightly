﻿using System;

namespace Sightly.DAL.DTO
{
    public class OrderBasic
    {
        public Guid OrderId { get; set; }
        public Guid OrderTypeId { get; set; }
        public string OrderName { get; set; }
        public string OrderRefCode { get; set; }
        public Guid AccountId { get; set; }
        public Guid CampaignId { get; set; }
        public Guid AdvertiserId { get; set; }
        public Guid OrderStatusId { get; set; }
        public bool SpendFlex { get; set; }
        public bool Deleted { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public Guid CreatorId { get; set; }
        public string CreatedBy { get; set; }
        public string LastModifiedBy { get; set; }

    }
}