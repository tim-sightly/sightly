﻿using System;

namespace Sightly.DAL.DTO
{
    public class OrderXrefBudgetGroup
    {
        public Guid OrderXrefBudgetGroupId { get; set; }
        public Guid OrderId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}
