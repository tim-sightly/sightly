﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefLocationStates
{
    public class BudgetGroupXrefLocationHistory : ABudgetGroupXrefLocation
    {
        public Guid CreatedBy { get; set; }
        public DateTime Created { get; set; }
    }
}
