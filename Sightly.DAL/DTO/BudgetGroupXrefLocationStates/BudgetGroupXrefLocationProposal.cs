﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefLocationStates
{
    public class BudgetGroupXrefLocationProposal : ABudgetGroupXrefLocation
    {
        public bool Deleted { get; set; }
        public int SequenceId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? ModifiedById { get; set; }
    }
}
