﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefLocationStates
{
    public class BudgetGroupXrefLocation : ABudgetGroupXrefLocation
    {
        public Guid CreatedBy { get; set; }
        public DateTime Created { get; set; }
    }
}
