﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefLocationStates
{
    public abstract class ABudgetGroupXrefLocation
    {
        public Guid BudgetGroupXrefLocationId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public Guid LocationId { get; set; }       
    }
}
