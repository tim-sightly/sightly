using System;

namespace Sightly.DAL.DTO
{
    public class MoatKpiStatsData
    {
        public long? AdvertiserId { get; set; }
        public decimal Two_Sec_In_View_Rate_Unfiltered { get; set; }
        public decimal ReachCompletePercent { get; set; }
        public decimal CustomerQuality { get; set; }
        public decimal HumanAndAvocRate { get; set; }
        public decimal HumanRate { get; set; }
        public DateTime? StatDate { get; set; }
        public long AwCustomerId { get; set; }
    }
}