﻿using System;

namespace Sightly.DAL.DTO
{
    public class Account
    {
        public Guid? AccountId { get; set; }
        public string AccountName { get; set; }
        public string ShortCode { get; set; }
        public Guid AccountTypeId { get; set; }
        public decimal AccountMargin { get; set; }
        public Guid ParentId { get; set; }
    }
}