﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates
{
    public abstract class ABudgetGroupXrefConversionAction
    {
        public Guid ConversionActionId { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime? Created { get; set; }
        public Guid? ModifiedBy { get; set; }
        public DateTime? Modified { get; set; }        
    }
}
