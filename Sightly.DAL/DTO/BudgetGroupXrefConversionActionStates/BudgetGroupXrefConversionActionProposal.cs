﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates
{
    public class BudgetGroupXrefConversionActionProposal : ABudgetGroupXrefConversionAction
    {
        public Guid BudgetGroupXrefConversionActionProposalId { get; set; }
        public Guid BudgetGroupProposalId { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? ModifiedById { get; set; }
    }
}
