﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates
{
    public class BudgetGroupXrefConversionAction : ABudgetGroupXrefConversionAction
    {
        public Guid BudgetGroupXrefConversionActionId { get; set; }
        public Guid BudgetGroupId { get; set; }
    }
}
