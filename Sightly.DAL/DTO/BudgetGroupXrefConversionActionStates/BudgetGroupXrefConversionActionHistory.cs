﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates
{
    public class BudgetGroupXrefConversionActionHistory : ABudgetGroupXrefConversionAction
    {
        public Guid BudgetGroupXrefConversionActionHistoryId { get; set; }
        public Guid BudgetGroupHistoryId { get; set; }
    }
}
