using System;

namespace Sightly.DAL.DTO
{
    public class NielsenKpiStatsData
    {
        public long? CustomerId { get; set; }
        public decimal DarComputer { get; set; }
        public decimal DarMobile { get; set; }
        public decimal DarTotalDigital { get; set; }
        public DateTime? StatDate { get; set; }
        public long AwCustomerId { get; set; }
    }
}