﻿using System;

namespace Sightly.DAL.DTO
{
    public class Campaign
    {
        public Guid CampaignId { get; set; }
        public Guid AccountId { get; set; }
        public Guid? AdvertiserId { get; set; }
        public string CampaignName { get; set; }
        public string CampaignRefCode { get; set; }
        public string ParentAccountNames { get; set; }
        public long AdWordsCustomerId { get; set; }
    }
}
