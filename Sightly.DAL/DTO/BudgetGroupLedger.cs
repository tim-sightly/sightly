﻿using System;

namespace Sightly.DAL.DTO
{
    public class BudgetGroupLedger
    {
        public long? BudgetGroupLedgerId { get; set; }
        public Guid? BudgetGroupId { get; set; }
        public Guid? BudgetGroupProposalId { get; set; }
        public Guid? BudgetGroupHistoryId { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public int? BudgetGroupEventId { get; set; }
        public string Notes { get; set; }
    }
}
