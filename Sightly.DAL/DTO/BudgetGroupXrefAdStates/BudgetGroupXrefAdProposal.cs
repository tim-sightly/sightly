using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefAdStates
{
    public class BudgetGroupXrefAdProposal : ABudgetGroupXrefAd
    {
        public int SequenceNo { get; set; }
        public bool Paused { get; set; }
        public bool Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? ModifiedById { get; set; }
    }
}
