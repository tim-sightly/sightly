using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefAdStates
{
    public class BudgetGroupXrefAdHistory : ABudgetGroupXrefAd
    {
        public Guid CreatedBy { get; set; }
        public DateTime Created { get; set; }
    }
}
