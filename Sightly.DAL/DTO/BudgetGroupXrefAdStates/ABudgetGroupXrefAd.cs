using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefAdStates
{
    public abstract class ABudgetGroupXrefAd
    {
        public Guid BudgetGroupXrefAdId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public Guid AdId { get; set; }       
    }
}
