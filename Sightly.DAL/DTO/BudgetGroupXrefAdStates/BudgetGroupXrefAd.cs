﻿using System;

namespace Sightly.DAL.DTO.BudgetGroupXrefAdStates
{
    public class BudgetGroupXrefAd : ABudgetGroupXrefAd
    {        
        public Guid CreatedBy { get; set; }
        public DateTime Created { get; set; }
    }
}
