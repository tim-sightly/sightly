﻿
namespace Sightly.DAL.DTO
{
    public class OrderChangeInfo
    {
        public string ChangeType { get; set; }
        public string ChangeDetails { get; set; }
    }
}
