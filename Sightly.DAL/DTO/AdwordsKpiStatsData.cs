namespace Sightly.DAL.DTO
{
    public class AdwordsKpiStatsData
    {
        public long CustomerId { get; set; }
        public decimal GS { get; set; }
        public decimal CPM { get; set; }
        public decimal CPV { get; set; }
        public decimal CPC { get; set; }
        public long Impressions { get; set; }
        public long Views { get; set; }
        public long Clicks { get; set; }
        public decimal VR { get; set; }
        public decimal CTR { get; set; }
    }
}