﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.DAL.DTO.Audience
{
    public class OrderHouseholdIncomeGroup
    {
        public Guid OrderId { get; set; }
        public int HouseholdIncomeGroupId { get; set; }
        public string GroupName { get; set; }
        public Boolean Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
    }
}
