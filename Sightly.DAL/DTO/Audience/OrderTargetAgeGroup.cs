﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.DAL.DTO.Audience
{
    public class OrderTargetAgeGroup
    {
        public Guid OrderId { get; set; }
        public Guid AgeGroupId { get; set; }
        public string AgeGroupName { get; set; }
        public Boolean Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}
