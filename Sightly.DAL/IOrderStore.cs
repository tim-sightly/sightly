﻿using System;
using System.Collections.Generic;
using System.Data;
using Sightly.DAL.DTO;
using Sightly.DAL.DTO.Audience;

namespace Sightly.DAL
{
    public interface IOrderStore
    {
        int Insert(Order order, IDbConnection connection = null);
        int InsertOrderXrefBudgetGroup(OrderXrefBudgetGroup orderXrefBudgetGroup, IDbConnection connection = null);
        Order GetByBudgetGroupId(Guid budgetGroupId, IDbConnection connection = null);
        List<Guid> GetBudgetGroupsByOrderId(Guid orderId, IDbConnection connection = null);

        OrderEmail GetOrderEmailByOrder(Guid orderId, IDbConnection connection = null);
        List<OrderChangeInfo> GetOrderChangeInfoByOrder(Guid orderId, IDbConnection connection = null);

        List<OrderTargetAgeGroup> GetTargetAgeGroupsForOrder(Guid orderId, IDbConnection connection = null);
        List<OrderTargetGenderGroup> GetTargetGenderGroupsForOrder(Guid orderId, IDbConnection connection = null);
        List<OrderParentalStatusGroup> GetParentalStatusGroupsForOrder(Guid orderId, IDbConnection connection = null);

        List<OrderHouseholdIncomeGroup> GetHouseholdIncomeGroupsForOrder(Guid orderId, IDbConnection connection = null);
    }
}
