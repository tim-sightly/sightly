﻿using System;
using System.Collections.Generic;
using System.Data;
using Sightly.DAL.DTO;

namespace Sightly.DAL
{
    public interface ICampaignCreationStore
    {
        List<Account> GetAllAccounts(IDbConnection connection = null);
        List<Account> GetAllAccountsByUser(Guid userId, IDbConnection connection = null);
        List<Advertiser> GetAdvertiserByAccountId(Guid accountId, IDbConnection connection = null);
        List<Advertiser> GetAdvertiserByAccountIdAndUser(Guid accountId, Guid userId, IDbConnection connection = null);
        Account InsertSubAccount(string accountName, Guid accountTypeId, Guid parentAccountId, IDbConnection connection = null);
        Advertiser InsertAdvertiser(string advertiserName, Guid accountId, IDbConnection connection = null);
        Campaign InsertCampaign(string campaignName, Guid advertiserId, IDbConnection connection = null);
        OrderBasic InsertOrder(string orderName, string orderRefCode, Guid campaignId, IDbConnection connection = null);
        Advertiser InsertAdvertiserExtended(string advertiserName, string advertiserRefCode, Guid accountId, Guid advertiserCategoryId, Guid advertiserSubCategoryId, IDbConnection connection = null);

        List<Advertiser> GetAllAdvertisersByUser(Guid userId, IDbConnection connection = null);
        List<CampaignAbbreviated> GetAllCampaignsByUser(Guid userId, IDbConnection connection = null);
    }
}