﻿using System;
using System.Collections.Generic;
using System.Data;
using Sightly.DAL.DTO;

namespace Sightly.DAL
{
    public interface IDoubleVerifyStore
    {
        List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyAccount(Guid accountId, IDbConnection connection = null);
        List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyAdvertiser(Guid advertiserId, IDbConnection connection = null);
        List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyCampaign(Guid campaignId, IDbConnection connection = null);
    }
}