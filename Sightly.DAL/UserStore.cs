﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Sightly.DAL.DTO;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class UserStore : ADataStore, IUserStore
    {
        private readonly ILoggingWrapper _loggingWrapper;

        public UserStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }

        public User GetUserByEmail(string userEmail, IDbConnection connection = null)
        {
            var sql = @"SELECT * FROM Users.[User] WHERE Email = @Email;";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<User>(sql, new { Email = userEmail }).FirstOrDefault();
                }

            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}
