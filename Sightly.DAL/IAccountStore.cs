﻿using System.Collections.Generic;
using System.Data;
using Sightly.DAL.DTO;

namespace Sightly.DAL
{
    public interface IAccountStore
    {
        List<Account> ListAllAccounts(IDbConnection connection = null);
        List<Advertiser> ListAllAdvertisers(IDbConnection connection = null);
    }
}