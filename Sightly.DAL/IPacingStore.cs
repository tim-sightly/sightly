﻿using System;
using System.Collections.Generic;
using System.Data;
using Sightly.DAL.DTO;

namespace Sightly.DAL
{
    public interface IPacingStore
    {
        List<CampaignPacingData> GetMediaAgencyPacingDataByDate(DateTime pacingDate, IDbConnection connection = null);
        List<PlacementAssociation> GetMediaAgencyPlacementByDate(DateTime pacingDate, IDbConnection connection = null);
        int SaveProposedBudgetPacing(
                long customerId, 
                long campaignId, 
                decimal recomendedBudget, 
                float overPacingRate, 
                DateTime startDate, 
                decimal totalBudget, 
                int daysRemaining, 
                Guid budgetGroupTimedBudgetId, 
                DateTime pacingDate, 
                Guid userId,
                IDbConnection connection = null);

        List<CustomerPacingDate> GetLastPaceDateForAllCustomers(IDbConnection connection = null);

        List<Manager> GetManagers(IDbConnection connection = null);
    }
}