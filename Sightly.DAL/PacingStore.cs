﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Sightly.DAL.DTO;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class PacingStore : ADataStore, IPacingStore
    {
        private readonly ILoggingWrapper _loggingWrapper;

        public PacingStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }

        public List<CampaignPacingData> GetMediaAgencyPacingDataByDate(DateTime pacingDate, IDbConnection connection = null)
        { 
            const string sql = @"EXECUTE tvdb.GetMediaAgencyPacingByDate @PacingDate;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<CampaignPacingData>(sql, new { PacingDate = pacingDate }, commandTimeout: DapperTimeOut).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public List<PlacementAssociation> GetMediaAgencyPlacementByDate(DateTime pacingDate, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE tvdb.GetMediaAgencyPlacementByDate @PacingDate;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<PlacementAssociation>(sql, new { PacingDate = pacingDate }, commandTimeout: DapperTimeOut).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public int SaveProposedBudgetPacing(long customerId, long campaignId, decimal recomendedBudget, float overPacingRate, DateTime startDate, decimal totalBudget, int daysRemaining, Guid budgetGroupTimedBudgetId, DateTime pacingDate, Guid userId, IDbConnection connection = null)
        {
            const string sql = @"Execute tvdb.SaveProposedBudgetPacing @CustomerId, @CampaignId, @RecomendedBudget, @OverPacingRate, @StartDate, @TotalBudget, @DaysRemaining, @BudgetGroupTimedBudgetId, @PacingDate, @UserId;";
            try
            {
                var rowsChanged = 0;
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    rowsChanged = db.Execute(sql, new
                    {
                        CustomerId = customerId,
                        CampaignId = campaignId,
                        RecomendedBudget = recomendedBudget,
                        OverPacingRate = overPacingRate,
                        StartDate = startDate,
                        TotalBudget = totalBudget,
                        DaysRemaining = daysRemaining,
                        BudgetGroupTimedBudgetId = budgetGroupTimedBudgetId,
                        PacingDate = pacingDate,
                        UserId = userId
                    });
                }
                if (rowsChanged == 0)
                {
                    throw new Exception("BudgetGroupProposal INSERT Failed.");
                }

                return rowsChanged;
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public List<CustomerPacingDate> GetLastPaceDateForAllCustomers(IDbConnection connection = null)
        {
            const string sql = @"SELECT MAX(PacingDate) AS PacingDate, CustomerId FROM [adw].[DailyPacing] GROUP BY CustomerId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    List<CustomerPacingDate> lastPacedDates = db.Query<CustomerPacingDate>(sql).ToList();
                    return lastPacedDates;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }

        }

        public List<Manager> GetManagers(IDbConnection connection = null)
        {
            try
            {
                string sql = @"SELECT * FROM [Users].[User];";
                using(IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    List<Manager> managers = db.Query<Manager>(sql).ToList();
                    return managers;
                }
            }
            catch(Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}