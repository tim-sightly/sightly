﻿using System.Data;
using Sightly.DAL.DTO;

namespace Sightly.DAL
{
    public interface IUserStore
    {
        User GetUserByEmail(string userEmail, IDbConnection connection = null);
    }
}
