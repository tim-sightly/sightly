﻿using Sightly.DAL.DTO;
using Sightly.DAL.DTO.BudgetGroupStates;

namespace Sightly.DAL
{
    public interface IStateTransitionStore
    {
        BudgetGroup MoveToLive(BudgetGroup budgetGroup, BudgetGroupHistory budgetGroupHistory, BudgetGroupLedger budgetGroupLedger, bool liveBudgetGroupExists);
    }
}
