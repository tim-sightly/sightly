﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Sightly.DAL.DTO;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class CampaignCreationStore : ADataStore, ICampaignCreationStore
    {
        private readonly ILoggingWrapper _loggingWrapper;

        public CampaignCreationStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }

        public List<Account> GetAllAccounts(IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    const string sql = @"SELECT AccountId, AccountName, AccountTypeId, ParentAccountId as ParentId FROM Accounts.Account";

                    return db.Query<Account>(sql, null).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }

        public List<Account> GetAllAccountsByUser(Guid userId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    const string sql = @"EXECUTE tvdb.GetAllAccountsByUser @UserId";

                    return db.Query<Account>(sql, new { UserId = userId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }

        public List<Advertiser> GetAllAdvertisersByUser(Guid userId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    const string sql = @"EXECUTE tvdb.GetAllAdvertisersByUser @UserId";

                    return db.Query<Advertiser>(sql, new { UserId = userId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }

        public List<CampaignAbbreviated> GetAllCampaignsByUser(Guid userId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    const string sql = @"EXECUTE tvdb.GetAllCampaignsByUser @UserId";

                    return db.Query<CampaignAbbreviated>(sql, new { UserId = userId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }

        public List<Advertiser> GetAdvertiserByAccountId(Guid accountId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    const string sql = @"EXECUTE tvdb.GetAdvertisersUnderAccountWithSub_ByAccountId @AccountId";

                    return db.Query<Advertiser>(sql, new { AccountId = accountId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }

        public List<Advertiser> GetAdvertiserByAccountIdAndUser(Guid accountId, Guid userId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    const string sql = @"Execute tvdb.GetAdvertiserByAccountAndUser @AccountId, @UserId";

                    return db.Query<Advertiser>(sql, new { AccountId = accountId, UserId = userId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }

        public Account InsertSubAccount(string accountName, Guid accountTypeId, Guid parentAccountId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    const string sql = @"EXECUTE tvdb.InsertSubAccount @AccountName, @AccountTypeId, @ParentAccountId";

                    return db.Query<Account>(
                        sql, 
                        new
                        {
                            AccountName = accountName,
                            AccountTypeId = accountTypeId,
                            ParentAccountId = parentAccountId
                        }).First();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }

        public Advertiser InsertAdvertiser(string advertiserName, Guid accountId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    const string sql = @"EXECUTE tvdb.InsertAdvertiser @AdvertiserName, @AccountId";

                    return db.Query<Advertiser>(
                        sql,
                        new
                        {
                            AdvertiserName = advertiserName,
                            AccountId = accountId
                        }).First();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }

        public Campaign InsertCampaign(string campaignName, Guid advertiserId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    const string sql = @"EXECUTE tvdb.InsertCampaign @CampaignName, @AdvertiserId";

                    return db.Query<Campaign>(
                        sql,
                        new
                        {
                            CampaignName = campaignName,
                            AdvertiserId = advertiserId
                        }).First();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }

        public OrderBasic InsertOrder(string orderName, string orderRefCode, Guid campaignId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    const string sql = @"EXECUTE tvdb.InsertOrderBasic @OrderName, @OrderRefCode, @CampaignId";

                    return db.Query<OrderBasic>(
                        sql,
                        new
                        {
                            OrderName = orderName,
                            OrderRefCode = orderRefCode,
                            CampaignId = campaignId
                        }).First();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }

        public Advertiser InsertAdvertiserExtended(string advertiserName, string advertiserRefCode, Guid accountId,
            Guid advertiserCategoryId, Guid advertiserSubCategoryId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    const string sql = @"EXECUTE tvdb.InsertAdvertiserExtended @AdvertiserName, @AdvertiserRefCode, @AccountId, @AdvertiserCategoryId, @AdvertiserSubCategoryId";

                    return db.Query<Advertiser>(
                        sql,
                        new
                        {
                            AdvertiserName = advertiserName,
                            AdvertiserRefCode = advertiserRefCode,
                            AccountId = accountId,
                            AdvertiserCategoryId = advertiserCategoryId,
                            AdvertiserSubCategoryId = advertiserSubCategoryId
                        }).First();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }
        }
    }
}