﻿using Sightly.DAL.DTO.BudgetGroupStates;
using Sightly.DAL.DTO.BudgetGroupTimedBudgetStates;
using Sightly.DAL.DTO.BudgetGroupXrefAdStates;
using Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates;
using Sightly.DAL.DTO.BudgetGroupXrefLocationStates;
using Sightly.DAL.DTO.BudgetGroupXrefPlacementStates;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Transactions;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class BudgetGroupStore : ADataStore, IBudgetGroupStore
    {
        private readonly ILoggingWrapper _loggingWrapper;
        private const int COMMAND_TIMEOUT = 300;

        public BudgetGroupStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }

        public BudgetGroup CreateBudgetGroup(BudgetGroup budgetGroup, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection())
                using (var transaction = new TransactionScope())
                {
                    db.ConnectionString = ConnString;
                    InsertIntoBudgetGroup(budgetGroup, db);
                    InsertIntoBudgetGroupXrefAd(budgetGroup.BudgetGroupXrefAds, db);
                    InsertIntoBudgetGroupXrefConversionAction(budgetGroup.BudgetGroupXrefConversionActions, db);
                    InsertIntoBudgetGroupXrefLocation(budgetGroup.BudgetGroupXrefLocations, db);
                    InsertIntoBudgetGroupXrefPlacement(budgetGroup.BudgetGroupXrefPlacements, db);
                    InsertIntoBudgetGroupTimedBudget(budgetGroup.BudgetGroupTimedBudgets, db);

                    transaction.Complete();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
            return budgetGroup;
        }

        public BudgetGroup UpdateCompleteBudgetGroup(BudgetGroup budgetGroup)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                {
                    using (var transaction = new TransactionScope())
                    {
                        UpdateBudgetGroup(budgetGroup, db);
                        UpdateBudgetGroupXrefAd(budgetGroup.BudgetGroupXrefAds, db);
                        UpdateBudgetGroupTimedBudget(budgetGroup.BudgetGroupTimedBudgets, db);
                        UpdateBudgetGroupXrefConversionAction(budgetGroup.BudgetGroupXrefConversionActions, db);
                        UpdateBudgetGroupXrefLocation(budgetGroup.BudgetGroupXrefLocations, db);
                        UpdateBudgetGroupXrefPlacement(budgetGroup.BudgetGroupXrefPlacements, db);

                        transaction.Complete();
                    }
                    return GetBudgetGroupById(budgetGroup.BudgetGroupId, db);
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public BudgetGroup UpdateBudgetGroup(BudgetGroup budgetGroup, Dictionary<string, List<Guid>> itemsToDelete)
        {
            if (budgetGroup == null)
                return null;

            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                {
                    using (var transaction = new TransactionScope())
                    {
                        UpdateBudgetGroup(budgetGroup, db);

                        DeleteBudgetGroupXrefAds(itemsToDelete["ads"], db);
                        DeleteBudgetGroupXrefLocations(itemsToDelete["locations"], db);
                        DeleteBudgetGroupTimedBudget(itemsToDelete["timedBudget"], db);
                        DeleteBudgetGroupXrefConversionActions(itemsToDelete["conversionActions"], db);
                        DeleteBudgetGroupXrefPlacements(itemsToDelete["placements"], db);

                        InsertIntoBudgetGroupXrefAd(budgetGroup.BudgetGroupXrefAds, db);
                        InsertIntoBudgetGroupXrefLocation(budgetGroup.BudgetGroupXrefLocations, db);
                        InsertIntoBudgetGroupTimedBudget(budgetGroup.BudgetGroupTimedBudgets, db);
                        InsertIntoBudgetGroupXrefConversionAction(budgetGroup.BudgetGroupXrefConversionActions, db);
                        InsertIntoBudgetGroupXrefPlacement(budgetGroup.BudgetGroupXrefPlacements, db);

                        transaction.Complete();
                    }
                    return GetBudgetGroupById(budgetGroup.BudgetGroupId, db);
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public BudgetGroup GetById(Guid budgetGroupId, IDbConnection connection = null)
        {
            var q1 = @"SELECT * FROM Live.BudgetGroup WHERE BudgetGroupId = @BudgetGroupId;";
            var q2 = @"SELECT * FROM Live.BudgetGroupXrefAd WHERE BudgetGroupId = @BudgetGroupId;";
            var q3 = @"SELECT * FROM Live.BudgetGroupXrefLocation WHERE BudgetGroupId = @BudgetGroupId;";
            var q4 = @"SELECT * FROM Live.BudgetGroupXrefConversionAction WHERE BudgetGroupId = @BudgetGroupId;";
            var q5 = @"SELECT * FROM Live.BudgetGroupXrefPlacement WHERE BudgetGroupId = @BudgetGroupId;";
            var q6 = @"SELECT * FROM Live.BudgetGroupTimedBudget WHERE BudgetGroupId = @BudgetGroupId;";
            var sql = $"{q1}{q2}{q3}{q4}{q5}{q6}";

            try
            {
                using (var db = connection ?? new SqlConnection(ConnString))
                using (var multi = db.QueryMultiple(sql, new { BudgetGroupId = budgetGroupId }, commandTimeout: COMMAND_TIMEOUT))
                {
                    var budgetGroup = multi.Read<BudgetGroup>().SingleOrDefault();
                    if (budgetGroup != null)
                    {
                        budgetGroup.BudgetGroupXrefAds = multi.Read<BudgetGroupXrefAd>().ToList();
                        budgetGroup.BudgetGroupXrefLocations = multi.Read<BudgetGroupXrefLocation>().ToList();
                        budgetGroup.BudgetGroupXrefConversionActions = multi.Read<BudgetGroupXrefConversionAction>().ToList();
                        budgetGroup.BudgetGroupXrefPlacements = multi.Read<BudgetGroupXrefPlacement>().ToList();
                        budgetGroup.BudgetGroupTimedBudgets = multi.Read<BudgetGroupTimedBudget>().ToList();
                    }
                    return budgetGroup;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public BudgetGroup GetBudgetGroupById(Guid budgetGroupId, IDbConnection connection = null, bool withChildren = true)
        {
            var q1 = @"SELECT * FROM Live.BudgetGroup WHERE BudgetGroupId = @BudgetGroupId;";
            var q2 = @"SELECT * FROM Live.BudgetGroupXrefAd WHERE BudgetGroupId = @BudgetGroupId;";
            var q3 = @"SELECT * FROM Live.BudgetGroupXrefLocation WHERE BudgetGroupId = @BudgetGroupId;";
            var q4 = @"SELECT * FROM Live.BudgetGroupXrefConversionAction WHERE BudgetGroupId = @BudgetGroupId;";
            var q5 = @"SELECT * FROM Live.BudgetGroupXrefPlacement WHERE BudgetGroupId = @BudgetGroupId;";
            var q6 = @"SELECT * FROM Live.BudgetGroupTimedBudget WHERE BudgetGroupId = @BudgetGroupId;";
            var sql = $"{q1}{q2}{q3}{q4}{q5}{q6}";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                using (var multi = db.QueryMultiple(sql, new { BudgetGroupId = budgetGroupId }, commandTimeout: COMMAND_TIMEOUT))
                {
                    var budgetGroup = multi.Read<BudgetGroup>().SingleOrDefault();
                    if (budgetGroup != null && withChildren)
                    {
                        budgetGroup.BudgetGroupXrefAds = multi.Read<BudgetGroupXrefAd>().ToList();
                        budgetGroup.BudgetGroupXrefLocations = multi.Read<BudgetGroupXrefLocation>().ToList();
                        budgetGroup.BudgetGroupXrefConversionActions = multi.Read<BudgetGroupXrefConversionAction>().ToList();
                        budgetGroup.BudgetGroupXrefPlacements = multi.Read<BudgetGroupXrefPlacement>().ToList();
                        budgetGroup.BudgetGroupTimedBudgets = multi.Read<BudgetGroupTimedBudget>().ToList();
                    }
                    return budgetGroup;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public List<BudgetGroup> GetBy(Guid currentUser, string column, int? statusId = null, Guid? filterByUser = null, bool withChildren = true)
        {
            var columnQueryLookup = new Dictionary<string, Tuple<string, dynamic>>()
            {
                {
                    "None",
                    new Tuple<string, dynamic>("SELECT * FROM Live.BudgetGroup;", new { })
                },
                {
                    "BudgetGroupStatusId",
                    new Tuple<string, dynamic>("SELECT * FROM Live.BudgetGroup WHERE BudgetGroupStatusId = @BudgetGroupStatusId;", new { BudgetGroupStatusId = statusId ?? -1 })
                },
                {
                    "CreatedBy",
                    new Tuple<string, dynamic>("SELECT * FROM Live.BudgetGroup WHERE CreatedBy = @CreatedBy;", new { CreatedBy = filterByUser })
                },
                {
                    "ModifiedBy",
                    new Tuple<string, dynamic>("SELECT * FROM Live.BudgetGroup WHERE ModifiedBy = @ModifiedBy;", new { Modified = filterByUser })
                },
            };

            var q1 = columnQueryLookup[column].Item1;
            var q2 = @"SELECT * FROM Live.BudgetGroupXrefAd WHERE BudgetGroupId = @BudgetGroupId;";
            var q3 = @"SELECT * FROM Live.BudgetGroupXrefLocation WHERE BudgetGroupId = @BudgetGroupId;";
            var q4 = @"SELECT * FROM Live.BudgetGroupXrefConversionAction WHERE BudgetGroupId = @BudgetGroupId;";
            var q5 = @"SELECT * FROM Live.BudgetGroupXrefPlacement WHERE BudgetGroupId = @BudgetGroupId;";
            var q6 = @"SELECT * FROM Live.BudgetGroupTimedBudget WHERE BudgetGroupId = @BudgetGroupId;";
            var sql = $"{q2}{q3}{q4}{q5}{q6}";

            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                using (var multi = db.QueryMultiple(q1, (object)columnQueryLookup[column].Item2))
                {
                    var budgetGroups = multi.Read<BudgetGroup>().ToList();
                    if (withChildren)
                    {
                        foreach (var bgp in budgetGroups)
                        {
                            using (var multiChildren = db.QueryMultiple(sql, new {bgp.BudgetGroupId }))
                            {
                                bgp.BudgetGroupXrefAds = multiChildren.Read<BudgetGroupXrefAd>().ToList();
                                bgp.BudgetGroupXrefLocations = multiChildren.Read<BudgetGroupXrefLocation>().ToList();
                                bgp.BudgetGroupXrefConversionActions = multiChildren.Read<BudgetGroupXrefConversionAction>().ToList();
                                bgp.BudgetGroupXrefPlacements = multiChildren.Read<BudgetGroupXrefPlacement>().ToList();
                                bgp.BudgetGroupTimedBudgets = multiChildren.Read<BudgetGroupTimedBudget>().ToList();
                            }
                        }
                    }
                    return budgetGroups;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public BudgetGroup CreateOrUpdateBudgetGroup(BudgetGroup budgetGroup, IDbConnection connection = null)
        {
            try
            {
                //Check to see if it is an insert or update
                var doesBgExist = GetBudgetGroupById(budgetGroup.BudgetGroupId, connection, false);
                if (doesBgExist == null)
                {
                    CreateBudgetGroup(budgetGroup, connection);
                }
                else
                {
                    UpdateCompleteBudgetGroup(budgetGroup);
                }

                var updatedBudgetGroup = GetBudgetGroupById(budgetGroup.BudgetGroupId);
                return updatedBudgetGroup;
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public int DeleteById(Guid budgetGroupId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    var sql = @"DELETE FROM Live.BudgetGroup WHERE BudgetGroupId = @BudgetGroupId;";

                    int rowsChanged = db.Execute(sql, new { BudgetGroupId = budgetGroupId });

                    if (rowsChanged == 0)
                    {
                        throw new Exception($"BudgetGroup DELETE failed for BudgetGroupId = {budgetGroupId}.");
                    }

                    return rowsChanged;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public int DeleteChildrenByBudgetGroupId(Guid budgetGroupId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    var sql = @"DELETE FROM Live.BudgetGroupXrefAd WHERE BudgetGroupId = @BudgetGroupId;
                                DELETE FROM Live.BudgetGroupXrefLocation WHERE BudgetGroupId = @BudgetGroupId;
                                DELETE FROM Live.BudgetGroupTimedBudget WHERE BudgetGroupId = @BudgetGroupId;
                                DELETE FROM Live.BudgetGroupXrefConversionAction WHERE BudgetGroupId = @BudgetGroupId;
                                DELETE FROM Live.BudgetGroupXrefPlacement WHERE BudgetGroupId = @BudgetGroupId;";

                    int rowsChanged = db.Execute(sql, new { BudgetGroupId = budgetGroupId }, commandTimeout: COMMAND_TIMEOUT);

                    if (rowsChanged == 0)
                    {
                        throw new Exception($"Live.BudgetGroup childrent DELETE failed for BudgetGroupId = {budgetGroupId}.");
                    }

                    return rowsChanged;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public BudgetGroup UpdateBudgetGroupInsertChildren(BudgetGroup budgetGroup, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db =  connection ?? new SqlConnection(ConnString))
                {
                    using (var transaction = new TransactionScope())
                    {
                        UpdateBudgetGroup(budgetGroup, db);

                        InsertIntoBudgetGroupXrefAd(budgetGroup.BudgetGroupXrefAds, db);
                        InsertIntoBudgetGroupXrefLocation(budgetGroup.BudgetGroupXrefLocations, db);
                        InsertIntoBudgetGroupXrefConversionAction(budgetGroup.BudgetGroupXrefConversionActions, db);
                        InsertIntoBudgetGroupXrefPlacement(budgetGroup.BudgetGroupXrefPlacements, db);
                        InsertIntoBudgetGroupTimedBudget(budgetGroup.BudgetGroupTimedBudgets, db);

                        transaction.Complete();
                    }
                    return GetBudgetGroupById(budgetGroup.BudgetGroupId, db);
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }



        #region Budget Group Helpers

        internal int InsertIntoBudgetGroup(BudgetGroup budgetGroup, IDbConnection db)
        {
            var rowsChanged = 0;

            var sql = @"INSERT INTO Live.BudgetGroup 
                        (BudgetGroupId, BudgetGroupStatusId, BudgetGroupName, CreatedBy, Created, ModifiedBy, Modified) VALUES 
                        (@BudgetGroupId, @BudgetGroupStatusId, @BudgetGroupName, @CreatedBy, @Created, @ModifiedBy, @Modified);";

            rowsChanged = db.Execute(sql, budgetGroup, commandTimeout: COMMAND_TIMEOUT);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroup INSERT Failed.");
            }

            return rowsChanged;
        }


        internal int UpdateBudgetGroup(BudgetGroup budgetGroup, IDbConnection db)
        {
            int rowsChanged = 0;
            var sql = @"UPDATE Live.BudgetGroup SET
                            BudgetGroupStatusId = @BudgetGroupStatusId
                           ,BudgetGroupName = @BudgetGroupName
                           ,ModifiedBy = @ModifiedBy
                           ,Modified = @Modified
                        WHERE BudgetGroupId = @BudgetGroupId;";

            rowsChanged = db.Execute(sql, budgetGroup);

            if (rowsChanged == 0)
            {
                throw new Exception($"BudgetGroup UPDATE failed for BudgetGroupId = {budgetGroup.BudgetGroupId}.");
            }

            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupXrefAd(List<BudgetGroupXrefAd> budgetGroupXrefAds, IDbConnection db)
        {
            var rowsChanged = 0;
            if (budgetGroupXrefAds == null || budgetGroupXrefAds.Count == 0)
            {
                return rowsChanged;
            }

            var sql = @"INSERT INTO Live.BudgetGroupXrefAd 
                        (BudgetGroupXrefAdId, BudgetGroupId, AdId, CreatedBy, Created) VALUES 
                        (@BudgetGroupXrefAdId, @BudgetGroupId, @AdId, @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefAds, commandTimeout: COMMAND_TIMEOUT);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefAd INSERT Failed.");
            }
            return rowsChanged;
        }

        internal int UpdateBudgetGroupXrefAd(List<BudgetGroupXrefAd> budgetGroupXrefAds, IDbConnection db)
        {
            var rowsChanged = 0;
            if (budgetGroupXrefAds == null || budgetGroupXrefAds.Count == 0)
            {
                return rowsChanged;
            }

            var sql = @"UPDATE Live.BudgetGroupXrefAd SET 
                        AdId = @AdId, 
                        ModifiedBy = @ModifiedBy, 
                        Modified = @Modified
                        WHERE  BudgetGroupXrefAdId = @BudgetGroupXrefAdId;";

            rowsChanged = db.Execute(sql, budgetGroupXrefAds);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefAd UPDATE Failed.");
            }
            return rowsChanged;

        }

        internal int MergeBudgetGroupXrefAds(List<BudgetGroupXrefAd> budgetGroupXrefAds, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefAds == null || budgetGroupXrefAds.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"MERGE Live.BudgetGroupXrefAd AS [target]
                            USING (SELECT @BudgetGroupXrefAdId AS BudgetGroupXrefAdId, @BudgetGroupId AS BudgetGroupId, @AdId AS AdId, @CreatedBy AS CreatedBy, @Created AS Created, @ModifiedBy AS ModifiedBy, @Modified AS Modified) AS [source]
                                ON target.BudgetGroupXrefAdId = source.BudgetGroupXrefAdId
                            WHEN MATCHED THEN
                                UPDATE SET AdId = @AdId, ModifiedBy = @ModifiedBy, Modified = @Modified 
                            WHEN NOT MATCHED THEN
                                INSERT (BudgetGroupXrefAdId, BudgetGroupId, AdId, Paused, Deleted, CreatedBy, Created, ModifiedBy, Modified) VALUES (@BudgetGroupXrefAdId, @BudgetGroupId, @AdId, Cast(0 as bit), Cast(0 as bit), @CreatedBy, @Created,  @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefAds);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefAd MERGE failed");
            }
            return rowsChanged;
        }

        internal int DeleteBudgetGroupXrefAds(List<Guid> budgetGroupXrefAds, IDbConnection db)
        {
            var rowsChanged = 0;
            if (budgetGroupXrefAds.Count == 0)
            {
                return rowsChanged;
            }

            var itemsToDelete = budgetGroupXrefAds.Select(a => new { BudgetGroupXrefAdId = a }).ToList();

            var sql = @"DELETE FROM Live.BudgetGroupXrefAd
                            WHERE BudgetGroupXrefAdId = @BudgetGroupXrefAdId;";

            rowsChanged = db.Execute(sql, itemsToDelete);

            if (rowsChanged == 0)
            {
                throw new Exception("DELETE FROM BudgetGroupXrefAd failed.");
            }
            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupXrefLocation(List<BudgetGroupXrefLocation> budgetGroupXrefLocations, IDbConnection db)
        {
            var rowsChanged = 0;
            if (budgetGroupXrefLocations == null || budgetGroupXrefLocations.Count == 0)
            {
                return rowsChanged;
            }

            var sql = @"INSERT INTO Live.BudgetGroupXrefLocation 
                        (BudgetGroupXrefLocationId, BudgetGroupId, LocationId, CreatedBy, Created) VALUES 
                        (@BudgetGroupXrefLocationId, @BudgetGroupId, @LocationId, @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefLocations, commandTimeout: COMMAND_TIMEOUT);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefLocation INSERT Failed.");
            }
            return rowsChanged;
        }

        internal int UpdateBudgetGroupXrefLocation(List<BudgetGroupXrefLocation> budgetGroupXrefLocations, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefLocations == null || budgetGroupXrefLocations.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"UPDATE Live.BudgetGroupXrefLocation SET
                        LocationId = @LocationId
                       ,ModifiedBy = @ModifiedBy
                       ,Modified = @Modified
                    WHERE BudgetGroupXrefLocationId = @BudgetGroupXrefLocationId;";

            rowsChanged = db.Execute(sql, budgetGroupXrefLocations);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefLocation UPDATE failed");
            }
            return rowsChanged;
        }

        internal int MergeBudgetGroupXrefLocations(List<BudgetGroupXrefLocation> budgetGroupXrefLocations, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefLocations == null || budgetGroupXrefLocations.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"MERGE Live.BudgetGroupXrefLocation AS [target]
                            USING (SELECT @BudgetGroupXrefLocationId AS BudgetGroupXrefLocationId, @BudgetGroupId AS BudgetGroupId, @LocationId AS LocationId, @CreatedBy AS CreatedBy, @Created AS Created, @ModifiedBy AS ModifiedBy, @Modified AS Modified) AS [source]
                                ON target.BudgetGroupXrefLocationId = source.BudgetGroupXrefLocationId
                            WHEN MATCHED THEN
                                UPDATE SET LocationId = @LocationId, ModifiedBy = @ModifiedBy, Modified = @Modified 
                            WHEN NOT MATCHED THEN
                                INSERT (BudgetGroupXrefLocationId, BudgetGroupId, LocationId, Deleted, CreatedBy, Created) VALUES (@BudgetGroupXrefLocationId, @BudgetGroupId, @LocationId, CAST(0 as bit),  @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefLocations);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefLocation MERGE failed");
            }
            return rowsChanged;
        }

        internal int DeleteBudgetGroupXrefLocations(List<Guid> budgetGroupXrefLocations, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefLocations == null || budgetGroupXrefLocations.Count == 0)
            {
                return rowsChanged;
            }

            var itemsToDelete = budgetGroupXrefLocations.Select(l => new { BudgetGroupXrefLocationId = l }).ToList();

            string sql = @"DELETE FROM Live.BudgetGroupXrefLocation
                            WHERE BudgetGroupXrefLocationId = @BudgetGroupXrefLocationId;";

            rowsChanged = db.Execute(sql, itemsToDelete);

            if (rowsChanged == 0)
            {
                throw new Exception("DELETE FROM BudgetGroupXrefLocation failed.");
            }
            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupTimedBudget(List<BudgetGroupTimedBudget> budgetGroupTimedBudget, IDbConnection db)
        {
            var rowsChanged = 0;
            if (budgetGroupTimedBudget == null)
            {
                return rowsChanged;
            }

            var sql = @"INSERT INTO Live.BudgetGroupTimedBudget 
                        (BudgetGroupTimedBudgetId, BudgetGroupId, BudgetAmount, Margin, StartDate, EndDate, CreatedBy, Created, BudgetGroupXrefPlacementId) VALUES 
                        (@BudgetGroupTimedBudgetId, @BudgetGroupId, @BudgetAmount, @Margin, @StartDate, @EndDate, @CreatedBy, @Created, @BudgetGroupXrefPlacementId);";

            rowsChanged = db.Execute(sql, budgetGroupTimedBudget, commandTimeout: COMMAND_TIMEOUT);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupTimedBudget INSERT Failed.");
            }
            return rowsChanged;
        }

        internal int UpdateBudgetGroupTimedBudget(List<BudgetGroupTimedBudget> budgetGroupTimedBudget, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupTimedBudget == null)
            {
                return rowsChanged;
            }

            string sql = @"UPDATE Live.BudgetGroupTimedBudget SET
                        BudgetAmount = @BudgetAmount
                       ,StartDate = @StartDate
                       ,EndDate = @EndDate
                       ,Margin = @Margin
                       ,BudgetGroupXrefPlacementId = @BudgetGroupXrefPlacementId
                    WHERE BudgetGroupTimedBudget = @BudgetGroupTimedBudget;";

            rowsChanged = db.Execute(sql, budgetGroupTimedBudget);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupTimedBudget UPDATE failed");
            }
            return rowsChanged;
        }

        internal int MergeBudgetGroupTimedBudget(List<BudgetGroupTimedBudget> budgetGroupTimedBudget, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupTimedBudget == null)
            {
                return rowsChanged;
            }

            string sql = @"MERGE Live.BudgetGroupTimedBudget AS [target]
                            USING (SELECT 
                                @BudgetGroupTimedBudgetId AS BudgetGroupTimedBudgetId
                               ,@BudgetGroupId AS BudgetGroupId
                               ,@BudgetAmount AS BudgetAmount
                               ,@StartDate AS StartDate
                               ,@EndDate AS EndDate
                               ,@Margin AS Margin
                               ,@CreatedBy AS CreatedBy
                               ,@Created AS Created
                               ,@BudgetGroupXrefPlacementId
                               ,target.BudgetGroupTimedBudgetId = source.BudgetGroupTimedBudgetId) AS [source]
                                ON 
                            WHEN MATCHED THEN
                                UPDATE SET BudgetAmount = @BudgetAmount, StartDate = @StartDate, EndDate = @EndDate, Margin = @Margin, BudgetGroupXrefPlacementId = @BudgetGroupXrefPlacementId 
                            WHEN NOT MATCHED THEN
                                INSERT (BudgetGroupTimedBudgetId, BudgetGroupId, BudgetAmount, StartDate, EndDate, CreatedBy, Created, Margin, BudgetGroupXrefPlacementId) VALUES 
                                       (@BudgetGroupTimedBudgetId, @BudgetGroupId, @BudgetAmount, @StartDate, @EndDate, @CreatedBy, @Created, @Margin, @BudgetGroupXrefPlacementId);";

            rowsChanged = db.Execute(sql, budgetGroupTimedBudget);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupTimedBudget MERGE failed");
            }
            return rowsChanged;
        }

        internal int DeleteBudgetGroupTimedBudget(List<Guid> timedBudgetIds, IDbConnection db)
        {
            int rowsChanged = 0;
            if (timedBudgetIds.Count == 0)
            {
                return rowsChanged;
            }

            var itemsToDelete = timedBudgetIds.Select(tb => new { BudgetGroupTimedBudgetId = tb }).ToList();

            string sql = @"DELETE FROM Live.BudgetGroupTimedBudget
                            WHERE BudgetGroupTimedBudgetId = @BudgetGroupTimedBudgetId;";

            rowsChanged = db.Execute(sql, itemsToDelete);

            if (rowsChanged == 0)
            {
                throw new Exception("DELETE FROM BudgetGroupTimedBudget failed.");
            }
            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupXrefConversionAction(List<BudgetGroupXrefConversionAction> budgetGroupXrefConversionActions, IDbConnection db)
        {
            var rowsChanged = 0;
            if (budgetGroupXrefConversionActions == null || budgetGroupXrefConversionActions.Count == 0)
            {
                return rowsChanged;
            }

            var sql = @"INSERT INTO Live.BudgetGroupXrefConversionAction 
                        (BudgetGroupXrefConversionActionId, BudgetGroupId, ConversionActionId, CreatedBy, Created) VALUES 
                        (@BudgetGroupXrefConversionActionId, @BudgetGroupId, @ConversionActionId, @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefConversionActions, commandTimeout: COMMAND_TIMEOUT);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefConversionAction INSERT Failed.");
            }
            return rowsChanged;
        }

        internal int UpdateBudgetGroupXrefConversionAction(List<BudgetGroupXrefConversionAction> budgetGroupXrefConversionActions, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefConversionActions == null || budgetGroupXrefConversionActions.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"UPDATE Live.BudgetGroupXrefConversionAction SET
                        ConversionActionId = @ConversionActionId
                       ,ModifiedBy = @ModifiedBy
                       ,Modified = @Modified
                    WHERE BudgetGroupXrefConversionActionId = @BudgetGroupXrefConversionActionId;";

            rowsChanged = db.Execute(sql, budgetGroupXrefConversionActions);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefConversionAction UPDATE failed");
            }
            return rowsChanged;
        }

        internal int MergeBudgetGroupXrefConversionActions(List<BudgetGroupXrefConversionAction> budgetGroupXrefConversionActions, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefConversionActions == null || budgetGroupXrefConversionActions.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"MERGE Live.BudgetGroupXrefConversionAction AS [target]
                            USING (SELECT @BudgetGroupXrefConversionActionId AS BudgetGroupXrefConversionActionId, @BudgetGroupId AS BudgetGroupId, @ConversionActionId AS ConversionActionId, @CreatedBy AS CreatedBy, @Created AS Created, @ModifiedBy AS ModifiedBy, @Modified AS Modified) AS [source]
                                ON target.BudgetGroupXrefConversionActionId = source.BudgetGroupXrefConversionActionId
                            WHEN MATCHED THEN
                                UPDATE SET ConversionActionId = @ConversionActionId, ModifiedBy = @ModifiedBy, Modified = @Modified 
                            WHEN NOT MATCHED THEN
                                INSERT (BudgetGroupXrefConversionActionId, BudgetGroupId, ConversionActionId, CreatedBy, Created) VALUES 
                                       (@BudgetGroupXrefConversionActionId, @BudgetGroupId, @ConversionActionId, @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefConversionActions);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefConversionAction MERGE failed");
            }
            return rowsChanged;
        }

        internal int DeleteBudgetGroupXrefConversionActions(List<Guid> budgetGroupXrefConversionActionIds, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefConversionActionIds == null || budgetGroupXrefConversionActionIds.Count == 0)
            {
                return rowsChanged;
            }

            var itemsToDelete = budgetGroupXrefConversionActionIds.Select(ca => new { BudgetGroupXrefConversionActionId = ca }).ToList();

            string sql = @"DELETE FROM Live.BudgetGroupXrefConversionAction
                            WHERE BudgetGroupXrefConversionActionId = @BudgetGroupXrefConversionActionId;";

            rowsChanged = db.Execute(sql, itemsToDelete);

            if (rowsChanged == 0)
            {
                throw new Exception("DELETE FROM BudgetGroupXrefConversionAction failed.");
            }
            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupXrefPlacement(List<BudgetGroupXrefPlacement> budgetGroupXrefPlacements, IDbConnection db)
        {
            var rowsChanged = 0;
            if (budgetGroupXrefPlacements == null || budgetGroupXrefPlacements.Count == 0)
            {
                return rowsChanged;
            }

            var sql = @"INSERT INTO Live.BudgetGroupXrefPlacement 
                        (BudgetGroupXrefPlacementId, BudgetGroupId, PlacementId, CreatedBy, Created) VALUES 
                        (@BudgetGroupXrefPlacementId, @BudgetGroupId, @PlacementId, @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefPlacements, commandTimeout: COMMAND_TIMEOUT);

            if (rowsChanged == 0)
            {
                throw new Exception("INSERT INTO Live.BudgetGroupXrefPlacement Failed.");
            }
            return rowsChanged;
        }

        internal int UpdateBudgetGroupXrefPlacement(List<BudgetGroupXrefPlacement> budgetGroupXrefPlacements, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefPlacements == null || budgetGroupXrefPlacements.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"UPDATE Live.BudgetGroupXrefPlacement SET
                        PlacementId = @PlacementId
                       ,ModifiedBy = @ModifiedBy
                       ,Modified = @Modified
                    WHERE BudgetGroupXrefPlacementId = @BudgetGroupXrefPlacementId;";

            rowsChanged = db.Execute(sql, budgetGroupXrefPlacements);

            if (rowsChanged == 0)
            {
                throw new Exception("UpdateBudgetGroupXrefPlacement UPDATE failed");
            }
            return rowsChanged;
        }

        internal int MergeBudgetGroupXrefPlacements(List<BudgetGroupXrefPlacement> budgetGroupXrefPlacements, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefPlacements == null || budgetGroupXrefPlacements.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"MERGE Live.BudgetGroupXrefPlacement AS [target]
                            USING (SELECT @BudgetGroupXrefPlacementId AS BudgetGroupXrefPlacementId, @BudgetGroupId AS BudgetGroupId, @PlacementId AS PlacementId, @AssignedBy AS AssignedBy, @AssignedDate AS AssignedDate) AS [source]
                                ON target.BudgetGroupXrefPlacementId = source.BudgetGroupXrefPlacementId
                            WHEN MATCHED THEN
                                UPDATE SET PlacementId = @PlacementId, AssignedBy = @AssignedBy, AssignedDate = @AssignedDate 
                            WHEN NOT MATCHED THEN
                                INSERT (BudgetGroupXrefPlacementId, BudgetGroupId, PlacementId, AssignedBy, AssignedDate) VALUES 
                                       (@BudgetGroupXrefPlacementId, @BudgetGroupId, @PlacementId, @AssignedBy, @AssignedDate);";

            rowsChanged = db.Execute(sql, budgetGroupXrefPlacements);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefPlacement MERGE failed");
            }
            return rowsChanged;
        }

        internal int DeleteBudgetGroupXrefPlacements(List<Guid> budgetGroupXrefPlacementIds, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefPlacementIds == null || budgetGroupXrefPlacementIds.Count == 0)
            {
                return rowsChanged;
            }

            var itemsToDelete = budgetGroupXrefPlacementIds.Select(p => new { BudgetGroupXrefPlacementId = p }).ToList();

            string sql = @"DELETE FROM Live.BudgetGroupXrefPlacement
                            WHERE BudgetGroupXrefPlacementId = @BudgetGroupXrefPlacementId;";

            rowsChanged = db.Execute(sql, itemsToDelete);

            if (rowsChanged == 0)
            {
                throw new Exception("DELETE FROM BudgetGroupXrefPlacement failed.");
            }
            return rowsChanged;
        }
        #endregion
    }
}
