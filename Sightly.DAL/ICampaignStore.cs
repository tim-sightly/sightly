﻿using System.Collections.Generic;
using System.Data;
using Sightly.DAL.DTO;

namespace Sightly.DAL
{
    public interface ICampaignStore
    {
        List<Campaign> GetAllCampaigns(IDbConnection connection = null);
        List<Campaign> GetAllMediaAgencyCampaigns(IDbConnection connection = null);
        List<Campaign> GetAllCampaignsForAdWordsCustomerId(long adWordsCustomerId, IDbConnection connection = null);
    }
}
