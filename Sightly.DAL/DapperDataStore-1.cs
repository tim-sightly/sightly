﻿using Sightly.DAL.DTO.BudgetGroupStates;
using Sightly.DAL.DTO.BudgetGroupTimedBudgetStates;
using Sightly.DAL.DTO.BudgetGroupXrefAdStates;
using Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates;
using Sightly.DAL.DTO.BudgetGroupXrefLocationStates;
using Sightly.DAL.DTO.BudgetGroupXrefPlacementStates;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Transactions;

namespace Sightly.DAL
{
    public class DapperDataStore1: ADataStore, IDataStore
    {
        public DapperDataStore()
            :base()
        {
        }

        #region Budget Group

        public int CreateBudgetGroup(BudgetGroup budgetGroup)
        {
            int rowsAffected = 0;
            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                using (var transaction = new TransactionScope())
                {
                    rowsAffected += InsertIntoBudgetGroup(budgetGroup, db);
                    rowsAffected += InsertIntoBudgetGroupXrefAd(budgetGroup.BudgetGroupXrefAds, db);
                    rowsAffected += InsertIntoBudgetGroupTimedBudget(budgetGroup.BudgetGroupTimedBudget, db);
                    rowsAffected += InsertIntoBudgetGroupXrefConversionAction(budgetGroup.BudgetGroupXrefConversionActions, db);
                    rowsAffected += InsertIntoBudgetGroupXrefLocation(budgetGroup.BudgetGroupXrefLocations, db);
                    rowsAffected += InsertIntoBudgetGroupXrefPlacement(budgetGroup.BudgetGroupXrefPlacements, db);

                    transaction.Complete();
                }
            }
            catch (Exception ex)
            {
                //TODO: Log this!!!
                Console.WriteLine(ex.Message);
                throw;
            }
            return rowsAffected;
        }

        public BudgetGroup GetBudgetGroupById(Guid budgetGroupId)
        {
            string q1 = @"SELECT * FROM Orders.BudgetGroup WHERE BudgetGroupId = @BudgetGroupId;";
            string q2 = @"SELECT * FROM Orders.BudgetGroupXrefAd WHERE BudgetGroupId = @BudgetGroupId;";
            string q3 = @"SELECT * FROM Orders.BudgetGroupXrefLocation WHERE BudgetGroupId = @BudgetGroupId;";
            string q4 = @"SELECT * FROM Orders.BudgetGroupXrefConversionAction WHERE BudgetGroupId = @BudgetGroupId;";
            string q5 = @"SELECT * FROM Orders.BudgetGroupXrefPlacement WHERE BudgetGroupId = @BudgetGroupId;";
            string q6 = @"SELECT * FROM Orders.BudgetGroupTimedBudget WHERE BudgetGroupId = @BudgetGroupId;";
            string sql = string.Format("{0}{1}{2}{3}{4}{5}", q1, q2, q3, q4, q5, q6);

            BudgetGroup budgetGroup = null;
            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                using (var multi = db.QueryMultiple(sql, new { BudgetGroupId = budgetGroupId }))
                {
                    budgetGroup = multi.Read<BudgetGroup>().SingleOrDefault();
                    if (budgetGroup != null)
                    {
                        budgetGroup.BudgetGroupXrefAds = multi.Read<BudgetGroupXrefAd>().ToList<BudgetGroupXrefAd>();
                        budgetGroup.BudgetGroupXrefLocations = multi.Read<BudgetGroupXrefLocation>().ToList<BudgetGroupXrefLocation>();
                        budgetGroup.BudgetGroupXrefConversionActions = multi.Read<BudgetGroupXrefConversionAction>().ToList<BudgetGroupXrefConversionAction>();
                        budgetGroup.BudgetGroupXrefPlacements = multi.Read<BudgetGroupXrefPlacement>().ToList<BudgetGroupXrefPlacement>();
                        budgetGroup.BudgetGroupTimedBudget = multi.Read<BudgetGroupTimedBudget>().FirstOrDefault();
                    }
                    return budgetGroup;
                }
            }
            catch (Exception ex)
            {
                //TODO: Log This!!!
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        #region Budget Group Helpers

        private int InsertIntoBudgetGroup(BudgetGroup budgetGroup, IDbConnection db)
        {
            int rowsChanged = 0;

            budgetGroup.CreatedDatetime = budgetGroup.CreatedDatetime ?? DateTime.Now;
            budgetGroup.LastModifiedDatetime = budgetGroup.LastModifiedDatetime ?? DateTime.Now;

            string sql = @"INSERT INTO Orders.BudgetGroup 
                        (BudgetGroupId, BudgetGroupBudget, BudgetGroupName, Deleted, CreatedBy, CreatedDatetime, LastModifiedBy, LastModifiedDatetime) VALUES 
                        (@BudgetGroupId, @BudgetGroupBudget, @BudgetGroupName, @Deleted, @CreatedBy, @CreatedDatetime, @LastModifiedBy, @LastModifiedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroup);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroup INSERT Failed.");
            }

            return rowsChanged;
        }

        private int InsertIntoBudgetGroupXrefAd(List<BudgetGroupXrefAd> budgetGroupXrefAds, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefAds == null || budgetGroupXrefAds.Count == 0)
            {
                return rowsChanged;
            }

            budgetGroupXrefAds.ForEach(a => {
                a.CreatedDatetime = a.CreatedDatetime ?? DateTime.Now;
                a.LastModifiedDatetime = a.LastModifiedDatetime ?? DateTime.Now;
            });

            string sql = @"INSERT INTO Orders.BudgetGroupXrefAd 
                        (BudgetGroupXrefAdId, BudgetGroupId, AdId, Paused, CreatedBy, Deleted, CreatedDatetime, LastModifiedBy, LastModifiedDatetime) VALUES 
                        (@BudgetGroupXrefAdId, @BudgetGroupId, @AdId, @Paused, @CreatedBy, @Deleted, @CreatedDatetime, @LastModifiedBy, @LastModifiedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupXrefAds);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefAd INSERT Failed.");
            }
            return rowsChanged;
        }

        private int InsertIntoBudgetGroupTimedBudget(BudgetGroupTimedBudget budgetGroupTimedBudget, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupTimedBudget == null)
            {
                return rowsChanged;
            }

            budgetGroupTimedBudget.CreatedDatetime = budgetGroupTimedBudget.CreatedDatetime ?? DateTime.Now;

            string sql = @"INSERT INTO Orders.BudgetGroupTimedBudget 
                        (BudgetGroupTimedBudgetId, BudgetGroupId, BudgetAmount, Margin, StartDate, EndDate, CreatedBy, CreatedDatetime) VALUES 
                        (@BudgetGroupTimedBudgetId, @BudgetGroupId, @BudgetAmount, @Margin, @StartDate, @EndDate, @CreatedBy, @CreatedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupTimedBudget);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupTimedBudget INSERT Failed.");
            }
            return rowsChanged;
        }

        private int InsertIntoBudgetGroupXrefConversionAction(List<BudgetGroupXrefConversionAction> budgetGroupXrefConversionActions, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefConversionActions == null || budgetGroupXrefConversionActions.Count == 0)
            {
                return rowsChanged;
            }

            budgetGroupXrefConversionActions.ForEach(ca => {
                ca.CreatedDatetime = ca.CreatedDatetime ?? DateTime.Now;
                ca.LastModifiedDatetime = ca.LastModifiedDatetime ?? DateTime.Now;
            });

            string sql = @"INSERT INTO Orders.BudgetGroupXrefConversionAction 
                        (BudgetGroupXrefConversionActionId, BudgetGroupId, ConversionActionId, SequenceId, CreatedBy, CreatedDatetime, LastModifiedBy, LastModifiedDatetime) VALUES 
                        (@BudgetGroupXrefConversionActionId, @BudgetGroupId, @ConversionActionId, @SequenceId, @CreatedBy, @CreatedDatetime, @LastModifiedBy, @LastModifiedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupXrefConversionActions);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefConversionAction INSERT Failed.");
            }
            return rowsChanged;
        }

        private int InsertIntoBudgetGroupXrefLocation(List<BudgetGroupXrefLocation> budgetGroupXrefLocations, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefLocations == null || budgetGroupXrefLocations.Count == 0)
            {
                return rowsChanged;
            }

            budgetGroupXrefLocations.ForEach(l => {
                l.CreatedDatetime = l.CreatedDatetime == null ? DateTime.Now : l.CreatedDatetime;
                l.LastModifiedDatetime = l.LastModifiedDatetime == null ? DateTime.Now : l.LastModifiedDatetime;
            });

            string sql = @"INSERT INTO Orders.BudgetGroupXrefLocation 
                        (BudgetGroupXrefLocationId, BudgetGroupId, Deleted, LocationId, SequenceId, CreatedBy, CreatedDatetime, LastModifiedBy, LastModifiedDatetime) VALUES 
                        (@BudgetGroupXrefLocationId, @BudgetGroupId, @Deleted, @LocationId, @SequenceId, @CreatedBy, @CreatedDatetime, @LastModifiedBy, @LastModifiedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupXrefLocations);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefLocation INSERT Failed.");
            }
            return rowsChanged;
        }

        private int InsertIntoBudgetGroupXrefPlacement(List<BudgetGroupXrefPlacement> budgetGroupXrefPlacements, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefPlacements == null || budgetGroupXrefPlacements.Count == 0)
            {
                return rowsChanged;
            }

            budgetGroupXrefPlacements.ForEach(p => {
                p.CreatedDatetime = p.CreatedDatetime == null ? DateTime.Now : p.CreatedDatetime;
                p.LastModifiedDatetime = p.LastModifiedDatetime == null ? DateTime.Now : p.LastModifiedDatetime;
            });

            string sql = @"INSERT INTO Orders.BudgetGroupXrefPlacement 
                        (BudgetGroupXrefPlacementId, BudgetGroupId, PlacementId, CreatedBy, CreatedDatetime) VALUES 
                        (@BudgetGroupXrefPlacementId, @BudgetGroupId, @PlacementId, @CreatedBy, @CreatedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupXrefPlacements);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefPlacement INSERT Failed.");
            }
            return rowsChanged;
        }

        #endregion

        #endregion

        #region Budget Group Proposal

        public int CreateBudgetGroupProposal(BudgetGroupProposal budgetGroupProposal)
        {
            int rowsAffected = 0;
            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                using (var transaction = new TransactionScope())
                {
                    rowsAffected += InsertIntoBudgetGroupProposal(budgetGroupProposal, db);
                    rowsAffected += InsertIntoBudgetGroupXrefAdProposals(budgetGroupProposal.BudgetGroupXrefAdProposals, db);
                    rowsAffected += InsertIntoBudgetGroupTimedBudgetProposal(budgetGroupProposal.BudgetGroupTimedBudgetProposal, db);
                    rowsAffected += InsertIntoBudgetGroupXrefConversionActionProposal(budgetGroupProposal.BudgetGroupXrefConversionActionProposals, db);
                    rowsAffected += InsertIntoBudgetGroupXrefLocationProposal(budgetGroupProposal.BudgetGroupXrefLocationProposals, db);
                    rowsAffected += InsertIntoBudgetGroupXrefPlacementProposal(budgetGroupProposal.BudgetGroupXrefPlacementProposals, db);

                    transaction.Complete();
                }
            }
            catch (Exception ex)
            {
                //TODO: Log this!!!
                Console.WriteLine(ex.Message);
                throw;
            }
            return rowsAffected;
        }

        public BudgetGroupProposal GetBudgetGroupProposalById(Guid budgetGroupProposalId)
        {
            string q1 = @"SELECT * FROM Orders.BudgetGroupProposal WHERE BudgetGroupProposalId = @BudgetGroupProposalId;";
            string q2 = @"SELECT * FROM Orders.BudgetGroupXrefAdProposal WHERE BudgetGroupProposalId = @BudgetGroupProposalId;";
            string q3 = @"SELECT * FROM Orders.BudgetGroupXrefLocationProposal WHERE BudgetGroupProposalId = @BudgetGroupProposalId;";
            string q4 = @"SELECT * FROM Orders.BudgetGroupXrefConversionActionProposal WHERE BudgetGroupProposalId = @BudgetGroupProposalId;";
            string q5 = @"SELECT * FROM Orders.BudgetGroupXrefPlacementProposal WHERE BudgetGroupProposalId = @BudgetGroupProposalId;";
            string q6 = @"SELECT * FROM Orders.BudgetGroupTimedBudgetProposal WHERE BudgetGroupProposalId = @BudgetGroupProposalId;";
            string sql = string.Format("{0}{1}{2}{3}{4}{5}", q1, q2, q3, q4, q5, q6);

            BudgetGroupProposal budgetGroupProposal = null;
            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                using (var multi = db.QueryMultiple(sql, new { BudgetGroupProposalId = budgetGroupProposalId }))
                {
                    budgetGroupProposal = multi.Read<BudgetGroupProposal>().SingleOrDefault();
                    if (budgetGroupProposal != null)
                    {
                        budgetGroupProposal.BudgetGroupXrefAdProposals = multi.Read<BudgetGroupXrefAdProposal>().ToList<BudgetGroupXrefAdProposal>();
                        budgetGroupProposal.BudgetGroupXrefLocationProposals = multi.Read<BudgetGroupXrefLocationProposal>().ToList<BudgetGroupXrefLocationProposal>();
                        budgetGroupProposal.BudgetGroupXrefConversionActionProposals = multi.Read<BudgetGroupXrefConversionActionProposal>().ToList<BudgetGroupXrefConversionActionProposal>();
                        budgetGroupProposal.BudgetGroupXrefPlacementProposals = multi.Read<BudgetGroupXrefPlacementProposal>().ToList<BudgetGroupXrefPlacementProposal>();
                        budgetGroupProposal.BudgetGroupTimedBudgetProposal = multi.Read<BudgetGroupTimedBudgetProposal>().FirstOrDefault();
                    }
                    return budgetGroupProposal;
                }
            }
            catch (Exception ex)
            {
                //TODO: Log This!!!
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public int UpdateBudgetGroupProposal(BudgetGroupProposal budgetGroupProposal)
        {
            int rowsAffected = 0;
            if (budgetGroupProposal == null)
                return rowsAffected;

            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                using (var transaction = new TransactionScope())
                {
                    rowsAffected += UpdateBudgetGroupProposalHelper(budgetGroupProposal, db);
                    transaction.Complete();
                    return rowsAffected;
                }
            }
            catch (Exception ex)
            {
                //TODO: Log This!!!
                Console.WriteLine(ex.Message);
                throw;
            }
        }



        #region Budg Group Proposal Helpers

        private int InsertIntoBudgetGroupProposal(BudgetGroupProposal budgetGroupProposal, IDbConnection db)
        {
            int rowsChanged = 0;
            string sql = @"INSERT INTO Orders.BudgetGroupProposal 
                        (BudgetGroupProposalId, BudgetGroupId, BudgetGroupName, BudgetGroupBudget, CreatedBy, LastModifiedBy) VALUES 
                        (@BudgetGroupProposalId, @BudgetGroupId, @BudgetGroupName, @BudgetGroupBudget, @CreatedBy, @LastModifiedBy);";

            budgetGroupProposal.CreatedDatetime = budgetGroupProposal.CreatedDatetime ?? DateTime.Now;
            budgetGroupProposal.LastModifiedDateTime = budgetGroupProposal.LastModifiedDateTime ?? DateTime.Now;

            rowsChanged = db.Execute(sql, budgetGroupProposal);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupProposal INSERT Failed.");
            }

            return rowsChanged;
        }

        private int InsertIntoBudgetGroupXrefAdProposals(List<BudgetGroupXrefAdProposal> budgetGroupXrefAdProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefAdProposals == null || budgetGroupXrefAdProposals.Count == 0)
            {
                return rowsChanged;
            }

            budgetGroupXrefAdProposals.ForEach(a => {
                a.CreatedDatetime = a.CreatedDatetime ?? DateTime.Now;
                a.LastModifiedDatetime = a.LastModifiedDatetime ?? DateTime.Now;
            });

            string sql = @"INSERT INTO Orders.BudgetGroupXrefAdProposal 
                        (BudgetGroupXrefAdProposalId, BudgetGroupProposalId, AdId, CreatedBy, CreatedDatetime, LastModifiedBy, LastModifiedDatetime) VALUES 
                        (@BudgetGroupXrefAdProposalId, @BudgetGroupProposalId, @AdId, @CreatedBy, @CreatedDatetime, @LastModifiedBy, @LastModifiedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupXrefAdProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefAdProposal INSERT Failed.");
            }
            return rowsChanged;
        }

        private int InsertIntoBudgetGroupTimedBudgetProposal(BudgetGroupTimedBudgetProposal budgetGroupTimedBudgetProposal, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupTimedBudgetProposal == null)
            {
                return rowsChanged;
            }

            budgetGroupTimedBudgetProposal.CreatedDatetime = budgetGroupTimedBudgetProposal.CreatedDatetime ?? DateTime.Now;

            string sql = @"INSERT INTO Orders.BudgetGroupTimedBudgetProposal 
                        (BudgetGroupTimedBudgetProposalId, BudgetGroupProposalId, BudgetAmount, Margin, StartDate, EndDate, CreatedBy) VALUES 
                        (@BudgetGroupTimedBudgetProposalId, @BudgetGroupProposalId, @BudgetAmount, @Margin, @StartDate, @EndDate, @CreatedBy);";

            rowsChanged = db.Execute(sql, budgetGroupTimedBudgetProposal);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupTimedBudgetProposal INSERT Failed.");
            }
            return rowsChanged;
        }

        private int InsertIntoBudgetGroupXrefConversionActionProposal(List<BudgetGroupXrefConversionActionProposal> budgetGroupXrefConversionActionProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefConversionActionProposals == null || budgetGroupXrefConversionActionProposals.Count == 0)
            {
                return rowsChanged;
            }

            budgetGroupXrefConversionActionProposals.ForEach(ca => {
                ca.CreatedDatetime = ca.CreatedDatetime ?? DateTime.Now;
                ca.LastModifiedDatetime = ca.LastModifiedDatetime ?? DateTime.Now;
            });

            string sql = @"INSERT INTO Orders.BudgetGroupXrefConversionActionProposal 
                        (BudgetGroupXrefConversionActionProposalId, BudgetGroupProposalId, ConversionActionId, CreatedBy, CreatedDatetime, LastModifiedBy, LastModifiedDatetime) VALUES 
                        (@BudgetGroupXrefConversionActionProposalId, @BudgetGroupProposalId, @ConversionActionId, @CreatedBy, @CreatedDatetime, @LastModifiedBy, @LastModifiedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupXrefConversionActionProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefConversionActionProposal INSERT Failed.");
            }
            return rowsChanged;
        }

        private int InsertIntoBudgetGroupXrefLocationProposal(List<BudgetGroupXrefLocationProposal> budgetGroupXrefLocationProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefLocationProposals == null || budgetGroupXrefLocationProposals.Count == 0)
            {
                return rowsChanged;
            }

            budgetGroupXrefLocationProposals.ForEach(p => {
                p.CreatedDatetime = p.CreatedDatetime ?? DateTime.Now;
                p.LastModifiedDatetime = p.LastModifiedDatetime ?? DateTime.Now;
            });

            string sql = @"INSERT INTO Orders.BudgetGroupXrefLocationProposal 
                        (BudgetGroupXrefLocationProposalId, BudgetGroupProposalId, LocationId, CreatedBy, CreatedDatetime, LastModifiedBy, LastModifiedDatetime) VALUES 
                        (@BudgetGroupXrefLocationProposalId, @BudgetGroupProposalId, @LocationId, @CreatedBy, @CreatedDatetime, @LastModifiedBy, @LastModifiedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupXrefLocationProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefLocationProposal INSERT Failed.");
            }
            return rowsChanged;
        }

        private int InsertIntoBudgetGroupXrefPlacementProposal(List<BudgetGroupXrefPlacementProposal> budgetGroupXrefPlacementProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefPlacementProposals == null || budgetGroupXrefPlacementProposals.Count == 0)
            {
                return rowsChanged;
            }

            budgetGroupXrefPlacementProposals.ForEach(p => {
                p.CreatedDatetime = p.CreatedDatetime ?? DateTime.Now;
                p.LastModifiedDatetime = p.LastModifiedDatetime ?? DateTime.Now;
            });

            string sql = @"INSERT INTO Orders.BudgetGroupXrefPlacementProposal 
                        (BudgetGroupXrefPlacementProposalId, BudgetGroupProposalId, PlacementId, CreatedBy, CreatedDatetime, LastModifiedBy, LastModifiedDatetime) VALUES 
                        (@BudgetGroupXrefPlacementProposalId, @BudgetGroupProposalId, @PlacementId, @CreatedBy, @CreatedDatetime, @LastModifiedBy, @LastModifiedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupXrefPlacementProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefPlacementProposal INSERT Failed.");
            }
            return rowsChanged;
        }

        private int UpdateBudgetGroupProposalHelper(BudgetGroupProposal budgetGroupProposal, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupProposal == null)
            {
                return rowsChanged;
            }

            budgetGroupProposal.LastModifiedDateTime = budgetGroupProposal.LastModifiedDateTime ?? DateTime.Now;

            string sql = @"UPDATE Orders.BudgetGroupProposal SET 
                           BudgetGroupId = @BudgetGroupId 
                           ,BudgetGroupName = @BudgetGroupname 
                           ,BudgetGroupBudget = @BudgetGroupBudget
                           ,CreatedBy = @CreatedBy
                           ,CreatedDatetime = @CreatedDatetime
                           ,LastModifiedBy = @LastModifiedBy
                           ,LastModifiedDatetime = @LastModifiedDatetime
                           WHERE BudgetGroupProposalId=@BudgetGroupProposalId;";
            rowsChanged = db.Execute(sql, budgetGroupProposal);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupProposal UPDATE Failed.");
            }
            return rowsChanged;
        }

        #endregion

        #endregion

        #region Budget Group History 

        public int CreateBudgetGroupHistory(BudgetGroupHistory budgetGroupHistory)
        {
            int rowsAffected = 0;
            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                using (var transaction = new TransactionScope())
                {
                    rowsAffected += InsertIntoBudgetGroupHistory(budgetGroupHistory, db);
                    rowsAffected += InsertIntoBudgetGroupXrefAdHistory(budgetGroupHistory.BudgetGroupXrefAdHistories, db);
                    rowsAffected += InsertIntoBudgetGroupTimedBudgetHistory(budgetGroupHistory.BudgetGroupTimedBudgetHistory, db);
                    rowsAffected += InsertIntoBudgetGroupXrefConversionActionHistory(budgetGroupHistory.BudgetGroupXrefConversionActionHistories, db);
                    rowsAffected += InsertIntoBudgetGroupXrefLocationHistory(budgetGroupHistory.BudgetGroupXrefLocationHistories, db);
                    rowsAffected += InsertIntoBudgetGroupXrefPlacementHistory(budgetGroupHistory.BudgetGroupXrefPlacementHistories, db);

                    transaction.Complete();
                }
            }
            catch (Exception ex)
            {
                //TODO: Log this!!!
                Console.WriteLine(ex.Message);
                throw;
            }
            return rowsAffected;
        }

        public BudgetGroupHistory GetBudgetGroupHistoryById(Guid budgetGroupHistoryId)
        {
            string q1 = @"SELECT * FROM Orders.BudgetGroupHistory WHERE BudgetGroupHistoryId = @BudgetGroupHistoryId;";
            string q2 = @"SELECT * FROM Orders.BudgetGroupXrefAdHistory WHERE BudgetGroupHistoryId = @BudgetGroupHistoryId;";
            string q3 = @"SELECT * FROM Orders.BudgetGroupXrefLocationHistory WHERE BudgetGroupHistoryId = @BudgetGroupHistoryId;";
            string q4 = @"SELECT * FROM Orders.BudgetGroupXrefConversionActionHistory WHERE BudgetGroupHistoryId = @BudgetGroupHistoryId;";
            string q5 = @"SELECT * FROM Orders.BudgetGroupXrefPlacementHistory WHERE BudgetGroupHistoryId = @BudgetGroupHistoryId;";
            string q6 = @"SELECT * FROM Orders.BudgetGroupTimedBudgetHistory WHERE BudgetGroupHistoryId = @BudgetGroupHistoryId;";
            string sql = string.Format("{0}{1}{2}{3}{4}{5}", q1, q2, q3, q4, q5, q6);

            BudgetGroupHistory budgetGroupHistory = null;
            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                using (var multi = db.QueryMultiple(sql, new { BudgetGroupHistoryId = budgetGroupHistoryId }))
                {
                    budgetGroupHistory = multi.Read<BudgetGroupHistory>().SingleOrDefault();
                    if (budgetGroupHistory != null)
                    {
                        budgetGroupHistory.BudgetGroupXrefAdHistories = multi.Read<BudgetGroupXrefAdHistory>().ToList<BudgetGroupXrefAdHistory>();
                        budgetGroupHistory.BudgetGroupXrefLocationHistories = multi.Read<BudgetGroupXrefLocationHistory>().ToList<BudgetGroupXrefLocationHistory>();
                        budgetGroupHistory.BudgetGroupXrefConversionActionHistories = multi.Read<BudgetGroupXrefConversionActionHistory>().ToList<BudgetGroupXrefConversionActionHistory>();
                        budgetGroupHistory.BudgetGroupXrefPlacementHistories = multi.Read<BudgetGroupXrefPlacementHistory>().ToList<BudgetGroupXrefPlacementHistory>();
                        budgetGroupHistory.BudgetGroupTimedBudgetHistory = multi.Read<BudgetGroupTimedBudgetHistory>().FirstOrDefault();
                    }
                    return budgetGroupHistory;
                }
            }
            catch (Exception ex)
            {
                //TODO: Log This!!!
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        #region Budget Group History Helpers

        private int InsertIntoBudgetGroupHistory(BudgetGroupHistory budgetGroupHistory, IDbConnection db)
        {
            int rowsChanged = 0;

            budgetGroupHistory.CreatedDatetime = budgetGroupHistory.CreatedDatetime ?? DateTime.Now;

            string sql = @"INSERT INTO Orders.BudgetGroupHistory
                        (BudgetGroupHistoryId, BudgetGroupId, HistoryDate, BudgetGroupName, BudgetGroupBudget, CreatedBy, CreatedDatetime) VALUES 
                        (@BudgetGroupHistoryId, @BudgetGroupId, @HistoryDate, @BudgetGroupName, @BudgetGroupBudget, @CreatedBy, @CreatedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupHistory);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupHistory INSERT Failed.");
            }

            return rowsChanged;
        }

        private int InsertIntoBudgetGroupXrefAdHistory(List<BudgetGroupXrefAdHistory> budgetGroupXrefAdHistories, IDbConnection db)
        {
            int rowsChanged = 0;

            if (budgetGroupXrefAdHistories == null || budgetGroupXrefAdHistories.Count == 0)
            {
                return rowsChanged;
            }

            budgetGroupXrefAdHistories.ForEach(a => a.CreatedDatetime = a.CreatedDatetime ?? DateTime.Now);

            string sql = @"INSERT INTO Orders.BudgetGroupXrefAdHistory 
                        (BudgetGroupXrefAdHistoryId, BudgetGroupHistoryId, HistoryDate, AdId, CreatedBy, CreatedDatetime) VALUES 
                        (@BudgetGroupXrefAdHistoryId, @BudgetGroupHistoryId, @HistoryDate, @AdId, @CreatedBy, @CreatedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupXrefAdHistories);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefAdHistory INSERT Failed.");
            }
            return rowsChanged;
        }

        private int InsertIntoBudgetGroupTimedBudgetHistory(BudgetGroupTimedBudgetHistory budgetGroupTimedBudgetHistory, IDbConnection db)
        {
            int rowsChanged = 0;

            if (budgetGroupTimedBudgetHistory == null)
            {
                return rowsChanged;
            }

            budgetGroupTimedBudgetHistory.CreatedDatetime = budgetGroupTimedBudgetHistory.CreatedDatetime ?? DateTime.Now;

            string sql = @"INSERT INTO Orders.BudgetGroupTimedBudgetHistory
                        (BudgetGroupTimedBudgetHistoryId, BudgetGroupHistoryId, HistoryDate, BudgetAmount, Margin, StartDate, EndDate, CreatedBy, CreatedDatetime) VALUES 
                        (@BudgetGroupTimedBudgetHistoryId, @BudgetGroupHistoryId, @HistoryDate, @BudgetAmount, @Margin, @StartDate, @EndDate, @CreatedBy, @CreatedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupTimedBudgetHistory);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupTimedBudgetHistory INSERT Failed.");
            }
            return rowsChanged;
        }

        private int InsertIntoBudgetGroupXrefConversionActionHistory(List<BudgetGroupXrefConversionActionHistory> budgetGroupXrefConversionActionHistories, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefConversionActionHistories == null || budgetGroupXrefConversionActionHistories.Count == 0)
            {
                return rowsChanged;
            }

            budgetGroupXrefConversionActionHistories.ForEach(ca => ca.CreatedDatetime = ca.CreatedDatetime ?? DateTime.Now);

            string sql = @"INSERT INTO Orders.BudgetGroupXrefConversionActionHistory 
                        (BudgetGroupXrefConversionActionHistoryId, BudgetGroupHistoryId, HistoryDate, ConversionActionId, CreatedBy, CreatedDatetime) VALUES 
                        (@BudgetGroupXrefConversionActionHistoryId, @BudgetGroupHistoryId, @HistoryDate, @ConversionActionId, @CreatedBy, @CreatedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupXrefConversionActionHistories);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefConversionActionHistory INSERT Failed.");
            }
            return rowsChanged;
        }

        private int InsertIntoBudgetGroupXrefLocationHistory(List<BudgetGroupXrefLocationHistory> budgetGroupXrefLocationHistories, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefLocationHistories == null || budgetGroupXrefLocationHistories.Count == 0)
            {
                return rowsChanged;
            }

            budgetGroupXrefLocationHistories.ForEach(l => l.CreatedDatetime = l.CreatedDatetime ?? DateTime.Now);

            string sql = @"INSERT INTO Orders.BudgetGroupXrefLocationHistory 
                        (BudgetGroupXrefLocationHistoryId, BudgetGroupHistoryId, HistoryDate, LocationId, CreatedBy, CreatedDatetime) VALUES 
                        (@BudgetGroupXrefLocationHistoryId, @BudgetGroupHistoryId, @HistoryDate, @LocationId, @CreatedBy, @CreatedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupXrefLocationHistories);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefLocationHistory INSERT Failed.");
            }
            return rowsChanged;
        }

        private int InsertIntoBudgetGroupXrefPlacementHistory(List<BudgetGroupXrefPlacementHistory> budgetGroupXrefPlacementHistories, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefPlacementHistories == null || budgetGroupXrefPlacementHistories.Count == 0)
            {
                return rowsChanged;
            }

            budgetGroupXrefPlacementHistories.ForEach(p => p.CreatedDatetime = p.CreatedDatetime ?? DateTime.Now);

            string sql = @"INSERT INTO Orders.BudgetGroupXrefPlacementHistory 
                        (BudgetGroupXrefPlacementHistoryId, BudgetGroupHistoryId, HistoryDate, PlacementId, CreatedBy, CreatedDatetime) VALUES 
                        (@BudgetGroupXrefPlacementHistoryId, @BudgetGroupHistoryId, @HistoryDate, @PlacementId, @CreatedBy, @CreatedDatetime);";

            rowsChanged = db.Execute(sql, budgetGroupXrefPlacementHistories);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefPlacementHistory INSERT Failed.");
            }
            return rowsChanged;
        }

        #endregion

        #endregion

        #region State Transitions

        public int CreateBudgetGroupAndBudgetGroupHistory(BudgetGroup budgetGroup, BudgetGroupProposal budgetGroupProposal, BudgetGroupHistory budgetGroupHistory)
        {
            int rowsAffected = 0;
            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                using (var transaction = new TransactionScope())
                {
                    //TODO: Duplicated code below!!! Find elegant way to eliminate/reduce.
                    rowsAffected += InsertIntoBudgetGroup(budgetGroup, db);
                    rowsAffected += UpdateBudgetGroupProposalHelper(budgetGroupProposal, db);
                    rowsAffected += InsertIntoBudgetGroupXrefAd(budgetGroup.BudgetGroupXrefAds, db);
                    rowsAffected += InsertIntoBudgetGroupTimedBudget(budgetGroup.BudgetGroupTimedBudget, db);
                    rowsAffected += InsertIntoBudgetGroupXrefConversionAction(budgetGroup.BudgetGroupXrefConversionActions, db);
                    rowsAffected += InsertIntoBudgetGroupXrefLocation(budgetGroup.BudgetGroupXrefLocations, db);
                    rowsAffected += InsertIntoBudgetGroupXrefPlacement(budgetGroup.BudgetGroupXrefPlacements, db);

                    rowsAffected += InsertIntoBudgetGroupHistory(budgetGroupHistory, db);
                    rowsAffected += InsertIntoBudgetGroupXrefAdHistory(budgetGroupHistory.BudgetGroupXrefAdHistories, db);
                    rowsAffected += InsertIntoBudgetGroupTimedBudgetHistory(budgetGroupHistory.BudgetGroupTimedBudgetHistory, db);
                    rowsAffected += InsertIntoBudgetGroupXrefConversionActionHistory(budgetGroupHistory.BudgetGroupXrefConversionActionHistories, db);
                    rowsAffected += InsertIntoBudgetGroupXrefLocationHistory(budgetGroupHistory.BudgetGroupXrefLocationHistories, db);
                    rowsAffected += InsertIntoBudgetGroupXrefPlacementHistory(budgetGroupHistory.BudgetGroupXrefPlacementHistories, db);

                    transaction.Complete();
                }
            }
            catch (Exception ex)
            {
                //TODO: Log this!!!
                Console.WriteLine(ex.Message);
                throw;
            }
            return rowsAffected;
        }

        #endregion
    }
}
