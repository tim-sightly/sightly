﻿using System;
using System.Collections.Generic;
using System.Data;
using Sightly.DAL.DTO.BudgetGroupStates;

namespace Sightly.DAL
{
    public interface IBudgetGroupStore
    {
        BudgetGroup GetById(Guid budgetGroupId, IDbConnection connection = null);

        BudgetGroup GetBudgetGroupById(Guid budgetGroupId, IDbConnection connection = null, bool withChildren = true);

        List<BudgetGroup> GetBy(Guid currentUser, string column = null, int? statusId = null, Guid? filterByUser = null, bool withChildren = true);

        BudgetGroup CreateBudgetGroup(BudgetGroup budgetGroup, IDbConnection connection = null);

        BudgetGroup CreateOrUpdateBudgetGroup(BudgetGroup budgetGroup, IDbConnection connection = null);

        BudgetGroup UpdateBudgetGroup(BudgetGroup budgetGroup, Dictionary<string, List<Guid>> itemsToDelete);

        BudgetGroup UpdateCompleteBudgetGroup(BudgetGroup budgetGroup);

        int DeleteById(Guid budgetGroupId, IDbConnection connection = null);

        int DeleteChildrenByBudgetGroupId(Guid budgetGroupId, IDbConnection connection = null);

        BudgetGroup UpdateBudgetGroupInsertChildren(BudgetGroup budgetGroup, IDbConnection connection = null);
    }
}
