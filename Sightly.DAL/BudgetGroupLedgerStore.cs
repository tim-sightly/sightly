﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Sightly.DAL.DTO;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class BudgetGroupLedgerStore : ADataStore, IBudgetGroupLedgerStore
    {
        private ILoggingWrapper _loggingWrapper;
        private const int COMMAND_TIMEOUT = 300;

        public BudgetGroupLedgerStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }

        public BudgetGroupLedger GetBudgetGroupLedgerByBudgetGroup(Guid budgetGroupId, IDbConnection connection = null)
        {
            const string sql = @"SELECT TOP 1 * FROM Orders.BudgetGroupLedger WHERE BudgetGroupId = @BudgetGroupId ORDER BY Created DESC";
            try
            {
                using (var db = connection ?? new SqlConnection(ConnString))
                {
                    var budgetGroupLedger = db.Query<BudgetGroupLedger>(sql, new {BudgetGroupId = budgetGroupId}).FirstOrDefault();
                    return budgetGroupLedger;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public List<BudgetGroupLedger> GetBudgetGroupLedgerByBudgetGroupProposal(Guid budgetGroupProposalId, IDbConnection connection = null)
        {
            const string sql = @"SELECT * FROM Orders.BudgetGroupLedger WHERE BudgetGroupProposalId = @BudgetGroupProposalId ORDER BY Created DESC";

            try
            {
                using (var db = connection ?? new SqlConnection(ConnString))
                {
                    List<BudgetGroupLedger> budgetGroupLedger = db.Query<BudgetGroupLedger>(sql, new { BudgetGroupProposalId = budgetGroupProposalId }).ToList();
                    return budgetGroupLedger;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public BudgetGroupLedger GetLiveEventByProposalId(Guid budgetGroupProposalId, IDbConnection connection = null)
        {
            const string sql = @"SELECT TOP 1 * FROM Orders.BudgetGroupLedger WHERE BudgetGroupProposalId = @BudgetGroupProposalId  AND BudgetGroupEventId = 5 ORDER BY Created DESC";

            try
            {
                using (var db = connection ?? new SqlConnection(ConnString))
                {
                    var budgetGroupLedger = db.Query<BudgetGroupLedger>(sql, new { BudgetGroupProposalId = budgetGroupProposalId }, commandTimeout: 300).FirstOrDefault();
                    return budgetGroupLedger;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public long InsertIntoBudgetGroupLedger(BudgetGroupLedger budgetGroupLedger, IDbConnection connection=null)
        {
            long budgetLedgerId = -1;

            if (budgetGroupLedger == null)
            {
                return -1;
            }

            try
            {
                using (var db = connection ?? new SqlConnection(ConnString))
                {
                    const string sql = @"INSERT INTO Orders.BudgetGroupLedger 
                        (BudgetGroupId, BudgetGroupProposalId, BudgetGroupHistoryId, CreatedBy, Created, BudgetGroupEventId, Notes) VALUES 
                        (@BudgetGroupId, @BudgetGroupProposalId, @BudgetGroupHistoryId, @CreatedBy, @Created, @BudgetGroupEventId, @Notes);
                        SELECT CAST(SCOPE_IDENTITY() as bigint);";

                    budgetLedgerId = db.Query<long>(sql, budgetGroupLedger, commandTimeout: COMMAND_TIMEOUT).Single();

                    if (budgetLedgerId == -1)
                        throw new Exception();

                    return budgetLedgerId;
                }
                
            }
            catch (Exception)
            {
                _loggingWrapper.Logger.Error("BudgetGroupLedger INSERT Failed.");
                throw new Exception("BudgetGroupLedger INSERT Failed.");
            }
        }
    }
}