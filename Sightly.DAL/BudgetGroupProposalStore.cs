﻿using Sightly.DAL.DTO.BudgetGroupStates;
using Sightly.DAL.DTO.BudgetGroupTimedBudgetStates;
using Sightly.DAL.DTO.BudgetGroupXrefAdStates;
using Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates;
using Sightly.DAL.DTO.BudgetGroupXrefLocationStates;
using Sightly.DAL.DTO.BudgetGroupXrefPlacementStates;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Transactions;
using Sightly.DAL.DTO;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class BudgetGroupProposalStore: ADataStore, IBudgetGroupProposalStore
    {
        private readonly IBudgetGroupStore _budgetGroupStore;
        private readonly IBudgetGroupHistoryStore _budgetGroupHistoryStore;
        private readonly IBudgetGroupLedgerStore _budgetGroupLedgerStore;
        private readonly IOrderStore _orderStore;
        private readonly ILoggingWrapper _loggingWrapper;

        public BudgetGroupProposalStore(IBudgetGroupStore budgetGroupStore, IBudgetGroupHistoryStore budgetGroupHistoryStore, IBudgetGroupLedgerStore budgetGroupLedgerStore, IOrderStore orderStore, ILoggingWrapper loggingWrapper)
        {
            _budgetGroupStore = budgetGroupStore;
            _budgetGroupHistoryStore = budgetGroupHistoryStore;
            _budgetGroupLedgerStore = budgetGroupLedgerStore;
            _orderStore = orderStore;
            _loggingWrapper = loggingWrapper;
        }


        public BudgetGroupProposal CreateBudgetGroupProposal(BudgetGroupProposal budgetGroupProposal, BudgetGroup budgetGroup, Order order, OrderXrefBudgetGroup orderXrefBudgetGroup, BudgetGroupLedger budgetGroupLedger)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["TargetView"].ConnectionString))
                {
                    using (var transaction = new TransactionScope())
                    {
                        InsertIntoBudgetGroupProposal(budgetGroupProposal, db);
                        InsertIntoBudgetGroupXrefAdProposals(budgetGroupProposal.BudgetGroupXrefAdProposals, db);
                        InsertIntoBudgetGroupTimedBudgetProposals(budgetGroupProposal.BudgetGroupTimedBudgetProposals,
                            db);
                        InsertIntoBudgetGroupXrefConversionActionProposal(
                            budgetGroupProposal.BudgetGroupXrefConversionActionProposals, db);
                        InsertIntoBudgetGroupXrefLocationProposal(
                            budgetGroupProposal.BudgetGroupXrefLocationProposals, db);
                        InsertIntoBudgetGroupXrefPlacementProposal(
                            budgetGroupProposal.BudgetGroupXrefPlacementProposals, db);

                        _budgetGroupStore.CreateBudgetGroup(budgetGroup, db);
                        db.ConnectionString = ConfigurationManager.ConnectionStrings["TargetView"].ConnectionString;
                        _budgetGroupLedgerStore.InsertIntoBudgetGroupLedger(budgetGroupLedger, db);
                        _orderStore.Insert(order, db);
                        db.ConnectionString = ConfigurationManager.ConnectionStrings["TargetView"].ConnectionString;
                        _orderStore.InsertOrderXrefBudgetGroup(orderXrefBudgetGroup, db);

                        transaction.Complete();
                    }

                    db.ConnectionString = ConfigurationManager.ConnectionStrings["TargetView"].ConnectionString;
                    BudgetGroupProposal createdBudgetGroupProposal =
                        GetBudgetGroupProposalById(budgetGroupProposal.BudgetGroupId, db);
                    return createdBudgetGroupProposal;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public BudgetGroupProposal GetBudgetGroupProposalById(Guid budgetGroupProposalId, IDbConnection connection = null, bool withChildren = true)
        {
            var q1 = @"SELECT bg.*, u1.UserId AS CreatedById, u2.UserId AS ModifiedById FROM Orders.BudgetGroup bg
                     LEFT OUTER JOIN Users.FullnameUserId u1 ON bg.CreatedBy = u1.FullName 
                     LEFT OUTER JOIN Users.FullNameUserId u2 ON bg.LastModifiedBy = u2.FullName 
                     WHERE bg.BudgetGroupId = @BudgetGroupId;";

            var q2 = @"SELECT a.*, u1.UserId AS CreatedById, u2.UserId AS ModifiedById FROM Orders.BudgetGroupXrefAd a 
                     LEFT OUTER JOIN Users.FullnameUserId u1 ON a.CreatedBy = u1.FullName 
                     LEFT OUTER JOIN Users.FullNameUserId u2 ON a.LastModifiedBy = u2.FullName 
                     WHERE a.BudgetGroupId = @BudgetGroupId;";

            var q3 = @"SELECT l.*, u1.UserId AS CreatedById, u2.UserId AS ModifiedById FROM Orders.BudgetGroupXrefLocation l 
                     LEFT OUTER JOIN Users.FullnameUserId u1 ON l.CreatedBy = u1.FullName 
                     LEFT OUTER JOIN Users.FullNameUserId u2 ON l.LastModifiedBy = u2.FullName 
                     WHERE l.BudgetGroupId = @BudgetGroupId;";

            var q4 = @"SELECT ca.*, u1.UserId AS CreatedById, u2.UserId FROM Orders.BudgetGroupXrefConversionAction ca 
                     LEFT OUTER JOIN Users.FullnameUserId u1 ON ca.CreatedBy = u1.FullName 
                     LEFT OUTER JOIN Users.FullNameUserId u2 ON ca.LastModifiedBy = u2.FullName 
                     WHERE ca.BudgetGroupId = @BudgetGroupId;";

            var q5 = @"SELECT p.*, u1.UserId AS CreatedById FROM Orders.BudgetGroupXrefPlacement p 
                     LEFT OUTER JOIN Users.FullnameUserId u1 ON p.AssignedBy = u1.FullName 
                     WHERE p.BudgetGroupId = @BudgetGroupId;";

            var q6 = @"SELECT tb.*, u1.UserId AS CreatedById FROM Orders.BudgetGroupTimedBudget tb
                     LEFT OUTER JOIN Users.FullnameUserId u1 ON tb.CreatedBy = u1.FullName 
                     WHERE tb.BudgetGroupId = @BudgetGroupId;";  

            var sql = $"{q1}{q2}{q3}{q4}{q5}{q6}";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                using (var multi = db.QueryMultiple(sql, new { BudgetGroupId = budgetGroupProposalId, Now = DateTime.UtcNow }, commandTimeout: 300))
                {
                    var budgetGroupProposal = multi.Read<BudgetGroupProposal>().SingleOrDefault();
                    if (budgetGroupProposal != null && withChildren)
                    {
                        budgetGroupProposal.BudgetGroupXrefAdProposals = multi.Read<BudgetGroupXrefAdProposal>().ToList();
                        budgetGroupProposal.BudgetGroupXrefLocationProposals = multi.Read<BudgetGroupXrefLocationProposal>().ToList();
                        budgetGroupProposal.BudgetGroupXrefConversionActionProposals = multi.Read<BudgetGroupXrefConversionActionProposal>().ToList();
                        budgetGroupProposal.BudgetGroupXrefPlacementProposals = multi.Read<BudgetGroupXrefPlacementProposal>().ToList();
                        budgetGroupProposal.BudgetGroupTimedBudgetProposals = multi.Read<BudgetGroupTimedBudgetProposal>().ToList();
                    }
                    return budgetGroupProposal;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public List<Guid> GetBudgetGroupProposalIdByOrderId(Guid orderId, IDbConnection connection = null, bool withChildren = true)
        {
            var sql = @"SELECT oxbg.BudgetGroupId FROM Orders.[Order] o
                        INNER JOIN Orders.OrderXrefBudgetGroup oxbg ON o.OrderId = oxbg.OrderId
                        INNER JOIN zLookups.OrderStatus os ON o.OrderStatusId = os.OrderStatusId 
                        WHERE o.orderId = @OrderId
                        AND os.OrderStatusName = 'Ready';";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<Guid>(sql, new { OrderId = orderId }).ToList();
                }

            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }

        }


        public List<BudgetGroupProposal> GetBudgetGroupProposalsBy(string column, int? status = null, Guid? user = null, bool withChildren = true)
        {
            Dictionary<string, Tuple<string, dynamic>> columnQueryLookup = new Dictionary<string, Tuple<string, dynamic>>()
            {
                {
                    "None",
                    new Tuple<string, dynamic>("SELECT * FROM Orders.BudgetGroupProposal;", new { })
                },
                {
                    "BudgetGroupProposalStatusId",
                    new Tuple<string, dynamic>("SELECT * FROM Orders.BudgetGroupProposal WHERE BudgetGroupProposalStatusId = @BudgetGroupProposalStatusId;", new { BudgetGroupProposalStatusId = status ?? -1 })
                },
                {
                    "CreatedBy",
                    new Tuple<string, dynamic>("SELECT * FROM Orders.BudgetGroupProposal WHERE CreatedBy = @CreatedBy;", new { CreatedBy = user ?? Guid.Empty })
                },
                {
                    "ModifiedBy",
                    new Tuple<string, dynamic>("SELECT * FROM Orders.BudgetGroupProposal WHERE ModifiedBy = @ModifiedBy;", new { Modified = user ?? Guid.Empty })
                },
            };

            var q1 = columnQueryLookup[column].Item1;
            var q2 = @"SELECT * FROM Orders.BudgetGroupXrefAdProposal WHERE BudgetGroupProposalId = @BudgetGroupProposalId;";
            var q3 = @"SELECT * FROM Orders.BudgetGroupXrefLocationProposal WHERE BudgetGroupProposalId = @BudgetGroupProposalId;";
            var q4 = @"SELECT * FROM Orders.BudgetGroupXrefConversionActionProposal WHERE BudgetGroupProposalId = @BudgetGroupProposalId;";
            var q5 = @"SELECT * FROM Orders.BudgetGroupXrefPlacementProposal WHERE BudgetGroupProposalId = @BudgetGroupProposalId;";
            var q6 = @"SELECT * FROM Orders.BudgetGroupTimedBudgetProposal WHERE BudgetGroupProposalId = @BudgetGroupProposalId;";
            var sql = $"{q2}{q3}{q4}{q5}{q6}";

            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                using (var multi = db.QueryMultiple(q1, (object)columnQueryLookup[column].Item2))
                {
                    var budgetGroupProposals = multi.Read<BudgetGroupProposal>().ToList();
                    if (withChildren)
                    {
                        foreach (BudgetGroupProposal bgp in budgetGroupProposals)
                        {
                            using (var multiChildren = db.QueryMultiple(sql, new {bgp.BudgetGroupId }))
                            {
                                bgp.BudgetGroupXrefAdProposals = multiChildren.Read<BudgetGroupXrefAdProposal>().ToList();
                                bgp.BudgetGroupXrefLocationProposals = multiChildren.Read<BudgetGroupXrefLocationProposal>().ToList();
                                bgp.BudgetGroupXrefConversionActionProposals = multiChildren.Read<BudgetGroupXrefConversionActionProposal>().ToList();
                                bgp.BudgetGroupXrefPlacementProposals = multiChildren.Read<BudgetGroupXrefPlacementProposal>().ToList();
                                bgp.BudgetGroupTimedBudgetProposals = multiChildren.Read<BudgetGroupTimedBudgetProposal>().ToList();
                            }
                        }
                    }                    
                    return budgetGroupProposals;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }


        public BudgetGroupProposal UpdateBudgetGroupProposalStatus(Guid budgetGroupProposalId, int statusId, Guid modifiedBy, BudgetGroupLedger budgetGroupLedger = null)
        {
            BudgetGroupProposal budgetGroupProposal = null;
            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                {
                    using (var transaction = new TransactionScope())
                    {
                        string sql = @"UPDATE Orders.BudgetGroupProposal SET 
                           BudgetGroupStatusId = @BudgetGroupStatusId,
                           ModifiedBy = @ModifiedBy, 
                           Modified = @Modified 
                           WHERE BudgetGroupProposalId = @BudgetGroupProposalId;";
                        db.Execute(sql, new { BudgetGroupProposalId = budgetGroupProposalId, BudgetGroupStatusId = statusId, ModifiedBy = modifiedBy, Modified = DateTime.UtcNow });

                        if (budgetGroupLedger != null)
                        {
                            _budgetGroupLedgerStore.InsertIntoBudgetGroupLedger(budgetGroupLedger, db);
                        }

                        transaction.Complete();
                    }
                    budgetGroupProposal = GetBudgetGroupProposalById(budgetGroupProposalId, db, false);
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message); 
                throw;
            }
            return budgetGroupProposal;
        }

        public BudgetGroupProposal UpdateBudgetGroupProposal(BudgetGroupProposal budgetGroupProposal, Dictionary<string, List<Guid>> itemsToDelete)
        {
            if (budgetGroupProposal == null)
                return null;

            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                {
                    using (var transaction = new TransactionScope())
                    {
                        UpdateBudgetGroupProposal(budgetGroupProposal, db);

                        DeleteBudgetGroupXrefAdProposals(itemsToDelete["ads"], db);
                        DeleteBudgetGroupXrefLocationProposals(itemsToDelete["locations"], db);
                        DeleteBudgetGroupTimedBudgetProposals(itemsToDelete["timedBudget"], db);
                        DeleteBudgetGroupXrefConversionActionProposals(itemsToDelete["conversionActions"], db);
                        DeleteBudgetGroupXrefPlacementProposals(itemsToDelete["placements"], db);

                        MergeBudgetGroupXrefAdProposals(budgetGroupProposal.BudgetGroupXrefAdProposals, db);
                        MergeBudgetGroupXrefLocationProposals(budgetGroupProposal.BudgetGroupXrefLocationProposals, db);
                        MergeBudgetGroupTimedBudgetProposals(budgetGroupProposal.BudgetGroupTimedBudgetProposals, db);
                        MergeBudgetGroupXrefConversionActionProposals(budgetGroupProposal.BudgetGroupXrefConversionActionProposals, db);
                        MergeBudgetGroupXrefPlacementProposals(budgetGroupProposal.BudgetGroupXrefPlacementProposals, db);

                        transaction.Complete();
                    }
                    return GetBudgetGroupProposalById(budgetGroupProposal.BudgetGroupId, db);
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public BudgetGroupProposal OverrideProposalWithLiveBudgetGroup(BudgetGroupProposal budgetGroupProposal,
            Dictionary<string, List<Guid>> itemsToDelete)
        {
            if (budgetGroupProposal == null)
                return null;

            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                {
                    using (var transaction = new TransactionScope())
                    {
                        UpdateBudgetGroupProposal(budgetGroupProposal, db);

                        DeleteBudgetGroupXrefAdProposals(itemsToDelete["ads"], db);
                        DeleteBudgetGroupXrefLocationProposals(itemsToDelete["locations"], db);
                        DeleteBudgetGroupTimedBudgetProposals(itemsToDelete["timedBudget"], db);
                        DeleteBudgetGroupXrefConversionActionProposals(itemsToDelete["conversionActions"], db);
                        DeleteBudgetGroupXrefPlacementProposals(itemsToDelete["placements"], db);

                        InsertIntoBudgetGroupXrefAdProposals(budgetGroupProposal.BudgetGroupXrefAdProposals, db);
                        InsertIntoBudgetGroupXrefLocationProposal(budgetGroupProposal.BudgetGroupXrefLocationProposals, db);
                        InsertIntoBudgetGroupTimedBudgetProposals(budgetGroupProposal.BudgetGroupTimedBudgetProposals, db);
                        InsertIntoBudgetGroupXrefConversionActionProposal(budgetGroupProposal.BudgetGroupXrefConversionActionProposals, db);
                        InsertIntoBudgetGroupXrefPlacementProposal(budgetGroupProposal.BudgetGroupXrefPlacementProposals, db);

                        transaction.Complete();
                    }
                    return GetBudgetGroupProposalById(budgetGroupProposal.BudgetGroupId, db);
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        #region BudgetGroupProposal Helpers

        internal int InsertIntoBudgetGroupProposal(BudgetGroupProposal budgetGroupProposal, IDbConnection db)
        {
            int rowsChanged = 0;
            string sql = @"INSERT INTO Orders.BudgetGroupProposal 
                        (BudgetGroupProposalId, BudgetGroupStatusId, BudgetGroupName, BudgetGroupBudget, CreatedBy, Created) VALUES 
                        (@BudgetGroupProposalId, @BudgetGroupStatusId, @BudgetGroupName, @BudgetGroupBudget, @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupProposal);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupProposal INSERT Failed.");
            }

            return rowsChanged;
        }

        internal int UpdateBudgetGroupProposal(BudgetGroupProposal budgetGroupProposal, IDbConnection db)
        {
            int rowsChanged = 0;
            string sql = @"UPDATE Orders.BudgetGroupProposal SET
                            BudgetGroupStatusId = @BudgetGroupStatusId
                           ,BudgetGroupName = @BudgetGroupName
                           ,BudgetGroupBudget = @BudgetGroupBudget
                           ,ModifiedBy = @ModifiedBy
                           ,Modified = @Modified
                        WHERE BudgetGroupProposalId = @BudgetGroupProposalId;";

            rowsChanged = db.Execute(sql, budgetGroupProposal);

            if (rowsChanged == 0)
            {
                throw new Exception($"BudgetGroupProposal UPDATE failed for BudgetGroupProposalId = {budgetGroupProposal.BudgetGroupId}.");
            }

            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupXrefAdProposals(List<BudgetGroupXrefAdProposal> budgetGroupXrefAdProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefAdProposals == null || budgetGroupXrefAdProposals.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"INSERT INTO Orders.BudgetGroupXrefAdProposal 
                        (BudgetGroupXrefAdProposalId, BudgetGroupProposalId, AdId, CreatedBy, Created) VALUES 
                        (@BudgetGroupXrefAdProposalId, @BudgetGroupProposalId, @AdId, @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefAdProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefAdProposal INSERT Failed.");
            }
            return rowsChanged;
        }

        internal int UpdateBudgetGroupXrefAdProposals(List<BudgetGroupXrefAdProposal> budgetGroupXrefAdProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefAdProposals == null || budgetGroupXrefAdProposals.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"UPDATE Orders.BudgetGroupXrefAdProposal SET
                        AdId = @AdId
                       ,ModifiedBy = @ModifiedBy
                       ,Modified = @Modified
                    WHERE BudgetGroupXrefAdProposalId = @BudgetGroupXrefAdProposalId;";

            rowsChanged = db.Execute(sql, budgetGroupXrefAdProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefAdProposal UPDATE failed");
            }
            return rowsChanged;
        }

        internal int MergeBudgetGroupXrefAdProposals(List<BudgetGroupXrefAdProposal> budgetGroupXrefAdProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefAdProposals == null || budgetGroupXrefAdProposals.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"MERGE Orders.BudgetGroupXrefAdProposal AS [target]
                            USING (SELECT 
                                    @BudgetGroupXrefAdProposalId AS BudgetGroupXrefAdProposalId, 
                                    @BudgetGroupProposalId AS BudgetGroupProposalId, 
                                    @AdId AS AdId, 
                                    @CreatedBy AS CreatedBy, 
                                    @Created AS Created, 
                                    @ModifiedBy AS ModifiedBy, 
                                    @Modified AS Modified) AS [source]
                                ON target.BudgetGroupXrefAdProposalId = source.BudgetGroupXrefAdProposalId
                            WHEN MATCHED THEN
                                UPDATE SET AdId = @AdId, ModifiedBy = @ModifiedBy, Modified = @Modified 
                            WHEN NOT MATCHED THEN
                                INSERT (BudgetGroupXrefAdProposalId, BudgetGroupProposalId, AdId, CreatedBy, Created) VALUES (@BudgetGroupXrefAdProposalId, @BudgetGroupProposalId, @AdId, @CreatedBy, @Created);";
                    
            rowsChanged = db.Execute(sql, budgetGroupXrefAdProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefAdProposal MERGE failed");
            }
            return rowsChanged;
        }

        internal int DeleteBudgetGroupXrefAdProposals(List<Guid> budgetGroupXrefAdProposalIds, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefAdProposalIds.Count == 0)
            {
                return rowsChanged;
            }

            var itemsToDelete = budgetGroupXrefAdProposalIds.Select(a => new { BudgetGroupXrefAdProposalId = a }).ToList();

            string sql = @"DELETE FROM Orders.BudgetGroupXrefAdProposal
                            WHERE BudgetGroupXrefAdProposalId = @BudgetGroupXrefAdProposalId;";

            rowsChanged = db.Execute(sql, itemsToDelete);

            if (rowsChanged == 0)
            {
                throw new Exception("DELETE FROM BudgetGroupXrefAdProposal failed.");
            }
            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupTimedBudgetProposals(List<BudgetGroupTimedBudgetProposal> budgetGroupTimedBudgetProposal, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupTimedBudgetProposal == null || budgetGroupTimedBudgetProposal.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"INSERT INTO Orders.BudgetGroupTimedBudgetProposal 
                        (BudgetGroupTimedBudgetProposalId, BudgetGroupProposalId, BudgetAmount, StartDate, EndDate, CreatedBy, Created, Margin, BudgetGroupXrefPlacementId) VALUES 
                        (@BudgetGroupTimedBudgetProposalId, @BudgetGroupProposalId, @BudgetAmount, @StartDate, @EndDate, @CreatedBy, @Created, @Margin, @BudgetGroupXrefPlacementId);";

            rowsChanged = db.Execute(sql, budgetGroupTimedBudgetProposal);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupTimedBudgetProposal INSERT Failed.");
            }
            return rowsChanged;
        }

        internal int UpdateBudgetGroupTimedBudgetProposals(List<BudgetGroupTimedBudgetProposal> budgetGroupTimedBudgetProposal, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupTimedBudgetProposal == null || budgetGroupTimedBudgetProposal.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"UPDATE Orders.BudgetGroupTimedBudgetProposal SET
                        BudgetAmount = @BudgetAmount
                       ,StartDate = @StartDate
                       ,EndDate = @EndDate
                       ,Margin = @Margin
                       ,BudgetGroupXrefPlacementId = @BudgetGroupXrefPlacementId
                    WHERE BudgetGroupTimedBudgetProposal = @BudgetGroupTimedBudgetProposal;";

            rowsChanged = db.Execute(sql, budgetGroupTimedBudgetProposal);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupTimedBudgetProposal UPDATE failed");
            }
            return rowsChanged;
        }

        internal int MergeBudgetGroupTimedBudgetProposals(List<BudgetGroupTimedBudgetProposal> budgetGroupTimedBudgetProposal, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupTimedBudgetProposal == null || budgetGroupTimedBudgetProposal.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"MERGE Orders.BudgetGroupTimedBudgetProposal AS [target]
                            USING (SELECT 
                                     @BudgetGroupTimedBudgetProposalId AS BudgetGroupTimedBudgetProposalId
                                    ,@BudgetGroupProposalId AS BudgetGroupProposalId
                                    ,@BudgetAmount AS BudgetAmount
                                    ,@StartDate AS StartDate
                                    ,@EndDate AS EndDate
                                    ,@Margin AS Margin
                                    ,@CreatedBy AS CreatedBy
                                    ,@Created AS Created
                                    ,@BudgetGroupXrefPlacementId AS BudgetGroupXrefPlacementId) AS [source]
                                ON target.BudgetGroupTimedBudgetProposalId = source.BudgetGroupTimedBudgetProposalId
                            WHEN MATCHED THEN
                                UPDATE SET BudgetAmount = @BudgetAmount, StartDate = @StartDate, EndDate = @EndDate, Margin = @Margin, BudgetGroupXrefPlacementId = @BudgetGroupXrefPlacementId 
                            WHEN NOT MATCHED THEN
                                INSERT (BudgetGroupTimedBudgetProposalId, BudgetGroupProposalId, BudgetAmount, StartDate, EndDate, CreatedBy, Created, Margin, BudgetGroupXrefPlacementId) VALUES 
                                       (@BudgetGroupTimedBudgetProposalId, @BudgetGroupProposalId, @BudgetAmount, @StartDate, @EndDate, @CreatedBy, @Created, @Margin, @BudgetGroupXrefPlacementId);";

            rowsChanged = db.Execute(sql, budgetGroupTimedBudgetProposal);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupTimedBudgetProposal MERGE failed");
            }
            return rowsChanged;
        }

        internal int DeleteBudgetGroupTimedBudgetProposals(List<Guid> timedBudgetProposalIds, IDbConnection db)
        {
            int rowsChanged = 0;
            if (timedBudgetProposalIds.Count == 0)
            {
                return rowsChanged;
            }

            var itemsToDelete = timedBudgetProposalIds.Select(tb => new { BudgetGroupTimedBudgetProposalId = tb }).ToList();

            string sql = @"DELETE FROM Orders.BudgetGroupTimedBudgetProposal
                            WHERE BudgetGroupTimedBudgetProposalId = @BudgetGroupTimedBudgetProposalId;";

            rowsChanged = db.Execute(sql, itemsToDelete);

            if (rowsChanged == 0)
            {
                throw new Exception("DELETE FROM BudgetGroupTimedBudgetProposal failed.");
            }
            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupXrefConversionActionProposal(List<BudgetGroupXrefConversionActionProposal> budgetGroupXrefConversionActionProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefConversionActionProposals == null || budgetGroupXrefConversionActionProposals.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"INSERT INTO Orders.BudgetGroupXrefConversionActionProposal 
                        (BudgetGroupXrefConversionActionProposalId, BudgetGroupProposalId, ConversionActionId, CreatedBy, Created) VALUES 
                        (@BudgetGroupXrefConversionActionProposalId, @BudgetGroupProposalId, @ConversionActionId, @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefConversionActionProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefConversionActionProposal INSERT Failed.");
            }
            return rowsChanged;
        }

        internal int UpdateBudgetGroupXrefConversionActionProposals(List<BudgetGroupXrefConversionActionProposal> budgetGroupXrefConversionActionProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefConversionActionProposals == null || budgetGroupXrefConversionActionProposals.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"UPDATE Orders.BudgetGroupXrefConversionActionProposal SET
                        ConversionActionId = @ConversionActionId
                       ,ModifiedBy = @ModifiedBy
                       ,Modified = @Modified
                    WHERE BudgetGroupXrefConversionActionProposalId = @BudgetGroupXrefConversionActionProposalId;";

            rowsChanged = db.Execute(sql, budgetGroupXrefConversionActionProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefConversionActionProposal UPDATE failed");
            }
            return rowsChanged;
        }

        internal int MergeBudgetGroupXrefConversionActionProposals(List<BudgetGroupXrefConversionActionProposal> budgetGroupXrefConversionActionProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefConversionActionProposals == null || budgetGroupXrefConversionActionProposals.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"MERGE Orders.BudgetGroupXrefConversionActionProposal AS [target]
                            USING (SELECT @BudgetGroupXrefConversionActionProposalId AS BudgetGroupXrefConversionActionProposalId, @BudgetGroupProposalId AS BudgetGroupProposalId, @ConversionActionId AS ConversionActionId, @CreatedBy AS CreatedBy, @Created AS Created, @ModifiedBy AS ModifiedBy, @Modified AS Modified) AS [source]
                                ON target.BudgetGroupXrefConversionActionProposalId = source.BudgetGroupXrefConversionActionProposalId
                            WHEN MATCHED THEN
                                UPDATE SET ConversionActionId = @ConversionActionId, ModifiedBy = @ModifiedBy, Modified = @Modified 
                            WHEN NOT MATCHED THEN
                                INSERT (BudgetGroupXrefConversionActionProposalId, BudgetGroupProposalId, ConversionActionId, CreatedBy, Created) VALUES 
                                       (@BudgetGroupXrefConversionActionProposalId, @BudgetGroupProposalId, @ConversionActionId, @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefConversionActionProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefConversionActionProposal MERGE failed");
            }
            return rowsChanged;
        }

        internal int DeleteBudgetGroupXrefConversionActionProposals(List<Guid> budgetGroupXrefConversionActionProposalIds, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefConversionActionProposalIds == null || budgetGroupXrefConversionActionProposalIds.Count == 0)
            {
                return rowsChanged;
            }

            var itemsToDelete = budgetGroupXrefConversionActionProposalIds.Select(ca => new { BudgetGroupXrefConversionActionProposalId = ca }).ToList();

            string sql = @"DELETE FROM Orders.BudgetGroupXrefConversionActionProposal
                            WHERE BudgetGroupXrefConversionActionProposalId = @BudgetGroupXrefConversionActionProposalId;";

            rowsChanged = db.Execute(sql, itemsToDelete);

            if (rowsChanged == 0)
            {
                throw new Exception("DELETE FROM BudgetGroupXrefConversionActionProposal failed.");
            }
            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupXrefLocationProposal(List<BudgetGroupXrefLocationProposal> budgetGroupXrefLocationProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefLocationProposals == null || budgetGroupXrefLocationProposals.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"INSERT INTO Orders.BudgetGroupXrefLocationProposal 
                        (BudgetGroupXrefLocationProposalId, BudgetGroupProposalId, LocationId, CreatedBy, Created) VALUES 
                        (@BudgetGroupXrefLocationProposalId, @BudgetGroupProposalId, @LocationId, @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefLocationProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefLocationProposal INSERT Failed.");
            }
            return rowsChanged;
        }

        internal int UpdateBudgetGroupXrefLocationProposals(List<BudgetGroupXrefLocationProposal> budgetGroupXrefLocationProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefLocationProposals == null || budgetGroupXrefLocationProposals.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"UPDATE Orders.BudgetGroupXrefLocationProposal SET
                        LocationId = @LocationId
                       ,ModifiedBy = @ModifiedBy
                       ,Modified = @Modified
                    WHERE BudgetGroupXrefLocationProposalId = @BudgetGroupXrefLocationProposalId;";

            rowsChanged = db.Execute(sql, budgetGroupXrefLocationProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefLocationProposal UPDATE failed");
            }
            return rowsChanged;
        }

        internal int MergeBudgetGroupXrefLocationProposals(List<BudgetGroupXrefLocationProposal> budgetGroupXrefLocationProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefLocationProposals == null || budgetGroupXrefLocationProposals.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"MERGE Orders.BudgetGroupXrefLocationProposal AS [target]
                            USING (SELECT @BudgetGroupXrefLocationProposalId AS BudgetGroupXrefLocationProposalId, @BudgetGroupProposalId AS BudgetGroupProposalId, @LocationId AS LocationId, @CreatedBy AS CreatedBy, @Created AS Created, @ModifiedBy AS ModifiedBy, @Modified AS Modified) AS [source]
                                ON target.BudgetGroupXrefLocationProposalId = source.BudgetGroupXrefLocationProposalId
                            WHEN MATCHED THEN
                                UPDATE SET LocationId = @LocationId, ModifiedBy = @ModifiedBy, Modified = @Modified 
                            WHEN NOT MATCHED THEN
                                INSERT (BudgetGroupXrefLocationProposalId, BudgetGroupProposalId, LocationId, CreatedBy, Created) VALUES (@BudgetGroupXrefLocationProposalId, @BudgetGroupProposalId, @LocationId, @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefLocationProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefLocationProposal MERGE failed");
            }
            return rowsChanged;
        }

        internal int DeleteBudgetGroupXrefLocationProposals(List<Guid> budgetGroupXrefLocationProposalIds, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefLocationProposalIds == null || budgetGroupXrefLocationProposalIds.Count == 0)
            {
                return rowsChanged;
            }

            var itemsToDelete = budgetGroupXrefLocationProposalIds.Select(l => new { BudgetGroupXrefLocationProposalId = l }).ToList();

            string sql = @"DELETE FROM Orders.BudgetGroupXrefLocationProposal
                            WHERE BudgetGroupXrefLocationProposalId = @BudgetGroupXrefLocationProposalId;";

            rowsChanged = db.Execute(sql, itemsToDelete);

            if (rowsChanged == 0)
            {
                throw new Exception("DELETE FROM BudgetGroupXrefLocationProposal failed.");
            }
            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupXrefPlacementProposal(List<BudgetGroupXrefPlacementProposal> budgetGroupXrefPlacementProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefPlacementProposals == null || budgetGroupXrefPlacementProposals.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"INSERT INTO Orders.BudgetGroupXrefPlacementProposal 
                        (BudgetGroupXrefPlacementProposalId, BudgetGroupProposalId, PlacementId, AssignedBy, AssignedDate) VALUES 
                        (@BudgetGroupXrefPlacementProposalId, @BudgetGroupProposalId, @PlacementId, @AssignedBy, @AssignedDate);";

            rowsChanged = db.Execute(sql, budgetGroupXrefPlacementProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefPlacementProposal INSERT Failed.");
            }
            return rowsChanged;
        }

        internal int UpdateBudgetGroupXrefPlacementProposals(List<BudgetGroupXrefPlacementProposal> budgetGroupXrefPlacementProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefPlacementProposals == null || budgetGroupXrefPlacementProposals.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"UPDATE Orders.BudgetGroupXrefPlacementProposal SET
                        PlacementId = @PlacementId
                       ,ModifiedBy = @ModifiedBy
                       ,Modified = @Modified
                    WHERE BudgetGroupXrefPlacementProposalId = @BudgetGroupXrefPlacementProposalId;";

            rowsChanged = db.Execute(sql, budgetGroupXrefPlacementProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefPlacementProposal UPDATE failed");
            }
            return rowsChanged;
        }

        internal int MergeBudgetGroupXrefPlacementProposals(List<BudgetGroupXrefPlacementProposal> budgetGroupXrefPlacementProposals, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefPlacementProposals == null || budgetGroupXrefPlacementProposals.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"MERGE Orders.BudgetGroupXrefPlacementProposal AS [target]
                            USING (SELECT @BudgetGroupXrefPlacementProposalId AS BudgetGroupXrefPlacementProposalId, @BudgetGroupProposalId AS BudgetGroupProposalId, @PlacementId AS PlacementId, @AssignedBy AS AssignedBy, @AssignedDate AS AssignedDate) AS [source]
                                ON target.BudgetGroupXrefPlacementProposalId = source.BudgetGroupXrefPlacementProposalId
                            WHEN MATCHED THEN
                                UPDATE SET PlacementId = @PlacementId, AssignedBy = @AssignedBy, AssignedDate = @AssignedDate 
                            WHEN NOT MATCHED THEN
                                INSERT (BudgetGroupXrefPlacementProposalId, BudgetGroupProposalId, PlacementId, AssignedBy, AssignedDate) VALUES 
                                       (@BudgetGroupXrefPlacementProposalId, @BudgetGroupProposalId, @PlacementId, @AssignedBy, @AssignedDate);";

            rowsChanged = db.Execute(sql, budgetGroupXrefPlacementProposals);

            if (rowsChanged == 0)
            {
                throw new Exception("BudgetGroupXrefPlacementProposal MERGE failed");
            }
            return rowsChanged;
        }

        internal int DeleteBudgetGroupXrefPlacementProposals(List<Guid> budgetGroupXrefPlacementProposalIds, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefPlacementProposalIds == null || budgetGroupXrefPlacementProposalIds.Count == 0)
            {
                return rowsChanged;
            }

            var itemsToDelete = budgetGroupXrefPlacementProposalIds.Select(p => new { BudgetGroupXrefPlacementProposalId = p }).ToList();

            string sql = @"DELETE FROM Orders.BudgetGroupXrefPlacementProposal
                            WHERE BudgetGroupXrefPlacementProposalId = @BudgetGroupXrefPlacementProposalId;";

            rowsChanged = db.Execute(sql, itemsToDelete);

            if (rowsChanged == 0)
            {
                throw new Exception("DELETE FROM BudgetGroupXrefPlacementProposal failed.");
            }
            return rowsChanged;
        }
        #endregion


        #region State Transitions


        public BudgetGroupProposal CreateOrUpdateBudgetGroupProposal(BudgetGroupProposal budgetGroupProposal, BudgetGroupLedger budgetGroupLedger)
        {
            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                {
                    using (var transaction = new TransactionScope())
                    {
                        UpdateBudgetGroupProposal(budgetGroupProposal, db);
                        UpdateBudgetGroupXrefAdProposals(budgetGroupProposal.BudgetGroupXrefAdProposals, db);
                        UpdateBudgetGroupXrefLocationProposals(budgetGroupProposal.BudgetGroupXrefLocationProposals, db);
                        UpdateBudgetGroupXrefPlacementProposals(budgetGroupProposal.BudgetGroupXrefPlacementProposals, db);
                        UpdateBudgetGroupXrefConversionActionProposals(budgetGroupProposal.BudgetGroupXrefConversionActionProposals, db);
                        UpdateBudgetGroupTimedBudgetProposals(budgetGroupProposal.BudgetGroupTimedBudgetProposals, db);
                        
                        transaction.Complete();
                    }

                    _budgetGroupLedgerStore.InsertIntoBudgetGroupLedger(budgetGroupLedger, db);

                    var updatedBudgetGroupProposal = GetBudgetGroupProposalById(budgetGroupProposal.BudgetGroupId, db);
                    return updatedBudgetGroupProposal;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
            
        }
        #endregion
    }
}
