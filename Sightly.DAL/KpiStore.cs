﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Sightly.DAL.DTO;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class KpiStore : ADataStore, IKpiStore
    {
        private readonly ILoggingWrapper _loggingWrapper;

        public KpiStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }

        public List<KpiCondition> GetKpiConditions(IDbConnection connection = null)
        {
            const string sql = @"Select EquationTypeId, ConditionName, ConditionDisplay from [raw].[EquationType] ";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<KpiCondition>(sql, null).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public List<KpiMetric> GetKpiMetrics(IDbConnection connection = null)
        {
            const string sql = @"SELECT km.KpiMetricId, km.KpiMetricName, km.KpiMetricAbbreviation FROM raw.KpiMetric km ";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<KpiMetric>(sql, null).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public List<KpiEditorData> GetKpiEditorDataByCustomerId(long awCustomerId, IDbConnection connection = null)
        {
            const string sql = @"Execute tvdb.GetKpiEditorDataByCustomerId @CustomerId ";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<KpiEditorData>(sql, new { CustomerId = awCustomerId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public int SaveKpiEditorData(long adwordsCustomerId, int kpiMetricId, int equationTypeId, string equationValue, int sequence,
            IDbConnection connection = null)
        {
            const string sql = @"Execute raw.SaveAdwordsCustomerKpi @AwCustomerId, @KpiMetricId, @EquationTypeId, @EquationValue, @Sequence";
            try
            {
                var rowsChanged = 0;
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    rowsChanged = db.Execute(sql, new
                    {
                        AwCustomerId = adwordsCustomerId,
                        KpiMetricId = kpiMetricId,
                        EquationTypeId = equationTypeId,
                        EquationValue = equationValue,
                        Sequence = sequence
                    });
                }
                if (rowsChanged == 0)
                {
                    throw new Exception("AdwordsCustomerKpi INSERT Failed.");
                }

                return rowsChanged;
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public void RemoveKpiEditorData(int adwordsCustomerKpiId, IDbConnection connection = null)
        {
            const string sql = @"Execute tvdb.RemoveKpiEditorData @AdwordsCustomerKpiId ";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    db.Execute(sql, new { AdwordsCustomerKpiId = adwordsCustomerKpiId });
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }
    }
}