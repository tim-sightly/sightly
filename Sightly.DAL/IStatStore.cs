﻿using System;
using System.Collections.Generic;
using System.Data;
using Sightly.DAL.DTO;

namespace Sightly.DAL
{
    public interface IStatStore
    {
        List<DailyStats> GetDailyDeviceStatsForAdWordsCustomerId(long adWordsCustomerId, IDbConnection connection = null);
        List<DailyStats> GetDailyDeviceStatsForCampaign(Guid campaignId, IDbConnection connection = null);
        List<DailyStats> GetDailyDeviceStatsForAccount(Guid accountId, IDbConnection connection = null);
        List<DailyStats> GetDailyDeviceStatsForAdvertiser(Guid advertiserId, IDbConnection connection = null);
    }
}