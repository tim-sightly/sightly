﻿using Sightly.DAL.DTO.BudgetGroupStates;
using Sightly.DAL.DTO.BudgetGroupTimedBudgetStates;
using Sightly.DAL.DTO.BudgetGroupXrefAdStates;
using Sightly.DAL.DTO.BudgetGroupXrefConversionActionStates;
using Sightly.DAL.DTO.BudgetGroupXrefLocationStates;
using Sightly.DAL.DTO.BudgetGroupXrefPlacementStates;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using System.Transactions;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class BudgetGroupHistoryStore : ADataStore, IBudgetGroupHistoryStore
    {
        private readonly ILoggingWrapper _loggingWrapper;
        private const int COMMAND_TIMEOUT = 300;

        public BudgetGroupHistoryStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }


        public int CreateBudgetGroupHistory(BudgetGroupHistory budgetGroupHistory, IDbConnection connection = null)
        {
            int rowsAffected = 0;
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                using (var transaction = new TransactionScope())
                {
                    rowsAffected += InsertIntoBudgetGroupHistory(budgetGroupHistory, db);
                    rowsAffected += InsertIntoBudgetGroupXrefAdHistory(budgetGroupHistory.BudgetGroupXrefAdHistories, db);
                    rowsAffected += InsertIntoBudgetGroupXrefConversionActionHistory(budgetGroupHistory.BudgetGroupXrefConversionActionHistories, db);
                    rowsAffected += InsertIntoBudgetGroupXrefLocationHistory(budgetGroupHistory.BudgetGroupXrefLocationHistories, db);
                    rowsAffected += InsertIntoBudgetGroupXrefPlacementHistory(budgetGroupHistory.BudgetGroupXrefPlacementHistories, db);
                    rowsAffected += InsertIntoBudgetGroupTimedBudgetHistory(budgetGroupHistory.BudgetGroupTimedBudgetHistories, db);

                    transaction.Complete();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
            return rowsAffected;
        }

        public BudgetGroupHistory GetBudgetGroupHistoryById(Guid budgetGroupHistoryId)
        {
            var q1 = @"SELECT * FROM Orders.BudgetGroupHistory WHERE BudgetGroupHistoryId = @BudgetGroupHistoryId;";
            var q2 = @"SELECT * FROM Orders.BudgetGroupXrefAdHistory WHERE BudgetGroupHistoryId = @BudgetGroupHistoryId;";
            var q3 = @"SELECT * FROM Orders.BudgetGroupXrefLocationHistory WHERE BudgetGroupHistoryId = @BudgetGroupHistoryId;";
            var q4 = @"SELECT * FROM Orders.BudgetGroupXrefConversionActionHistory WHERE BudgetGroupHistoryId = @BudgetGroupHistoryId;";
            var q5 = @"SELECT * FROM Orders.BudgetGroupXrefPlacementHistory WHERE BudgetGroupHistoryId = @BudgetGroupHistoryId;";
            var q6 = @"SELECT * FROM Orders.BudgetGroupTimedBudgetHistory WHERE BudgetGroupHistoryId = @BudgetGroupHistoryId;";
            var sql = $"{q1}{q2}{q3}{q4}{q5}{q6}";

            try
            {
                using (IDbConnection db = new SqlConnection(ConnString))
                using (var multi = db.QueryMultiple(sql, new { BudgetGroupHistoryId = budgetGroupHistoryId }))
                {
                    var budgetGroupHistory = multi.Read<BudgetGroupHistory>().SingleOrDefault();
                    if (budgetGroupHistory != null)
                    {
                        budgetGroupHistory.BudgetGroupXrefAdHistories = multi.Read<BudgetGroupXrefAdHistory>().ToList();
                        budgetGroupHistory.BudgetGroupXrefLocationHistories = multi.Read<BudgetGroupXrefLocationHistory>().ToList();
                        budgetGroupHistory.BudgetGroupXrefConversionActionHistories = multi.Read<BudgetGroupXrefConversionActionHistory>().ToList();
                        budgetGroupHistory.BudgetGroupXrefPlacementHistories = multi.Read<BudgetGroupXrefPlacementHistory>().ToList();
                        budgetGroupHistory.BudgetGroupTimedBudgetHistories = multi.Read<BudgetGroupTimedBudgetHistory>().ToList();
                    }
                    return budgetGroupHistory;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        #region Helper Methods

        internal int InsertIntoBudgetGroupHistory(BudgetGroupHistory budgetGroupHistory, IDbConnection db)
        {
            int rowsChanged = 0;

            string sql = @"INSERT INTO Hist.BudgetGroup
                        (BudgetGroupId, BudgetGroupStatusId, BudgetGroupName, CreatedBy, Created, ModifiedBy, Modified) VALUES 
                        (@BudgetGroupId, @BudgetGroupStatusId, @BudgetGroupName, @CreatedBy, @Created, @ModifiedBy, @Modified);";

            rowsChanged = db.Execute(sql, budgetGroupHistory, commandTimeout: COMMAND_TIMEOUT);

            if (rowsChanged == 0)
            {
                throw new Exception("Hist.BudgetGroup INSERT Failed.");
            }

            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupXrefAdHistory(List<BudgetGroupXrefAdHistory> budgetGroupXrefAdHistories, IDbConnection db)
        {
            int rowsChanged = 0;

            if (budgetGroupXrefAdHistories == null || budgetGroupXrefAdHistories.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"INSERT INTO Hist.BudgetGroupXrefAd 
                        (BudgetGroupXrefAdId, BudgetGroupId, AdId, CreatedBy, Created) VALUES 
                        (@BudgetGroupXrefAdId, @BudgetGroupId, @AdId, @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefAdHistories, commandTimeout: COMMAND_TIMEOUT);

            if (rowsChanged == 0)
            {
                throw new Exception("Hist.BudgetGroupXrefAd INSERT Failed.");
            }
            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupTimedBudgetHistory(List<BudgetGroupTimedBudgetHistory> budgetGroupTimedBudgetHistory, IDbConnection db)
        {
            int rowsChanged = 0;

            if (budgetGroupTimedBudgetHistory == null)
            {
                return rowsChanged;
            }

            string sql = @"INSERT INTO Hist.BudgetGroupTimedBudget
                        (BudgetGroupTimedBudgetId, BudgetGroupId, BudgetAmount, Margin, StartDate, EndDate, CreatedBy, Created, BudgetGroupXrefPlacementId) VALUES 
                        (@BudgetGroupTimedBudgetId, @BudgetGroupId, @BudgetAmount, @Margin, @StartDate, @EndDate, @CreatedBy, @Created, @BudgetGroupXrefPlacementId);";

            rowsChanged = db.Execute(sql, budgetGroupTimedBudgetHistory, commandTimeout: COMMAND_TIMEOUT);

            if (rowsChanged == 0)
            {
                throw new Exception("Hist.BudgetGroupTimedBudget INSERT Failed.");
            }
            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupXrefConversionActionHistory(List<BudgetGroupXrefConversionActionHistory> budgetGroupXrefConversionActionHistories, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefConversionActionHistories == null || budgetGroupXrefConversionActionHistories.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"INSERT INTO Hist.BudgetGroupXrefConversionAction
                        (BudgetGroupXrefConversionActionId, BudgetGroupId, ConversionActionId, CreatedBy, Created, ModifiedBy, Modified) VALUES 
                        (@BudgetGroupXrefConversionActionId, @BudgetGroupId, @ConversionActionId, @CreatedBy, @Created, @ModifiedBy, @Modified);";

            rowsChanged = db.Execute(sql, budgetGroupXrefConversionActionHistories, commandTimeout: COMMAND_TIMEOUT);

            if (rowsChanged == 0)
            {
                throw new Exception("INTO Hist.BudgetGroupXrefConversionAction INSERT Failed.");
            }
            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupXrefLocationHistory(List<BudgetGroupXrefLocationHistory> budgetGroupXrefLocationHistories, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefLocationHistories == null || budgetGroupXrefLocationHistories.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"INSERT INTO Hist.BudgetGroupXrefLocation 
                        (BudgetGroupXrefLocationId, BudgetGroupId, LocationId, CreatedBy, Created) VALUES 
                        (@BudgetGroupXrefLocationId, @BudgetGroupId, @LocationId, @CreatedBy, @Created);";

            rowsChanged = db.Execute(sql, budgetGroupXrefLocationHistories, commandTimeout: COMMAND_TIMEOUT);

            if (rowsChanged == 0)
            {
                throw new Exception("Hist.BudgetGroupXrefLocation INSERT Failed.");
            }
            return rowsChanged;
        }

        internal int InsertIntoBudgetGroupXrefPlacementHistory(List<BudgetGroupXrefPlacementHistory> budgetGroupXrefPlacementHistories, IDbConnection db)
        {
            int rowsChanged = 0;
            if (budgetGroupXrefPlacementHistories == null || budgetGroupXrefPlacementHistories.Count == 0)
            {
                return rowsChanged;
            }

            string sql = @"INSERT INTO Hist.BudgetGroupXrefPlacement 
                        (BudgetGroupXrefPlacementId, BudgetGroupId, PlacementId, CreatedBy, Created, AddedToHistory) VALUES 
                        (@BudgetGroupXrefPlacementId, @BudgetGroupId, @PlacementId, @CreatedBy, @Created, @AddedToHistory);";

            rowsChanged = db.Execute(sql, budgetGroupXrefPlacementHistories, commandTimeout: COMMAND_TIMEOUT);

            if (rowsChanged == 0)
            {
                throw new Exception("Hist.BudgetGroupXrefPlacement INSERT Failed.");
            }
            return rowsChanged;
        }

        #endregion
    }
}
