﻿using Sightly.DAL.DTO;
using Sightly.DAL.DTO.BudgetGroupStates;
using System;
using System.Collections.Generic;
using System.Data;

namespace Sightly.DAL
{
    public interface IBudgetGroupProposalStore
    {
        BudgetGroupProposal CreateBudgetGroupProposal(BudgetGroupProposal budgetGroupProposal, BudgetGroup budgetGroup,
            Order order, OrderXrefBudgetGroup orderXrefBudgetGroup, BudgetGroupLedger budgetGroupLedger);
        BudgetGroupProposal GetBudgetGroupProposalById(Guid budgetGroupPropsalId, IDbConnection connection = null, bool withChildren = true);
        List<Guid> GetBudgetGroupProposalIdByOrderId(Guid orderId, IDbConnection connection = null, bool withChildren = true);
        List<BudgetGroupProposal> GetBudgetGroupProposalsBy(string column, int? status = null, Guid? user = null, bool withChildren = true);
        BudgetGroupProposal UpdateBudgetGroupProposalStatus(Guid budgetGroupProposalId, int status, Guid modifiedBy, BudgetGroupLedger budgetGroupLedger = null);
        BudgetGroupProposal UpdateBudgetGroupProposal(BudgetGroupProposal budgetGroupProposal, Dictionary<string, List<Guid>> itemsToDelete);

        BudgetGroupProposal CreateOrUpdateBudgetGroupProposal(BudgetGroupProposal budgetGroupProposal, BudgetGroupLedger budgetGroupLedger);

        BudgetGroupProposal OverrideProposalWithLiveBudgetGroup(BudgetGroupProposal budgetGroupProposal, Dictionary<string, List<Guid>> itemsToDelete);
    }
}
