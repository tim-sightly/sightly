﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Sightly.DAL.DTO;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class DoubleVerifyStore : ADataStore, IDoubleVerifyStore
    {
        private readonly ILoggingWrapper _loggingWrapper;

        public DoubleVerifyStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }


        public List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyAccount(Guid accountId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[GetDoubleVerifyKpiStatsDatabyAccount] @AccountId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<DoubleVerifyKpiStatsData>(sql, new { AccountId = accountId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyAdvertiser(Guid advertiserId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[GetDoubleVerifyKpiStatsDatabyAdvertiser] @AdvertiserId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<DoubleVerifyKpiStatsData>(sql, new { AdvertiserId = advertiserId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public List<DoubleVerifyKpiStatsData> GetDoubleVerifyKpiStatsDatabyCampaign(Guid campaignId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[GetDoubleVerifyKpiStatsDatabyCampaignPlusCID] @CampaignId ;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<DoubleVerifyKpiStatsData>(sql, new { CampaignId = campaignId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}