﻿using System;
using System.Collections.Generic;
using System.Data;
using Sightly.DAL.DTO;

namespace Sightly.DAL
{
    public interface INielsenStore
    {
        List<NielsenKpiStatsData> GetNielsenKpiStatsDataByAccount(Guid accountId, IDbConnection connection = null);
        List<NielsenKpiStatsData> GetNielsenKpiStatsDataByAdvertiser(Guid advertiserId, IDbConnection connection = null);
        List<NielsenKpiStatsData> GetNielsenKpiStatsDataByCampaign(Guid campaignId, IDbConnection connection = null);
    }
}