﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Sightly.DAL.DTO;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class StatStore : ADataStore, IStatStore
    {
        private ILoggingWrapper _loggingWrapper;
        
        public StatStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }

        public List<DailyStats> GetDailyDeviceStatsForAdWordsCustomerId(long adWordsCustomerId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    List<DailyStats> results;
                    
                    results = db.Query<DailyStats>("[raw].[GetStatsForCustomerByDate]", new {CustomerId = adWordsCustomerId},
                        commandType: CommandType.StoredProcedure, commandTimeout: DapperTimeOut).ToList();

                    return results;
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }

        }
        
        public List<DailyStats> GetDailyDeviceStatsForCampaign(Guid campaignId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {                    
                    return db.Query<DailyStats>("[tvdb].[GetPacingAndAggregatedStatsFromCampaignPlusCID]", new {CampaignId = campaignId},
                        commandType: CommandType.StoredProcedure, commandTimeout: DapperTimeOut).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }

        }

        public List<DailyStats> GetDailyDeviceStatsForAccount(Guid accountId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {                    
                    return db.Query<DailyStats>("tvdb.GetPacingAndAggregatedStatsFromAccount", new {AccountId = accountId},
                        commandType: CommandType.StoredProcedure, commandTimeout: DapperTimeOut).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }    
        }

        public List<DailyStats> GetDailyDeviceStatsForAdvertiser(Guid advertiserId, IDbConnection connection = null)
        {
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {                    
                    return db.Query<DailyStats>("tvdb.GetPacingAndAggregatedStatsFromAdvertiser", new {AdvertiserId = advertiserId},
                        commandType: CommandType.StoredProcedure, commandTimeout: DapperTimeOut).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex.Message);
                throw;
            }  
        }
    }
}