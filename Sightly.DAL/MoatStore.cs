using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Sightly.DAL.DTO;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class MoatStore : ADataStore, IMoatStore
    {
        private readonly ILoggingWrapper _loggingWrapper;

        public MoatStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }

        public List<MoatKpiStatsData> GetMoatKpiStatsDataByAccount(Guid accountId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[GetMoatKpiStatsDataByAccount] @AccountId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<MoatKpiStatsData>(sql, new { AccountId = accountId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public List<MoatKpiStatsData> GetMoatKpiStatsDataByAdvertiser(Guid advertiserId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[GetMoatKpiStatsDataByAdvertiser] @AdvertiserId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<MoatKpiStatsData>(sql, new { AdvertiserId = advertiserId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public List<MoatKpiStatsData> GetMoatKpiStatsDataByCampaign(Guid campaignId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[GetMoatKpiStatsDataByCampaignPlusCID] @CampaignId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<MoatKpiStatsData>(sql, new { CampaignId = campaignId }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}