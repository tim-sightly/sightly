﻿using System;
using System.Collections.Generic;
using System.Data;
using Sightly.DAL.DTO;

namespace Sightly.DAL
{
    public interface IAlertCentralStore
    {
        List<AdwordCustomer> GetAwCustomersThatAreCurrentlyActive(DateTime statDate, IDbConnection connection = null);
        List<AdwordCustomer> GetAwCustomersThatAreCurrentlyActiveByFlight(DateTime statDate, IDbConnection connection = null);
        List<KpiDefinitionData> GetKpiDefinitionData(long customerId, IDbConnection connection = null);
        AdwordsKpiStatsData GetAdwordsKpiStatsData(long customerId, IDbConnection connection = null);
        AdwordsKpiStatsData GetAdwordsKpiStatsDataByCustomerAndDates(long customerId, DateTime startDate, DateTime endDate, IDbConnection connection = null);
        MoatKpiStatsData GetMoatKpiStatsData(long customerId, IDbConnection connection = null);
        MoatKpiStatsData GetMoatKpiStatsDataByCustomerAndDates(long customerId, DateTime startDate, DateTime endDate, IDbConnection connection = null);
        NielsenKpiStatsData GetNielsenKpiStatsData(long customerId, IDbConnection connection = null);
        NielsenKpiStatsData GetNielsenKpiStatsDataByCustomerAndDates(long customerId, DateTime startDate, DateTime endDate, IDbConnection connection = null);
        DoubleVerifyKpiStatsData GetDoubleVerifyKpiStatsData(long customerId, IDbConnection connection = null);
        DoubleVerifyKpiStatsData GetDoubleVerifyKpiStatsDataByCustomerAndDates(long customerId, DateTime startDate, DateTime endDate, IDbConnection connection = null);
    }
}
