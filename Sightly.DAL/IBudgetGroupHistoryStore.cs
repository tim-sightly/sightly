﻿using System;
using System.Data;
using Sightly.DAL.DTO.BudgetGroupStates;

namespace Sightly.DAL
{
    public interface IBudgetGroupHistoryStore
    {
        int CreateBudgetGroupHistory(BudgetGroupHistory budgetGroupHistory, IDbConnection connection  = null);

        BudgetGroupHistory GetBudgetGroupHistoryById(Guid budgetGroupHistoryId);
    }
}
