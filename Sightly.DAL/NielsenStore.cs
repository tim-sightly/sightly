﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Sightly.DAL.DTO;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class NielsenStore : ADataStore, INielsenStore
    {
        private readonly ILoggingWrapper _loggingWrapper;

        public NielsenStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }

        public List<NielsenKpiStatsData> GetNielsenKpiStatsDataByAccount(Guid accountId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[GetNielsenKpiStatsDataByAccount] @AccountId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<NielsenKpiStatsData>(sql, new { AccountId = accountId }, commandTimeout: DapperTimeOut).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public List<NielsenKpiStatsData> GetNielsenKpiStatsDataByAdvertiser(Guid advertiserId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[GetNielsenKpiStatsDataByAdvertiser] @AdvertiserId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<NielsenKpiStatsData>(sql, new { AdvertiserId = advertiserId }, commandTimeout: DapperTimeOut).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public List<NielsenKpiStatsData> GetNielsenKpiStatsDataByCampaign(Guid campaignId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[GetNielsenKpiStatsDataByCampaignPlusCID] @CampaignId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<NielsenKpiStatsData>(sql, new { CampaignId = campaignId }, commandTimeout: DapperTimeOut).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}