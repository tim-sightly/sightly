using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Sightly.DAL.DTO;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class AlertCentralStore : ADataStore, IAlertCentralStore
    {
        private readonly ILoggingWrapper _loggingWrapper;

        public AlertCentralStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }

        public List<AdwordCustomer> GetAwCustomersThatAreCurrentlyActive(DateTime statDate, IDbConnection connection = null)
        {
            const string sql = @"raw.GetAwCustomersCurrentlyActive @StatDate;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<AdwordCustomer> (sql, new { StatDate = statDate }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public List<AdwordCustomer> GetAwCustomersThatAreCurrentlyActiveByFlight(DateTime statDate, IDbConnection connection = null)
        {
            const string sql = @"raw.GetAwCustomersCurrentlyActiveByFlight @StatDate;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<AdwordCustomer>(sql, new { StatDate = statDate }).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public List<KpiDefinitionData> GetKpiDefinitionData(long customerId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[GetKPIDataFromCustomerWithEmail] @CustomerId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<KpiDefinitionData>(sql, new {CustomerId = customerId}).ToList();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public AdwordsKpiStatsData GetAdwordsKpiStatsData(long customerId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[AdwordsStatsByCustomer] @CustomerId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<AdwordsKpiStatsData>(sql, new { CustomerId = customerId }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public AdwordsKpiStatsData GetAdwordsKpiStatsDataByCustomerAndDates(long customerId, DateTime startDate, DateTime endDate, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[AdwordsStatsByCustomerAndDate] @CustomerId, @StartDate, @EndDate;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<AdwordsKpiStatsData>(
                        sql, 
                        new
                        {
                            CustomerId = customerId,
                            StartDate = startDate,
                            EndDate = endDate
                        }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public MoatKpiStatsData GetMoatKpiStatsData(long customerId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[MoatStatsBuCustomer] @CustomerId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<MoatKpiStatsData>(sql, new { CustomerId = customerId }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public MoatKpiStatsData GetMoatKpiStatsDataByCustomerAndDates(long customerId, DateTime startDate, DateTime endDate, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[MoatStatsBuCustomerAndDate] @CustomerId, @StartDate, @EndDate;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<MoatKpiStatsData>(
                        sql, 
                        new
                        {
                            CustomerId = customerId,
                            StartDate = startDate,
                            EndDate = endDate
                        }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public NielsenKpiStatsData GetNielsenKpiStatsData(long customerId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[NielsenDarByCustomer] @CustomerId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<NielsenKpiStatsData>(sql, new { CustomerId = customerId }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public NielsenKpiStatsData GetNielsenKpiStatsDataByCustomerAndDates(long customerId, DateTime startDate, DateTime endDate, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[NielsenDarByCustomerAndDateRange] @CustomerId, @StartDate, @EndDate;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<NielsenKpiStatsData>(
                        sql, 
                        new
                        {
                            CustomerId = customerId,
                            StartDate = startDate,
                            EndDate = endDate
                        }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public DoubleVerifyKpiStatsData GetDoubleVerifyKpiStatsData(long customerId, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[DoubleVerifyPlacementStatsByCustomer] @CustomerId;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<DoubleVerifyKpiStatsData>(sql, new { CustomerId = customerId }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }

        public DoubleVerifyKpiStatsData GetDoubleVerifyKpiStatsDataByCustomerAndDates(long customerId, DateTime startDate,
            DateTime endDate, IDbConnection connection = null)
        {
            const string sql = @"Execute [raw].[DoubleVerifyPlacementStatsByCustomerAndDate] @CustomerId, @StartDate, @EndDate;";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<DoubleVerifyKpiStatsData>(
                        sql, 
                        new
                        {
                            CustomerId = customerId,
                            StartDate = startDate,
                            EndDate = endDate
                        }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                Console.WriteLine(ex);
                throw;
            }
        }
    }
}