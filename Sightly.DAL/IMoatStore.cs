﻿using System;
using System.Collections.Generic;
using System.Data;
using Sightly.DAL.DTO;

namespace Sightly.DAL
{
    public interface IMoatStore
    {
        List<MoatKpiStatsData> GetMoatKpiStatsDataByAccount(Guid accountId, IDbConnection connection = null);
        List<MoatKpiStatsData> GetMoatKpiStatsDataByAdvertiser(Guid advertiserId, IDbConnection connection = null);
        List<MoatKpiStatsData> GetMoatKpiStatsDataByCampaign(Guid campaignId, IDbConnection connection = null);
    }
}