﻿using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Dapper;
using Sightly.DAL.DTO;
using Sightly.Logging;

namespace Sightly.DAL
{
    public class AccountStore : ADataStore, IAccountStore
    {
        private readonly ILoggingWrapper _loggingWrapper;

        public AccountStore(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
        }
        
        public List<Account> ListAllAccounts(IDbConnection connection = null)
        {
            string sql = @"SELECT AccountId, AccountName, ShortCode, AccountTypeId, AccountMargin FROM [Accounts].[Account]
                          WHERE Deleted = 0
                          ORDER BY AccountName;";
            
            using (IDbConnection db = connection ?? new SqlConnection(ConnString))
            {
                return db.Query<Account>(sql, commandTimeout: DapperTimeOut).ToList();
            }
        }
        
        public List<Advertiser> ListAllAdvertisers(IDbConnection connection = null)
        {
            string sql = @"SELECT AdvertiserId, AccountId, AdvertiserName, AdvertiserCategoryId, AdvertiserMargin, AdvertiserRefCode FROM [Accounts].[Advertiser]
                          WHERE Deleted = 0
                          ORDER BY AdvertiserName;";
            
            using (IDbConnection db = connection ?? new SqlConnection(ConnString))
            {
                return db.Query<Advertiser>(sql, commandTimeout: DapperTimeOut).ToList();
            }
        }
    }
}