﻿using System;
using System.Collections.Generic;
using System.Data;
using Sightly.DAL.DTO;

namespace Sightly.DAL
{
    public interface IBudgetGroupLedgerStore
    {
        BudgetGroupLedger GetBudgetGroupLedgerByBudgetGroup(Guid budgetGroupId, IDbConnection connection = null);
        List<BudgetGroupLedger> GetBudgetGroupLedgerByBudgetGroupProposal(Guid budgetGroupProposalId, IDbConnection connection = null);
        BudgetGroupLedger GetLiveEventByProposalId(Guid budgetGroupProposalId, IDbConnection connection = null);
        long InsertIntoBudgetGroupLedger(BudgetGroupLedger budgetGroupLedger, IDbConnection connection = null);
    }
}