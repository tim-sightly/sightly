﻿using System.Collections.Generic;
using System.Data;
using Sightly.DAL.DTO;

namespace Sightly.DAL
{
    public interface IKpiStore
    {
        List<KpiCondition> GetKpiConditions(IDbConnection connection = null);
        List<KpiMetric> GetKpiMetrics(IDbConnection connection = null);
        List<KpiEditorData> GetKpiEditorDataByCustomerId(long awCustomerId, IDbConnection connection = null);
        int SaveKpiEditorData(long adwordsCustomerId, int kpiMetricId, int equationTypeId, string equationValue, int sequence, IDbConnection connection = null);
        void RemoveKpiEditorData(int adwordsCustomerKpiId, IDbConnection connection = null);
    }
}