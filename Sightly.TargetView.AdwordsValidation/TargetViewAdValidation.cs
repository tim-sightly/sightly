using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Models;
using Sightly.TargetView.AdwordsValidation.Interfaces;

namespace Sightly.TargetView.AdwordsValidation
{
    public class TargetViewAdValidation : IAdValidation
    {
        
        public MissingAds TargetViewToAdwordsAdChecker(List<TargetViewAdwordsData> tvData, AwCampaign awData)
        {
            var ads = new MissingAds();
            awData.Ads.Where(a => a.AdState == "enabled").ToList().ForEach(ad =>
            {
                if (!tvData.Exists(tvAd => tvAd.AwAdId == ad.AdId))
                {
                    ads.AdsInAw.Add(new AwAdTvRef {AwAd = ad});
                }

            });

            tvData.ForEach(tvAd =>
            {
                if (awData.Ads.All(awAd => awAd.AdId != tvAd.AwAdId))
                {
                    ads.AdsInTV.Add(tvAd);
                }
            });


            return ads;
        }

        public List<AwAdTvRef> TargetViewAdVideoValidation(List<AwAdTvRef> adsInAw, List<VideoAssetData> tvVideoData)
        {
            adsInAw.ForEach(val =>
            {
                var tvRecord = tvVideoData
                    .FirstOrDefault(tv => tv.VideoAssetName == val.AwAd.Video.VideoName &&
                                          tv.YouTubeId == val.AwAd.Video.VideoId);
                if (tvRecord != null)
                    val.TvVideoAssetId = tvRecord.VideoAssetId;
                else
                    val.AwVideo = val.AwAd.Video;
            });

            return adsInAw;
        }

        public List<AwAdTvRef> TargetViewAdUrlValidation(List<AwAdTvRef> adsInAw, List<TargetViewAdwordsData> tvData)
        {
            adsInAw.Where(a => a.AwAd.AdState == "enabled").ToList().ForEach(ad =>
            {
                var tvRecord =
                    tvData.FirstOrDefault(
                        tv =>
                            //tv.AwAdId == ad.AwAd.AdId && 
                                tv.DestinationUrl == ad.AwAd.DestinationUrl &&
                                tv.DisplayUrl == $"http://www.{ad.AwAd.DisplayUrl}");
                if (tvRecord == null) return;
                ad.TvAdId = tvRecord.AdId;
                ad.TvOrderId = tvRecord.OrderId;
                ad.TvBudgetGroupId = tvRecord.BudgetGroupId;
            });

            return adsInAw;
        }
        
    }
}