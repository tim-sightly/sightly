using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.TargetView.AdwordsValidation.Interfaces
{
    public interface ITargetViewAdwordsValidation
    {
        IEnumerable<AwCampaignStatics> GetCampaignStatisticsForMccCustomer(DateTime statDate, List<AdwordsManagedCustomerTreeNode> childCustomers);
        IEnumerable<AwCampaignOrder> GetCampaignOrderForMccCustomer(DateTime statDate, List<AdwordsManagedCustomerTreeNode> childCustomers);

        IEnumerable<AwCampaign> GetCustomerAndActiveAdsFromAdwords(string customerId);
        IEnumerable<AwCampaign> GetCustomerAndAllAdsFromAdwords(string customerId);

        IEnumerable<TvCampaignAdWordsInfo> GetAdWordsCustomersInfo(List<TvCampaignAdWordsInfo> campaigns);

    }
}