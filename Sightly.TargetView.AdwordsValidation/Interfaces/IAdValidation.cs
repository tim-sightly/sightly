using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.TargetView.AdwordsValidation.Interfaces
{
    public interface IAdValidation
    {
        MissingAds TargetViewToAdwordsAdChecker(List<TargetViewAdwordsData> tvData, AwCampaign awData);
        List<AwAdTvRef> TargetViewAdVideoValidation(List<AwAdTvRef> adsInAw, List<VideoAssetData> tvVideoData);

        List<AwAdTvRef> TargetViewAdUrlValidation(List<AwAdTvRef> adsInAw, List<TargetViewAdwordsData> tvData);
    }
}