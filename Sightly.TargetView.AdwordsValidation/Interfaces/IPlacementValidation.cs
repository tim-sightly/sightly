using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.TargetView.AdwordsValidation.Interfaces
{
    public interface IPlacementValidation
    {
        List<CampaignLabelData> GetCampaignLabelData(long awCustomerId);
    }
}