using System;
using System.Collections.Generic;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;
using Sightly.TargetView.AdwordsValidation.Interfaces;

namespace Sightly.TargetView.AdwordsValidation
{
    public class PlacementValidation : IPlacementValidation
    {
        private readonly ICampaignLabel _campaignLabel;
        

        public PlacementValidation(ICampaignLabel campaignLabel)
        {
            _campaignLabel = campaignLabel;
        }


        public List<CampaignLabelData> GetCampaignLabelData(long awCustomerId)
        {
            var today = DateTime.UtcNow;
            var campLabData =  _campaignLabel.GetCampaignLabelData(awCustomerId.ToString(), today);

            return campLabData;
        }
    }
}