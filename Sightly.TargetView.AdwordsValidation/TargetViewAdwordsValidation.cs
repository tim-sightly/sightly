﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Sightly.Adwords.Utility;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;
using Sightly.TargetView.AdwordsValidation.Interfaces;

namespace Sightly.TargetView.AdwordsValidation
{
    public class TargetViewAdwordsValidation: ITargetViewAdwordsValidation
    {

        private readonly ICampaignPerformance _campaignPerformance;
        private readonly ICustomerAgent _customerAgent;

        public TargetViewAdwordsValidation(ICampaignPerformance campaignPerformance, ICustomerAgent customerAgent)
        {
            _campaignPerformance = campaignPerformance;
            _customerAgent = customerAgent;
        }

        IEnumerable<AwCampaignStatics> ITargetViewAdwordsValidation.GetCampaignStatisticsForMccCustomer(DateTime statDate, List<AdwordsManagedCustomerTreeNode> childCustomers)
        {
            var campStats = new List<AwCampaignStatics>();

            childCustomers.ToList().ForEach(child =>
            {
                var campData = _campaignPerformance.GetCampaignPerformanceData(child.Customer.AwCustomerId.ToString(CultureInfo.InvariantCulture), statDate);
                campData.ForEach(camp =>{
                                            if (camp.Impressions > 0)
                                            {
                                                campStats.Add(new AwCampaignStatics
                                                {
                                                    CampaignId = camp.CampaignId,
                                                    CampaignName = camp.CampaignName,
                                                    CustomerId = child.Customer.AwCustomerId,
                                                    CustomerName = child.Customer.AwCustomerName,
                                                    Impressions = camp.Impressions,
                                                    StatDate = statDate,
                                                    VideoViews = camp.Views
                                                });
                                            }
                });
            });

            return campStats;
        }

        public IEnumerable<AwCampaignOrder> GetCampaignOrderForMccCustomer(DateTime statDate, List<AdwordsManagedCustomerTreeNode> childCustomers)
        {
            var campStats = new List<AwCampaignOrder>();

            childCustomers.ToList().ForEach(child =>
            {
                var campData = _campaignPerformance.GetCampaignPerformanceData(child.Customer.AwCustomerId.ToString(CultureInfo.InvariantCulture), statDate);
                campData.ForEach(camp =>
                {
                    if (camp.Impressions > 0)
                    {
                        campStats.Add(new AwCampaignOrder
                        {
                            CampaignId = camp.CampaignId,
                            CampaignName = camp.CampaignName,
                            CustomerId = child.Customer.AwCustomerId,
                            CustomerName = child.Customer.AwCustomerName,
                            StatDate = statDate
                        });
                    }
                });
            });

            return campStats;
        }

        public IEnumerable<AwCampaign> GetCustomerAndAllAdsFromAdwords(string customerId)
        {
            var awCampaignList = new List<AwCampaign>();
            var campaignService = new AdwordCampaign();
            var campaigns = campaignService.GetCampaignData(customerId);

            campaigns.ForEach(camp => awCampaignList.Add(new AwCampaign
            {
                CampaignId = camp.AwCampaignId,
                CampaignName = camp.AwCampaignName,
                StartDate = camp.StartDate,
                EndDate = camp.EndDate
            }));

            var adService = new AdwordAd();
            var ads = adService.GetAdData(customerId);

            var videoService = new AdwordVideo();
            var videos = videoService.GetVideoData(customerId);
            awCampaignList.ForEach(camp => ads.ToList().ForEach(a =>
            {
                if (camp.CampaignId != a.AwCampaignId) return;
                var specVid = videos.FirstOrDefault(v => v.AdId == a.AdId);

                if (specVid == null) return;
                var vid = new AwVideo
                {
                    VideoId = specVid.VideoId,
                    VideoName = specVid.VideoName,
                    VideoChannelId = specVid.VideoChannelId,
                    VideoDuration = specVid.VideoDuration >= 1L ? specVid.VideoDuration / 1000 : 30L
                };


                camp.Ads.Add(new AwAd
                {
                    AdId = a.AdId,
                    AdName = a.AdName,
                    AdGroupId = a.AdGroupId,
                    AdGroupName = a.AdGroupName,
                    AdState = a.AdState,
                    DestinationUrl = a.DestinationUrl,
                    DisplayUrl = a.DisplayUrl,
                    Video = vid
                });
            }));

            return awCampaignList;
        }

        public IEnumerable<TvCampaignAdWordsInfo> GetAdWordsCustomersInfo(List<TvCampaignAdWordsInfo> campaigns)
        {
            return _customerAgent.GetAdWordsCustomers(campaigns);
        }


        public IEnumerable<AwCampaign> GetCustomerAndActiveAdsFromAdwords(string customerId)
        {
            var awCampaignList = GetCustomerAndAllAdsFromAdwords(customerId);
            foreach (var awCampaign in awCampaignList)
            {
                var removedAds = awCampaign.Ads.Where(x => x.AdState != "enabled").ToList();
                awCampaign.Ads.RemoveAll(r => removedAds.Contains(r));
            }

            return awCampaignList;
        }
    }
}