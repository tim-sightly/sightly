﻿using System;
using Autofac;
using Sightly.IOC;

namespace Sightly.Test.Common
{
    public class DIConfiguration
    {
        private static readonly Lazy<IContainer> Container = new Lazy<IContainer>(() =>
        {
            var builder = new ContainerBuilder();
            (new AutofacConfiguration()).RegisterServices(builder);
            var container = builder.Build();

            return container;
        });

        public static IContainer GetConfiguredContainer()
        {
            return Container.Value;
        }
    }
}
