﻿using System;

namespace Sightly.Tools
{
    public class DateTimeUtility
    {
        public static string GetDateAsString()
        {
            DateTime currentDate = ConvertDateToSpecificTimeZone(DateTime.Now.Date);
            return currentDate.ToString(format: "MM/dd/yyyy");
        }
        public static string GetYesterdayDateAsString()
        {
            DateTime currentDate = ConvertDateToSpecificTimeZone(DateTime.Now.Date.AddDays(-1));
            return currentDate.ToString(format: "MM/dd/yyyy");
        }
        public static DateTime ConvertDateToSpecificTimeZone(DateTime current, string timeZone = "Pacific Standard Time")
        {
            TimeZoneInfo timeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(timeZone);
            return TimeZoneInfo.ConvertTime(current, timeZoneInfo);
        }
        public static DateTime GetDateFromAdWordsString(string value)
        {
            string[] values = value.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
            int year = int.Parse(values[0].Substring(startIndex: 0, length: 4));
            int month = int.Parse(values[0].Substring(startIndex: 4, length: 2));
            int day = int.Parse(values[0].Substring(values[0].Length - 2));

            return new DateTime(year, month, day);
        }
        public static DateTime GetCurrentDate(DateTime date)
        {
            DateTime currentDate = ConvertDateToSpecificTimeZone(date);
            if (currentDate.Hour >= 0 && currentDate.Hour < 17)
            {
                currentDate = currentDate.AddDays(-1).Date;
            }
            else
            {
                currentDate = currentDate.Date;
            }
            return currentDate;
        }
        public static DateTime GetPreviousDate()
        {
            return ConvertDateToSpecificTimeZone(DateTime.Now.Date.AddDays(-1));
        }
        public static string FormatTimestamp(DateTime dateTime)
        {
            return dateTime.ToString("yyyyMMddHHmmssffff");
        }
    }
}

