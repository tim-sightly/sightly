﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.IO;
using CsvHelper;

namespace Sightly.Tools
{
    public class JsonUtility
    {
        public static DataTable JsonStringToTable(string jsonContent)
        {
            return JsonConvert.DeserializeObject<DataTable>(jsonContent);
        }
        public static string JsonToCsv(string jsonContent, string delimiter)
        {
            var csvString = new StringWriter();
            using (var csv = new CsvWriter(csvString))
            {
                csv.Configuration.SkipEmptyRecords = true;
                csv.Configuration.WillThrowOnMissingField = false;
                csv.Configuration.Delimiter = delimiter;

                using (var dt = JsonStringToTable(jsonContent))
                {
                    foreach (DataColumn column in dt.Columns)
                    {
                        csv.WriteField(column.ColumnName);
                    }
                    csv.NextRecord();

                    foreach (DataRow row in dt.Rows)
                    {
                        for (var i = 0; i < dt.Columns.Count; i++)
                        {
                            csv.WriteField(row[i]);
                        }
                        csv.NextRecord();
                    }
                }
            }
            return csvString.ToString();
        }
        public static string GetJTokenValueAsString(string json, string path)
        {
            try
            {
                JObject root = GetJObject(json);
                return root.SelectToken(path).ToString();
            }
            catch
            {
                return null;
            }
        }
        public static JObject GetJObject(JObject jObject, string key)
        {
            return jObject[key] as JObject;
        }
        public static JObject GetJObject(string json, string key = null)
        {
            JObject root = JObject.Parse(json);
            if (key == null)
            {
                return root;
            }
            else
            {
                return root[key] as JObject;
            }
        }
        public static string GetValue(JObject jObject, string key)
        {
            JToken obj = jObject[key];
            if (obj == null)
            {
                return null;
            }
            else
            {
                return jObject[key].ToString();
            }
        }
        public static JArray GetJArray(JObject jObject, string key)
        {
            JToken array = jObject[key];
            if (array == null)
            {
                return null;
            }
            else
            {
                return (JArray)array;
            }
        }
        public static JArray GetJArray(string json, string key)
        {
            JObject root = GetJObject(json);
            return (JArray)root[key];
        }
        public static string ConvertObjectToJArrayString(object obj)
        {
            return JArray.FromObject(obj).ToString();
        }
        public static void WriteJsonFile(object jsonObj, string path)
        {
            string output = JsonConvert.SerializeObject(jsonObj, Formatting.Indented);
            File.WriteAllText(path, output);
        }
    }
}
