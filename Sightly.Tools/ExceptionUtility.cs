﻿using System;
using System.Text;

namespace Sightly.Tools
{
    public class ExceptionUtility
    {
        public static string FormatException(Exception ex)
        {
            var messages = new StringBuilder();
            var rootEx = ex;
            while (rootEx != null)
            {
                messages.AppendLine($"{rootEx.GetType()} ({rootEx.Message})\n\n{rootEx.StackTrace}");
                rootEx = rootEx.InnerException;
            }
            return messages.ToString();
        }
        public static Exception ThrowException(Exception ex)
        {
            return new Exception(FormatException(ex), ex.InnerException);
        }
    }
}
