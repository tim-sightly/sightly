﻿using System.IO;
using System.Text.RegularExpressions;


namespace Sightly.Tools
{
    public class StringUtility
    {
        public static string ReplaceInvalidFileNameChars(string input, string newValue = "_")
        {
            var pattern = new string(Path.GetInvalidFileNameChars());

            var regEx = new Regex(pattern);
            return regEx.Replace(input: input, replacement: newValue);
        }
        public static string ReplaceInvalidPathChars(string input, string newValue = "_")
        {
            var pattern = new string(Path.GetInvalidPathChars());

            var regEx = new Regex("[" + pattern + "]");
            return regEx.Replace(input: input, replacement: newValue);
        }
        public static string ReplaceInvalidExcelChars(string input, string newValue = "_")
        {
            string pattern = "[,']";

            var regEx = new Regex(pattern);
            return regEx.Replace(input: input, replacement: newValue);
        }
        public static string ReplaceString(string input, string oldValue, string newValue = "")
        {
            return input.Replace(oldValue, newValue);
        }
        public static string EscapeComma(string input)
        {
            if (input.Contains(","))
            {
                input = string.Format("\"{0}\"", input);
            }
            return input;
        }
    }
}
