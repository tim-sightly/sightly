﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Sightly.Tools
{
    public class CsvUtility
    {
        public static void CreateCsvFromGenericList<T>(List<T> list, string fileNameWithExtention)
        {
            if(list == null || list.Count == 0) return;
            
            //get the type
            var t = list[0].GetType();
            var newLine = Environment.NewLine;

           // string appPath = System.Reflection.Assembly.GetEntryAssembly().Location;


            var newPath = Regex.Replace(fileNameWithExtention, @"[\[\]\\\^\$\|\?\*\+\(\)\{\}%,:;><!@#&\-\+/]", "", RegexOptions.Compiled);

            using (var sw = new StreamWriter(newPath))
            {
                var o = Activator.CreateInstance(t);
                var props = o.GetType().GetProperties();

                foreach (var pi in props)
                {
                    sw.Write(pi.Name.ToUpper() + ",");
                }
                sw.Write(newLine);
                foreach (var item in list)
                {
                    var specificItem = item;
                    foreach (var whatToWrite in props.Select(propertyInfo => Convert.ToString(specificItem.GetType()
                        .GetProperty(propertyInfo.Name)
                        .GetValue(specificItem, null))
                        .Replace(',', ' ') + ','))
                    {
                        sw.Write(whatToWrite);
                    }
                    sw.Write(newLine);
                }
            }
        }

    }
    public class CsvRow : List<string>
    {
        public string Line { get; set; }
    }
    public class CsvFileWriter : StreamWriter
    {
        public CsvFileWriter(Stream stream)
            : base(stream)
        {
        }

        public CsvFileWriter(string filename)
            : base(filename)
        {
        }

        /// <summary>
        /// Writes a single row to a CSV file.
        /// </summary>
        /// <param name="row">The row to be written</param>
        public void WriteRow(CsvRow row)
        {
            StringBuilder builder = new StringBuilder();
            bool firstColumn = true;
            foreach (string value in row)
            {
                // Add separator if this isn't the first value
                if (!firstColumn)
                    builder.Append(',');
                // Implement special handling for values that contain comma or quote
                // Enclose in quotes and double up any double quotes
                if (value.IndexOfAny(new[] { '"', ',' }) != -1)
                    builder.AppendFormat("\"{0}\"", value.Replace("\"", "\"\""));
                else
                    builder.Append(value);
                firstColumn = false;
            }
            row.Line = builder.ToString();
            WriteLine(row.Line);
        }
    }

    /// <summary>
    /// Class to read data from a CSV file
    /// </summary>
    public class CsvFileReader : StreamReader
    {
        public CsvFileReader(Stream stream)
            : base(stream)
        {
        }

        public CsvFileReader(string filename)
            : base(filename)
        {
        }

        /// <summary>
        /// Reads a row of data from a CSV file
        /// </summary>
        /// <param name="row"></param>
        /// <returns></returns>
        public bool ReadRow(CsvRow row)
        {
            row.Line = ReadLine();
            if (String.IsNullOrEmpty(row.Line))
                return false;

            int pos = 0;
            int rows = 0;

            while (pos < row.Line.Length)
            {
                string value;

                // Special handling for quoted field
                if (row.Line[pos] == '"')
                {
                    // Skip initial quote
                    pos++;

                    // Parse quoted value
                    int start = pos;
                    while (pos < row.Line.Length)
                    {
                        // Test for quote character
                        if (row.Line[pos] == '"')
                        {
                            // Found one
                            pos++;

                            // If two quotes together, keep one
                            // Otherwise, indicates end of value
                            if (pos >= row.Line.Length || row.Line[pos] != '"')
                            {
                                pos--;
                                break;
                            }
                        }
                        pos++;
                    }
                    value = row.Line.Substring(start, pos - start);
                    value = value.Replace("\"\"", "\"");
                }
                else
                {
                    // Parse unquoted value
                    int start = pos;
                    while (pos < row.Line.Length && row.Line[pos] != ',')
                        pos++;
                    value = row.Line.Substring(start, pos - start);
                }

                // Add field to list
                if (rows < row.Count)
                    row[rows] = value;
                else
                    row.Add(value);
                rows++;

                // Eat up to and including next comma
                while (pos < row.Line.Length && row.Line[pos] != ',')
                    pos++;
                if (pos < row.Line.Length)
                    pos++;
            }
            // Delete any unused items
            while (row.Count > rows)
                row.RemoveAt(rows);

            // Return true if any columns read
            return (row.Count > 0);
        }
    }
}
