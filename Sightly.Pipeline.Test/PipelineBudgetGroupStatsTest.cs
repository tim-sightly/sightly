﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.StatsGather.Raw;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;

namespace Sightly.Pipeline.Test
{
    [TestClass]
    public class PipelineBudgetGroupStatsTest
    {
        private const long CustomerId = 7386079498L;
        private static readonly DateTime StatDate = new DateTime(2017, 08, 20);
        private static IContainer Container { get; set; }
        private IRawStatsService _rawStatsService;
        private IAdStatsTransform _adStatsTransform;
        private IAgeGroupStatsTransform _ageGroupStatsTransform;
        private IBudgetGroupStatsTransform _budgetGroupStatsTransform;
        private IBudgetGroupAdStatsTransform _budgetGroupAdStatsTransform;
        private IBudgetGroupAgeStatsTransform _budgetGroupAgeStatsTransform;
        private IBudgetGroupDeviceStatsTransform _budgetGroupDeviceStatsTransform;
        private IBudgetGroupGenderStatsTransform _budgetGroupGenderStatsTransform;
        private IDeviceStatsTransform _deviceStatsTransform;
        private IGenderStatsTransform _genderStatsTransform;
        private IOrderStatsTransform _orderStatsTransform;
        private IStatRepository _statRepository;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _rawStatsService = Container.Resolve<IRawStatsService>();
            _adStatsTransform = Container.Resolve<IAdStatsTransform>();
            _ageGroupStatsTransform = Container.Resolve<IAgeGroupStatsTransform>();
            _budgetGroupStatsTransform = Container.Resolve<IBudgetGroupStatsTransform>();
            _budgetGroupAdStatsTransform = Container.Resolve<IBudgetGroupAdStatsTransform>();
            _budgetGroupAgeStatsTransform = Container.Resolve<IBudgetGroupAgeStatsTransform>();
            _budgetGroupDeviceStatsTransform = Container.Resolve<IBudgetGroupDeviceStatsTransform>();
            _budgetGroupGenderStatsTransform = Container.Resolve<IBudgetGroupGenderStatsTransform>();
            _deviceStatsTransform = Container.Resolve<IDeviceStatsTransform>();
            _genderStatsTransform = Container.Resolve<IGenderStatsTransform>();
            _orderStatsTransform = Container.Resolve<IOrderStatsTransform>();
            _statRepository = Container.Resolve<IStatRepository>();
        }
        [TestMethod]
        public void GatherAllStatsAndSaveOffBudgetGroups()
        {

            var adDevStats = _rawStatsService.GetAndSaveAdDeviceStats(CustomerId, StatDate);
            var ageStats = _rawStatsService.GetAndSaveAgeRangeStats(CustomerId, StatDate);
            var genderStats = _rawStatsService.GetAndSaveGenderStats(CustomerId, StatDate);

            try
            {
                var budgetGroupAdStats = _budgetGroupAdStatsTransform.Execute(adDevStats);
                _statRepository.InsertBudgetGroupAdStats(budgetGroupAdStats);
                var adStats = _adStatsTransform.Execute(budgetGroupAdStats);
                _statRepository.InsertAdStats(adStats);

                var budgetGroupDeviceStats = _budgetGroupDeviceStatsTransform.Execute(adDevStats);
                _statRepository.InsertBudgetGroupDeviceStats(budgetGroupDeviceStats);
                var deviceStats = _deviceStatsTransform.Execute(budgetGroupDeviceStats);
                _statRepository.InsertDeviceStats(deviceStats);

                var budgetGroupStats = _budgetGroupStatsTransform.Execute(adDevStats);
                _statRepository.InsertBudgetGroupStatsOneAtATime(budgetGroupStats);
                var orderStats = _orderStatsTransform.Execute(budgetGroupStats);
                _statRepository.InsertOrderStats(orderStats);

                var budgetGroupAgeStats = _budgetGroupAgeStatsTransform.Execute(ageStats);
                _statRepository.InsertBudgetGroupAgeStats(budgetGroupAgeStats);
                var ageGroupStats = _ageGroupStatsTransform.Execute(budgetGroupAgeStats);
                _statRepository.InsertAgeGroupStats(ageGroupStats);
                Assert.IsNotNull(ageGroupStats);

                var budgetGroupGenderStats = _budgetGroupGenderStatsTransform.Execute(genderStats);
                _statRepository.InsertBudgetGroupGenderStats(budgetGroupGenderStats);
                var newGenderStats = _genderStatsTransform.Execute(budgetGroupGenderStats);
                _statRepository.InsertGenderStats(newGenderStats);
                Assert.IsNotNull(newGenderStats);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }
    }
}
