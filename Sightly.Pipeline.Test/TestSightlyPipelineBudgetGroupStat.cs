﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.StatsGather;
using Sightly.Business.StatsGather.Raw;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;

namespace Sightly.Pipeline.Test
{
    [Ignore]
    [TestClass]
    public class TestSightlyPipelineBudgetGroupStat
    {
        private const long CustomerId = 7386079498L;
        private static readonly DateTime StatDate = new DateTime(2017, 08, 17);
        private static IContainer Container { get; set; }
        private IRawStatsService _rawStatsService;
        private IBudgetGroupStatsTransform _budgetGroupStatsTransform;
        private IStatRepository _statRepository;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _rawStatsService = Container.Resolve<IRawStatsService>();
            _budgetGroupStatsTransform = Container.Resolve<IBudgetGroupStatsTransform>();
            _statRepository = Container.Resolve<IStatRepository>();
        }
        [TestMethod]
        public void GatherStatsAndSaveOffBudgetGroupStat()
        {
            var bgStats = _rawStatsService.GetAndSaveAdDeviceStats(CustomerId, StatDate);

            var newStats = _budgetGroupStatsTransform.Execute(bgStats);
            Assert.IsNotNull(newStats);

            try
            {
                _statRepository.InsertBudgetGroupStatsOneAtATime(newStats);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
    }
}
