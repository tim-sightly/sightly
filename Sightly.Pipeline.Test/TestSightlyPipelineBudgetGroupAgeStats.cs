﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.StatsGather.Raw;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;

namespace Sightly.Pipeline.Test
{
    /// <summary>
    /// Summary description for TestSightlyPipelineBudgetGroupAgeStats
    /// </summary>
    //[Ignore]
    [TestClass]
    public class TestSightlyPipelineBudgetGroupAgeStats
    {
        private const long CustomerId = 1586861474L;
        private static readonly DateTime StatDate = new DateTime(2017, 09, 10);
        private static IContainer Container { get; set; }
        private IRawStatsService _rawStatsService;
        private IBudgetGroupAgeStatsTransform _budgetGroupAgeStatsTransform;
        private IStatRepository _statRepository;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _rawStatsService = Container.Resolve<IRawStatsService>();
            _budgetGroupAgeStatsTransform = Container.Resolve<IBudgetGroupAgeStatsTransform>();
            _statRepository = Container.Resolve<IStatRepository>();
        }
        [TestMethod]
        public void GatherStatsAndSaveOffBudgetGroupTargetedAge()
        {
            var ageStats = _rawStatsService.GetAndSaveAgeRangeStats(CustomerId, StatDate);
            Assert.IsNotNull(ageStats);

            var newStats = _budgetGroupAgeStatsTransform.Execute(ageStats);
            Assert.IsNotNull(newStats);

            try
            {
                _statRepository.InsertBudgetGroupAgeStats(newStats);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
    }
}
