﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;
using System;
using Sightly.Business.StatsGather.Raw;

namespace Sightly.Pipeline.Test
{
   // [Ignore]
    [TestClass]
    public class TestSightlyPipelineBudgetGroupDeviceStats
    {
        private const long CustomerId = 9796651158L;
        private static readonly DateTime StatDate = new DateTime(2017, 09, 11);
        private static IContainer Container { get; set; }
        private IRawStatsService _rawStatsService;
        private IBudgetGroupDeviceStatsTransform _budgetGroupDeviceStatsTransform;
        private IStatRepository _statRepository;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _rawStatsService = Container.Resolve<IRawStatsService>();
            _budgetGroupDeviceStatsTransform = Container.Resolve<IBudgetGroupDeviceStatsTransform>();
            _statRepository = Container.Resolve<IStatRepository>();
        }
        [TestMethod]
        public void GatherStatsAndSaveOffBudgetGroupDevices()
        {
            var adDevices = _rawStatsService.GetAndSaveAdDeviceStats(CustomerId, StatDate);
            Assert.IsNotNull(adDevices);

            var newStats = _budgetGroupDeviceStatsTransform.Execute(adDevices);
            Assert.IsNotNull(newStats);

            try
            {
                _statRepository.InsertBudgetGroupDeviceStats(newStats);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message); 
            }

        }
    }
}
