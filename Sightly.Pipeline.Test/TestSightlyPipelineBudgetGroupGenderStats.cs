﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.StatsGather.Raw;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;

namespace Sightly.Pipeline.Test
{

   //[Ignore]
    [TestClass]
    public class TestSightlyPipelineBudgetGroupGenderStats
    {
        private const long CustomerId = 1586861474L;
        private static readonly DateTime StatDate = new DateTime(2017, 09, 10);
        private static IContainer Container { get; set; }
        private IRawStatsService _rawStatsService;
        private IBudgetGroupGenderStatsTransform _budgetGroupGenderStatsTransform;
        private IStatRepository _statRepository;

        [TestInitialize]
        public void InitializeTest()
        {
        Container = DIConfiguration.GetConfiguredContainer();
        _rawStatsService = Container.Resolve<IRawStatsService>();
        _budgetGroupGenderStatsTransform = Container.Resolve<IBudgetGroupGenderStatsTransform>();
        _statRepository = Container.Resolve<IStatRepository>();
        }

        [TestMethod]
        public void GatherStatsAndSaveOffBudgetGroupTargetedGender()
        {
            var genderStats = _rawStatsService.GetAndSaveGenderStats(CustomerId, StatDate);
            Assert.IsNotNull(genderStats);

            var newStats = _budgetGroupGenderStatsTransform.Execute(genderStats);
            Assert.IsNotNull(newStats);

            try
            {
                _statRepository.InsertBudgetGroupGenderStats(newStats);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
    }
}
