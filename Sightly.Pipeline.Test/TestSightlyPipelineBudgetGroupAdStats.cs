﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using Autofac;
using Sightly.Business.StatsGather.Raw;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;

namespace Sightly.Pipeline.Test
{
    [Ignore]
    [TestClass]
    public class TestSightlyPipelineBudgetGroupAdStats
    {
        private const long CustomerId = 7386079498L;
        private static readonly DateTime StatDate = new DateTime(2017, 08, 17);
        private static IContainer Container { get; set; }
        private IRawStatsService _rawStatsService;
        private IBudgetGroupAdStatsTransform _budgetGroupAdStatsTransform;
        private IStatRepository _statRepository;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _rawStatsService = Container.Resolve<IRawStatsService>();
            _budgetGroupAdStatsTransform = Container.Resolve<IBudgetGroupAdStatsTransform>();
            _statRepository = Container.Resolve<IStatRepository>();
        }

        [TestMethod]
        public void GatherStatsAndSaveOffBudgetGroupAds()
        {
            var adDevices = _rawStatsService.GetAndSaveAdDeviceStats(CustomerId, StatDate);
            Assert.IsNotNull(adDevices);

            var newStats = _budgetGroupAdStatsTransform.Execute(adDevices);
            Assert.IsNotNull(newStats);

            try
            {
                _statRepository.InsertBudgetGroupAdStats(newStats);
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }

        }
    }
}
