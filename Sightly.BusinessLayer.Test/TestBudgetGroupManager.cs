﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.BusinessLayer.Common;

namespace Sightly.BusinessLayer.Test
{
    [TestClass]
    public class TestBudgetGroupManager
    {
        protected readonly Guid userId = Guid.Parse("4D7CC371-647D-4375-A52B-A55000D46A3A");

        private static IContainer Container { get; set; }
        private IBudgetGroupManager _budgetGroupManager;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = Sightly.IOC.Testing.DIConfiguration.GetConfiguredContainer();
            _budgetGroupManager = Container.Resolve<IBudgetGroupManager>();
        }

        [TestMethod]
        public void TestGetById_ReadyStatusHappyPath_ExpectedNotNull()
        {
            var budgetGroupId = Guid.Parse("");
            var results = _budgetGroupManager.GetById(budgetGroupId, userId);

            Assert.IsNotNull(results,"GetById() method call returned a NULL value");
            Assert.AreEqual(budgetGroupId, results.BudgetGroupId, "Expected value does not match with the actual returned value from GetById() method call");
            Assert.IsNotNull(results.BudgetGroupAds, "Returned BudgetGroupAds object is Null");
            Assert.IsNotNull(results.BudgetGroupLocations, "Returned BudgetGroupLocations is Null");
            
        }

        [TestMethod]
        public void TestGetById_WithCompletedOrderStatus_ExpectedNull()
        {
            var budgetGroupId = Guid.Parse("9407E921-8B1F-47AD-A053-99CECA34990FC");
            var results = _budgetGroupManager.GetById(budgetGroupId, userId);

            Assert.IsNotNull(results, "GetById() method call returned a NULL value");
            Assert.AreEqual(budgetGroupId, results.BudgetGroupId, "Expected value does not match with the actual returned value from GetById() method call");
            Assert.IsNotNull(results.BudgetGroupAds, "Returned BudgetGroupAds object is Null");
            Assert.IsNotNull(results.BudgetGroupLocations, "Returned BudgetGroupLocations is Null");

        }

        [TestMethod]
        public void TestGetById_WithLiveOrderStatus_ExpectedNotNull()
        {
            var budgetGroupId = Guid.Parse("92016DBD-D734-4AB5-86FB-A7CA00EA5408");
            var results = _budgetGroupManager.GetById(budgetGroupId, userId);

            Assert.IsNotNull(results, "GetById() method call returned a NULL value");
            Assert.AreEqual(budgetGroupId, results.BudgetGroupId, "Expected value does not match with the actual returned value from GetById() method call");
            Assert.IsNotNull(results.BudgetGroupAds, "Returned BudgetGroupAds object is Null");
            Assert.IsNotNull(results.BudgetGroupLocations, "Returned BudgetGroupLocations is Null");

        }

        [TestMethod]
        public void TestGetAll()
        {

            var results = _budgetGroupManager.GetAll(userId);

            Assert.IsNotNull(results, "GetAll() returned NULL");
       
        }

        [TestMethod]
        public void TestGetByBudgetGroupStatus()
        {
            var budgetGroupStatus = BudgetGroupStatus.Live;

            var results = _budgetGroupManager.GetByBudgetGroupStatus(budgetGroupStatus, userId);

            Assert.IsNotNull(results, "GetByBudgetGroupStatus");
        }
    }
}
