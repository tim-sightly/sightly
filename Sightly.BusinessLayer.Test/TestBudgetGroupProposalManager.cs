﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Sightly.BusinessLayer.Test
{
    [TestClass]
    public class TestBudgetGroupProposalManager
    {
        protected readonly Guid userId = Guid.Parse("4D7CC371-647D-4375-A52B-A55000D46A3A");

        private static IContainer Container { get; set; }
        private IBudgetGroupProposalManager _budgetGroupProposalManager;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = Sightly.IOC.Testing.DIConfiguration.GetConfiguredContainer();
            _budgetGroupProposalManager = Container.Resolve<IBudgetGroupProposalManager>();
        }
       
    }
}
