﻿using System;
using System.Collections.Generic;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupAdStates;

namespace Sightly.BusinessLayer.Test
{
    [TestClass]
    public class BudgetGroupAdMergeTest
    {
        protected readonly Guid userId = Guid.Parse("4D7CC371-647D-4375-A52B-A55000D46A3A");

        private static IContainer Container { get; set; }
        private IBudgetGroupManager _budgetGroupManager;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = Sightly.IOC.Testing.DIConfiguration.GetConfiguredContainer();
            _budgetGroupManager = Container.Resolve<IBudgetGroupManager>();
        }

        //[TestMethod]
        //public void TestMethod1()
        //{
        //    var newBudgetGroupAd = _budgetGroupManager.MergeAds(
        //        BuildOldAds(), 
        //        BuildNewAds(),
        //        Guid.Parse("AF1E5967-BE0E-4317-ADA9-18F781B3E783"), 
        //        Guid.Parse("4D7CC371-647D-4375-A52B-A55000D46A3A"),
        //        DateTime.Now);

        //    Assert.AreSame(1, newBudgetGroupAd.Count);

        //}

        private List<BudgetGroupAd> BuildOldAds()
        {
            var oldAds = new List<BudgetGroupAd>();
            oldAds.Add(new BudgetGroupAd
            {
                BudgetGroupAdId = Guid.Parse("181F3E2A-098D-414E-BD01-8A4BC629A90F"),
                BudgetGroupId = Guid.Parse("AF1E5967-BE0E-4317-ADA9-18F781B3E783"),
                AdId = Guid.Parse("A4ECEC77-5C7F-4297-B6D4-A771017AA39E"),
                Paused = false,
                Deleted = false,
                CreatedBy = Guid.Parse("4D7CC371-647D-4375-A52B-A55000D46A3A"),
                Created = DateTime.Parse("2017-07-24 21:53:29.0700000"),
                ModifiedBy = Guid.Parse("4D7CC371-647D-4375-A52B-A55000D46A3A"),
                Modified = DateTime.Parse("2017-07-24 21:53:29.0700000"),
            });
            return oldAds;
        }

        private List<BudgetGroupAd> BuildNewAds()
        {
            var newAds = new List<BudgetGroupAd>();
            newAds.Add(new BudgetGroupAd
            {
                BudgetGroupId = Guid.Parse("AF1E5967-BE0E-4317-ADA9-18F781B3E783"),
                AdId = Guid.Parse("D3C4FF15-77B5-453A-8182-A77E018B6967"),
                Paused = false,
                Deleted = false,
                CreatedBy = Guid.Parse("4D7CC371-647D-4375-A52B-A55000D46A3A"),
                Created = DateTime.Parse("2017-07-24 21:57:29.0700000"),
                ModifiedBy = Guid.Parse("4D7CC371-647D-4375-A52B-A55000D46A3A"),
                Modified = DateTime.Parse("2017-07-24 21:57:29.0700000"),
            });
            return newAds;
        }
    }
}
