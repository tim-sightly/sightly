﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;

namespace Sightly.BusinessLayer.Test
{
    [TestClass]
    public class BusinessLayerTest
    {
        [Ignore]
        [TestMethod]
        public void MoveProposalToLive_ProposalExists_ReturnBudgetGroup()
        {
            IBudgetGroupLayer businessLayer = new BudgetGroupLayer();
            BudgetGroup budgetGroup = businessLayer.MoveProposalToLive(new Guid("E0547EE8-F580-4333-B8F9-78950FB3ED36"));
            Assert.IsNotNull(budgetGroup);
        }
    }

}
