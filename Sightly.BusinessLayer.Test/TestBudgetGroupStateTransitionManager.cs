﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.BusinessLayer.Exceptions;
using Sightly.DAL;
using Sightly.Logging;


namespace Sightly.BusinessLayer.Test
{
    [TestClass]
    public class TestBudgetGroupStateTransitionManager
    {
        protected readonly Guid userId = Guid.Parse("4D7CC371-647D-4375-A52B-A55000D46A3A");

        private static IContainer Container { get; set; }
        private IBudgetGroupStateTransitionManager _budgetGroupStateTransitionManager;
        private IBudgetGroupProposalManager _budgetGroupProposalManager;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = Sightly.IOC.Testing.DIConfiguration.GetConfiguredContainer();
            _budgetGroupStateTransitionManager = Container.Resolve<IBudgetGroupStateTransitionManager>();
            _budgetGroupProposalManager = Container.Resolve<IBudgetGroupProposalManager>();
        
        }

        [TestMethod]
        public void MoveBudgetGroupProposalToLive_HappyPath_ReturnLiveBudgetGroup()
        {
            var budgetGroupProposalId = Guid.Parse("16F63D34-6271-4E11-A65F-6D5925E314C6");

            var liveBudgetGroup =
                _budgetGroupStateTransitionManager.MakeBudgetGroupProposalReadyForLive(budgetGroupProposalId, userId, "Here is a test from the unit test");

            Assert.IsNotNull(liveBudgetGroup);
        }

        [TestMethod]
        public void MoveBudgetGroupProposalToLive_ProposalDoesNotExist_ThrowException()
        {
            var budgetGroupProposalId = Guid.Parse("16F63D34-6271-4E11-A65F-6D5925E31Z15");
            try
            {
                var liveBudgetGroup =
                    _budgetGroupStateTransitionManager.MakeBudgetGroupProposalReadyForLive(budgetGroupProposalId, userId, "Here is a test from the unit test");

                Assert.Fail("Expected EntityNotFoundException");
            }
            catch (Exception ex)
            {
                Assert.IsTrue(ex is EntityNotFoundException);
            }

        }
    }
}
