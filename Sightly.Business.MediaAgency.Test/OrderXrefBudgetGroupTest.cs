﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Models;
using Sightly.Test.Common;

namespace Sightly.Business.MediaAgency.Test
{
    [TestClass]
    public class OrderXrefBudgetGroupTest
    {
        private static IContainer Container { get; set; }
        private IOrderXrefBudgetGroupHelper _orderXrefBudgetGroupHelper;
        private Guid OrderId = Guid.Parse("DF16421D-3E06-4AB1-A0B2-47239FCB4839");
        private Guid BudgetGroupId = Guid.Parse("05B1DD8A-7CA0-4B64-BDBE-D5CFF8C961ED");



        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _orderXrefBudgetGroupHelper = Container.Resolve<IOrderXrefBudgetGroupHelper>();

        }
        [TestMethod]
        public void OrderXrefBudgetGroupInsert()
        {
            var orderXrefBudgetGroup =  _orderXrefBudgetGroupHelper.CreateOrderXrefBudgetGroup(OrderId, BudgetGroupId);

            Assert.AreEqual(OrderId, orderXrefBudgetGroup.OrderId);
            Assert.AreEqual(BudgetGroupId, orderXrefBudgetGroup.BudgetGroupId);
        }
    }
}
