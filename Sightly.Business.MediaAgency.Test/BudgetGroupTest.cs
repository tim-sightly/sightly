﻿using System;
using Autofac;
using Autofac.Core;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Test.Common;

namespace Sightly.Business.MediaAgency.Test
{
  
    [TestClass]
    public class BudgetGroupTest
    {
        private static IContainer Container { get; set; }
        private  IBudgetGroupHelper _budgetGroupHelper;

        private readonly string BudgetGroupName = "Atlanta - 1000 CUMBERLAND MALL SE SPC 5523 ATLANTA, GA 30339 - English";
        private decimal BudgetGroupBudget = 3400.6M;


        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _budgetGroupHelper = Container.Resolve<IBudgetGroupHelper>();

        }
        [TestMethod]
        public void CreeateBudgetGroup()
        {
            var budgetGroup = _budgetGroupHelper.CreateBudgetGroup(BudgetGroupName, BudgetGroupBudget);

            Assert.AreEqual(BudgetGroupName, budgetGroup.BudgetGroupName);
        }
    }
}
