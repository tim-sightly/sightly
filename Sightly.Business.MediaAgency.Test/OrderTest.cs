﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Test.Common;

namespace Sightly.Business.MediaAgency.Test
{
    [TestClass]
    public class OrderTest
    {
        private const string GoodOrderName = "Lookout Mountain Attractions - Nashville";
        private const string OrderName = "Sprint Now 2017";
        private Guid accountId = Guid.Parse("470335AB-E4D5-4292-8C8E-B89F53D5E6E1");
        private Guid advertiserId = Guid.Parse("3C86549A-705D-4829-934C-720B073DD38C");
        private Guid campaignId = Guid.Parse("A8BFFA1D-5631-4A3B-986C-6E50256C940F");
        private Guid orderStatusId = Guid.Parse("20D17615-CDCF-44BF-8906-F2F3C432BD57");
        private decimal CampaignBudget = 34006.00M;
        private DateTime StartDate = DateTime.Parse("2017,08, 29");
        private DateTime EndDate = DateTime.Parse("2017,09, 07");
        private static IContainer Container { get; set; }
        private ITvOrderHelper _tvOrderHelper;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _tvOrderHelper = Container.Resolve<ITvOrderHelper>();

        }
        [TestMethod]
        public void CheckExistingOrder()
        {
            var goodOrder = _tvOrderHelper.CheckExistanceOfOrderByName(GoodOrderName);

            Assert.AreEqual(GoodOrderName, goodOrder.OrderName);
        }

        [TestMethod]
        public void CheckNonExistingOrder()
        {
            var badOrder = _tvOrderHelper.CheckExistanceOfOrderByName("Flipper Zipper Bezolt GOO Fnangle.");

            Assert.IsNull(badOrder);
        }

        [TestMethod]
        public void CreateOrder()
        {
            var newOrder = _tvOrderHelper.CreateOrderIfNotExisting(OrderName, accountId, advertiserId, campaignId,
                orderStatusId, CampaignBudget, StartDate, EndDate);

            Assert.AreEqual(OrderName, newOrder.OrderName);
        }
    }
}
