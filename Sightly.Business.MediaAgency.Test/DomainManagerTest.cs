﻿using System;
using System.Collections.Generic;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.MediaAgency;
using Sightly.Business.MediaAgency.AwModel;
using Sightly.Models;
using Sightly.Test.Common;

namespace Sightly.Business.MediaAgency.Test
{
    [TestClass]
    public class DomainManagerTest
    {
        private static IContainer Container { get; set; }
        private IDomainManager _domainManager;
        
        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _domainManager = Container.Resolve<IDomainManager>();
        }


        [TestMethod]
        public void RunUpdateForNewAds()
        {
            var customerId = 1477615082L;

            try
            {
                _domainManager.UpdateTvMediaAgencyAds(customerId);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Assert.Fail();
            }
        }


        [TestMethod]
        public void GetDataReadyForImportTest()
        {
            var mediaAgencyImport = GetMediaAgencyImportData();

            _domainManager.CreateTvMediaAgencyDomain(mediaAgencyImport);

            Assert.IsNotNull(mediaAgencyImport); 

        }
        
        private List<MediaAgencyImport> GetMediaAgencyImportData()
        {
            var mediaAgencyImport = new List<MediaAgencyImport>();
            //mediaAgencyImport.Add(new MediaAgencyImport
            //{
            //    AccountName = "Sprint",
            //    AdvertiserName = "Sprint NOW",
            //    CampaignName = "Sprint NOW",
            //    OrderName = "Sprint NOW 2017a",
            //    AwCampaignName = "Atlanta - 1000 CUMBERLAND MALL SE SPC 5523 ATLANTA, GA 30339 - English",
            //    AwCampaignId = 913873675L,
            //    AwCustomerId = 3197581403L,
            //    CampaignBudget = 3400.6M,
            //    StartDate = DateTime.Parse("2017-08-29"),
            //    EndDate = DateTime.Parse("2017-09-07"),
            //    Margin = .12M,
            //    PlacementId = 204135380L,
            //    PlacementName = "Sightly.com_SLY_P_Sprint Now Atlanta Market TrueView In Stream :15_CHD_PI_OLVN_TVW_CRD_ATL_VPR_VID_VID_15_CPV_DS_BHV_IMC_NAUD_A18-49_IASM_N_MENA_ONA_BNA_MANA_MONA_ROT_08/25_09/07_1x1",
            //    TotalBudget = 34006.00M
            //});
            //mediaAgencyImport.Add(new MediaAgencyImport
            //{
            //    AccountName = "Sprint",
            //    AdvertiserName = "Sprint NOW",
            //    CampaignName = "Sprint NOW",
            //    OrderName = "Sprint NOW 2017a",
            //    AwCampaignName = "Atlanta - 1000 CUMBERLAND MALL SE SPC 5523 ATLANTA, GA 30339 - Spanish",
            //    AwCampaignId = 914969163L,
            //    AwCustomerId = 3197581403L,
            //    CampaignBudget = 3400.6M,
            //    StartDate = DateTime.Parse("2017-08-29"),
            //    EndDate = DateTime.Parse("2017-09-07"),
            //    Margin = .12M,
            //    PlacementId = 204134402L,
            //    PlacementName = "Sightly.com_SLY_P_Sprint Now Atlanta Market Hispanic TrueView In Stream :15_CHD_PI_OLVN_TVW_CRD_ATL_VPR_VID_VID_15_CPV_DS_BHV_IMC_NAUD_A18-49_IASM_N_MENA_ONA_BNA_MANA_MONA_ROT_08/25_09/07_1x1",
            //    TotalBudget = 34006.00M
            //});
            //mediaAgencyImport.Add(new MediaAgencyImport
            //{
            //    CampaignName = "Some Name",
            //    AwCampaignName = "Atlanta - 1209 CAROLINE ST STE 110 ATLANTA, GA 30307 - English",
            //    AwCampaignId = 913915420L,
            //    AwCustomerId = 3197581403L,
            //    CampaignBudget = 3400.6M,
            //    StartDate = DateTime.Parse("2017-08-29"),
            //    EndDate = DateTime.Parse("2017-09-07"),
            //    Margin = .12M,
            //    PlacementId = "204135380",
            //    PlacementName = "Sightly.com_SLY_P_Sprint Now Atlanta Market TrueView In Stream :15_CHD_PI_OLVN_TVW_CRD_ATL_VPR_VID_VID_15_CPV_DS_BHV_IMC_NAUD_A18-49_IASM_N_MENA_ONA_BNA_MANA_MONA_ROT_08/25_09/07_1x1",
            //    TotalBudget = 34006.00M
            //});
            //mediaAgencyImport.Add(new MediaAgencyImport
            //{
            //    CampaignName = "Sprint NOW",
            //    AwCampaignName = "Atlanta - 1209 CAROLINE ST STE 110 ATLANTA, GA 30307 - Spanish",
            //    AwCampaignId = 914347163L,
            //    AwCustomerId = 3197581403L,
            //    CampaignBudget = 3400.6M,
            //    StartDate = DateTime.Parse("2017-08-29"),
            //    EndDate = DateTime.Parse("2017-09-07"),
            //    Margin = .12M,
            //    PlacementId = 204134402L,
            //    PlacementName = "Sightly.com_SLY_P_Sprint Now Atlanta Market Hispanic TrueView In Stream :15_CHD_PI_OLVN_TVW_CRD_ATL_VPR_VID_VID_15_CPV_DS_BHV_IMC_NAUD_A18-49_IASM_N_MENA_ONA_BNA_MANA_MONA_ROT_08/25_09/07_1x1",
            //    TotalBudget = 34006.00M
            //});
            //mediaAgencyImport.Add(new MediaAgencyImport
            //{
            //    CampaignName = "Sprint NOW",
            //    AwCampaignName = "Atlanta - 2110 HENDERSON MILL RD STE 22A ATLANTA, GA 30345 - English",
            //    AwCampaignId = 913916059L,
            //    AwCustomerId = 3197581403L,
            //    CampaignBudget = 3400.6M,
            //    StartDate = DateTime.Parse("2017-08-29"),
            //    EndDate = DateTime.Parse("2017-09-07"),
            //    Margin = .12M,
            //    PlacementId = 204135380L,
            //    PlacementName = "Sightly.com_SLY_P_Sprint Now Atlanta Market TrueView In Stream :15_CHD_PI_OLVN_TVW_CRD_ATL_VPR_VID_VID_15_CPV_DS_BHV_IMC_NAUD_A18-49_IASM_N_MENA_ONA_BNA_MANA_MONA_ROT_08/25_09/07_1x1",
            //    TotalBudget = 34006.00M
            //});
            //mediaAgencyImport.Add(new MediaAgencyImport
            //{
            //    CampaignName = "Sprint NOW",
            //    AwCampaignName = "Atlanta - 2110 HENDERSON MILL RD STE 22A ATLANTA, GA 30345 - Spanish",
            //    AwCampaignId = 914986257L,
            //    AwCustomerId = 3197581403L,
            //    CampaignBudget = 3400.6M,
            //    StartDate = DateTime.Parse("2017-08-29"),
            //    EndDate = DateTime.Parse("2017-09-07"),
            //    Margin = .12M,
            //    PlacementId = 204134402L,
            //    PlacementName = "Sightly.com_SLY_P_Sprint Now Atlanta Market Hispanic TrueView In Stream :15_CHD_PI_OLVN_TVW_CRD_ATL_VPR_VID_VID_15_CPV_DS_BHV_IMC_NAUD_A18-49_IASM_N_MENA_ONA_BNA_MANA_MONA_ROT_08/25_09/07_1x1",
            //    TotalBudget = 34006.00M
            //});
            //mediaAgencyImport.Add(new MediaAgencyImport
            //{
            //    CampaignName = "Sprint NOW",
            //    AwCampaignName = "Atlanta - 2955 COBB PKWY SE STE 260 ATLANTA, GA 30339 - English",
            //    AwCampaignId = 914348540L,
            //    AwCustomerId = 3197581403L,
            //    CampaignBudget = 3400.6M,
            //    StartDate = DateTime.Parse("2017-08-29"),
            //    EndDate = DateTime.Parse("2017-09-07"),
            //    Margin = .12M,
            //    PlacementId = 204135380L,
            //    PlacementName = "Sightly.com_SLY_P_Sprint Now Atlanta Market TrueView In Stream :15_CHD_PI_OLVN_TVW_CRD_ATL_VPR_VID_VID_15_CPV_DS_BHV_IMC_NAUD_A18-49_IASM_N_MENA_ONA_BNA_MANA_MONA_ROT_08/25_09/07_1x1",
            //    TotalBudget = 34006.00M
            //});
            //mediaAgencyImport.Add(new MediaAgencyImport
            //{
            //    CampaignName = "Sprint NOW",
            //    AwCampaignName = "Atlanta - 2955 COBB PKWY SE STE 260 ATLANTA, GA 30339 - Spanish",
            //    AwCampaignId = 913917775L,
            //    AwCustomerId = 3197581403L,
            //    CampaignBudget = 3400.6M,
            //    StartDate = DateTime.Parse("2017-08-29"),
            //    EndDate = DateTime.Parse("2017-09-07"),
            //    Margin = .12M,
            //    PlacementId = 204134402L,
            //    PlacementName = "Sightly.com_SLY_P_Sprint Now Atlanta Market Hispanic TrueView In Stream :15_CHD_PI_OLVN_TVW_CRD_ATL_VPR_VID_VID_15_CPV_DS_BHV_IMC_NAUD_A18-49_IASM_N_MENA_ONA_BNA_MANA_MONA_ROT_08/25_09/07_1x1",
            //    TotalBudget = 34006.00M
            //});
            //mediaAgencyImport.Add(new MediaAgencyImport
            //{
            //    CampaignName = "Sprint NOW",
            //    AwCampaignName = "Atlanta - 805 PEACHTREE ST NE STE A ATLANTA, GA 30308 - English",
            //    AwCampaignId = 913919278L,
            //    AwCustomerId = 3197581403L,
            //    CampaignBudget = 3400.6M,
            //    StartDate = DateTime.Parse("2017-08-29"),
            //    EndDate = DateTime.Parse("2017-09-07"),
            //    Margin = .12M,
            //    PlacementId = 204135380L,
            //    PlacementName = "Sightly.com_SLY_P_Sprint Now Atlanta Market TrueView In Stream :15_CHD_PI_OLVN_TVW_CRD_ATL_VPR_VID_VID_15_CPV_DS_BHV_IMC_NAUD_A18-49_IASM_N_MENA_ONA_BNA_MANA_MONA_ROT_08/25_09/07_1x1",
            //    TotalBudget = 34006.00M
            //});
            //mediaAgencyImport.Add(new MediaAgencyImport
            //{
            //    CampaignName = "Sprint NOW",
            //    AwCampaignName = "Atlanta - 805 PEACHTREE ST NE STE A ATLANTA, GA 30308 - Spanish",
            //    AwCampaignId = 913919509L,
            //    AwCustomerId = 3197581403L,
            //    CampaignBudget = 3400.6M,
            //    StartDate = DateTime.Parse("2017-08-29"),
            //    EndDate = DateTime.Parse("2017-09-07"),
            //    Margin = .12M,
            //    PlacementId = 204134402L,
            //    PlacementName = "Sightly.com_SLY_P_Sprint Now Atlanta Market Hispanic TrueView In Stream :15_CHD_PI_OLVN_TVW_CRD_ATL_VPR_VID_VID_15_CPV_DS_BHV_IMC_NAUD_A18-49_IASM_N_MENA_ONA_BNA_MANA_MONA_ROT_08/25_09/07_1x1",
            //    TotalBudget = 34006.00M
            //});

            return mediaAgencyImport;
        }
    }
}
