﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Test.Common;

namespace Sightly.Business.MediaAgency.Test
{
    
    [TestClass]
    public class CampaignTest
    {
        private const string AdvertiserName = "Sprint NOW";
        private const string CampaignName = "Sprint NOW 2017";
        private Guid accountId = Guid.Parse("470335AB-E4D5-4292-8C8E-B89F53D5E6E1");
        private Guid advertiserId = Guid.Parse("3C86549A-705D-4829-934C-720B073DD38C");


        private static IContainer Container { get; set; }
        private IAdvertiserHelper _advertiserHelper;
        private ICampaignHelper _campaignHelper;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _advertiserHelper = Container.Resolve<IAdvertiserHelper>();
            _campaignHelper = Container.Resolve<ICampaignHelper>();

        }

        [TestMethod]
        public void CampaignExistsTest()
        {
            const string seaworldName = "Seaworld";
            var seaworld = _campaignHelper.CheckExistanceOfCampaignByName(seaworldName);
            Assert.AreEqual(seaworldName, seaworld.CampaignName);
        }

        [TestMethod]
        public void CampaignNotExistTest()
        {
            const string badName = "Scezelwopper 5000";
            var testCampaign = _campaignHelper.CheckExistanceOfCampaignByName(badName);
            Assert.IsNull(testCampaign);
        }

        [TestMethod]
        public void AdvertiserGetByNameTest()
        {
            var advertiser = _advertiserHelper.GetAdvertiserByName(AdvertiserName);
            Assert.AreEqual(AdvertiserName, advertiser.AdvertiserName);
        }

        [TestMethod]
        public void CreateCampaignTest()
        {
            var newCampaign = _campaignHelper.CreateCampaignIfNotExisting(AdvertiserName, CampaignName);

            Assert.AreEqual(CampaignName, newCampaign.CampaignName);
        }
    }
}
