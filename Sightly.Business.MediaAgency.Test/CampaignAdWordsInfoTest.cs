﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.MediaAgency.Crud.Interface;
using Sightly.Test.Common;

namespace Sightly.Business.MediaAgency.Test
{
    [TestClass]
    public class CampaignAdWordsInfoTest
    {
        private long customerId = 3197581403L;
        private Guid advertiserId = Guid.Parse("3C86549A-705D-4829-934C-720B073DD38C");
        private Guid campaignId = Guid.Parse("A8BFFA1D-5631-4A3B-986C-6E50256C940F");

        private static IContainer Container { get; set; }
        private ICampaignAdWordsInfoHelper _campaignAdWordsInfoHelper;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _campaignAdWordsInfoHelper = Container.Resolve<ICampaignAdWordsInfoHelper>();

        }
        [TestMethod]
        public void CheckExistingCampaignAdwordsInfo()
        {
            var existingCustomerId = 8778115637L;
            var campaignAdwordsInfo =
                _campaignAdWordsInfoHelper.CheckExistanceOfCampaignAdWordsInfoBycustomerId(existingCustomerId);

            Assert.AreEqual(existingCustomerId, campaignAdwordsInfo.AdWordsCustomerId);
        }

        [TestMethod]
        public void CheckNonExistingCampaignAdwordsInfo()
        {
            var existingCustomerId = 870990037L;
            var campaignAdwordsInfo =
                _campaignAdWordsInfoHelper.CheckExistanceOfCampaignAdWordsInfoBycustomerId(existingCustomerId);

            Assert.IsNull(campaignAdwordsInfo);
        }

        [TestMethod]
        public void CreateCampaignAdwordsInfo()
        {
            var newcampaignAdwordsInfo =
                _campaignAdWordsInfoHelper.CreateCampaignAdWordsInfoIfNotExisting(customerId, advertiserId, campaignId, "stefan@sightly.com");


            Assert.AreEqual(advertiserId, newcampaignAdwordsInfo.AdvertiserId);
            Assert.AreEqual(campaignId, newcampaignAdwordsInfo.CampaignId);
        }
    }
}
