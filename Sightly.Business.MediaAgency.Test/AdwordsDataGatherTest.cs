﻿using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Business.MediaAgency.AwModel;
using Sightly.Test.Common;

namespace Sightly.Business.MediaAgency.Test
{
    [TestClass]
    public class AdwordsDataGatherTest
    {
        private static IContainer Container { get; set; }
        private  IAwDataAssociation _awDataAssociation;

        private const long CustomerId = 3197581403;
        private const long CampaignId = 913873675;


        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _awDataAssociation = Container.Resolve<IAwDataAssociation>();
        }

        [TestMethod]
        public void GetAdwordsCampaignTest()
        {
            var adwordsData = _awDataAssociation.GetAdwordsData(CustomerId, CampaignId);
            Assert.AreEqual(CustomerId, adwordsData.CustomerId);
        }
    }
}
