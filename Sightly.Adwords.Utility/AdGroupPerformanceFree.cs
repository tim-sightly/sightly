﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class AdGroupPerformanceFree : AdWordsBase, IAdGroupPerformanceFree
    {
        public AdGroupPerformanceFree()
        {
            User = new AdWordsUser();
        }

        public AdGroupPerformanceFree(AdWordsUser user)
        {
            User = user;
        }

        public List<AdGroupPerformanceFreeData> GetAdGroupPerformanceFreeData(string customerId, DateTime statDate)
        {
            return GetAdGroupPerformanceFreeData(customerId, statDate, statDate);
        }

        public List<AdGroupPerformanceFreeData> GetAdGroupPerformanceFreeData(string customerId, DateTime startDate, DateTime endDate)
        {
            var config = (AdWordsAppConfig)User.Config;
            config.ClientCustomerId = customerId;

            //var otherUser = new AdWordsUser(config);

            var query = "SELECT CampaignId, CampaignName, AdGroupId, AdGroupName, " +
                        "Clicks, Impressions, Cost, VideoViews, Date " +
                        $"FROM ADGROUP_PERFORMANCE_REPORT DURING {startDate:yyyyMMdd}, {endDate:yyyyMMdd}";

            var adGroupList = new List<AdGroupPerformanceFreeData>();

            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(adGroupList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return adGroupList;
        }

        private static void ParseRow(List<AdGroupPerformanceFreeData> adGroupList, XmlTextReader reader)
        {
            string adGroupName = null;
            string campaignName = null;
            long adGroupId = -1;
            long campaignId = -1,
                impressions = -1,
                views = -1,
                clicks = -1;
            const long defaultVal = -1;
            DateTime? statDate = null;
            decimal? cost = null;

            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "adGroupID":
                        adGroupId = long.Parse(reader.Value);
                        break;
                    case "adGroup":
                        adGroupName = reader.Value;
                        break;
                    case "campaignID":
                        campaignId = long.Parse(reader.Value);
                        break;
                    case "campaign":
                        campaignName = reader.Value;
                        break;
                    case "day":
                        statDate = DateTime.Parse(reader.Value);
                        break;
                    case "impressions":
                        impressions = long.Parse(reader.Value);
                        break;
                    case "views":
                        views = long.Parse(reader.Value);
                        break;
                    case "cost":
                        cost = decimal.Parse(reader.Value);
                        break;
                    case "clicks":
                        clicks = long.Parse(reader.Value);
                        break;
                }

            }

            if (adGroupId != defaultVal && adGroupName != null && campaignId != defaultVal && campaignName != string.Empty &&
                impressions != defaultVal && views != defaultVal && clicks != defaultVal && statDate != null && cost.HasValue )
            {
                adGroupList.Add(new AdGroupPerformanceFreeData()
                {
                    AdGroupId = adGroupId,
                    AdGroupName = adGroupName,
                    CampaignId = campaignId,
                    CampaignName = campaignName,
                    StatDate = statDate.Value,
                    Impressions = impressions,
                    Views = views,
                    Cost = cost.Value,
                    Clicks = clicks
                });
            }
        }
    }
}

