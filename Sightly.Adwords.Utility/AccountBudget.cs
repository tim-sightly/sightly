﻿using System;
using System.Collections.Generic;
using System.Linq;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class AccountBudget : AdWordsBase, IAccountBudget
    {
        public AccountBudget()
        {
            User = new AdWordsUser();
        }

        public AccountBudget(AdWordsUser user)
        {
            User = user;
        }

        public List<AccountBudgetData> GetCustomerAccountBudgets()
        {
            var accountBudgetDataList = new List<AccountBudgetData>();
            try
            {
                var budgetOrderService =
                    (BudgetOrderService) User.GetService(AdWordsService.v201806.BudgetOrderService);

                var selector = new Selector
                {
                    fields = new string[]
                    {
                        Google.Api.Ads.AdWords.v201806.BudgetOrder.Fields.EndDateTime,
                        Google.Api.Ads.AdWords.v201806.BudgetOrder.Fields.Id,
                        Google.Api.Ads.AdWords.v201806.BudgetOrder.Fields.SpendingLimit,
                        Google.Api.Ads.AdWords.v201806.BudgetOrder.Fields.StartDateTime,
                        Google.Api.Ads.AdWords.v201806.BudgetOrder.Fields.TotalAdjustments
                    }
                };

                BudgetOrderPage page = budgetOrderService.get(selector);

                if (page != null && page.entries != null)
                {
                    page.entries.ToList().ForEach(budgetOrder =>
                    {
                        accountBudgetDataList.Add(new AccountBudgetData(budgetOrder));
                    });
                }
                return accountBudgetDataList.OrderByDescending(ab => ab.Id).ToList();
            }
            catch
            {
                return null;
            }


        }
    }
}