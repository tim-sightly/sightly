using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class GeoPerformance : AdWordsBase, IGeoPerformance
    {
        public GeoPerformance()
        {
            User = new AdWordsUser();
        }


        public GeoPerformance(AdWordsUser user)
        {
            User = user;
        }

        public List<GeoPerformanceData> GetGeoPerformanceData(string customerId, DateTime statDate)
        {
            return GetGeoPerformanceData(customerId, statDate, statDate);
        }
        public List<GeoPerformanceData> GetGeoPerformanceData(string customerId, DateTime startDate, DateTime endDate)
        {
            var config = (AdWordsAppConfig) User.Config;
            config.ClientCustomerId = customerId;

            //var otherUser = new AdWordsUser(config);

            var query = "SELECT CampaignId, CampaignName, AdGroupId, AdGroupName, Date, " +
                        "CountryCriteriaId, CityCriteriaId, RegionCriteriaId, MetroCriteriaId, " +
                        "MostSpecificCriteriaId, AverageCpc, AverageCpm, VideoViews, Clicks, " +
                        "Conversions, ViewThroughConversions, Cost, Impressions, CostPerAllConversion, " +
                        "CostPerConversion, CostPerConvertedClick, ValuePerAllConversion, ValuePerConversion, ValuePerConvertedClick " +
                        $"FROM GEO_PERFORMANCE_REPORT DURING {startDate:yyyyMMdd}, {endDate:yyyyMMdd}";

            var geoList = new List<GeoPerformanceData>();

            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(geoList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return geoList;
        }

        private static void ParseRow(List<GeoPerformanceData> geoList, XmlTextReader reader)
        {
            string adGroupName = null;
            string campaignName = null;
            const long defaultVal = -1;
            long adGroupId = -1,
                campaignId = -1,
                countryCriteriaId = -1,
                cityCriteriaId = -1,
                regionCriteriaId = -1,
                metroCriteriaId = -1,
                mostSpecificCriteriaId = -1,
                impressions = -1,
                videoViews = -1,
                viewThroughConversions = -1,
                clicks = -1;
            DateTime? statDate = null;
            decimal? averageCpc = null,
                averageCpm = null,
                conversions = null,
                cost = null,
                costPerAllConversion = null,
                costPerConverstion = null,
                costPerConvertedClick = null,
                valuePerAllConverstion = null,
                valuePerConversion = null,
                valuePerConvertedClick = null;
            const string emptyCriteria = "--";
            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "adGroupID":
                        adGroupId = long.Parse(reader.Value);
                        break;
                    case "adGroup":
                        adGroupName = reader.Value;
                        break;
                    case "campaignID":
                        campaignId = long.Parse(reader.Value);
                        break;
                    case "campaign":
                        campaignName = reader.Value;
                        break;
                    case "countryTerritory":
                        countryCriteriaId = reader.Value != emptyCriteria ?  long.Parse(reader.Value) : 0;
                        break;
                    case "city":
                        cityCriteriaId = reader.Value != emptyCriteria ? long.Parse(reader.Value) : 0;
                        break;
                    case "region":
                        regionCriteriaId = reader.Value != emptyCriteria ? long.Parse(reader.Value) : 0;
                        break;
                    case "metroArea":
                        metroCriteriaId = reader.Value != emptyCriteria ? long.Parse(reader.Value) : 0;
                        break;
                    case "mostSpecificLocation":
                        mostSpecificCriteriaId = reader.Value != emptyCriteria ? long.Parse(reader.Value) : 0;
                        break;
                    case "day":
                        statDate = DateTime.Parse(reader.Value);
                        break;
                    case "impressions":
                        impressions = long.Parse(reader.Value);
                        break;
                    case "views":
                        videoViews = long.Parse(reader.Value);
                        break;
                    case "viewThroughConv":
                        viewThroughConversions = long.Parse(reader.Value);
                        break;
                    case "cost":
                        cost = decimal.Parse(reader.Value);
                        break;
                    case "clicks":
                        clicks = long.Parse(reader.Value);
                        break;
                    case "avgCPC":
                        averageCpc = decimal.Parse(reader.Value);
                        break;
                    case "avgCPM":
                        averageCpm = decimal.Parse(reader.Value);
                        break;
                    case "conversions":
                        conversions = decimal.Parse(reader.Value);
                        break;
                    case "costAllConv":
                        costPerAllConversion = decimal.Parse(reader.Value);
                        break;
                    case "costConv":
                        costPerConverstion = decimal.Parse(reader.Value);
                        break;
                    case "costConvertedClick":
                        costPerConvertedClick = decimal.Parse(reader.Value);
                        break;
                    case "valueAllConv":
                        valuePerAllConverstion = decimal.Parse(reader.Value);
                        break;
                    case "valueConv":
                        valuePerConversion = decimal.Parse(reader.Value);
                        break;
                    case "valueConvertedClick":
                        valuePerConvertedClick = decimal.Parse(reader.Value);
                        break;
                }

            }

            if (adGroupId != defaultVal && adGroupName != null &&
                campaignId != defaultVal && campaignName != string.Empty && statDate.HasValue &&
                impressions != defaultVal && cost.HasValue && clicks != defaultVal && averageCpc.HasValue &&
                averageCpm.HasValue && countryCriteriaId != defaultVal && regionCriteriaId != defaultVal &&
                metroCriteriaId != defaultVal && mostSpecificCriteriaId != defaultVal && cityCriteriaId != defaultVal &&
                conversions.HasValue && viewThroughConversions != defaultVal && videoViews != defaultVal &&
                costPerAllConversion.HasValue && costPerConverstion.HasValue && costPerConvertedClick.HasValue &&
                valuePerAllConverstion.HasValue && valuePerConversion.HasValue && valuePerConvertedClick.HasValue)
            {
                geoList.Add(new GeoPerformanceData()
                {
                    AdGroupId = adGroupId,
                    CampaignId = campaignId,
                    StatDate = statDate.Value,
                    Impressions = impressions,
                    Cost = cost.Value,
                    Clicks = clicks,
                    AverageCpc = averageCpc.Value,
                    AverageCpm = averageCpm.Value,
                    CountryCriteriaId = countryCriteriaId,
                    RegionCriteriaId = regionCriteriaId,
                    MetroCriteriaId = metroCriteriaId,
                    MostSpecificCriteriaId = mostSpecificCriteriaId,
                    CityCriteriaId = cityCriteriaId,
                    Conversions = conversions.Value,
                    ViewThroughConversions = viewThroughConversions,
                    VideoViews = videoViews,
                    CostPerAllConversion = costPerAllConversion.Value,
                    CostPerConversion = costPerConverstion.Value,
                    CostPerConversionClick = costPerConvertedClick.Value,
                    ValuePerAllConversion = valuePerAllConverstion.Value,
                    ValuePerConversion = valuePerConversion.Value,
                    ValuePerConvertedClick = valuePerConvertedClick.Value

                });
            }
        }
    }
}