﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class AccountHierarchy : AdWordsBase, IAccountHierarchy
    {
        public AccountHierarchy()
        {
            User = new AdWordsUser();
        }
        public AccountHierarchy(AdWordsUser user)
        {
            User = user;
        }

        public Dictionary<long, AdwordsManagedCustomerTreeNode> GetAccountHierarchy()
        {
            var managedCustomerService =
                (ManagedCustomerService) User.GetService(AdWordsService.v201806.ManagedCustomerService);
            
            var selector = new Selector
            {
                fields = new String[] {ManagedCustomer.Fields.CustomerId, ManagedCustomer.Fields.Name},
                paging = Paging.Default
            };

            var accountHierarchy = new Dictionary<long, AdwordsManagedCustomerTreeNode>();
            //var accountHierarchy = new Dictionary<long, string>();

            //Temporary cache to save links
            var allLinks = new List<ManagedCustomerLink>();

            try
            {
                ManagedCustomerPage page = null;
                do
                {
                    page = managedCustomerService.get(selector);

                    if (page.entries != null)
                    {
                        // Create account tree nodes for each customer.
                        foreach (var customer in page.entries)
                        {
                            var node = new AdwordsManagedCustomerTreeNode
                            {
                                Customer = new AdwordsCustomerData
                                {
                                    AwCustomerId = customer.customerId,
                                    AwCustomerName = customer.name
                                }
                            };
                            if(!accountHierarchy.ContainsKey(customer.customerId))
                            { 
                                accountHierarchy.Add(customer.customerId, node);
                            }
                        }

                        if (page.links != null)
                        {
                            allLinks.AddRange(page.links);
                        }
                    }
                    selector.paging.IncreaseOffset();
                } while (selector.paging.startIndex < page.totalNumEntries);

                // for each link - connect nodes in tree
                foreach (var link in allLinks)
                {
                    var managerNode = accountHierarchy[link.managerCustomerId];
                    var childNode = accountHierarchy[link.clientCustomerId];
                    childNode.ParentNode = managerNode;
                    if (managerNode != null)
                    {
                        managerNode.ChildCustomers.Add(childNode);
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to retrieve the Account Hierarchy.", e);
            }
            return accountHierarchy;
        }

        public long FindSpecificCustomerByName(string customerName)
        {
            Dictionary<long, AdwordsManagedCustomerTreeNode> returnedValues = GetAccountHierarchy();
            return returnedValues.FirstOrDefault(rv => rv.Value.Customer.AwCustomerName== customerName).Key;
        }

        public string GetAwordsCustomerHierachy()
        {
            Dictionary<long, AdwordsManagedCustomerTreeNode> returnedValues = GetAccountHierarchy();
            //Fill the root account node in the tree
            AdwordsManagedCustomerTreeNode rootNode = returnedValues.Values.FirstOrDefault(node => node.ParentNode == null);

            return rootNode != null ? rootNode.ToTreeString(0, new StringBuilder()).ToString() : "Account Hierarchy is temporarally no available, please try again.";
        }

        public List<long> GetAllSightlyAdwordsCustomerIds()
        {
             var allCustomers = new List<long>();
            var returnedValues = GetAccountHierarchy();
            allCustomers.AddRange(GetAllChildren(returnedValues.Values.FirstOrDefault(node => node.ParentNode == null), new List<long>()));

            return allCustomers;
        }


        public List<long> GetAllCustomersFromMCCList(List<long> mccCustomerList)
        {
            var allCustomers = new List<long>();
            mccCustomerList.ForEach(customer =>
            {
                allCustomers.AddRange(GetAllCustomersFromMCC(customer));
            });

            return allCustomers;
        }

        public List<long> GetAllCustomersFromMCC(long mccCustomerId)
        {
            var customers = new List<long>();
            var returnedValues = GetAdwordsCustomerByMcc(mccCustomerId);
            customers.AddRange(GetAllChildren(returnedValues.FirstOrDefault(), new List<long>()));

            return customers;
        }

        private static List<long> GetAllChildren(AdwordsManagedCustomerTreeNode oId, ICollection<long> list)
        {
            list.Add(oId.Customer.AwCustomerId);
            oId.ChildCustomers.ForEach(child =>
            {
                if (child.Customer.AwCustomerId != oId.Customer.AwCustomerId)
                {
                    GetAllChildren(child, list);
                }
            });
            return list.ToList();

        }
        

        public IEnumerable<AdwordsManagedCustomerTreeNode> GetAdwordsCustomerByMcc(long mccCustomerId)
        {
            var getAllCustomer = GetAccountHierarchy();
            IEnumerable<AdwordsManagedCustomerTreeNode> specificMccCustomer =
                getAllCustomer.Values.Where(node => node.Customer.AwCustomerId == mccCustomerId);
            if (specificMccCustomer == null)
            {
                throw new Exception("Customers Hierarch was not found or Adwords failed. Please try again");
            }
            return specificMccCustomer.ToList();
        }

        
    }
}
