﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Newtonsoft.Json;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class CampaignLabel: AdWordsBase, ICampaignLabel
    {
        public CampaignLabel()
        {
            User = new AdWordsUser();
        }

        public CampaignLabel(AdWordsUser user)
        {
            User = user;
        }

        public List<CampaignLabelData> GetCampaignLabelData(string customerId, DateTime statDate)
        {
            return GetCampaignLabelData(customerId, statDate, statDate);
        }

        public List<CampaignLabelData> GetCampaignLabelData(string customerId, DateTime startDate, DateTime endDate)
        {
            var config = (AdWordsAppConfig) User.Config;
            config.ClientCustomerId = customerId;
            var query = $"SELECT CampaignId, Labels FROM CAMPAIGN_PERFORMANCE_REPORT DURING {startDate:yyyyMMdd}, {endDate:yyyyMMdd}";
            var campaignLabelData = new List<CampaignLabelData>();

            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(campaignLabelData, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return campaignLabelData;
        }

        private static void ParseRow(List<CampaignLabelData> campaignLabelDataList, XmlTextReader reader)
        {
            LabelList labelList = new LabelList();
            long campaignId = -1,
                 defaultValue = -1;

            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "campaignID":
                        campaignId = long.Parse(reader.Value);
                        break;
                    case "labels":
                        if(reader.Value == "--") break;
                        labelList.Label = new List<string>();
                        labelList.Label.AddRange(JsonConvert.DeserializeObject<List<string>>(reader.Value));
                        break;
                }
            }

            if (campaignId != defaultValue && labelList.Label != null)
            {
                campaignLabelDataList.Add(new CampaignLabelData
                {
                    CampaignId = campaignId,
                    Labels = labelList.Label
                });
            }

        }

        public class LabelList
        {
            public List<string> Label { get; set; }
        }
    }
}
