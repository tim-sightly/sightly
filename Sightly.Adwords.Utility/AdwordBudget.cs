﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class AdwordBudget : AdWordsBase, IBudget
    {

        public AdwordBudget()
        {
            User = new AdWordsUser();
        }

        public AdwordBudget(AdWordsUser user)
        {
            User = user;
        }
        public List<AdwordBudgetData> GetCampaignBudgetData(string customerId)
        {
            var config = (AdWordsAppConfig)User.Config;
            config.ClientCustomerId = customerId;

            //var otherUser = new AdWordsUser(config);

            var query = "SELECT AssociatedCampaignId, BudgetId, Amount " +
                "FROM BUDGET_PERFORMANCE_REPORT";

            var budgetList = new List<AdwordBudgetData>();

            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(budgetList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return budgetList;

        }


        private static void ParseRow(List<AdwordBudgetData> campaignList, XmlTextReader reader)
        {
            long campaignId = 0;
            long budgetId = 0;
            double amount = 0.00;

            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "campaignID":
                        campaignId = long.Parse(reader.Value);
                        break;
                    case "budgetID":
                        budgetId = long.Parse(reader.Value);
                        break;
                    case "budget":
                        amount = double.Parse(reader.Value);
                        break;
                }

            }

            if (campaignId != 0 && budgetId != 0 && amount != 0.00)
            {
                campaignList.Add(new AdwordBudgetData
                {
                    AwCampaignId = campaignId,
                    BudgetId = budgetId,
                    Amount = amount
                });
            }
        }
    }
}