using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class AdwordVideo : AdWordsBase, IVideo
    {

        public AdwordVideo()
        {
            User = new AdWordsUser();
        }

        public AdwordVideo(AdWordsUser user)
        {
            User = user;
        }

        public List<AdwordVideoData> GetVideoData(string customerId)
        {
            
            var endDate = DateTime.UtcNow;
            var startDate = endDate.AddDays(-1);
            return GetVideoData(customerId, startDate, endDate);
        }

        public List<AdwordVideoData> GetVideoData(string customerId, DateTime statDate)
        {
            return GetVideoData(customerId, statDate, statDate);
        }
        public List<AdwordVideoData> GetVideoData(string customerId, DateTime startDate, DateTime endDate)
        {
            var config = (AdWordsAppConfig) User.Config;
            config.ClientCustomerId = customerId;

            var query =
                "SELECT VideoId, VideoTitle, CreativeId, CreativeStatus, AdGroupId, AdGroupName, CampaignId, CampaignName, " +
                $" VideoDuration, VideoChannelId FROM VIDEO_PERFORMANCE_REPORT DURING {startDate:yyyyMMdd}, {endDate:yyyyMMdd}";

            var videoList = new List<AdwordVideoData>();

            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(videoList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return videoList;
        }

        private void ParseRow(List<AdwordVideoData> videoList, XmlTextReader reader)
        {
            long adId = -1,
                awCampaignId = -1,
                adGroupId = -1,
                videoDuration = -1;
            string videoId = null,
                videoName = null,
                adState = null,
                awCampaignName = null,
                adGroupName = null,
                videoChannelId = null;

            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "adID":
                        adId = long.Parse(reader.Value);
                        break;
                    case "campaignID":
                        awCampaignId = long.Parse(reader.Value);
                        break;
                    case "adGroupID":
                        adGroupId = long.Parse(reader.Value);
                        break;
                    case "videoDuration":
                        videoDuration = long.Parse(reader.Value);
                        break;
                    case "videoId":
                        videoId = reader.Value;
                        break;
                    case "videoTitle":
                        videoName = reader.Value;
                        break;
                    case "adState":
                        adState = reader.Value;
                        break;
                    case "campaign":
                        awCampaignName = reader.Value;
                        break;
                    case "adGroup":
                        adGroupName = reader.Value;
                        break;
                    case "videoChannelId":
                        videoChannelId = reader.Value;
                        break;

                }
            }

            if (adId != -1 && awCampaignId != -1 && adGroupId != -1 && videoDuration != -1 &&
                videoId != null && videoName != null && adState != null && awCampaignName != null && 
                adGroupName != null && videoChannelId != null)
            {
                videoList.Add(new AdwordVideoData
                {
                    AdGroupId = adGroupId,
                    AdGroupName = adGroupName,
                    AdId = adId,
                    AdState = adState,
                    AwCampaignId = awCampaignId,
                    AwCampaignName = awCampaignName,
                    VideoChannelId = videoChannelId,
                    VideoDuration = videoDuration,
                    VideoId = videoId,
                    VideoName = videoName
                });
            }
        }
    }
}