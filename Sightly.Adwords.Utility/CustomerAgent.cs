using System;
using System.Collections.Generic;
using System.Linq;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class CustomerAgent : ICustomerAgent
    {
        private static bool CheckIfResponseEntriesIsEmpty<T>(T[] entries)
        {
            if (entries == null)
                return true;

            return entries.Length <= 0;
        }

        private static long? GetAdWordsCustomerId(IManagedCustomerService customerService, string filterValue)
        {
            var filters = GetCustomerFilters(filterValue);
            var response = customerService.get(filters);

            if (!CheckIfResponseEntriesIsEmpty(response.entries))
                return response.entries.First().customerId;

            return null;
        }

        private static Selector GetCustomerFilters(string filterValue)
        {
            return new Selector
            {
                fields = new String[]
                {
                    ManagedCustomer.Fields.CustomerId,
                    ManagedCustomer.Fields.Name
                },
                paging = Paging.Default,
                predicates = GetCustomerPredicates(filterValue).ToArray()
            };
        }

        private static List<Predicate> GetCustomerPredicates(string filterValue)
        {
            var predicates = new List<Predicate>
            {
                Predicate.Contains(
                    ManagedCustomer.Fields.Name,
                    filterValue
                )
            };


            return predicates;
        }

        private static ManagedCustomerService GetCustomerService()
        {
            var user = new AdWordsUser();
            var customerService = AdWordsService.v201806.ManagedCustomerService;

            return (ManagedCustomerService)user.GetService(customerService);
        }


        public List<TvCampaignAdWordsInfo> GetAdWordsCustomers(List<TvCampaignAdWordsInfo> campaigns)
        {
            var customerService = GetCustomerService();

            campaigns.ForEach(campaign =>
            {
                campaign.AdWordsCustomerId = GetAdWordsCustomerId(customerService, campaign.AdvertiserName.ToLower());

                if (!campaign.AdWordsCustomerId.HasValue)
                {
                    campaign.AdWordsCustomerId =
                        GetAdWordsCustomerId(customerService, campaign.AdvertiserId.ToString());
                }
            });

            return campaigns.Where(c => c.AdWordsCustomerId.HasValue).ToList();
        }
    }
}