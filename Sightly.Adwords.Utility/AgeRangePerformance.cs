﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class AgeRangePerformance : AdWordsBase, IAgeRangePerformance
    {

        public AgeRangePerformance()
        {
            User = new AdWordsUser();
        }
        public AgeRangePerformance(AdWordsUser user)
        {
            User = user;
        }

        public List<AgeRangePerformanceData> GetAgeRangePerformanceData(string customerId, DateTime statDate)
        {
            return GetAgeRangePerformanceData(customerId, statDate, statDate);
        }

        public List<AgeRangePerformanceData> GetAgeRangePerformanceData(string customerId, DateTime startDate, DateTime endDate)
        {
            var config = (AdWordsAppConfig)User.Config;
            config.ClientCustomerId = customerId;
            var query =
                "SELECT AdGroupId, Criteria, CampaignId, CampaignName, Date, Impressions, VideoViews, Cost, Clicks " +
                $"FROM AGE_RANGE_PERFORMANCE_REPORT DURING {startDate:yyyyMMdd}, {endDate:yyyyMMdd}";

            var ageList = new List<AgeRangePerformanceData>();

            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(ageList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return ageList;
        }

        private static void ParseRow(List<AgeRangePerformanceData> ageList, XmlTextReader reader)
        {
            string campaignName = null,
                   criteria = null;
            long adGroupId = -1,
                 campaignId = -1,
                 impressions = -1,
                 views = -1,
                 clicks = -1;
            const long defaultVal = -1;
            DateTime? statDate = null;
            decimal? cost = null;

            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "ageRange":
                        criteria = reader.Value;
                        break;
                    case "adGroupID":
                        adGroupId = long.Parse(reader.Value);
                        break;
                    case "campaignID":
                        campaignId = long.Parse(reader.Value);
                        break;
                    case "campaign":
                        campaignName = reader.Value;
                        break;
                    case "day":
                        statDate = DateTime.Parse(reader.Value);
                        break;
                    case "impressions":
                        impressions = long.Parse(reader.Value);
                        break;
                    case "views":
                        views = long.Parse(reader.Value);
                        break;
                    case "cost":
                        cost = decimal.Parse(reader.Value);
                        break;
                    case "clicks":
                        clicks = long.Parse(reader.Value);
                        break;
                }

            }

            if (adGroupId != defaultVal && criteria != null && campaignId != defaultVal && campaignName != null && impressions != defaultVal && 
                views != defaultVal && clicks != defaultVal && statDate != null )
            {
                ageList.Add(new AgeRangePerformanceData
                {
                    AdGroupId = adGroupId,
                    Criteria = criteria,
                    CampaignId = campaignId,
                    StatDate = statDate.Value,
                    CampaignName = campaignName,
                    Impressions = impressions,
                    Views = views,
                    Cost = cost.Value,
                    Clicks = clicks
                });
            }
        }
    }
}