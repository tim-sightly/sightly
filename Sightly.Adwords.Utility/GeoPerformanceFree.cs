﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class GeoPerformanceFree : AdWordsBase, IGeoPerformanceFree
    {
        public GeoPerformanceFree()
        {
            User = new AdWordsUser();
        }


        public GeoPerformanceFree(AdWordsUser user)
        {
            User = user;
        }

        public List<GeoPerformanceFreeData> GetGeoPerformanceFreeData(string customerId, DateTime statDate)
        {
            return GetGeoPerformanceFreeData(customerId, statDate, statDate);
        }
        public List<GeoPerformanceFreeData> GetGeoPerformanceFreeData(string customerId, DateTime startDate, DateTime endDate)
        {
            var config = (AdWordsAppConfig)User.Config;
            config.ClientCustomerId = customerId;

            var query = "SELECT CampaignId, CampaignName, AdGroupId, AdGroupName, Date, " +
                        "CountryCriteriaId, CityCriteriaId, RegionCriteriaId, MetroCriteriaId, " +
                        "MostSpecificCriteriaId, VideoViews, Clicks, Cost, Impressions " +
                        $"FROM GEO_PERFORMANCE_REPORT DURING {startDate:yyyyMMdd}, {endDate:yyyyMMdd}";

            var geoList = new List<GeoPerformanceFreeData>();

            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(geoList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return geoList;
        }

        private static void ParseRow(ICollection<GeoPerformanceFreeData> geoList, XmlReader reader)
        {
            string adGroupName = null;
            string campaignName = null;
            const long defaultVal = -1;
            long adGroupId = -1,
                campaignId = -1,
                countryCriteriaId = -1,
                cityCriteriaId = -1,
                regionCriteriaId = -1,
                metroCriteriaId = -1,
                mostSpecificCriteriaId = -1,
                impressions = -1,
                videoViews = -1,
                clicks = -1;
            DateTime? statDate = null;
            decimal? cost = null;
            const string emptyCriteria = "--";
            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "adGroupID":
                        adGroupId = long.Parse(reader.Value);
                        break;
                    case "adGroup":
                        adGroupName = reader.Value;
                        break;
                    case "campaignID":
                        campaignId = long.Parse(reader.Value);
                        break;
                    case "campaign":
                        campaignName = reader.Value;
                        break;
                    case "countryTerritory":
                        countryCriteriaId = reader.Value != emptyCriteria ? long.Parse(reader.Value) : 0;
                        break;
                    case "city":
                        cityCriteriaId = reader.Value != emptyCriteria ? long.Parse(reader.Value) : 0;
                        break;
                    case "region":
                        regionCriteriaId = reader.Value != emptyCriteria ? long.Parse(reader.Value) : 0;
                        break;
                    case "metroArea":
                        metroCriteriaId = reader.Value != emptyCriteria ? long.Parse(reader.Value) : 0;
                        break;
                    case "mostSpecificLocation":
                        mostSpecificCriteriaId = reader.Value != emptyCriteria ? long.Parse(reader.Value) : 0;
                        break;
                    case "day":
                        statDate = DateTime.Parse(reader.Value);
                        break;
                    case "impressions":
                        impressions = long.Parse(reader.Value);
                        break;
                    case "views":
                        videoViews = long.Parse(reader.Value);
                        break;
                    case "cost":
                        cost = decimal.Parse(reader.Value);
                        break;
                    case "clicks":
                        clicks = long.Parse(reader.Value);
                        break;
                }

            }

            if (adGroupId != defaultVal && adGroupName != null &&
                campaignId != defaultVal && campaignName != string.Empty && statDate.HasValue &&
                impressions != defaultVal && cost.HasValue && clicks != defaultVal && countryCriteriaId != defaultVal && 
                regionCriteriaId != defaultVal && metroCriteriaId != defaultVal && mostSpecificCriteriaId != defaultVal && 
                cityCriteriaId != defaultVal && videoViews != defaultVal)
            {
                geoList.Add(new GeoPerformanceFreeData()
                {
                    AdGroupId = adGroupId,
                    AdGroupName = adGroupName,
                    CampaignId = campaignId,
                    CampaignName = campaignName,
                    StatDate = statDate.Value,
                    Impressions = impressions,
                    Cost = cost.Value,
                    Clicks = clicks,
                    CountryCriteriaId = countryCriteriaId,
                    RegionCriteriaId = regionCriteriaId,
                    MetroCriteriaId = metroCriteriaId,
                    MostSpecificCriteriaId = mostSpecificCriteriaId,
                    CityCriteriaId = cityCriteriaId,
                    VideoViews = videoViews
                });
            }
        }
    }
}