using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class GenderPerformanceFree : AdWordsBase, IGenderPerformanceFree
    {
        public GenderPerformanceFree()
        {
            User = new AdWordsUser();
        }

        public GenderPerformanceFree(AdWordsUser user)
        {
            User = user;
        }

        public List<GenderPerformanceFreeData> GetGenderPerformanceFreeData(string customerId, DateTime statDate)
        {
            return GetGenderPerformanceFreeData(customerId, statDate, statDate);
        }

        public List<GenderPerformanceFreeData> GetGenderPerformanceFreeData(string customerId, DateTime startDate, DateTime endDate)
        {
            var config = (AdWordsAppConfig)User.Config;
            config.ClientCustomerId = customerId;

            var query = "SELECT AdGroupId, CampaignId, Criteria, Impressions, VideoViews, Cost, Clicks, Date " +
                        $"FROM GENDER_PERFORMANCE_REPORT DURING {startDate:yyyyMMdd}, {endDate:yyyyMMdd}";

            var genderPerformanceFreeList = new List<GenderPerformanceFreeData>();
            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());
            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(genderPerformanceFreeList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return genderPerformanceFreeList;
        }

        private static void ParseRow(ICollection<GenderPerformanceFreeData> adList, XmlReader reader)
        {
            string genderName = null;
            long adGroupId = -1,
                campaignId = -1,
                impressions = -1,
                views = -1,
                clicks = -1;
            const long defaultVal = -1;
            DateTime? statDate = null;
            decimal? cost = null;

            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "gender":
                        genderName = reader.Value;
                        break;
                    case "campaignID":
                        campaignId = long.Parse(reader.Value);
                        break;
                    case "adGroupID":
                        adGroupId = long.Parse(reader.Value);
                        break;
                    case "day":
                        statDate = DateTime.Parse(reader.Value);
                        break;
                    case "impressions":
                        impressions = long.Parse(reader.Value);
                        break;
                    case "views":
                        views = long.Parse(reader.Value);
                        break;
                    case "cost":
                        cost = decimal.Parse(reader.Value);
                        break;
                    case "clicks":
                        clicks = long.Parse(reader.Value);
                        break;
                }

            }

            if (genderName != null && campaignId != defaultVal && adGroupId != defaultVal && impressions != defaultVal &&
                views != defaultVal && clicks != defaultVal && statDate != null && cost.HasValue)
            {
                adList.Add(new GenderPerformanceFreeData
                {
                    CampaignId = campaignId,
                    AdGroupId = adGroupId,
                    GenderName = genderName,
                    StatDate = statDate.Value,
                    Impressions = impressions,
                    Views = views,
                    Cost = cost.Value,
                    Clicks = clicks
                });
            }
        }
    }
}