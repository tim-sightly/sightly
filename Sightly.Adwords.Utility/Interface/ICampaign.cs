﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface ICampaign
    {
        List<AdwordsCampaignData> GetCampaignData(string customerId);
        List<AdwordsCampaignData> GetCampaignDataForDate(string customerId, DateTime statDate);
        long GetCampaignIdFromCampaignName(string name);
    }
}