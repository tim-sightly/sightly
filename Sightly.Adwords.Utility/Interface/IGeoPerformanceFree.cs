using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IGeoPerformanceFree
    {
        List<GeoPerformanceFreeData> GetGeoPerformanceFreeData(string customerId, DateTime statDate);
        List<GeoPerformanceFreeData> GetGeoPerformanceFreeData(string customerId, DateTime startDate, DateTime endDate);
    }
}