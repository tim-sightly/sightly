﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IAccountBudget
    {
        List<AccountBudgetData> GetCustomerAccountBudgets();
    }
}
