﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IAccountHierarchy 
    {
        Dictionary<long, AdwordsManagedCustomerTreeNode> GetAccountHierarchy();

        List<long> GetAllSightlyAdwordsCustomerIds();
        List<long> GetAllCustomersFromMCCList(List<long> mccCustomerList);
        List<long> GetAllCustomersFromMCC(long mccCustomerId);
        long FindSpecificCustomerByName(string customerName);
        IEnumerable<AdwordsManagedCustomerTreeNode> GetAdwordsCustomerByMcc(long mccCustomerId);
    }
}