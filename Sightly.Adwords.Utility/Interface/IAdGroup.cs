﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IAdGroup
    {
        List<AdwordAdGroupData> GetAdGroupData(string customerId);
    }
}