using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IAgeRangePerformanceFree
    {
        List<AgeRangePerformanceFreeData> GetAgeRangePerformanceFreeData(string customerId, DateTime statDate);
        List<AgeRangePerformanceFreeData> GetAgeRangePerformanceFreeData(string customerId, DateTime startDate, DateTime endDate);
    }
}