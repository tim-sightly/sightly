using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IGenderPerformanceFree
    {
        List<GenderPerformanceFreeData> GetGenderPerformanceFreeData(string customerId, DateTime statDate);
        List<GenderPerformanceFreeData> GetGenderPerformanceFreeData(string customerId, DateTime startDate, DateTime endDate);
    }
}