﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IParentalPerformance
    {
        List<ParentalPerformanceData> GetParentalPerformanceData(string customerId, DateTime statDate);
        List<ParentalPerformanceData> GetParentalPerformanceData(string customerId, DateTime startDate, DateTime endDate);
    }
}