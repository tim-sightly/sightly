﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IAgeRangePerformance
    {
        List<AgeRangePerformanceData> GetAgeRangePerformanceData(string customerId, DateTime statDate);
        List<AgeRangePerformanceData> GetAgeRangePerformanceData(string customerId, DateTime startDate, DateTime endDate);
    }
}