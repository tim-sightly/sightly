﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IAdGroupPerformanceFree
    {
        List<AdGroupPerformanceFreeData> GetAdGroupPerformanceFreeData(string customerId, DateTime statDate);
        List<AdGroupPerformanceFreeData> GetAdGroupPerformanceFreeData(string customerId, DateTime startDate, DateTime endDate);
    }
}
