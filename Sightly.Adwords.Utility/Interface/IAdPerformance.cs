﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IAdPerformance
    {
        List<AdPerformanceData> GetAdPerformanceData(string customerId, DateTime statDate);
        List<AdPerformanceData> GetAdPerformanceData(string customerId, DateTime startDate, DateTime endDate);
    }
}