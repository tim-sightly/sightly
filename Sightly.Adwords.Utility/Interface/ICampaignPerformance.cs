﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface ICampaignPerformance
    {
        List<CampaignPerformanceData> GetCampaignPerformanceData(string customerId, DateTime statDate);

        List<CampaignPerformanceData> GetCampaignPerformanceData(string customerId, DateTime startDate, DateTime endDate);
    }
}