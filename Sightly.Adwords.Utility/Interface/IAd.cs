﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IAd
    {
        List<AdwordAdData> GetAdData(string customerId);
    }
}