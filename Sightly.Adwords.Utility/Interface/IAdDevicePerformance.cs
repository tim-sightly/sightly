﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IAdDevicePerformance
    { 
        List<AdDevicePerformanceData> GetAdDevicePerformanceData(string customerId, DateTime statDate);
        List<AdDevicePerformanceData> GetAdDevicePerformanceData(string customerId, DateTime startDate, DateTime endDate);
    }
}