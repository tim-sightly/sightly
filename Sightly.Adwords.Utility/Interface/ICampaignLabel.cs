﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface ICampaignLabel
    {
        List<CampaignLabelData> GetCampaignLabelData(string customerId, DateTime statDate);
        List<CampaignLabelData> GetCampaignLabelData(string customerId, DateTime startDate, DateTime endDate);
    }
}