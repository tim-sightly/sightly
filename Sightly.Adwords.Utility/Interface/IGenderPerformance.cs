﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IGenderPerformance
    {
        List<GenderPerformanceData> GetGenderPerformanceData(string customerId, DateTime statDate);
        List<GenderPerformanceData> GetGenderPerformanceData(string customerId, DateTime startDate, DateTime endDate);
    }
}