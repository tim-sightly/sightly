﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IBudget
    {
        List<AdwordBudgetData> GetCampaignBudgetData(string customerId);
    }
}