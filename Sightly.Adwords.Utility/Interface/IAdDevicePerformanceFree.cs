﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IAdDevicePerformanceFree
    {
        List<AdDevicePerformanceFreeData> GetAdDevicePerformanceFreeData(string customerId, DateTime statDate);
        List<AdDevicePerformanceFreeData> GetAdDevicePerformanceFreeData(string customerId, DateTime startDate, DateTime endDate);
    }
}
