using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IAdGroupPerformance
    {
        List<AdGroupPerformanceData> GetAdGroupPerformanceData(string customerId, DateTime statDate);
        List<AdGroupPerformanceData> GetAdGroupPerformanceData(string customerId, DateTime startDate, DateTime endDate);
    }
}