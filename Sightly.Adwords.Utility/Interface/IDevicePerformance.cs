﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IDevicePerformance
    {
        List<DevicePerformanceData> GetDevicePerformanceData(string customerId, DateTime statDate);
        List<DevicePerformanceData> GetDevicePerformanceData(string customerId, DateTime startDate, DateTime endDate);
    }
}