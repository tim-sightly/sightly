using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IGeoPerformance
    {
        List<GeoPerformanceData> GetGeoPerformanceData(string customerId, DateTime statDate);
        List<GeoPerformanceData> GetGeoPerformanceData(string customerId, DateTime startDate, DateTime endDate);
    }
}