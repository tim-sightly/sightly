﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Interface
{
    public interface IVideo
    {
        List<AdwordVideoData> GetVideoData(string customerId);
        List<AdwordVideoData> GetVideoData(string customerId, DateTime statDate);
        List<AdwordVideoData> GetVideoData(string customerId, DateTime startDate, DateTime endDate);
    }
}