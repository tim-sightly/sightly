﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class AdwordAdGroup : AdWordsBase, IAdGroup
    {

        public AdwordAdGroup()
        {
            User = new AdWordsUser();
        }

        public AdwordAdGroup(AdWordsUser user, string customerId)
        {
            User = user;
        }

        public List<AdwordAdGroupData> GetAdGroupData(string customerId)
        {
            var config = (AdWordsAppConfig) User.Config;
            config.ClientCustomerId = customerId;

            //var otherUser = new AdWordsUser(config);

            var query = "SELECT CampaignId, CampaignName, AdGroupId, AdGroupName " +
                           "FROM ADGROUP_PERFORMANCE_REPORT DURING YESTERDAY";

            var adGroupList = new List<AdwordAdGroupData>();

            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(adGroupList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return adGroupList;
        }

        private static void ParseRow(List<AdwordAdGroupData> adGroupList, XmlTextReader reader)
        {
            string adGroupName = null;
            string campaignName = null;
            long adGroupId = 0;
            long campaignId = 0;

            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "adGroupID":
                        adGroupId = long.Parse(reader.Value);
                        break;
                    case "adGroup":
                        adGroupName = reader.Value;
                        break;
                    case "campaignID":
                        campaignId = long.Parse(reader.Value);
                        break;
                    case "campaign":
                        campaignName = reader.Value;
                        break;
                }

            }

            if (adGroupId != 0 && adGroupName != null &&
                campaignName != string.Empty && adGroupName != string.Empty)
            {
                adGroupList.Add(new AdwordAdGroupData()
                {
                    AdGroupId = adGroupId,
                    AdGroupName = adGroupName,
                    AwCampaignId =  campaignId,
                    AwCampaignName = campaignName
                });
            }
        }
    }
}
