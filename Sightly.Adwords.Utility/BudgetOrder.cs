﻿using System;
using System.Collections.Generic;
using System.Linq;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class BudgetOrder : AdWordsBase
    {
        public BudgetOrder()
        {
            User = new AdWordsUser();
        }

        public BudgetOrder(AdWordsUser user)
        {
            User = user;
        }

        public List<AdwordsBudgetOrderData> GetBudgetOrders()
        {
            var budgetOrderService = (BudgetOrderService) User.GetService(AdWordsService.v201806.BudgetOrderService);

            var selector = new Selector
            {

                fields = new string[]
                {   
                    Google.Api.Ads.AdWords.v201806.BudgetOrder.Fields.BillingAccountId,
                    Google.Api.Ads.AdWords.v201806.BudgetOrder.Fields.BudgetOrderName,
                    Google.Api.Ads.AdWords.v201806.BudgetOrder.Fields.Id,
                    Google.Api.Ads.AdWords.v201806.BudgetOrder.Fields.SpendingLimit,
                    Google.Api.Ads.AdWords.v201806.BudgetOrder.Fields.PrimaryBillingId
                },
                predicates = new Predicate[]
                {
                    Predicate.Equals(Google.Api.Ads.AdWords.v201806.BudgetOrder.Fields.BillingAccountId, "9303-8888-5544-8975")
                },
                paging = Paging.Default
            };



            var budgetOrders = new List<AdwordsBudgetOrderData>();

            try
            {
                BudgetOrderPage page = null;
                do
                {
                    page = budgetOrderService.get(selector);
                    if (page.entries != null)
                    {
                        budgetOrders.AddRange(page.entries.Select(budgetOrder => new AdwordsBudgetOrderData
                        {
                            BillingAccountId = budgetOrder.billingAccountId, 
                            BudgetOrderName = budgetOrder.budgetOrderName, 
                            Id = budgetOrder.id, 
                            PrimaryBillingId = budgetOrder.primaryBillingId, 
                            SpendingLimit = Convert.ToInt64(budgetOrder.spendingLimit)
                        }));
                    }

                    selector.paging.IncreaseOffset();
                } while (selector.paging.startIndex < page.totalNumEntries);
            }
            catch (Exception e)
            {

                throw new System.ApplicationException("Failed to retrieve the Budget Order.", e);
            }
            return budgetOrders;
        }

        public List<AdwordsBillingAccounts> GetBillingAccounts()
        {
            var budgetOrderService = (BudgetOrderService) User.GetService(AdWordsService.v201806.BudgetOrderService);
            
            var billingAccount = new List<AdwordsBillingAccounts>();

            try
            {
                var bAccounts = budgetOrderService.getBillingAccounts();
                billingAccount.AddRange(bAccounts.Select(bAccount => new AdwordsBillingAccounts
                {
                    CurrencyCode = bAccount.currencyCode, Id = bAccount.id, Name = bAccount.name, PrimaryBillingId = bAccount.primaryBillingId, SecondaryBillingId = bAccount.secondaryBillingId
                }));
            }
            catch (Exception e)
            {

                throw new System.ApplicationException("Failed to retrieve the Bill Accounts.", e);
            }

            return billingAccount;
        }
    
    }

    public class AdwordsBillingAccounts
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string CurrencyCode { get; set; }
        public string PrimaryBillingId { get; set; }
        public string SecondaryBillingId { get; set; }
    }
}
