using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class AdGroupPerformance : AdWordsBase, IAdGroupPerformance
    {

        public AdGroupPerformance()
        {
            User = new AdWordsUser();
        }

        public AdGroupPerformance(AdWordsUser user)
        {
            User = user;
        }

        public List<AdGroupPerformanceData> GetAdGroupPerformanceData(string customerId, DateTime statDate)
        {
            return GetAdGroupPerformanceData(customerId, statDate, statDate);
        }

        public List<AdGroupPerformanceData> GetAdGroupPerformanceData(string customerId, DateTime startDate, DateTime endDate)
        {
            var config = (AdWordsAppConfig)User.Config;
            config.ClientCustomerId = customerId;

            //var otherUser = new AdWordsUser(config);

            var query = "SELECT CampaignId, CampaignName, AdGroupId, AdGroupName, " +
                        "Clicks, Impressions, Cost, VideoViews, Date, " +
                        "VideoQuartile25Rate, VideoQuartile50Rate, VideoQuartile75Rate, VideoQuartile100Rate " +
                        $"FROM ADGROUP_PERFORMANCE_REPORT DURING {startDate:yyyyMMdd}, {endDate:yyyyMMdd}";

            var adGroupList = new List<AdGroupPerformanceData>();

            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(adGroupList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return adGroupList;
        }

        private static void ParseRow(List<AdGroupPerformanceData> adGroupList, XmlTextReader reader)
        {
            string adGroupName = null;
            string campaignName = null;
            long adGroupId = -1;
            long campaignId = -1,
                 impressions = -1,
                 views = -1,
                 clicks = -1;
            const long defaultVal = -1;
            DateTime? statDate = null;
            decimal? cost = null,
                 video25 = null,
                 video50 = null,
                 video75 = null,
                 video100 = null;

            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "adGroupID":
                        adGroupId = long.Parse(reader.Value);
                        break;
                    case "adGroup":
                        adGroupName = reader.Value;
                        break;
                    case "campaignID":
                        campaignId = long.Parse(reader.Value);
                        break;
                    case "campaign":
                        campaignName = reader.Value;
                        break;
                    case "day":
                        statDate = DateTime.Parse(reader.Value);
                        break;
                    case "impressions":
                        impressions = long.Parse(reader.Value);
                        break;
                    case "views":
                        views = long.Parse(reader.Value);
                        break;
                    case "cost":
                        cost = decimal.Parse(reader.Value);
                        break;
                    case "clicks":
                        clicks = long.Parse(reader.Value);
                        break;
                    case "videoPlayedTo25":
                        video25 = decimal.Parse(reader.Value.Replace("%", "")) / 100;
                        break;
                    case "videoPlayedTo50":
                        video50 = decimal.Parse(reader.Value.Replace("%", "")) / 100;
                        break;
                    case "videoPlayedTo75":
                        video75 = decimal.Parse(reader.Value.Replace("%", "")) / 100;
                        break;
                    case "videoPlayedTo100":
                        video100 = decimal.Parse(reader.Value.Replace("%", "")) / 100;
                        break;
                }

            }

            if (adGroupId != defaultVal && adGroupName != null &&
                campaignId != defaultVal && campaignName != string.Empty && 
                impressions != defaultVal && views != defaultVal && clicks != defaultVal && statDate != null &&
                cost.HasValue && video25.HasValue && video50.HasValue && video75.HasValue && video100.HasValue)
            {
                adGroupList.Add(new AdGroupPerformanceData()
                {
                    AdGroupId = adGroupId,
                    AdGroupName = adGroupName,
                    CampaignId = campaignId,
                    CampaignName = campaignName,
                    StatDate = statDate.Value,
                    Impressions = impressions,
                    Views = views,
                    Cost = cost.Value,
                    Clicks = clicks,
                    VideoQuartile25Rate = video25.Value,
                    VideoQuartile50Rate = video50.Value,
                    VideoQuartile75Rate = video75.Value,
                    VideoQuartile100Rate = video100.Value

                });
            }
        }
    }
}