﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Linq;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class AdwordAd : AdWordsBase, IAd
    {

        public AdwordAd()
        {
            User = new AdWordsUser();
        }

        public AdwordAd(AdWordsUser user)
        {
            User = user;
        }

        public List<AdwordAdData> GetAdData(string customerId)
        {
            var config = (AdWordsAppConfig)User.Config;
            config.ClientCustomerId = customerId;

            const string query = "SELECT Id, Headline, Status, CampaignId, CampaignName, AdGroupId, AdGroupName,  CreativeFinalUrls,  DisplayUrl " +
                                 "FROM AD_PERFORMANCE_REPORT DURING YESTERDAY";

            var adList = new List<AdwordAdData>();

            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(adList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return adList;
        }


        private static void ParseRow(List<AdwordAdData> adList, XmlTextReader reader)
        {
            long campaignId = 0;
            long adGroupId = 0;
            long adId = 0;
            string adName = string.Empty,
                adGroupName = string.Empty,
                campaignName = string.Empty,
                adState = string.Empty,
                destinationUrl = string.Empty,
                displayUrl = string.Empty;

            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "adID":
                        adId = long.Parse(reader.Value);
                        break;
                    case "ad":
                        adName = reader.Value;
                        break;
                    case "campaignID":
                        campaignId = long.Parse(reader.Value);
                        break;
                    case "campaign":
                        campaignName = reader.Value;
                        break;
                    case "adGroupID":
                        adGroupId = long.Parse(reader.Value);
                        break;
                    case "adGroup":
                        adGroupName = reader.Value;
                        break;
                    case "adState":
                        adState = reader.Value;
                        break;
                    case "finalURL":
                        destinationUrl = reader.Value.TrimStart('"', '[').TrimEnd('"', ']');
                        break;
                    case "displayURL":
                        displayUrl = reader.Value;
                        break;
                }

            }

            if (adId != 0 && campaignId != 0 && adGroupId != 0 &&
                adName != string.Empty && campaignName != string.Empty && adGroupName != string.Empty 
                && adState != string.Empty && destinationUrl != string.Empty && displayUrl != string.Empty)
            {
                adList.Add(new AdwordAdData()
                {
                    AdId = adId,
                    AdName = adName,
                    AdGroupId = adGroupId,
                    AdGroupName = adGroupName,
                    AwCampaignId =  campaignId,
                    AwCampaignName = campaignName,
                    AdState = adState,
                    DestinationUrl = destinationUrl,
                    DisplayUrl = displayUrl
                });
            }
        }
    }
}
