﻿using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class CampaignPerformance : AdWordsBase, ICampaignPerformance
    {
        public CampaignPerformance()
        {
            User = new AdWordsUser();
        }

        public CampaignPerformance(AdWordsUser user)
        {
            User = user;
        }

        public List<CampaignPerformanceData> GetCampaignPerformanceData(string customerId, DateTime statDate)
        {
            return GetCampaignPerformanceData(customerId, statDate, statDate);
        }

        public List<CampaignPerformanceData> GetCampaignPerformanceData(string customerId, DateTime startDate, DateTime endDate)
        {
            var config = (AdWordsAppConfig)User.Config;
            config.ClientCustomerId = customerId;
            var query = "SELECT CampaignId, CampaignName, Date, Impressions, VideoViews, Cost, Clicks, " +
                        $"VideoQuartile25Rate, VideoQuartile50Rate, VideoQuartile75Rate, VideoQuartile100Rate FROM CAMPAIGN_PERFORMANCE_REPORT DURING {startDate:yyyyMMdd}, {endDate:yyyyMMdd}";

            var campaignList = new List<CampaignPerformanceData>();

            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(campaignList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return campaignList;
        }

        private static void ParseRow(List<CampaignPerformanceData> campaignList, XmlTextReader reader)
        {
            string campaignName = null;
            long campaignId = -1,
                 impressions = -1,
                 views = -1,
                 clicks = -1;
            const long defaultVal = -1;
            DateTime? statDate = null;
            decimal? cost = null,
                 video25 = null,
                 video50 = null,
                 video75 = null,
                 video100 = null;

            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "campaignID":
                        campaignId = long.Parse(reader.Value);
                        break;
                    case "campaign":
                        campaignName = reader.Value;
                        break;
                    case "day":
                        statDate = DateTime.Parse(reader.Value);
                        break;
                    case "impressions":
                        impressions = long.Parse(reader.Value);
                        break;
                    case "views":
                        views = long.Parse(reader.Value);
                        break;
                    case "cost":
                        cost = decimal.Parse(reader.Value);
                        break;
                    case "clicks":
                        clicks = long.Parse(reader.Value);
                        break;
                    case "videoPlayedTo25":
                        video25 = decimal.Parse(reader.Value.Replace("%", "")) / 100;
                        break;
                    case "videoPlayedTo50":
                        video50 = decimal.Parse(reader.Value.Replace("%", "")) / 100;
                        break;
                    case "videoPlayedTo75":
                        video75 = decimal.Parse(reader.Value.Replace("%", "")) / 100;
                        break;
                    case "videoPlayedTo100":
                        video100 = decimal.Parse(reader.Value.Replace("%", "")) / 100;
                        break;
                }

            }

            if (campaignId != defaultVal && campaignName != null && impressions != defaultVal && views != defaultVal && clicks != defaultVal && statDate != null &&
                cost.HasValue && video25.HasValue && video50.HasValue && video75.HasValue && video100.HasValue)
            {
                campaignList.Add(new CampaignPerformanceData
                {
                    CampaignId = campaignId,
                    StatDate = statDate.Value,
                    CampaignName = campaignName,
                    Impressions = impressions,
                    Views = views,
                    Cost = cost.Value,
                    Clicks = clicks,
                    VideoQuartile25Rate = video25.Value,
                    VideoQuartile50Rate = video50.Value,
                    VideoQuartile75Rate = video75.Value,
                    VideoQuartile100Rate = video100.Value
                });
            }
        }
    }
}