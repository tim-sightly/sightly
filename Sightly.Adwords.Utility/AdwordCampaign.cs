﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO.Compression;
using System.Linq;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class AdwordCampaign : AdWordsBase, ICampaign
    {
        public AdwordCampaign()
        {
            User = new AdWordsUser();
        }

        public AdwordCampaign(AdWordsUser user)
        {
            User = user;
        }

        public List<AdwordsCampaignData> GetCampaignData(string customerId)
        {
            var config = (AdWordsAppConfig) User.Config;
            config.ClientCustomerId =customerId;

            //var otherUser = new AdWordsUser(config);

            var query = "SELECT CampaignStatus, CampaignId, CampaignName, ExternalCustomerId, StartDate, EndDate " +
                           "FROM CAMPAIGN_PERFORMANCE_REPORT DURING YESTERDAY";

            var campaignList = new List<AdwordsCampaignData>();
            
            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());

            try{
                using (var response = reportUtilities.GetResponse()){
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress)){
                        using (var reader = new XmlTextReader(gZipStream)){
                            while (reader.Read()){
                                switch (reader.NodeType){
                                        case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(campaignList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return campaignList;
        }

        public List<AdwordsCampaignData> GetCampaignDataForDate(string customerId, DateTime statDate)
        {
            var config = (AdWordsAppConfig)User.Config;
            config.ClientCustomerId = customerId;

            //var otherUser = new AdWordsUser(config);

            var query = "SELECT CampaignStatus, CampaignId, CampaignName, ExternalCustomerId, StartDate, EndDate " +
                        $"FROM CAMPAIGN_PERFORMANCE_REPORT DURING {statDate:yyyyMMdd}, {statDate:yyyyMMdd}";

            var campaignList = new List<AdwordsCampaignData>();

            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());

            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(campaignList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return campaignList;
        }

        private static void ParseRow(List<AdwordsCampaignData> campaignList, XmlTextReader reader)
        {
            string campaignName = null, campaignStatus = null;
            long campaignId = 0;
            var startDate = new DateTime(1900, 1, 1);
            var endDate = new DateTime(1900, 1, 1);
            var badDate = new DateTime(1900, 1, 1);
            
            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "campaignState":
                        campaignStatus = reader.Value;
                        break;
                    case "campaignID":
                        campaignId = long.Parse(reader.Value);
                        break;
                    case "campaign":
                        campaignName = reader.Value;
                        break;
                    case "startDate":
                        DateTime.TryParse(reader.Value, out startDate);
                        break;
                    case "endDate":
                        DateTime.TryParse(reader.Value, out endDate);
                        break;
                }

            }

            if (campaignId != 0 && campaignName != null &&
                startDate != badDate && endDate != badDate && campaignStatus != null) // && campaignStatus.Contains("enabled"))
            {
                campaignList.Add(new AdwordsCampaignData()
                {
                    AwCampaignId = campaignId,
                    AwCampaignName = campaignName,
                    StartDate = startDate,
                    EndDate = endDate,
                    CampaignStatus = campaignStatus
                });
            }
            
        }

        public long GetCampaignIdFromCampaignName(string campaignName)
        {
            const long parentCustomer = 9086250783L;
            var returnedValue = GetCampaignData(parentCustomer.ToString(CultureInfo.InvariantCulture));
            var adwordsCampaignData = returnedValue.FirstOrDefault(rv => rv.AwCampaignName == campaignName);
            return adwordsCampaignData != null ? adwordsCampaignData.AwCampaignId : 0;
        }
    }
}
