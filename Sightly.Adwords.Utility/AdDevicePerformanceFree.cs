using System;
using System.Collections.Generic;
using System.IO.Compression;
using System.Xml;
using Google.Api.Ads.AdWords.Lib;
using Google.Api.Ads.AdWords.Util.Reports;
using Google.Api.Ads.AdWords.v201806;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;

namespace Sightly.Adwords.Utility
{
    public class AdDevicePerformanceFree : AdWordsBase, IAdDevicePerformanceFree
    {
        public AdDevicePerformanceFree()
        {
            User = new AdWordsUser();
        }

        public AdDevicePerformanceFree(AdWordsUser user)
        {
            User = user;
        }


        public List<AdDevicePerformanceFreeData> GetAdDevicePerformanceFreeData(string customerId, DateTime statDate)
        {
            return GetAdDevicePerformanceFreeData(customerId, statDate, statDate);
        }

        public List<AdDevicePerformanceFreeData> GetAdDevicePerformanceFreeData(string customerId, DateTime startDate, DateTime endDate)
        {
            var config = (AdWordsAppConfig) User.Config;
            config.ClientCustomerId = customerId;
            var query = "SELECT CampaignId, CampaignName, AdGroupId, Id, Headline, Device, Impressions, VideoViews, Cost, Clicks, Date " +
                        $"FROM AD_PERFORMANCE_REPORT DURING {startDate:yyyyMMdd}, {endDate:yyyyMMdd}";

            var adDevicePerformanceFreeList = new List<AdDevicePerformanceFreeData>();

            var reportUtilities = new ReportUtilities(User, "v201806", query, DownloadFormat.GZIPPED_XML.ToString());
            try
            {
                using (var response = reportUtilities.GetResponse())
                {
                    using (var gZipStream = new GZipStream(response.Stream, CompressionMode.Decompress))
                    {
                        using (var reader = new XmlTextReader(gZipStream))
                        {
                            while (reader.Read())
                            {
                                switch (reader.NodeType)
                                {
                                    case XmlNodeType.Element:
                                        if (reader.Name == "row")
                                        {
                                            ParseRow(adDevicePerformanceFreeList, reader);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                throw new System.ApplicationException("Failed to download report.", e);
            }

            return adDevicePerformanceFreeList;
        }

        private static void ParseRow(ICollection<AdDevicePerformanceFreeData> adList, XmlReader reader)
        {
            string adName = null,
                deviceName = null,
                campaignName  = null;
            long adGroupId = -1,
                adId = -1,
                campaignId = -1,
                impressions = -1,
                views = -1,
                clicks = -1;
            const long defaultVal = -1;
            DateTime? statDate = null;
            decimal? cost = null;

            while (reader.MoveToNextAttribute())
            {
                switch (reader.Name)
                {
                    case "device":
                        deviceName = reader.Value;
                        break;
                    case "campaignID":
                        campaignId = long.Parse(reader.Value);
                        break;
                    case "campaign":
                        campaignName = reader.Value;
                        break;
                    case "adGroupID":
                        adGroupId = long.Parse(reader.Value);
                        break;
                    case "adID":
                        adId = long.Parse(reader.Value);
                        break;
                    case "ad":
                        adName = reader.Value;
                        break;
                    case "day":
                        statDate = DateTime.Parse(reader.Value);
                        break;
                    case "impressions":
                        impressions = long.Parse(reader.Value);
                        break;
                    case "views":
                        views = long.Parse(reader.Value);
                        break;
                    case "cost":
                        cost = decimal.Parse(reader.Value);
                        break;
                    case "clicks":
                        clicks = long.Parse(reader.Value);
                        break;
                }

            }

            if (deviceName != null && campaignId != defaultVal && adGroupId != defaultVal && adId != defaultVal && adName != null && campaignName != null &&
                impressions != defaultVal && views != defaultVal && clicks != defaultVal && statDate != null && cost.HasValue)
            {
                adList.Add(new AdDevicePerformanceFreeData
                {
                    CampaignId = campaignId,
                    CampaignName = campaignName,
                    AdGroupId = adGroupId,
                    AdId = adId,
                    AdName = adName,
                    StatDate = statDate.Value,
                    DeviceName = deviceName,
                    Impressions = impressions,
                    Views = views,
                    Cost = cost.Value,
                    Clicks = clicks
                });
            }
        }
    }
}