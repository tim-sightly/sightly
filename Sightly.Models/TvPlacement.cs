using System;

namespace Sightly.Models
{
    public class TvPlacement
    {
        public Guid PlacementId { get; set; }
        public Guid AdvertiserId { get; set; }
        public string PlacementName { get; set; }
        public string PlacementValue { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}