﻿using System.Collections.Generic;

namespace Sightly.Models
{
    public class NielsenCombinedData
    {
        //public List<NielsenCampaignReference> NielsenCampaings { get; set; }
        //public List<NielsenCampaignSiteReference> NielsenCampaingPlacements { get; set; }
        public List<NielsenCampaignPlacementExposure> NielsenStatsExposure { get; set; }

        public NielsenCombinedData()
        {
            //NielsenCampaings = new List<NielsenCampaignReference>();
            //NielsenCampaingPlacements = new List<NielsenCampaignSiteReference>();
            NielsenStatsExposure = new List<NielsenCampaignPlacementExposure>();
        }
    }
}
