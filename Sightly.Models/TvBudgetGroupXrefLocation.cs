using System;

namespace Sightly.Models
{
    public class TvBudgetGroupXrefLocation
    {
        public Guid BudgetGroupXrefLocationId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public Guid LocationId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}