﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.Models
{
    public class AccountMetaData
    {
        public string AccountName { get; set; }
        public string AdvertiserName { get; set; }
        public string CampaignName { get; set; }
    }
}
