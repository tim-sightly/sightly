﻿using System;

namespace Sightly.Models
{
    public class VideoAssetData
    {
        public Guid VideoAssetId { get; set; }
        public Guid AccountId { get; set; }
        public string VideoAssetName { get; set; }
        public string VideoAssetFileName { get; set; }
        public string VideoAssetRefCode { get; set; }
        public string YouTubeId { get; set; }
        public int? VideoLengthInSeconds { get; set; }        
    }
}