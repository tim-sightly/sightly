﻿using System;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace Sightly.Models
{
    public class MediaAgencyImport
    {
        public string CampaignName { get; set; }
        public string OrderName { get; set; }
        public long AwCustomerId { get; set; }
        public long AwCampaignId { get; set; }
        public string AwCampaignName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal CampaignBudget { get; set; }
        public decimal Margin { get; set; }
        public string PlacementId { get; set; }
        public string PlacementName { get; set; }
        public decimal TotalBudget { get; set; }
        public string  CampaignMangerEmail { get; set; }
        public Guid? BudgetGroupTimedBudgetId { get; set; }

        public static MediaAgencyImport FromCsv(string csvLine)
        {
            var parser = new TextFieldParser(new StringReader(csvLine))
            {
                HasFieldsEnclosedInQuotes = true
            };
            parser.SetDelimiters(",");

            var values = new string[] { }; 

            while(!parser.EndOfData)
            {
                values = parser.ReadFields();
            }

            var mai = new MediaAgencyImport
            {
                CampaignName = values[0],
                AwCampaignId = Convert.ToInt64(values[2]),
                AwCampaignName = values[3],
                AwCustomerId = Convert.ToInt64(values[1].Replace("-", "")),
                CampaignBudget = Convert.ToDecimal(values[6].Replace("$", "")),
                StartDate = DateTime.Parse(values[4]),
                EndDate = DateTime.Parse(values[5]),
                Margin = Convert.ToDecimal(values[7].Replace("%", "")) / 100,
                PlacementId = values[8],
                PlacementName = values[9],
                BudgetGroupTimedBudgetId = values[10] != string.Empty ? Guid.Parse(values[10]) : (Guid?)null,
                CampaignMangerEmail = values[11],
                TotalBudget = 0.0M
            };



            return mai;
        }
    }
}
