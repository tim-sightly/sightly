﻿using System;


namespace Sightly.Models
{
    public class AdwordsTvAssociationData
    {
        public long AwCustomerId { get; set; }
        public Guid OrderId { get; set; }
        public long AwCampaignId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public long AwAdId { get; set; }
        public Guid AdId { get; set; }
    }
}
