﻿using System;

namespace Sightly.Models
{
    public class CustomerStartEndData
    {
        public long CustomerId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
