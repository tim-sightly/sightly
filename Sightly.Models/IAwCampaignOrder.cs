using System;

namespace Sightly.Models
{
    public interface IAwCampaignOrder
    {
        string AccountName { get; set; }
        long CustomerId { get; set; }
        string CustomerName { get; set; }
        long CampaignId { get; set; }
        string CampaignName { get; set; }
        DateTime StatDate { get; set; }
        Guid TvOrderId { get; set; }
    }
}