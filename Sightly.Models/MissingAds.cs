using System.Collections.Generic;

namespace Sightly.Models
{
    public class MissingAds
    {
        public MissingAds()
        {
            AdsInAw = new List<AwAdTvRef>();
            AdsInTV = new List<TargetViewAdwordsData>();
        }
        public List<AwAdTvRef> AdsInAw { get; set; }
        public List<TargetViewAdwordsData> AdsInTV { get; set; }
    }
}