﻿using System.Collections.Generic;

namespace Sightly.Models
{
    public class CampaignLabelData  
    {
        public long CampaignId { get; set; }
        public List<string> Labels { get; set; }

        public CampaignLabelData()
        {
            Labels = new List<string>();
        }
    }
}