using System;

namespace Sightly.Models
{
    public class TvOrderXrefBudgetGroup
    {
        public Guid OrderXrefBudgetGroupId { get; set; }
        public Guid OrderId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}