using System;

namespace Sightly.Models
{
    public class AdDevicePerformanceFreeData
    {
        public DateTime StatDate { get; set; }
        public long AdGroupId { get; set; }
        public long AdId { get; set; }
        public string AdName { get; set; }
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public long Clicks { get; set; }
        public decimal Cost { get; set; }
        public string DeviceName { get; set; }
        public long Impressions { get; set; }
        public long Views { get; set; }
    }
}