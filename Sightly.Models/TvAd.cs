using System;

namespace Sightly.Models
{
    public class TvAd
    {
        public Guid AdId { get; set; }
        public string DestinationUrl { get; set; }
        public string DisplayUrl { get; set; }
        public string VideoAdName { get; set; } 
        public Guid VideoAssetVersionId { get; set; }
        public string CreatedBy { get; set; } 
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; } 
        public DateTime LastModifiedDatetime { get; set; } 
    }
}