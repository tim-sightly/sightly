namespace Sightly.Models
{
    public class CampaignData
    {
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public long CustomerId { get; set; }
    }
}