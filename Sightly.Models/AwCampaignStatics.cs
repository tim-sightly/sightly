using System;

namespace Sightly.Models
{
    public class AwCampaignStatics : IAwCampaignOrder
    {
        public string AccountName { get; set; }
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public DateTime StatDate { get; set; }
        public long Impressions { get; set; }
        public long TvVideoImpressions { get; set; }
        public long TvOrderImpressions { get; set; }
        public long TvBudgetGroupImpressions { get; set; }
        public long VideoViews { get; set; }
        public long TvVideoViews { get; set; }
        public long TvOrderViews { get; set; }
        public long TvBudgetGroupViews { get; set; }
        public Guid TvOrderId { get; set; }
        public Guid TvBudgetGroupId { get; set; }
        public string TvOrderStatus { get; set; }
        ////added by GH
        public long VideoImpressionsDiff { get; set; }
        public long VideoViewsDiff { get; set; }
        public long OrderImpressionsDiff { get; set; }
        public long OrderVideoViewsDiff { get; set; }
        public decimal PercentImpressionChange { get; set; }
        public decimal PercentVideoChange { get; set; }
        public bool ChangeGreaterThan10Percent { get; set; }

        public bool OrderIsEmpty { get; set; }

        public long BudgetGroupImpressionsDiff { get; set; }
        public long BudgetGroupVideoViewsDiff { get; set; }


    }
}