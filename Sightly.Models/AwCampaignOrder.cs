using System;

namespace Sightly.Models
{
    public class AwCampaignOrder : IAwCampaignOrder
    {
        public string AccountName { get; set; }
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public DateTime StatDate { get; set; }
        public Guid TvOrderId { get; set; }
        public Guid TvCampaignId { get; set; }

    }
}