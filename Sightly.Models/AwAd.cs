﻿
namespace Sightly.Models
{
    public class AwAd
    {
        public long AdId { get; set; }
        public string AdName { get; set; }
        public long AdGroupId { get; set; }
        public string AdGroupName { get; set; }
        public string AdState { get; set; }
        public string DestinationUrl { get; set; }
        public string DisplayUrl { get; set; }
        public AwVideo Video { get; set; }
        public long CampaignId { get; set; }
    }
}