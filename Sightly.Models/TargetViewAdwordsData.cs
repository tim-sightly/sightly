﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.Models
{
    public class TargetViewAdwordsData
    {
        public Guid AccountId { get; set; }
        public string AccountName { get; set; }
        public Guid AdvertiserId { get; set; }
        public string AdvertiserName { get; set; }
        public Guid CampaignId { get; set; }
        public string CampaignName { get; set; }
        public long AwCustomerId { get; set; }
        public Guid OrderId { get; set; }
        public string OrderName { get; set; }
        public string OrderRefCode { get; set; }
        public Guid BudgetGroupId { get; set; }
        public string BudgetGroupName { get; set; }
        public long AwCampaignId { get; set; }
        public string AwCampaignName { get; set; }
        public Guid AdId { get; set; }
        public string VideoAdName { get; set; }
        public string DestinationUrl { get; set; }
        public string DisplayUrl { get; set; }
        public long AwAdId { get; set; }
        
    }
}
