using System;

namespace Sightly.Models
{
    public class BudgetGroupCampaignWithAdData
    {
        public Guid OrderId { get; set; }
        public long AdWordsCustomerId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public string BudgetGroupName { get; set; }
        public string AdWordsCampaignName { get; set; }
        public long? AdWordsCampaignId { get; set; }
        public Guid AdId { get; set; }
        public string VideoAdName { get; set; }
        public string AdWordsAdName { get; set; }
        public long? AdWordsAdId { get; set; }

        public string CampaignId => $"{BudgetGroupName} - {BudgetGroupId}";
        public string AdAdId => $"{VideoAdName} - {AdId}";
    }
}