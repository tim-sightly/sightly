﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace Sightly.Models
{
    public class DoubleVerifyStats
    {
        public long PlacementId { get; set; }
        public string PlacementName { get; set; }
        public DateTime StatDate { get; set; }
        public string DeviceType { get; set; }
        public long MonitoredImpressions { get; set; }  
        public long? BrandSafetyOnTargetImpressions_1x1 { get; set; }
        public long? FraudSIVTImpressions { get; set; }
        public long? FraudSIVTImpressions_1x1 { get; set; }
        public long? InGeoImpressions_1x1 { get; set; }
        public long? VideoViewableImpressions { get; set; }
    }
}
