﻿using System;
using System.Collections.Generic;

namespace Sightly.Models
{
    public class AwCampaign
    {
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public AwBudget Budget { get; set; }
        public List<AwAd> Ads { get; set; }

        public AwCampaign()
        {
            Ads = new List<AwAd>();
        }
    }
}