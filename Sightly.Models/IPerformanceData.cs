﻿using System;

namespace Sightly.Models
{
    public interface IPerformanceData
    {
        long CampaignId { get; set; }
        DateTime StatDate { get; set; }
        string CampaignName { get; set; }
        long Impressions { get; set; }
        long Views { get; set; }
        decimal Cost { get; set; }
        long Clicks { get; set; }
    }
}