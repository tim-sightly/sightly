﻿namespace Sightly.Models
{
    public class AwVideo
    {
        public string VideoId { get; set; }
        public string VideoName { get; set; }
        public long VideoDuration { get; set; }
        public string VideoChannelId { get; set; }
        public long CampaignId { get; set; }
    }
}