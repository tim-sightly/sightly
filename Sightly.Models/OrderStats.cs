﻿using System;

namespace Sightly.Models
{
    public class OrderStats
    {
        #region Properties
        public Guid OrderStatId { get; set; }
        public decimal ActualSpend { get; set; }

        public decimal Amount { get; set; }

        public int AudienceRetention25 { get; set; }

        public int AudienceRetention50 { get; set; }

        public int AudienceRetention75 { get; set; }

        public int AudienceRetention100 { get; set; }

        public decimal AverageCpc { get; set; }

        public double AverageCpe { get; set; }

        public decimal AverageCpm { get; set; }

        public double AverageCpv { get; set; }

        public double AveragePosition { get; set; }

        public long CompletedViews { get; set; }

        public long Clicks { get; set; }

        public double ClickThroughRate { get; set; }

        public double Conversions { get; set; }

        public long Engagements { get; set; }

        public long Impressions { get; set; }

        public long Interactions { get; set; }

        public Guid OrderId { get; set; }

        public DateTime StatDate { get; set; }

        public decimal TotalSpend { get; set; }

        public double ViewRate { get; set; }

        public long ViewThroughConversions { get; set; }
        #endregion
    }
}
