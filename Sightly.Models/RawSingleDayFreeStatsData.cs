using System.Collections.Generic;

namespace Sightly.Models
{
    public class RawSingleDayFreeStatsData
    {
        public List<AdDevicePerformanceData> AdDeviceStats { get; set; }
        public List<AgeRangePerformanceFreeData> AgeRangeStats { get; set; }
        public List<GenderPerformanceFreeData> GenderStats { get; set; }
    }
}