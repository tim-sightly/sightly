namespace Sightly.Models
{
    public interface IVideoPerformanceData : IPerformanceData
    {
        decimal VideoQuartile25Rate { get; set; }
        decimal VideoQuartile50Rate { get; set; }
        decimal VideoQuartile75Rate { get; set; }
        decimal VideoQuartile100Rate { get; set; }
    }
}