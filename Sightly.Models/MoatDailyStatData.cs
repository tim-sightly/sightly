using Newtonsoft.Json;
using System;

namespace Sightly.Models
{
    public class MoatDailyStatData
    {
        [JsonProperty("Date")]
        public DateTime StatDate { get; set; }

        [JsonProperty("level1_id")]
        public long AdvertiserId { get; set; }

        [JsonProperty("level1_label")]
        public string AdvertiserName { get; set; }

        [JsonProperty("impressions_analyzed")]
        public long Impressions { get; set; }

        [JsonProperty("loads_unfiltered")]
        public long LoadsUnfiltered { get; set; }

        [JsonProperty("strict_or_px_2sec_consec_video_ots_unfiltered")]
        public long StrictOrPx2SecConsecVideoOtsUnfiltered { get; set; }

        [JsonProperty("l_somehow_measurable_unfiltered")]
        public long ISomehowMeasurableUnfiltered { get; set; }

        [JsonProperty("human_and_avoc")]
        public long? HumanAndAvoc { get; set; }

        [JsonProperty("measurable_impressions")]
        public long MeasurableImpressions { get; set; }

        [JsonProperty("reached_complete_sum")]
        public long ReachedCompleteSum { get; set; }

        public MoatDailyStatData() { }

    }
}