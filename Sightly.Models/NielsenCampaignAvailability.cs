﻿using Newtonsoft.Json;

namespace Sightly.Models
{
    public class NielsenCampaignAvailability
    {
        #region Public Properties
        [JsonProperty("campaignId")]
        public string CampaignId { get; set; }
        public string CampaignName { get; set; }

        [JsonProperty("parentCampaignId")]
        public string ParentCampaignId { get; set; }

        [JsonProperty("reportDate")]
        public string ReportDate { get; set; }

        [JsonProperty("startDate")]
        public string StartDate { get; set; }

        [JsonProperty("reportStatus")]
        public string ReportStatus { get; set; }

        [JsonProperty("campaignStatus")]
        public string CampaignStatus { get; set; }

        [JsonProperty("releaseStatus")]
        public string ReleaseStatus { get; set; }

        [JsonProperty("dataReleaseId")]
        public string DataReleaseId { get; set; }


        #endregion Public Properties

        #region Public Constructors
        public NielsenCampaignAvailability()
        { }
        #endregion Public Constructors
    }
}
