using System;

namespace Sightly.Models
{
    public class TvAdvertiser
    {
        public Guid AdvertiserId { get; set; }
        public Guid AccountId { get; set; }
        public string AdvertiserName { get; set; }
        public Guid AdvertiserCategoryId { get; set; }
        public string AdvertiserRefCode { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}