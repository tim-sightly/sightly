﻿using System;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace Sightly.Models
{
    public class MediaAgencyDateBudgetUpdate
    {
        public string CampaignName { get; set; }
        public long AwCustomerId { get; set; }
        public long AwCampaignId { get; set; }
        public string AwCampaignName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal CampaignBudget { get; set; }
        public decimal Margin { get; set; }
        public string PlacementId { get; set; }
        public string PlacementName { get; set; }
        public decimal TotalBudget { get; set; }
        public Guid BudgetGroupTimedBudgetId { get; set; }

        public static MediaAgencyDateBudgetUpdate FromCsv(string csvLine)
        {
            var parser = new TextFieldParser(new StringReader(csvLine))
            {
                HasFieldsEnclosedInQuotes = true
            };
            parser.SetDelimiters(",");

            var values = new string[] { };

            while (!parser.EndOfData)
            {
                values = parser.ReadFields();
            }

            var mai = new MediaAgencyDateBudgetUpdate
            {
                CampaignName = values[0],
                AwCampaignId = Convert.ToInt64(values[2]),
                AwCampaignName = values[3],
                AwCustomerId = Convert.ToInt64(values[1].Replace("-", "")),
                CampaignBudget = Convert.ToDecimal(values[6].Replace("$", "")),
                StartDate = DateTime.Parse(values[4]),
                EndDate = DateTime.Parse(values[5]),
                Margin = Convert.ToDecimal(values[7].Replace("%", "")) ,
                PlacementId = values[8],
                PlacementName = values[9],
                TotalBudget = 0.0M,
                BudgetGroupTimedBudgetId = Guid.Parse(values[10])
            };
            
            return mai;
        }
        
    }
}
