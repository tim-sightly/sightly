﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.Models
{
    public class OrderLite
    {
        public Guid OrderId { get; set; }
        public string OrderName { get; set; }
        public string OrderRefCode { get; set; }
        public string AccountName { get; set; }
        public Guid AdvertiserId { get; set; }
        public string AdvertiserName { get; set; }
        public Guid CampaignId { get; set; }
        public string CampaignName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public String OrderStatus { get; set; }
        public decimal BudgetAmount { get; set; }
    }
}
