using System;

namespace Sightly.Models
{
    public class TvBudgetGroupXrefAd
    {
        public Guid BudgetGroupXrefAdId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public Guid AdId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}