﻿using System;

namespace Sightly.Models
{
    public class AwAdTvRef
    {
        public Guid TvVideoAssetId { get; set; }
        public Guid TvAdId { get; set; }
        public Guid TvOrderId { get; set; }
        public Guid TvBudgetGroupId { get; set; }

        public AwAd AwAd { get; set; }

        public AwVideo AwVideo { get; set; }
    }
}