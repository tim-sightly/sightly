using System;

namespace Sightly.Models
{
    public class AdvertiserCustomerData
    {
        public Guid AdvertiserId { get; set; }
        public string AdvertiserName { get; set; }
        public long? AdWordsCustomerId { get; set; }
        public string LastModifiedBy { get; set; }

    }
}