﻿namespace Sightly.Models
{
    public class AdwordBudgetData
    {
        public long BudgetId { get; set; }
        public long AwCampaignId { get; set; }
        public double Amount { get; set; }
    }
}
