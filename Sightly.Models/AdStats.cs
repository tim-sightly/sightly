﻿using System;

namespace Sightly.Models
{
    public class AdStats
    {
        public decimal ActualSpend { get; set; }

        public Guid AdId { get; set; }

        public Guid AdStatsId { get; set; }

        public long AdWordsAdId { get; set; }

        public int AudienceRetention25 { get; set; }

        public int AudienceRetention50 { get; set; }

        public int AudienceRetention75 { get; set; }

        public int AudienceRetention100 { get; set; }

        public double AverageCpv { get; set; }

        public Guid? BudgetGroupId { get; set; }

        public long Clicks { get; set; }

        public double ClickThroughRate { get; set; }

        public long CompletedViews { get; set; }

        public double Conversions { get; set; }

        public decimal EstimatedCost { get; set; }

        public long Impressions { get; set; }

        public Guid? OrderId { get; set; }

        public decimal PartialViews { get; set; }

        public DateTime StatDate { get; set; }

        public decimal TotalSpend { get; set; }

        public double ViewRate { get; set; }

        public long ViewThroughConversions { get; set; }

        public long ViewTime { get; set; }
    }
}
