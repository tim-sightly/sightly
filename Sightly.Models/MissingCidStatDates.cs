using System;

namespace Sightly.Models
{
    public class MissingCidStatDates
    {
        public long CustomerId { get; set; }
        public DateTime StatDate { get; set; }
    }
}