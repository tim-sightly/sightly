﻿using System;

namespace Sightly.Models
{
    public class OrderCancelData
    {
        public string CurrentUserEmail { get; set; }
        public string AccountName { get; set; }
        public string AdvertiserName { get; set; }
        public string CampaignName { get; set; }
        public string AdwordsCustomerId { get; set; }
        public string CampaignManagerEmail { get; set; }
        public string OrderName { get; set; }
        public string OrderRefCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal TotalBudget { get; set; }
        public string Notes { get; set; }
        public string ActionDate { get; set; }
    }
}