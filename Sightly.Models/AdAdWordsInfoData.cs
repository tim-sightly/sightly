using System;

namespace Sightly.Models
{
    public class AdAdWordsInfoData
    {
        public Guid AdAdWordsInfoId { get; set; }
        public Guid AdId { get; set; }
        public Guid OrderId { get; set; }

        public Guid BudgetGroupId { get; set; }

        public string AdWordsAdName { get; set; }
        public long AdWordsAdId { get; set; }
        public string LastModified { get; set; }        
    }
}