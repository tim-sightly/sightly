﻿using System;

namespace Sightly.Models
{
    public class GeoPerformanceFreeData
    {
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public DateTime StatDate { get; set; }
        public long AdGroupId { get; set; }
        public string AdGroupName { get; set; } 
        public long CountryCriteriaId { get; set; }
        public long CityCriteriaId { get; set; }
        public long RegionCriteriaId { get; set; }
        public long MetroCriteriaId { get; set; }
        public long MostSpecificCriteriaId { get; set; }
        public long VideoViews { get; set; }
        public long Clicks { get; set; }
        public decimal Cost { get; set; }
        public long Impressions { get; set; }
        public long CustomerId { get; set; }
     
    }
}