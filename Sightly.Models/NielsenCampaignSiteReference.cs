﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Sightly.Models
{
    public class NielsenCampaignSiteReference
    {
        #region Public Properties
        [JsonProperty("campaignId")]
        public long CampaignId { get; set; }

        [JsonProperty("siteId")]
        public long SiteId { get; set; }

        [JsonProperty("siteName")]
        public string SiteName { get; set; }

        [JsonProperty("placementId")]
        public long PlacementId { get; set; }

        [JsonProperty("tagPlacementId")]
        public string TagPlacementId { get; set; }

        [JsonProperty("placementName")]
        public string PlacementName { get; set; }

        [JsonProperty("placementStatus")]
        public string PlacementStatus { get; set; }

        [JsonProperty("siteURL")]
        public string SiteURL { get; set; }

        #endregion Public Properties

        #region Public Constructors
        public NielsenCampaignSiteReference()
        { }
        #endregion Public Constructors
    }
}
