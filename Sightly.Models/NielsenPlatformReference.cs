﻿using Newtonsoft.Json;

namespace Sightly.Models
{
    public class NielsenPlatformReference
    {
        #region Public Properties
        [JsonProperty("platformCode")]
        public int PlatformCode { get; set; }

        [JsonProperty("platformDescription")]
        public string PlatformDescription { get; set; }
        #endregion Public Properties

        #region Public Properties
        public NielsenPlatformReference()
        { }
        #endregion Public Properties
    }
}
