﻿using System;

namespace Sightly.Models
{
    public class NielsenDailyDarStats
    {
        public long CustomerId { get; set; }
        public DateTime StatDate { get; set; }
        public decimal ComputerDar { get; set; }
        public decimal MobileDar { get; set; }
        public decimal TotalDigitalDar { get; set; }

    }
}
