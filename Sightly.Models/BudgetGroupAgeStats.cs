using System;

namespace Sightly.Models
{
    public class BudgetGroupAgeStats : BudgetGroupBaseStats
    {
        #region Properties

        public string AdWordsAgeGroupName { get; set; }

        public Guid BudgetGroupTargetAgeGroupStatsId { get; set; }
        #endregion
    }
}