﻿using System;

namespace Sightly.Models
{
    public class ParentalPerformanceData : IPerformanceData
    {
        public long AdGroupId { get; set; }
        public string Criteria { get; set; }
        public long CampaignId { get; set; }
        public DateTime StatDate { get; set; }
        public string CampaignName { get; set; }
        public long Impressions { get; set; }
        public long Views { get; set; }
        public decimal Cost { get; set; }
        public long Clicks { get; set; }
    }
}