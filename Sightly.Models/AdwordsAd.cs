﻿namespace Sightly.Models
{
    public class AdwordsAd
    {
        public long AdId { get; set; }
        public string AdName { get; set; }
        public long AwCampaignId { get; set; }
        public string AdState { get; set; }
        public string VideoId { get; set; }
        public string VideoName { get; set; }
        public long VideoDuration { get; set; }
        public string DestinationUrl { get; set; }
        public string DisplayUrl { get; set; }
    }
}