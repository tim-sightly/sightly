﻿using System.Security.AccessControl;

namespace Sightly.Models
{
    public class CustomerCampaignRawStatsByDayData
    {
        public long CustomerId { get; set; }
        public long CampaignId { get; set; }
        public string CampaignName { get; set; }
        public long AdClicks { get; set; }
        public long AgeClicks { get; set; }
        public long GenderClicks { get; set; }
        public long AdCost { get; set; }
        public long AgeCost { get; set; }
        public long GenderCost { get; set; }
        public long AdImpressions { get; set; }
        public long AgeImpressions { get; set; }
        public long GenderImpressions { get; set; }
        public long AdViews { get; set; }
        public long AgeViews { get; set; }
        public long GenderViews { get; set; }
        
    }
}
