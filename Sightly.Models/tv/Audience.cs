﻿using System;
using System.Collections.Generic;

namespace Sightly.Models.tv
{
    public class Audience
    {
        public List<Guid> AgeRanges { get; set; }
        public string CompetitorUrls { get; set; }
        public List<Guid> Genders { get; set; }
        public List<Int16> HouseholdIncomes { get; set; }
        public string KeyWords { get; set; }
        public string Notes { get; set; }
        public List<Guid> ParentalStatuses { get; set; }


        public Audience()
        {
            AgeRanges = new List<Guid>();
            Genders = new List<Guid>();
            ParentalStatuses = new List<Guid>();
            HouseholdIncomes = new List<Int16>();
        }
    }
}