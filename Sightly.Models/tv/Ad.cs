﻿using System;

namespace Sightly.Models.tv
{
    public class Ad
    {
        public Guid AdId { get; set; }
        public string AdName { get; set; }
        public string ClickableUrl { get; set; }
        public int Index { get; set; }
        public string LandingUrl { get; set; }
        public int AdFormat { get; set; }
        public string YoutubeUrl { get; set; }
        public Guid VideoAssetVersionId { get; set; }
        public string CampanionBannerUrl { get; set; }
        public bool Pause { get; set; }
        public bool Deleted { get; set; }
    }
}