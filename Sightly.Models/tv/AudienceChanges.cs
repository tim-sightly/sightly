﻿using System.Collections.Generic;

namespace Sightly.Models.tv
{
    public class AudienceChanges
    {
        public string AgeRangeChanges { get; set; }
        public string  CompetitorUrlChanges { get; set; }
        public string GenderChanges { get; set; }
        public string HouseholdIncomeChanges { get; set; }
        public string KeywordChanges { get; set; }
        public string NoteChanges { get; set; }
        public string ParentalStatusChanges { get; set; }
        public string SpecificAgeRanges { get; set; }
        public string SpecificGenders { get; set; }
        public string SpecificHouseholdIncomes { get; set; }
        public string SpecificParentalStatus { get; set; }


        public bool HasChanges => !string.IsNullOrEmpty(AgeRangeChanges) ||
                                  !string.IsNullOrEmpty(CompetitorUrlChanges) ||
                                  !string.IsNullOrEmpty(GenderChanges) ||
                                  !string.IsNullOrEmpty(HouseholdIncomeChanges) ||
                                  !string.IsNullOrEmpty(KeywordChanges) ||
                                  !string.IsNullOrEmpty(NoteChanges);


    }
}