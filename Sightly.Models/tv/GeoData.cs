﻿using System;

namespace Sightly.Models.tv
{
    public class GeoData
    {
        public string DmaName { get; set; }
        public Guid Id { get; set; }
        public long OriginalPopulation { get; set; }
    }
}