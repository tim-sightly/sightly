﻿using System;

namespace Sightly.Models.tv.extended.audiencegroups
{
    public class OrderTargetGenderGroup
    {
        public Guid OrderId { get; set; }
        public Guid GenderId { get; set; }
        public string GenderName { get; set; }
        public Boolean Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}
