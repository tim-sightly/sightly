﻿using System;

namespace Sightly.Models.tv.extended.audiencegroups
{
    public class OrderParentalStatusGroup
    {
        public Guid OrderId { get; set; }
        public Guid ParentalStatusId { get; set; }
        public string ParentalStatusName { get; set; }
        public Boolean Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}
