﻿using System;

namespace Sightly.Models.tv.extended.audiencegroups
{
    public class OrderHouseholdIncomeGroup
    {
        public Guid OrderId { get; set; }
        public int HouseholdIncomeGroupId { get; set; }
        public string GroupName { get; set; }
        public Boolean Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
    }
}
