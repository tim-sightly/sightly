﻿using System;

namespace Sightly.Models.tv.extended
{
    public class InfoExtended: Info
    {
        public string AccountName { get; set; }
        public string AdvertiserName { get; set; }
        public string AdvertiserCategoryName { get; set; }
        public string AdvertiserSubCategoryName { get; set; }
        public string OrderStatus { get; set; }

        public string OrderSubCategoryName { get; set; }

        public string CampaignName { get; set; }
        public string ObjectiveName { get; set; }

        public string SubmittedBy { get; set; }
        public DateTime SubmittedOn { get; set; }
    }
}
