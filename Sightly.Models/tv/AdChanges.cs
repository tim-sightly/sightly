﻿namespace Sightly.Models.tv
{
    public class AdChanges
    {
        public string AdPausedChange { get; set; }
        public string DisplayUrlChange { get; set; }
        public string DestinationUrlChange { get; set; }
        public string CompanionBannerChange { get; set; }
        public string NewAdChange { get; set; }

        public bool IsChangeExisting => !string.IsNullOrEmpty(AdPausedChange) ||
                                        !string.IsNullOrEmpty(DisplayUrlChange) ||
                                        !string.IsNullOrEmpty(DestinationUrlChange) ||
                                        !string.IsNullOrEmpty(CompanionBannerChange) ||
                                        !string.IsNullOrEmpty(NewAdChange);
    }
}