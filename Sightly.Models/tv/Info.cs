﻿using System;
using System.Security.AccessControl;

namespace Sightly.Models.tv
{
    public class Info
    {
        public Guid Account { get; set; }
        public Guid Advertiser { get; set; }
        public Guid AdvertiserSubCategory { get; set; }
        public Guid Campaign { get; set; }
        public Guid? OrderId { get; set; }
        public string OrderName { get; set; }
        public string OrderRefCode { get; set; }
        public Guid? Objective { get; set; }
        public decimal TotalBudget { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Paused { get; set; }
        public Guid? ParentOrderId { get; set; }
    }
}
 