﻿using System.Collections.Generic;

namespace Sightly.Models.tv
{
    public class Geo
    {
        public List<GeoData> SelectedServiceAreas { get; set; }


        public Geo()
        {
            SelectedServiceAreas = new List<GeoData>();
        }
    }
}