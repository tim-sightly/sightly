namespace Sightly.Models
{
    public class AdwordsBudgetOrderData
    {
        public long Id { get; set; }
        public string BillingAccountId { get; set; }
        public string BudgetOrderName { get; set; }
        public string PrimaryBillingId { get; set; }
        public double SpendingLimit { get; set; }
    }
}