using System;

namespace Sightly.Models
{
    public class TvBudgetGroupXrefPlacement
    {
        public Guid BudgetGroupXrefPlacementId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public Guid PlacementId { get; set; }
        public string AssignedBy { get; set; }
        public DateTime AssignedDate { get; set; }
    }
}