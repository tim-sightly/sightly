using System;

namespace Sightly.Models
{
    public class TvBudgetGroupAdwordsInfo
    {
        public Guid BudgetGroupAdwordsInfoId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public long AdwordsCustomerId { get; set; }
        public long AdwordsCampaignId { get; set; }
        public string AdwordsCampaignName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}