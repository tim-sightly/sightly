﻿using System;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace Sightly.Models
{
    public class DoubleVerifyImport
    {
        public long PlacementId { get; set; }
        public string PlacementName { get; set; }
        public DateTime StatDate { get; set; }
        public string DeviceType { get; set; }
        public long MonitoredImpressions { get; set; }

        public long? BrandSafeImpression_1x1 { get; set; }
        public long? FraudSivtFreeImpression_1x1 { get; set; }
        public long? InGeoImpression_1x1 { get; set; }

        public long? FraudSivtFreeImpression { get; set; }
        public long? VideoViewableImpression { get; set; }
    }
}