﻿using System;

namespace Sightly.Models
{
    public class GeoPerformanceData
    {
        public long CampaignId { get; set; }
        public DateTime StatDate { get; set; }
        public long AdGroupId { get; set; }
        public long CountryCriteriaId { get; set; }
        public long CityCriteriaId { get; set; }
        public long RegionCriteriaId { get; set; }
        public long MetroCriteriaId { get; set; }
        public long MostSpecificCriteriaId { get; set; }
        public decimal AverageCpc { get; set; }
        public decimal AverageCpm { get; set; }
        public long VideoViews { get; set; }
        public long Clicks { get; set; }
        public decimal Conversions { get; set; }
        public long ViewThroughConversions { get; set; }
        public decimal Cost { get; set; }
        public long Impressions { get; set; }
        public decimal CostPerAllConversion { get; set; }
        public decimal CostPerConversion { get; set; }
        public decimal CostPerConversionClick { get; set; }
        public decimal ValuePerAllConversion { get; set; }
        public decimal ValuePerConversion { get; set; }
        public decimal ValuePerConvertedClick { get; set; }

    }
}