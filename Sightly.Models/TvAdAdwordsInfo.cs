using System;

namespace Sightly.Models
{
    public class TvAdAdwordsInfo
    {
        public Guid AdAdwordsInfoId { get; set; }
        public Guid AdId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public string AdWordsAdName { get; set; }
        public long AdWordsAdId { get; set; }
        public long AdWordsCampaignId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}