﻿using System;
using Google.Api.Ads.AdWords.v201806;

namespace Sightly.Models
{
    public class AccountBudgetData
    {
        public DateTime EndDateTime { get; set; }
        public long Id { get; set; }
        public long SpendingLimit { get; set; }
        public DateTime StartDateTime { get; set; }
        public long TotalAdjustments { get; set; }
        public bool Active { get; set; } = true;
        public string BudgetOrderName { get; set; }

        public AccountBudgetData()
        {
        }

        public AccountBudgetData(BudgetOrder budgetOrder)
        {
            BudgetOrderName = budgetOrder.budgetOrderName;
            EndDateTime = Convert.ToDateTime(budgetOrder.endDateTime);
            Id = budgetOrder.id;
            SpendingLimit = budgetOrder.spendingLimit.microAmount;
            StartDateTime = Convert.ToDateTime(budgetOrder.startDateTime);
            TotalAdjustments = budgetOrder.totalAdjustments?.microAmount ?? 0;
        }
    }
}