﻿namespace Sightly.Models
{
    public class AdwordVideoData
    {
        public string VideoId { get; set; }
        public string VideoName { get; set; }
        public long AdId { get; set; }
        public string AdState { get; set; }
        public long AwCampaignId { get; set; }
        public string AwCampaignName { get; set; }
        public long AdGroupId { get; set; }
        public string AdGroupName { get; set; }
        public long VideoDuration { get; set; }
        public string VideoChannelId { get; set; }
    }
}