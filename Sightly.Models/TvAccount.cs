using System;

namespace Sightly.Models
{
    public class TvAccount
    {
        public Guid AccountId { get; set; }
        public string AccountName { get; set; }
        public string ShortCode { get; set; }
        public Guid AccountTypeId { get; set; }
        public Guid? BillingTypeId { get; set; }
        public  decimal? AccountMargin { get; set; }
        public decimal? SightlyMargin { get; set; }
        public bool? ApproverWorkflow { get; set; }
        public long? AdwordsMccAccount { get; set; }
        public Guid? ParentAccountId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }        
    }
}