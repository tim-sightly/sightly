using System;
using System.Collections.Generic;

namespace Sightly.Models
{
    public class TvBudgetGroup
    {
        public Guid BudgetGroupId { get; set; }
        public decimal BudgetGroupBudget { get; set; }
        public string BudgetGroupName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}