using System;

namespace Sightly.Models
{
    public class BudgetGroupGenderStats : BudgetGroupBaseStats
    {
        #region Properties
        
        public string AdwordsGenderName { get; set; }
        public Guid BudgetGroupTargetGenderStatsId { get; set; }
        #endregion
    }
}