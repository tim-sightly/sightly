using System;

namespace Sightly.Models
{
    public class TvVideoAssetVersion
    {
        public Guid VideoAssetVersionId { get; set; }
        public Guid VideoAssetId { get; set; }
        public string VideoAssetVersionName { get; set; }
        public string VideoAssetVersionRefCode { get; set; }
        public int VideoLengthInSeconds { get; set; }
        public string YouTubeId { get; set; }
        public string YouTubeUrl { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}