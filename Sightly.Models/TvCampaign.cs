using System;

namespace Sightly.Models
{
    public class TvCampaign
    {
        public Guid CampaignId { get; set; }
        public Guid AccountId { get; set; }
        public Guid AdvertiserId { get; set; }
        public string CampaignName { get; set; }
        public string CampaignRefCode { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }

    }
}