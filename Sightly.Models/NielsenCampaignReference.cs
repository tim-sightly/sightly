﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Sightly.Models
{
    public class NielsenCampaignReference
    {
        #region Public Properties
        [JsonProperty("campaignId")]
        public long CampaignId { get; set; }
        [JsonProperty("parentCampaignId")]
        public long ParentCampaignId { get; set; }
        [JsonProperty("campaignName")]
        public string CampaignName { get; set; }
        [JsonProperty("advertiserId")]
        public long AdvertiserId { get; set; }
        [JsonProperty("advertiserName")]
        public string AdvertiserName { get; set; }
        [JsonProperty("brandId")]
        public string BrandId { get; set; }
        [JsonProperty("brandName")]
        public string BrandName { get; set; }
        [JsonProperty("campaignStartDate")]
        public string CampaignStartDate { get; set; }
        [JsonProperty("campaignEndDate")]
        public string CampaignEndDate { get; set; }
        [JsonProperty("mediaType")]
        public string MediaType { get; set; }
        [JsonProperty("targetDemo")]
        public string TargetDemo { get; set; }
        [JsonProperty("targetStartAge")]
        public string TargetStartAge { get; set; }
        [JsonProperty("TargetEndAge")]
        public string TargetEndAge { get; set; }
        [JsonProperty("adReferenceCampaignId")]
        public string AdReferenceCampaignId { get; set; }
        [JsonProperty("countryCode")]
        public string CountryCode { get; set; }
        [JsonProperty("viewabilityEnabled")]
        public string ViewabilityEnabled { get; set; }
        [JsonProperty("viewabilityProviderName")]
        public string ViewabilityProviderName { get; set; }
        //public string ChildCampaignId { get; set; } = null;
        #endregion Public Properties

        #region Public Constructors
        public NielsenCampaignReference()
        { }
        #endregion Public Constructors
    }
}
