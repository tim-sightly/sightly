﻿using System;

namespace Sightly.Models
{
    public class VideoAssetUpdateData
    {
        public Guid VideoAssetId { get; set; }
        public string VideoName { get; set; }
        public string CreatedBy { get; set; }
    }
}