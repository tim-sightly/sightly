﻿namespace Sightly.Models
{
    public class AwBudget
    {
        public long BudgetId { get; set; }
        public long Amount { get; set; }
    }
}