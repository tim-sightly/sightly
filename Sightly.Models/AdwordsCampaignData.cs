﻿using System;

namespace Sightly.Models
{
    public class AdwordsCampaignData
    {
        public long AwCampaignId { get; set; }
        public string AwCampaignName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string CampaignStatus { get; set; }
    }
}