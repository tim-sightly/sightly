using System;

namespace Sightly.Models
{
    public class AdAwAdData
    {
        public Guid AdAdWordsInfoId { get; set; }
        public Guid AdId { get; set; }
        public string AdWordsAdName { get; set; }
        public long AdWordsAdId { get; set; }
        public long AdWordsCampaignId { get; set; }
        public Guid BudgetGroupId { get; set; }
    }
}