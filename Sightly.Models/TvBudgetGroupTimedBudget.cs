using System;

namespace Sightly.Models
{
    public class TvBudgetGroupTimedBudget
    {
        public Guid BudgetGroupTimedBudgetId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public decimal BudgetAmount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal Margin { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
        public Guid? BudgetGroupXrefPlacementId { get; set; }
        public Guid? OrderId { get; set; }
    }
}