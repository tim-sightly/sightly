﻿using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace Sightly.Models
{
    public class OrderEmailData
    {
        public string SubmittedBy { get; set; }
        public string ActionDate { get; set; }
        public string AccountName { get; set; }
        public string AdvertiserName { get; set; }
        public string CampaignName { get; set; }
        public string CreaterName { get; set; }
        public string OrderName { get; set; }
        public string OrderRefCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal TotalBudget { get; set; }

        public Attachment EmailAttachment { get; set; }

        public List<OrderChangeInfo> OrderChanges { get; set; }
        public string Notes { get; set; }
        public string CampaignManagerEmail { get; set; }

        public OrderEmailData()
        {
            OrderChanges = new List<OrderChangeInfo>();
        }

    }
}
