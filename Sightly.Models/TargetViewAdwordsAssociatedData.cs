using System;

namespace Sightly.Models
{
    public class TargetViewAdwordsAssociatedData
    {
        public Guid OrderId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public Guid AdId { get; set; }
        public long AdWordsCustomerId { get; set; }
        public long? AdwordsCampaignId { get; set; }
        public string AdwordsCampaignName { get; set; }
        public long? AdwordsAdId { get; set; }
        public string AdwordsAdName { get; set; }

        public bool UpdateCampaignId { get; set; }
        public bool UpdateAdId { get; set; }

    }
}