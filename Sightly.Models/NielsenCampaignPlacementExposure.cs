﻿using Newtonsoft.Json;

namespace Sightly.Models
{
    public class NielsenCampaignPlacementExposure
    {
        #region Public Properties
        [JsonProperty("campaignDataDate")]
        public string CampaignDataDate { get; set; }

        [JsonProperty("campaignId")]
        public long CampaignId { get; set; }

        [JsonProperty("platformId")]
        public int PlatformId { get; set; }
        
        [JsonProperty("demoId")]
        public int DemoId { get; set; }
        
        [JsonProperty("placementId")]
        public string PlacementId { get; set; }

        [JsonProperty("tagPlacementId")]
        public string TagPlacementId { get; set; }

        [JsonProperty("impressions")]
        public string Impressions { get; set; }

        [JsonProperty("universeEstimate")]
        public string UniverseEstimate { get; set; }

        #endregion Public Properties

        #region Public Constructors
        public NielsenCampaignPlacementExposure()
        { }
        #endregion Public Constructors
    }
}
