using System.Collections.Generic;

namespace Sightly.Models
{
    public class BudgetDateNewCampaignData
    {
        public string Email { get; set; }
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
        public List<long> ChangedCampaignIds { get; set; }
    }
}