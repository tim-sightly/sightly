using System;

namespace Sightly.Models
{
    public class TvBudgetGroupAndPlacement
    {
        public Guid BudgetGroupId { get; set; }
        public Guid? BudgetGroupXrefPlacementId { get; set; }
        public Guid? OrderId { get; set; }
    }
}