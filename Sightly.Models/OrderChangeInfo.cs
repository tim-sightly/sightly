﻿namespace Sightly.Models
{
    public class OrderChangeInfo
    {
        public string ChangeType { get; set; }
        public string ChangeDetails { get; set; }
    }
}