using System;

namespace Sightly.Models
{
    public class CustomerRawStatsByDayDate
    {
        public long CustomerId { get; set; }
        public DateTime StatDate { get; set; }
        public long AdClicks { get; set; }
        public long AgeClicks { get; set; }
        public long GenderClicks { get; set; }
        public long AdCost { get; set; }
        public long AgeCost { get; set; }
        public long GenderCost { get; set; }
        public long AdImpressions { get; set; }
        public long AgeImpressions { get; set; }
        public long GenderImpressions { get; set; }
        public long AdViews { get; set; }
        public long AgeViews { get; set; }
        public long GenderViews { get; set; }

        public decimal Clicks_AdoAge => AdClicks == 0 ? 0M : Convert.ToDecimal(AdClicks - AgeClicks)/ Convert.ToDecimal(AdClicks);
        public decimal Clicks_AdoGen => AdClicks == 0 ? 0M : Convert.ToDecimal(AdClicks - GenderClicks) / Convert.ToDecimal(AdClicks);
        public decimal Clicks_AgeoGen => AgeClicks == 0 ? 0M : Convert.ToDecimal(AgeClicks - GenderClicks) / Convert.ToDecimal(AgeClicks);
        public decimal Cost_AdoAge => AdCost == 0 ? 0M : Convert.ToDecimal(AdCost - AgeCost) / Convert.ToDecimal(AdCost);
        public decimal Cost_AdoGen => AdCost == 0 ? 0M : Convert.ToDecimal(AdCost - GenderCost) / Convert.ToDecimal(AdCost);
        public decimal Cost_AgeoGen => AgeCost == 0 ? 0M : Convert.ToDecimal(AgeCost - GenderCost) / Convert.ToDecimal(AgeCost);
        public decimal Impressions_AdoAge => AdImpressions == 0 ? 0M : Convert.ToDecimal(AdImpressions - AgeImpressions) / Convert.ToDecimal(AdImpressions);
        public decimal Impressions_AdoGen => AdImpressions == 0 ? 0M : Convert.ToDecimal(AdImpressions - GenderImpressions) / Convert.ToDecimal(AdImpressions);
        public decimal Impressions_AgeoGen => AgeImpressions == 0 ? 0M : Convert.ToDecimal(AgeImpressions - GenderImpressions) / Convert.ToDecimal(AgeImpressions);
        public decimal Views_AdoAge => AdViews == 0 ? 0M : Convert.ToDecimal(AdViews - AgeViews) / Convert.ToDecimal(AdViews);
        public decimal Views_AdoGen => AdViews == 0 ? 0M : Convert.ToDecimal(AdViews - GenderViews) / Convert.ToDecimal(AdViews);
        //public decimal Views_AgeoGen => AdClicks == 0 ? 0M : Convert.ToDecimal(AgeViews - GenderViews) / Convert.ToDecimal(AdClicks);
        public decimal Views_AgeoGen => AdViews == 0 ? 0M : Convert.ToDecimal(AgeViews - GenderViews) / Convert.ToDecimal(AdViews);
    }
}