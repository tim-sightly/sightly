﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.Models
{
    public class AdGroupPerformanceFreeData
    {
        public long AdGroupId { get; set; }
        public long CampaignId { get; set; }
        public DateTime StatDate { get; set; }
        public string AdGroupName { get; set; }
        public string CampaignName { get; set; }
        public long Impressions { get; set; }
        public long Views { get; set; }
        public decimal Cost { get; set; }
        public long Clicks { get; set; }
        public long CustomerId { get; set; }
    }
}
