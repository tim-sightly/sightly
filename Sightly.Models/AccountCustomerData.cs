﻿using System;

namespace Sightly.Models
{
    public class AccountCustomerData
    {
        public Guid ParentAccountId { get; set; }
        public Guid AccountId { get; set; }
        public Guid AdvertiserId { get; set; }
        public string AdvertiserName{ get; set; }
        public long AdwordsMccAccountId { get; set; }
        public long? AdwordsCustomerId { get; set; }

        public string CustomerId => $"{AdvertiserName} - {AdvertiserId}";
    }
}