using System;

namespace Sightly.Models
{
    public class BudgetGroupCampaignData
    {
        public Guid BudgetGroupId { get; set; }
        public long AdWordsCampaignId { get; set; }
        public string LastModifiedBy { get; set; }
    }
}