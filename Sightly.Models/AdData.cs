namespace Sightly.Models
{
    public class AdData
    {
        public long AdId { get; set; }
        public string AdName { get; set; }
    }
}