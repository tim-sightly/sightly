﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Sightly.Models
{
    public class AdwordsData
    {
        public long CustomerId { get; set; }
        public long AwCampaignId { get; set; }
        public string AwCampaignName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long Budget { get; set; }
        public List<AdwordsAd> AdwordsAds { get; set; }
        public List<string> Labels { get; set; }
        
        /// <summary>
        /// We need the ability to remove a ' - GUID' from the back of a campaign name
        /// </summary>
        public string SafeCampaignName
        {
            get
            {
                return Regex.Replace(AwCampaignName,
                    " - ([0-9A-Fa-f]{8}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{4}[-][0-9A-Fa-f]{12})", "");
            }
        }
        public AdwordsData()
        {
            AdwordsAds = new List<AdwordsAd>();
            Labels = new List<string>();
        }
    }
}
