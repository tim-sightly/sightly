﻿using System;

namespace Sightly.Models
{
    public class TvCampaignAdWordsInfo
    {
        public Guid CampaignAdWordsInfoId { get; set; }

        public Guid? AdvertiserId { get; set; }

        public string AdvertiserName { get; set; }

        public Guid CampaignId { get; set; }

        public long? AdWordsCustomerId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }

        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
        public string CampaignMangerEmail { get; set; }

        public string AdWordsCustomerName => $"{this.AdvertiserName} - {this.AdvertiserId}";
    }
}
