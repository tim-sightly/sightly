﻿using System;
using System.Runtime.ConstrainedExecution;

namespace Sightly.Models
{
    public class AdGroupPerformanceData : IVideoPerformanceData
    {
        public long AdGroupId { get; set; }
        public long CampaignId { get; set; }
        public DateTime StatDate { get; set; }
        public string AdGroupName { get; set; }
        public string CampaignName { get; set; }
        public long Impressions { get; set; }
        public long Views { get; set; }
        public decimal Cost { get; set; }
        public long Clicks { get; set; }
        public long CustomerId { get; set; }
        public decimal VideoQuartile25Rate { get; set; }
        public decimal VideoQuartile50Rate { get; set; }
        public decimal VideoQuartile75Rate { get; set; }
        public decimal VideoQuartile100Rate { get; set; }
    }
}