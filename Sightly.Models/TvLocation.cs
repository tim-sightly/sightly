using System;

namespace Sightly.Models
{
    public class TvLocation
    {
        public Guid LocationId { get; set; }
        public Guid AccountId { get; set; }
        public string LocationName { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}