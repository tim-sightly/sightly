using System;

namespace Sightly.Models
{
    /// <summary>
    /// Old TargetView Code
    /// </summary>
    public class BudgetGroupBaseStats
    {
        
        #region Properties

        /// <summary>
        /// Property that holds the actual spend
        /// of the stats.
        /// </summary>
        public decimal ActualSpend { get; set; }

        /// <summary>
        /// Property that holds the audience's retention
        /// up to 25% of the video of the stats.
        /// </summary>
        public int AudienceRetention25 { get; set; }

        /// <summary>
        /// Property that holds the audience's retention
        /// up to 50% of the video of the stats.
        /// </summary>
        public int AudienceRetention50 { get; set; }

        /// <summary>
        /// Property that holds the audience's retention
        /// up to 75% of the video of the stats.
        /// </summary>
        public int AudienceRetention75 { get; set; }

        /// <summary>
        /// Property that holds the audience's retention
        /// up to 100% of the video of the stats.
        /// </summary>
        public int AudienceRetention100 { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// budget group that owns the stats.
        /// </summary>
        public Guid BudgetGroupId { get; set; }

        /// <summary>
        /// Property that holds the number of
        /// clicks made.
        /// </summary>
        public long Clicks { get; set; }

        /// <summary>
        /// Property that holds the click-through-rate
        /// of the stats.
        /// </summary>
        public double ClickThroughRate { get; set; }

        /// <summary>
        /// Property that holds the completed views
        /// of the stats.
        /// </summary>
        public long CompletedViews { get; set; }

        /// <summary>
        /// Property that holds the conversions
        /// of the stats.
        /// </summary>
        public double Conversions { get; set; }

        /// <summary>
        /// Property that holds the total estimated
        /// cost of the stats.
        /// </summary>
        public decimal EstimatedCost { get; set; }

        /// <summary>
        /// Property that holds the impressions
        /// of the stats.
        /// </summary>
        public long Impressions { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// order where the budget group is associated.
        /// </summary>
        public Guid OrderId { get; set; }

        /// <summary>
        /// Property that holds the date when
        /// the stats was gathered.
        /// </summary>
        public DateTime StatDate { get; set; }

        /// <summary>
        /// Property that holds the view rate
        /// of the stats.
        /// </summary>
        public double ViewRate { get; set; }

        /// <summary>
        /// Property that holds the view through
        /// conversions of the stats.
        /// </summary>
        public long ViewThroughConversions { get; set; }

        #endregion
    }
}