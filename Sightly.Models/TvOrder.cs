using System;

namespace Sightly.Models
{
    public class TvOrder
    {
        public Guid OrderId { get; set; }
        public string OrderName { get; set; }
        public string OrderRefCode { get; set; }
        public Guid AccountId { get; set; }
        public Guid CampaignId { get; set; }
        public Guid AdvertiserId { get; set; }
        public decimal CampaignBudget { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal CustomerAccountMargin { get; set; }
        public decimal SightlyMargin { get; set; }
        public Guid OrderStatusId { get; set; }
        public Guid ParentOrderId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
        public Guid? ObjectiveCategoryId { get; set; }
        public bool Paused { get; set; }
    }
}