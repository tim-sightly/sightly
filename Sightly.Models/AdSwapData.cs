﻿namespace Sightly.Models
{
    public class AdSwapData
    {
        public string Email { get; set; }
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
    }
}