﻿using System;

namespace Sightly.Models
{
    public class AdvertiserEntityData
    {
        public Guid AccountId { get; set; }
        public Guid AdvertiserId { get; set; }
    }
}