﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Sightly.Models
{
    public class NielsenDemographicReference
    {
        #region Public Properties
        [JsonProperty("demoId")]
        public long DemoId { get; set; }

        [JsonProperty("demoGroupGender")]
        public string DemoGroupGender { get; set; }

        [JsonProperty("demoGroupAlphaCode")]
        public string DemoGroupAlphaCode { get; set; }

        [JsonProperty("demoGroupStartAge")]
        public int DemoGroupStartAge { get; set; }

        [JsonProperty("demoGroupEndAge")]
        public int DemoGroupEndAge { get; set; }
        #endregion Public Properties

        #region Public Properties
        public NielsenDemographicReference()
        { }
        #endregion Public Properties
    }
}
