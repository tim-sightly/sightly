using System;

namespace Sightly.Models
{
    public class TvVideoAsset
    {
        public Guid VideoAssetId { get; set; }
        public Guid AccountId { get; set; }
        public Guid AdvertiserId { get; set; }
        public string VideoAssetFileName { get; set; }
        public string VideoAssetName { get; set; }
        public string VideoAssetRefCode { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}