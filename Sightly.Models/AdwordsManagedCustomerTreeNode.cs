﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sightly.Models
{
    public class AdwordsManagedCustomerTreeNode
    {
        public AdwordsManagedCustomerTreeNode()
        {
            ChildCustomers = new List<AdwordsManagedCustomerTreeNode>();
        }

        /// <summary>
        /// Gets or sets the parent node.
        /// </summary>
        public AdwordsManagedCustomerTreeNode ParentNode { get; set; }

        /// <summary>
        /// Gets or sets the account.
        /// </summary>
        public AdwordsCustomerData Customer { get; set; }

        /// <summary>
        /// Gets the child accounts.
        /// </summary>
        public List<AdwordsManagedCustomerTreeNode> ChildCustomers { get; private set; }

        /// <summary>
        /// Returns a <see cref="System.String"/> that represents this instance.
        /// </summary>
        /// <returns>
        /// A <see cref="System.String"/> that represents this instance.
        /// </returns>
        public override String ToString()
        {
            return String.Format("{0}, {1}", Customer.AwCustomerId, Customer.AwCustomerName);
        }

        /// <summary>
        /// Returns a string representation of the current level of the tree and
        /// recursively returns the string representation of the levels below it.
        /// </summary>
        /// <param name="depth">The depth of the node.</param>
        /// <param name="sb">The String Builder containing the tree
        /// representation.</param>
        /// <returns>The tree string representation.</returns>
        public StringBuilder ToTreeString(int depth, StringBuilder sb)
        {
            sb.Append('-', depth * 2);
            sb.Append(this);
            sb.AppendLine();
            foreach (AdwordsManagedCustomerTreeNode childAccount in ChildCustomers)
            {
                childAccount.ToTreeString(depth + 1, sb);
            }
            return sb;
        }
    }
}
