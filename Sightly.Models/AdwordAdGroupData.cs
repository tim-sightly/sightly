﻿namespace Sightly.Models
{
    public class AdwordAdGroupData
    {
        public long AwCampaignId { get; set; }
        public string AwCampaignName { get; set; }

        public long AdGroupId { get; set; }
        public string AdGroupName { get; set; }
    }
}
