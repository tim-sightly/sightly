using System;

namespace Sightly.Models
{
    public class AdMetaData
    {
        public Guid AccountId { get; set; }
        public Guid AdvertiserId { get; set; }
        public Guid BudgetGroupId { get; set; }

    }
}