﻿using System.Collections.Generic;

namespace Sightly.Models
{
    public class AdwordsCustomerData
    {
        public long AwCustomerId { get; set; }
        public string AwCustomerName { get; set; }

        public bool CanManageClients { get; set; }
        public List<AdwordsCampaignData> Campaigns { get; set; }

        public AdwordsCustomerData()
        {
            Campaigns = new List<AdwordsCampaignData>();
        }
    }
}