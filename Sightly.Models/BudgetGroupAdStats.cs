using System;

namespace Sightly.Models
{
    /// <summary>
    /// Old TargetView Code
    /// </summary>
    public class BudgetGroupAdStats : BudgetGroupBaseStats
    {
        #region Properties
        /// <summary>
        /// Property that holds the ID of the
        /// ad related to the stats.
        /// </summary>
        public Guid AdId { get; set; }

        /// <summary>
        /// Property that holds the average cost
        /// per view of the stats.
        /// </summary>
        public double AverageCpv { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// budget group ad stats record.
        /// </summary>
        public Guid BudgetGroupAdStatsId { get; set; }

        /// <summary>
        /// Property that holds the partial
        /// views of the stats.
        /// </summary>
        public decimal PartialViews { get; set; }

        /// <summary>
        /// Property that holds the total view
        /// time of the stats.
        /// </summary>
        public long ViewTime { get; set; }
        #endregion
    }

    public class BudgetGroupStats : BudgetGroupBaseStats
    {
        public decimal Amount { get; set; }

        /// <summary>
        /// Property that holds the average CPC
        /// for a budget group stats.
        /// </summary>
        public decimal AverageCpc { get; set; }

        /// <summary>
        /// Property that holds the average CPE
        /// for a budget group stats.
        /// </summary>
        public double AverageCpe { get; set; }

        /// <summary>
        /// Property that holds the average CPM
        /// for a budget group stats.
        /// </summary>
        public decimal AverageCpm { get; set; }

        /// <summary>
        /// Property that holds the average CPV
        /// for a budget group stats.
        /// </summary>
        public double AverageCpv { get; set; }

        /// <summary>
        /// Property that holds the average position
        /// for a budget group stats.
        /// </summary>
        public double AveragePosition { get; set; }

        public Guid BudgetGroupStatsId { get; set; }


        /// <summary>
        /// Property that holds the
        /// engagements of the budget group stats.
        /// </summary>
        public long Engagements { get; set; }

        /// <summary>
        /// Property that holds the
        /// interactions of the budget group stats.
        /// </summary>
        public long Interactions { get; set; }


    }

}