using System;

namespace Sightly.Models
{
    public class TvOrderTargetAgeGroup
    {
        public Guid OrderId { get; set; }
        public Guid AgeGroupId { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDatetime { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDatetime { get; set; }
    }
}