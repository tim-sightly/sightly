using System;

namespace Sightly.Models
{
    public class BudgetGroupDeviceStats : BudgetGroupBaseStats
    {
        #region Properties
        public string AdWordsDeviceTypeName { get; set; }

        public double AverageCpv { get; set; }

        public Guid BudgetGroupDeviceStatsId { get; set; }

        public decimal PartialViews { get; set; }

        public long ViewTime { get; set; }
        #endregion
    }
}