﻿using System;

namespace Sightly.Models
{
    public class AgeRangePerformanceFreeData
    {
        public long AdGroupId { get; set; }
        public string AgeRangeName { get; set; }
        public long CampaignId { get; set; }
        public DateTime StatDate { get; set; }
        public long Impressions { get; set; }
        public long Views { get; set; }
        public decimal Cost { get; set; }
        public long Clicks { get; set; }
        public long CustomerId { get; set; }
    }
}