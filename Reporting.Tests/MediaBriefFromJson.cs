﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using Reporting.DAL;
using Reporting.DAL.DTO;
using Reporting.DAL.DTO.NewTargetview;

namespace Reporting.Tests
{
    [TestClass]
    public class MediaBriefFromJson
    {
        private IReportStore _reportStore;

        [TestInitialize]
        public void InitializeTest()
        {
            _reportStore = new ReportStore();
        }


        [TestMethod]
        public void GetJsonIntoOrderObjectTest()
        {
//            Guid orderId =  Guid.Parse("5EE8FD27-000C-49F0-86E9-A893013B5882");

            var jsonData = _reportStore.GetJsonOfSavedOrder(Guid.Parse("5EE8FD27-000C-49F0-86E9-A893013B5882"));

            //Assert.IsNotNull(jsonData);



            var order = JsonConvert.DeserializeObject<Order>(jsonData);

            Assert.AreEqual("MMG - Ballas Buick GMC - 2018", order.Info.OrderName);

        }
    }
}
