﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Reporting.DAL;
using Reporting.DAL.DTO;

namespace Reporting.Tests
{
    [TestClass]
    public class ResellerOrderPlacementDownloadTest
    {
        private IReportStore _reportStore;

        [TestInitialize]
        public void InitializeTest()
        {
            _reportStore = new ReportStore();
        }

        [TestMethod]
        public void GetOrderPlacementWithData()
        {
            var orderId = Guid.Parse("60EBF3C4-0E28-4EC5-B89F-A7DE00F61A37");
            ResellerCampaignUploader uploader = _reportStore.GetResellerCampaignUploaderData(orderId);

            Assert.IsNotNull(uploader);
            Assert.IsNotNull(uploader.SightlyMargin);
        }
    }
}
