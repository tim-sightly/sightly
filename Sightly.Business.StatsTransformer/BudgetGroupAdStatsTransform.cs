﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;


namespace Sightly.Business.StatsTransformer
{
    public class BudgetGroupAdStatsTransform : IBudgetGroupAdStatsTransform
    {
        private readonly IStatRepository _statRepository;

        public BudgetGroupAdStatsTransform(IStatRepository statRepository)
        {
            _statRepository = statRepository;
        }

        #region Public Methods

        public List<BudgetGroupAdStats> Execute(List<AdDevicePerformanceData> rawAdDevicePerformance)
        {
            if (rawAdDevicePerformance == null || 
                rawAdDevicePerformance.Count <= 0)
                return null;

            var budgetGroupAdStats = new List<BudgetGroupAdStats>();

            var distinctCampaignIds = rawAdDevicePerformance.GroupBy(adPerf => adPerf.CampaignId)
                .Select(cam => cam.First())
                .ToList();
            distinctCampaignIds.ForEach(campId =>
            {
                var awTvAssociationDataList = _statRepository.GetAdwordsTvAssociationData(campId.CampaignId);
                budgetGroupAdStats.AddRange(TranformBudgetGroupAdStatsFromRaw(awTvAssociationDataList, rawAdDevicePerformance)); 
            });
            return budgetGroupAdStats;
        }

        #endregion

        #region Private Methods

        private List<BudgetGroupAdStats> TranformBudgetGroupAdStatsFromRaw(List<AdwordsTvAssociationData> awTvAssociationDataList, List<AdDevicePerformanceData> rawAdDevicePerformanceList)
        {
            var budgetGroupAdStats = new List<BudgetGroupAdStats>();
            var tvAdMappings = awTvAssociationDataList.GroupBy(tvData => new {tvData.AdId, tvData.BudgetGroupId, tvData.OrderId}).ToList();

            tvAdMappings.ForEach(tvAdMap =>
                {
                    var rawStats = rawAdDevicePerformanceList.Where(stats => tvAdMap.Any(map => map.AwAdId == stats.AdId)).ToList();

                    if (rawStats.Count <= 0) return;
                    var adStats = TransformRowData(tvAdMap.Key.AdId, tvAdMap.Key.BudgetGroupId, tvAdMap.Key.OrderId, rawStats);

                    budgetGroupAdStats.Add(adStats);
                }
            );

            return budgetGroupAdStats;
        }

        private BudgetGroupAdStats TransformRowData(Guid adId, Guid budgetGroupId, Guid orderId, List<AdDevicePerformanceData> adData)
        {
            var adLength = _statRepository.GetAdVideoLengthByAdId(adId) ?? 0;
            var impressions = adData.Sum(ad => ad.Impressions);
            var views = (adData.Sum(ad => ad.Views));
            var clicks = (adData.Sum(ad => ad.Clicks));
            var cost = StatsTransformerUtility.ConvertToMoney(adData.Sum(ad => ad.Cost));
            var averageCpv = views < 0.0 ? cost / views: -1.0M;
            var avgCpv = StatsTransformerUtility.ConvertToMoney(averageCpv);
            var avgVideosPlayedTo25Percent = adData.Average(ad => ad.VideoQuartile25Rate);
            var avgVideosPlayedTo50Percent = adData.Average(ad => ad.VideoQuartile50Rate);
            var avgVideosPlayedTo75Percent = adData.Average(ad => ad.VideoQuartile75Rate);
            var avgVideosPlayedTo100Percent = adData.Average(ad => ad.VideoQuartile100Rate);
            var partialViews = new  Calculators.PartialViewsCalculator().Execute(
                adLength, impressions, views, avgCpv,
                avgVideosPlayedTo25Percent, avgVideosPlayedTo50Percent,
                avgVideosPlayedTo75Percent, avgVideosPlayedTo100Percent
            );
            var viewTime = new Calculators.ViewTimeCalculator().Execute(
                adLength, impressions, views,
                avgVideosPlayedTo25Percent, avgVideosPlayedTo50Percent,
                avgVideosPlayedTo75Percent, avgVideosPlayedTo100Percent
            );

            return new BudgetGroupAdStats()
            {
                AdId = adId,
                AudienceRetention25 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo25Percent),
                AudienceRetention50 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo50Percent),
                AudienceRetention75 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo75Percent),
                AudienceRetention100 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo100Percent),
                AverageCpv = Convert.ToDouble(avgCpv),
                BudgetGroupId = budgetGroupId,
                Clicks = clicks,
                ClickThroughRate = StatsTransformerUtility.CalculateClickThroughRate(clicks, impressions),
                CompletedViews = views,
                //Conversions = (adData.Sum(ad => ad.Conversions)),
                EstimatedCost = 0,
                Impressions = impressions,
                ActualSpend = cost,
                OrderId = orderId,
                PartialViews = partialViews,
                StatDate =  adData[0].StatDate,
                ViewRate = StatsTransformerUtility.CalculateViewRate(views, impressions),
                //ViewThroughConversions = adData.Sum(ad => ad.ViewThroughConversions),
                ViewTime = viewTime
            };
        }

        #endregion


    }
}