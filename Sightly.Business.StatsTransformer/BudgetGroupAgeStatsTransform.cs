using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.StatsTransformer
{
    public class BudgetGroupAgeStatsTransform : IBudgetGroupAgeStatsTransform
    {
        private readonly IStatRepository _statRepository;

        public BudgetGroupAgeStatsTransform(IStatRepository statRepository)
        {
            _statRepository = statRepository;
        }

        #region Public Methods

        public List<BudgetGroupAgeStats> Execute(List<AgeRangePerformanceFreeData> rawAgePerformanceList)
        {
            if (rawAgePerformanceList == null ||
                rawAgePerformanceList.Count <= 0)
                return null;
            var budgetGroupAgeStats = new List<BudgetGroupAgeStats>();
            var distinctCampaignIds = rawAgePerformanceList.GroupBy(agePerf => agePerf.CampaignId)
                .Select(cam => cam.First())
                .ToList();
            distinctCampaignIds.ForEach(campId =>
            {
                var awTvAssociationDataList = _statRepository.GetAdwordsTvAssociationData(campId.CampaignId);
                budgetGroupAgeStats.AddRange(TransformBudgetGroupAgeStatsFromRaw(awTvAssociationDataList, rawAgePerformanceList));
            });


            return budgetGroupAgeStats;
        }


        #endregion

        #region Private Methods
        private List<BudgetGroupAgeStats> TransformBudgetGroupAgeStatsFromRaw(List<AdwordsTvAssociationData> awTvAssociationDataList, List<AgeRangePerformanceFreeData> rawAgePerformanceList)
        {
            var budgetGroupAgeStats = new List<BudgetGroupAgeStats>();
            var tvAgeMappings = awTvAssociationDataList.GroupBy(tvData => new {tvData.AwCampaignId, tvData.BudgetGroupId, tvData.OrderId}).ToList();

            tvAgeMappings.ForEach(tvAgeMap =>
            {
                var unknownStats = rawAgePerformanceList.Where(rawAge => rawAge.AgeRangeName == "Undetermined" && rawAge.CampaignId == tvAgeMap.Key.AwCampaignId).ToList();
                if(unknownStats.Count > 0)
                    budgetGroupAgeStats.Add(CompileAgeStats(tvAgeMap.Key.BudgetGroupId,tvAgeMap.Key.OrderId, unknownStats));

                var o18To24Stats = rawAgePerformanceList.Where(rawAge => rawAge.AgeRangeName == "18-24" && rawAge.CampaignId == tvAgeMap.Key.AwCampaignId).ToList();
                if (o18To24Stats.Count > 0)
                    budgetGroupAgeStats.Add(CompileAgeStats(tvAgeMap.Key.BudgetGroupId, tvAgeMap.Key.OrderId, o18To24Stats));

                var o25To34Stats = rawAgePerformanceList.Where(rawAge => rawAge.AgeRangeName == "25-34" && rawAge.CampaignId == tvAgeMap.Key.AwCampaignId).ToList();
                if (o25To34Stats.Count > 0)
                    budgetGroupAgeStats.Add(CompileAgeStats(tvAgeMap.Key.BudgetGroupId, tvAgeMap.Key.OrderId, o25To34Stats));

                var o35To44Stats = rawAgePerformanceList.Where(rawAge => rawAge.AgeRangeName == "35-44" && rawAge.CampaignId == tvAgeMap.Key.AwCampaignId).ToList();
                if (o35To44Stats.Count > 0)
                    budgetGroupAgeStats.Add(CompileAgeStats(tvAgeMap.Key.BudgetGroupId, tvAgeMap.Key.OrderId, o35To44Stats));

                var o45To54Stats = rawAgePerformanceList.Where(rawAge => rawAge.AgeRangeName == "45-54" && rawAge.CampaignId == tvAgeMap.Key.AwCampaignId).ToList();
                if (o45To54Stats.Count > 0)
                    budgetGroupAgeStats.Add(CompileAgeStats(tvAgeMap.Key.BudgetGroupId, tvAgeMap.Key.OrderId, o45To54Stats));

                var o55To64Stats = rawAgePerformanceList.Where(rawAge => rawAge.AgeRangeName == "55-64" && rawAge.CampaignId == tvAgeMap.Key.AwCampaignId).ToList();
                if (o55To64Stats.Count > 0)
                    budgetGroupAgeStats.Add(CompileAgeStats(tvAgeMap.Key.BudgetGroupId, tvAgeMap.Key.OrderId, o55To64Stats));

                var o65PlusStats = rawAgePerformanceList.Where(rawAge => rawAge.AgeRangeName == "65 or more" && rawAge.CampaignId == tvAgeMap.Key.AwCampaignId).ToList();
                if (o65PlusStats.Count > 0)
                    budgetGroupAgeStats.Add(CompileAgeStats(tvAgeMap.Key.BudgetGroupId, tvAgeMap.Key.OrderId, o65PlusStats));

            });


            return budgetGroupAgeStats;
        }

        private BudgetGroupAgeStats CompileAgeStats(Guid budgetGroupId, Guid orderId, List<AgeRangePerformanceFreeData> ageStats)
        {
            return TransformRowData(budgetGroupId, orderId, ageStats);
        }

        private BudgetGroupAgeStats TransformRowData(Guid budgetGroupId, Guid orderId, List<AgeRangePerformanceFreeData> ageStats)
        {
            var impressions = ageStats.Sum(age => age.Impressions);
            var views = ageStats.Sum(age => age.Views);
            var clicks = ageStats.Sum(age => age.Clicks);
            var cost = StatsTransformerUtility.ConvertToMoney(ageStats.Sum(age => age.Cost));
            
            return new BudgetGroupAgeStats
            {
                ActualSpend = cost,
                AdWordsAgeGroupName = ageStats.First().AgeRangeName,
                BudgetGroupId = budgetGroupId,
                Clicks = clicks,
                ClickThroughRate = CalculateClickThroughRate(clicks, impressions),
                CompletedViews = views,
                Conversions = 0.0D, ////Conversions = ageStats.Sum(age => age.Conversions),
                EstimatedCost = 0,
                Impressions = impressions,
                OrderId = orderId,
                StatDate = ageStats.First().StatDate,
                ViewRate = StatsTransformerUtility.CalculateViewRate(views, impressions),
                ViewThroughConversions = 0L
            };

        }

        private double CalculateClickThroughRate(long clicks, long impressions)
        {
            if (impressions > 0)
                return StatsTransformerUtility.ParseDouble(clicks) / StatsTransformerUtility.ParseDouble(impressions);

            return 0;
        }

        #endregion
    }
}