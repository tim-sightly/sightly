using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.StatsTransformer
{
    public class BudgetGroupDeviceStatsTransform : IBudgetGroupDeviceStatsTransform
    {
        private readonly IStatRepository _statRepository;

        public BudgetGroupDeviceStatsTransform(IStatRepository statRepository)
        {
            _statRepository = statRepository;
        }

        #region Public Methods

        public List<BudgetGroupDeviceStats> Execute(List<AdDevicePerformanceData> rawAdDevicePerformanceList)
        {
            if (rawAdDevicePerformanceList == null ||
                rawAdDevicePerformanceList.Count <= 0)
                return null;

            var budgetGroupDeviceStats = new List<BudgetGroupDeviceStats>();
            var distinctCampaignIds = rawAdDevicePerformanceList.GroupBy(devPerf => devPerf.CampaignId)
                .Select(cam => cam.First())
                .ToList();

            distinctCampaignIds.ForEach(campId =>
            {
                var awTvAssociationDataList = _statRepository.GetAdwordsTvAssociationData(campId.CampaignId);
                budgetGroupDeviceStats.AddRange(TranformBudgetGroupDeviceStatsFromRaw(awTvAssociationDataList, rawAdDevicePerformanceList));
            });

            return budgetGroupDeviceStats;

        }

        #endregion

        #region Private Methods
        private List<BudgetGroupDeviceStats> TranformBudgetGroupDeviceStatsFromRaw(List<AdwordsTvAssociationData> awTvAssociationDataList, List<AdDevicePerformanceData> rawAdDevicePerformanceList)
        {
            var budgetGroupDeviceStats = new List<BudgetGroupDeviceStats>();

            var tvDeviceMappings = awTvAssociationDataList.GroupBy(tvData => new { tvData.AwCampaignId, tvData.BudgetGroupId, tvData.OrderId}).ToList();

            tvDeviceMappings.ForEach(tvDeviceMap =>
            {
                var compStats = rawAdDevicePerformanceList.Where( rawDev => rawDev.DeviceName== "Computers" && rawDev.CampaignId == tvDeviceMap.Key.AwCampaignId).ToList();
                if (compStats.Count > 0)
                {
                    budgetGroupDeviceStats.Add(CompileDeviceStats(tvDeviceMap.Key.BudgetGroupId, tvDeviceMap.Key.OrderId, compStats));
                }


                var mobilStats = rawAdDevicePerformanceList.Where(rawDev => rawDev.DeviceName == "Mobile devices with full browsers" && rawDev.CampaignId == tvDeviceMap.Key.AwCampaignId).ToList();
                if (mobilStats.Count > 0)
                {
                    budgetGroupDeviceStats.Add(CompileDeviceStats(tvDeviceMap.Key.BudgetGroupId, tvDeviceMap.Key.OrderId, mobilStats));
                }


                var tabletStats = rawAdDevicePerformanceList.Where(rawDev => rawDev.DeviceName == "Tablets with full browsers" && rawDev.CampaignId == tvDeviceMap.Key.AwCampaignId).ToList();
                if (tabletStats.Count > 0)
                {
                    budgetGroupDeviceStats.Add(CompileDeviceStats(tvDeviceMap.Key.BudgetGroupId, tvDeviceMap.Key.OrderId, tabletStats));
                }


                var otherStats = rawAdDevicePerformanceList.Where(rawDev => rawDev.DeviceName == "Other" && rawDev.CampaignId == tvDeviceMap.Key.AwCampaignId).ToList();
                if (otherStats.Count > 0)
                {
                    budgetGroupDeviceStats.Add(CompileDeviceStats(tvDeviceMap.Key.BudgetGroupId, tvDeviceMap.Key.OrderId, otherStats));
                }
                
            });

            return budgetGroupDeviceStats;
        }

        private BudgetGroupDeviceStats CompileDeviceStats(Guid budgetGroupId, Guid orderId, List<AdDevicePerformanceData> devStats)
        {
            return TransformRowData(budgetGroupId, orderId, devStats);
        }

        private BudgetGroupDeviceStats TransformRowData(Guid budgetGroupId, Guid orderId, List<AdDevicePerformanceData> devStats)
        {
            var impressions = devStats.Sum(dev => dev.Impressions);
            var views = devStats.Sum(dev => dev.Views);
            var clicks = devStats.Sum(dev => dev.Clicks);
            var cost = StatsTransformerUtility.ConvertToMoney(devStats.Sum(dev => dev.Cost));
            var averageCpv = views < 0 ? cost / views : -1.0M;
            var avgCpv = StatsTransformerUtility.ConvertToMoney(averageCpv);
            var avgVideosPlayedTo25Percent = devStats.Average(ad => ad.VideoQuartile25Rate);
            var avgVideosPlayedTo50Percent = devStats.Average(ad => ad.VideoQuartile50Rate);
            var avgVideosPlayedTo75Percent = devStats.Average(ad => ad.VideoQuartile75Rate);
            var avgVideosPlayedTo100Percent = devStats.Average(ad => ad.VideoQuartile100Rate);

            return new BudgetGroupDeviceStats
            {
                ActualSpend = cost,
                AdWordsDeviceTypeName = devStats.First().DeviceName,
                AudienceRetention25 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo25Percent),
                AudienceRetention50 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo50Percent),
                AudienceRetention75 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo75Percent),
                AudienceRetention100 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo100Percent),
                AverageCpv = Convert.ToDouble(avgCpv),
                BudgetGroupId = budgetGroupId,
                Clicks = clicks,
                ClickThroughRate = CalculateClickThroughRate(clicks, impressions),
                CompletedViews = views,
                Conversions = 0.0D, //devStats.Sum(dev => dev.Conversions),
                EstimatedCost  = 0,
                Impressions = impressions,
                OrderId = orderId,
                PartialViews = 0.0M,
                StatDate = devStats.First().StatDate,

                ViewRate = StatsTransformerUtility.CalculateViewRate(views, impressions),
                ViewThroughConversions = 0L, //adData.Sum(ad => ad.ViewThroughConversions),
                ViewTime = 0L//viewTime

            };

        }

        private double CalculateClickThroughRate(long clicks, long impressions)
        {
            if (impressions > 0)
                return StatsTransformerUtility.ParseDouble(clicks) / StatsTransformerUtility.ParseDouble(impressions);

            return 0;
        }

        #endregion

    }
}