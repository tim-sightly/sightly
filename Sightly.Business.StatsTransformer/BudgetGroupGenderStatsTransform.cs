using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.StatsTransformer
{
    public class BudgetGroupGenderStatsTransform : IBudgetGroupGenderStatsTransform
    {
        private readonly IStatRepository _statRepository;

        public BudgetGroupGenderStatsTransform(IStatRepository statRepository)
        {
            _statRepository = statRepository;
        }

        #region Public Methods
        public List<BudgetGroupGenderStats> Execute(List<GenderPerformanceFreeData> rawGenderPerformanceList)
        {
            if (rawGenderPerformanceList == null ||
                rawGenderPerformanceList.Count <= 0)
                return null;

            var budgetGroupGenderStats = new List<BudgetGroupGenderStats>();

            var distinctCampaignIds = rawGenderPerformanceList.GroupBy(genPerf => genPerf.CampaignId)
                .Select(cam => cam.First())
                .ToList();
            distinctCampaignIds.ForEach(campId =>
            {
                var awTvAssociatedDateList = _statRepository.GetAdwordsTvAssociationData(campId.CampaignId);
                budgetGroupGenderStats.AddRange(TransformBudgetGroupGenderStatsFromRaw(awTvAssociatedDateList, rawGenderPerformanceList));
            });

            return budgetGroupGenderStats;
        }
        #endregion

        #region Private Methods

        private List<BudgetGroupGenderStats> TransformBudgetGroupGenderStatsFromRaw(List<AdwordsTvAssociationData> awTvAssociatedDateList, List<GenderPerformanceFreeData> rawGenderPerformanceList)
        {
            var budgetGroupGenderStats = new List<BudgetGroupGenderStats>();
            var tvGenderMapping = awTvAssociatedDateList.GroupBy(tvData => new { tvData.AwCampaignId, tvData.BudgetGroupId, tvData.OrderId }).ToList();

            tvGenderMapping.ForEach(tvGenderMap =>
            {
                var unknownStats = rawGenderPerformanceList.Where(rawGender =>  rawGender.GenderName == "Undetermined" && rawGender.CampaignId == tvGenderMap.Key.AwCampaignId).ToList();
                if (unknownStats.Count > 0)
                    budgetGroupGenderStats.Add(CompileGenderStats(tvGenderMap.Key.BudgetGroupId, tvGenderMap.Key.OrderId, unknownStats));

                var femaleStats = rawGenderPerformanceList.Where(rawGender =>  rawGender.GenderName == "Female" && rawGender.CampaignId == tvGenderMap.Key.AwCampaignId).ToList();
                if (femaleStats.Count > 0)
                    budgetGroupGenderStats.Add(CompileGenderStats(tvGenderMap.Key.BudgetGroupId, tvGenderMap.Key.OrderId, femaleStats));

                var maleStats = rawGenderPerformanceList.Where(rawGender =>  rawGender.GenderName == "Male" && rawGender.CampaignId == tvGenderMap.Key.AwCampaignId).ToList();
                if (maleStats.Count > 0)
                    budgetGroupGenderStats.Add(CompileGenderStats(tvGenderMap.Key.BudgetGroupId, tvGenderMap.Key.OrderId, maleStats));
            });
            return budgetGroupGenderStats;
        }

        private BudgetGroupGenderStats CompileGenderStats(Guid budgetGroupId, Guid orderId, List<GenderPerformanceFreeData> genderStats)
        {
            return TransformRowData(budgetGroupId, orderId, genderStats);
        }

        private BudgetGroupGenderStats TransformRowData(Guid budgetGroupId, Guid orderId, List<GenderPerformanceFreeData> genderStats)
        {
            var impressions = genderStats.Sum(gender => gender.Impressions);
            var views = genderStats.Sum(gender => gender.Views);
            var clicks = genderStats.Sum(gender => gender.Clicks);
            var cost = StatsTransformerUtility.ConvertToMoney(genderStats.Sum(gender => gender.Cost));
            return new BudgetGroupGenderStats
            {
                ActualSpend = cost,
                AdwordsGenderName = genderStats.First().GenderName,
                BudgetGroupId = budgetGroupId,
                Clicks = clicks,
                ClickThroughRate = CalculateClickThroughRate(clicks, impressions),
                CompletedViews = views,
                Conversions = 0.0D, ////Conversions = ageStats.Sum(age => age.Conversions),
                EstimatedCost = 0,
                Impressions = impressions,
                OrderId = orderId,
                StatDate = genderStats.First().StatDate,
                ViewRate = StatsTransformerUtility.CalculateViewRate(views, impressions),
                ViewThroughConversions = 0L
            };
        }

        private double CalculateClickThroughRate(long clicks, long impressions)
        {
            if (impressions > 0)
                return StatsTransformerUtility.ParseDouble(clicks) / StatsTransformerUtility.ParseDouble(impressions);

            return 0;
        }

        #endregion
        
    }
}