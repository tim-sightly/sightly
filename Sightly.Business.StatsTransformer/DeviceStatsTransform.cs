using System.Collections.Generic;
using System.Linq;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer
{
    public class DeviceStatsTransform : IDeviceStatsTransform
    {
        #region Public Methods

        public List<DeviceStats> Execute(List<BudgetGroupDeviceStats> budgetGroupDeviceStats)
        {
            var deviceStats = new List<DeviceStats>();
            budgetGroupDeviceStats.GroupBy(bgd => bgd.OrderId).ToList().ForEach(order =>
            {
                deviceStats.AddRange(order.GroupBy(bgds => bgds.AdWordsDeviceTypeName)
                    .Select(TransformRowData)
                    .ToList());
            });
            return deviceStats;
        }

        #endregion


        #region Private Methods

        private static DeviceStats TransformRowData(IGrouping<string, BudgetGroupDeviceStats> budgetGroupDeviceStats)
        {
            var avgVideosPlayedTo25Percent = budgetGroupDeviceStats.Average(ds => ds.AudienceRetention25);
            var avgVideosPlayedTo50Percent = budgetGroupDeviceStats.Average(ds => ds.AudienceRetention50);
            var avgVideosPlayedTo75Percent = budgetGroupDeviceStats.Average(ds => ds.AudienceRetention75);
            var avgVideosPlayedTo100Percent = budgetGroupDeviceStats.Average(ds => ds.AudienceRetention100);
            var clicks = budgetGroupDeviceStats.Sum(ds => ds.Clicks);
            var firstItem = budgetGroupDeviceStats.FirstOrDefault();
            var impressions = budgetGroupDeviceStats.Sum(ds => ds.Impressions);
            var views = budgetGroupDeviceStats.Sum(ds => ds.CompletedViews);

            return new DeviceStats()
            {
                ActualSpend = budgetGroupDeviceStats.Sum(ds => ds.ActualSpend),
                AdWordsDeviceTypeName = budgetGroupDeviceStats.Key,
                AudienceRetention25 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo25Percent),
                AudienceRetention50 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo50Percent),
                AudienceRetention75 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo75Percent),
                AudienceRetention100 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo100Percent),
                AverageCpv = StatsTransformerUtility.ConvertToMoney(budgetGroupDeviceStats.Average(ds => ds.AverageCpv)),
                Clicks = clicks,
                ClickThroughRate = StatsTransformerUtility.CalculateClickThroughRate(clicks, impressions),
                CompletedViews = views,
                Conversions = budgetGroupDeviceStats.Sum(ds => ds.Conversions),
                DeviceTypeName = budgetGroupDeviceStats.Key,
                EstimatedCost = 0,
                Impressions = impressions,
                OrderId = firstItem.OrderId,
                PartialViews = budgetGroupDeviceStats.Sum(ds => ds.PartialViews),
                StatDate = firstItem.StatDate,
                ViewRate = StatsTransformerUtility.CalculateViewRate(views, impressions),
                ViewThroughConversions = budgetGroupDeviceStats.Sum(ds => ds.ViewThroughConversions),
                ViewTime = budgetGroupDeviceStats.Sum(ds => ds.ViewTime),
            };
        }

        #endregion

        
    }
}