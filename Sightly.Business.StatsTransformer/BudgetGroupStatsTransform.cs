﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.Business.StatsTransformer
{
    public class BudgetGroupStatsTransform : IBudgetGroupStatsTransform
    {
        private readonly IStatRepository _statRepository;

        public BudgetGroupStatsTransform(IStatRepository statRepository)
        {
            _statRepository = statRepository;
        }

        #region Public Methods

        public List<BudgetGroupStats> Execute(List<AdDevicePerformanceData> rawAdDevicePerformanceList)
        {
            if (rawAdDevicePerformanceList == null ||
                rawAdDevicePerformanceList.Count <= 0)
                return null;

            var budgetGroupStats = new List<BudgetGroupStats>();

            var distinctCampaignIds = rawAdDevicePerformanceList.GroupBy(adPerf => adPerf.CampaignId)
                .Select(cam => cam.First())
                .ToList();
            distinctCampaignIds.ForEach(campId =>
            {
                var awTvAssociatedDataList =
                    _statRepository.GetAdwordsTvAssociationData(campId.CampaignId);

                budgetGroupStats.AddRange(
                    TransformBudgetGroupStatsFromRaw(awTvAssociatedDataList, rawAdDevicePerformanceList));
            });

            return budgetGroupStats;
        }

        #endregion


        #region Private Methods

        private List<BudgetGroupStats> TransformBudgetGroupStatsFromRaw(List<AdwordsTvAssociationData> awTvAssociatedDataList, List<AdDevicePerformanceData> rawAdDevicePerformanceList)
        {
            var budgetGroupStats = new List<BudgetGroupStats>();
            var tvMappings = awTvAssociatedDataList.GroupBy(tvData => new { tvData.AwCampaignId, tvData.BudgetGroupId, tvData.OrderId }).ToList();
            
            tvMappings.ForEach(tvMap =>
            {
                var budgetGroupSpecificStats = rawAdDevicePerformanceList.Where(stat => stat.CampaignId == tvMap.Key.AwCampaignId).ToList();
                budgetGroupStats.Add(CompileBudgetGroupStats(tvMap.Key.BudgetGroupId, tvMap.Key.OrderId, budgetGroupSpecificStats));
            });

            return budgetGroupStats;
        }

        private BudgetGroupStats CompileBudgetGroupStats(Guid budgetGroupId, Guid orderId, List<AdDevicePerformanceData> budgetGroupStats)
        {
            return budgetGroupStats.Count <= 0 ? null : TransformRowData(budgetGroupId, orderId, budgetGroupStats);
        }

        private BudgetGroupStats TransformRowData(Guid budgetGroupId, Guid orderId, List<AdDevicePerformanceData> budgetGroupStats)
        {
           // var adLength = _statRepository.GetAdVideoLengthByAdId(adId) ?? 0;
            var impressions = budgetGroupStats.Sum(ad => ad.Impressions);
            var views = (budgetGroupStats.Sum(ad => ad.Views));
            var clicks = (budgetGroupStats.Sum(ad => ad.Clicks));
            var cost = StatsTransformerUtility.ConvertToMoney(budgetGroupStats.Sum(ad => ad.Cost));
            var averageCpv = views > 0? cost / views : -1.0M;
            var avgCpv = StatsTransformerUtility.ConvertToMoney(averageCpv);
            var avgVideosPlayedTo25Percent = budgetGroupStats.Average(ad => ad.VideoQuartile25Rate);
            var avgVideosPlayedTo50Percent = budgetGroupStats.Average(ad => ad.VideoQuartile50Rate);
            var avgVideosPlayedTo75Percent = budgetGroupStats.Average(ad => ad.VideoQuartile75Rate);
            var avgVideosPlayedTo100Percent = budgetGroupStats.Average(ad => ad.VideoQuartile100Rate);
            //var partialViews = new Calculators.PartialViewsCalculator().Execute(
            //    adLength, impressions, views, avgCpv,
            //    avgVideosPlayedTo25Percent, avgVideosPlayedTo50Percent,
            //    avgVideosPlayedTo75Percent, avgVideosPlayedTo100Percent
            //);
            //var viewTime = new Calculators.ViewTimeCalculator().Execute(
            //    adLength, impressions, views,
            //    avgVideosPlayedTo25Percent, avgVideosPlayedTo50Percent,
            //    avgVideosPlayedTo75Percent, avgVideosPlayedTo100Percent
            //);

            return new BudgetGroupStats
            {
                ActualSpend = cost,
                Amount = cost,
                AudienceRetention25 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo25Percent),
                AudienceRetention50 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo50Percent),
                AudienceRetention75 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo75Percent),
                AudienceRetention100 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo100Percent),
                AverageCpv = Convert.ToDouble(avgCpv),
                BudgetGroupId = budgetGroupId,
                Clicks = clicks,
                ClickThroughRate = StatsTransformerUtility.CalculateClickThroughRate(clicks, impressions),
                CompletedViews = views,
                Conversions = 0.0D, //Conversions = (adData.Sum(ad => ad.Conversions)),
                EstimatedCost = 0,
                Impressions = impressions,
                OrderId = orderId,
                //PartialViews = partialViews,
                StatDate = budgetGroupStats[0].StatDate,
                ViewRate = StatsTransformerUtility.CalculateViewRate(views, impressions),
                ViewThroughConversions = 0L//ViewThroughConversions = adData.Sum(ad => ad.ViewThroughConversions),
                //ViewTime = viewTime
            };
        }

        #endregion

    }
}