﻿using System;

namespace Sightly.Business.StatsTransformer.Calculators
{
    public class ViewTimeCalculator 
    {
        public int Execute(
            int adLengthInSeconds, long impressions, long views,
            decimal q25Percent, decimal q50Percent, decimal q75Percent, decimal q100Percent
        )
        {
            if (adLengthInSeconds == 0 || impressions == 0)
                return 0;

            var q025ImpressionsFromQuartile = impressions * (q25Percent / 100);
            var q050ImpressionsFromQuartile = impressions * (q50Percent / 100);
            var q075ImpressionsFromQuartile = impressions * (q75Percent / 100);
            var q100ImpressionsFromQuartile = impressions * (q100Percent / 100);

            var q025PartialViews = impressions - q025ImpressionsFromQuartile;
            var q050PartialViews = q025ImpressionsFromQuartile - q050ImpressionsFromQuartile;
            var q075PartialViews = q050ImpressionsFromQuartile - q075ImpressionsFromQuartile;
            var q100PartialViews = q075ImpressionsFromQuartile - q100ImpressionsFromQuartile;

            var q025Seconds = ((adLengthInSeconds * .25) / 2);
            var q050Seconds = ((adLengthInSeconds * .25) / 2) + (adLengthInSeconds * .25);
            var q075Seconds = ((adLengthInSeconds * .25) / 2) + (adLengthInSeconds * .5);
            var q100Seconds = ((adLengthInSeconds * .25) / 2) + (adLengthInSeconds * .75);

            var q025PartialViewTimeInSeconds = q025PartialViews * (decimal) q025Seconds;
            var q050PartialViewTimeInSeconds = q050PartialViews * (decimal) q050Seconds;
            var q075PartialViewTimeInSeconds = q075PartialViews * (decimal) q075Seconds;
            var q100PartialViewTimeInSeconds = q100PartialViews * (decimal) q100Seconds;

            var fullViewTimeInSeconds = (adLengthInSeconds * views);

            var totalViewTimeInSeconds = q025PartialViewTimeInSeconds
                                         + q050PartialViewTimeInSeconds
                                         + q075PartialViewTimeInSeconds
                                         + q100PartialViewTimeInSeconds
                                         + fullViewTimeInSeconds;

            return (int)Math.Ceiling(totalViewTimeInSeconds);
        }
    }
}
