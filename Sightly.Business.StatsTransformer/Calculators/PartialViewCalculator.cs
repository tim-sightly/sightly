﻿using System;
using System.Globalization;

namespace Sightly.Business.StatsTransformer.Calculators
{
    public class PartialViewsCalculator 
    {
        #region Private Methods
        private static decimal Calculate20SecondAdPartialViews(
            int adLength, long impressions, decimal avgCpv,
            decimal q25Percent, decimal q50Percent,
            decimal q75Percent, decimal q100Percent
        )
        {
            var q025ImpressionsFromQuartile = impressions * (q25Percent / 100);
            var q050ImpressionsFromQuartile = impressions * (q50Percent / 100);
            var q075ImpressionsFromQuartile = impressions * (q75Percent / 100);
            var q100ImpressionsFromQuartile = impressions * (q100Percent / 100);

            var q025PartialViews = impressions - q025ImpressionsFromQuartile;
            var q050PartialViews = q025ImpressionsFromQuartile - q050ImpressionsFromQuartile;
            var q075PartialViews = q050ImpressionsFromQuartile - q075ImpressionsFromQuartile;
            var q100PartialViews = q075ImpressionsFromQuartile - q100ImpressionsFromQuartile;

            var split5SPercent = 5D / adLength;

            var q025Value = avgCpv * (decimal) 0.25 * q025PartialViews;
            var q050Value = ((avgCpv * (decimal) split5SPercent) * (q050PartialViews * (decimal) (split5SPercent / 25))) +
                            (((avgCpv * (decimal) (50 - split5SPercent)) * (q050PartialViews * (decimal) (1 - (split5SPercent / 25)))));
            var q075Value = avgCpv * (decimal) 0.625 * q075PartialViews;
            var q100Value = avgCpv * (decimal) 0.875 * q100PartialViews;

            return q025Value + q050Value + q075Value + q100Value;
        }

        private static decimal Calculate20To30SecondAdPartialViews(
            int adLength, long impressions, decimal avgCpv,
            decimal q25Percent, decimal q50Percent,
            decimal q75Percent, decimal q100Percent
        )
        {
            var q025ImpressionsFromQuartile = impressions * (q25Percent / 100);
            var q050ImpressionsFromQuartile = impressions * (q50Percent / 100);
            var q075ImpressionsFromQuartile = impressions * (q75Percent / 100);
            var q100ImpressionsFromQuartile = impressions * (q100Percent / 100);

            var q025PartialViews = impressions - q025ImpressionsFromQuartile;
            var q050PartialViews = q025ImpressionsFromQuartile - q050ImpressionsFromQuartile;
            var q075PartialViews = q050ImpressionsFromQuartile - q075ImpressionsFromQuartile;
            var q100PartialViews = q075ImpressionsFromQuartile - q100ImpressionsFromQuartile;

            var split5SPercent = 5D / adLength;

            var q025Value = ((avgCpv * (decimal) split5SPercent) * (q025PartialViews * (decimal) (split5SPercent / 25))) +
                            (((avgCpv * (decimal) (25 - split5SPercent)) * (q025PartialViews * (decimal) (1 - (split5SPercent / 25)))));
            var q050Value = avgCpv * (decimal) 0.375 * q050PartialViews;
            var q075Value = avgCpv * (decimal) 0.625 * q075PartialViews;
            var q100Value = avgCpv * (decimal) 0.875 * q100PartialViews;

            return q025Value + q050Value + q075Value + q100Value;
        }

        private static decimal Calculate30To40SecondAdPartialViews(
            int adLength, long impressions, long views, decimal avgCpv,
            decimal q25Percent, decimal q50Percent,
            decimal q75Percent, decimal q100Percent
        )
        {
            var q025ImpressionsFromQuartile = impressions * (q25Percent / 100);
            var q050ImpressionsFromQuartile = impressions * (q50Percent / 100);
            var q075ImpressionsFromQuartile = impressions * (q75Percent / 100);
            var q100ImpressionsFromQuartile = impressions * (q100Percent / 100);

            var q025PartialViews = impressions - q025ImpressionsFromQuartile;
            var q050PartialViews = q025ImpressionsFromQuartile - q050ImpressionsFromQuartile;
            var q075PartialViews = q050ImpressionsFromQuartile - q075ImpressionsFromQuartile;
            var q100PartialViews = q075ImpressionsFromQuartile - q100ImpressionsFromQuartile;

            var split5SPercent = 5 / adLength;
            var split30SPercent = 30 / adLength;

            var q025Value = ((avgCpv * split5SPercent) * (q025PartialViews * (split5SPercent / 25))) +
                            (((avgCpv * (25 - split5SPercent)) * (q025PartialViews * (1 - (split5SPercent / 25)))));
            var q050Value = avgCpv * (decimal) 0.375 * q050PartialViews;
            var q075Value = avgCpv * (decimal) 0.625 * q075PartialViews;
            var q100Value = avgCpv * ((split30SPercent - 75) / 2) * (q100PartialViews - views);

            return q025Value + q050Value + q075Value + q100Value;
        }

        private static decimal Calculate40To60SecondAdPartialViews(
            int adLength, long impressions, long views, decimal avgCpv,
            decimal q25Percent, decimal q50Percent,
            decimal q75Percent, decimal q100Percent
        )
        {
            var q025ImpressionsFromQuartile = impressions * (q25Percent / 100);
            var q050ImpressionsFromQuartile = impressions * (q50Percent / 100);
            var q075ImpressionsFromQuartile = impressions * (q75Percent / 100);
            var q100ImpressionsFromQuartile = impressions * (q100Percent / 100);

            var q025PartialViews = impressions - q025ImpressionsFromQuartile;
            var q050PartialViews = q025ImpressionsFromQuartile - q050ImpressionsFromQuartile;
            var q075PartialViews = q050ImpressionsFromQuartile - q075ImpressionsFromQuartile;
            var q100PartialViews = q075ImpressionsFromQuartile - q100ImpressionsFromQuartile;

            var split5SPercent = 5 / adLength;
            var split30SPercent = 30 / adLength;

            var q025Value = ((avgCpv * split5SPercent) * (q025PartialViews * (split5SPercent / 25))) +
                            (((avgCpv *  (25 - split5SPercent)) * (q025PartialViews *  (1 - (split5SPercent / 25)))));
            var q050Value = avgCpv * (decimal) 0.375 * q050PartialViews;
            var q075Value = avgCpv *  ((split30SPercent - 50) / 2) * (q075PartialViews - q100PartialViews - views);
            var q100Value = 0;

            return q025Value + q050Value + q075Value + q100Value;
        }

        private static decimal Calculate60To120SecondAdPartialViews(
            int adLength, long impressions, long views,  decimal avgCpv,
            decimal q25Percent, decimal q50Percent,
            decimal q75Percent, decimal q100Percent
        )
        {
            var q025ImpressionsFromQuartile = impressions * (q25Percent / 100);
            var q050ImpressionsFromQuartile = impressions * (q50Percent / 100);
            var q075ImpressionsFromQuartile = impressions * (q75Percent / 100);
            var q100ImpressionsFromQuartile = impressions * (q100Percent / 100);

            var q025PartialViews = impressions - q025ImpressionsFromQuartile;
            var q050PartialViews = q025ImpressionsFromQuartile - q050ImpressionsFromQuartile;
            var q075PartialViews = q050ImpressionsFromQuartile - q075ImpressionsFromQuartile;
            var q100PartialViews = q075ImpressionsFromQuartile - q100ImpressionsFromQuartile;

            var split5SPercent = 5 / adLength;
            var split30SPercent = 30 / adLength;

            var q025Value = ((avgCpv * split5SPercent) * (q025PartialViews * (split5SPercent / 25))) +
                            (((avgCpv * (25 - split5SPercent)) * (q025PartialViews * (1 - (split5SPercent / 25)))));
            var q050Value = avgCpv * ((split30SPercent - 25) / 2) * (q050PartialViews - q075PartialViews - q100PartialViews - views);
            const int q075Value = 0;
            const int q100Value = 0;

            return q025Value + q050Value + q075Value + q100Value;
        }

        private static decimal CalculateAbove120SecondAdPartialViews(
            int adLength, long impressions, long views, decimal avgCpv,
            decimal q25Percent, decimal q50Percent,
            decimal q75Percent, decimal q100Percent
        )
        {
            var q025ImpressionsFromQuartile = impressions * (q25Percent / 100);
            var q050ImpressionsFromQuartile = impressions * (q50Percent / 100);
            var q075ImpressionsFromQuartile = impressions * (q75Percent / 100);
            var q100ImpressionsFromQuartile = impressions * (q100Percent / 100);

            var q025PartialViews = impressions - q025ImpressionsFromQuartile;
            var q050PartialViews = q025ImpressionsFromQuartile - q050ImpressionsFromQuartile;
            var q075PartialViews = q050ImpressionsFromQuartile - q075ImpressionsFromQuartile;
            var q100PartialViews = q075ImpressionsFromQuartile - q100ImpressionsFromQuartile;

            var split5SPercent = 5D / adLength;
            var split30SPercent = 30D / adLength;

            var q025Value = ((avgCpv * (decimal) split5SPercent) * (q025PartialViews * (decimal) (split5SPercent / split30SPercent))) +
                            (((avgCpv * (decimal) (split30SPercent - split5SPercent) / 2) * (q025PartialViews * (decimal) (1 - (split5SPercent / split30SPercent)))));
            var q050Value = avgCpv * (decimal) ((split30SPercent - 25) / 2) * (q050PartialViews - q075PartialViews - q100PartialViews - views);
            const int q075Value = 0;
            const int q100Value = 0;

            return q025Value + q050Value + q075Value + q100Value;
        }
        #endregion

        #region Public Methods
       
        public long Execute(
            int adLength, long impressions, long views, decimal avgCpv,
            decimal q25Percent, decimal q50Percent, decimal q75Percent, decimal q100Percent
        )
        {
            decimal output;

            if (adLength == 0)
                return 0L;

            if (adLength <= 20)
            {
                output = Calculate20SecondAdPartialViews(
                    adLength, impressions, avgCpv,
                    q25Percent, q50Percent, q75Percent, q100Percent
                );
            }
            else if (adLength > 20 && adLength <= 30)
            {
                output = Calculate20To30SecondAdPartialViews(
                    adLength, impressions, avgCpv,
                    q25Percent, q50Percent, q75Percent, q100Percent
                );
            }
            else if (adLength > 30 && adLength <= 40)
            {
                output = Calculate30To40SecondAdPartialViews(
                    adLength, impressions, views, avgCpv,
                    q25Percent, q50Percent, q75Percent, q100Percent
                );
            }
            else if (adLength > 40 && adLength <= 60)
            {
                output = Calculate40To60SecondAdPartialViews(
                    adLength, impressions, views, avgCpv,
                    q25Percent, q50Percent, q75Percent, q100Percent
                );
            }
            else if (adLength > 60 && adLength <= 120)
            {
                output = Calculate60To120SecondAdPartialViews(
                    adLength, impressions, views, avgCpv,
                    q25Percent, q50Percent, q75Percent, q100Percent
                );
            }
            else
            {
                output = CalculateAbove120SecondAdPartialViews(
                    adLength, impressions, views, avgCpv,
                    q25Percent, q50Percent, q75Percent, q100Percent
                );
            }

            return long.Parse(Math.Round(output).ToString(CultureInfo.InvariantCulture));
        }
        #endregion
    }
}
