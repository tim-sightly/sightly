using System.Collections.Generic;
using System.Linq;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer
{
    public class GenderStatsTransform : IGenderStatsTransform
    {

        public List<GenderStats> Execute(List<BudgetGroupGenderStats> budgetGroupGenderStats)
        {
            var genderStats = new List<GenderStats>();
            budgetGroupGenderStats.GroupBy(bgg => bgg.OrderId).ToList().ForEach(order =>
            {
                genderStats.AddRange(order.GroupBy(bggs => bggs.AdwordsGenderName)
                .Select(TransformRowData)
                .ToList());
            });
            return genderStats;
        }

        private static GenderStats TransformRowData(IGrouping<string, BudgetGroupGenderStats> budgetGroupGenderStats)
        {
            var avgVideosPlayedTo25Percent = budgetGroupGenderStats.Average(gs => gs.AudienceRetention25);
            var avgVideosPlayedTo50Percent = budgetGroupGenderStats.Average(gs => gs.AudienceRetention50);
            var avgVideosPlayedTo75Percent = budgetGroupGenderStats.Average(gs => gs.AudienceRetention75);
            var avgVideosPlayedTo100Percent = budgetGroupGenderStats.Average(gs => gs.AudienceRetention100);
            var clicks = budgetGroupGenderStats.Sum(gs => gs.Clicks);
            var firstItem = budgetGroupGenderStats.FirstOrDefault();
            var impressions = budgetGroupGenderStats.Sum(gs => gs.Impressions);
            var views = budgetGroupGenderStats.Sum(gs => gs.CompletedViews);

            return new GenderStats()
            {
                ActualSpend = budgetGroupGenderStats.Sum(gs => gs.ActualSpend),
                AudienceRetention25 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo25Percent),
                AudienceRetention50 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo50Percent),
                AudienceRetention75 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo75Percent),
                AudienceRetention100 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo100Percent),
                Clicks = clicks,
                ClickThroughRate = StatsTransformerUtility.CalculateClickThroughRate(clicks, impressions),
                CompletedViews = views,
                Conversions = budgetGroupGenderStats.Sum(gs => gs.Conversions),
                EstimatedCost = 0,
                GenderName = budgetGroupGenderStats.Key,
                Impressions = impressions,
                OrderId = firstItem.OrderId,
                StatDate = firstItem.StatDate,
                ViewRate = StatsTransformerUtility.CalculateViewRate(views, impressions),
                ViewThroughConversions = budgetGroupGenderStats.Sum(gs => gs.ViewThroughConversions),
            };
        }
    }
}