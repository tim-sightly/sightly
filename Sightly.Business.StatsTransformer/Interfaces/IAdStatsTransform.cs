using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer.Interfaces
{
    public interface IAdStatsTransform
    {
        List<AdStats> Execute(List<BudgetGroupAdStats> budgetGroupAdStats);
    }
}