﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer.Interfaces
{
    public interface IBudgetGroupDeviceStatsTransform
    {
        List<BudgetGroupDeviceStats> Execute(List<AdDevicePerformanceData> rawAdDevicePerformanceList);
    }
}