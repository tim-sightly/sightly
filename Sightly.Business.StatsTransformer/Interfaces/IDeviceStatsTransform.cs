using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer.Interfaces
{
    public interface IDeviceStatsTransform
    {
        List<DeviceStats> Execute(List<BudgetGroupDeviceStats> budgetGroupDeviceStats);
    }
}