﻿using System;
using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer.Interfaces
{
    public interface IBudgetGroupAdStatsTransform
    {
        List<BudgetGroupAdStats> Execute(List<AdDevicePerformanceData> rawAdDevicePerformanceList);
    }
}
