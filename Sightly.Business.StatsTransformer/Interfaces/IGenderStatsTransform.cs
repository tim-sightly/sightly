using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer.Interfaces
{
    public interface IGenderStatsTransform
    {
        List<GenderStats> Execute(List<BudgetGroupGenderStats> budgetGroupGenderStats);
    }
}