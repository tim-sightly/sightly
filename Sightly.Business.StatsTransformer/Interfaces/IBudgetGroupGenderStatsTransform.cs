﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer.Interfaces
{
    public interface IBudgetGroupGenderStatsTransform
    {
        List<BudgetGroupGenderStats> Execute(List<GenderPerformanceFreeData> rawGenderPerformanceList);
    }
}