using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer.Interfaces
{
    public interface IAgeGroupStatsTransform
    {
        List<AgeGroupStats> Execute(List<BudgetGroupAgeStats> budgetGroupAgeStats);
    }
}