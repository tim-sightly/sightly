using System.Collections.Generic;
using System.Net;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer.Interfaces
{
    public interface IBudgetGroupAgeStatsTransform
    {
        List<BudgetGroupAgeStats> Execute(List<AgeRangePerformanceFreeData> rawAgePerformanceList);
    }
}