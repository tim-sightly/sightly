﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer.Interfaces
{
    public interface IOrderStatsTransform
    {
        List<OrderStats> Execute(List<BudgetGroupStats> budgetGroupStats);
    }
}