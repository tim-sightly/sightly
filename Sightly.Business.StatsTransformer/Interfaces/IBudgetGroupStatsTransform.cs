﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer.Interfaces
{
    public interface IBudgetGroupStatsTransform
    {
        List<BudgetGroupStats> Execute(List<AdDevicePerformanceData> rawAdDevicePerformanceList);
    }
}