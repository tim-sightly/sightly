using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer
{
    public class AdStatsTransform : IAdStatsTransform
    {
        #region Public Methods

        public List<AdStats> Execute(List<BudgetGroupAdStats> budgetGroupAdStats)
        {
            var adStats = new List<AdStats>();
            budgetGroupAdStats.GroupBy(bg => bg.OrderId).ToList().ForEach(order =>
            {
                adStats.AddRange(order.GroupBy(rs => rs.AdId)
                    .Select(TransformRowData)
                    .ToList()
                );
            });
            return adStats;
        }

        #endregion





        private static AdStats TransformRowData(IGrouping<Guid, BudgetGroupAdStats> adStats)
        {
            var avgVideosPlayedTo25Percent = adStats.Average(ads => ads.AudienceRetention25);
            var avgVideosPlayedTo50Percent = adStats.Average(ads => ads.AudienceRetention50);
            var avgVideosPlayedTo75Percent = adStats.Average(ads => ads.AudienceRetention75);
            var avgVideosPlayedTo100Percent = adStats.Average(ads => ads.AudienceRetention100);
            var clicks = adStats.Sum(ads => ads.Clicks);
            var firstItem = adStats.FirstOrDefault();
            var impressions = adStats.Sum(ads => ads.Impressions);
            var views = adStats.Sum(ads => ads.CompletedViews);

            return new AdStats()
            {
                AdId = adStats.Key,
                ActualSpend = adStats.Sum(ads => ads.ActualSpend),
                AudienceRetention25 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo25Percent),
                AudienceRetention50 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo50Percent),
                AudienceRetention75 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo75Percent),
                AudienceRetention100 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo100Percent),
                AverageCpv = StatsTransformerUtility.ConvertToMoney(adStats.Average(rs => rs.AverageCpv)),
                Clicks = clicks,
                ClickThroughRate = StatsTransformerUtility.CalculateClickThroughRate(clicks, impressions),
                CompletedViews = views,
                Conversions = adStats.Sum(rs => rs.Conversions),
                EstimatedCost = 0,
                Impressions = impressions,
                OrderId = firstItem.OrderId,
                PartialViews = adStats.Sum(ads => ads.PartialViews),
                StatDate = firstItem.StatDate,
                ViewRate = StatsTransformerUtility.CalculateViewRate(views, impressions),
                ViewThroughConversions = adStats.Sum(rs => rs.ViewThroughConversions),
                ViewTime = adStats.Sum(ad => ad.ViewTime),
            };
        }
    }
}