using System.Collections.Generic;
using System.Linq;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer
{
    public class AgeGroupStatsTransform : IAgeGroupStatsTransform
    {
        #region Public Methods
        public List<AgeGroupStats> Execute(List<BudgetGroupAgeStats> budgetGroupAgeStats)
        {
            var ageGroupStats = new List<AgeGroupStats>();
            budgetGroupAgeStats.GroupBy(bga => bga.OrderId).ToList().ForEach(order =>
            {
                ageGroupStats.AddRange(order.GroupBy(bgags => bgags.AdWordsAgeGroupName)
                    .Select(TransformRowData)
                    .ToList()
                );
            });
            return ageGroupStats;
        }
        
        #endregion

        #region Private Methods
        
        private static AgeGroupStats TransformRowData(IGrouping<string, BudgetGroupAgeStats> budgetGroupAgeStats)
        {
            var avgVideosPlayedTo25Percent = budgetGroupAgeStats.Average(ads => ads.AudienceRetention25);
            var avgVideosPlayedTo50Percent = budgetGroupAgeStats.Average(ads => ads.AudienceRetention50);
            var avgVideosPlayedTo75Percent = budgetGroupAgeStats.Average(ads => ads.AudienceRetention75);
            var avgVideosPlayedTo100Percent = budgetGroupAgeStats.Average(ads => ads.AudienceRetention100);
            var clicks = budgetGroupAgeStats.Sum(ad => ad.Clicks);
            var firstItem = budgetGroupAgeStats.FirstOrDefault();
            var impressions = budgetGroupAgeStats.Sum(ad => ad.Impressions);
            var views = budgetGroupAgeStats.Sum(ad => ad.CompletedViews);

            return new AgeGroupStats()
            {
                AgeGroupName = budgetGroupAgeStats.Key,
                AudienceRetention25 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo25Percent),
                AudienceRetention50 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo50Percent),
                AudienceRetention75 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo75Percent),
                AudienceRetention100 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo100Percent),
                Clicks = clicks,
                ClickThroughRate = StatsTransformerUtility.CalculateClickThroughRate(clicks, impressions),
                CompletedViews = views,
                Conversions = budgetGroupAgeStats.Sum(ad => ad.Conversions),
                EstimatedCost = 0,
                Impressions = impressions,
                ActualSpend = budgetGroupAgeStats.Sum(ad => ad.ActualSpend),
                OrderId = firstItem.OrderId,
                StatDate = firstItem.StatDate,
                ViewRate = StatsTransformerUtility.CalculateViewRate(views, impressions),
                ViewThroughConversions = budgetGroupAgeStats.Sum(ad => ad.ViewThroughConversions)
            };
        }

        #endregion
    }
}