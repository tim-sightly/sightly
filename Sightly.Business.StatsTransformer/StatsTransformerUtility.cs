﻿using System;
using System.Globalization;

namespace Sightly.Business.StatsTransformer
{
    public class StatsTransformerUtility
    {
        #region Private Methods

        internal static double ParseDouble(long value)
        {
            return double.Parse(value.ToString());
        }
        #endregion

        #region Protected Methods

        internal static int CalculateAudienceRetention(decimal value)
        {
            return value > 0 ? int.Parse(Math.Round((value)).ToString(CultureInfo.InvariantCulture)) : 0;
        }

        public static int CalculateAudienceRetention(double value)
        {
            return value > 0 ? int.Parse(Math.Round((value)).ToString(CultureInfo.InvariantCulture)) : 0;
        }
        internal static double CalculateClickThroughRate(long clicks, long impressions)
        {
            return impressions > 0 ? ParseDouble(clicks) / ParseDouble(impressions) : 0;
        }

        internal static double CalculateViewRate(long views, long impressions)
        {
            return impressions > 0 ? ParseDouble(views) / ParseDouble(impressions) : 0;
        }

        internal static decimal ConvertToMoney(decimal value)
        {
            return value > 0 ? value / 1000000M : 0;
        }

        internal static double ConvertToMoney(double value)
        {
            return value > 0 ? value / 1000000 : 0;
        }
        #endregion

    }
}
