using System;
using System.Collections.Generic;
using System.Linq;
using Sightly.Business.StatsTransformer.Interfaces;
using Sightly.Models;

namespace Sightly.Business.StatsTransformer
{
    public class OrderStatsTransform : IOrderStatsTransform
    {
        public List<OrderStats> Execute(List<BudgetGroupStats> budgetGroupStats)
        {
            var orderStats = new List<OrderStats>();

            budgetGroupStats.GroupBy(bg => bg.OrderId).ToList().ForEach(order =>
            {
                orderStats.AddRange(order.GroupBy(bgs => bgs.BudgetGroupId)
                .Select(TransformRowData)
                .ToList());
            });
            return orderStats;
        }

        private static OrderStats TransformRowData(IGrouping<Guid, BudgetGroupStats> budgetGroupStats)
        {
            var avgVideosPlayedTo25Percent = budgetGroupStats.Average(bgs => bgs.AudienceRetention25);
            var avgVideosPlayedTo50Percent = budgetGroupStats.Average(bgs => bgs.AudienceRetention50);
            var avgVideosPlayedTo75Percent = budgetGroupStats.Average(bgs => bgs.AudienceRetention75);
            var avgVideosPlayedTo100Percent = budgetGroupStats.Average(bgs => bgs.AudienceRetention100);
            var clicks = budgetGroupStats.Sum(bgs => bgs.Clicks);
            var impressions = budgetGroupStats.Sum(bgs => bgs.Impressions);
            var views = budgetGroupStats.Sum(bgs => bgs.CompletedViews);

            return new OrderStats()
            {
                ActualSpend = budgetGroupStats.Sum(bgs => bgs.ActualSpend),
                Amount = budgetGroupStats.Sum(bgs => bgs.ActualSpend),
                AudienceRetention25 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo25Percent),
                AudienceRetention50 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo50Percent),
                AudienceRetention75 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo75Percent),
                AudienceRetention100 = StatsTransformerUtility.CalculateAudienceRetention(avgVideosPlayedTo100Percent),
                AverageCpc = StatsTransformerUtility.ConvertToMoney(budgetGroupStats.Average(bgs => bgs.AverageCpc)),
                AverageCpe = StatsTransformerUtility.ConvertToMoney(budgetGroupStats.Average(bgs => bgs.AverageCpe)),
                AverageCpm = StatsTransformerUtility.ConvertToMoney(budgetGroupStats.Average(bgs => bgs.AverageCpm)),
                AverageCpv = StatsTransformerUtility.ConvertToMoney(budgetGroupStats.Average(bgs => bgs.AverageCpv)),
                AveragePosition = budgetGroupStats.Average(bgs => bgs.AveragePosition),
                Clicks = clicks,
                ClickThroughRate = StatsTransformerUtility.CalculateClickThroughRate(clicks, impressions),
                CompletedViews = views,
                Conversions = budgetGroupStats.Sum(bgs => bgs.Conversions),
                Engagements = budgetGroupStats.Sum(bgs => bgs.Engagements),
                Impressions = impressions,
                Interactions = budgetGroupStats.Sum(bgs => bgs.Interactions),
                OrderId = budgetGroupStats.First().OrderId,
                StatDate = budgetGroupStats.First().StatDate,
                ViewRate = StatsTransformerUtility.CalculateViewRate(views, impressions),
                ViewThroughConversions = budgetGroupStats.Sum(bgs => bgs.ViewThroughConversions),
            };
        }
    }
}