﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetDevicePerformanceTest
    {
        private const string Customer = "8978000030";
        private readonly DateTime _statDate = new DateTime(2016, 6, 6);

        [TestMethod]
        public void TestDevicePerformance()
        {
            var deviceReport = new DevicePerformance();
            List<DevicePerformanceData> returnedValues = deviceReport.GetDevicePerformanceData(Customer, _statDate);

            Assert.IsNotNull(returnedValues);
        }
    }
}
