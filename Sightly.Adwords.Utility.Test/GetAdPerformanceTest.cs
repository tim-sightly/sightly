﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetAdPerformanceTest
    {
        private const string customer = "4085269081";
        private DateTime statDate = new DateTime(2016, 8, 1);

        [TestMethod]
        public void TestAdPerformance()
        {
            var adReport = new AdPerformance();
            List<AdPerformanceData> returnedValues = adReport.GetAdPerformanceData(customer, statDate);

            Assert.IsNotNull(returnedValues);
        }
    }
}
