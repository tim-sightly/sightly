﻿using System;
using System.Collections.Generic;
using System.Threading;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class BudgetOrderTest
    {
        [TestMethod]
        public void TestBudgetOrder()
        {
            var budgetOrderService = new BudgetOrder();
            List<AdwordsBudgetOrderData> returnedBudgetOrderData = budgetOrderService.GetBudgetOrders();

            Assert.IsNotNull(returnedBudgetOrderData);
        }

        [TestMethod]
        public void TestBillingAccount()
        {
            var budgetOrderService = new BudgetOrder();
            var returnedVal = budgetOrderService.GetBillingAccounts();

            Assert.IsNotNull(returnedVal);
        }
    }
}
