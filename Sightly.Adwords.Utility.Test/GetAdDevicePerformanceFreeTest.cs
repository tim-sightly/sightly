﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    /// <summary>
    /// Summary description for GetAdDevicePerformanceFreeTest
    /// </summary>
    [TestClass]
    public class GetAdDevicePerformanceFreeTest
    {
        private const string customer = "8942764941";
        private DateTime statDate = new DateTime(2017, 6, 10);


        [TestMethod]
        public void TestGetAdDevicePerformanceFree()
        {
            var adDeviceReport = new AdDevicePerformanceFree();
            List<AdDevicePerformanceFreeData> returnedValues = adDeviceReport.GetAdDevicePerformanceFreeData(customer, statDate);

            Assert.IsNotNull(returnedValues);
        }
    }
}
