﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetAccountBudgetTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            var accountBudget = new AccountBudget();

            List<AccountBudgetData> returedValues = accountBudget.GetCustomerAccountBudgets();

            Assert.IsNotNull(returedValues);
        }
    }
}
