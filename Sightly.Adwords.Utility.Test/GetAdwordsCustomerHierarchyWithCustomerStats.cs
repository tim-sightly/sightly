﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Autofac;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;
using Sightly.TargetView.AdwordsValidation;
using Sightly.TargetView.AdwordsValidation.Interfaces;
using Sightly.Test.Common;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetAdwordsCustomerHierarchyWithCustomerStats
    {
        private static IContainer Container { get; set; }
        private IAccountHierarchy _accountHierarchy;
        private ITargetViewAdwordsValidation _targetViewAdwordsValidation;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _accountHierarchy = Container.Resolve<IAccountHierarchy>();
            _targetViewAdwordsValidation = Container.Resolve<ITargetViewAdwordsValidation>();

        }

        [TestMethod]
        public void GetCustHierarchyWithChildrenAndStats()
        {
            //--9217729477, Moore and Scarry MCC
            //const long mmcCustomerId = 9217729477;
            //--3554989310, Advance Digital MCC
            const long mmcCustomerId = 3554989310;


            var statDate = DateTime.Parse("2016-09-27");

            //var accountHierarchy = new AccountHierarchy();
            var adiCustomer = _accountHierarchy.GetAdwordsCustomerByMcc(mmcCustomerId);


            var campStatsList = _targetViewAdwordsValidation.GetCampaignStatisticsForMccCustomer(statDate, adiCustomer.First().ChildCustomers);


            Assert.IsNotNull(campStatsList.ToList());

        }


    }
}
