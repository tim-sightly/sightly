﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;


namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetAgeRangePerformanceFree
    {
        private const string customer = "8942764941";
        private DateTime statDate = new DateTime(2017, 6, 10);

        [TestMethod]
        public void TestGetAgeRangePerformanceFree()
        {
            var ageRangeReport = new AgeRangePerformanceFree();
            List<AgeRangePerformanceFreeData> returnedValues = ageRangeReport.GetAgeRangePerformanceFreeData(customer, statDate);

            Assert.IsNotNull(returnedValues);
        }
    }
}
