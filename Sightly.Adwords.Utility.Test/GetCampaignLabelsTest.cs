﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetCampaignLabelsTest
    {
        private const string customer = "5426470786"; // "7104344915";
        private DateTime statDate = DateTime.Today;
     
        [TestMethod]
        public void TestCampaignLabels()
        {
            var campaignLabel = new CampaignLabel();
            var campLables = campaignLabel.GetCampaignLabelData(customer, statDate);

            Assert.IsNotNull(campLables);
        }
    }
}
