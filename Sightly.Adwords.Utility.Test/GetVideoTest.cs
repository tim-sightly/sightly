﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetVideoTest
    {
        private const string customer = "4085269081";

        [TestMethod]
        public void TestGetVideos()
        {
            var videoService = new AdwordVideo();
            List<AdwordVideoData> returnedValues = videoService.GetVideoData(customer);

            Assert.IsNotNull(returnedValues);

            Assert.AreNotEqual(0, returnedValues);
        }
    }
}
