﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetAdGroupTest
    {
        private const string customerId = "8978000030";
        [TestMethod]
        public void TestAdGroups()
        {
            var adGroupService = new AdwordAdGroup();
            List<AdwordAdGroupData> returnedValues = adGroupService.GetAdGroupData(customerId);

            Assert.IsNotNull(returnedValues);
            Assert.AreNotEqual(0, returnedValues.Count);

        }
    }
}
