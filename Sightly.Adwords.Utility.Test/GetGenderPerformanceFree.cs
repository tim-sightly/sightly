﻿using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetGenderPerformanceFree
    {
        private const string customer = "8942764941";
        private DateTime statDate = new DateTime(2017, 6, 10);

        [TestMethod]
        public void TestGetGenderPerformanceFree()
        {
            var genderReport = new GenderPerformanceFree();
            List<GenderPerformanceFreeData> returnedValues = genderReport.GetGenderPerformanceFreeData(customer, statDate);

            Assert.IsNotNull(returnedValues);
        }
    }
}
