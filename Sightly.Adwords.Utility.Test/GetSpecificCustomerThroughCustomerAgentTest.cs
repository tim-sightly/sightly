﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetSpecificCustomerThroughCustomerAgentTest
    {

        [TestMethod]
        public void TestCustomerAgent()
        {
            var campAdData = new List<TvCampaignAdWordsInfo>
            {
                new TvCampaignAdWordsInfo
                {
                    AdvertiserId = new Guid("D12C7411-BBBD-40D5-B862-96ADFD0DF678"),
                    AdvertiserName = "Berk Trade & Business School",
                    CampaignAdWordsInfoId = new Guid("CE156628-38E1-4248-BF48-29B2B46458F9"),
                    CampaignId = new Guid("53E2AC65-1D65-45F1-A95F-CFA28E97A2CA")
                }
            };
            var customerService = new CustomerAgent();
            List<TvCampaignAdWordsInfo> returnedValues = customerService.GetAdWordsCustomers(campAdData);

            Assert.IsNotNull(returnedValues);
            Assert.AreEqual(3805379346, returnedValues.First().AdWordsCustomerId);

        }
    }
}
