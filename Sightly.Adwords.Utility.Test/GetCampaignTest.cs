﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetCampaignTest
    {
        private const string customer = "8978000030";
        private const string goodName = "Triple T Heating & Cooling";
        private const string badName = "Test Auto Campaign";
        
        [TestMethod]
        public void TestGetCampaigns()
        {
            var campaign = new AdwordCampaign();
            List<AdwordsCampaignData> returnedValues = campaign.GetCampaignData(customer);

            Assert.IsNotNull(returnedValues);
            Assert.AreNotEqual(0, returnedValues.Count);
        }

        [TestMethod]
        public void GetCampaignHistoryData()
        {
            var oldCampId = "1285664376";
            var campaign = new AdwordCampaign();
            var returnedValues = campaign.GetCampaignDataForDate(oldCampId, DateTime.Parse("2017-05-09"));

            Assert.IsNotNull(returnedValues);
            Assert.AreNotEqual(0, returnedValues.Count);
        }

        [TestMethod]
        public void TestGetCampaignByGoodName()
        {
            var campaign = new AdwordCampaign();
            long returnedValues = campaign.GetCampaignIdFromCampaignName(goodName);
            Assert.AreEqual(433105706, returnedValues);
        }

        [TestMethod]
        public void TestGetCampaignByBadName()
        {
            var campaign = new AdwordCampaign();
            long returnedValues = campaign.GetCampaignIdFromCampaignName(badName);
            Assert.AreEqual(0, returnedValues);
        }
    }
}
