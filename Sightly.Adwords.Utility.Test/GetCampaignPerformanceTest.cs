﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetCampaignPerformanceTest
    {
        private const string customer = "9273847302";
        private DateTime statDate = new DateTime(2016, 8,4);

        [TestMethod]
        public void TestCampaignPerformance()
        {
            var campaignReport = new CampaignPerformance();
            List<CampaignPerformanceData> returnedValues = campaignReport.GetCampaignPerformanceData(customer, statDate);

            Assert.IsNotNull(returnedValues);
        }
    }
}
