﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetAgeRangePerformanceTest
    {
        private const string customer = "8978000030";
        private DateTime statDate = new DateTime(2016, 6, 6);

        [TestMethod]
        public void TestCampaignPerformance()
        {
            var AgeReport = new AgeRangePerformance();
            List<AgeRangePerformanceData> returnedValues = AgeReport.GetAgeRangePerformanceData(customer, statDate);

            Assert.IsNotNull(returnedValues);
        }
    }
}
