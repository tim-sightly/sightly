﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetBudgetTest
    {
        private const string customer = "8978000030";

        [TestMethod]
        public void TestGetBudget()
        {
            var budgetService = new AdwordBudget();
            List<AdwordBudgetData> returnedValues = budgetService.GetCampaignBudgetData(customer);

            Assert.IsNotNull(returnedValues);
            Assert.AreNotEqual(0, returnedValues.Count);
        }
    }
}
