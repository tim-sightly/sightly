﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetAdTest
    {
        private const string customer = "4085269081";

        [TestMethod]
        public void TestGetAds()
        {
            var adService = new AdwordAd();

            List<AdwordAdData> returnedValues = adService.GetAdData(customer);

            Assert.IsNotNull(returnedValues);
            Assert.AreNotEqual(0, returnedValues.Count);
        }
    }
}
