﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Models;

namespace Sightly.Adwords.Utility.Test
{
    [TestClass]
    public class GetAccountHierarchyTest
    {
        [TestMethod]
        public void TestGetAccountHierarchy()
        {
            var accountHierarchy = new AccountHierarchy();

            Dictionary<long, AdwordsManagedCustomerTreeNode> returnedValues = accountHierarchy.GetAccountHierarchy();

            Assert.IsNotNull(returnedValues);
        }

        [TestMethod]
        public void TestGetSpecificAccount()
        {
            const string accountName = "Triple T Heating & Cooling";
            var accountHierarchy = new AccountHierarchy();

            long customerId = accountHierarchy.FindSpecificCustomerByName(accountName);

            Assert.AreEqual(8978000030, customerId);


        }

        [TestMethod]
        public void GetAllCids()
        {
            var ahservice = new AccountHierarchy();
            var returnedCustomers = ahservice.GetAllSightlyAdwordsCustomerIds();

            Assert.IsNotNull(returnedCustomers);
        }

        [TestMethod]
        public void TestMccCustomerWithChildren()
        {
            //9217729477, Moore and Scarry MCC this currently has 108 child Customers
            //5426470786, Advance Digital (PAMG)
            const long mmcCustomerId = 5426470786L;
            
            var accountHierarchy = new AccountHierarchy();
            var returnedCustomer = accountHierarchy.GetAdwordsCustomerByMcc(mmcCustomerId);

            Assert.IsNotNull(returnedCustomer);
            //Assert.AreEqual(31, returnedCustomer.First().ChildCustomers.Count);

        }

        [TestMethod]
        public void GetAllSightlyCustomers()
        {
          //  const long SighltMccCustomerId = 9086250783L;


            var accountHierarchy = new AccountHierarchy();
            var returnedCustomer = accountHierarchy.GetAwordsCustomerHierachy();
            Assert.IsNotNull(returnedCustomer);

        }

        [TestMethod]
        public void GetAllCustomerFromMCCList()
        {
            List<long> customerList = new List<long>
            {
                5485078930
                ,4716165769
                ,5900643125
                ,4042173296
                ,4184301678
                ,9420806383
                ,7493359748
                ,6912216380
                ,7380337192
                ,5127219927
                ,8752108882
                ,8409054131
            };
            var accountHierarchy = new AccountHierarchy();
            var allCustomers = new List<long>();
            customerList.ForEach(customer =>
            {
                allCustomers.AddRange(accountHierarchy.GetAllCustomersFromMCC(customer));
            });

            Assert.IsNotNull(allCustomers);

        }

    }
}
