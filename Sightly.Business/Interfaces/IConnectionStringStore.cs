﻿using Sightly.Business.Enums;

namespace Sightly.Business.Interfaces
{
    public interface IConnectionStringStore
    {
        string GetKeyValue(ConnectionStringKey key);
    }
}
