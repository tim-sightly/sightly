﻿using System.Configuration;
using Sightly.Business.Enums;
using Sightly.Business.Interfaces;

namespace Sightly.Business.Utilities
{
    public class WebConnectionStore : IConnectionStringStore
    {
        public string GetKeyValue(ConnectionStringKey key)
        {
            return ConfigurationManager.ConnectionStrings[key.ToString()].ConnectionString;
        }
    }
}
