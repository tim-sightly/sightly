﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Email.Templates;
using Sightly.Models;

namespace Sightly.Email.Test
{
    [TestClass]
    public class EmailTest
    {
        [TestMethod]
        public void TestEmail()
        {
            var orderData = new OrderEmailData
            {
                AdvertiserName = "Test From The Test Client",
                CampaignName = "Super SESSY Campaign",
                CreaterName = "stefan@sightly.com",
                EndDate = DateTime.Parse("06/06/2018"),
                OrderName = "Test Order",
                OrderRefCode = "123-qwe-1324S",
                StartDate = DateTime.Parse("01/01/2018"),
                TotalBudget = 201.42M
            };

            var orderer = new NewOrderReady();
            var emailData = orderer.MakeNewOrderReady(orderData);

            var mailClient = new EmailClient();
            mailClient.SendEmail(emailData.SendToMailMessage());
            
            
        }
    }
}
