﻿using System;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.BusinessServices.cs;
using Sightly.Email.Templates;
using Sightly.Models;
using Sightly.Test.Common;

namespace Sightly.Email.Test
{
    [TestClass]
    public class ActualEmailTest
    {
        private static IContainer Container { get; set; }
        private IEmailService _emailService;
        private IEmailClient _emailClient;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _emailService = Container.Resolve<IEmailService>();
            _emailClient = Container.Resolve<IEmailClient>();
        }


        [TestMethod]
        public void GetAndSendEmail()
        {
            var orderId = Guid.Parse("CB4489FF-BAFD-4A71-91EB-A86D011A8A02");

            var orderData = _emailService.GetOrderEmailByOrder(orderId);

            //Alter the recipient for testing purposes
            orderData.CreaterName = "stefan@sightly.com";

            var orderer = new NewOrderReady();
            var emailData = orderer.MakeNewOrderReady(orderData);
            
            _emailClient.SendEmail(emailData.SendToMailMessage());

        }

        [TestMethod]
        public void GetAndSendChangeOrder()
        {
            var orderId = Guid.Parse("5B0C634F-25E1-4862-8C86-A84E0136D1EC");

            var orderData = _emailService.GetOrderEmailByOrder(orderId);
            orderData.OrderChanges = _emailService.GetOrderChangesByOrder(orderId);

            orderData.CreaterName = "stefan@sightly.com";

            var orderer = new ChangeOrderLive();
            var emailData = orderer.MakeChangeOrderLive(orderData);
            _emailClient.SendEmail(emailData.SendToMailMessage());
        }
    }
}
