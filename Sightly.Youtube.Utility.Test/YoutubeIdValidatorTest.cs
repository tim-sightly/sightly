﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Test.Common;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.YouTube.v3;

namespace Sightly.Youtube.Utility.Test
{
    [TestClass]
    public class YoutubeIdValidatorTest
    {
        private static IContainer Container { get; set; }
        private IYoutubeIdValidator _youtubeIdValidator;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _youtubeIdValidator = Container.Resolve<IYoutubeIdValidator>();
        }

        [TestMethod]
        public void TestMethod1()
        {
            var idList = new List<string>()
            {
                "7hSAx8RJpgk",
                "7i1Dn8CAZmA",
                "jUJH8BRP2Uw", //duplicate
                "7i4vdn9pzAY",
                "7i9Wwfa-hXE"
            };
            
            _youTubeService = GetYouTubeServiceAsync().Result;
            var dict = _youtubeIdValidator.FindDuplicateIdsFromListOfIds(idList, _youTubeService);
            
            Assert.AreEqual(1, dict.Count);
            Assert.AreEqual("odiK8tNonw4", dict["jUJH8BRP2Uw"] );
        }
        private static async Task<YouTubeService> GetYouTubeServiceAsync()
        {
            UserCredential credential;
            using (var stream = new FileStream("C://Users//Neal C//Desktop//Sightly//neal_branch//sightly//Sightly.Youtube.Utility.Test//Permissions.json", FileMode.Open, FileAccess.Read))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,

                    new[] { YouTubeService.Scope.YoutubeForceSsl, YouTubeService.Scope.Youtubepartner },
                    "Sightly Videos",
                    CancellationToken.None
                );
            }

            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
            });

            return youtubeService;

        }

        private static YouTubeService _youTubeService;



    }
}
