﻿using System;
using System.IO;
using System.Linq;
using Autofac;
using Sightly.Business.MediaAgency;
using Sightly.Models;
using Sightly.Test.Common;

namespace Sightly.MediaAgency.DateAndBudgetUpdate
{
    class Program
    {
        private static IContainer Container { get; set; }
        private static IDomainManager _domainManager;

        static void Initialize()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _domainManager = Container.Resolve<IDomainManager>();
        }

        static void Main(string[] args)
        {
            Console.WriteLine($"Started: {DateTime.Now}");

            Initialize();

            var mediaUpdateList = File.ReadAllLines("..\\..\\CSV\\458-616-1027.csv")
                .Skip(1)
                .Select(MediaAgencyDateBudgetUpdate.FromCsv)
                .ToList();

            mediaUpdateList.GroupBy(x => x.AwCustomerId).ToList().ForEach(mu =>
            {
                decimal totalBudget = mediaUpdateList.Where(x => x.AwCustomerId == mu.Key).Sum(y => y.CampaignBudget);
                mediaUpdateList.Where(x => x.AwCustomerId == mu.Key).ToList().ForEach(mai =>
                {
                    mai.TotalBudget = totalBudget;
                });
            });


            _domainManager.UpdateBudgetGroupTimedBudgetInfo(mediaUpdateList);



            Console.WriteLine($"Ended: {DateTime.Now}");
            Console.ReadKey();
        }        
    }
}
