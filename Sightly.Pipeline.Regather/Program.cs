﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using Autofac;
using Sightly.Adwords.Utility.Interface;
using Sightly.Business.StatsGather;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;

namespace Sightly.Pipeline.Regather
{
    class Program
    {
        private static IContainer Container { get; set; }
        private static IGatherStats _gatherStats;
        private static IStatRepository _statRepository;

        static void Initialize()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _gatherStats = Container.Resolve<IGatherStats>();
            _statRepository = Container.Resolve<IStatRepository>();
        }
        static void Main()
        {
            Initialize();

            Console.WriteLine($"Started At {DateTime.Now}");

            VerifyAndRerunOutOfBoundsCustomers();

            //VerifyAndRerunOutOfBoundsByDate();

            Console.WriteLine($"Ended At {DateTime.Now}");
            Console.WriteLine($"--------------Validation Complete-----------------");
            Console.ReadKey();
        }

        private static void VerifyAndRerunOutOfBoundsByDate()
        {
            var statDate = new DateTime(2017, 11, 21);

            var customers = _statRepository.GetDailyStatsRegatherByDate(statDate);
            customers.ForEach(customer =>
            {
                Console.WriteLine($"**Regather {customer} for {statDate}**");
                _statRepository.DeleteRawStatsData(customer, statDate);
                //Console.WriteLine($"{DateTime.Now} -  {dateCounter}");
                _gatherStats.Execute(customer, statDate);

                SaveOffValidation(customer, statDate);
            });
        }

        private static void VerifyAndRerunOutOfBoundsCustomers()
        {
            var customerIds = new List<long>
            {

                9069914356

            };

            customerIds.ForEach(customerId =>
            {
                //Get Start and End (or todays date) for the processing of our daily validation
                var customerStartEndDate = _statRepository.GetStartAndEndDateForCustomerById(customerId);
                Console.WriteLine($"Validating started for {customerId} over {customerStartEndDate.StartDate} to {customerStartEndDate.EndDate}");
                var today = DateTime.Now.Date;
                if (customerStartEndDate.EndDate > today)
                    customerStartEndDate.EndDate = today;

                for (var dateCounter = customerStartEndDate.StartDate.Date;
                    dateCounter <= customerStartEndDate.EndDate.Date;
                    dateCounter = dateCounter.AddDays(1).Date)
                {
                    Console.WriteLine($"{dateCounter}");
                    var dailyChecker =
                        _statRepository.GetValidationForDailyRawDataByCustomerIdAndDate(customerId, dateCounter);
                    //Check to see if the 4 sections add up and are equal If not - regather

                    Console.WriteLine($"Validating {customerId} for {dateCounter}");

                    if (!ValidateRawData(dailyChecker))
                    {
                        Console.WriteLine($"**Regather {customerId} for {dateCounter}**");
                        _statRepository.DeleteRawStatsData(customerId, dateCounter);
                        //Console.WriteLine($"{DateTime.Now} -  {dateCounter}");
                        _gatherStats.Execute(customerId, dateCounter);
                    }
                }
            });


        }

        private static bool ValidateRawData(List<CustomerCampaignRawStatsByDayData> dailyChecker)
        {
            bool validated = true;

            dailyChecker.ForEach(row =>
            {
                if (
                    (row.AdClicks != row.AgeClicks           || row.AdClicks != row.GenderClicks           || row.AgeClicks != row.GenderClicks)          ||
                    (row.AdCost != row.AgeCost               || row.AdCost != row.GenderCost               || row.AgeCost != row.GenderCost)              ||
                    (row.AdImpressions != row.AgeImpressions || row.AdImpressions != row.GenderImpressions || row.AgeImpressions != row.GenderImpressions)||
                    (row.AdViews != row.AgeViews             || row.AdViews != row.GenderViews             || row.AgeViews != row.GenderViews)
                   )
                    validated = false;
            });

            return validated;
        }
        private static void SaveOffValidation(long customerId, DateTime statDate)
        {
            var dailyChecker =
                _statRepository.GetValidationForDailyRawDataByCustomerIdAndDate(customerId, statDate);
            var reportedStats = new CustomerRawStatsByDayDate { CustomerId = customerId, StatDate = statDate };
            dailyChecker.ForEach(campaign =>
            {
                reportedStats.AdClicks += campaign.AdClicks;
                reportedStats.AgeClicks += campaign.AgeClicks;
                reportedStats.GenderClicks += campaign.GenderClicks;
                reportedStats.AdCost += campaign.AdCost;
                reportedStats.AgeCost += campaign.AgeCost;
                reportedStats.GenderCost += campaign.GenderCost;
                reportedStats.AdImpressions += campaign.AdImpressions;
                reportedStats.AgeImpressions += campaign.AgeImpressions;
                reportedStats.GenderImpressions += campaign.GenderImpressions;
                reportedStats.AdViews += campaign.AdViews;
                reportedStats.AgeViews += campaign.AgeViews;
                reportedStats.GenderViews += campaign.GenderViews;
            });
            _statRepository.InsertDailyStatsValidation(reportedStats);
        }
    }
}
