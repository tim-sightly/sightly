﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Reporting.DAL.DTO;

namespace Reporting.DAL
{
    public class AccountStore : AReportStore, IAccountStore
    {
        public List<Campaign> GetAllCampaigns(IDbConnection connection = null)
        {
            try
            {
                string sql = @"SELECT CampaignId, CampaignName, CampaignRefCode FROM Accounts.Campaign;";

                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    List<Campaign> campaigns = db.Query<Campaign>(sql).ToList();
                    return campaigns;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<CampaignAwCustomer> GetAllMediaAgencyCampaigns(IDbConnection connection = null)
        {
            try
            {
                string sql = @"SELECT  c.CampaignId,  c.CampaignName + '-' + CAST(cai.AdWordsCustomerId AS VARCHAR(12)) AS CampaignName, cai.AdWordsCustomerId
                                FROM Accounts.Account a 
                                INNER JOIN Accounts.Advertiser adv on a.AccountId = adv.AccountId
                                INNER JOIN Accounts.Campaign c ON c.AccountId = a.AccountId and adv.AdvertiserId = c.AdvertiserId
                                INNER JOIN Accounts.CampaignAdWordsInfo cai on c.CampaignId = cai.CampaignId
                                WHERE a.AccountTypeId = 'D600FA41-FFC0-4B03-8272-CAE6E093082F'
                                and cai.AdWordsCustomerId IS NOT NULL;";

                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    List<CampaignAwCustomer> campaigns = db.Query<CampaignAwCustomer>(sql).ToList();
                    return campaigns;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string GetCampaignName(long cid, IDbConnection connection = null)
        {
            try
            {
                string sql = @"SELECT  c.CampaignName FROM Accounts.Campaign c
                INNER JOIN [Accounts].[CampaignAdWordsInfo] cai on c.CampaignId = cai.CampaignId
                WHERE cai.AdwordsCustomerId = @cid";

                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    var result = db.Query<string>(sql, new { cid = cid }).FirstOrDefault();
                    return string.IsNullOrEmpty(result) ? "" : result;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}
