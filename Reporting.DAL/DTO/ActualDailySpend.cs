﻿using System;

namespace Reporting.DAL.DTO
{
    public class ActualDailySpend
    {
       public decimal TotalSpend { get; set; } 
       public DateTime StatDate { get; set; } 
    }
}