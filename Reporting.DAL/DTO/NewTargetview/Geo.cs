﻿using System.Collections.Generic;

namespace Reporting.DAL.DTO.NewTargetview
{
    public class Geo
    {
        public List<GeoData> SelectedServiceAreas { get; set; }


        public Geo()
        {
            SelectedServiceAreas = new List<GeoData>();
        }
    }
}
