﻿using System.Collections.Generic;

namespace Reporting.DAL.DTO.NewTargetview
{
    public class Order
    {
        public Info Info { get; set; }
        public List<Ad> Ads { get; set; }

        public Audience Audience { get; set; }

        public Geo Geo { get; set; }

        public Order()
        {
            Ads = new List<Ad>();
        }
    }
}
