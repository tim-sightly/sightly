﻿using System;

namespace Reporting.DAL.DTO.NewTargetview
{
    public class GeoData
    {
        public string DmaName { get; set; }
        public Guid Id { get; set; }
        public long OriginalPopulation { get; set; }
    }
}
