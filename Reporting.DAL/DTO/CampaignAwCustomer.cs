﻿using System;

namespace Reporting.DAL.DTO
{
    public class CampaignAwCustomer
    {
        public Guid CampaignId { get; set; }
        public string CampaignName { get; set; }
        public long AdWordsCustomerId { get; set; }
    }
}