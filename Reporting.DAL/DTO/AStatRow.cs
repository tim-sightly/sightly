﻿namespace Reporting.DAL.DTO
{
    public abstract class AStatRow
    {
        public long Impressions { get; set; }
        public long Views { get; set; }
        public double ViewRate => Impressions == 0 ? 0 : (double)Views / Impressions;
        public long Clicks { get; set; }
        public double CTR => Impressions == 0 ? 0 : (double)Clicks / Impressions;
        public decimal TotalSpend { get; set; }
        public decimal AvgCPCV => (Clicks == 0 || Views == 0) ? 0 : (TotalSpend / Views);
        public decimal Ecpm =>  TotalSpend / Impressions * 1000;
    }
}
