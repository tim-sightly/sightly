﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Reporting.DAL.DTO
{
    public class PerformanceReport
    {
        public string CampaignName { get; set; }
        public CampaignToDate CampaignToDate { get; set; }

        public Performance Performance { get; set; }

        public List<AdStats> AdStats { get; set; }

        public List<AgeStats> AgeStats { get; set; }

        public List<GenderStats> GenderStats { get; set; }

        public List<DeviceStats> DeviceStats { get; set; }

        public DateTime CampaignStartDate
        {
            get { return BudgetGroupTimedBudgetGroups.Min( tb => tb.StartDate ); }
        }
        
        public DateTime CampaignEndDate
        {
            get { return BudgetGroupTimedBudgetGroups.Max( tb => tb.EndDate ); }
        }

        public DateTime OrderStartDate { get; set; }
        public DateTime OrderEndDate { get; set; }

        public List<BudgetGroupTimedBudgetGroup> BudgetGroupTimedBudgetGroups { get; set; }
        
        public List<ActualDailySpend> ActualDailySpend { get; set; }
    }
}
