﻿using System;
using System.Collections.Generic;
using System.Net;

namespace Reporting.DAL.DTO
{
    public class MediaBrief
    {
        public OrderData OrderData { get; set; }


        public List<GenderAgeData> GenderAgeList { get; set; }
        public List<LocationData> LocationList { get; set; }

        public BudgetData BudgetData { get; set; }

        public List<string> KeywordList { get; set; }

        public List<AdData> AdList { get; set; }

        public List<HouseholdIncomeData> HouseholdIncomeList { get; set; }
        public KeywordUrl KeywordUrl { get; set; }

        public MediaBrief()
        {
            GenderAgeList = new List<GenderAgeData>();
            LocationList = new List<LocationData>();
            KeywordList = new List<string>();
            AdList = new List<AdData>();
            HouseholdIncomeList = new List<HouseholdIncomeData>();
        }
    }

    public class KeywordUrl
    {
        public string KeyWords { get; set; }
        public string CompetitorsUrls { get; set; }

        public string TargetingNotes { get; set; }
    }

    public class OrderData
    {

        public string AccountName { get; set; }
        public string AdvertiserName { get; set; }
        public string AdvertiserCategoryName { get; set; }
        public string CampaignName { get; set; }
        public string OrderTypeName { get; set; }
        public string IOName { get; set; }
        public string OrderName { get; set; }
        public string OrderRefCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int NumOfDays { get; set; }
    }
    public class GenderAgeData
    {
        public string GenderName { get; set; }
        public string AgeGroupName { get; set; }
    }

    public class HouseholdIncomeData
    {
        public string IncomeLabels { get; set; }
    }

    public class LocationData
    {
        public string LocationName { get; set; }
        public string GeographyName { get; set; }
    }

    public class BudgetData
    {
        public decimal GrossBudget { get; set; }
        public decimal SightlyBudget { get; set; }
        public decimal CampaignBudget { get; set; }
        public decimal SightlyMargin { get; set; }
        public decimal DailyGrossBudget { get; set; }
        public decimal DailySightlyBudget { get; set; }
        public decimal DailyCampaignBudget { get; set; }
    }

    public class AdData
    {
        public string AdName { get; set; }
        public string VideoAdAsset { get; set; }
        public string VideoUrl { get; set; }
        public Guid VideoId { get; set; }
        public int VideoAdId { get; set; }
        public string DisplayUrl { get; set; }
        public string DestinationUrl { get; set; }
        public int VideoLengthInSeconds { get; set; }
        public Guid? BannerAssetId { get; set; }
        public string CompanionBannerAsset { get; set; }
        public string CompanionBannerUrl { get; set; }  
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Paused { get; set; }
    }

}
