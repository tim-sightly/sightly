﻿using System;

namespace Reporting.DAL.DTO
{
    public class BillingData
    {
        public long AWCustomerID { get; set; }
        public long AWCampaignID { get; set; }
        public string TopParent { get; set; }
        public string Account { get; set; }
        public string Advertiser { get; set; }
        public Guid AdvertiserID { get; set; }
        public string Campaign { get; set; }
        public string Order { get; set; }
        public Guid OrderID { get; set; }
        public string OrderStatus { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal SightlyMargin { get; set; }
        public decimal CustomerMargin { get; set; }
        public decimal GrossBudget { get; set; }
        public decimal SightlyBudget { get; set; }
        public decimal NetBudget { get; set; }
        public decimal GrossSpend { get; set; }
        public decimal SightlySpend { get; set; }
        public decimal NetSpend { get; set; }
        public decimal MTDGrossSpend { get; set; }
        public decimal MTDSightlySpend { get; set; }
        public decimal MTDNetSpend { get; set; }
        public decimal SightlySpendToPreviousMonthsEnd { get; set; }

    }
}