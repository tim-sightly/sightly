﻿using System;
using System.Collections.Generic;

namespace Reporting.DAL.DTO
{
    public class CampaignPlacementReport
    {
        public string CampaignName { get; set; }
        public List<MediaAgencyCampaignPlacement> MediaAgencyCampaignPlacements { get; set; }

        public CampaignPlacementReport()
        {
            MediaAgencyCampaignPlacements = new List<MediaAgencyCampaignPlacement>();
        }
    }
}
