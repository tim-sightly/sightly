﻿namespace Reporting.DAL.DTO
{
    public class AdStats : AStatRow
    {
        private double _q25;
        private double _q50;
        private double _q75;
        private double _q100;

        public string AdName { get; set; }

        public double Q25
        {
            get { return _q25 / 100; }
            set { _q25 = value; }
        }
        public double Q50
        {
            get { return _q50 / 100; }
            set { _q50 = value; }
        }
        public double Q75
        {
            get { return _q75 / 100; }
            set { _q75 = value; }
        }
        public double Q100
        {
            get { return _q100 / 100; }
            set { _q100 = value; }
        }
    }
}
