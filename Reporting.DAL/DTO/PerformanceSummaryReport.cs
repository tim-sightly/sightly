﻿using System;
using System.Collections.Generic;

namespace Reporting.DAL.DTO
{
    public class PerformanceSummaryReport
    {
        public DateTime ReportDate { get; set; }
        public DateTime RequestedStartDate { get; set; }
        public DateTime RequestedEndDate { get; set; }
        public DateTime AvailableStartDate { get; set; }
        public DateTime AvailableEndDate { get; set; }

        public List<OrderInfoStat> OrderInfoStats { get; set; }

        public PerformanceSummaryReport()
        {
            OrderInfoStats = new List<OrderInfoStat>();
        }

    }
}