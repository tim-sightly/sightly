﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class AgeGroup
    {
        /// <summary>
        /// Property that holds the ID of an age group.
        /// </summary>
        public Guid AgeGroupId { get; set; }

        /// <summary>
        /// Property that holds the name of an age group.
        /// </summary>
        public string AgeGroupName { get; set; }

        /// <summary>
        /// Property that holds the end age of an age group.
        /// </summary>
        public int? EndAge { get; set; }

        /// <summary>
        /// Property that holds the flag that indicates
        /// that an age group have an indefinite end age.
        /// </summary>
        public bool IsStartAgeUpwards { get; set; }

        /// <summary>
        /// Property that holds the population 
        /// percentage of an age group.
        /// </summary>
        public decimal PopulationPercentage { get; set; }

        /// <summary>
        /// Property that holds start age of an age group.
        /// </summary>
        public int? StartAge { get; set; }
    }
}