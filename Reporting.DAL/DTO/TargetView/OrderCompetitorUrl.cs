﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class OrderCompetitorUrl
    {
        /// <summary>
        /// Property that holds the
        /// competitor URL.
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Property that holds the
        /// ID of the competitor URL.
        /// </summary>
        public Guid? OrderCompetitorUrlId { get; set; }

        /// <summary>
        /// Property that holds the Id of the
        /// order on which competitor URL belongs.
        /// </summary>
        public Guid? OrderId { get; set; }

        /// <summary>
        /// Property that holds the ID that indicates
        /// the order of the competitor URLs.
        /// </summary>
        public int? SequenceId { get; set; }
    }
}