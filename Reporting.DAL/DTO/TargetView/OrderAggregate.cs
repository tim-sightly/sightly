﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.DAL.DTO.TargetView
{
    public class OrderAggregate
    {
        private long _impressions;
        private long _views;
        private decimal _clickThroughRate;
        private decimal _viewRate;
        private long _clicks;
        private decimal _cost;
        private decimal _viewTime;
        private decimal _reach;
        private decimal _cpv;

        public OrderAggregate()
        {
            _impressions = 0;
            _clickThroughRate = 0.00m;
            _views = 0;
            _viewRate = 0.00m;
            _clicks = 0;
            _cost = 0.00m;
            _viewTime = 0.00m;
            _reach = 0.00m;
            _cpv = 0.00m;
        }

        public long Impressions
        {
            get { return _impressions; }
            set { _impressions = value; }
        }

        public long Views
        {
            get { return _views; }
            set { _views = value; }
        }

        public decimal ViewRate
        {
            get { return _viewRate; }
            set { _viewRate = value; }
        }

        public long Clicks
        {
            get { return _clicks; }
            set { _clicks = value; }
        }

        public decimal ClickThroughRate
        {
            get { return _clickThroughRate; }
            set { _clickThroughRate = value; }
        }

        public decimal Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }

        public decimal ViewTime
        {
            get { return _viewTime; }
            set { _viewTime = value; }
        }

        public decimal Reach
        {
            get { return _reach; }
            set { _reach = value; }
        }

        public decimal CPV
        {
            get { return _clickThroughRate; }
            set { _clickThroughRate = value; }
        }
    }
}
