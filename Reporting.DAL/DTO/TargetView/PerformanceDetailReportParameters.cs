﻿using System;
using System.Collections.Generic;
using System.Dynamic;

namespace Reporting.DAL.DTO.TargetView
{
    public class PerformanceDetailReportParameters
    {
        public string AdvertiserName { get; set; }
        public Guid? ApiSessionId { get; set; }
        public List<AdPerformanceDetail> AdPerformanceDetails { get; set; }
        public List<AgeGroupPerformanceDetail> AgeGroupPerformanceDetails { get; set; }
        public OrderAggregate OrderAggregate { get; set; }

        public string CampaignName { get; set; }
        public List<Order> CampaignOrders { get; set; }
        public long? TargetPopulation { get; set; }
        public string FormattedTargetPopulation
        {
            get
            {
                if (TargetPopulation.HasValue)
                    return TargetPopulation.Value.ToString("N0");

                return string.Empty;
            }
        }
        public List<DeviceTypePerformanceDetail> DevicePerformanceDetails { get; set; }
        public DateTime EndDate { get; set; }

        public string EndDateString
        {
            get
            {
                if (EndDate != null)
                    return EndDate.Day.ToString("mm/dd/yyyy");
                return string.Empty;
            }
        }

        public List<GenderPerformanceDetail> GenderPerformanceDetails { get; set; }
        public string OrderName { get; set; }
        public List<ConversionPerformanceDetail> OrderPerformanceDetailConversionStat { get; set; }
        //public ReportFormat ReportFormat { get; set; }
        public string ReportPeriodText { get; set; }
        public bool? ShowConversionStatSection { get; set; }
        public DateTime StartDate { get; set; }
        public string StartDateString  
        {
            get
            {
                if (StartDate != null)
                    return StartDate.Day.ToString("mm/dd/yyyy");
                return string.Empty;
                
            }
        }


        //public class CampaignPopulation
        //{
        //    #region Properties
        //    /// <summary>
        //    /// Property that holds the aggregated target
        //    /// population of the campaign.
        //    /// </summary>
        //    public long? TargetPopulation { get; set; }
        //    #endregion

        //    #region Read-Only Properties
        //    /// <summary>
        //    /// Read-only property that holds the formatted
        //    /// target population.
        //    /// </summary>
        //    public string FormattedPopulation
        //    {
        //        get
        //        {
        //            if (TargetPopulation.HasValue)
        //                return TargetPopulation.Value.ToString("N0");

        //            return string.Empty;
        //        }
        //    }
        //    #endregion
        //}

        //public class ConversionPerformanceDetail
        //{

        //    #region Properties
        //    /// <summary>
        //    /// Property that holds the ID of the conversion action.
        //    /// </summary>
        //    public Guid ConversionActionId { get; set; }

        //    /// <summary>
        //    /// Property that holds the name of the converion action.
        //    /// </summary>
        //    public string ConversionActionName { get; set; }

        //    /// <summary>
        //    /// Property that holds the value for cross-device
        //    /// conversion.
        //    /// </summary>
        //    public long? CrossDeviceConversions { get; set; }

        //    /// <summary>
        //    /// Property that holds the value for cost per
        //    /// conversion.
        //    /// </summary>
        //    public decimal? CostPerConversion { get; set; }

        //    /// <summary>
        //    /// Property that holds the value for impression
        //    /// conversion.
        //    /// </summary>
        //    public long? ImpressionConversions { get; set; }

        //    /// <summary>
        //    /// Property that holds the value for conversion.
        //    /// </summary>
        //    public long? SameDeviceConversions { get; set; }

        //    /// <summary>
        //    /// Property that holds the computer value for
        //    /// total conversion.
        //    /// </summary>
        //    public decimal? TotalConversions { get; set; }

        //    /// <summary>
        //    /// Property that holds the computed value for
        //    /// total conversion rate.
        //    /// </summary>
        //    public decimal? TotalConversionRate { get; set; }

        //    /// <summary>
        //    /// Property that holds the computed value of
        //    /// total conversion value.
        //    /// </summary>
        //    public decimal? TotalConversionValue { get; set; }
        //    #endregion

        //    #region Read-only Properties
        //    /// <summary>
        //    /// Read-only property that holds the value 
        //    /// for cross-device conversion.
        //    /// </summary>
        //    public string FormattedCrossDeviceConversions
        //    {
        //        get
        //        {
        //            if (CrossDeviceConversions.HasValue)
        //                return CrossDeviceConversions.Value.ToString("N0");

        //            return "-";
        //        }
        //    }

        //    /// <summary>
        //    /// Read-only property that holds the value 
        //    /// for cost per conversion.
        //    /// </summary>
        //    public string FormattedCostPerConversions
        //    {
        //        get
        //        {
        //            if (CostPerConversion.HasValue)
        //                return CostPerConversion.Value.ToString("C2");

        //            return "-";
        //        }
        //    }

        //    /// <summary>
        //    /// Read-only property that holds the value 
        //    /// for impression conversion.
        //    /// </summary>
        //    public string FormattedImpressionConversions
        //    {
        //        get
        //        {
        //            if (ImpressionConversions.HasValue)
        //                return ImpressionConversions.Value.ToString("N0");

        //            return "-";
        //        }
        //    }

        //    /// <summary>
        //    /// Read-only property that holds the value 
        //    /// for conversion.
        //    /// </summary>
        //    public string FormattedSameDeviceConversions
        //    {
        //        get
        //        {
        //            if (SameDeviceConversions.HasValue)
        //                return SameDeviceConversions.Value.ToString("N0");

        //            return "-";
        //        }
        //    }

        //    /// <summary>
        //    /// Read-only property that holds the computed 
        //    /// value for total conversion.
        //    /// </summary>
        //    public string FormattedTotalConversion
        //    {
        //        get
        //        {
        //            if (TotalConversions.HasValue)
        //                return TotalConversions.Value.ToString("N0");

        //            return "-";
        //        }
        //    }

        //    /// <summary>
        //    /// Read-only property that holds the computed 
        //    /// value for total conversion rate.
        //    /// </summary>
        //    public string FormattedTotalConversionRate
        //    {
        //        get
        //        {
        //            if (TotalConversionRate.HasValue)
        //                return TotalConversionRate.Value.ToString("N2") + "%";

        //            return "-";
        //        }
        //    }

        //    /// <summary>
        //    /// Read-only property that holds the computed 
        //    /// value of total conversion value.
        //    /// </summary>
        //    public string FormattedTotalConversionValue
        //    {
        //        get
        //        {
        //            if (TotalConversionValue.HasValue)
        //                return TotalConversionValue.Value.ToString("C0");

        //            return "-";
        //        }
        //    }
        //    #endregion
        //}
    }
}