﻿using System;
using System.Collections.Generic;

namespace Reporting.DAL.DTO.TargetView
{
    public class Order
    {
        #region Properties
        /// <summary>
        /// Property that holds ID of the
        /// account that owns the order.
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Property that holds the name of 
        /// the account that owns the order.
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// Property that holds the number of ads
        /// assigned to the order.
        /// </summary>
        public int AdCount { get; set; }

        /// <summary>
        /// Property that holds the duration of the
        /// estimate's ad in seconds.
        /// </summary>
        public int AdDurationInSeconds { get; set; }

        /// <summary>
        /// Property that holds the ads of an order.
        /// </summary>
        public List<Ad> Ads { get; set; }

        /// <summary>
        /// Property that holds the ID
        /// of the related advertiser.
        /// </summary>
        public Guid? AdvertiserId { get; set; }

        /// <summary>
        /// Property that holds the name
        /// of the related advertiser.
        /// </summary>
        public string AdvertiserName { get; set; }

        /// <summary>
        /// Property that holds the ID
        /// of the category of the advertiser.
        /// </summary>
        public Guid? AdvertiserCategoryId { get; set; }

        /// <summary>
        /// Property that holds the name
        /// of the category of the advertiser.
        /// </summary>
        public string AdvertiserCategoryName { get; set; }

        /// <summary>
        /// Property that holds the ID 
        /// of the sub-category of the advertiser.
        /// </summary>
        public Guid? AdvertiserSubCategoryId { get; set; }

        /// <summary>
        /// Property that holds the name
        /// of the sub-category of the advertiser.
        /// </summary>
        public string AdvertiserSubCategoryName { get; set; }

        /// <summary>
        /// Property that holds the campaign ID of
        /// the order on AdWords.
        /// </summary>
        public long? AdWordsCampaignId { get; set; }

        /// <summary>
        /// Property that holds the campaign name of the
        /// order on AdWords.
        /// </summary>
        public string AdWordsCampaignName { get; set; }

        /// <summary>
        /// Property that holds the customer ID of
        /// the order's advertiser on AdWords.
        /// </summary>
        public long? AdWordsCustomerId { get; set; }

        /// <summary>
        /// Property that holds the IDs of targeted
        /// age groups of the order.
        /// </summary>
        public List<Guid> AgeGroupIds { get; set; }

        /// <summary>
        /// Property that holds the assumed view rate of an order.
        /// </summary>
        public decimal? AssumedViewRate { get; set; }

        /// <summary>
        /// Property that holds the average
        /// cost per completed view of an order.
        /// </summary>
        public decimal? AvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the selected budget
        /// allocation type for the order.
        /// </summary>
        public string BudgetAllocationName { get; set; }

        /// <summary>
        /// Property that holds the ID of a
        /// budget group an order belongs.
        /// </summary>
        public Guid? BudgetGroupId { get; set; }

        /// <summary>
        /// Property that holds the name of
        /// a budget group an order belongs.
        /// </summary>
        public string BudgetGroupName { get; set; }

        /// <summary>
        /// Property that holds the list of
        /// a budget groups of an order.
        /// </summary>
        public List<OrderBudgetGroup> BudgetGroupList { get; set; }

        /// <summary>
        /// Property that holds the ID
        /// of the related budget type.
        /// </summary>
        public Guid? BudgetTypeId { get; set; }

        /// <summary>
        /// Property that holds the
        /// budget type name of the order.
        /// </summary>
        public string BudgetTypeName { get; set; }

        /// <summary>
        /// Property that holds the 
        /// campaign budget.
        /// </summary>
        public decimal? CampaignBudget { get; set; }

        /// <summary>
        /// Property that holds the ID
        /// of the related campaign.
        /// </summary>
        public Guid? CampaignId { get; set; }

        /// <summary>
        /// Property that holds the name
        /// of the related campaign.
        /// </summary>
        public string CampaignName { get; set; }

        /// <summary>
        /// Property that holds the date 
        /// when the order was canceled.
        /// </summary>
        public DateTime? CanceledDatetime { get; set; }

        /// <summary>
        /// Property that holds the information 
        /// on the user who canceled the order.
        /// </summary>
        public User Canceler { get; set; }

        /// <summary>
        /// Property that holds the ID of 
        /// the user who cancel the order.
        /// </summary>
        public Guid? CancelerId { get; set; }

        /// <summary>
        /// Property that holds the list of 
        /// change order records related to 
        /// the order including its parent order.
        /// </summary>
        public List<Order> ChangeOrderFamily { get; set; }

        /// <summary>
        /// Property that holds the IDs of selected
        /// change types of a change order.
        /// </summary>
        public List<Guid> ChangeTypeIds { get; set; }

        /// <summary>
        /// Property that holds the order's
        /// competitor URLs.
        /// </summary>
        public List<OrderCompetitorUrl> CompetitorUrls { get; set; }

        /// <summary>
        /// Property that holds the completed
        /// views of an order.
        /// </summary>
        public long? CompletedViews { get; set; }

        /// <summary>
        /// Property that holds the conversion
        /// actions of the order.
        /// </summary>
        public List<ConversionAction> ConversionActions { get; set; }

        /// <summary>
        /// Property that holds the date 
        /// when the order was created.
        /// </summary>
        public DateTime CreatedDatetime { get; set; }

        /// <summary>
        /// Property that holds the information
        /// on the user who created the order.
        /// </summary>
        public User Creator { get; set; }

        /// <summary>
        /// Property that holds the ID of 
        /// the user that created the order.
        /// </summary>
        public Guid? CreatorId { get; set; }

        /// <summary>
        /// Property that holds the 
        /// customer account margin.
        /// </summary>
        public decimal? CustomerAccountMargin { get; set; }

        /// <summary>
        /// Property that holds the flag
        /// if the order is already deleted.
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Property that holds the list 
        /// of delivery key performance 
        /// indicators of the order.
        /// </summary>
        public List<DeliveryKpi> DeliveryKpis { get; set; }

        /// <summary>
        /// Property that holds the ID of 
        /// the delivery priority of the order.
        /// </summary>
        public Guid? DeliveryPriorityId { get; set; }

        /// <summary>
        /// Property that holds the end date
        /// of the order.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Property that holds the end date
        /// hard of the order.
        /// </summary>
        public bool EndDateHard { get; set; }

        /// <summary>
        /// Property that holds the assumed
        /// cost per completed view of an order.
        /// </summary>
        public decimal? EstimatedCpcv { get; set; }

        /// <summary>
        /// Property that holds the IDs of
        /// targeted genders of the order.
        /// </summary>
        public List<Guid> GenderIds { get; set; }

        /// <summary>
        /// Property that holds the
        /// key terms of the order.
        /// </summary>
        public List<OrderKeyTerm> KeyTerms { get; set; }

        /// <summary>
        /// Property that holds the name of
        /// the most recent entity that edited 
        /// the order.
        /// </summary>
        public string LastModifiedBy { get; set; }

        /// <summary>
        /// Property that holds the latest date and 
        /// time when the order record was modified.
        /// </summary>
        public DateTime LastModifiedDatetime { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// order's location.
        /// </summary>
        public Guid? LocationId { get; set; }

        /// <summary>
        /// Property that holds the ID of locations
        /// assigned to the order.
        /// </summary>
        public List<Guid> LocationIds { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// order's location.
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// order's location names.
        /// </summary>
        public string LocationNames { get; set; }

        /// <summary>
        /// Property that holds the minimum location
        /// budget for the order.
        /// </summary>
        public decimal? MinimumLocationBudget { get; set; }

        /// <summary>
        /// Property that holds number of months
        /// of the order.
        /// </summary>
        public int? MonthCount { get; set; }

        /// <summary>
        /// Property that holds monthly budget 
        /// of the order.
        /// </summary>
        public decimal? MonthlyBudget { get; set; }

        /// <summary>
        /// Property that holds the objectives 
        /// of the order.
        /// </summary>
        public List<Objective> Objectives { get; set; }

        /// <summary>
        /// Property that holds the approval
        /// status name of the order.
        /// </summary>
        public string OrderApprovalStatusName { get; set; }

        /// <summary>
        /// Property that holds the conversion 
        /// gathering durations of an order.
        /// </summary>
        public List<OrderConversionGatheringDuration> OrderConversionGatheringDurations { get; set; }

        /// <summary>
        /// Property that holds the ID
        /// of the order.
        /// </summary>
        public Guid? OrderId { get; set; }

        /// <summary>
        /// Property that holds the name
        /// of the order.
        /// </summary>
        public string OrderName { get; set; }

        /// <summary>
        /// Property that holds the
        /// reference code of the order.
        /// </summary>
        public string OrderRefCode { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// record log of the order.
        /// </summary>
        public Guid OrderRecordLogId { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// order status.
        /// </summary>
        public Guid OrderStatusId { get; set; }

        /// <summary>
        /// Property that holds the 
        /// status name of the order.
        /// </summary>
        public string OrderStatusName { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// order type.
        /// </summary>
        public Guid OrderTypeId { get; set; }

        /// <summary>
        /// Property that holds the
        /// type name of the order.
        /// </summary>
        public string OrderTypeName { get; set; }

        /// <summary>
        /// Property that holds the ID of the parent
        /// of an order.
        /// </summary>
        public Guid? ParentOrderId { get; set; }

        /// <summary>
        /// Property that holds the IDs of targeted
        /// parental statuses of the order.
        /// </summary>
        public List<Guid> ParentalStatusIds { get; set; }

        /// <summary>
        /// Property that holds the date when a live
        /// order is paused.
        /// </summary>
        public DateTime PauseDate { get; set; }

        /// <summary>
        /// Property that holds the reason why the
        /// order has been paused.
        /// </summary>
        public string PauseReason { get; set; }

        /// <summary>
        /// Property that holds the reach of an order.
        /// </summary>
        public decimal? Reach { get; set; }

        /// <summary>
        /// Property that holds the remaining budget
        /// of an order.
        /// </summary>
        public decimal? RemainingBudget { get; set; }

        /// <summary>
        /// Property that indicates whether the budget
        /// would be rolled between locations.
        /// </summary>
        public bool? RollBudgetBetweenLocations { get; set; }

        /// <summary>
        /// Property that holds the sightly margin.
        /// </summary>
        public decimal? SightlyMargin { get; set; }

        /// <summary>
        /// Property that holds the spend flex
        /// value of an order.
        /// </summary>
        public bool SpendFlex { get; set; }

        /// <summary>
        /// Property that holds the start
        /// date of the order.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Property that holds the start
        /// date hard of the order.
        /// </summary>
        public bool StartDateHard { get; set; }

        /// <summary>
        /// Property that holds that date 
        /// when the order was submitted.
        /// </summary>
        public DateTime? SubmittedDatetime { get; set; }

        /// <summary>
        /// Property that holds the information 
        /// on the user who submitted the order.
        /// </summary>
        public User Submitter { get; set; }

        /// <summary>
        /// Property that holds the ID of 
        /// the user who submitted the order.
        /// </summary>
        public Guid? SubmitterId { get; set; }

        /// <summary>
        /// Property that holds the target age groups.
        /// </summary>
        public List<AgeGroup> TargetAgeGroups { get; set; }

        /// <summary>
        /// Property that holds the target genders.
        /// </summary>
        public List<Gender> TargetGenders { get; set; }

        /// <summary>
        /// Property that holds the targeting type.
        /// </summary>
        public TargetingType TargetingType { get; set; }

        /// <summary>
        /// Property that holds the ID of 
        /// the audience targeting type of 
        /// the order.
        /// </summary>
        public Guid? TargetingTypeId { get; set; }

        /// <summary>
        /// Property that holds the name of 
        /// the order's targeting type.
        /// </summary>
        public string TargetingTypeName { get; set; }

        /// <summary>
        /// Property that holds the target location.
        /// </summary>
        public Location TargetLocation { get; set; }

        /// <summary>
        /// Property that holds the target parental status.
        /// </summary>
        public List<ParentalStatus> TargetParentalStatuses { get; set; }

        /// <summary>
        /// Property that holds the calculated total
        /// budget of the order.
        /// </summary>
        public decimal? TotalBudget { get; set; }

        /// <summary>
        /// Property that hold the 
        /// total money spent by the order.
        /// </summary>
        public decimal? TotalSpend { get; set; }

        /// <summary>
        /// Property that holds a 
        /// comma-separated video ad names.
        /// </summary>
        public string VideoAdNames { get; set; }

        /// <summary>
        /// Property that holds the view rate of an order.
        /// </summary>
        public double? ViewRate { get; set; }

        /// <summary>
        /// Property that holds the view time of an order.
        /// </summary>
        public long? ViewTime { get; set; }
        #endregion

        #region Read-Only Properties
        /// <summary>
        /// Read-only property that holds the related
        /// customer name of the order on AdWords.
        /// </summary>
        public string AdWordsCustomerName
        {
            get
            {
                return string.Format(
                    "{0} - {1}",
                    this.AdvertiserName,
                    this.CampaignId
                );
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// calculated total budget of the order.
        /// </summary>
        public decimal? Budget
        {
            get
            {
                if (MonthlyBudget.HasValue)
                    return MonthlyBudget.Value
                           * MonthCount.GetValueOrDefault(1);

                return CampaignBudget;
            }
        }

        /// <summary>
        /// Read-only property that holds the 
        /// computed end date of the order.
        /// </summary>
        public DateTime? ComputedEndDate
        {
            get
            {
                if (EndDate.HasValue)
                {
                    return EndDate;
                }
                else if (MonthCount.HasValue && StartDate.HasValue)
                {
                    return StartDate.Value.AddMonths(
                        MonthCount.Value
                    );
                }

                return null;
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted
        /// average cost per completed view of an order.
        /// </summary>
        public string FormattedAvgCPCV
        {
            get
            {
                if (AvgCPCV.HasValue)
                    return AvgCPCV.Value.ToString("C");

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted budget for the order.
        /// </summary>
        public string FormattedBudget
        {
            get
            {
                if (Budget.HasValue)
                    return Budget.Value.ToString("C2");

                return null;
            }
        }

        /// <summary>
        /// Read-only property that holds the 
        /// formatted budget for the performance
        /// details order.
        /// </summary>
        public string FormattedPerformanceDetailBudget
        {
            get
            {
                if (Budget.HasValue)
                    return Budget.Value.ToString("C2");

                return null;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted campaign budget.
        /// </summary>
        public string FormattedCampaignBudget
        {
            get
            {
                if (CampaignBudget.HasValue)
                {
                    return CampaignBudget.Value.ToString("C");
                }

                return null;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted completed views of an order.
        /// </summary>
        public string FormattedCompletedViews
        {
            get
            {
                if (CompletedViews.HasValue)
                    return CompletedViews.Value.ToString("N0");

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted monthly budget.
        /// </summary>
        public string FormattedMonthlyBudget
        {
            get
            {
                if (MonthlyBudget.HasValue)
                {
                    return MonthlyBudget.Value.ToString("C");
                }

                return null;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted end date of the order.
        /// </summary>
        public string FormattedEndDate
        {
            get
            {
                if (EndDate.HasValue)
                    return EndDate.Value.ToString("M/d/yy");

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted reach of an order.
        /// </summary>
        public string FormattedReach
        {
            get
            {
                if (Reach.HasValue)
                    return Reach.Value.ToString("N0") + "%";

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted remaining budget for the order.
        /// </summary>
        public string FormattedRemainingBudget
        {
            get
            {
                if (RemainingBudget.HasValue)
                    return RemainingBudget.Value.ToString("C2");

                else if (Budget.HasValue)
                    return Budget.Value.ToString("C2");

                else
                    return null;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted start date of the order.
        /// </summary>
        public string FormattedStartDate
        {
            get
            {
                if (StartDate.HasValue)
                    return StartDate.Value.ToString("M/d/yy");

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted
        /// total budget for the order.
        /// </summary>
        public string FormattedTotalBudget
        {
            get
            {
                if (TotalBudget.HasValue)
                    return TotalBudget.Value.ToString("C2");

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted view rate of an order.
        /// </summary>
        public string FormattedViewRate
        {
            get
            {
                if (ViewRate.HasValue)
                    return ViewRate.Value.ToString("##0%");

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted view time of an order.
        /// </summary>
        public string FormattedViewTime
        {
            get
            {
                if (ViewTime.HasValue)
                    return string.Format("{0:N0} sec", ViewTime.Value);

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted view time of an order in hours.
        /// </summary>
        public string FormattedViewTimeHours
        {
            get
            {
                if (ViewTimeHours.HasValue)
                    return string.Format("{0:N0} Hours", ViewTimeHours.Value);

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted view time of an order in hours.
        /// </summary>
        public string FormattedViewTimeInHours
        {
            get
            {
                if (ViewTime.HasValue)
                    return string.Format("{0:N0}", ViewTime.Value / 3600);

                return string.Empty;
            }
        }

        /// <summary>
        /// Property that holds the view time of an order in hours.
        /// </summary>
        public long? ViewTimeHours
        {
            get
            {
                if (ViewTime.HasValue)
                    return ViewTime.Value / 60;

                return null;
            }
        }
        #endregion
    }
}
