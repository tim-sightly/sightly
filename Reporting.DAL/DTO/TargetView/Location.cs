﻿using System;
using System.Collections.Generic;

namespace Reporting.DAL.DTO.TargetView
{
    public class Location
    {
        /// <summary>
        /// Property that holds the ID of an account 
        /// that owns the location information.
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Property that holds the ID of 
        /// the cross reference table between 
        /// the budget group and the Ad table.
        /// NOTE: Each location in an order 
        /// now has an independent budget group.
        /// </summary>
        public Guid? BudgetGroupXrefAdId { get; set; }

        /// <summary>
        /// Property that holds the list of
        /// geography IDs under a location.
        /// </summary>
        public List<Guid> GeographyIds { get; set; }

        /// <summary>
        /// Property that holds the geography
        /// names delimited with a separator.
        /// </summary>
        public string GeographyNames { get; set; }

        /// <summary>
        /// Property that holds the ID of
        /// a location.
        /// </summary>
        public Guid LocationId { get; set; }

        /// <summary>
        /// Property that holds the name
        /// of a location.
        /// </summary>
        public string LocationName { get; set; }

        /// <summary>
        /// Property that holds reference code
        /// of a location.
        /// </summary>
        public string LocationRefCode { get; set; }

        /// <summary>
        /// Property that holds the ID that
        /// indicates the order of location.
        /// </summary>
        public int? SequenceId { get; set; }

        /// <summary>
        /// Property that holds the total population
        /// of all the geographies under the location.
        /// </summary>
        public long? TotalPopulation { get; set; }

    }
}