﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class Gender
    {
        /// <summary>
        /// Property that holds the ID of a gender.
        /// </summary>
        public Guid GenderId { get; set; }

        /// <summary>
        /// Property that holds the name of a gender.
        /// </summary>
        public string GenderName { get; set; }

        /// <summary>
        /// Property that holds the population 
        /// percentage of a gender.
        /// </summary>
        public decimal PopulationPercentage { get; set; }
    }
}