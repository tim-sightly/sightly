﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class PerformanceDetailsData
    {
        public string AdvertiserName { get; set; }
        public string CampaignName { get; set; }
        public long? TargetPopulation { get; set; }
        public DateTime EndDate { get; set; }
        public string OrderName { get; set; }
        public DateTime StartDate { get; set; }

    }
}