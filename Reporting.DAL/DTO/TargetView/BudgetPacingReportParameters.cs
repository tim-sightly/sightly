﻿using System.Collections.Generic;

namespace Reporting.DAL.DTO.TargetView
{
    public class BudgetPacingReportParameters
    {
        public string ReportPeriodText { get; set; }
        public decimal PacingRate { get; set; }
        public int AverageDaysLeft { get; set; }
        public decimal TotalBudget { get; set; }
        public double ComputedViewRate { get; set; }
        public long TotalCompletedViews { get; set; }
        public long TotalImpressions { get; set; }
        public List<OrderBudgetPacingSummary> BudgetPacingSummaries { get; set; }
    }
}
