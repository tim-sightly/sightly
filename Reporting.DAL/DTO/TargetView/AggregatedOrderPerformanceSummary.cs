﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class AggregatedOrderPerformanceSummary
    {
        #region Private Methods
        private decimal GetShortValue(decimal input)
        {
            if (input >= 1000000000000)
                return input / 1000000000000;
            else if (input >= 1000000000)
                return input / 1000000000;
            else if (input >= 1000000)
                return input / 1000000;
            else if (input >= 1000)
                return input / 1000;
            else
                return input;
        }

        private long GetShortValue(long input)
        {
            if (input >= 1000000000000)
                return input / 1000000000000;
            else if (input >= 1000000000)
                return input / 1000000000;
            else if (input >= 1000000)
                return input / 1000000;
            else if (input >= 1000)
                return input / 1000;
            else
                return input;
        }

        private string GetShortValueUnit(decimal input)
        {
            if (input >= 1000000000000)
                return "T";
            else if (input >= 1000000000)
                return "B";
            else if (input >= 1000000)
                return "M";
            else if (input >= 1000)
                return "K";
            else
                return string.Empty;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Property that holds the aggregated 
        /// average CPCV of all requested orders.
        /// </summary>
        public decimal AvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the
        /// calculated consumed budget percentage.
        /// </summary>
        public decimal ConsumedBudgetPercentage { get; set; }

        /// <summary>
        /// Property that holds the total budget
        /// of all requested orders.
        /// </summary>
        public decimal TotalBudget { get; set; }

        /// <summary>
        /// Property that holds the total number of
        /// total completed views of all requested orders.
        /// </summary>
        public long TotalCompletedViews { get; set; }

        /// <summary>
        /// Property that holds the total impressions
        /// made for all requested orders.
        /// </summary>
        public long TotalImpressions { get; set; }

        /// <summary>
        /// Property that holds the budget that was
        /// already spent for all requested orders.
        /// </summary>
        public decimal TotalSpend { get; set; }

        /// <summary>
        /// Property that holds the number of total 
        /// view time hours of all requested orders.
        /// </summary>
        public long TotalViewTime { get; set; }

        /// <summary>
        /// Property that holds the average view
        /// rate of all requested orders.
        /// </summary>
        public decimal ViewRate { get; set; }
        #endregion

        #region Read-Only Properties
        /// <summary>
        /// Read-only property that holds the computed
        /// view rate for performance summary.
        /// </summary>
        public double ComputedViewRate
        {
            get
            {
                if (TotalImpressions > 0)
                    return (double)TotalCompletedViews / (double)TotalImpressions;

                return 0;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted aggregated  average CPCV of 
        /// all requested orders.
        /// </summary>
        public string FormattedAvgCPCV
        {
            get
            {
                return this.AvgCPCV.ToString("C");
            }
        }

        /// <summary>
        /// Read-only property that holds the 
        /// formatted calculated consumed budget 
        /// percentage.
        /// </summary>
        public string FormattedConsumedBudgetPercentage
        {
            get
            {
                return ConsumedBudgetPercentage.ToString("##0%");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted short total spend.
        /// </summary>
        public string FormattedShortTotalSpend
        {
            get
            {
                return ShortTotalSpend.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted short total view time.
        /// </summary>
        public string FormattedShortTotalViewTime
        {
            get
            {
                return ShortTotalViewTime.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted short total view time in hours.
        /// </summary>
        public string FormattedShortTotalViewTimeInHours
        {
            get
            
            {
                
                return Math.Round(Convert.ToDecimal(this.TotalViewTime / 3600), 3).ToString();
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted short total completed views.
        /// </summary>
        public string FormattedShortTotalCompletedViews
        {
            get
            {
                return ShortTotalCompletedViews.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted short total impressions.
        /// </summary>
        public string FormattedShortTotalImpressions
        {
            get
            {
                return ShortTotalImpressions.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted total budget of all requested
        /// orders.
        /// </summary>
        public string FormattedTotalBudget
        {
            get
            {
                return this.TotalBudget.ToString("C");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted aggregated total budget.
        /// </summary>
        public string FormattedTotalCompletedViews
        {
            get
            {
                return this.TotalCompletedViews.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted aggregated total impressions.
        /// </summary>
        public string FormattedTotalImpressions
        {
            get
            {
                return this.TotalImpressions.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted aggregated total spend.
        /// </summary>
        public string FormattedTotalSpend
        {
            get
            {
                return this.TotalSpend.ToString("C");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted aggregated total spend.
        /// </summary>
        public string FormattedTotalViewTime
        {
            get
            {
                return this.TotalViewTime.ToString("N0") + " hrs";
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted aggregated total spend.
        /// </summary>
        public string FormattedViewRate
        {
            get
            {
                return (ComputedViewRate * 100).ToString("N0") + "%";
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// short total completed views.
        /// </summary>
        public long ShortTotalCompletedViews
        {
            get
            {
                return GetShortValue(this.TotalCompletedViews);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// short total impressions.
        /// </summary>
        public long ShortTotalImpressions
        {
            get
            {
                return GetShortValue(this.TotalImpressions);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// short total spend.
        /// </summary>
        public decimal ShortTotalSpend
        {
            get
            {
                return Math.Round(GetShortValue(this.TotalSpend));
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// short total view time.
        /// </summary>
        public long ShortTotalViewTime
        {
            get
            {
                return GetShortValue(this.TotalViewTime);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// total completed views unit.
        /// </summary>
        public string TotalCompletedViewsUnit
        {
            get
            {
                return GetShortValueUnit(this.TotalCompletedViews);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// total impressions unit.
        /// </summary>
        public string TotalImpressionsUnit
        {
            get
            {
                return GetShortValueUnit(this.TotalImpressions);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// total spend unit.
        /// </summary>
        public string TotalSpendUnit
        {
            get
            {
                return GetShortValueUnit(this.TotalSpend);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// total view time in hours unit.
        /// </summary>
        public string TotalViewTimeInHoursUnit
        {
            get
            {
                return GetShortValueUnit(this.TotalViewTime / 3600);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// total view time unit.
        /// </summary>
        public string TotalViewTimeUnit
        {
            get
            {
                return GetShortValueUnit(this.TotalViewTime);
            }
        }
        #endregion
    }
}
