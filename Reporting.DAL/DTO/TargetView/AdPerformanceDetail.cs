﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class AdPerformanceDetail
    {
        #region Private Methods
        private string FormatVideoLengthInSeconds(double videoLengthInSeconds)
        {
            var format = videoLengthInSeconds % 1 == 0 ? "N0" : "N1";

            return string.Format(
                "To {0} Seconds",
                videoLengthInSeconds.ToString(format)
            );
        }
        #endregion

        #region Properties
        /// <summary>
        /// Property that holds the ID of the ad.
        /// </summary>
        public Guid AdId { get; set; }

        /// <summary>
        /// Property that holds the audience retention percentage
        /// where the viewer watched 25% of the video ad.
        /// </summary>
        public int AudienceRetention25 { get; set; }

        /// <summary>
        /// Property that holds the audience retention percentage
        /// where the viewer watched 50% of the video ad.
        /// </summary>
        public int AudienceRetention50 { get; set; }

        /// <summary>
        /// Property that holds the audience retention percentage
        /// where the viewer watched 75% of the video ad.
        /// </summary>
        public int AudienceRetention75 { get; set; }

        /// <summary>
        /// Property that holds the audience retention percentage
        /// where the viewer watched 100% of the video ad.
        /// </summary>
        public int AudienceRetention100 { get; set; }

        /// <summary>
        /// Property that holds the average CPCV
        /// for an ad performance detail.
        /// </summary>
        public decimal? AvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the number of clicks
        /// for an ad performance detail.
        /// </summary>
        public long? Clicks { get; set; }

        /// <summary>
        /// Property that holds the number of completed
        /// ad views for an ad performance detail.
        /// </summary>
        public long? CompletedViews { get; set; }

        /// <summary>
        /// Property that holds the ID of the specific
        /// order where the ad performance detail fall
        /// under.
        /// </summary>
        public Guid? OrderId { get; set; }

        /// <summary>
        /// Property that holds the impressions
        /// for an ad performance detail.
        /// </summary>
        public long? Impressions { get; set; }

        /// <summary>
        /// Property that holds the name of the video ad.
        /// </summary>
        public string VideoAdName { get; set; }

        /// <summary>
        /// Property that holds the length of the video asset in seconds.
        /// </summary>
        public int VideoLengthInSeconds { get; set; }

        /// <summary>
        /// Property that holds the view rate
        /// for an ad performance detail.
        /// </summary>
        public double? ViewRate { get; set; }
        #endregion

        #region Read-only Properties
        /// <summary>
        /// Read-only property that holds the
        /// calculated click through rate of an
        /// ad performance detail.
        /// </summary>
        public decimal ClickThroughRate
        {
            get
            {
                if (Impressions > 0)
                    return (decimal)Clicks / (decimal)Impressions;

                return 0;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// calculated view rate of an ad
        /// performance detail.
        /// </summary>
        public double? ComputedViewRate
        {
            get
            {
                if (Impressions > 0)
                    return (double)CompletedViews / (double)Impressions;

                return null;
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted
        /// average CPCV for an ad performance detail.
        /// </summary>
        public string FormattedAvgCPCV
        {
            get
            {
                if (AvgCPCV.HasValue)
                    return AvgCPCV.Value.ToString("C");

                return "-";
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted
        /// number of clicks for an ad performance detail.
        /// </summary>
        public string FormattedClicks
        {
            get
            {
                if (Clicks.HasValue)
                    return Clicks.Value.ToString("N0");

                return "-";
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted number
        /// of click through rate for an ad performance
        /// detail.
        /// </summary>
        public string FormattedClickThroughRate
        {
            get
            {
                if (ClickThroughRate > 0)
                    return (ClickThroughRate * 100).ToString("N2") + "%";

                return "-";
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted
        /// completed views for an ad performance detail.
        /// </summary>
        public string FormattedCompletedViews
        {
            get
            {
                if (CompletedViews.HasValue)
                    return CompletedViews.Value.ToString("N0");

                return "-";
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted
        /// impressions for an ad performance detail.
        /// </summary>
        public string FormattedImpressions
        {
            get
            {
                if (Impressions.HasValue)
                    return Impressions.Value.ToString("N0");

                return "-";
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted length
        /// of the 25% of the video asset in seconds.
        /// </summary>
        public string FormattedVideoLengthInSeconds25
        {
            get
            {
                return FormatVideoLengthInSeconds(VideoLengthInSeconds25);
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted length
        /// of the 50% of the video asset in seconds.
        /// </summary>
        public string FormattedVideoLengthInSeconds50
        {
            get
            {
                return FormatVideoLengthInSeconds(VideoLengthInSeconds50);
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted length
        /// of the 75% of the video asset in seconds.
        /// </summary>
        public string FormattedVideoLengthInSeconds75
        {
            get
            {
                return FormatVideoLengthInSeconds(VideoLengthInSeconds75);
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted length
        /// of the 100% of the video asset in seconds.
        /// </summary>
        public string FormattedVideoLengthInSeconds100
        {
            get
            {
                return FormatVideoLengthInSeconds(VideoLengthInSeconds100);
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted
        /// view rate for an ad performance detail.
        /// </summary>
        public string FormattedViewRate
        {
            get
            {
                if (ComputedViewRate.HasValue)
                    return (ComputedViewRate.Value * 100).ToString("N2") + "%";

                return "-";
            }
        }

        /// <summary>
        /// Read-only property that holds the length
        /// of the 25% of the video asset in seconds.
        /// </summary>
        public double VideoLengthInSeconds25
        {
            get
            {
                return VideoLengthInSeconds * 0.25;
            }
        }

        /// <summary>
        /// Read-only property that holds the length
        /// of the 50% of the video asset in seconds.
        /// </summary>
        public double VideoLengthInSeconds50
        {
            get
            {
                return VideoLengthInSeconds * 0.5;
            }
        }

        /// <summary>
        /// Read-only property that holds the length
        /// of the 75% of the video asset in seconds.
        /// </summary>
        public double VideoLengthInSeconds75
        {
            get
            {
                return VideoLengthInSeconds * 0.75;
            }
        }

        /// <summary>
        /// Read-only property that holds the length
        /// of the 100% of the video asset in seconds.
        /// </summary>
        public double VideoLengthInSeconds100
        {
            get
            {
                return VideoLengthInSeconds;
            }
        }
        #endregion
    }
}
