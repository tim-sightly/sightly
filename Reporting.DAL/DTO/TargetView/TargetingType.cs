﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class TargetingType
    {
        /// <summary>
        /// Property that holds the ID of an
        /// audience targeting type.
        /// </summary>
        public Guid? TargetingTypeId { get; set; }

        /// <summary>
        /// Property that holds the name of an
        /// audience targeting type.
        /// </summary>
        public string TargetingTypeName { get; set; }

        /// <summary>
        /// Property that holds the CPCV multiplier
        /// associated with the targeting type.
        /// </summary>
        public decimal? CpcvMultiplier { get; set; }
    }
}