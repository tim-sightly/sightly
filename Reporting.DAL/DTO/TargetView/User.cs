﻿using System;
using System.Collections.Generic;

namespace Reporting.DAL.DTO.TargetView
{
    public class User
    {
        #region Properties
        /// <summary>
        /// Property that holds the password
        /// of the user record.
        /// </summary>
        public string Access { get; set; }

        /// <summary>
        /// Property that holds the names of account 
        /// short codes and advertisers that the user 
        /// have access to.
        /// </summary>
        public List<string> AccessNames { get; set; }

        /// <summary>
        /// Property that holds the ID of
        /// the account of the user record.
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Property that holds the IDs of the 
        /// accounts that the user has access to.
        /// </summary>
        public List<Guid> AccountIds { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// account where the user belongs.
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// Property that holds the
        /// account user code.
        /// </summary>
        public int AccountUserCode { get; set; }

        /// <summary>
        /// Property that holds the IDs of the 
        /// advertisers that the user has access to.
        /// </summary>
        public List<Guid> AdvertiserIds { get; set; }

        /// <summary>
        /// Property that holds the value for
        /// identifying the availability of
        /// approver workflow of the user.
        /// </summary>
        public bool ApproverWorkflow { get; set; }

        /// <summary>
        /// Property that holds the combined access
        /// names that are under the user.
        /// </summary>
        public string CombinedAccessNames { get; set; }

        /// <summary>
        /// Property that indicates whether the
        /// user is deleted.
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Property that holds the email
        /// of the user record.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Property that holds the first name
        /// of the user record.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Property that indicates whether the
        /// user is forced to reset his password.
        /// </summary>
        public bool ForceToReset { get; set; }

        /// <summary>
        /// Property that holds the last name
        /// of the user record.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Property that holds the mobile number
        /// of the user record.
        /// </summary>
        public string Mobile { get; set; }

        /// <summary>
        /// Property that holds the user's 
        /// profile photo's file extension.
        /// </summary>
        public string ProfilePhotoFileExtension { get; set; }

        /// <summary>
        /// Property that holds the time when
        /// the user's profile photo was uploaded.
        /// </summary>
        public DateTime? ProfilePhotoUploadDatetime { get; set; }

        /// <summary>
        /// Property that holds the 
        /// role title of the user record.
        /// </summary>
        public string Role { get; set; }

        /// <summary>
        /// Property that holds the ID of
        /// the role of the user record.
        /// </summary>
        public Guid RoleId { get; set; }

        /// <summary>
        /// Property that holds the ID of 
        /// the user record.
        /// </summary>
        public Guid UserId { get; set; }
        #endregion

        #region Read-only Properties
        /// <summary>
        /// Read-only property that returns a list of
        /// access names in comma-separated format.
        /// </summary>
        public string CommaSeparatedAccessNames
        {
            get
            {
                if (this.AccessNames != null)
                {
                    return String.Join(", ", this.AccessNames);
                }
                else
                {
                    return String.Empty;
                }
            }
        }

        /// <summary>
        /// Read-only property that returns a list of
        /// access names.
        /// </summary>
        public string FormattedCombinedAccessNames
        {
            get
            {
                if (CombinedAccessNames != null)
                    return CombinedAccessNames;

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only properties that returns the 
        /// formatted profile photo upload date time.
        /// </summary>
        public string FormattedProfilePhotoUploadDatetime
        {
            get
            {
                if (!ProfilePhotoUploadDatetime.HasValue)
                    return string.Empty;

                return this.ProfilePhotoUploadDatetime.Value.ToString("MMddyyyyHHmmss");
            }
        }

        /// <summary>
        /// Property that holds the full name
        /// of the user record.
        /// </summary>
        public string Name
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }
        #endregion
    }
}