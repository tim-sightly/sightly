﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class DeviceTypePerformanceDetail
    {
        #region Properties
        /// <summary>
        /// Property that holds the average CPCV
        /// for a device type performance detail.
        /// </summary>
        public decimal? AvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the average CPV of
        /// the device performance detail.
        /// </summary>
        public decimal? AvgCPV { get; set; }

        /// <summary>
        /// Property that holds the number of clicks
        /// for a device type performance detail.
        /// </summary>
        public long? Clicks { get; set; }

        /// <summary>
        /// Property that holds the color that will be used
        /// for charting for device type performance detail.
        /// </summary>
        public string Color { get; set; }

        /// <summary>
        /// Property that holds the number of completed
        /// ad views for a device type performance detail.
        /// </summary>
        public long? CompletedViews { get; set; }

        /// <summary>
        /// Property that holds the name of the device type.
        /// </summary>
        public string DeviceTypeName { get; set; }

        /// <summary>
        /// Property that holds the impressions
        /// for a device type performance detail.
        /// </summary>
        public long? Impressions { get; set; }

        /// <summary>
        /// Property that holds the ID of the order
        /// where the device stats fall under.
        /// </summary>
        public Guid? OrderId { get; set; }

        /// <summary>
        /// Property that holds the view rate
        /// for a device type performance detail.
        /// </summary>
        public double? ViewRate { get; set; }
        #endregion

        #region Read-only Properties
        /// <summary>
        /// Read-only property that holds the
        /// calculated click through rate of a device
        /// type performance detail.
        /// </summary>
        public decimal ClickThroughRate
        {
            get
            {
                if (Impressions > 0)
                    return (decimal)Clicks / (decimal)Impressions;

                return 0;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// calculated view rate of an device
        /// type performance detail.
        /// </summary>
        public double? ComputedViewRate
        {
            get
            {
                if (Impressions > 0)
                    return (double)CompletedViews / (double)Impressions;

                return null;
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted
        /// average CPCV for a device type performance detail.
        /// </summary>
        public string FormattedAvgCPCV
        {
            get
            {
                if (AvgCPCV.HasValue)
                    return AvgCPCV.Value.ToString("C");

                return "-";
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted number
        /// of clicks for a device type performance detail.
        /// </summary>
        public string FormattedClicks
        {
            get
            {
                if (Clicks.HasValue)
                    return Clicks.Value.ToString("N0");

                return "-";
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted number
        /// of click through rate for an gender performance
        /// detail.
        /// </summary>
        public string FormattedClickThroughRate
        {
            get
            {
                if (ClickThroughRate > 0)
                    return (ClickThroughRate * 100).ToString("N2") + "%";

                return "-";
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted
        /// completed views for a device type performance detail.
        /// </summary>
        public string FormattedCompletedViews
        {
            get
            {
                if (CompletedViews.HasValue)
                    return CompletedViews.Value.ToString("N0");

                return "-";
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted
        /// impressions for a device type performance detail.
        /// </summary>
        public string FormattedImpressions
        {
            get
            {
                if (Impressions.HasValue)
                    return Impressions.Value.ToString("N0");

                return "-";
            }
        }

        /// <summary>
        /// Read-only property that holds the formatted
        /// view rate for a device type performance detail.
        /// </summary>
        public string FormattedViewRate
        {
            get
            {
                if (ComputedViewRate.HasValue)
                    return (ComputedViewRate.Value * 100).ToString("N2") + "%";

                return "-";
            }
        }
        #endregion
    }
    
}
