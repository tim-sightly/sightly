﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class OrderBudgetGroup
    {
        /// <summary>
        /// Property that holds the campaign ID of the
        /// budget group on AdWords.
        /// </summary>
        public long? AdWordsCampaignId { get; set; }

        /// <summary>
        /// Property that holds the customer ID of
        /// the order's advertiser on AdWords where
        /// the budget group is associated.
        /// </summary>
        public long? AdWordsCustomerId { get; set; }

        /// <summary>
        /// Property that holds the budget for the
        /// order budget group.
        /// </summary>
        public decimal Budget { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// budget allocation under a budget group.
        /// </summary>
        public string BudgetAllocationName { get; set; }

        /// <summary>
        /// Property that holds the ID of a budget group an order belongs.
        /// </summary>
        public Guid? BudgetGroupId { get; set; }

        /// <summary>
        /// Property that holds the name of a budget group an order belongs.
        /// </summary>
        public string BudgetGroupName { get; set; }

        /// <summary>
        /// Property that holds the minimum budget
        /// for the locations that fall under the 
        /// budget group.
        /// </summary>
        public decimal? MinimumLocationBudget { get; set; }

        /// <summary>
        /// Property that holds the ID of an order under the budget group.
        /// </summary>
        public Guid? OrderId { get; set; }

        /// <summary>
        /// Property that holds the ID of the order budget group record.
        /// </summary>
        public Guid? OrderXrefBudgetGroupId { get; set; }

        /// <summary>
        /// Property that holds the ID of the order budget
        /// group for the parent order.
        /// </summary>
        public Guid? OrderXrefBudgetGroupId2 { get; set; }

        /// <summary>
        /// Property that determines if roll budget 
        /// between locations is active.
        /// </summary>
        public bool RollBudgetBetweenLocations { get; set; }
    }
}