﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class Objective
    {
        /// <summary>
        /// Property that holds the ID of 
        /// the cross reference between 
        /// budget group and objective.
        /// </summary>
        public Guid? BudgetGroupXrefObjectiveId { get; set; }

        /// <summary>
        /// Property that holds the ID 
        /// of the category of the objective.
        /// </summary>
        public Guid ObjectiveCategoryId { get; set; }

        /// <summary>
        /// Property that holds the name 
        /// of the category of the objective.
        /// </summary>
        public string ObjectiveCategoryName { get; set; }

        /// <summary>
        /// Property that holds 
        /// the ID of the objective.
        /// </summary>
        public Guid ObjectiveId { get; set; }

        /// <summary>
        /// Property that holds the 
        /// name of the objective.
        /// </summary>
        public string ObjectiveName { get; set; }

        /// <summary>
        /// Property that determines if 
        /// an objective is a prime objective 
        /// of a budget group.
        /// </summary>
        public bool IsPrimeObjective { get; set; }
    }
}