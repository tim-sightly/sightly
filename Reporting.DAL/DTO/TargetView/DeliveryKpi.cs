﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class DeliveryKpi
    {
        /// <summary>
        /// Property that holds the ID of 
        /// the cross reference between 
        /// order and delivery key 
        /// performance indicator.
        /// </summary>
        public Guid OrderXrefDeliveryKpiId { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// Delivery key performance indicator.
        /// </summary>
        public Guid DeliveryKpiId { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// Delivery key performance indicator.
        /// </summary>
        public string DeliveryKpiName { get; set; }

        /// <summary>
        /// Property that indicates if the 
        /// delivery key performance indicator 
        /// is the selected primary with regards 
        /// to the budget group it belongs to.
        /// </summary>
        public bool IsPrimeDeliveryKpi { get; set; }

        /// <summary>
        /// Property that holds the ID of the 
        /// objective category where the key 
        /// performance indicator belongs to.
        /// </summary>
        public Guid ObjectiveCategoryId { get; set; }

        /// <summary>
        /// Property that holds the sequence 
        /// number of the deliver key performance 
        /// indicator with regards to the budget 
        /// group associated to it.
        /// </summary>
        public int SequenceNo { get; set; }
    }
}