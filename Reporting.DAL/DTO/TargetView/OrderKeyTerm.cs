﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class OrderKeyTerm
    {
        /// <summary>
        /// Property that holds the
        /// key term of the order.
        /// </summary>
        public string KeyTerm { get; set; }

        /// <summary>
        /// Property that holds the
        /// ID of the key term.
        /// </summary>
        public Guid KeyTermId { get; set; }
    }
}