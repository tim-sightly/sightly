﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class AggregatedCampaignPerformanceSummary
    {
        #region Private Methods
        private decimal GetShortValue(decimal input)
        {
            if (input >= 1000000000000)
                return input / 1000000000000;
            else if (input >= 1000000000)
                return input / 1000000000;
            else if (input >= 1000000)
                return input / 1000000;
            else if (input >= 1000)
                return input / 1000;
            else
                return input;
        }

        private long GetShortValue(long input)
        {
            if (input >= 1000000000000)
                return input / 1000000000000;
            else if (input >= 1000000000)
                return input / 1000000000;
            else if (input >= 1000000)
                return input / 1000000;
            else if (input >= 1000)
                return input / 1000;
            else
                return input;
        }

        private string GetShortValueUnit(decimal input)
        {
            if (input >= 1000000000000)
                return "T";
            else if (input >= 1000000000)
                return "B";
            else if (input >= 1000000)
                return "M";
            else if (input >= 1000)
                return "K";
            else
                return string.Empty;
        }
        #endregion

        #region Properties
        /// <summary>
        /// Property that holds the aggregated 
        /// average CPCV of the target campaign.
        /// </summary>
        public decimal AvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the aggregated 
        /// average CPV of the target campaign.
        /// </summary>
        public decimal AvgCPV { get; set; }

        /// <summary>
        /// Property that holds the aggregated 
        /// average CTR of the target campaign.
        /// </summary>
        public decimal AvgCTR { get; set; }

        /// <summary>
        /// Property that holds the average
        /// reach of the target campaign.
        /// </summary>
        public decimal AvgReach { get; set; }

        /// <summary>
        /// Property that holds the number of clicks
        /// of the target campaign.
        /// </summary>
        public long Clicks { get; set; }

        /// <summary>
        /// Property that holds the latest order end
        /// date in the campaign.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Property that determines whether the
        /// campaign contains multiple orders.
        /// </summary>
        public bool HasMultipleOrders { get; set; }

        /// <summary>
        /// Property that holds the earliest order start
        /// date in the campaign.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// Property that holds the total budget
        /// of the targeted campaign.
        /// </summary>
        public decimal TotalBudget { get; set; }

        /// <summary>
        /// Property that holds the total number of
        /// total completed views of the targeted
        /// campaign.
        /// </summary>
        public long TotalCompletedViews { get; set; }

        /// <summary>
        /// Property that holds the total impressions
        /// of the targeted campaign.
        /// </summary>
        public long TotalImpressions { get; set; }

        /// <summary>
        /// Property that holds the budget that was
        /// already spent for the targeted campaign.
        /// </summary>
        public decimal TotalSpend { get; set; }

        /// <summary>
        /// Property that holds the number of total 
        /// view time hours for the targeted campaign.
        /// </summary>
        public long TotalViewTime { get; set; }

        /// <summary>
        /// Property that holds the average view
        /// rate for the targeted campaign.
        /// </summary>
        public double ViewRate { get; set; }

        public bool DisplayGrp { get; set; }

        #endregion

        #region Read-Only Properties
        /// <summary>
        /// Read-only property that holds the
        /// clicks unit.
        /// </summary>
        public string ClicksUnit
        {
            get
            {
                return GetShortValueUnit(this.Clicks);
            }
        }

        /// <summary>
        /// Read-only property that holds the 
        /// calculated click through rate of an order.
        /// </summary>
        public decimal ClickThroughRate
        {
            get
            {
                if (TotalImpressions > 0)
                    return ((decimal)Clicks / (decimal)TotalImpressions);

                return 0;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// calculated view rate.
        /// </summary>
        public double ComputedViewRate
        {
            get
            {
                if (TotalImpressions > 0)
                    return (double)TotalCompletedViews / (double)TotalImpressions;

                return 0;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted aggregated average CPCV of 
        /// the targeted campaign.
        /// </summary>
        public string FormattedAvgCPCV
        {
            get
            {
                return this.AvgCPCV.ToString("N2");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted average reach of the 
        /// targeted campaign.
        /// </summary>
        public string FormattedAvgReach
        {
            get
            {
                return this.AvgReach.ToString("N0") + "%";
            }
        }

        /// <summary>
        /// Property that holds the formatted campaign
        /// time span (earliest order start date - latest 
        /// order end date).
        /// </summary>
        public string FormattedCampaignDates
        {
            get
            {
                return this.StartDate.ToString("MM/dd/yyyy") + " - " + EndDate.ToString("MM/dd/yyyy");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted number of clicks for an order.
        /// </summary>
        public string FormattedClicks
        {
            get
            {
                return this.Clicks.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted click through rate for an
        /// order.
        /// </summary>
        public string FormattedClickThroughRate
        {
            get
            {
                return (ClickThroughRate * 100).ToString("N2") + "%";
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted short total spend.
        /// </summary>
        public string FormattedShortTotalSpend
        {
            get
            {
                return ShortTotalSpend.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted short total view time.
        /// </summary>
        public string FormattedShortTotalViewTime
        {
            get
            {
                return (this.TotalViewTime / 3600).ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted short total completed views.
        /// </summary>
        public string FormattedShortTotalCompletedViews
        {
            get
            {
                return ShortTotalCompletedViews.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted short total impressions.
        /// </summary>
        public string FormattedShortTotalImpressions
        {
            get
            {
                return ShortTotalImpressions.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted total budget of the targeted
        /// campaign.
        /// </summary>
        public string FormattedTotalBudget
        {
            get
            {
                return this.TotalBudget.ToString("C2");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted aggregated total budget.
        /// </summary>
        public string FormattedTotalCompletedViews
        {
            get
            {
                return this.TotalCompletedViews.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted aggregated total impressions.
        /// </summary>
        public string FormattedTotalImpressions
        {
            get
            {
                return this.TotalImpressions.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted aggregated total spend.
        /// </summary>
        public string FormattedTotalSpend
        {
            get
            {
                return this.TotalSpend.ToString("C2");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted aggregated total spend.
        /// </summary>
        public string FormattedTotalViewTime
        {
            get
            {
                return this.TotalViewTime.ToString("N0") + " hrs";
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted aggregated total spend.
        /// </summary>
        public string FormattedViewRate
        {
            get
            {
                if (ComputedViewRate > 0)
                    return (ComputedViewRate * 100).ToString("N0") + "%";

                return "0";
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// short total completed views.
        /// </summary>
        public long ShortTotalCompletedViews
        {
            get
            {
                return GetShortValue(this.TotalCompletedViews);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// short total impressions.
        /// </summary>
        public long ShortTotalImpressions
        {
            get
            {
                return GetShortValue(this.TotalImpressions);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// short total spend.
        /// </summary>
        public decimal ShortTotalSpend
        {
            get
            {
                return GetShortValue(this.TotalSpend);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// short total view time.
        /// </summary>
        public long ShortTotalViewTime
        {
            get
            {
                return GetShortValue(this.TotalViewTime);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// total completed views unit.
        /// </summary>
        public string TotalCompletedViewsUnit
        {
            get
            {
                return GetShortValueUnit(this.TotalCompletedViews);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// total impressions unit.
        /// </summary>
        public string TotalImpressionsUnit
        {
            get
            {
                return GetShortValueUnit(this.TotalImpressions);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// total spend unit.
        /// </summary>
        public string TotalSpendUnit
        {
            get
            {
                return GetShortValueUnit(this.TotalSpend);
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// total view time unit.
        /// </summary>
        public string TotalViewTimeUnit
        {
            get
            {
                return GetShortValueUnit(this.TotalViewTime / 3600);
            }
        }

        public long Population { get; set; }

        #endregion
    }
}
