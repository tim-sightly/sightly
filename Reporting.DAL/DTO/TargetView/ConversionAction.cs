﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class ConversionAction
    {
        /// <summary>
        /// Property that holds the ID of the account where
        /// the conversion action belongs to.
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Property that holds the ID of the 
        /// advertiser that is associated to the
        /// conversion action.
        /// </summary>
        public Guid AdvertiserId { get; set; }

        /// <summary>
        /// Property that holds the name of the 
        /// advertiser that is associated to the
        /// conversion action.
        /// </summary>
        public string AdvertiserName { get; set; }

        /// <summary>
        /// Property that holds the information if
        /// the conversion action is available for all
        /// advertisers.
        /// </summary>
        public bool AvailableForAllAdvertisers { get; set; }

        /// <summary>
        /// Property that holds the status of
        /// conversion tracker on AdWords.
        /// </summary>
        public string AdWordsConversionTrackerStatus { get; set; }

        /// <summary>
        /// Property that holds the AdWords customer ID of 
        /// the advertiser where the conversion action belongs.
        /// </summary>
        public long AdWordsCustomerId { get; set; }

        /// <summary>
        /// Property that holds the ID of the conversion
        /// action.
        /// </summary>
        public Guid ConversionActionId { get; set; }

        /// <summary>
        /// Property that holds the name of the conversion
        /// action.
        /// </summary>
        public string ConversionActionName { get; set; }

        /// <summary>
        /// Property that holds the actual tag
        /// of the conversion action from AdWords.
        /// </summary>
        public string ConversionActionTag { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// tracking status of the conversion action.
        /// </summary>
        public Guid ConversionActionTrackingStatusId { get; set; }

        /// <summary>
        /// Property that holds the name of the 
        /// tracking status of the conversion action.
        /// </summary>
        public string ConversionActionTrackingStatusName { get; set; }

        /// <summary>
        /// Property that holds the ID of the conversion
        /// count.
        /// </summary>
        public Guid ConversionCountId { get; set; }

        /// <summary>
        /// Property that holds the name of the 
        /// conversion count.
        /// </summary>
        public string ConversionCountName { get; set; }

        /// <summary>
        /// Property that holds the ID of the conversion 
        /// tracking type.
        /// </summary>
        public Guid ConversionTrackingTypeId { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// conversion tracking type.
        /// </summary>
        public string ConversionTrackingTypeName { get; set; }

        /// <summary>
        /// Property that holds the ID of the conversion
        /// type.
        /// </summary>
        public Guid ConversionTypeId { get; set; }

        /// <summary>
        /// Property that holds the the name of the 
        /// conversion type name.
        /// </summary>
        public string ConversionTypeName { get; set; }

        /// <summary>
        /// Property that holds the value of conversion
        /// windows from clicks custom.
        /// </summary>
        public int? ConversionWindowsFromClicksCustom { get; set; }

        /// <summary>
        /// Property that holds the ID of the conversion
        /// windows from clicks.
        /// </summary>
        public Guid ConversionWindowsFromClicksId { get; set; }

        /// <summary>
        /// Property that holds the name of the conversion
        /// windows from clicks.
        /// </summary>
        public string ConversionWindowsFromClicksName { get; set; }

        /// <summary>
        /// Property that holds the conversion windows 
        /// from clicks number of days if it is not set to "Custom".
        /// Also see ConversionWindowsFromClicksActualNumberOfDays.
        /// </summary>
        public int? ConversionWindowsFromClicksNumberOfDays { get; set; }

        /// <summary>
        /// Property that holds the value of conversion
        /// windows from impressions custom.
        /// </summary>
        public int? ConversionWindowsFromImpressionsCustom { get; set; }

        /// <summary>
        /// Property that holds the ID of the conversion
        /// windows from impressions.
        /// </summary>
        public Guid ConversionWindowsFromImpressionsId { get; set; }

        /// <summary>
        /// Property that holds the name of the conversion
        /// windows from impressions.
        /// </summary>
        public string ConversionWindowsFromImpressionsName { get; set; }

        /// <summary>
        /// Property that holds the conversion windows 
        /// from impressions number of days if it is not set to "Custom".
        /// Also see ConversionWindowsFromImpressionsActualNumberOfDays.
        /// </summary>
        public int? ConversionWindowsFromImpressionsNumberOfDays { get; set; }

        /// <summary>
        /// Property that holds the information if the 
        /// conversion action is deleted.
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        /// Property that holds the description of the 
        /// conversion action.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Property that holds the end date of
        /// gathering conversion stats.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// conversion action on the order.
        /// </summary>
        public Guid? OrderConversionActionId { get; set; }

        /// <summary>
        /// Property that holds the number that
        /// indicates the order of conversion actions.
        /// </summary>
        public int? SequenceId { get; set; }

        /// <summary>
        /// Property that holds the start date of
        /// gathering conversion stats.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Property that holds information 
        /// if the conversion action is used 
        /// in an order that was at least submitted.
        /// </summary>
        public bool Submitted { get; set; }

        /// <summary>
        /// Property that holds the latest date 
        /// when the conversion was recorded.
        /// </summary>
        public DateTime? LastConversionRecorded { get; set; }

        /// <summary>
        /// Property that holds the latest date 
        /// when the conversion action was seen.
        /// </summary>
        public DateTime? LastSeenDatetime { get; set; }

        /// <summary>
        /// Property that holds the indicator if
        /// a conversion action is being used on
        /// a live order.
        /// </summary>
        public bool UsedInLiveOrder { get; set; }

        /// <summary>
        /// Property that holds the value of value
        /// per conversion.
        /// </summary>
        public decimal? ValuePerConversion { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// value per conversion.
        /// </summary>
        public Guid ValuePerConversionId { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// value per conversion.
        /// </summary>
        public string ValuePerConversionName { get; set; }

        #region Read-Only Properties
        /// <summary>
        /// Read-only property that holds the actual 
        /// conversion windows from clicks which
        /// is based whether "Custom" is selected.
        /// </summary>
        public int ConversionWindowsFromClicksActualNumberOfDays
        {
            get
            {
                if (ConversionWindowsFromClicksNumberOfDays.HasValue)
                    return ConversionWindowsFromClicksNumberOfDays.Value;

                if (ConversionWindowsFromClicksCustom.HasValue)
                    return ConversionWindowsFromClicksCustom.Value;

                return 0;
            }
        }

        /// <summary>
        /// Read-only property that holds the actual 
        /// conversion windows from impression days 
        /// which is based whether "Custom" is selected.
        /// </summary>
        public int ConversionWindowsFromImpressionsActualNumberOfDays
        {
            get
            {
                if (ConversionWindowsFromImpressionsNumberOfDays.HasValue)
                    return ConversionWindowsFromImpressionsNumberOfDays.Value;

                if (ConversionWindowsFromImpressionsCustom.HasValue)
                    return ConversionWindowsFromImpressionsCustom.Value;

                return 0;
            }
        }

        /// <summary>
        /// Read-only property that holds 
        /// the formatted value per conversion.
        /// </summary>
        public string FormattedValuePerConversion
        {
            get
            {
                if (ValuePerConversion.HasValue)
                    return ValuePerConversion.Value.ToString("C2");

                return String.Empty;
            }
        }
        #endregion
    }
}