﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.DAL.DTO.TargetView
{
    public class Ad
    {
        /// <summary>
        /// Property that holds the ID of the account
        /// associated to an ad.
        /// </summary>
        public Guid AccountId { get; set; }

        /// <summary>
        /// Property that holds the ID of an ad.
        /// </summary>
        public Guid? AdId { get; set; }

        /// <summary>
        /// Property that holds the status of the ad.
        /// </summary>
        public string AdStatus { get; set; }

        /// <summary>
        /// Property that holds the ID of 
        /// the advertiser related to the ad.
        /// </summary>
        public Guid? AdvertiserId { get; set; }

        /// <summary>
        /// Property that holds the name of 
        /// the advertiser related to the ad.
        /// </summary>
        public string AdvertiserName { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// ad in AdWords API.
        /// </summary>
        public long? AdWordsAdId { get; set; }

        /// <summary>
        /// Property that holds the ad name
        /// on AdWords.
        /// </summary>
        public string AdWordsAdName { get; set; }

        /// <summary>
        /// Property that holds the campaign ID of
        /// the order on AdWords.
        /// </summary>
        public long? AdWordsCampaignId { get; set; }

        /// <summary>
        /// Property that holds the campaign name of the
        /// order on AdWords.
        /// </summary>
        public string AdWordsCampaignName { get; set; }

        /// <summary>
        /// Property that holds the customer ID of
        /// the order's advertiser on AdWords.
        /// </summary>
        public long? AdWordsCustomerId { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// banner asset of an ad.
        /// </summary>
        public Guid? BannerAssetId { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// banner asset of an ad.
        /// </summary>
        public string BannerAssetName { get; set; }

        /// <summary>
        /// Property that holds the ID
        /// of the ad's reference to budget group.
        /// </summary>
        public Guid? BudgetGroupAdId { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// BudgetGroup that holds the ad
        /// </summary>
        public Guid BudgetGroupId { get; set; }

        /// <summary>
        /// Property that holds the ID that is a child of the ad.
        /// </summary>
        public Guid? ChildAdId { get; set; }

        /// <summary>
        /// Property that holds the flag if the ad is soft deleted.
        /// </summary>
        public bool? Deleted { get; set; }

        /// <summary>
        /// Property that holds the destination URL of an ad.
        /// </summary>
        public string DestinationUrl { get; set; }

        /// <summary>
        /// Property that holds the display URL of an ad.
        /// </summary>
        public string DisplayUrl { get; set; }

        /// <summary>
        /// Property that holds the locations
        /// associated with the ad.
        /// </summary>
        public List<Location> Locations { get; set; }

        /// <summary>
        /// Property that holds the ID of the order
        /// and ad cross reference record.
        /// </summary>
        public Guid? OrderAdId { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// order on which the ad belongs.
        /// </summary>
        public Guid? OrderId { get; set; }

        /// <summary>
        /// Property that holds the ID of the parent of the ad.
        /// </summary>
        public Guid? ParentAdId { get; set; }

        /// <summary>
        /// Property that holds the flag if
        /// the ad is paused.
        /// </summary>
        public bool? Paused { get; set; }

        /// <summary>
        /// Property that holds the ID that
        /// indicates the order of ads.
        /// </summary>
        public int? SequenceId { get; set; }

        /// <summary>
        /// Property that holds the number that
        /// indicates the order of ads.
        /// </summary>
        public int? SequenceNo { get; set; }

        /// <summary>
        /// Property that holds the flag if an ad
        /// is used by more than one order.
        /// </summary>
        public bool? Shared { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// of the video advertisement.
        /// </summary>
        public string VideoAdName { get; set; }

        /// <summary>
        /// Property that holds the 
        /// file name of the video asset.
        /// </summary>
        public string VideoAssetFileName { get; set; }

        /// <summary>
        /// Property that holds the ID of the
        /// video asset of an ad.
        /// </summary>
        public Guid? VideoAssetId { get; set; }

        /// <summary>
        /// Property that holds the name of the
        /// video asset of an ad.
        /// </summary>
        public string VideoAssetName { get; set; }

        /// <summary>
        /// Property that holds the ID of 
        /// the video asset version of the ad.
        /// </summary>
        public Guid? VideoAssetVersionId { get; set; }

        /// <summary>
        /// Property that holds the name of
        /// the video asset version of the ad.
        /// </summary>
        public string VideoAssetVersionName { get; set; }

        /// <summary>
        /// Property that holds the length of the video asset in seconds.
        /// </summary>
        public int? VideoLengthInSeconds { get; set; }

        /// <summary>
        /// Property that holds the ID of the YouTube video.
        /// </summary>
        public string YouTubeId { get; set; }

        /// <summary>
        /// Property that holds the youtube URL
        /// of the provided video.
        /// </summary>
        public string YouTubeUrl { get; set; }
    }
}
