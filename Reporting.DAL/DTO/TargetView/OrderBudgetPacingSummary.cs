﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.DAL.DTO.TargetView
{
    public class OrderBudgetPacingSummary
    {
        public string OrderName { get; set; }
        public string OrderRefCode { get; set; }
        public string AdvertiserName { get; set; }
        public string CampaignName { get; set; }
        public string CampaignRefCode { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? Budget { get; set; }
        public decimal? AvgDailySpend { get; set; }
        public decimal? ProjectedFinalSpend { get; set; }
        public string PacingStatusName { get; set; }
        public decimal? SpendToDate { get; set; }
        public decimal AverageDailySpend { get; set; }
        public decimal? RemainingBudget { get; set; }
        public decimal? PacingRate { get; set; }
        public int? DaysLeft { get; set; }
        public long? Clicks { get; set; }
        public decimal ClickThroughRate { get; set; }
    }
}
