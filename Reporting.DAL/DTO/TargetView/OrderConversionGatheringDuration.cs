﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class OrderConversionGatheringDuration
    {
        /// <summary>
        /// Property that holds the ID of the 
        /// conversion action related to the order 
        /// conversion gathering duration.
        /// </summary>
        public Guid ConversionActionId { get; set; }

        /// <summary>
        /// Property that holds the end date of 
        /// the order conversion gathering duration.
        /// </summary>
        public DateTime EndDate { get; set; }

        /// <summary>
        /// Property that holds the ID order 
        /// conversion gathering duration.
        /// </summary>
        public Guid OrderConversionGatheringDurationId { get; set; }

        /// <summary>
        /// Property that holds the ID of 
        /// the order related to the order 
        /// conversion gathering duration.
        /// </summary>
        public Guid OrderId { get; set; }

        /// <summary>
        /// Property that holds the start date of 
        /// the order conversion gathering duration.
        /// </summary>
        public DateTime StartDate { get; set; }
    }
}