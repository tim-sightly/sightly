﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class OrderPerformanceSummary
    {
        #region Properties
        /// <summary>
        /// Property that holds the ID of the account
        /// that owns the order.
        /// </summary>
        public Guid? AccountId { get; set; }

        /// <summary>
        /// Property that holds the ID of the advertiser
        /// assigned to the order.
        /// </summary>
        public Guid? AdvertiserId { get; set; }

        /// <summary>
        /// Property that holds the name of the advertiser
        /// assigned to the order.
        /// </summary>
        public string AdvertiserName { get; set; }

        /// <summary>
        /// Property that holds the average CPCV of an order.
        /// </summary>
        public decimal AvgCPCV { get; set; }

        /// <summary>
        /// Property that holds the budget type name 
        /// of the order.
        /// </summary>
        public string BudgetTypeName { get; set; }


        /// <summary>
        /// Property that holds the campaign budget.
        /// </summary>
        public decimal? CampaignBudget { get; set; }

        /// <summary>
        /// Property that holds the ID of the campaign
        /// on which the order falls under.
        /// </summary>
        public Guid? CampaignId { get; set; }

        /// <summary>
        /// Property that holds the name of the campaign 
        /// on which the order falls under.
        /// </summary>
        public string CampaignName { get; set; }

        /// <summary>
        /// Property that holds the campaign ref code
        /// on which the order falls under.
        /// </summary>
        public string CampaignRefCode { get; set; }

        /// <summary>
        /// Property that holds the number of clicks for an order.
        /// </summary>
        public long? Clicks { get; set; }

        /// <summary>
        /// Property that holds the number of
        /// completed ad views under the order.
        /// </summary>
        public long? CompletedViews { get; set; }

        /// <summary>
        /// Property that holds the cost per conversion.
        /// </summary>
        public decimal CostPerConversion { get; set; }

        /// <summary>
        /// Property that holds the number of cross-device
        /// conversion for an order.
        /// </summary>
        public decimal CrossDeviceConversions { get; set; }

        /// <summary>
        /// Property that holds the calculated 
        /// click through rate of the order.
        /// (Note: This property is computed from
        /// the stored procedure for server side
        /// pagination and sorting.)
        /// </summary>
        public decimal Ctr { get; set; }

        /// <summary>
        /// Property that holds the end date of the order.
        /// </summary>
        public DateTime? EndDate { get; set; }

        /// <summary>
        /// Property that holds the impressions
        /// conversion for the order.
        /// </summary>
        public long ImpressionConversions { get; set; }

        /// <summary>
        /// Property that holds the impressions
        /// made for the order.
        /// </summary>
        public long? Impressions { get; set; }

        /// <summary>
        /// Property that holds number of months
        /// the order would run.
        /// </summary>
        public int? MonthCount { get; set; }

        /// <summary>
        /// Property that holds monthly budget 
        /// for the order.
        /// </summary>
        public decimal? MonthlyBudget { get; set; }

        /// <summary>
        /// Property that holds the ID of the order.
        /// </summary>
        public Guid? OrderId { get; set; }

        /// <summary>
        /// Property that holds the name of the order.
        /// </summary>
        public string OrderName { get; set; }

        /// <summary>
        /// Property that holds the reference 
        /// code of the order.
        /// </summary>
        public string OrderRefCode { get; set; }

        /// <summary>
        /// Property that holds the status of
        /// the order.
        /// </summary>
        public string OrderStatus { get; set; }

        /// <summary>
        /// Property that holds the order's reach.
        /// </summary>
        public decimal? Reach { get; set; }

        /// <summary>
        /// Property that holds the number of same-device
        /// conversion for an order.
        /// </summary>
        public decimal SameDeviceConversions { get; set; }

        /// <summary>
        /// Property that holds the budget that was
        /// already spent for the order.
        /// </summary>
        public decimal? Spend { get; set; }

        /// <summary>
        /// Property that holds the start date of 
        /// the order.
        /// </summary>
        public DateTime? StartDate { get; set; }

        /// <summary>
        /// Property that holds the calculated
        /// total budget of the order.
        /// (Note: This property was calculated
        /// from stored procedure.)
        /// </summary>
        public decimal? TotalBudget { get; set; }

        /// <summary>
        /// Property that holds the calculated
        /// total conversion rate of the order.
        /// </summary>
        public decimal TotalConversionRate { get; set; }

        /// <summary>
        /// Property that holds the calculated
        /// total conversions of the order.
        /// </summary>
        public decimal TotalConversions { get; set; }

        /// <summary>
        /// Property that holds the calculated
        /// total conversions value of the order.
        /// </summary>
        public decimal TotalConversionsValue { get; set; }


        public decimal ViewRate { get; set; }

        /// <summary>
        /// Property that holds the number of ad view
        /// time in seconds.
        /// </summary>
        public long? ViewTime { get; set; }

        public long? Grp { get; set; }

        #endregion

        #region Read-only Properties
        /// <summary>
        /// Read-only property that holds the
        /// calculated total budget of the order.
        /// </summary>
        public decimal? Budget
        {
            get
            {
                if (MonthlyBudget.HasValue)
                    return MonthlyBudget.Value
                           * MonthCount.GetValueOrDefault(1);

                return CampaignBudget;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// calculated click through rate of the order.
        /// </summary>
        public decimal ClickThroughRate
        {
            get
            {
                if (Impressions > 0)
                    return (decimal)Clicks / (decimal)Impressions;

                return 0;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted average CPCV of an order.
        /// </summary>
        public string FormattedAvgCPCV
        {
            get
            {
                return this.AvgCPCV.ToString("C");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted budget for the order.
        /// </summary>
        public string FormattedBudget
        {
            get
            {
                if (Budget.HasValue)
                    return Budget.Value.ToString("C2");

                return null;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted number of clicks for an order.
        /// </summary>
        public string FormattedClicks
        {
            get
            {
                if (Clicks.HasValue)
                    return Clicks.Value.ToString("N0");

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the 
        /// formatted click through rate for an order.
        /// </summary>
        public string FormattedClickThroughRate
        {
            get
            {
                return (ClickThroughRate * 100).ToString("N2") + "%";
            }
        }

        public string FormattedViewRate
        {
            get
            {
                if (Impressions > 0)
                    //return Math.Round((double)CompletedViews / (double)Impressions, 2).ToString("P");
                    return ((double)CompletedViews / (double)Impressions).ToString("P");

                return "";
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted completed views for the 
        /// order.
        /// </summary>
        public string FormattedCompletedViews
        {
            get
            {
                if (CompletedViews.HasValue)
                    return CompletedViews.Value.ToString("N0");

                return null;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted cost per conversion for the 
        /// order.
        /// </summary>
        public string FormattedCostPerConversion
        {
            get
            {
                return CostPerConversion.ToString("C0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted cross device conversion for the 
        /// order.
        /// </summary>
        public string FormattedCrossDeviceConversions
        {
            get
            {
                return CrossDeviceConversions.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted click through rate for an order
        /// computed from the stored procedure.
        /// </summary>
        public string FormattedCtr
        {
            get
            {
                return (Ctr * 100).ToString("N2") + "%";
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted end date of the order.
        /// </summary>
        public string FormattedEndDate
        {
            get
            {
                if (EndDate.HasValue)
                    return EndDate.Value.ToString("M/d/yy");

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted impression conversions for the 
        /// order.
        /// </summary>
        public string FormattedImpressionConversions
        {
            get
            {
                return ImpressionConversions.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted impressions of the order.
        /// </summary>
        public string FormattedImpressions
        {
            get
            {
                if (Impressions.HasValue)
                    return Impressions.Value.ToString("N0");

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted reach for the order.
        /// </summary>
        public string FormattedReach
        {
            get
            {
                if (Reach.HasValue)
                    return Reach.Value.ToString("N0") + "%";

                return null;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted same device conversions for the 
        /// order.
        /// </summary>
        public string FormattedSameDeviceConversions
        {
            get
            {
                return SameDeviceConversions.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted spend for the order.
        /// </summary>
        public string FormattedSpend
        {
            get
            {
                if (Spend.HasValue)
                    return Spend.Value.ToString("C2");

                return null;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted start date of the order.
        /// </summary>
        public string FormattedStartDate
        {
            get
            {
                if (StartDate.HasValue)
                    return StartDate.Value.ToString("M/d/yy");

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted budget for the order.
        /// </summary>
        public string FormattedTotalBudget
        {
            get
            {
                if (TotalBudget.HasValue)
                    return TotalBudget.Value.ToString("C2");

                return string.Empty;
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted total conversion rate for the 
        /// order.
        /// </summary>
        public string FormattedTotalConversionRate
        {
            get
            {
                return TotalConversionRate.ToString("N2") + "%";
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted total conversions for the 
        /// order.
        /// </summary>
        public string FormattedTotalConversions
        {
            get
            {
                return TotalConversions.ToString("N0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted total conversions value for the 
        /// order.
        /// </summary>
        public string FormattedTotalConversionsValue
        {
            get
            {
                return TotalConversionsValue.ToString("C0");
            }
        }

        /// <summary>
        /// Read-only property that holds the
        /// formatted view time for the order.
        /// </summary>
        public string FormattedViewTime
        {
            get
            {
                if (ViewTime.HasValue)
                {
                    var viewTimeInHours = ViewTime.Value / 3600;

                    return viewTimeInHours.ToString("N0");
                }

                return null;
            }
        }

        public string FormattedGrp
        {
            get
            {
                return Grp.ToString();
            }
        }
        #endregion
    }
}
