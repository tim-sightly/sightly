﻿using System.Collections.Generic;

namespace Reporting.DAL.DTO.TargetView
{
    public class PerformanceSummaryReportParameters
    {
        public List<OrderPerformanceSummary> OrderPerformanceSummaries { get; set; }
        public AggregatedOrderPerformanceSummary AggregatedOrderPerformanceSummary { get; set; }
        public string ReportPeriodText { get; set; }
        public List<string> IncludedColumns { get; set; } // default to null,
        public bool DisplayGrp { get; set; } //default to false,
        public long? TargetPopulation { get; set; }
        public bool ShowConversionStatColumns { get; set; } //default to false
    }
}
