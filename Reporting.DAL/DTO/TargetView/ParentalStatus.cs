﻿using System;

namespace Reporting.DAL.DTO.TargetView
{
    public class ParentalStatus
    {
        /// <summary>
        /// Property that holds the ID of a parental status.
        /// </summary>
        public Guid ParentalStatusId { get; set; }

        /// <summary>
        /// Property that holds the name of a parental status.
        /// </summary>
        public string ParentalStatusName { get; set; }
    }
}