﻿using System;

namespace Reporting.DAL.DTO
{
    public class BudgetGroupTimedBudgetGroup
    {
        public Guid BudgetGroupTimedBudgetId { get; set; }
        public Guid BudgetGroupId { get; set; }
        public decimal BudgetAmount { get; set; }
        public decimal Margin { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public Guid CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public Guid BudgetGroupXrefPlacementId { get; set; }
    }
}