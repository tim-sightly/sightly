﻿using System.Collections.Generic;
using Reporting.DAL.DTO.TV.extended.audiencegroups;

namespace Reporting.DAL.DTO.TV.extended
{
    public class AudienceExtended
    {
        public List<OrderTargetAgeGroup> AgeRanges { get; set; }
        public string CompetitorUrls { get; set; }
        public List<OrderTargetGenderGroup> Genders { get; set; }
        public List<OrderHouseholdIncomeGroup> HouseholdIncomes { get; set; }
        public string KeyWords { get; set; }
        public string Notes { get; set; }
        public List<OrderParentalStatusGroup> ParentalStatuses { get; set; }
    }
}
