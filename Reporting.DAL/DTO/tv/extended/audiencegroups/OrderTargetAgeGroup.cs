﻿using System;

namespace Reporting.DAL.DTO.TV.extended.audiencegroups
{
    public class OrderTargetAgeGroup
    {
        public Guid OrderId { get; set; }
        public Guid AgeGroupId { get; set; }
        public string AgeGroupName { get; set; }
        public Boolean Deleted { get; set; }
        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModified { get; set; }
    }
}
