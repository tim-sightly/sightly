﻿using System.Collections.Generic;

namespace Reporting.DAL.DTO.TV.extended
{
    public class OrderExtended
    {
        public InfoExtended Info { get; set; }
        public List<Ad> Ads { get; set; }
        public AudienceExtended Audience { get; set; }
        public Geo Geo { get; set; }
    }
}
