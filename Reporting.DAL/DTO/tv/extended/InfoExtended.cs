﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.DAL.DTO.TV.extended
{
    public class InfoExtended: Info
    {
        public string AccountName { get; set; }
        public string AdvertiserName { get; set; }
        public string AdvertiserCategoryName { get; set; }
        public string AdvertiserSubCategoryName { get; set; }

        public string OrderSubCategoryName { get; set; }

        public string CampaignName { get; set; }
        public string ObjectiveName { get; set; }
    }
}
