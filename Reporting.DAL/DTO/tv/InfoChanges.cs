﻿namespace Reporting.DAL.DTO.TV
{
    public class InfoChanges
    {
        public string OrderNameChange { get; set; }
        public string OrderRefCodeChange { get; set; }
        public string TotalBudgetChange { get; set; }
        public string StartDateChange { get; set; }
        public string EndDateChange { get; set; }

        public bool HasChanges => !string.IsNullOrEmpty(OrderRefCodeChange) ||
                                  !string.IsNullOrEmpty(TotalBudgetChange) ||
                                  !string.IsNullOrEmpty(EndDateChange);
    }
}