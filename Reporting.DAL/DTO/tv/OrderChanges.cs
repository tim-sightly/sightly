﻿using System;
using System.Collections.Generic;

namespace Reporting.DAL.DTO.TV
{
    public class OrderChanges
    {
        public string SubmittedBy { get; set; }
        public DateTime SubmittedDate { get; set; }
        public string AccountName { get; set; }
        public string AdvertiserName { get; set; }
        public string OrderName { get; set; }
        public string OrderRefCode { get; set; }
        public DateTime ChangeDate { get; set; }

        public InfoChanges InfoChanges { get; set; }
        public List<AdChanges> AdChanges { get; set; }
        public AudienceChanges AudienceChanges { get; set; }
        public GeoChanges GeoChanges{ get; set; }

        public bool HasAdChanges => AdChanges.Count > 0;

        public OrderChanges()
        {
            InfoChanges = new InfoChanges();
            AdChanges = new List<AdChanges>();
            AudienceChanges = new AudienceChanges();
            GeoChanges = new GeoChanges();
        }
    }
}