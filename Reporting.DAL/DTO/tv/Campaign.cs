﻿
using System;

namespace Reporting.DAL.DTO.TV
{
    public class Campaign
    {
        public  Guid CampaignId { get; set; }
        public string CampaignName { get; set; }
        public Guid AdvertiserId { get; set; }
        public Guid AccountId { get; set; }
    }
}
