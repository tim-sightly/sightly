﻿namespace Reporting.DAL.DTO.TV
{
    public class GeoChanges
    {
        public string ChangedGeos { get; set; }

        public bool HasChanges => !string.IsNullOrEmpty(ChangedGeos);
    }
}