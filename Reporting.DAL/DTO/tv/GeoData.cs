﻿using System;

namespace Reporting.DAL.DTO.TV
{
    public class GeoData
    {
        public string DmaName { get; set; }
        public Guid Id { get; set; }
        public long OriginalPopulation { get; set; }
    }
}