﻿namespace Reporting.DAL.DTO.TV
{
    public class AdChanges
    {
        public string AdPausedChange { get; set; }
        public string DisplayUrlChange { get; set; }
        public string DestinationUrlChange { get; set; }
        public string CompanionBannerChange { get; set; }
        public string NewAdChange { get; set; }

        public bool IsChangeExisting => AdPausedChange.Length > 0 ||
                                        DisplayUrlChange.Length > 0 ||
                                        DestinationUrlChange.Length > 0 ||
                                        CompanionBannerChange.Length > 0 ||
                                        NewAdChange.Length > 0;
    }
}