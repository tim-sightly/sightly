﻿namespace Reporting.DAL.DTO
{
    public class CampaignToDate : AStatRow
    {
        public decimal Ecpm =>  TotalSpend / Impressions * 1000;
    }
}
