﻿using System;

namespace Reporting.DAL.DTO
{
    public class Performance : AStatRow
    {
        public decimal Ecpm => Views == 0 ? 0 : (TotalSpend * 1000) / Impressions;
        public DateTime StartWeekDate { get; set; }
        public DateTime EndWeekDate { get; set; }
    }
}
