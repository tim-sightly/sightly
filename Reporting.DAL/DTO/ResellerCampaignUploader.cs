﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.DAL.DTO
{
    public class ResellerCampaignUploader
    {
        public string TargetviewCampaignName { get; set; }
        public string TargetviewOrderName { get; set; }
        public string BudgetGroupName { get; set; }
        public Guid BudgetGroupId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal CampaignBudget { get; set; }
        public decimal SightlyMargin { get; set; }
    }
}
