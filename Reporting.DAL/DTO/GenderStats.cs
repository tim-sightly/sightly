﻿namespace Reporting.DAL.DTO
{
    public class GenderStats : AStatRow
    {
        public string GenderName { get; set; }        
    }
}
