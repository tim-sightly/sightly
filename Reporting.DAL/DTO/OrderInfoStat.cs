﻿using System;

namespace Reporting.DAL.DTO
{
    public class OrderInfoStat
    {
        public Guid OrderId { get; set; }
        public string OrderName { get; set; }
        public string Status { get; set; }
        public string OrderRefCode { get; set; }
        public Guid AdvertiserId { get; set; }
        public string AdvertiserName { get; set; }
        public Guid CampaignId { get; set; }
        public string CampaignName { get; set; }
        public DateTime OrderStartDate { get; set; }
        public DateTime OrderEndDate { get; set; }
        public decimal OrderBudget { get; set; }
        public DateTime AvailableStartDate { get; set; }
        public DateTime AvailableEndDate { get; set; }
        public int AvailableDays { get; set; }
        public int StatDays { get; set; }
        public decimal Spend { get; set; }
        public long Impressions { get; set; }
        public long CompletedViews { get; set; }
        public decimal ViewRate { get; set; }
        public long ViewTime { get; set; }
        public decimal CPV { get; set; }
        public decimal Reach { get; set; }
        public long Clicks { get; set; }
        public decimal CTR { get; set; }


    }
}