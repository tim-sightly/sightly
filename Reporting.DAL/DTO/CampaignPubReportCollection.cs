﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.DAL.DTO
{
    public class CampaignPubReportCollection
    {
        public List<CampaignPubReport> CampaignPubReports { get; set; }

        public CampaignPubReportCollection()
        {
            CampaignPubReports = new List<CampaignPubReport>();
        }
    }
}
