﻿using System.Collections.Generic;

namespace Reporting.DAL.DTO
{
    public class OrderPlacementReport
    {
        public string OrderName { get; set; }
        public List<MediaAgencyOrderPlacement> MediaAgencyOrderPlacements { get; set; }

        public OrderPlacementReport()
        {
            MediaAgencyOrderPlacements = new List<MediaAgencyOrderPlacement>();
        }
    }
}