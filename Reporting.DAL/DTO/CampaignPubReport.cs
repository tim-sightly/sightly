﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.DAL.DTO
{
    public class CampaignPubReport
    {
        public long CID { get; set; }
        public string CampaignName { get; set; }
        public string PlacementName { get; set; }
        public string PlacementID { get; set; }
        public DateTime StatDate { get; set; }
        public long Impressions { get; set; }
        public long Clicks { get; set; }
        public long VideoCompletion25 { get; set; }
        public long VideoCompletion50 { get; set; }
        public long VideoCompletion75 { get; set; }
        public long VideoCompletion100 { get; set; }
        public long Views { get; set; }
        public decimal Cost { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public Guid BudgetGroupTimedBudgetId { get; set; }
    }
}
