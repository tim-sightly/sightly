﻿namespace Reporting.DAL.DTO
{
    public class DeviceStats : AStatRow
    {
        public string Device { get; set; }
    }     
}
