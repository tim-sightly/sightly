﻿using System;

namespace Reporting.DAL.DTO
{
    public class CampaignBudgetReport
    {
        public string AdwordsCampaignName { get; set; }
        public DateTime StatDate { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public long Impressions { get; set; }
        public long Views { get; set; }
        public decimal ViewRate { get; set; }
        public decimal CPV { get; set; }
        public long Clicks { get; set; }
        public decimal CTR { get; set; }
        public decimal Spend { get; set; }
        public decimal VideoCompletion25 { get; set; }
        public decimal VideoCompletion50 { get; set; }
        public decimal VideoCompletion75 { get; set; }
        public decimal VideoCompletion100 { get; set; }

        public decimal CMP => Impressions == 0 ? 0.0M : Spend / Impressions * 1000;
    }
}