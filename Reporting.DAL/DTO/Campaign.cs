﻿using System;

namespace Reporting.DAL.DTO
{
    public class Campaign
    {
        public Guid CampaignId { get; set; }
        public string CampaignName { get; set; }
        public string CampaignRefCode { get; set; }
    }
}
