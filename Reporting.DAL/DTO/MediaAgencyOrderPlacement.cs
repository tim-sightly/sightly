﻿using System;

namespace Reporting.DAL.DTO
{
    public class MediaAgencyOrderPlacement
    {
        public string TargetViewCampaignName { get; set; }
        public string OrderName { get; set; }
        public string AdwordsCID { get; set; }
        public long AdwordsCampaignId { get; set; }
        public string AdwordsCampaignName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal CampaignBudget { get; set; }
        public decimal Margin { get; set; }
        public string PlacementValue { get; set; }
        public string PlacementName { get; set; }
        public Guid? BudgetGroupTimedBudgetId { get; set; }
        public string CampaignManagerEmail { get; set; }
    }
}