﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using Dapper;
using Reporting.DAL.DTO;
using Reporting.DAL.DTO.NewTargetview;
using Reporting.DAL.DTO.TargetView;
using Order = Reporting.DAL.DTO.NewTargetview.Order;

namespace Reporting.DAL
{
    public class ReportStore : AReportStore, IReportStore
    {
        public PerformanceReport GetPerformanceReport(Guid campaignId, IDbConnection connection = null)
        {
	        PerformanceReport performanceReport = new PerformanceReport();
            
            string setStartWeekQuery = @"SET DATEFIRST 1;";

            string campaignNameQuery =
                @"SELECT CampaignName FROM Accounts.Campaign WHERE CampaignId = @CampaignId;";

            string orderStatsQuery = @"SELECT c.CampaignName, 
									Sum(ads.Impressions) as [Impressions],
									Sum(ads.TotalViews) as [Views],
									Sum(ads.Clicks) as [Clicks],
									Sum((ads.Cost/(1-tb.Margin)))/1000000 as [TotalSpend] 					 
									FROM Orders.[Order] o
									INNER JOIN Accounts.Campaign c
									ON o.CampaignId = c.CampaignId
									INNER JOIN Orders.OrderXrefBudgetGroup obg
									ON o.OrderId = obg.OrderId
									INNER JOIN Live.BudgetGroupTimedBudget tb ON obg.BudgetGroupId = tb.BudgetGroupId
									INNER JOIN Orders.BudgetGroupAdWordsInfo bgai
									ON obg.BudgetGroupId = bgai.BudgetGroupId
									INNER JOIN raw.AdDeviceStats ads
									ON bgai.AdWordsCampaignId = ads.CampaignId
									WHERE o.campaignId = @CampaignId
									GROUP BY c.CampaignName;";

            string performanceStatsQuery = @"SELECT c.CampaignName, 
										CAST(dbo.GetStartDatePreviousWeek(GetDate()) AS Date) AS StartWeekDate,
										CAST(dbo.GetEndDatePreviousWeek(GetDate()) AS Date) AS EndWeekDate, 
										Sum(ads.Impressions) as [Impressions],
										Sum(ads.TotalViews) as [Views],
										Sum(ads.Clicks) as [Clicks],
										Sum((ads.Cost/(1-tb.Margin)))/1000000 as [TotalSpend]
										FROM Orders.[Order] o
										INNER JOIN Accounts.Campaign c
										ON o.CampaignId = c.CampaignId
										INNER JOIN Orders.OrderXrefBudgetGroup obg ON o.OrderId = obg.OrderId
										INNER JOIN Live.BudgetGroupTimedBudget tb ON obg.BudgetGroupId = tb.BudgetGroupId
										INNER JOIN Orders.BudgetGroupAdWordsInfo bgai
										ON obg.BudgetGroupId = bgai.BudgetGroupId
										INNER JOIN raw.AdDeviceStats ads
										ON bgai.AdWordsCampaignId = ads.CampaignId
										WHERE o.campaignId = @CampaignId
										AND CAST(ads.StatDate AS Date) >= CAST(dbo.GetStartDatePreviousWeek(GetDate()) AS Date) 
										AND CAST(ads.StatDate AS Date) <= CAST(dbo.GetEndDatePreviousWeek(GetDate()) AS Date)
										GROUP BY c.CampaignName;";

	        string adStatsQuery = @"SELECT  ad.VideoAdName AS [AdName], 
									SUM(ads.Impressions) AS [Impressions],
									SUM(ads.TotalViews) AS [Views],
									CAST(SUM(Ads.TotalViews) As FLOAT)/CAST(SUM(ads.Impressions) As FLOAT) AS [ViewRate],
									SUM(ads.Clicks) AS [Clicks],
									CAST(SUM(ads.Clicks) As FLOAT)/ CAST(Sum(ads.Impressions) AS FLOAT) AS [CTR],
									CASE
										WHEN SUM(ads.Impressions) = 0 THEN 0
										ELSE SUM(ads.Aud25xImpressions)/ SUM(ads.Impressions)
									END AS [Q25],
									CASE
										WHEN SUM(ads.Impressions) = 0 THEN 0
										ELSE SUM(ads.Aud50xImpressions)/ SUM(ads.Impressions)
									END AS [Q50],
									CASE
										WHEN SUM(ads.Impressions) = 0 THEN 0
										ELSE SUM(ads.Aud75xImpressions)/ SUM(ads.Impressions)
									END AS [Q75], 
									CASE
										WHEN SUM(ads.Impressions) = 0 THEN 0
										ELSE SUM(ads.Aud100xImpressions)/ SUM(ads.Impressions)
									END AS [Q100],
									Sum((ads.Cost/(1-tb.Margin)))/1000000 as [TotalSpend],
									CASE 
										WHEN SUM(ads.TotalViews) = 0 THEN 0  
										ELSE SUM((ads.Cost/(1-tb.Margin))/1000000)/SUM(ads.TotalViews) 
									END AS [AverageCpcv]
									FROM Orders.[Order] o
									INNER JOIN Orders.OrderXrefBudgetGroup obg ON o.OrderId = obg.OrderId
									INNER JOIN Live.BudgetGroupTimedBudget tb ON obg.BudgetGroupId = tb.BudgetGroupId
									INNER JOIN Live.BudgetGroupXrefAd bga ON obg.BudgetGroupId = bga.BudgetGroupId
									INNER JOIN Orders.Ad ad ON bga.AdId = ad.AdId
									INNER JOIN Orders.AdAdWordsInfo aai ON ad.AdId = aai.AdId AND obg.BudgetGroupId = aai.BudgetGroupId
									INNER JOIN raw.AdDeviceStats ads ON aai.AdWordsCampaignId = ads.CampaignId AND aai.AdWordsAdId = ads.AdId
									WHERE o.campaignId = @CampaignId
									GROUP BY ad.VideoAdName;";

            string ageStatsQuery = @"SELECT ars.AgeRangeName as [AgeGroupName], 
									SUM(ars.Impressions) AS [Impressions],
									SUM(ars.TotalViews) AS [Views],
									SUM(ars.Clicks) AS [Clicks],
									SUM((ars.Cost/(1-tb.Margin)))/1000000 as [TotalSpend]
									FROM Orders.[Order] o
									INNER JOIN Orders.OrderXrefBudgetGroup obg ON o.OrderId = obg.OrderId
									INNER JOIN Live.BudgetGroupTimedBudget tb ON obg.BudgetGroupId = tb.BudgetGroupId
									INNER JOIN Orders.BudgetGroupAdWordsInfo bgai ON obg.BudgetGroupId = bgai.BudgetGroupId
									INNER JOIN raw.AgeRangeStats ars ON bgai.AdWordsCampaignId = ars.CampaignId
									WHERE o.campaignId = @CampaignId
									GROUP BY ars.AgeRangeName;";

	        string deviceStatsQuery = @"SELECT  ads.Device, 
										SUM(ads.Impressions) AS [Impressions],
										SUM(ads.TotalViews) AS [Views],
										SUM(ads.Clicks) AS [Clicks],
										SUM((ads.Cost/(1-tb.Margin)))/1000000 as [TotalSpend]
										FROM Orders.[Order] o
										INNER JOIN Orders.OrderXrefBudgetGroup obg ON o.OrderId = obg.OrderId
										INNER JOIN Live.BudgetGroupTimedBudget tb ON obg.BudgetGroupId = tb.BudgetGroupId
										INNER JOIN Orders.BudgetGroupAdWordsInfo bgai ON obg.BudgetGroupId = bgai.BudgetGroupId
										INNER JOIN raw.AdDeviceStats ads
										on bgai.AdWordsCampaignId = ads.CampaignId
										WHERE o.campaignId = @CampaignId
										GROUP BY ads.Device;";

            string genderStatsQuery = @"SELECT gs.GenderName, 
										SUM(gs.Impressions) AS [Impressions],
										SUM(gs.TotalViews) AS [Views],
										SUM(gs.Clicks) AS [Clicks],
										SUM((gs.Cost/(1-tb.Margin)))/1000000 as [TotalSpend]
										FROM Orders.[Order] o
										INNER JOIN Orders.OrderXrefBudgetGroup obg ON o.OrderId = obg.OrderId
										INNER JOIN Live.BudgetGroupTimedBudget tb ON obg.BudgetGroupId = tb.BudgetGroupId
										INNER JOIN Orders.BudgetGroupAdWordsInfo bgai ON obg.BudgetGroupId = bgai.BudgetGroupId
										INNER JOIN raw.GenderStats gs ON bgai.AdWordsCampaignId = gs.CampaignId
										WHERE o.campaignId = @CampaignId
										GROUP BY gs.GenderName;";

	        string timedBudgetGroupQuery = @"SELECT
											bgtb.BudgetGroupTimedBudgetId AS [BudgetGroupTimedBudgetId], 
											bgtb.BudgetGroupId AS [BudgetGroupId],
											bgtb.BudgetAmount / (1-bgtb.Margin) AS [BudgetAmount],
											bgtb.Margin AS [Margin],
											bgtb.StartDate AS [StartDate],
											bgtb.EndDate AS [EndDate],
											bgtb.CreatedBy AS [CreatedBy],
											bgtb.Created AS [Created],
											bgtb.BudgetGroupXrefPlacementId AS [BudgetGroupXrefPlacementId]
											FROM Orders.[Order] o
											INNER JOIN Orders.OrderXrefBudgetGroup oxbg ON o.OrderId = oxbg.OrderId
											INNER JOIN Live.BudgetGroup bg ON oxbg.BudgetGroupId = bg.BudgetGroupId
											INNER JOIN Live.BudgetGroupTimedBudget bgtb ON bg.BudgetGroupId = bgtb.BudgetGroupId
											WHERE o.CampaignId = @CampaignId;";
											
	        
            string setEndWeekQuery = @"SET DATEFIRST 7;";

            var sql = $"{setStartWeekQuery}" +
                      $"{campaignNameQuery}" +
                      $"{orderStatsQuery}" +
                      $"{performanceStatsQuery}" +
                      $"{adStatsQuery}" +
                      $"{ageStatsQuery}" +
                      $"{deviceStatsQuery}" +
                      $"{genderStatsQuery}" +
                      $"{timedBudgetGroupQuery}" +
                      $"{setEndWeekQuery}";

            try
            {
	            using (IDbConnection db = connection ?? new SqlConnection(ConnString))
	            {
		            using (var multi = db.QueryMultiple(sql, new {CampaignId = campaignId}))
		            {
			            string campaignName = multi.Read<string>().SingleOrDefault();
			            CampaignToDate campaignToDate = multi.Read<CampaignToDate>().SingleOrDefault();
			            Performance performance = multi.Read<Performance>().SingleOrDefault();
			            List<AdStats> adStats = multi.Read<AdStats>().ToList();
			            List<AgeStats> ageStats = multi.Read<AgeStats>().ToList();
			            List<DeviceStats> deviceStats = multi.Read<DeviceStats>().ToList();
			            List<GenderStats> genderStats = multi.Read<GenderStats>().ToList();
			            List<BudgetGroupTimedBudgetGroup> timedBudgetGroups = multi.Read<BudgetGroupTimedBudgetGroup>().ToList();

			            performanceReport.CampaignName = campaignName;
			            performanceReport.CampaignToDate = campaignToDate;
			            performanceReport.Performance = performance;
			            performanceReport.AdStats = adStats;
			            performanceReport.AgeStats = ageStats;
			            performanceReport.DeviceStats = deviceStats;
			            performanceReport.GenderStats = genderStats;
			            performanceReport.BudgetGroupTimedBudgetGroups = timedBudgetGroups;
		            }

		            List<ActualDailySpend> actualDailySpend = db.Query<ActualDailySpend>("[stat].[GetTotalCampaignSpendToDate]", new {campaignId = campaignId}, commandType: CommandType.StoredProcedure).ToList();
		            performanceReport.ActualDailySpend = actualDailySpend;
		            return performanceReport;
	            }

            }
            catch (Exception ex)
            {
                //TODO: log this!!!
                return null;
            }
        }



        public PerformanceReport GetPerformanceReport(long customerId, IDbConnection connection = null)
        {
            Guid campaignId;
            string getCampaignSql = $"SELECT CampaignId FROM Accounts.CampaignAdWordsInfo WHERE AdWordsCustomerId = {customerId}";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    campaignId =  db.Query<Guid>(getCampaignSql, null, commandTimeout: 120).First();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return GetPerformanceReport(campaignId, null);

        }

        public PerformanceReport GetPerformanceReport(long customerId, DateTime startDate, DateTime endDate,
            IDbConnection connection = null)
        {
            PerformanceReport performanceReport = new PerformanceReport();

            var campaignName = "Execute tvdb.GetCampaignNameFromCustomerAndDates @CustomerId, @StartDate, @EndDate;";
            var orderStats = "Execute tvdb.GetOrderStatsByCustomerAndDates @CustomerId, @StartDate, @EndDate;";
            var adStats = "Execute tvdb.GetAdStatsByCustomerAndDates @CustomerId, @StartDate, @EndDate;";
            var ageStats = "Execute tvdb.GetAgeGroupStatsWithCustomerAndDates @CustomerId, @StartDate, @EndDate;";
            var deviceStats = "Execute tvdb.GetDeviceStatsFromCustomerAndDates @CustomerId, @StartDate, @EndDate;";
            var genderStats = "Execute tvdb.GetGenderStatsFromCustomerAndDates @CustomerId, @StartDate, @EndDate;";
            var bgStats = "Execute tvdb.GetBudgetGroupTimedBudgetsFromCustomerAndDates @CustomerId, @StartDate, @EndDate;";
            var statDates = "Execute tvdb.GetStatsDatesForCustomerAndDateRange @CustomerId, @StartDate, @EndDate;";

            var sql = $"{campaignName}" +
                      $"{orderStats}" +
                      $"{adStats}" +
                      $"{ageStats}" +
                      $"{deviceStats}" +
                      $"{genderStats}" +
                      $"{bgStats}" +
                      $"{statDates}";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    using (var multi = db.QueryMultiple(sql,new {CustomerId = customerId, StartDate = startDate, EndDate = endDate}))
                    {
                        string rCampaignName = multi.Read<string>().SingleOrDefault();
                        CampaignToDate campaignToDate = multi.Read<CampaignToDate>().SingleOrDefault();
                        List<AdStats> rAdStats = multi.Read<AdStats>().ToList();
                        List<AgeStats> rAgeStats = multi.Read<AgeStats>().ToList();
                        List<DeviceStats> rDeviceStats = multi.Read<DeviceStats>().ToList();
                        List<GenderStats> rGenderStats = multi.Read<GenderStats>().ToList();
                        List<BudgetGroupTimedBudgetGroup> timedBudgetGroups = multi.Read<BudgetGroupTimedBudgetGroup>().ToList();
                        OrdersDates orderDates = multi.Read<OrdersDates>().SingleOrDefault();

                        if (orderDates == null)
                        {

                            performanceReport.OrderStartDate = startDate;
                            performanceReport.OrderEndDate = endDate;
                        }
                        else
                        {
                            performanceReport.OrderStartDate = orderDates.StartDate ;
                            performanceReport.OrderEndDate = orderDates.EndDate;
                        }
                        performanceReport.CampaignName = rCampaignName;
                        performanceReport.CampaignToDate = campaignToDate;
                        performanceReport.AdStats = rAdStats;
                        performanceReport.AgeStats = rAgeStats;
                        performanceReport.DeviceStats = rDeviceStats;
                        performanceReport.GenderStats = rGenderStats;
                        performanceReport.BudgetGroupTimedBudgetGroups = timedBudgetGroups;

                    }

                    List<ActualDailySpend> actualDailySpend = db.Query<ActualDailySpend>("[stat].[GetTotalCampaignSpendToDateByCustomer]", new { CustomerId = customerId }, commandType: CommandType.StoredProcedure).ToList();
                    performanceReport.ActualDailySpend = actualDailySpend;
                    return performanceReport;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public List<MediaAgencyCampaignPlacement> GetMediaAgencyCampaignPlacements(long customerId, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE tvdb.MediaAgencyWithPlacementWGuidEmail_Import_Export @CustomerId;";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<MediaAgencyCampaignPlacement>(sql, new {CustomerId = customerId}, commandTimeout: 120).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public List<MediaAgencyOrderPlacement> GetMediaAgencyOrderPlacements(long customerId, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE tvdb.MediaAgencyOrderWithPlacementWGuidEmail_Import_Export @CustomerId;";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<MediaAgencyOrderPlacement>(sql, new { CustomerId = customerId }, commandTimeout: 120).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public List<CampaignPubReport> GetPubReports(long customerId, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE [tvdb].[PubByCustomerId] @CustomerId;";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<CampaignPubReport>(sql, new { CustomerId = customerId }, commandTimeout:120).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public List<CampaignPubReport> GetPubReportsByDateRange(long customerId, DateTime startDate, DateTime endDate, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE [tvdb].[PubByCustomerIdWithDateRange] @CustomerId, @StartDate, @EndDate;";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<CampaignPubReport>(sql, new { CustomerId = customerId, StartDate = startDate, EndDate = endDate }, commandTimeout: 120).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public IEnumerable<CampaignPubReport> GetAggregatedPubReportData(List<long> customers, DateTime startDate, DateTime endDate, bool isSummary, IDbConnection connection = null)
        {
            const string sql = @"[tvdb].[PubAggregateByCustomerIdWithDateRange] @CustomerId, @StartDate, @EndDate, @Summary";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<CampaignPubReport>(
                        sql, 
                        new
                        {
                            CustomerId = string.Join(",", customers.Select(x => x.ToString())),
                            StartDate = startDate,
                            EndDate = endDate,
                            Summary = isSummary
                        }, 
                        commandTimeout: 120
                    ).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public IEnumerable<CampaignPubReport> GetInternalAggregatedPubReportData(List<long> customers, DateTime startDate, DateTime endDate, bool isSummary, IDbConnection connection = null)
        {
            const string sql = @"[tvdb].[PubByMultipleCustomerIdWithDateRange] @CustomerId, @StartDate, @EndDate, @Summary";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<CampaignPubReport>(
                        sql,
                        new
                        {
                            CustomerId = string.Join(",", customers.Select(x => x.ToString())),
                            StartDate = startDate,
                            EndDate = endDate,
                            Summary = isSummary
                        },
                        commandTimeout: 120
                    ).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public IEnumerable<dynamic> ProcDo(string procName, List<KeyValuePair<string, string>> parameters, IDbConnection connection = null)
	    {
		    try
		    {
			    var dbArgs = new DynamicParameters();
			    foreach (KeyValuePair<string, string> parameter in parameters)
			    {
					dbArgs.Add(parameter.Key, parameter.Value);
			    }
			    
			    using (IDbConnection db = connection ?? new SqlConnection(ConnString))
			    {
			        if (parameters.Count == 0)
			        {
			            return db.Query(procName, commandTimeout: 300, commandType: CommandType.StoredProcedure);
			        }
			        else
			        {
			            return db.Query(procName, dbArgs, commandTimeout: 300, commandType: CommandType.StoredProcedure);
                    }
                }
		    }
		    catch (Exception e)
		    {
			    Console.WriteLine(e);
			    throw;
		    }
	    }

        public MediaBrief GetMediaBrief(Guid orderId, IDbConnection connection = null)
        {
            var mediaBrief = new MediaBrief();
//            string perfDetailData = $"Execute tvdb.PerformanceDetailsData @OrderId;";
            string orderInfo = @"Execute tvdb.MediaBrief_OrderInfo @OrderId;";
            string genderAgeData = @"Execute tvdb.MediaBrief_GenderAgeData @OrderId;";
            string locationData = @"Execute tvdb.MediaBrief_LocationData @OrderId;";
            string budgetInfo = @"Execute tvdb.MediaBrief_BudgetData @OrderId;";
            string keywordInfo = @"Execute tvdb.MediaBrief_KeywordsData @OrderId;";
            string adData = @"Execute tvdb.MediaBrief_AdData @OrderId";

            //var sql = $"{perfDetailData}" +
            var sql = $"{orderInfo} " +     
                      $"{genderAgeData} " +
                      $"{locationData} " +
                      $"{budgetInfo} " +
                      $"{keywordInfo }" +
                      $"{adData} ";
            using (IDbConnection db = connection ?? new SqlConnection(ConnString))
            {
                using (var multi = db.QueryMultiple(sql, new {OrderId = orderId}))
                {
                    OrderData orderData = multi.Read<OrderData>().SingleOrDefault();
                    List<GenderAgeData> genderAgeListData = multi.Read<GenderAgeData>().ToList();
                    List<LocationData> locationListData = multi.Read<LocationData>().ToList();
                    BudgetData budgetData = multi.Read<BudgetData>().SingleOrDefault();
                    List<string> keywordData = multi.Read<string>().ToList();
                    List<AdData> adListData = multi.Read<AdData>().ToList();

                    mediaBrief.OrderData = orderData;
                    mediaBrief.GenderAgeList = genderAgeListData;
                    mediaBrief.LocationList = locationListData;
                    mediaBrief.BudgetData = budgetData;
                    mediaBrief.KeywordList = keywordData;
                    mediaBrief.AdList = adListData;
                }
            }

            return mediaBrief;
        }

        public MediaBrief GetNewMediaBrief(Guid orderId, IDbConnection connection = null)
        {
            var mediaBrief = new MediaBrief();
            //            string perfDetailData = $"Execute tvdb.PerformanceDetailsData @OrderId;";
            string orderInfo = @"Execute tvdb.MediaBrief_OrderInfo @OrderId;";
            string genderAgeData = @"Execute tvdb.MediaBrief_GenderAgeData @OrderId;";
            string locationData = @"Execute tvdb.MediaBrief_LocationData @OrderId;";
            string budgetInfo = @"Execute tvdb.MediaBrief_BudgetData @OrderId;";
            string keywordUrlInfo = @"Execute tvdb.MediaBrief_KeywordsAndUrls @OrderId;";
            string adData = @"Execute tvdb.MediaBrief_NewAdData @OrderId";
            string hhiData = @"Execute tvdb.MediaBrief_HouseholdIncomeData @OrderId;";

            //var sql = $"{perfDetailData}" +
            var sql = $"{orderInfo} " +
                      $"{genderAgeData} " +
                      $"{locationData} " +
                      $"{budgetInfo} " +
                      $"{keywordUrlInfo } " +
                      $"{adData} " +
                      $"{hhiData} ";
            using (IDbConnection db = connection ?? new SqlConnection(ConnString))
            {
                using (var multi = db.QueryMultiple(sql, new { OrderId = orderId }))
                {
                    OrderData orderData = multi.Read<OrderData>().SingleOrDefault();
                    List<GenderAgeData> genderAgeListData = multi.Read<GenderAgeData>().ToList();
                    List<LocationData> locationListData = multi.Read<LocationData>().ToList();
                    BudgetData budgetData = multi.Read<BudgetData>().SingleOrDefault();
                    KeywordUrl keywordUrlData = multi.Read<KeywordUrl>().SingleOrDefault();
                    List<AdData> adListData = multi.Read<AdData>().ToList();
                    List<HouseholdIncomeData> householdIncomeData = multi.Read<HouseholdIncomeData>().ToList();

                    mediaBrief.OrderData = orderData;
                    mediaBrief.GenderAgeList = genderAgeListData;
                    mediaBrief.LocationList = locationListData;
                    mediaBrief.BudgetData = budgetData;
                    mediaBrief.KeywordUrl = keywordUrlData;
                    mediaBrief.AdList = adListData;
                    mediaBrief.HouseholdIncomeList = householdIncomeData;
                }
            }

            return mediaBrief;
        }

        public PerformanceDetailReportParameters GetPerfomanceDetailReport(
            Guid orderId, 
            DateTime startDate,
            DateTime endDate, 
            IDbConnection connection = null)
        {
            PerformanceDetailReportParameters perfDetail = new PerformanceDetailReportParameters();

            


            var performanceRaw = $"Execute tvdb.PerformanceDetailRawData_GetByOrderId @OrderId;";
            var orderPerformance = $"Execute [tvdb].[AdDeviceStats_GetCampaignDataAggregate] @OrderId, @StartDate, @EndDate;";
            var adLists = $"Execute tvdb.PerformanceDetailAdList_GetByOrderId @OrderId, @StartDate, @EndDate;";
            var ageGroupLists = $"Execute tvdb.PerformanceDetailAgeGroupList_GetByOrderId @OrderId, @StartDate, @EndDate;";
            var deviceList = $"Execute tvdb.PerformanceDetailDeviceTypeList_GetByOrderId @OrderId, @StartDate, @EndDate;";
            var genderList = $"Execute tvdb.PerformanceDetailGenderList_GetByOrderId @OrderId, @StartDate, @EndDate;";

            var sql = $"{performanceRaw} " +
                      $"{orderPerformance} " +
                      $"{adLists} " +
                      $"{ageGroupLists} " +
                      $"{deviceList} " +
                      $"{genderList} ";

            using (IDbConnection db = connection ?? new SqlConnection(ConnString))
            {
                OrdersDates ordersDates = db.Query<OrdersDates>("Select StartDate, EndDate from orders.[order] where orderId = @OrderId", new { OrderId = orderId }, commandType: CommandType.Text).FirstOrDefault();

                DateTime actualStart = ordersDates.StartDate >= startDate ? ordersDates.StartDate : startDate;
                DateTime actualEnd = ordersDates.EndDate <= endDate ? ordersDates.EndDate : endDate;
                actualEnd = actualEnd <= DateTime.Today.Date ? actualEnd : DateTime.Today.Date;

                using (var multi =
                    db.QueryMultiple(sql, new {OrderId = orderId, StartDate = actualStart, EndDate = actualEnd}))
                {
                    var performanceDetailRaw = multi.Read<PerformanceDetailsData>().SingleOrDefault();
                    var campaignPerformanceSummary = multi.Read<OrderAggregate>().FirstOrDefault() ?? new OrderAggregate();
                    List<AdPerformanceDetail> adPerformanceList = multi.Read<AdPerformanceDetail>().ToList();
                    List<AgeGroupPerformanceDetail> ageGroupPerformanceList = multi.Read<AgeGroupPerformanceDetail>().ToList();
                    List<DeviceTypePerformanceDetail> deviceTypePerformanceList = multi.Read<DeviceTypePerformanceDetail>().ToList();
                    List<GenderPerformanceDetail> genderPerformanceList = multi.Read<GenderPerformanceDetail>().ToList();

                    if (performanceDetailRaw != null)
                    {
                        perfDetail.AdvertiserName = performanceDetailRaw.AdvertiserName;
                        perfDetail.CampaignName = performanceDetailRaw.CampaignName;
                        perfDetail.TargetPopulation = performanceDetailRaw.TargetPopulation ?? 0L;
                        perfDetail.OrderName = performanceDetailRaw.OrderName;
                        DateTime reportStartDate = (startDate < performanceDetailRaw.StartDate) ? performanceDetailRaw.StartDate : startDate;
                        DateTime reportEndDate = (endDate > performanceDetailRaw.EndDate) ? performanceDetailRaw.EndDate : endDate;
                        perfDetail.StartDate = reportStartDate;
                        perfDetail.EndDate = reportEndDate;
                    }
                    perfDetail.AdPerformanceDetails = adPerformanceList;
                    perfDetail.AgeGroupPerformanceDetails = ageGroupPerformanceList;
                    perfDetail.OrderAggregate = campaignPerformanceSummary;
                    perfDetail.DevicePerformanceDetails = deviceTypePerformanceList;
                    perfDetail.GenderPerformanceDetails = genderPerformanceList;
                }
            }

            return perfDetail;
        }

        public PerformanceSummaryReport GetPerformanceSummaryData(string orders, DateTime startDate, DateTime endDate, IDbConnection connection = null)
        {
            try
            {
                var performanceSummaryReport  = new PerformanceSummaryReport();

                var sql = @"Execute [tvdb].[OrderPerformanceSummary_GROSSEDUP_GetByOrderListAndDatesNEW] @Orders, @StartDate, @EndDate;";
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    List<OrderInfoStat> orderStats = db.Query<OrderInfoStat>(
                        sql,
                        new
                        {
                            Orders = orders,
                            StartDate = startDate,
                            EndDate = endDate
                        },
                        commandTimeout:500).ToList();
                

                    performanceSummaryReport.ReportDate = DateTime.Now;
                    performanceSummaryReport.RequestedStartDate = startDate;
                    performanceSummaryReport.RequestedEndDate = endDate;
                    performanceSummaryReport.AvailableStartDate = orderStats.Min(asd => asd.OrderStartDate);
                    performanceSummaryReport.AvailableEndDate = orderStats.Max(aed => aed.OrderEndDate);
                    performanceSummaryReport.OrderInfoStats.AddRange(orderStats);

                    return performanceSummaryReport;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public PerformanceSummaryReportParameters GetPerformanceSummary(List<Guid> orderList, DateTime startDate, DateTime endDate, IDbConnection connection = null)
        {
            try
            {
                PerformanceSummaryReportParameters perfSummary = new PerformanceSummaryReportParameters();
                var sql = @"Execute tvdb.OrderPerformanceSummary_GROSSEDUP_GetByOrderListAndDates @Orders, @StartDate, @EndDate;";
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    List<OrderPerformanceSummary> summaries = db.Query<OrderPerformanceSummary>(sql, new
                    {
                        Orders = string.Join(",", orderList.Select(n=> n.ToString()).ToArray()), //create a string list of order guids as comma separated
                        StartDate = startDate,
                        EndDate = endDate
                    }).ToList();

                    perfSummary.OrderPerformanceSummaries = new List<OrderPerformanceSummary>();
                    perfSummary.OrderPerformanceSummaries.AddRange(summaries);
                    perfSummary.OrderPerformanceSummaries.ForEach(ps =>
                    {
                        ps.StartDate = (startDate < ps.StartDate) ? ps.StartDate.Value : startDate;
                        ps.EndDate = (endDate > ps.EndDate) ? endDate: ps.EndDate.Value ;
                    });
                    DateTime minDate = perfSummary.OrderPerformanceSummaries.Min(ps => ps.StartDate).GetValueOrDefault();
                    DateTime maxDate = perfSummary.OrderPerformanceSummaries.Max(ps => ps.EndDate).GetValueOrDefault();

                    //DateTime reportStartDate = (startDate < summaries[0].StartDate) ? summaries[0].StartDate.Value : startDate;
                    //DateTime reportEndDate = (endDate > summaries[0].EndDate) ? summaries[0].EndDate.Value : endDate;
                    perfSummary.ReportPeriodText = $"{minDate:MM/dd/yyyy} to {maxDate:MM/dd/yyyy}";

                    AggregatedOrderPerformanceSummary aggSummary = new AggregatedOrderPerformanceSummary();
                    aggSummary.TotalSpend = perfSummary.OrderPerformanceSummaries.Sum(x => x.Spend ?? 0.0M);
                    aggSummary.TotalViewTime = perfSummary.OrderPerformanceSummaries.Sum(x => x.ViewTime ?? 0L);
                    aggSummary.AvgCPCV = perfSummary.OrderPerformanceSummaries.Average(x => x.AvgCPCV);
                    aggSummary.ViewRate = perfSummary.OrderPerformanceSummaries.Average(x => x.ViewRate);
                    aggSummary.TotalCompletedViews = perfSummary.OrderPerformanceSummaries.Sum(x => x.CompletedViews ?? 0L);
                    aggSummary.TotalImpressions = perfSummary.OrderPerformanceSummaries.Sum(x => x.Impressions ?? 0L);

                    perfSummary.AggregatedOrderPerformanceSummary = aggSummary;
                    return perfSummary;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }
        }

        public ResellerCampaignUploader GetResellerCampaignUploaderData(Guid orderId, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE tvdb.GetResellerCampaignUploaderData @OrderId;";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<ResellerCampaignUploader>(sql, new { OrderId = orderId }, commandTimeout: 120).First();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public string GetJsonOfSavedOrder(Guid orderId, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE tvdb.GetJsonOfSavedOrder @OrderId";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<string>(sql, new { OrderId = orderId }, commandTimeout: 120).FirstOrDefault();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public OrderData GetOrderDataFromOrderInfo(Info orderInfo, IDbConnection connection = null)
        {
            var orderData = new OrderData
            {
                OrderName = orderInfo.OrderName,
                OrderRefCode = orderInfo.OrderRefCode,
                StartDate = orderInfo.StartDate,
                EndDate = orderInfo.EndDate,
                NumOfDays = (int)(orderInfo.EndDate - orderInfo.StartDate).TotalDays
            };

            const string sql = @"EXECUTE tvdb.GetOrderDataFromInfo @CampaignId";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    var orderStuff = db.Query<OrderHierarchy>(sql, new { CampaignId = orderInfo.Campaign }, commandTimeout: 120).FirstOrDefault();
                    if (orderStuff != null)
                    {
                        orderData.AccountName = orderStuff.AccountName;
                        orderData.AdvertiserName = orderStuff.AdvertiserName;
                        orderData.AdvertiserCategoryName = orderStuff.AdvertiserCategoryName;
                        orderData.CampaignName = orderStuff.CampaignName;
                        orderData.IOName = orderStuff.CampaignName;
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return orderData;
        }

        public List<GenderAgeData> GetGenderAgeListFromOrderGenders(List<Guid> genders, List<Guid> ages, IDbConnection connection = null)
        {
            var genderAgeList = new List<GenderAgeData>();

            const string sql = @"EXECUTE tvdb.GetGenderAgeData @GenderList, @AgeList";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    string genderList = string.Join(",", genders.Select(x => x.ToString()));
                    string ageList = string.Join(",", ages.Select(y => y.ToString()));

                    List<GenderAges> genderAges = db.Query<GenderAges>(
                            sql, 
                            new
                            {
                                GenderList = genderList,
                                AgeList = ageList
                            }, 
                            commandTimeout: 120
                    ).ToList();
                    if (genderAges != null)
                    {
                        genderAges.ForEach(ga =>
                        {
                            genderAgeList.Add(new GenderAgeData
                            {
                                AgeGroupName = ga.AgeGroupName,
                                GenderName = ga.GenderName
                            });
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


            return genderAgeList;
        }

        public BudgetData GetBudgetDataFromOrder(Info orderInfo, IDbConnection connection = null)
        {
            var budgetData = new BudgetData();
            budgetData.GrossBudget = orderInfo.TotalBudget;

            return budgetData;
        }

        public List<HouseholdIncomeData> GetHHIFromOrderHHI(List<short> householdIncomes, IDbConnection connection = null)
        {
            var hhiList = new List<HouseholdIncomeData>();

            const string sql = @"EXECUTE tvdb.GetHouseholdIncomeData @HhiList";

            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    var hhis = db.Query<string>(
                        sql,
                        new
                        {
                            HhiList = string.Join(",", householdIncomes.Select(x => x.ToString()))
                        },
                        commandTimeout: 120
                    ).ToList();
                    if (hhis != null)
                    {
                        hhis.ForEach(hhi =>
                        {
                            hhiList.Add(new HouseholdIncomeData
                            {
                                IncomeLabels = hhi.ToString()
                            });
                        });
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }


            return hhiList;
        }

        public List<CampaignBudgetReport> GetCampaignBudgetingReports(List<long> customers, DateTime startDate, DateTime endDate, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE tvdb.GetBudgetGroupReportByCustomersAndDateRange @Customers, @StartDate, @EndDate";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    var customerList = string.Join(",", customers.Select(x => x.ToString()));
                    return db.Query<CampaignBudgetReport>(
                        sql, 
                        new
                        {
                            Customers = customerList,
                            StartDate = startDate,
                            EndDate = endDate
                        }, 
                        commandTimeout: 120
                    ).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }

        public List<BillingData> GetResellersBillingReport(DateTime lastDay, IDbConnection connection = null)
        {
            const string sql = @"EXECUTE tvdb.ResellersBillingReport @Yesterday";
            try
            {
                using (IDbConnection db = connection ?? new SqlConnection(ConnString))
                {
                    return db.Query<BillingData>(
                        sql,
                        new
                        {
                            Yesterday = lastDay
                        },
                        commandTimeout: 120
                    ).ToList();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
    }

    public class OrdersDates
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class OrderHierarchy
    {
        public string AccountName { get; set; }
        public string AdvertiserName { get; set; }
        public string AdvertiserCategoryName { get; set; }
        public string CampaignName { get; set; }
    }

    public class GenderAges
    {
        public string GenderName { get; set; }
        public string AgeGroupName { get; set; }
    }
}
