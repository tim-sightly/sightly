﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Reporting.DAL
{
    

    public abstract class AReportStore
    {
        protected string ConnString { get; set; }

        public AReportStore()
        {
            ConnString = ConfigurationManager.ConnectionStrings["TargetViewDb"].ConnectionString;
        }

    }
}
