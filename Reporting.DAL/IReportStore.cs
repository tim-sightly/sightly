﻿using System;
using System.Collections.Generic;
using Reporting.DAL.DTO;
using System.Data;
using Reporting.DAL.DTO.NewTargetview;
using Reporting.DAL.DTO.TargetView;

namespace Reporting.DAL
{
    public interface IReportStore
    {
        PerformanceReport GetPerformanceReport(Guid campaignId, IDbConnection connection = null);
        PerformanceReport GetPerformanceReport(long customerId, IDbConnection connection = null);
        PerformanceReport GetPerformanceReport(long customerId, DateTime startDate, DateTime endDate, IDbConnection connection = null);
        List<MediaAgencyCampaignPlacement> GetMediaAgencyCampaignPlacements(long customerId, IDbConnection connection = null);
        List<MediaAgencyOrderPlacement> GetMediaAgencyOrderPlacements(long customerId, IDbConnection connection = null);

        List<CampaignPubReport> GetPubReports(long customerId, IDbConnection connection = null);
        List<CampaignPubReport> GetPubReportsByDateRange(long prCustomerId, DateTime startDate, DateTime endDate, IDbConnection connection = null);
        IEnumerable<CampaignPubReport> GetAggregatedPubReportData(List<long> customers, DateTime startDate, DateTime endDate, bool isSummary, IDbConnection connection = null);
        IEnumerable<CampaignPubReport> GetInternalAggregatedPubReportData(List<long> customers, DateTime startDate, DateTime endDate, bool isSummary, IDbConnection connection = null);
        IEnumerable<dynamic> ProcDo(string procName, List<KeyValuePair<string, string>> parameters, IDbConnection connection = null);
        MediaBrief GetMediaBrief(Guid orderId, IDbConnection connection = null);
        MediaBrief GetNewMediaBrief(Guid orderId, IDbConnection connection = null);

        PerformanceDetailReportParameters GetPerfomanceDetailReport(Guid orderId, DateTime startDate, DateTime endDate, IDbConnection connection = null);
        PerformanceSummaryReport GetPerformanceSummaryData(string orders, DateTime startDate, DateTime endDate, IDbConnection connection = null);
        PerformanceSummaryReportParameters GetPerformanceSummary(List<Guid> orderList, DateTime startDate, DateTime endDate, IDbConnection connection = null);
        ResellerCampaignUploader GetResellerCampaignUploaderData(Guid orderId, IDbConnection connection = null);
        string GetJsonOfSavedOrder(Guid orderId, IDbConnection connection = null);
        OrderData GetOrderDataFromOrderInfo(Info orderInfo, IDbConnection connection = null);
        List<GenderAgeData> GetGenderAgeListFromOrderGenders(List<Guid> genders, List<Guid> ages, IDbConnection connection = null);
        BudgetData GetBudgetDataFromOrder(Info orderInfo, IDbConnection connection = null);
        List<HouseholdIncomeData> GetHHIFromOrderHHI(List<short> householdIncomes, IDbConnection connection = null);
        List<CampaignBudgetReport> GetCampaignBudgetingReports(List<long> customers, DateTime startDate, DateTime endDate, IDbConnection connection = null);
        List<BillingData> GetResellersBillingReport(DateTime lastDay, IDbConnection connection = null);
    }
}
