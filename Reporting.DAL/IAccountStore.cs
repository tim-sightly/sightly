﻿using System.Collections.Generic;
using System.Data;
using Reporting.DAL.DTO;

namespace Reporting.DAL
{
    public interface IAccountStore
    {
        List<Campaign> GetAllCampaigns(IDbConnection connection = null);
        List<CampaignAwCustomer> GetAllMediaAgencyCampaigns(IDbConnection connection = null);
        string GetCampaignName(long cid, IDbConnection connection = null);
    }
}
