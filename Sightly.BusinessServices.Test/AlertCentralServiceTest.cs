﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessServices.cs;

namespace Sightly.BusinessServices.Test
{
    [TestClass]
    public class AlertCentralServiceTest
    {

        private static IContainer Container { get; set; }
        private static IAlertCentralService _alertCentralService { get; set; }
        private const long CustomerId = 1361365800;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = IOC.Testing.DIConfiguration.GetConfiguredContainer();
            _alertCentralService = Container.Resolve<IAlertCentralService>();
        }

        [TestMethod]
        public void GetKpiData()
        {
            List<AlertKpiDataObject> alerts = new List<AlertKpiDataObject>();
            var listOfCids = _alertCentralService.GetAwCustomersThatAreCurrentlyActive(DateTime.Parse("2017-12-20"));
            

            listOfCids.ForEach(customer =>
            {

                var kpiData = GetAlertKpiDataFromCustomerId(customer);

                kpiData.KpiDefinitions.ForEach(kpi =>
                {
                    switch (kpi.KpiMetricAbbreviation)
                    {
                        case "audOTR":
                            if (kpiData.NielsenKpiStats != null)
                            {
                                kpi.ActualValue = 
                                    kpiData.NielsenKpiStats.DarTotalDigital.ToString(CultureInfo.InvariantCulture);
                                kpi.Status = GetStatusFromKpi(kpi);
                            }
                            else
                                kpi.Status = -2;
                            break;
                        case "CPM":
                            if(kpiData.AdwordsKpiStats != null)
                            { 
                                kpi.ActualValue = 
                                    kpiData.AdwordsKpiStats.CPM.ToString(CultureInfo.InvariantCulture);
                                kpi.Status = GetStatusFromKpi(kpi);
                            }
                            else
                                kpi.Status = -2;
                            break;
                        case "CPV":
                            if(kpiData.AdwordsKpiStats != null)
                            { 
                                kpi.ActualValue = 
                                    kpiData.AdwordsKpiStats.CPV.ToString(CultureInfo.InvariantCulture);
                                kpi.Status = GetStatusFromKpi(kpi);
                            }
                            else
                                kpi.Status = -2;
                            break;
                        case "CTR":
                            if (kpiData.AdwordsKpiStats != null)
                            {
                                kpi.ActualValue = 
                                    kpiData.AdwordsKpiStats.CTR.ToString(CultureInfo.InvariantCulture);
                                kpi.Status = GetStatusFromKpi(kpi);
                            }
                            else
                                kpi.Status = -2;
                            break;
                        case "dv1x1bsOTR":
                            if(kpiData.DoubleVerifyKpiStats != null)
                            { 
                                kpi.ActualValue = 
                                    kpiData.DoubleVerifyKpiStats.BrandSafetyOnTargetPercent_1x1.ToString(CultureInfo.InvariantCulture);
                                kpi.Status = GetStatusFromKpi(kpi);
                            }
                            else
                                kpi.Status = -2;
                            break;
                        case "dv1x1frdOTR":
                            if (kpiData.DoubleVerifyKpiStats != null)
                            {
                                kpi.ActualValue =
                                    kpiData.DoubleVerifyKpiStats.FraudSivtRate_1x1.ToString(CultureInfo.InvariantCulture);
                                kpi.Status = GetStatusFromKpi(kpi);
                            }
                            else
                                kpi.Status = -2;
                            break;
                        case "dv1x1geoOTR":
                            if (kpiData.DoubleVerifyKpiStats != null)
                            {
                                kpi.ActualValue =
                                    kpiData.DoubleVerifyKpiStats.InGeoRate_1x1.ToString(CultureInfo.InvariantCulture);
                                kpi.Status = GetStatusFromKpi(kpi);
                            }
                            else
                                kpi.Status = -2;
                            break;
                        case "dvfrdOTR":
                            if (kpiData.DoubleVerifyKpiStats != null)
                            {
                                kpi.ActualValue =
                                    kpiData.DoubleVerifyKpiStats.FraudSivtRate_1x1.ToString(CultureInfo.InvariantCulture);
                                kpi.Status = GetStatusFromKpi(kpi);
                            }
                            else
                                kpi.Status = -2;
                            break;
                        case "dvmrcvOTR":
                            if (kpiData.DoubleVerifyKpiStats != null)
                            {
                                kpi.ActualValue =
                                    kpiData.DoubleVerifyKpiStats.VideoViewableRate.ToString(CultureInfo.InvariantCulture);
                                kpi.Status = GetStatusFromKpi(kpi);
                            }
                            else
                                kpi.Status = -2;
                            break;
                        case "enGRP":
                            kpi.ActualValue = "I don't know what this is!!";
                            kpi.Status = -2;
                            break;
                        case "IMP":
                            if (kpiData.AdwordsKpiStats != null)
                            {
                                kpi.ActualValue = 
                                    kpiData.AdwordsKpiStats.Impressions.ToString(CultureInfo.InvariantCulture);
                                kpi.Status = GetStatusFromKpi(kpi);
                            }
                            else
                                kpi.Status = -2;
                            break;
                        case "mhumOTR":
                            if(kpiData.MoatKpiStats != null)
                            { 
                                kpi.ActualValue = 
                                    kpiData.MoatKpiStats.HumanRate.ToString(CultureInfo.InvariantCulture);
                                kpi.Status = GetStatusFromKpi(kpi);
                            }
                            else
                                kpi.Status = -2;
                            break;
                        case "mmrcvOTR":
                            if (kpiData.MoatKpiStats != null)
                            {
                                kpi.ActualValue =
                                    kpiData.MoatKpiStats.Two_Sec_In_View_Rate_Unfiltered.ToString(CultureInfo.InvariantCulture);
                                kpi.Status = GetStatusFromKpi(kpi);
                            }
                            else
                                kpi.Status = -2;
                            break;
                        case "VR":
                            if (kpiData.AdwordsKpiStats != null)
                            {
                                kpi.ActualValue = 
                                    kpiData.AdwordsKpiStats.VR.ToString(CultureInfo.InvariantCulture);
                                kpi.Status = GetStatusFromKpi(kpi);
                            }
                            else
                                kpi.Status = -2;
                            break;
                        default:
                            break;
                    }
                });

                alerts.Add(kpiData);
            });



            Assert.IsNotNull(alerts);
        }

        private int GetStatusFromKpi(KpiDefinitionData kpi)
        {
            int status = 0;
            switch (kpi.ConditionDisplay)
            {
                case "<=":
                    status = Convert.ToDecimal(kpi.ActualValue) <=  Convert.ToDecimal(kpi.EquationValue)? 1 : -1;
                    break;
                case ">=":
                    status = Convert.ToDecimal(kpi.ActualValue) >= Convert.ToDecimal(kpi.EquationValue) ? 1 : -1;
                    break;
                case "<":
                    status = Convert.ToDecimal(kpi.ActualValue) < Convert.ToDecimal(kpi.EquationValue) ? 1 : -1;
                    break;
                case ">":
                    status = Convert.ToDecimal(kpi.ActualValue) > Convert.ToDecimal(kpi.EquationValue) ? 1 : -1;
                    break;
                case "=":
                    status = Convert.ToDecimal(kpi.ActualValue) == Convert.ToDecimal(kpi.EquationValue) ? 1 : -1;
                    break;
                default:
                    break;
            }
            return status;
        }

        

        private AlertKpiDataObject GetAlertKpiDataFromCustomerId(AdwordCustomer customer)
        {
            var kpiData = new AlertKpiDataObject(customer.CustomerId, customer.CustomerName, customer.StartDate, customer.EndDate, customer.TotalDays, customer.DaysSoFar, customer.PercentComplete, customer.DaysLeft);
            kpiData.KpiDefinitions = _alertCentralService.GetKpiDefinitionData(customer.CustomerId);
            kpiData.AdwordsKpiStats = _alertCentralService.GetAdwordsKpiStatsData(customer.CustomerId);
            kpiData.DoubleVerifyKpiStats = _alertCentralService.GetDoubleVerifyKpiStatsData(customer.CustomerId);
            kpiData.MoatKpiStats = _alertCentralService.GetMoatKpiStatsData(customer.CustomerId);
            kpiData.NielsenKpiStats = _alertCentralService.GetNielsenKpiStatsData(customer.CustomerId);

            return kpiData;
        }
       
    }
}
