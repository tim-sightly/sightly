﻿using System;
using System.Text;
using System.Collections.Generic;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Adwords.Utility.Interface;
using Sightly.Business.Association.Customer;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;

namespace Sightly.Business.Association.Test
{
    /// <summary>
    /// Summary description for CustomerAssociationTest
    /// </summary>
    [TestClass]
    public class CustomerAssociationTest
    {

        private static IContainer Container { get; set; }
        private ICustomerAssociation _customerAssociation;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
           _customerAssociation = Container.Resolve<ICustomerAssociation>();
        }

        [TestMethod]
        public void AssociateAdwordsCustomerTest()
        {
            try
            {
                _customerAssociation.AssociationAwCustomerToTv();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Assert.Fail();
            }
        }
    }
}
