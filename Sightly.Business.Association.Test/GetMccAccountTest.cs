﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Adwords.Utility.Interface;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;

namespace Sightly.Business.Association.Test
{
    [TestClass]
    public class GetMccAccountTest
    {
        private static  IContainer Container { get; set; }
        private IStatRepository _statRepository;
        private IAccountHierarchy _accountHierarchy;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _statRepository = Container.Resolve<IStatRepository>();
            _accountHierarchy = Container.Resolve<IAccountHierarchy>();
        }


        [TestMethod]
        public void GetTvChannelPartnerAccountData()
        {
            var accountData = _statRepository.GetChannelPartnerMccCustomerList();
            Assert.IsNotNull(accountData);
        }

        [TestMethod]
        public void GetAdwordsChannelPartnerData()
        {
            var adwordsAccount = new List<AdwordsManagedCustomerTreeNode>();
            var accountData = _statRepository.GetChannelPartnerMccCustomerList();

            accountData.GroupBy(ad => ad.AdwordsMccAccountId).ToList().ForEach(account =>
            {
                adwordsAccount.AddRange(_accountHierarchy.GetAdwordsCustomerByMcc(account.Key));
            });

            Assert.IsNotNull(adwordsAccount);
        }

        [TestMethod]
        public void CompareAwCustomerWithTvAccountCustomerData()
        {
            var accountData = _statRepository.GetChannelPartnerMccCustomerList().Where(cust => cust.AdwordsCustomerId == null).ToList();
            
            accountData.GroupBy(ad => ad.AdwordsMccAccountId).Select(grp => grp.ToList()).ToList().ForEach(customer =>
            {
                var adwordsCustomers = _accountHierarchy.GetAdwordsCustomerByMcc(customer.First().AdwordsMccAccountId).First().ChildCustomers.ToList();
                customer.ForEach(rec =>
                {
                    var adwordsManagedCustomerTreeNode = adwordsCustomers.FirstOrDefault(ac => String.Equals(ac.Customer.AwCustomerName, rec.CustomerId, StringComparison.CurrentCultureIgnoreCase));
                    if (adwordsManagedCustomerTreeNode != null)
                        rec.AdwordsCustomerId = adwordsManagedCustomerTreeNode.Customer.AwCustomerId;
                });
            });

            Assert.IsNotNull(accountData);
        }
    }
}
