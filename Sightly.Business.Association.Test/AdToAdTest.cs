﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Adwords.Utility.Interface;
using Sightly.Business.Association.Ad;
using Sightly.Business.Association.Campaign;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;

namespace Sightly.Business.Association.Test
{
    [TestClass]
    public class AdToAdTest
    {
        private static IContainer Container { get; set; }
        private IStatRepository _statRepository;
        private IAd _adwordAd;
        private IAdAssociation _adAssociation;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _statRepository = Container.Resolve<IStatRepository>();
            _adwordAd = Container.Resolve<IAd>();
            _adAssociation = Container.Resolve<IAdAssociation>();
        }

        [TestMethod]
        public void AssociateAdwordsCustomerTest()
        {
            try
            {
                _adAssociation.AssociateAwAdToTv();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Assert.Fail();
            }
        }

        [TestMethod]
        public void GetAdWordsAdToAd()
        {
            var awAdList = new List<AdAwAdData>();
            var adData = _statRepository.GetChannelPartnerCampaignList().Where(bg => bg.AdWordsAdId == null).ToList();

            if (adData.Count > 0)
            {
                adData.ForEach(ad =>
                {
                    var adRecords = _adwordAd.GetAdData(ad.AdWordsCustomerId.ToString());

                    adRecords.ForEach(awAd =>
                    {
                        var awAdId = GetAdIdFromAdName(awAd.AdName);

                        if(ad.AdId != awAdId) return;

                        awAdList.Add(new AdAwAdData
                        {
                            AdAdWordsInfoId = Guid.NewGuid(),
                            AdId = ad.AdId,
                            AdWordsAdId = awAd.AdId,
                            AdWordsAdName = awAd.AdName,
                            AdWordsCampaignId = awAd.AwCampaignId,
                            BudgetGroupId = ad.BudgetGroupId
                        });

                    });
                });
            }


            Assert.IsNotNull(awAdList);
        }

        private Guid GetAdIdFromAdName(string adName)
        {
            var counter = adName.LastIndexOf(" - ", StringComparison.Ordinal) + 3;
            Guid result;
            Guid.TryParse(adName.Substring(counter, adName.Length - counter), out result);
            return result;
        }
    }
}
