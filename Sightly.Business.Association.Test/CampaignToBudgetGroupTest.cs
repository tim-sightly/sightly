﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sightly.Adwords.Utility;
using Sightly.Adwords.Utility.Interface;
using Sightly.Business.Association.Campaign;
using Sightly.Models;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using Sightly.Test.Common;

namespace Sightly.Business.Association.Test
{
    [TestClass]
    public class CampaignToBudgetGroupTest
    {
        private static IContainer Container { get; set; }
        private IStatRepository _statRepository;
        private ICampaign _adwordCampaign;
        private ICampaignAssociation _campaignAssociation;

        [TestInitialize]
        public void InitializeTest()
        {
            Container = DIConfiguration.GetConfiguredContainer();
            _statRepository = Container.Resolve<IStatRepository>();
            _adwordCampaign = Container.Resolve<ICampaign>();
            _campaignAssociation = Container.Resolve<ICampaignAssociation>();
        }

        [TestMethod]
        public void TestCampaignAssociation()
        {
            try
            {
                _campaignAssociation.AssociationAwCustomerToTv();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                Assert.Fail();
            }
        }
        [TestMethod]
        public void GetTvCampaignAndAdChannelPartnerData()
        {
            var budgetGroupData = _statRepository.GetChannelPartnerCampaignList();
            Assert.IsNotNull(budgetGroupData);
        }

        [TestMethod]
        public void GetAdwordsChannelPartnerCampaignData()
        {

            var campaignList = new List<BudgetGroupCampaignData>();

            var budgetGroupData = _statRepository.GetChannelPartnerCampaignList().Where(bg => bg.AdWordsCampaignId == null).ToList();
            if (budgetGroupData.Count > 0)
            {
                budgetGroupData.ToList().ForEach(campaign =>
                {
                    var campRecord = _adwordCampaign.GetCampaignData(campaign.AdWordsCustomerId.ToString());
                    campRecord.ForEach(camp =>
                    {
                        var awBudgetGroupId = GetCampaignIdFromCampaignName(camp.AwCampaignName);
                        if (campaign.BudgetGroupId != awBudgetGroupId) return;
                    
                        campaignList.Add(new BudgetGroupCampaignData
                        {
                            AdWordsCampaignId = camp.AwCampaignId,
                            BudgetGroupId = campaign.BudgetGroupId,
                            LastModifiedBy = "Data Pipeline"
                        });
                    });
                
                });
            }
         Assert.IsNotNull(campaignList);
        }

        private Guid GetCampaignIdFromCampaignName(string campaignName)
        {
            var counter = campaignName.LastIndexOf(" - ", StringComparison.Ordinal) + 3;
            return Guid.Parse(campaignName.Substring(counter, campaignName.Length - counter));
        }
    }
}
