﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using OfficeOpenXml.Style;
using Reporting.DAL.DTO;
using Sightly.ReportGeneration.Generators.Interface;

namespace Sightly.ReportGeneration.Generators
{
    public class PerformanceReportExcel : AExcelReport, IExcelReport
    {
        #region constants

        private const int ROW_CAMPAIGN_TO_DATE = 9;
        private const int ROW_PERFORMANCE = 13;
        private const int ROW_AD = 8;
        private const int ROW_AGE = 17;
        private const int ROW_GENDER = 27;
        private const int ROW_DEVICE = 37;

        private const int AD_STATS_BASE_ROW = 9;

        private readonly Color liteGreyBackgroundColor = ColorTranslator.FromHtml("#F2F2F2");

        private readonly string INT_FORMAT = "#,##0";
        private readonly string PERCENTAGE_FORMAT = "#0.00%";
        private readonly string MONEY_FORMAT = "$0.00";

        #endregion

        #region variables

        private PerformanceReport _data;
        private DateTime _now;
        private int _rowOffset;
        private readonly string _dateFormatHeader = "MM-dd-yyyy";
        private readonly string _dateFormat = "MM/dd/yyyy";

        public string CampaignName
        {
            get
            {
                return _data.CampaignName;
            }
        }

        protected ExcelWorksheet AdStatsWorkSheet
        {
            get
            {
                return Report.Workbook.Worksheets[2];
            }
        }

        #endregion

        public PerformanceReportExcel(PerformanceReport performanceReport, DateTime? now = null) 
            : base("Sightly.ReportGeneration.Templates.Excel.PerformanceReport.xlsx")
        {
            _data = performanceReport;
            _now = now ?? DateTime.Now;
            _rowOffset = 0;

            Build();
        }

        private void Build()
        {
            foreach(var ws in Report.Workbook.Worksheets)
            {
                BuildHeader(ws);
            }
            
            BuildCampaignToDate();
            BuildPerformanceStats();
            BuildAdStats();
            ManipulateAgeChart();
            ManipulateGenderChart();
            ManipulateDeviceChart();
            BuildAgeStats();
            BuildGenderStats();
            BuildDeviceStats();
        }

        private void BuildHeader(ExcelWorksheet ws)
        {
            ws.Cells[2, 5].Value = _data.CampaignName;
            ws.Cells[3, 5].Value = _data.CampaignStartDate.ToString(_dateFormatHeader);
            ws.Cells[4, 5].Value = _data.CampaignEndDate.ToString(_dateFormatHeader);
            ws.Cells[5, 5].Value = _now.ToString(_dateFormatHeader);
        }

        private void BuildCampaignToDate()
        {
            if (_data.CampaignToDate == null)
            {
                _data.CampaignToDate = new CampaignToDate()
                {
                    Impressions = 0,
                    Views = 0,
                    Clicks = 0,
                    TotalSpend = 0.00m
                };
            }

            WorkSheet.Cells[ROW_CAMPAIGN_TO_DATE, 2].Value = _data.CampaignToDate.Impressions;
            WorkSheet.Cells[ROW_CAMPAIGN_TO_DATE, 3].Value = _data.CampaignToDate.Views;
            WorkSheet.Cells[ROW_CAMPAIGN_TO_DATE, 4].Value = _data.CampaignToDate.ViewRate;
            WorkSheet.Cells[ROW_CAMPAIGN_TO_DATE, 5].Value = _data.CampaignToDate.Clicks;
            WorkSheet.Cells[ROW_CAMPAIGN_TO_DATE, 6].Value = _data.CampaignToDate.CTR;
            WorkSheet.Cells[ROW_CAMPAIGN_TO_DATE, 7].Value = _data.CampaignToDate.AvgCPCV;
            WorkSheet.Cells[ROW_CAMPAIGN_TO_DATE, 8].Value = _data.CampaignToDate.Ecpm;
            WorkSheet.Cells[ROW_CAMPAIGN_TO_DATE, 9].Value = _data.CampaignToDate.TotalSpend;
            WorkSheet.Cells[ROW_CAMPAIGN_TO_DATE, 10].Value = GetActualDailySpendToDate();
        }

        private void BuildPerformanceStats()
        {
            _data.Performance = _data.Performance ?? new Performance()
            {
                Impressions = 0,
                Views = 0,
                Clicks = 0,
                TotalSpend = 0,
                StartWeekDate = GetPreviousWholeWeekStartDate(),
                EndWeekDate = GetPreviousWholeWeekEndDate()
            };

            string header = $"Performance \n {_data.Performance.StartWeekDate.ToString(_dateFormat)} - {_data.Performance.EndWeekDate.ToString(_dateFormat)}";

            WorkSheet.Cells[ROW_PERFORMANCE - 1, 1].Value = header;
            WorkSheet.Cells[ROW_PERFORMANCE, 2].Value = _data.Performance.Impressions;
            WorkSheet.Cells[ROW_PERFORMANCE, 3].Value = _data.Performance.Views;
            WorkSheet.Cells[ROW_PERFORMANCE, 4].Value = _data.Performance.ViewRate;
            WorkSheet.Cells[ROW_PERFORMANCE, 5].Value = _data.Performance.Clicks;
            WorkSheet.Cells[ROW_PERFORMANCE, 6].Value = _data.Performance.CTR;
            WorkSheet.Cells[ROW_PERFORMANCE, 7].Value = _data.Performance.AvgCPCV;
            WorkSheet.Cells[ROW_PERFORMANCE, 8].Value = _data.Performance.Ecpm;
            WorkSheet.Cells[ROW_PERFORMANCE, 9].Value = _data.Performance.TotalSpend;
            DateTime mondayOfLastWeek = _now.AddDays( -(int)_now.DayOfWeek - 6 );
            DateTime sundayOfLastWeek = mondayOfLastWeek.AddDays(6);
            WorkSheet.Cells[ROW_PERFORMANCE, 10].Value = GetActualDailySpendPreviousWeek();
        }

        private void BuildAdStats()
        {
            int rowCount = AD_STATS_BASE_ROW;
            bool shadeEvenRow = AD_STATS_BASE_ROW % 2 != 0;

            FormatAdStats(AD_STATS_BASE_ROW, AD_STATS_BASE_ROW + _data.AdStats.Count);

            var sortedAdStats = _data.AdStats.OrderByDescending(adStat => adStat.Views);

            foreach (var adStat in sortedAdStats)
            {
                if ((rowCount % 2 == 0) == shadeEvenRow)
                    BuildAdRow(adStat, liteGreyBackgroundColor, rowCount);
                else
                    BuildAdRow(adStat, rowCount); 

                rowCount++;
            }
        }

        private void ManipulateAgeChart()
        {
            int rowCount = ROW_AGE;

            Range chart3xSeries = new Range()
            {
                TopRow = rowCount,
                LeftColumn = 1,
                BottomRow = rowCount + _data.AgeStats.Count - 1,
                RightColumn = 1
            };

            Range chart3Series = new Range()
            {
                TopRow = rowCount,
                LeftColumn = 2,
                BottomRow = rowCount + _data.AgeStats.Count - 1,
                RightColumn = 2
            };

            Position chart3Position = new Position()
            {
                TopRow = rowCount - 2,
                TopIntOffset = 0,
                LeftColumn = 8,
                LeftIntOffset = 25
            };

            MoveBarChart("Chart 3", chart3xSeries, chart3Series, chart3Position);
        }

        private void ManipulateGenderChart()
        {
            int rowCount = ROW_GENDER;

            Range chart4xSeries = new Range()
            {
                TopRow = rowCount,
                LeftColumn = 1,
                BottomRow = rowCount + _data.GenderStats.Count - 1,
                RightColumn = 1
            };

            Range chart4Series = new Range()
            {
                TopRow = rowCount,
                LeftColumn = 2,
                BottomRow = rowCount + _data.GenderStats.Count - 1,
                RightColumn = 2
            };

            Position chart4Position = new Position()
            {
                TopRow = rowCount - 2,
                TopIntOffset = 0,
                LeftColumn = 8,
                LeftIntOffset = 25
            };

            MoveBarChart("Chart 4", chart4xSeries, chart4Series, chart4Position);
        }

        private void ManipulateDeviceChart()
        {
            int rowCount = ROW_DEVICE;

            Range chart1xSeries = new Range()
            {
                TopRow = rowCount,
                LeftColumn = 1,
                BottomRow = rowCount + _data.DeviceStats.Count - 1,
                RightColumn = 1
            };

            Range chart1Series = new Range()
            {
                TopRow = rowCount,
                LeftColumn = 2,
                BottomRow = rowCount + _data.DeviceStats.Count - 1,
                RightColumn = 2
            };

            Position chart1Position = new Position()
            {
                TopRow = rowCount - 2,
                TopIntOffset = 0,
                LeftColumn = 8,
                LeftIntOffset = 25
            };

            MoveBarChart("Chart 1", chart1xSeries, chart1Series, chart1Position);
        }

        private void BuildAgeStats()
        {
            int rowCount = ROW_AGE;
            bool shadeEvenRow = rowCount % 2 != 0;            

            //Age stats should not have to be formatted.
            //The template should take care of this but for some reason
            //formatting is lost when writing to cells.
            FormatAgeStats();

            _data.AgeStats.Sort((a, b) => a.AgeGroupName.CompareTo(b.AgeGroupName));

            foreach (var ageStat in _data.AgeStats)
            {
                if ( (rowCount % 2 == 0) == shadeEvenRow)
                    BuildRow(ageStat.AgeGroupName, ageStat, liteGreyBackgroundColor, rowCount);
                else
                    BuildRow(ageStat.AgeGroupName, ageStat, rowCount);

                rowCount++;
            }
        }

        private void BuildGenderStats()
        {
            int rowCount = ROW_GENDER;

            _data.GenderStats.Where(gs => gs.GenderName == "Male").ForEach(gs => BuildRow(gs.GenderName, gs, rowCount++));
            _data.GenderStats.Where(gs => gs.GenderName == "Female").ForEach(gs => BuildRow(gs.GenderName, gs, liteGreyBackgroundColor, rowCount++));
            _data.GenderStats.Where(gs => String.Equals(gs.GenderName, "undetermined", StringComparison.OrdinalIgnoreCase) || String.Equals(gs.GenderName, "unknown", StringComparison.OrdinalIgnoreCase))
                .ForEach(gs => BuildRow(gs.GenderName, gs, rowCount++));
        }

        private void BuildDeviceStats()
        {
            int rowCount = ROW_DEVICE + _rowOffset;

            _data.DeviceStats.Where(ds => String.Equals(ds.Device, "Computers", StringComparison.OrdinalIgnoreCase)).ForEach(ds => BuildRow(ds.Device, ds, rowCount++));
            _data.DeviceStats.Where(ds => String.Equals(ds.Device, "Mobile devices with full browsers", StringComparison.OrdinalIgnoreCase)).ForEach(ds => BuildRow(ds.Device, ds, liteGreyBackgroundColor, rowCount++));
            _data.DeviceStats.Where(ds => String.Equals(ds.Device, "Tablets with full browsers", StringComparison.OrdinalIgnoreCase)).ForEach(ds => BuildRow(ds.Device, ds, rowCount++));
            _data.DeviceStats.Where(ds => String.Equals(ds.Device, "Other", StringComparison.OrdinalIgnoreCase)).ForEach(ds => BuildRow(ds.Device, ds, liteGreyBackgroundColor, rowCount++));
        }

        #region Helpers

        private void BuildRow(string rowName, AStatRow row, int rowCount)
        {
            WorkSheet.Cells[rowCount, 2, rowCount, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            WorkSheet.Cells[rowCount, 7, rowCount, 8].Style.Numberformat.Format = "$###,###,##0.00";

            WorkSheet.Cells[rowCount, 1].Value = rowName;
            WorkSheet.Cells[rowCount, 2].Value = row.Impressions;
            WorkSheet.Cells[rowCount, 3].Value = row.Views;
            WorkSheet.Cells[rowCount, 4].Value = row.ViewRate;
            WorkSheet.Cells[rowCount, 5].Value = row.Clicks;
            WorkSheet.Cells[rowCount, 6].Value = row.CTR;
            WorkSheet.Cells[rowCount, 7].Value = row.AvgCPCV;
            WorkSheet.Cells[rowCount, 8].Value = row.TotalSpend;

        }

        private void BuildRow(string rowName, AStatRow row, Color backgroundColor, int rowCount)
        {
            WorkSheet.Cells[rowCount, 1, rowCount, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
            WorkSheet.Cells[rowCount, 1, rowCount, 8].Style.Fill.BackgroundColor.SetColor(backgroundColor);
            WorkSheet.Cells[rowCount, 2, rowCount, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            WorkSheet.Cells[rowCount, 2, rowCount, 3].Style.Numberformat.Format = INT_FORMAT;
            WorkSheet.Cells[rowCount, 5, rowCount, 5].Style.Numberformat.Format = INT_FORMAT;
            WorkSheet.Cells[rowCount, 7, rowCount, 8].Style.Numberformat.Format = "$###,###,##0.00";

            WorkSheet.Cells[rowCount, 1].Value = rowName;
            WorkSheet.Cells[rowCount, 2].Value = row.Impressions;
            WorkSheet.Cells[rowCount, 3].Value = row.Views;
            WorkSheet.Cells[rowCount, 4].Value = row.ViewRate;
            WorkSheet.Cells[rowCount, 5].Value = row.Clicks;
            WorkSheet.Cells[rowCount, 6].Value = row.CTR;
            WorkSheet.Cells[rowCount, 7].Value = row.AvgCPCV;
            WorkSheet.Cells[rowCount, 8].Value = row.TotalSpend;
        }

        private void BuildAdRow(AdStats adStats, int rowCount)
        {
            WorkSheet.Cells[rowCount, 2, rowCount, 3].Style.Numberformat.Format = INT_FORMAT;
            WorkSheet.Cells[rowCount, 5, rowCount, 5].Style.Numberformat.Format = INT_FORMAT;
            WorkSheet.Cells[rowCount, 7, rowCount, 8].Style.Numberformat.Format = "$###,###,##0.00";

            AdStatsWorkSheet.Cells[rowCount, 1].Value = adStats.AdName;
            AdStatsWorkSheet.Cells[rowCount, 2].Value = adStats.Impressions;
            AdStatsWorkSheet.Cells[rowCount, 3].Value = adStats.Views;
            AdStatsWorkSheet.Cells[rowCount, 4].Value = adStats.ViewRate;
            AdStatsWorkSheet.Cells[rowCount, 5].Value = adStats.Clicks;
            AdStatsWorkSheet.Cells[rowCount, 6].Value = adStats.CTR;
            AdStatsWorkSheet.Cells[rowCount, 7].Value = adStats.Q25;
            AdStatsWorkSheet.Cells[rowCount, 8].Value = adStats.Q50;
            AdStatsWorkSheet.Cells[rowCount, 9].Value = adStats.Q75;
            AdStatsWorkSheet.Cells[rowCount, 10].Value = adStats.Q100;
            AdStatsWorkSheet.Cells[rowCount, 11].Value = adStats.AvgCPCV;
            AdStatsWorkSheet.Cells[rowCount, 12].Value = adStats.TotalSpend;
        }


        private void BuildAdRow(AdStats adStats, Color backgroundColor, int rowCount)
        {
            AdStatsWorkSheet.Cells[rowCount, 1, rowCount, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
            AdStatsWorkSheet.Cells[rowCount, 1, rowCount, 12].Style.Fill.BackgroundColor.SetColor(backgroundColor);
            AdStatsWorkSheet.Cells[rowCount, 1].Value = adStats.AdName;
            AdStatsWorkSheet.Cells[rowCount, 2].Value = adStats.Impressions;
            AdStatsWorkSheet.Cells[rowCount, 3].Value = adStats.Views;
            AdStatsWorkSheet.Cells[rowCount, 4].Value = adStats.ViewRate;
            AdStatsWorkSheet.Cells[rowCount, 5].Value = adStats.Clicks;
            AdStatsWorkSheet.Cells[rowCount, 6].Value = adStats.CTR;
            AdStatsWorkSheet.Cells[rowCount, 7].Value = adStats.Q25;
            AdStatsWorkSheet.Cells[rowCount, 8].Value = adStats.Q50;
            AdStatsWorkSheet.Cells[rowCount, 9].Value = adStats.Q75;
            AdStatsWorkSheet.Cells[rowCount, 10].Value = adStats.Q100;
            AdStatsWorkSheet.Cells[rowCount, 11].Value = adStats.AvgCPCV;
            AdStatsWorkSheet.Cells[rowCount, 12].Value = adStats.TotalSpend;
        }

        private void MovePieChart(string chartName, Range xSeries, Range series, Position position)
        {
            ExcelPieChart chart = WorkSheet.Drawings[chartName] as ExcelPieChart;
            var chartSeries = chart.Series[0];
            chartSeries.XSeries = WorkSheet.Cells[xSeries.TopRow, xSeries.LeftColumn, xSeries.BottomRow, xSeries.RightColumn].FullAddress;
            chartSeries.Series = WorkSheet.Cells[series.TopRow, series.LeftColumn, series.BottomRow, series.RightColumn].FullAddress;
            chart.SetPosition(position.TopRow, position.TopIntOffset, position.LeftColumn, position.LeftIntOffset);
        }

        private void MoveBarChart(string chartName, Range xSeries, Range series, Position position)
        {
            ExcelBarChart chart = WorkSheet.Drawings[chartName] as ExcelBarChart;
            var chartSeries = chart.Series[0];
            chartSeries.XSeries = WorkSheet.Cells[xSeries.TopRow, xSeries.LeftColumn, xSeries.BottomRow, xSeries.RightColumn].FullAddress;
            chartSeries.Series = WorkSheet.Cells[series.TopRow, series.LeftColumn, series.BottomRow, series.RightColumn].FullAddress;
            chart.SetPosition(position.TopRow, position.TopIntOffset, position.LeftColumn, position.LeftIntOffset);
        }

        private DateTime GetPreviousWholeWeekStartDate()
        {
            DayOfWeek startOfWeek = DayOfWeek.Monday;
            int diff = _now.DayOfWeek - startOfWeek;
            return _now.AddDays(-(diff + 7));
        }

        private DateTime GetPreviousWholeWeekEndDate()
        {
            DayOfWeek startOfWeek = DayOfWeek.Monday;
            int diff = _now.DayOfWeek - startOfWeek;
            return _now.AddDays(-(diff + 1));
        }

        private void FormatAgeStats()
        {
            //This eliminates Age cells in Rows 20-21 displaying as 12pt. This is weird because
            //the template shows those cells as 10pt.
            WorkSheet.Cells[ROW_AGE, 1, ROW_AGE + 7, 1].StyleID = WorkSheet.Cells[ROW_AGE, 1].StyleID;

            //center every column except Age
            WorkSheet.Cells[ROW_AGE, 2, ROW_AGE + 7, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            //Apply formatting to View Rate
            WorkSheet.Cells[ROW_AGE, 4, ROW_AGE + 7, 4].Style.Numberformat.Format = PERCENTAGE_FORMAT;

            //Apply comma formatted number to Impressions/Views 
            WorkSheet.Cells[ROW_AGE, 2, ROW_AGE + 7, 3].Style.Numberformat.Format = INT_FORMAT;

            //Apply formatting to CTR
            WorkSheet.Cells[ROW_AGE, 6, ROW_AGE + 7, 6].Style.Numberformat.Format = PERCENTAGE_FORMAT;

            //Apply comma formatted number to Clicks 
            WorkSheet.Cells[ROW_AGE, 5, ROW_AGE + 7, 5].Style.Numberformat.Format = INT_FORMAT;

            //Apply formate to Avg CPCV 
            WorkSheet.Cells[ROW_AGE, 7, ROW_AGE + 7, 7].Style.Numberformat.Format = MONEY_FORMAT;
        }

        private void FormatAdStats(int rowStart, int rowEnd)
        {
            AdStatsWorkSheet.Cells[rowStart, 1, rowEnd, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            AdStatsWorkSheet.Cells[rowStart, 2, rowEnd, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            AdStatsWorkSheet.Cells[rowStart, 2, rowEnd, 3].Style.Numberformat.Format = INT_FORMAT;
            AdStatsWorkSheet.Cells[rowStart, 4, rowEnd, 4].Style.Numberformat.Format = PERCENTAGE_FORMAT;
            AdStatsWorkSheet.Cells[rowStart, 5, rowEnd, 5].Style.Numberformat.Format = INT_FORMAT;
            AdStatsWorkSheet.Cells[rowStart, 6, rowEnd, 10].Style.Numberformat.Format = PERCENTAGE_FORMAT;
            AdStatsWorkSheet.Cells[rowStart, 11, rowEnd, 11].Style.Numberformat.Format = MONEY_FORMAT;
            AdStatsWorkSheet.Cells[rowStart, 12, rowEnd, 12].Style.Numberformat.Format = MONEY_FORMAT;
        }

        private decimal GetActualDailySpendToDate()
        {
            DateTime cutOffDate = _data.CampaignEndDate > _now.AddDays(-1) ? _now.AddDays(-1) : _data.CampaignEndDate;
            decimal projectedSpend = GetProjectedSpendToDate(_data.BudgetGroupTimedBudgetGroups, cutOffDate);
            decimal totalDailySpend = _data.ActualDailySpend.Where(ds => ds.StatDate <= cutOffDate).Sum(ds => ds.TotalSpend);            
            return projectedSpend == 0 ? 0 : totalDailySpend / projectedSpend;    
        }
        
        private decimal GetActualDailySpendPreviousWeek()
        {
            DateTime mondayOfLastWeek = _now.AddDays( -(int)_now.DayOfWeek - 6 );
            DateTime sundayOfLastWeek = mondayOfLastWeek.AddDays(6);
            decimal projectedSpend = GetProjectedSpendForTimeSpan(_data.BudgetGroupTimedBudgetGroups, mondayOfLastWeek.Date, sundayOfLastWeek.Date);
            List<ActualDailySpend> filteredDailySpend = _data.ActualDailySpend.Where(ds => ds.StatDate.Date >= mondayOfLastWeek.Date && ds.StatDate.Date <= sundayOfLastWeek.Date).ToList();
            decimal totalDailySpend = filteredDailySpend.Sum(ds => ds.TotalSpend);
            return projectedSpend == 0 ? 0 : totalDailySpend / projectedSpend;    
        }

        private decimal GetProjectedSpendToDate(List<BudgetGroupTimedBudgetGroup> timedBudgetGroups, DateTime cutOffDate)
        {
            decimal projectedToDateSpend = 0;

            if (this._data.CampaignStartDate >= _now)
                return 0;
            
            IEnumerable<BudgetGroupTimedBudgetGroup> activeTimedBudgetGroups = timedBudgetGroups.Where(tbg => !(tbg.StartDate >= cutOffDate));
            
            activeTimedBudgetGroups.ForEach(tbg =>
            {
                if (tbg.EndDate > _now.AddDays(-1))
                {
                    int totalNumberOfDays = (tbg.EndDate - tbg.StartDate).Days + 1;
                    int totalDaysInPeriod = totalNumberOfDays - ((tbg.EndDate - _now).Days + 1);
                    projectedToDateSpend += ((decimal)totalDaysInPeriod/totalNumberOfDays) * tbg.BudgetAmount;
                }
                else
                {
                    projectedToDateSpend += tbg.BudgetAmount;
                }
            });
                       
            return projectedToDateSpend;
        }

        private decimal GetProjectedSpendForTimeSpan(List<BudgetGroupTimedBudgetGroup> timedBudgetGroups, DateTime spanBegin, DateTime spanEnd)
        {
            decimal totalSpend = 0;

            IEnumerable<BudgetGroupTimedBudgetGroup> activeTimedBudgetGroups = timedBudgetGroups.Where(tbg => !(tbg.EndDate < spanBegin || tbg.StartDate > spanEnd));
            activeTimedBudgetGroups.ForEach(tbg =>
            {                
                if (tbg.StartDate.Date >= spanBegin && tbg.EndDate.Date <= spanEnd)//if timedBudgetGroup dates are entirely contained in span
                {
                    //then use the entire budget
                    totalSpend += tbg.BudgetAmount;                    
                }
                else
                {
                    int spanDays = (spanEnd - spanBegin).Days + 1;
                    int budgetTimedBudgetDays = (tbg.EndDate.Date - tbg.StartDate.Date).Days + 1;
                    
                    if (tbg.StartDate.Date < spanBegin && tbg.EndDate.Date > spanEnd)
                    {
                        totalSpend += ((decimal)spanDays / budgetTimedBudgetDays) * tbg.BudgetAmount;
                    } else if (tbg.StartDate.Date < spanBegin)
                    {
                        int daysInSpan = (tbg.EndDate.Date - spanBegin).Days + 1;
                        totalSpend += ((decimal)daysInSpan / budgetTimedBudgetDays) * tbg.BudgetAmount;
                    }
                    else
                    {
                        int daysInSpan = (spanEnd - tbg.StartDate.Date).Days + 1;
                        totalSpend += ((decimal)daysInSpan / budgetTimedBudgetDays) * tbg.BudgetAmount;    
                    }
                }
            });
            return totalSpend;
        }

        #endregion

        private class Range
        {
            public int TopRow { get; set; }
            public int LeftColumn { get; set; }
            public int BottomRow { get; set; }
            public int RightColumn { get; set; }
        }

        private class Position
        {
            public int TopRow { get; set; }
            public int TopIntOffset { get; set; }
            public int LeftColumn { get; set; }
            public int LeftIntOffset { get; set; }
        }
    }
}