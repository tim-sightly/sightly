﻿using System;
using System.IO;
using System.Reflection;
using OfficeOpenXml;

namespace Sightly.ReportGeneration.Generators
{
    public abstract class AExcelReport
    {
        public AExcelReport(string qualifiedName)
        {
            QualifiedName = qualifiedName;

            Assembly assembly = Assembly.GetExecutingAssembly();
            using (Stream stream = assembly.GetManifestResourceStream(QualifiedName))
            {
                MemoryStream ms = new MemoryStream();
                stream.CopyTo(ms);
                Report = new ExcelPackage(ms);

                if (Report?.Workbook == null || Report.Workbook.Worksheets.Count == 0)
                    throw new Exception($"Unable to open excel worksheet template: {QualifiedName}.");
            }
        }

        public AExcelReport()
        {
            Report = new ExcelPackage();
            Report.Workbook.Worksheets.Add("Worksheet1");
        }

        protected string QualifiedName { get; set; }
        protected ExcelPackage Report { get; }
        protected ExcelWorksheet WorkSheet => Report.Workbook.Worksheets[1];

        public virtual MemoryStream ToStream()
        {
            return new MemoryStream(Report.GetAsByteArray());
        }
    }
}