﻿using System;
using System.Drawing;
using System.Linq;
using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using OfficeOpenXml.Style;
using Reporting.DAL.DTO;
using Sightly.ReportGeneration.Generators.Interface;

namespace Sightly.ReportGeneration.Generators
{
    public class Performance_ViewExcel : AExcelReport, IExcelReport
    {

        #region constants

        private const int ROW_OVERVIEW = 6;
        private const int ROW_HIGHLIGHTS = 8;
        private const int ROW_AD = 40;
        private const int ROW_AGE = 15;
        private const int ROW_GENDER = 25;
        private const int ROW_DEVICE = 32;

        private const int AD_STATS_BASE_ROW = 40;

        private readonly Color liteGreyBackgroundColor = ColorTranslator.FromHtml("#F2F2F2");

        private readonly string INT_FORMAT = "#,##0";
        private readonly string PERCENTAGE_FORMAT = "#0.00%";
        private readonly string MONEY_FORMAT = "$0.00";

        #endregion

        #region variables

        private PerformanceReport _data;
        private DateTime _now;
        private int _rowOffset;
        private readonly string _dateFormatHeader = "MM-dd-yyyy";
        private readonly string _dateFormat = "MM/dd/yyyy";

        public string CampaignName
        {
            get
            {
                return _data.CampaignName;
            }
        }

        #endregion
        
        public Performance_ViewExcel(PerformanceReport performanceReport, DateTime? now = null)
            : base("Sightly.ReportGeneration.Templates.Excel.Performance_View.xlsx")
        {
            _data = performanceReport;
            _now = now ?? DateTime.Now;
            _rowOffset = 0;

            Build();
        }

        private void Build()
        {
            foreach (var ws in Report.Workbook.Worksheets)
            {
                BuildHeader(ws);
            }

            BuildOverview();
            BuildHighlights();
            BuildAdStats();
            BuildAgeStats();
            BuildGenderStats();
            BuildDeviceStats();
            ManipulateAgeChart();
            ManipulateGenderChart();
            ManipulateDeviceChart();
        }

        private void BuildHeader(ExcelWorksheet ws)
        {
            ws.Cells[2, 4].Value = _data.CampaignName;
            ws.Cells[3, 4].Value = $"{_data.OrderStartDate.Date.ToShortDateString()} - {_data.OrderEndDate.Date.ToShortDateString()}";
        }

        private void BuildOverview()
        {
            if (_data.CampaignToDate == null)
            {
                _data.CampaignToDate = new CampaignToDate()
                {
                    Impressions = 0,
                    Views = 0,
                    Clicks = 0,
                    TotalSpend = 0.00m
                };
            }

            WorkSheet.Cells[ROW_OVERVIEW, 2].Value = _data.CampaignToDate.Impressions;
            WorkSheet.Cells[ROW_OVERVIEW, 3].Value = _data.CampaignToDate.Views;
            WorkSheet.Cells[ROW_OVERVIEW, 4].Value = _data.CampaignToDate.ViewRate;
            WorkSheet.Cells[ROW_OVERVIEW, 5].Value = _data.CampaignToDate.AvgCPCV;
            WorkSheet.Cells[ROW_OVERVIEW, 6].Value = _data.CampaignToDate.Clicks;
            WorkSheet.Cells[ROW_OVERVIEW, 7].Value = _data.CampaignToDate.CTR;

            WorkSheet.Cells[ROW_OVERVIEW, 5].Style.Numberformat.Format = MONEY_FORMAT;
            WorkSheet.Cells[ROW_OVERVIEW, 8].Value = _data.CampaignToDate.TotalSpend;
        }

        private void BuildHighlights()
        {
            WorkSheet.Cells[ROW_HIGHLIGHTS, 2].Value = GetAgeHighlights();
            WorkSheet.Cells[ROW_HIGHLIGHTS + 1, 2].Value = GetGenderHighlights();
            WorkSheet.Cells[ROW_HIGHLIGHTS + 2, 2].Value = GetDeviceHighlights();
            WorkSheet.Cells[ROW_HIGHLIGHTS + 3, 2].Value = GetAdHighlights();
        }

        private string GetAgeHighlights()
        {
            var insightableAges = _data.AgeStats.Where(a => a.AgeGroupName != "Undetermined" && a.Views > 0);
            if (insightableAges.Any())
            {
                var highestAgeViews = insightableAges.OrderByDescending(a => a.Views).First();
                var highestAgeViewRate = insightableAges.OrderByDescending(a => a.ViewRate).First();

                if (highestAgeViews.AgeGroupName == highestAgeViewRate.AgeGroupName)
                {
                    return string.Format("Ages {0} accounted for the highest amount of views of known ages.", highestAgeViews.AgeGroupName);
                }
                else
                {
                    return string.Format("Ages {0} had the most views of known ages. Ages {1} had the greatest View Rate ({2:p}). ", highestAgeViews.AgeGroupName, highestAgeViewRate.AgeGroupName, highestAgeViewRate.ViewRate).Replace(" %", "%");
                }
            }
            return string.Empty;
        }

        private string GetGenderHighlights()
        {
            var insightableGenders = _data.GenderStats.Where(a => a.GenderName != "Undetermined" && a.Views > 0).OrderByDescending(a => a.Views);

            if (insightableGenders.Any())
            {
                var firstGender = insightableGenders.First();
                var secondGender = insightableGenders.Last();
                if (firstGender.GenderName != secondGender.GenderName)
                {
                    var totalViews = insightableGenders.Sum(a => a.Views);
                    return string.Format("{0}s accounted for {1:p} of views vs. {2}s", firstGender.GenderName, ((double)firstGender.Views / (double)totalViews), secondGender.GenderName).Replace(" %", "%");
                }
            }

            return string.Empty;
        }

        private string GetDeviceHighlights()
        {
            var eligableDevices = _data.DeviceStats.Where(d => d.Views > 0 && d.Device != "Other / Unknown");

            if (eligableDevices.Any())
            {
                var highestDeviceViews = eligableDevices.OrderByDescending(a => a.Views).First();
                var highestDeviceViewRate = eligableDevices.OrderByDescending(a => a.ViewRate).First();

                if (highestDeviceViews.Device == highestDeviceViewRate.Device)
                {
                    return string.Format("{0} had the strongest View Rate of known devices at {1:p}.", highestDeviceViews.Device, highestDeviceViews.ViewRate).Replace(" %", "%");
                }
                else
                {
                    return string.Format("{0} had the most views across devices. {1} had the best View Rate.", highestDeviceViews.Device, highestDeviceViewRate.Device);
                }
            }
            return string.Empty;
        }

        private string GetAdHighlights()
        {
            var eligableAds = _data.AdStats.Where(a => a.Views > 0);
            if (eligableAds.Any())
            {
                var highestAdViews = eligableAds.OrderByDescending(a => a.Views).First();
                var highestAdViewRate = eligableAds.OrderByDescending(a => a.ViewRate).First();

                if (highestAdViews.AdName == highestAdViewRate.AdName)
                {
                    var totalAdViews = (double)eligableAds.Sum(a => a.Views);
                    return string.Format("The {0} ad accounted for the largest percentage of views at {1:p} of total views", highestAdViews.AdName, highestAdViews.Views / totalAdViews).Replace(" %", "%");
                }
                else
                {
                    return string.Format("The {0} ad accounted for the highest number of views while the {1} ad had a higher View Rate. ", highestAdViews.AdName, highestAdViewRate.AdName);
                }
            }

            return string.Empty;
        }


        private void BuildAgeStats()
        {
            int rowCount = ROW_AGE;
            bool shadeEvenRow = rowCount % 2 != 0;

            //Age stats should not have to be formatted.
            //The template should take care of this but for some reason
            //formatting is lost when writing to cells.
            FormatAgeStats();

            _data.AgeStats.Sort((a, b) => a.AgeGroupName.CompareTo(b.AgeGroupName));

            foreach (var ageStat in _data.AgeStats)
            {
                if ((rowCount % 2 == 0) == shadeEvenRow)
                    BuildRow(ageStat.AgeGroupName, ageStat, liteGreyBackgroundColor, rowCount);
                else
                    BuildRow(ageStat.AgeGroupName, ageStat, rowCount);

                rowCount++;
            }
        }
 
        private void BuildGenderStats()
        {
            int rowCount = ROW_GENDER;

            _data.GenderStats.Where(gs => gs.GenderName == "Male").ForEach(gs => BuildRow(gs.GenderName, gs, rowCount++));
            _data.GenderStats.Where(gs => gs.GenderName == "Female").ForEach(gs => BuildRow(gs.GenderName, gs, liteGreyBackgroundColor, rowCount++));
            _data.GenderStats.Where(gs => String.Equals(gs.GenderName, "undetermined", StringComparison.OrdinalIgnoreCase) || String.Equals(gs.GenderName, "unknown", StringComparison.OrdinalIgnoreCase))
                .ForEach(gs => BuildRow(gs.GenderName, gs, rowCount++));
        }

        private void BuildDeviceStats()
        {
            int rowCount = ROW_DEVICE + _rowOffset;

            _data.DeviceStats.Where(ds => String.Equals(ds.Device, "Computers", StringComparison.OrdinalIgnoreCase)).ForEach(ds => BuildRow(ds.Device, ds, rowCount++));
            _data.DeviceStats.Where(ds => String.Equals(ds.Device, "Mobile devices with full browsers", StringComparison.OrdinalIgnoreCase)).ForEach(ds => BuildRow(ds.Device, ds, liteGreyBackgroundColor, rowCount++));
            _data.DeviceStats.Where(ds => String.Equals(ds.Device, "Tablets with full browsers", StringComparison.OrdinalIgnoreCase)).ForEach(ds => BuildRow(ds.Device, ds, rowCount++));
            _data.DeviceStats.Where(ds => String.Equals(ds.Device, "Other", StringComparison.OrdinalIgnoreCase)).ForEach(ds => BuildRow(ds.Device, ds, liteGreyBackgroundColor, rowCount++));
        }
    

        private void BuildAdStats()
        {
            int rowCount = AD_STATS_BASE_ROW;
            bool shadeEvenRow = AD_STATS_BASE_ROW % 2 != 0;

            FormatAdStats(AD_STATS_BASE_ROW, AD_STATS_BASE_ROW + _data.AdStats.Count);

            var sortedAdStats = _data.AdStats.OrderByDescending(adStat => adStat.Views);

            foreach (var adStat in sortedAdStats)
            {
                if ((rowCount % 2 == 0) == shadeEvenRow)
                    BuildAdRow(adStat, liteGreyBackgroundColor, rowCount);
                else
                    BuildAdRow(adStat, rowCount);

                rowCount++;
            }
        }

        private void ManipulateAgeChart()
        {
            int rowCount = ROW_AGE;

            Range chart3xSeries = new Range()
            {
                TopRow = rowCount,
                LeftColumn = 1,
                BottomRow = rowCount + _data.AgeStats.Count - 1,
                RightColumn = 1
            };

            Range chart3Series = new Range()
            {
                TopRow = rowCount,
                LeftColumn = 2,
                BottomRow = rowCount + _data.AgeStats.Count - 1,
                RightColumn = 2
            };

            Position chart3Position = new Position()
            {
                TopRow = rowCount - 2,
                TopIntOffset = 0,
                LeftColumn = 8,
                LeftIntOffset = 25
            };

            MovePieChart("Chart 3", chart3xSeries, chart3Series, chart3Position);
        }

        private void ManipulateGenderChart()
        {
            int rowCount = ROW_GENDER;

            Range chart4xSeries = new Range()
            {
                TopRow = rowCount,
                LeftColumn = 1,
                BottomRow = rowCount + _data.GenderStats.Count - 1,
                RightColumn = 1
            };

            Range chart4Series = new Range()
            {
                TopRow = rowCount,
                LeftColumn = 2,
                BottomRow = rowCount + _data.GenderStats.Count - 1,
                RightColumn = 2
            };

            Position chart4Position = new Position()
            {
                TopRow = rowCount - 2,
                TopIntOffset = 0,
                LeftColumn = 8,
                LeftIntOffset = 25
            };

            MovePieChart("Chart 4", chart4xSeries, chart4Series, chart4Position);
        }

        private void ManipulateDeviceChart()
        {
            int rowCount = ROW_DEVICE;

            Range chart1xSeries = new Range()
            {
                TopRow = rowCount,
                LeftColumn = 1,
                BottomRow = rowCount + _data.DeviceStats.Count - 1,
                RightColumn = 1
            };

            Range chart1Series = new Range()
            {
                TopRow = rowCount,
                LeftColumn = 2,
                BottomRow = rowCount + _data.DeviceStats.Count - 1,
                RightColumn = 2
            };

            Position chart1Position = new Position()
            {
                TopRow = rowCount - 2,
                TopIntOffset = 0,
                LeftColumn = 8,
                LeftIntOffset = 25
            };

            MoveBarChart("Chart 1", chart1xSeries, chart1Series, chart1Position);
        }

        #region Helpers

        

        private void FormatAgeStats()
        {
             //the template shows those cells as 10pt.
            WorkSheet.Cells[ROW_AGE, 1, ROW_AGE + 7, 1].StyleID = WorkSheet.Cells[ROW_AGE, 1].StyleID;

            //center every column except Age
            WorkSheet.Cells[ROW_AGE, 2, ROW_AGE + 7, 7].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            //Apply formatting to View Rate
            WorkSheet.Cells[ROW_AGE, 4, ROW_AGE + 7, 4].Style.Numberformat.Format = PERCENTAGE_FORMAT;

            //Apply comma formatted number to Impressions/Views 
            WorkSheet.Cells[ROW_AGE, 2, ROW_AGE + 7, 3].Style.Numberformat.Format = INT_FORMAT;

            //Apply formatting to CTR
            WorkSheet.Cells[ROW_AGE, 7, ROW_AGE + 7, 7].Style.Numberformat.Format = PERCENTAGE_FORMAT;

            //Apply comma formatted number to Clicks 
            WorkSheet.Cells[ROW_AGE, 6, ROW_AGE + 7, 6].Style.Numberformat.Format = INT_FORMAT;

            //Apply formate to Avg CPCV 
            WorkSheet.Cells[ROW_AGE, 5, ROW_AGE + 7, 5].Style.Numberformat.Format = MONEY_FORMAT;
        }
        private void FormatAdStats(int rowStart, int rowEnd)
        {
            WorkSheet.Cells[rowStart, 1, rowEnd, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            WorkSheet.Cells[rowStart, 2, rowEnd, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            WorkSheet.Cells[rowStart, 2, rowEnd, 3].Style.Numberformat.Format = INT_FORMAT;
            WorkSheet.Cells[rowStart, 4, rowEnd, 4].Style.Numberformat.Format = PERCENTAGE_FORMAT;
            WorkSheet.Cells[rowStart, 5, rowEnd, 5].Style.Numberformat.Format = MONEY_FORMAT;
            WorkSheet.Cells[rowStart, 6, rowEnd, 6].Style.Numberformat.Format = INT_FORMAT;
            WorkSheet.Cells[rowStart, 7, rowEnd, 7].Style.Numberformat.Format = PERCENTAGE_FORMAT;
            WorkSheet.Cells[rowStart, 8, rowEnd, 8].Style.Numberformat.Format = MONEY_FORMAT;
            WorkSheet.Cells[rowStart, 9, rowEnd, 12].Style.Numberformat.Format = PERCENTAGE_FORMAT;
        }
        private void BuildRow(string rowName, AStatRow row, int rowCount)
        {
            WorkSheet.Cells[rowCount, 2, rowCount, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            WorkSheet.Cells[rowCount, 8].Style.Numberformat.Format = "$###,###,##0.00";

            WorkSheet.Cells[rowCount, 1].Value = rowName;
            WorkSheet.Cells[rowCount, 2].Value = row.Impressions;
            WorkSheet.Cells[rowCount, 3].Value = row.Views;
            WorkSheet.Cells[rowCount, 4].Value = row.ViewRate;
            WorkSheet.Cells[rowCount, 5].Value = row.AvgCPCV;
            WorkSheet.Cells[rowCount, 6].Value = row.Clicks;
            WorkSheet.Cells[rowCount, 7].Value = row.CTR;
            WorkSheet.Cells[rowCount, 8].Value = row.TotalSpend;

        }

        private void BuildRow(string rowName, AStatRow row, Color backgroundColor, int rowCount)
        {
            WorkSheet.Cells[rowCount, 1, rowCount, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
            WorkSheet.Cells[rowCount, 1, rowCount, 8].Style.Fill.BackgroundColor.SetColor(backgroundColor);
            WorkSheet.Cells[rowCount, 2, rowCount, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            //WorkSheet.Cells[rowCount, 2, rowCount, 3].Style.Numberformat.Format = INT_FORMAT;
            //WorkSheet.Cells[rowCount, 5, rowCount, 5].Style.Numberformat.Format = INT_FORMAT;
            //WorkSheet.Cells[rowCount, 7, rowCount, 8].Style.Numberformat.Format = "$###,###,##0.00";
            WorkSheet.Cells[rowCount, 8].Style.Numberformat.Format = "$###,###,##0.00";


            WorkSheet.Cells[rowCount, 1].Value = rowName;
            WorkSheet.Cells[rowCount, 2].Value = row.Impressions;
            WorkSheet.Cells[rowCount, 3].Value = row.Views;
            WorkSheet.Cells[rowCount, 4].Value = row.ViewRate;
            WorkSheet.Cells[rowCount, 5].Value = row.AvgCPCV;
            WorkSheet.Cells[rowCount, 6].Value = row.Clicks;
            WorkSheet.Cells[rowCount, 7].Value = row.CTR;
            WorkSheet.Cells[rowCount, 8].Value = row.TotalSpend;
        }
        private void BuildAdRow(AdStats adStats, int rowCount)
        {
            WorkSheet.Cells[rowCount, 1].Value = adStats.AdName;
            WorkSheet.Cells[rowCount, 2].Value = adStats.Impressions;
            WorkSheet.Cells[rowCount, 3].Value = adStats.Views;
            WorkSheet.Cells[rowCount, 4].Value = adStats.ViewRate;
            WorkSheet.Cells[rowCount, 5].Value = adStats.AvgCPCV;
            WorkSheet.Cells[rowCount, 6].Value = adStats.Clicks;
            WorkSheet.Cells[rowCount, 7].Value = adStats.CTR;
            WorkSheet.Cells[rowCount, 8].Value = adStats.TotalSpend;
            WorkSheet.Cells[rowCount, 9].Value = adStats.Q25;
            WorkSheet.Cells[rowCount, 10].Value = adStats.Q50;
            WorkSheet.Cells[rowCount, 11].Value = adStats.Q75;
            WorkSheet.Cells[rowCount, 12].Value = adStats.Q100;
        }


        private void BuildAdRow(AdStats adStats, Color backgroundColor, int rowCount)
        {
            WorkSheet.Cells[rowCount, 1, rowCount, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
            WorkSheet.Cells[rowCount, 1, rowCount, 12].Style.Fill.BackgroundColor.SetColor(backgroundColor);

            WorkSheet.Cells[rowCount, 1].Value = adStats.AdName;
            WorkSheet.Cells[rowCount, 2].Value = adStats.Impressions;
            WorkSheet.Cells[rowCount, 3].Value = adStats.Views;
            WorkSheet.Cells[rowCount, 4].Value = adStats.ViewRate;
            WorkSheet.Cells[rowCount, 5].Value = adStats.AvgCPCV;
            WorkSheet.Cells[rowCount, 6].Value = adStats.Clicks;
            WorkSheet.Cells[rowCount, 7].Value = adStats.CTR;
            WorkSheet.Cells[rowCount, 8].Value = adStats.TotalSpend;
            WorkSheet.Cells[rowCount, 9].Value = adStats.Q25;
            WorkSheet.Cells[rowCount, 10].Value = adStats.Q50;
            WorkSheet.Cells[rowCount, 11].Value = adStats.Q75;
            WorkSheet.Cells[rowCount, 12].Value = adStats.Q100;
        }
        private void MovePieChart(string chartName, Range xSeries, Range series, Position position)
        {
            ExcelPieChart chart = WorkSheet.Drawings[chartName] as ExcelPieChart;
            var chartSeries = chart.Series[0];
            chartSeries.XSeries = WorkSheet.Cells[xSeries.TopRow, xSeries.LeftColumn, xSeries.BottomRow, xSeries.RightColumn].FullAddress;
            chartSeries.Series = WorkSheet.Cells[series.TopRow, series.LeftColumn, series.BottomRow, series.RightColumn].FullAddress;
            chart.SetPosition(position.TopRow, position.TopIntOffset, position.LeftColumn, position.LeftIntOffset);
        }

        private void MoveBarChart(string chartName, Range xSeries, Range series, Position position)
        {
            ExcelBarChart chart = WorkSheet.Drawings[chartName] as ExcelBarChart;
            var chartSeries = chart.Series[0];
            chartSeries.XSeries = WorkSheet.Cells[xSeries.TopRow, xSeries.LeftColumn, xSeries.BottomRow, xSeries.RightColumn].FullAddress;
            chartSeries.Series = WorkSheet.Cells[series.TopRow, series.LeftColumn, series.BottomRow, series.RightColumn].FullAddress;
            chart.SetPosition(position.TopRow, position.TopIntOffset, position.LeftColumn, position.LeftIntOffset);
        }


        #endregion

        public class Range
        {
            public int TopRow { get; set; }
            public int LeftColumn { get; set; }
            public int BottomRow { get; set; }
            public int RightColumn { get; set; }
        }

        public class Position
        {
            public int TopRow { get; set; }
            public int TopIntOffset { get; set; }
            public int LeftColumn { get; set; }
            public int LeftIntOffset { get; set; }
        }
    }
}