﻿using System;
using System.Drawing;
using System.Linq;
using ClosedXML.Excel;
using OfficeOpenXml;
using OfficeOpenXml.Drawing.Chart;
using OfficeOpenXml.Style;
using Reporting.DAL.DTO;
using Sightly.ReportGeneration.Generators.Interface;

namespace Sightly.ReportGeneration.Generators
{
    public class Performance_1KImpressionExcel : AExcelReport, IExcelReport
    {

        #region constants

        private const int ROW_OVERVIEW = 6;
        private const int ROW_HIGHLIGHTS = 8;
        private const int ROW_AD = 40;
        private const int ROW_AGE = 15;
        private const int ROW_GENDER = 25;
        private const int ROW_DEVICE = 32;

        private const int AD_STATS_BASE_ROW = 40;

        private readonly Color liteGreyBackgroundColor = ColorTranslator.FromHtml("#F2F2F2");

        private readonly string INT_FORMAT = "#,##0";
        private readonly string PERCENTAGE_FORMAT = "#0.00%";
        private readonly string MONEY_FORMAT = "$0.00";

        #endregion

        #region variables

        private PerformanceReport _data;
        private DateTime _now;
        private int _rowOffset;
        private readonly string _dateFormatHeader = "MM-dd-yyyy";
        private readonly string _dateFormat = "MM/dd/yyyy";

        public string CampaignName
        {
            get
            {
                return _data.CampaignName;
            }
        }

        #endregion


        public Performance_1KImpressionExcel(PerformanceReport performanceReport, DateTime? now = null) 
            : base("Sightly.ReportGeneration.Templates.Excel.Performance_1kImpression.xlsx")
        {
            _data = performanceReport;
            _now = now ?? DateTime.Now;
            _rowOffset = 0;

            Build();
        }



        private void Build()
        {
            foreach (var ws in Report.Workbook.Worksheets)
            {
                BuildHeader(ws);
            }

            BuildOverview();
            BuildHighlights();
            BuildAdStats();
            BuildAgeStats();
            BuildGenderStats();
            BuildDeviceStats();
            ManipulateAgeChart();
            ManipulateGenderChart();
            ManipulateDeviceChart();
        }

        private void BuildHeader(ExcelWorksheet ws)
        {
            ws.Cells[2, 4].Value = _data.CampaignName;
            ws.Cells[3, 4].Value = $"{_data.OrderStartDate.Date.ToShortDateString()} - {_data.OrderEndDate.Date.ToShortDateString()}";
        }

        private void BuildOverview()
        {
            if (_data.CampaignToDate == null)
            {
                _data.CampaignToDate = new CampaignToDate()
                {
                    Impressions = 0,
                    Views = 0,
                    Clicks = 0,
                    TotalSpend = 0.00m
                };
            }

            WorkSheet.Cells[ROW_OVERVIEW, 2].Value = _data.CampaignToDate.Impressions;
            WorkSheet.Cells[ROW_OVERVIEW, 3].Value = _data.CampaignToDate.Ecpm;
            WorkSheet.Cells[ROW_OVERVIEW, 4].Value = _data.CampaignToDate.Clicks;
            WorkSheet.Cells[ROW_OVERVIEW, 5].Value = _data.CampaignToDate.CTR;

            WorkSheet.Cells[ROW_OVERVIEW, 6].Style.Numberformat.Format = MONEY_FORMAT;
            WorkSheet.Cells[ROW_OVERVIEW, 6].Value = _data.CampaignToDate.TotalSpend;
        }

        private void BuildHighlights()
        {
            WorkSheet.Cells[ROW_HIGHLIGHTS, 2].Value = GetAgeHighlights();
            WorkSheet.Cells[ROW_HIGHLIGHTS + 1, 2].Value = GetGenderHighlights();
            WorkSheet.Cells[ROW_HIGHLIGHTS + 2, 2].Value = GetDeviceHighlights();
            WorkSheet.Cells[ROW_HIGHLIGHTS + 3, 2].Value = GetAdHighlights();
        }

        private string GetAgeHighlights()
        {
            var insightableAges = _data.AgeStats.Where(a => a.AgeGroupName != "Undetermined" && a.Impressions > 0);
            if (!insightableAges.Any()) return string.Empty;

            var highestAgeImpressions = insightableAges.OrderByDescending(a => a.Impressions).First();

            return $"Ages {highestAgeImpressions.AgeGroupName} had the most impressions of known ages. ";
            
        }

        private string GetGenderHighlights()
        {
            var insightableGenders = _data.GenderStats.Where(a => a.GenderName != "Undetermined" && a.Impressions > 0).OrderByDescending(a => a.Views);

            if (insightableGenders.Any())
            {
                var firstGender = insightableGenders.First();
                var secondGender = insightableGenders.Last();
                if (firstGender.GenderName == secondGender.GenderName) return string.Empty;
                return
                    $"{firstGender.GenderName}s accounted for {firstGender.Impressions} of impressions vs. {secondGender.GenderName}s";
            }

            return string.Empty;
        }

        private string GetDeviceHighlights()
        {
            var eligableDevices = _data.DeviceStats.Where(d => d.Impressions > 0 && d.Device != "Other / Unknown");

            if (!eligableDevices.Any()) return string.Empty;
            var highestDeviceViews = eligableDevices.OrderByDescending(a => a.Impressions).First();

            return $"{highestDeviceViews.Device} had the most impressions across devices. ";
        }

        private string GetAdHighlights()
        {
            var eligableAds = _data.AdStats.Where(a => a.Impressions > 0);
            if (!eligableAds.Any()) return string.Empty;

            var highestAdVImpressions = eligableAds.OrderByDescending(a => a.Impressions).First();
                
            return $"The {highestAdVImpressions.AdName} ad accounted for the highest number of impressions. ";
        }


        private void BuildAgeStats()
        {
            int rowCount = ROW_AGE;
            bool shadeEvenRow = rowCount % 2 != 0;

            //Age stats should not have to be formatted.
            //The template should take care of this but for some reason
            //formatting is lost when writing to cells.
            FormatAgeStats();

            _data.AgeStats.Sort((a, b) => a.AgeGroupName.CompareTo(b.AgeGroupName));

            foreach (var ageStat in _data.AgeStats)
            {
                if ((rowCount % 2 == 0) == shadeEvenRow)
                    BuildRow(ageStat.AgeGroupName, ageStat, liteGreyBackgroundColor, rowCount);
                else
                    BuildRow(ageStat.AgeGroupName, ageStat, rowCount);

                rowCount++;
            }
        }

        private void BuildGenderStats()
        {
            int rowCount = ROW_GENDER;

            _data.GenderStats.Where(gs => gs.GenderName == "Male").ForEach(gs => BuildRow(gs.GenderName, gs, rowCount++));
            _data.GenderStats.Where(gs => gs.GenderName == "Female").ForEach(gs => BuildRow(gs.GenderName, gs, liteGreyBackgroundColor, rowCount++));
            _data.GenderStats.Where(gs => String.Equals(gs.GenderName, "undetermined", StringComparison.OrdinalIgnoreCase) || String.Equals(gs.GenderName, "unknown", StringComparison.OrdinalIgnoreCase))
                .ForEach(gs => BuildRow(gs.GenderName, gs, rowCount++));
        }

        private void BuildDeviceStats()
        {
            int rowCount = ROW_DEVICE + _rowOffset;

            _data.DeviceStats.Where(ds => String.Equals(ds.Device, "Computers", StringComparison.OrdinalIgnoreCase)).ForEach(ds => BuildRow(ds.Device, ds, rowCount++));
            _data.DeviceStats.Where(ds => String.Equals(ds.Device, "Mobile devices with full browsers", StringComparison.OrdinalIgnoreCase)).ForEach(ds => BuildRow(ds.Device, ds, liteGreyBackgroundColor, rowCount++));
            _data.DeviceStats.Where(ds => String.Equals(ds.Device, "Tablets with full browsers", StringComparison.OrdinalIgnoreCase)).ForEach(ds => BuildRow(ds.Device, ds, rowCount++));
            _data.DeviceStats.Where(ds => String.Equals(ds.Device, "Other", StringComparison.OrdinalIgnoreCase)).ForEach(ds => BuildRow(ds.Device, ds, liteGreyBackgroundColor, rowCount++));
        }


        private void BuildAdStats()
        {
            int rowCount = AD_STATS_BASE_ROW;
            bool shadeEvenRow = AD_STATS_BASE_ROW % 2 != 0;

            FormatAdStats(AD_STATS_BASE_ROW, AD_STATS_BASE_ROW + _data.AdStats.Count);

            var sortedAdStats = _data.AdStats.OrderByDescending(adStat => adStat.Views);

            foreach (var adStat in sortedAdStats)
            {
                if ((rowCount % 2 == 0) == shadeEvenRow)
                    BuildAdRow(adStat, liteGreyBackgroundColor, rowCount);
                else
                    BuildAdRow(adStat, rowCount);

                rowCount++;
            }
        }

        private void ManipulateAgeChart()
        {
            int rowCount = ROW_AGE;

            Performance_ViewExcel.Range chart3xSeries = new Performance_ViewExcel.Range()
            {
                TopRow = rowCount,
                LeftColumn = 1,
                BottomRow = rowCount + _data.AgeStats.Count - 1,
                RightColumn = 1
            };

            Performance_ViewExcel.Range chart3Series = new Performance_ViewExcel.Range()
            {
                TopRow = rowCount,
                LeftColumn = 2,
                BottomRow = rowCount + _data.AgeStats.Count - 1,
                RightColumn = 2
            };

            Performance_ViewExcel.Position chart3Position = new Performance_ViewExcel.Position()
            {
                TopRow = rowCount - 2,
                TopIntOffset = 0,
                LeftColumn = 8,
                LeftIntOffset = 25
            };

            MovePieChart("Chart 3", chart3xSeries, chart3Series, chart3Position);
        }

        private void ManipulateGenderChart()
        {
            int rowCount = ROW_GENDER;

            Performance_ViewExcel.Range chart4xSeries = new Performance_ViewExcel.Range()
            {
                TopRow = rowCount,
                LeftColumn = 1,
                BottomRow = rowCount + _data.GenderStats.Count - 1,
                RightColumn = 1
            };

            Performance_ViewExcel.Range chart4Series = new Performance_ViewExcel.Range()
            {
                TopRow = rowCount,
                LeftColumn = 2,
                BottomRow = rowCount + _data.GenderStats.Count - 1,
                RightColumn = 2
            };

            Performance_ViewExcel.Position chart4Position = new Performance_ViewExcel.Position()
            {
                TopRow = rowCount - 2,
                TopIntOffset = 0,
                LeftColumn = 8,
                LeftIntOffset = 25
            };

            MovePieChart("Chart 4", chart4xSeries, chart4Series, chart4Position);
        }

        private void ManipulateDeviceChart()
        {
            int rowCount = ROW_DEVICE;

            Performance_ViewExcel.Range chart1xSeries = new Performance_ViewExcel.Range()
            {
                TopRow = rowCount,
                LeftColumn = 1,
                BottomRow = rowCount + _data.DeviceStats.Count - 1,
                RightColumn = 1
            };

            Performance_ViewExcel.Range chart1Series = new Performance_ViewExcel.Range()
            {
                TopRow = rowCount,
                LeftColumn = 2,
                BottomRow = rowCount + _data.DeviceStats.Count - 1,
                RightColumn = 2
            };

            Performance_ViewExcel.Position chart1Position = new Performance_ViewExcel.Position()
            {
                TopRow = rowCount - 2,
                TopIntOffset = 0,
                LeftColumn = 8,
                LeftIntOffset = 25
            };

            MoveBarChart("Chart 1", chart1xSeries, chart1Series, chart1Position);
        }

        #region Helpers



        private void FormatAgeStats()
        {
            //the template shows those cells as 10pt.
            WorkSheet.Cells[ROW_AGE, 1, ROW_AGE + 7, 1].StyleID = WorkSheet.Cells[ROW_AGE, 1].StyleID;

            //center every column except Age
            WorkSheet.Cells[ROW_AGE, 2, ROW_AGE + 7, 6].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            //Apply formatting to View Rate
            WorkSheet.Cells[ROW_AGE, 3, ROW_AGE + 7, 3].Style.Numberformat.Format = PERCENTAGE_FORMAT;

            //Apply comma formatted number to Impressions/Views 
            WorkSheet.Cells[ROW_AGE, 2, ROW_AGE + 7, 2].Style.Numberformat.Format = INT_FORMAT;

            //Apply formatting to CTR
            WorkSheet.Cells[ROW_AGE, 5, ROW_AGE + 7, 5].Style.Numberformat.Format = PERCENTAGE_FORMAT;

            //Apply comma formatted number to Clicks 
            WorkSheet.Cells[ROW_AGE, 4, ROW_AGE + 7, 4].Style.Numberformat.Format = INT_FORMAT;

            //Apply formate to Avg CPM 
            WorkSheet.Cells[ROW_AGE, 3, ROW_AGE + 7, 3].Style.Numberformat.Format = MONEY_FORMAT;
        }
        private void FormatAdStats(int rowStart, int rowEnd)
        {
            WorkSheet.Cells[rowStart, 1, rowEnd, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            WorkSheet.Cells[rowStart, 2, rowEnd, 11].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            WorkSheet.Cells[rowStart, 2, rowEnd, 2].Style.Numberformat.Format = INT_FORMAT;
            WorkSheet.Cells[rowStart, 3, rowEnd, 3].Style.Numberformat.Format = MONEY_FORMAT;
            WorkSheet.Cells[rowStart, 4, rowEnd, 4].Style.Numberformat.Format = INT_FORMAT;
            WorkSheet.Cells[rowStart, 5, rowEnd, 5].Style.Numberformat.Format = PERCENTAGE_FORMAT;
            WorkSheet.Cells[rowStart, 6, rowEnd, 6].Style.Numberformat.Format = MONEY_FORMAT;
            WorkSheet.Cells[rowStart, 7, rowEnd, 10].Style.Numberformat.Format = PERCENTAGE_FORMAT;
        }
        private void BuildRow(string rowName, AStatRow row, int rowCount)
        {
            WorkSheet.Cells[rowCount, 2, rowCount, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
            WorkSheet.Cells[rowCount, 6].Style.Numberformat.Format = "$###,###,##0.00";

            WorkSheet.Cells[rowCount, 1].Value = rowName;
            WorkSheet.Cells[rowCount, 2].Value = row.Impressions;
            WorkSheet.Cells[rowCount, 3].Value = row.Ecpm;
            WorkSheet.Cells[rowCount, 4].Value = row.Clicks;
            WorkSheet.Cells[rowCount, 5].Value = row.CTR;
            WorkSheet.Cells[rowCount, 6].Value = row.TotalSpend;

        }

        private void BuildRow(string rowName, AStatRow row, Color backgroundColor, int rowCount)
        {
            WorkSheet.Cells[rowCount, 1, rowCount, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
            WorkSheet.Cells[rowCount, 1, rowCount, 8].Style.Fill.BackgroundColor.SetColor(backgroundColor);
            WorkSheet.Cells[rowCount, 2, rowCount, 8].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

            //WorkSheet.Cells[rowCount, 2, rowCount, 3].Style.Numberformat.Format = INT_FORMAT;
            //WorkSheet.Cells[rowCount, 5, rowCount, 5].Style.Numberformat.Format = INT_FORMAT;
            //WorkSheet.Cells[rowCount, 7, rowCount, 8].Style.Numberformat.Format = "$###,###,##0.00";
            WorkSheet.Cells[rowCount, 6].Style.Numberformat.Format = "$###,###,##0.00";

            WorkSheet.Cells[rowCount, 1].Value = rowName;
            WorkSheet.Cells[rowCount, 2].Value = row.Impressions;
            WorkSheet.Cells[rowCount, 3].Value = row.Ecpm;
            WorkSheet.Cells[rowCount, 4].Value = row.Clicks;
            WorkSheet.Cells[rowCount, 5].Value = row.CTR;
            WorkSheet.Cells[rowCount, 6].Value = row.TotalSpend;
        }
        private void BuildAdRow(AdStats adStats, int rowCount)
        {
            WorkSheet.Cells[rowCount, 1].Value = adStats.AdName;
            WorkSheet.Cells[rowCount, 2].Value = adStats.Impressions;
            WorkSheet.Cells[rowCount, 3].Value = adStats.Ecpm;
            WorkSheet.Cells[rowCount, 4].Value = adStats.Clicks;
            WorkSheet.Cells[rowCount, 5].Value = adStats.CTR;
            WorkSheet.Cells[rowCount, 6].Value = adStats.TotalSpend;
            WorkSheet.Cells[rowCount, 7].Value = adStats.Q25;
            WorkSheet.Cells[rowCount, 8].Value = adStats.Q50;
            WorkSheet.Cells[rowCount, 9].Value = adStats.Q75;
            WorkSheet.Cells[rowCount, 10].Value = adStats.Q100;
        }


        private void BuildAdRow(AdStats adStats, Color backgroundColor, int rowCount)
        {
            WorkSheet.Cells[rowCount, 1, rowCount, 12].Style.Fill.PatternType = ExcelFillStyle.Solid;
            WorkSheet.Cells[rowCount, 1, rowCount, 12].Style.Fill.BackgroundColor.SetColor(backgroundColor);

            WorkSheet.Cells[rowCount, 1].Value = adStats.AdName;
            WorkSheet.Cells[rowCount, 2].Value = adStats.Impressions;
            WorkSheet.Cells[rowCount, 3].Value = adStats.Ecpm;
            WorkSheet.Cells[rowCount, 4].Value = adStats.Clicks;
            WorkSheet.Cells[rowCount, 5].Value = adStats.CTR;
            WorkSheet.Cells[rowCount, 6].Value = adStats.TotalSpend;
            WorkSheet.Cells[rowCount, 7].Value = adStats.Q25;
            WorkSheet.Cells[rowCount, 8].Value = adStats.Q50;
            WorkSheet.Cells[rowCount, 9].Value = adStats.Q75;
            WorkSheet.Cells[rowCount, 10].Value = adStats.Q100;
        }
        private void MovePieChart(string chartName, Performance_ViewExcel.Range xSeries, Performance_ViewExcel.Range series, Performance_ViewExcel.Position position)
        {
            ExcelPieChart chart = WorkSheet.Drawings[chartName] as ExcelPieChart;
            var chartSeries = chart.Series[0];
            chartSeries.XSeries = WorkSheet.Cells[xSeries.TopRow, xSeries.LeftColumn, xSeries.BottomRow, xSeries.RightColumn].FullAddress;
            chartSeries.Series = WorkSheet.Cells[series.TopRow, series.LeftColumn, series.BottomRow, series.RightColumn].FullAddress;
            chart.SetPosition(position.TopRow, position.TopIntOffset, position.LeftColumn, position.LeftIntOffset);
        }

        private void MoveBarChart(string chartName, Performance_ViewExcel.Range xSeries, Performance_ViewExcel.Range series, Performance_ViewExcel.Position position)
        {
            ExcelBarChart chart = WorkSheet.Drawings[chartName] as ExcelBarChart;
            var chartSeries = chart.Series[0];
            chartSeries.XSeries = WorkSheet.Cells[xSeries.TopRow, xSeries.LeftColumn, xSeries.BottomRow, xSeries.RightColumn].FullAddress;
            chartSeries.Series = WorkSheet.Cells[series.TopRow, series.LeftColumn, series.BottomRow, series.RightColumn].FullAddress;
            chart.SetPosition(position.TopRow, position.TopIntOffset, position.LeftColumn, position.LeftIntOffset);
        }


        #endregion
        private class Range
        {
            public int TopRow { get; set; }
            public int LeftColumn { get; set; }
            public int BottomRow { get; set; }
            public int RightColumn { get; set; }
        }

        private class Position
        {
            public int TopRow { get; set; }
            public int TopIntOffset { get; set; }
            public int LeftColumn { get; set; }
            public int LeftIntOffset { get; set; }
        }
    }
}