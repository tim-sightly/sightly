﻿using System;
using System.Drawing;
using Reporting.DAL.DTO;
using Sightly.ReportGeneration.Generators.Interface;

namespace Sightly.ReportGeneration.Generators
{
    public class ExtendedMediaBriefExcel : AExcelReport, IExcelReport
    {
        #region constants

        private readonly string INT_FORMAT = "#,##0";
        private readonly string PERCENTAGE_FORMAT = "#0.00%";
        private readonly string MONEY_FORMAT = "$0.00";
        private readonly string _dateFormat = "MM/dd/yyyy";

        #endregion

        #region variables

        private MediaBrief _data;
        private DateTime _now;
        private int _rowOffset;
        private readonly string _dateFormatHeader = "MM-dd-yyyy";
        private readonly int _locationRowOffset = 30;
        private readonly int _genderAgeRowOffset;
        private readonly int _keywordsRowOffset;
        private readonly int _householdIncomeRowOffset;
        private readonly int _adRowOffset;



        public string CampaignName => _data.OrderData.CampaignName;

        #endregion
        public ExtendedMediaBriefExcel(MediaBrief mediaBrief, DateTime? now = null)
            : base("Sightly.ReportGeneration.Templates.Excel.ExtendedMediaBriefReport.xlsx")
        {
            _data = mediaBrief;
            _now = now ?? DateTime.Now;
            _rowOffset = 0;

            _genderAgeRowOffset = _locationRowOffset + _data.LocationList.Count + 3;
            _keywordsRowOffset = _genderAgeRowOffset + _data.GenderAgeList.Count + 3;
            _householdIncomeRowOffset = _keywordsRowOffset + 10;
            _adRowOffset = _householdIncomeRowOffset + _data.HouseholdIncomeList.Count + 3;

            Build();
        }

        private void Build()
        {
            BuildOrderInfo();
            BuildBudgetInfo();
            BuildLocationInfo();
            BuildGenderAgeInfo();
            BuildKeywordsInfo();
            BuildHhiInfo();
            BuildAdInfo();
        }

        private void BuildOrderInfo()
        {
            WorkSheet.Cells[2, 2].Value = DateTime.Now.Date.ToString(_dateFormat);
            WorkSheet.Cells[3, 2].Value = _data.OrderData.AccountName;
            WorkSheet.Cells[4, 2].Value = _data.OrderData.AdvertiserName;
            WorkSheet.Cells[5, 2].Value = _data.OrderData.AdvertiserCategoryName;
            WorkSheet.Cells[7, 2].Value = _data.OrderData.CampaignName;
            WorkSheet.Cells[8, 2].Value = _data.OrderData.OrderTypeName;
            WorkSheet.Cells[9, 2].Value = _data.OrderData.IOName;
            WorkSheet.Cells[10, 2].Value = _data.OrderData.OrderName;
            WorkSheet.Cells[11, 2].Value = _data.OrderData.OrderRefCode;
            WorkSheet.Cells[14, 2].Value = _data.OrderData.StartDate.Date.ToString(_dateFormat);
            WorkSheet.Cells[15, 2].Value = _data.OrderData.EndDate.Date.ToString(_dateFormat);
            WorkSheet.Cells[16, 2].Value = _data.OrderData.NumOfDays;
        }

        private void BuildBudgetInfo()
        {
            WorkSheet.Cells[19, 2].Value = _data.BudgetData.GrossBudget;
            WorkSheet.Cells[20, 2].Value = _data.BudgetData.SightlyBudget;
            WorkSheet.Cells[21, 2].Value = _data.BudgetData.CampaignBudget;
            WorkSheet.Cells[22, 2].Value = _data.BudgetData.SightlyMargin;
            WorkSheet.Cells[25, 2].Value = _data.BudgetData.DailyGrossBudget;
            WorkSheet.Cells[26, 2].Value = _data.BudgetData.DailySightlyBudget;
            WorkSheet.Cells[27, 2].Value = _data.BudgetData.DailyCampaignBudget;

        }

        private void BuildLocationInfo()
        {
            int currentRow = _locationRowOffset + 1;
            WorkSheet.Cells[_locationRowOffset, 1, _locationRowOffset, 2].Style.Font.Size = 12;
            WorkSheet.Cells[_locationRowOffset, 1, _locationRowOffset, 2].Style.Font.SetFromFont(new Font("Open Sans", 12));
            WorkSheet.Cells[_locationRowOffset, 1].Value = "Location Name";
            WorkSheet.Cells[_locationRowOffset, 2].Value = "Geography Name";

            _data.LocationList.ForEach(location =>
            {
                WorkSheet.Cells[currentRow, 1].Value = location.LocationName;
                WorkSheet.Cells[currentRow, 2].Value = location.GeographyName;

                currentRow++;
            });
        }

        private void BuildGenderAgeInfo()
        {
            int currentRow = _genderAgeRowOffset + 1;
            WorkSheet.Cells[_genderAgeRowOffset, 1, _adRowOffset, 2].Style.Font.Size = 12;
            WorkSheet.Cells[_genderAgeRowOffset, 1, _genderAgeRowOffset, 2].Style.Font.SetFromFont(new Font("Open Sans", 12));
            WorkSheet.Cells[_genderAgeRowOffset, 1].Value = "Gender Name";
            WorkSheet.Cells[_genderAgeRowOffset, 2].Value = "Age Group Name";

            _data.GenderAgeList.ForEach(ga =>
            {
                WorkSheet.Cells[currentRow, 1].Value = ga.GenderName;
                WorkSheet.Cells[currentRow, 2].Value = ga.AgeGroupName;

                currentRow++;
            });
        }

        private void BuildKeywordsInfo()
        {
            int currentRow = _keywordsRowOffset + 1;

            WorkSheet.Cells[_keywordsRowOffset, 1, _keywordsRowOffset, 1].Style.Font.Size = 12;
            WorkSheet.Cells[_keywordsRowOffset, 1, _keywordsRowOffset, 1].Style.Font.SetFromFont(new Font("Open Sans", 12));
            WorkSheet.Cells[_keywordsRowOffset, 1].Value = "Keywords";
            WorkSheet.Cells[currentRow, 1].Value = _data.KeywordUrl.KeyWords;

            currentRow += 2;
            
            WorkSheet.Cells[currentRow, 1].Value = "Custom Interest Urls";
            WorkSheet.Cells[currentRow + 1, 1].Value = _data.KeywordUrl.CompetitorsUrls;

            currentRow += 2;
            WorkSheet.Cells[currentRow, 1].Value = "Notes";
            WorkSheet.Cells[currentRow + 1, 1].Value = _data.KeywordUrl.TargetingNotes;

        }

        private void BuildHhiInfo()
        {
            int currentRow = _householdIncomeRowOffset + 1;

            WorkSheet.Cells[_householdIncomeRowOffset, 1, _adRowOffset, 9].Style.Font.Size = 12;
            WorkSheet.Cells[_householdIncomeRowOffset, 1, _adRowOffset, 9].Style.Font.SetFromFont(new Font("Open Sans", 12));
            WorkSheet.Cells[_householdIncomeRowOffset, 1].Value = "Household Incomes";

            _data.HouseholdIncomeList.ForEach(hi =>
            {
                WorkSheet.Cells[currentRow, 1].Value = hi.IncomeLabels;

                currentRow++;
            });
        }


        private void BuildAdInfo()
        {
            int currentRow = _adRowOffset + 1;

            WorkSheet.Cells[_adRowOffset, 1, _adRowOffset, 9].Style.Font.Size = 12;
            WorkSheet.Cells[_adRowOffset, 1, _adRowOffset, 9].Style.Font.SetFromFont(new Font("Open Sans", 12));
            WorkSheet.Cells[_adRowOffset, 1].Value = "Ad Name";
            WorkSheet.Cells[_adRowOffset, 2].Value = "Video Ad Asset";
            WorkSheet.Cells[_adRowOffset, 3].Value = "Video URL";
            WorkSheet.Cells[_adRowOffset, 4].Value = "Video Id";
            WorkSheet.Cells[_adRowOffset, 5].Value = "Display URL";
            WorkSheet.Cells[_adRowOffset, 6].Value = "Destination URL";
            WorkSheet.Cells[_adRowOffset, 7].Value = "Video Length in Seconds";
            WorkSheet.Cells[_adRowOffset, 8].Value = "Banner Asset Id";
            WorkSheet.Cells[_adRowOffset, 9].Value = "Companion Banner Asset";
            WorkSheet.Cells[_adRowOffset, 10].Value = "Companion Banner Url";
            WorkSheet.Cells[_adRowOffset, 11].Value = "Start Date";
            WorkSheet.Cells[_adRowOffset, 12].Value = "End Date";

            _data.AdList.ForEach(a =>
            {
                WorkSheet.Cells[currentRow, 1].Value = a.AdName;
                WorkSheet.Cells[currentRow, 2].Value = a.VideoAdAsset;
                WorkSheet.Cells[currentRow, 3].Value = a.VideoUrl;
                if (a.VideoId == null)
                {
                    WorkSheet.Cells[currentRow, 4].Value = a.VideoAdId;
                }
                else
                {
                    WorkSheet.Cells[currentRow, 4].Value = a.VideoId;
                }

                WorkSheet.Cells[currentRow, 5].Value = a.DisplayUrl;
                WorkSheet.Cells[currentRow, 6].Value = a.DestinationUrl;
                WorkSheet.Cells[currentRow, 7].Value = a.VideoLengthInSeconds;
                WorkSheet.Cells[currentRow, 8].Value = a.BannerAssetId;
                WorkSheet.Cells[currentRow, 9].Value = a.CompanionBannerAsset;
                WorkSheet.Cells[currentRow, 10].Value = a.CompanionBannerUrl;
                WorkSheet.Cells[currentRow, 11].Value = a.StartDate.ToString(_dateFormat);
                WorkSheet.Cells[currentRow, 12].Value = a.EndDate.ToString(_dateFormat);

                currentRow++;
            });
        }
    }
}