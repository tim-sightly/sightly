﻿using System;
using System.Linq;
using Reporting.DAL.DTO;
using Sightly.ReportGeneration.Generators.Interface;

namespace Sightly.ReportGeneration.Generators
{
    public class PerformanceSummaryReportExcel : AExcelReport, IExcelReport
    {
        #region constants

        private const int ROW_CAMPAIGN = 9;

        private readonly string INT_FORMAT = "#,##0";
        private readonly string PERCENTAGE_FORMAT = "#0.00%";
        private readonly string MONEY_FORMAT = "$0.00";

        #endregion

        #region variables

        private PerformanceSummaryReport _data;
        private DateTime _now;
        private int _rowOffset;
        private readonly string _dateFormatHeader = "MM-dd-yyyy";
        private readonly string _dateFormat = "MM/dd/yyyy";


        #endregion

        public PerformanceSummaryReportExcel(PerformanceSummaryReport performanceSummaryReport, DateTime? now = null)
            : base("Sightly.ReportGeneration.Templates.Excel.PerformanceSummaryReport.xlsx")
        {
            _data = performanceSummaryReport;
            _now = now ?? DateTime.Now;
            _rowOffset = 0;

            BuildHeader();
            BuildOrderStats();
            BuildTotals();
        }

        private void BuildHeader()
        {
            WorkSheet.Cells[1, 2].Value = _now.ToString(_dateFormat);
            WorkSheet.Cells[2, 2].Value = _data.RequestedStartDate.ToString(_dateFormat);
            WorkSheet.Cells[3, 2].Value = _data.RequestedEndDate.ToString(_dateFormat);
            WorkSheet.Cells[4, 2].Value = _data.AvailableStartDate.ToString(_dateFormat);
            WorkSheet.Cells[5, 2].Value = _data.AvailableEndDate.ToString(_dateFormat);
        }

        private void BuildOrderStats()
        {
            var orderRow = ROW_CAMPAIGN;
            _data.OrderInfoStats.ForEach(ois =>
            {
                WorkSheet.Cells[orderRow, 1].Value = ois.OrderName;
                WorkSheet.Cells[orderRow, 2].Value = ois.Status;
                WorkSheet.Cells[orderRow, 3].Value = ois.OrderRefCode;
                WorkSheet.Cells[orderRow, 4].Value = ois.AdvertiserName;
                WorkSheet.Cells[orderRow, 5].Value = ois.CampaignName;
                WorkSheet.Cells[orderRow, 6].Value = ois.OrderStartDate.ToString(_dateFormat);
                WorkSheet.Cells[orderRow, 7].Value = ois.OrderEndDate.ToString(_dateFormat);
                WorkSheet.Cells[orderRow, 8].Style.Numberformat.Format = MONEY_FORMAT;
                WorkSheet.Cells[orderRow, 8].Value = ois.OrderBudget;
                WorkSheet.Cells[orderRow, 9].Value = ois.AvailableStartDate.ToString(_dateFormat);
                WorkSheet.Cells[orderRow, 10].Value = ois.AvailableEndDate.ToString(_dateFormat);
                WorkSheet.Cells[orderRow, 11].Value = ois.AvailableDays;
                WorkSheet.Cells[orderRow, 12].Value = ois.StatDays;
                WorkSheet.Cells[orderRow, 13].Style.Numberformat.Format = MONEY_FORMAT;
                WorkSheet.Cells[orderRow, 13].Value = ois.Spend;
                WorkSheet.Cells[orderRow, 14].Value = ois.Impressions;
                WorkSheet.Cells[orderRow, 15].Value = ois.CompletedViews;
                WorkSheet.Cells[orderRow, 16].Style.Numberformat.Format = PERCENTAGE_FORMAT;
                WorkSheet.Cells[orderRow, 16].Value = ois.ViewRate;
                WorkSheet.Cells[orderRow, 17].Value = ois.ViewTime;
                WorkSheet.Cells[orderRow, 18].Style.Numberformat.Format = MONEY_FORMAT;
                WorkSheet.Cells[orderRow, 18].Value = ois.CPV;
                WorkSheet.Cells[orderRow, 19].Style.Numberformat.Format = PERCENTAGE_FORMAT;
                WorkSheet.Cells[orderRow, 19].Value = ois.Reach;
                WorkSheet.Cells[orderRow, 20].Value = ois.Clicks;
                WorkSheet.Cells[orderRow, 21].Style.Numberformat.Format = PERCENTAGE_FORMAT;
                WorkSheet.Cells[orderRow, 21].Value = ois.CTR;


                orderRow++;
            });

            _rowOffset = orderRow;
        }

        private void BuildTotals()
        {
            var totalRow = _rowOffset;

            WorkSheet.Cells[totalRow, 1].Value = "TOTAL";
            WorkSheet.Cells[totalRow, 13].Style.Numberformat.Format = MONEY_FORMAT;
            WorkSheet.Cells[totalRow, 13].Value= _data.OrderInfoStats.Sum(x => x.Spend);
            WorkSheet.Cells[totalRow, 14].Value = _data.OrderInfoStats.Sum(x => x.Impressions);
            WorkSheet.Cells[totalRow, 15].Value = _data.OrderInfoStats.Sum(x => x.CompletedViews);
            WorkSheet.Cells[totalRow, 16].Style.Numberformat.Format = PERCENTAGE_FORMAT;
            WorkSheet.Cells[totalRow, 16].Value = _data.OrderInfoStats.Average(x => x.ViewRate);
            WorkSheet.Cells[totalRow, 17].Value = _data.OrderInfoStats.Sum(x => x.ViewTime);
            WorkSheet.Cells[totalRow, 18].Style.Numberformat.Format = MONEY_FORMAT;
            WorkSheet.Cells[totalRow, 18].Value = _data.OrderInfoStats.Average(x => x.CPV);
            WorkSheet.Cells[totalRow, 20].Value = _data.OrderInfoStats.Sum(x => x.Clicks);
            WorkSheet.Cells[totalRow, 21].Style.Numberformat.Format = PERCENTAGE_FORMAT;
            WorkSheet.Cells[totalRow, 21].Value = _data.OrderInfoStats.Average(x => x.CTR);

        }
    
    }
}