﻿using Sightly.ReportGeneration.Generators.Interface;
using Sightly.ReportGeneration.Models;

namespace Sightly.ReportGeneration.Generators
{
    public class LatLongExportExcel : AExcelReport, IExcelReport
    {
        public LatLongExportExcel(LatLongReportRequest latLongReportRequest) : base("Sightly.ReportGeneration.Templates.Excel.LatLongReport.xlsx")
        {
            _latLongCollection = latLongReportRequest;

            Build();
        }

        private void Build()
        {
            var counter = 1;
            _latLongCollection.Places.ForEach(row =>
            {
                counter++;
                WorkSheet.Cells[counter, 1].Value = row.Address;
                WorkSheet.Cells[counter, 2].Value = row.Lat;
                WorkSheet.Cells[counter, 3].Value = row.Lng;
            });
        }


        public string CampaignName => "LatLong";
        private LatLongReportRequest _latLongCollection;

    }
}