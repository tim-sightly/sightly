﻿using System.IO;

namespace Sightly.ReportGeneration.Generators.TargetView
{
    public interface ICsvReport
    {
        void Build();
        MemoryStream ToStream();
        string ToString();
    }
}
