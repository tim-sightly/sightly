﻿using System;
using System.Collections.Generic;
using System.Linq;
using Reporting.DAL.DTO.TargetView;

namespace Sightly.ReportGeneration.Generators.TargetView
{
    public class PerformanceSummaryReportCsv : ACsvReport, ICsvReport
    {
        private readonly List<OrderPerformanceSummary> _orderPerformanceSummaries;
        private readonly List<string> _includedColumns;
        private long? _targetPopulation;
        private readonly AggregatedOrderPerformanceSummary _aggregatedOrderPerformanceSummary;
        private readonly string _reportPeriodText;
        private readonly bool _showConversionStatColumns;
        private readonly PerformanceSummaryHelper _performanceSummaryHelper;

        private Dictionary<string, string> _headerLookup = new Dictionary<string, string>()
        {
            {"ORDER NAME", "ORDER NAME,"},
            {"ORDER REFERENCE CODE", "ORDER REFERENCE CODE,"},
            {"ADVERTISER", "ADVERTISER,"},
            {"CAMPAIGN", "CAMPAIGN,"},
            {"CAMPAIGN REF CODE", "CAMPAIGN REF CODE,"},
            {"START", "START,"},
            {"END", "END,"},
            {"BUDGET", "BUDGET,"},
            {"SPEND", "SPEND,"},
            {"IMPRESSIONS", "IMPRESSIONS,"},
            {"VIEWS", "VIEWS,"},
            {"VIEW RATE", "VIEW RATE,"},
            {"VIEW TIME (HOURS)", "VIEW TIME(HOURS),"},
            {"REACH", "REACH,"},
            {"GRPs", "GRPs,"},
            {"AVG. CPV", "AVG. CPV,"},
            {"CLICKS", "CLICKS,"},
            {"CLICK THROUGH RATE (CTR)", "CLICK THROUGH RATE (CTR),"},
        };

        private readonly List<string> _defaultIncludedColumns = new List<string>()
        {
            "ORDER NAME",
            "ORDER REFERENCE CODE",
            "ADVERTISER",
            "CAMPAIGN",
            "CAMPAIGN REF CODE",
            "START",
            "END",
            "BUDGET",
            "SPEND",
            "IMPRESSIONS",
            "VIEWS",
            "VIEW RATE",
            "VIEW TIME (HOURS)",
            "AVG. CPV",
            "REACH",
            "CLICKS",
            "CLICK THROUGH RATE (CTR)"
        };

        public PerformanceSummaryReportCsv(PerformanceSummaryReportParameters parameters) : base()
        {
            _orderPerformanceSummaries = parameters.OrderPerformanceSummaries;

            //TODO: Look at dumbass hack in constructor below that removes last element. Do we need it here?
            _includedColumns = parameters.IncludedColumns ?? _defaultIncludedColumns;

            //only add Grp column if Grp turned on AND if not Grp column not already added
            if (parameters.DisplayGrp && !_includedColumns.Contains("GRPs"))
            {
                _includedColumns.Insert(_includedColumns.FindIndex(c => c == "REACH") + 1, "GRPs");
            }

            _targetPopulation = parameters.TargetPopulation;
            _aggregatedOrderPerformanceSummary = parameters.AggregatedOrderPerformanceSummary;
            _reportPeriodText = parameters.ReportPeriodText;
            _showConversionStatColumns = parameters.ShowConversionStatColumns;
            _performanceSummaryHelper = new PerformanceSummaryHelper(_orderPerformanceSummaries, _targetPopulation);
            
        }

        public PerformanceSummaryReportCsv(List<OrderPerformanceSummary> orderPerformanceSummaries,
            AggregatedOrderPerformanceSummary aggregatedOrderPerformanceSummary,
            string reportPeriodText,
            List<string> includedColumns = null,
            bool displayGrp = false,
            long? targetPopulation = null,
            bool showConversionStatColumns = false) :base()
        {
            _orderPerformanceSummaries = orderPerformanceSummaries;

            //hack to remove nbsp; that occurs at end of column list
            if (includedColumns != null && includedColumns.Last() == "&nbsp;")
            {
                includedColumns.Remove(includedColumns.Last());
            }

            _includedColumns = includedColumns ?? _defaultIncludedColumns;

            //only add Grp column if Grp turned on AND if not Grp column not already added
            if (displayGrp && !_includedColumns.Contains("GRPs"))
            {
                _includedColumns.Insert(_includedColumns.FindIndex(c => c == "REACH") + 1, "GRPs");
            }

            _targetPopulation = targetPopulation;
            _aggregatedOrderPerformanceSummary = aggregatedOrderPerformanceSummary;
            _reportPeriodText = reportPeriodText;
            _showConversionStatColumns = showConversionStatColumns;
            _performanceSummaryHelper = new PerformanceSummaryHelper(_orderPerformanceSummaries, _targetPopulation);
            //_report = new StringBuilder();
        }

        #region Private Methods
        private void BuildColumnHeaders()
        {
            foreach (var column in _includedColumns)
            {
                _report.Append(_headerLookup[column]);
            }

            if (_showConversionStatColumns)
            {
                _report.Append("SAME-DEVICE CONVERSIONS,");
                _report.Append("CROSS-DEVICE CONVERSIONS,");
                _report.Append("IMPRESSION CONVERSIONS,");
                _report.Append("TOTAL CONVERSIONS,");
                _report.Append("TOTAL CONVERSION RATE,");
                _report.Append("COST PER CONVERSION,");
                _report.Append("TOTAL CONVERSIONS VALUE*,");
            }

            _report.AppendLine();
        }

        private void BuildCellValue(string value)
        {
            if (value == null)
                _report.Append(",");
            else
                _report.Append(EscapeCsvString(value) + ",");
        }

        private void BuildTableData()
        {
            for (var i = 0; i < _orderPerformanceSummaries.Count; i++)
            {
                foreach (var column in _includedColumns)
                {
                    BuildCellValue(_performanceSummaryHelper.Get(column, i));
                }

                if (_showConversionStatColumns)
                {
                    BuildCellValue(_orderPerformanceSummaries[i].FormattedSameDeviceConversions);
                    BuildCellValue(_orderPerformanceSummaries[i].FormattedCrossDeviceConversions);
                    BuildCellValue(_orderPerformanceSummaries[i].FormattedImpressionConversions.Trim());
                    BuildCellValue(_orderPerformanceSummaries[i].FormattedTotalConversions);
                    BuildCellValue(_orderPerformanceSummaries[i].FormattedTotalConversionRate);
                    BuildCellValue(_orderPerformanceSummaries[i].FormattedCostPerConversion);
                    BuildCellValue(_orderPerformanceSummaries[i].FormattedTotalConversionsValue);
                }

                _report.AppendLine();
            }

            _report.AppendLine();
        }

        private void BuildTableFooter()
        {
            _report.Append(",,,,,,,,,,,,,,,,,,,");
            _report.Append("*Only appears if all conversion actions in the order have a stated conversion value.,");
            _report.AppendLine();
        }

        private void BuildTableHeader()
        {
            _report.Append("Report Period,");
            _report.AppendLine(EscapeCsvString(_reportPeriodText) + ",");

            _report.Append("Total Ad Spend Orders To Date,");
            _report.AppendLine(EscapeCsvString(_aggregatedOrderPerformanceSummary.FormattedTotalSpend) + ",");

            _report.Append("Total Hours View Time,");
            _report.AppendLine(EscapeCsvString(_aggregatedOrderPerformanceSummary.FormattedShortTotalViewTimeInHours));

            _report.Append("Total Average Cost Per View,");
            _report.AppendLine(EscapeCsvString(_aggregatedOrderPerformanceSummary.FormattedAvgCPCV) + ",");

            _report.Append("View Rate,");
            _report.AppendLine(EscapeCsvString(Math.Round(_aggregatedOrderPerformanceSummary.ViewRate, 2).ToString("P")) + ",");

            _report.Append("Total Views,");
            _report.AppendLine(EscapeCsvString(_aggregatedOrderPerformanceSummary.FormattedTotalCompletedViews) + ",");

            _report.Append("Total Impressions,");
            _report.AppendLine(EscapeCsvString(_aggregatedOrderPerformanceSummary.FormattedTotalImpressions) + ",");
            _report.AppendLine();
        }
        #endregion

        #region Public Methods

        public override void Build()
        {
            BuildTableHeader();
            BuildColumnHeaders();
            BuildTableData();
            if (_showConversionStatColumns)
            {
                BuildTableFooter();
            }
        }

        #endregion

        private class PerformanceSummaryHelper
        {
            private List<OrderPerformanceSummary> _summaries;
            private long? _targetPopulation;

            public PerformanceSummaryHelper(List<OrderPerformanceSummary> summaries, long? targetPopulation)
            {
                _summaries = summaries;
                _targetPopulation = targetPopulation;
            }

            public string Get(string key, int index)
            {
                switch (key)
                {
                    case "ORDER NAME":
                        return _summaries[index].OrderName;
                    case "ADVERTISER":
                        return _summaries[index].AdvertiserName;
                    case "START":
                        return _summaries[index].FormattedStartDate;
                    case "END":
                        return _summaries[index].FormattedEndDate;
                    case "BUDGET":
                        return _summaries[index].FormattedBudget;
                    case "SPEND":
                        return _summaries[index].FormattedSpend;
                    case "REACH":
                        return _summaries[index].FormattedReach;
                    case "GRPs":
                        return GetGrp(_summaries[index].Impressions).ToString();
                    case "IMPRESSIONS":
                        return _summaries[index].FormattedImpressions;
                    case "VIEWS":
                        return _summaries[index].FormattedCompletedViews;
                    case "VIEW RATE":
                        return _summaries[index].FormattedViewRate;
                    case "VIEW TIME (HOURS)":
                        return _summaries[index].FormattedViewTime;
                    case "ORDER REFERENCE CODE":
                        return _summaries[index].OrderRefCode;
                    case "CAMPAIGN":
                        return _summaries[index].CampaignName;
                    case "CAMPAIGN REF CODE":
                        return _summaries[index].OrderRefCode;
                    case "CLICKS":
                        return _summaries[index].FormattedClicks;
                    case "CLICK THROUGH RATE (CTR)":
                        return _summaries[index].FormattedClickThroughRate;
                    case "AVG. CPV":
                        return _summaries[index].FormattedAvgCPCV;
                    default:
                        return "";
                }

            }

            private long GetGrp(long? impressions)
            {
                return !impressions.HasValue || !_targetPopulation.HasValue
                    ? 0
                    : Convert.ToInt64((100 * impressions) / (1.00 * _targetPopulation));
            }
        }
    }
}