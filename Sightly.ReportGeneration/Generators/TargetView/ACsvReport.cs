﻿using System.IO;
using System.Linq;
using System.Text;

namespace Sightly.ReportGeneration.Generators.TargetView
{
    public abstract class ACsvReport
    {
        protected StringBuilder _report;

        protected ACsvReport()
        {
            _report = new StringBuilder();
        }
        #region Public Methods

        public abstract void Build();

        public virtual MemoryStream ToStream()
        {
            byte[] byteArray = Encoding.UTF8.GetBytes(ToString());
            var ms = new MemoryStream(byteArray);
            return ms;
        }

        public virtual string ToString()
        {
            return _report.ToString();
        }

        protected virtual string EncloseWithParenthesis(string input)
        {
            return "(" + $"{input.ToUpper()}" + ")";
        }

        protected virtual string EscapeCsvString(string str)
        {
            if (str == null)
                return string.Empty;

            bool mustQuote = (str.Contains(",") || str.Contains("\"")
                              || str.Contains("\r") || str.Contains("\n"));

            if (mustQuote)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("\"");
                foreach (char nextChar in str)
                {
                    sb.Append(nextChar);
                    if (nextChar == '"')
                        sb.Append("\"");
                }
                sb.Append("\"");
                return sb.ToString();
            }

            return str;
        }

        protected virtual string ConvertToCsvCell(string value)
        {
            var mustQuote = value.Any(x => x == ',' || x == '\"' || x == '\r' || x == '\n');

            if (!mustQuote)
            {
                return value;
            }

            value = value.Replace("\"", "\"\"");

            return string.Format("\"{0}\"", value);
        }

        protected virtual string GetEscapedCsvCell(string input)
        {
            return string.Format("{0},", EscapeCsvString(input));
        }
        #endregion
    }
}