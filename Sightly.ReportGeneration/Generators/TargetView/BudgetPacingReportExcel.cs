﻿using System.Text;
using Reporting.DAL.DTO.TargetView;

namespace Sightly.ReportGeneration.Generators.TargetView
{
    public class BudgetPacingReportCsv : ACsvReport, ICsvReport
    {
        private readonly BudgetPacingReportParameters _input;

        public BudgetPacingReportCsv(BudgetPacingReportParameters parameters):base()
        {
            _input = parameters;
        }

        public override void Build()
        {
            BuildReportHeaders();
            BuildDataHeaders();
            BuildDataHeaders();
            BuildData();
        }

        private void BuildCellValue(StringBuilder csvBuilder, string value)
        {
            if (value == null)
                csvBuilder.Append(",");
            else
                csvBuilder.Append(EscapeCsvString(value) + ",");
        }

        private void BuildReportHeaders()
        {
            _report.Append("Report Period,");

            //TODO: I think we can look at a generated budget pacing report to figure out what 'reportPeriodText' should be.
            _report.AppendLine(EscapeCsvString(_input.ReportPeriodText) + ",");

            _report.Append("Aggregate Pacing Rate,");
            string aggregatePacingRate = _input.PacingRate.ToString("N3") + "%";
            _report.AppendLine(EscapeCsvString(aggregatePacingRate) + ",");

            _report.Append("Average Days Left Per Order,");
            _report.AppendLine(EscapeCsvString(_input.AverageDaysLeft.ToString("N0")) + ",");

            _report.Append("Aggregate Total Budget,");
            _report.AppendLine(EscapeCsvString(_input.TotalBudget.ToString("C2")) + ",");

            _report.Append("View Rate,");
            string viewRateString = (_input.ComputedViewRate * 100).ToString("N2") + "%";
            _report.AppendLine(EscapeCsvString(viewRateString) + ",");

            _report.Append("Total Completed Views,");
            _report.AppendLine(EscapeCsvString(_input.TotalCompletedViews.ToString("N0")) + ",");

            _report.Append("Total Impressions,");
            _report.AppendLine(EscapeCsvString(_input.TotalImpressions.ToString("N0")) + ",");
            _report.AppendLine();
        }

        private void BuildDataHeaders()
        {
            _report.Append("ORDER NAME,");
            _report.Append("ORDER REF CODE,");
            _report.Append("ADVERTISER,");
            _report.Append("CAMPAIGN,");
            _report.Append("CAMPAIGN REF CODE,");
            _report.Append("START,");
            _report.Append("END,");
            _report.Append("PACING,");
            _report.Append("TOTAL BUDGET,");
            _report.Append("SPEND OTD,");
            _report.Append("AVG DAILY SPEND,");
            _report.Append("REMAINING BUDGET,");
            _report.Append("PACING RATE,");
            _report.Append("DAYS LEFT,");
            _report.Append("PROJECTED FINAL SPEND,");
            _report.Append("CLICKS,");
            _report.Append("CLICK THROUGH RATE (CTR),");
            _report.AppendLine();

        }

        private void BuildData()
        {
            foreach (var summary in _input.BudgetPacingSummaries)
            {
                BuildCellValue(_report, summary.OrderName);
                BuildCellValue(_report, summary.OrderRefCode);
                BuildCellValue(_report, summary.AdvertiserName);
                BuildCellValue(_report, summary.CampaignName);
                BuildCellValue(_report, summary.CampaignRefCode);
                BuildCellValue(_report, summary.StartDate?.ToString("M/d/yy") ?? "");
                BuildCellValue(_report, summary.EndDate?.ToString("M/d/yy") ?? "");
                BuildCellValue(_report, summary.PacingStatusName);
                BuildCellValue(_report, summary.Budget?.ToString("C2") ?? "");
                BuildCellValue(_report, summary.SpendToDate?.ToString("C2") ?? "");
                BuildCellValue(_report, summary.AvgDailySpend?.ToString("C2") ?? "");
                BuildCellValue(_report, summary.RemainingBudget?.ToString("C2") ?? "");
                BuildCellValue(_report, summary.PacingRate.HasValue ? summary.PacingRate.Value.ToString("N0") + "%": "");
                BuildCellValue(_report, summary.DaysLeft?.ToString("N0") ?? "");
                BuildCellValue(_report, summary.ProjectedFinalSpend?.ToString("C2") ?? "");
                BuildCellValue(_report, summary.Clicks?.ToString("N0") ?? "");
                BuildCellValue(_report, (summary.ClickThroughRate * 100).ToString("N2") + "%");
                _report.AppendLine();
            }
        }
    }
}