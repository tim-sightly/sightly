﻿using Reporting.DAL.DTO.TargetView;

namespace Sightly.ReportGeneration.Generators.TargetView
{
    public class StatsDecorator
    {
        public StatsDecorator(AdPerformanceDetail adPerformanceDetail, string name)
        {
            AvgCPCV = adPerformanceDetail.AvgCPCV;
            Clicks = adPerformanceDetail.Clicks;
            CompletedViews = adPerformanceDetail.CompletedViews;
            Impressions = adPerformanceDetail.Impressions;
            VideoLengthInSeconds = adPerformanceDetail.VideoLengthInSeconds;
            ViewRate = adPerformanceDetail.ViewRate;
            Name = name;
        }

        public StatsDecorator(AgeGroupPerformanceDetail ageGroupPerformanceDetail, string name)
        {
            AvgCPCV = ageGroupPerformanceDetail.AvgCPCV;
            Clicks = ageGroupPerformanceDetail.Clicks;
            CompletedViews = ageGroupPerformanceDetail.CompletedViews;
            Impressions = ageGroupPerformanceDetail.Impressions;
            //VideoLengthInSeconds = ageGroupPerformanceDetail.VideoLengthInSeconds;
            ViewRate = ageGroupPerformanceDetail.ViewRate;
            Name = name;
        }

        public StatsDecorator(DeviceTypePerformanceDetail deviceTypePerformanceDetail, string name)
        {
            AvgCPCV = deviceTypePerformanceDetail.AvgCPCV;
            Clicks = deviceTypePerformanceDetail.Clicks;
            CompletedViews = deviceTypePerformanceDetail.CompletedViews;
            Impressions = deviceTypePerformanceDetail.Impressions;
            //VideoLengthInSeconds = deviceTypePerformanceDetail.VideoLengthInSeconds;
            ViewRate = deviceTypePerformanceDetail.ViewRate;
            Name = name;
        }

        public StatsDecorator(GenderPerformanceDetail genderPerformanceDetail, string name)
        {
            AvgCPCV = genderPerformanceDetail.AvgCPCV;
            Clicks = genderPerformanceDetail.Clicks;
            CompletedViews = genderPerformanceDetail.CompletedViews;
            Impressions = genderPerformanceDetail.Impressions;
            //VideoLengthInSeconds = genderPerformanceDetail.VideoLengthInSeconds;
            ViewRate = genderPerformanceDetail.ViewRate;
            Name = name;
        }



        public decimal? AvgCPCV { get; set; }

        public long? Clicks { get; set; }

        public long? CompletedViews { get; set; }

        public long? Impressions { get; set; }

        public int VideoLengthInSeconds { get; set; }

        public double? ViewRate { get; set; }

        public decimal ClickThroughRate
        {
            get
            {
                if (Impressions > 0)
                    return (decimal)Clicks / (decimal)Impressions;

                return 0;
            }
        }

        public double? ComputedViewRate
        {
            get
            {
                if (Impressions > 0)
                    return (double)CompletedViews / (double)Impressions;

                return 0;
            }
        }

        public string FormattedAvgCPCV
        {
            get
            {
                if (AvgCPCV.HasValue)
                    return AvgCPCV.Value.ToString("C");

                return "-";
            }
        }

        public string FormattedClicks
        {
            get
            {
                if (Clicks.HasValue)
                    return Clicks.Value.ToString("N0");

                return "-";
            }
        }

        public string FormattedClickThroughRate
        {
            get
            {
                if (ClickThroughRate > 0)
                    return (ClickThroughRate * 100).ToString("N2") + "%";

                return "-";
            }
        }

        public string FormattedCompletedViews
        {
            get
            {
                if (CompletedViews.HasValue)
                    return CompletedViews.Value.ToString("N0");

                return "-";
            }
        }

        public string FormattedImpressions
        {
            get
            {
                if (Impressions.HasValue)
                    return Impressions.Value.ToString("N0");

                return "-";
            }
        }

        public string FormattedViewRate
        {
            get
            {
                if (ComputedViewRate.HasValue)
                    return (ComputedViewRate.Value * 100).ToString("N0") + "%";

                return "0";
            }
        }

        public string Name { get; }
    }
}