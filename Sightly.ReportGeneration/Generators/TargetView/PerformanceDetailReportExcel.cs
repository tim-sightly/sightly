﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using ClosedXML.Excel;
using DocumentFormat.OpenXml.Packaging;
using Reporting.DAL.DTO.TargetView;
using Sightly.ReportGeneration.Generators.ChartGenerators;
using Sightly.ReportGeneration.Generators.Interface;
using Sightly.ReportGeneration.Storage;
using SpreadsheetLight;
using SpreadsheetLight.Drawing;

namespace Sightly.ReportGeneration.Generators.TargetView
{
    public class PerformanceDetailReportExcel: IExcelTargetViewReport
    {
        private readonly PerformanceDetailReportParameters _input;
        private XLWorkbook _workBook;
        private IXLWorksheet _workSheet;

        private readonly IStorageManager _fileManager;
        private readonly IStorageManager _imageManager;

        private const string SHEET_NAME = "Performance Summary Detail";
        private const string AGE_GROUP_CHART_NAME = "Views by Known Age";
        private const string BLUE_GRAY_HEX = "#44546A";
        private const string BORDER_GRAY_HEX = "#D9D9D9";
        private const int CLOSED_XML_GENERAL_FORMAT_ID = 0;
        private const int CLOSED_XML_PERCENTAGE_FORMAT_ID = 10;
        private const int CLOSED_XML_WHOLE_NUMBER_FORMAT_ID = 3;
        private const int CLOSED_XML_WHOLE_PERCENTAGE_FORMAT_ID = 9;
        private const string DEVICE_CHART_NAME = "Views by Device";
        private const string GENDER_CHART_NAME = "Views by Gender";
        private const string GRAY_HEX = "#BFBFBF";
        private const string LIGHT_BLUE_HEX = "#04AEEF";
        private const string LIGHT_GRAY_HEX = "#F2F2F2";
        private const string NOT_AVAILABLE_INDICATOR = "N/A";
        private const string PERFORMANCE_DETAIL_SHEET_NAME = "Performance Summary Detail";
        private const string TARGET_VIEW_IMAGE_NAME = "TargetView Logo";
        private const string TOTAL_CONVERSIONS_CHART_TITLE = "Total Conversions";
        private const string TOTAL_CONVERSION_RATE_CHART_TITLE = "Conversion Rate";
        private const string TWO_DECIMAL_MONEY_FORMAT = "0.00";
        private const string WHOLE_NUMBER_FORMAT = "#,##0";
        

        public PerformanceDetailReportExcel(PerformanceDetailReportParameters parameters, string containerName, string filename)
        {
            _input = parameters;
            _workBook = new XLWorkbook();
            _workBook.AddWorksheet(SHEET_NAME);
            _workSheet = _workBook.Worksheet(SHEET_NAME);
            _fileManager = new BlobStorageManager(containerName, filename);
            _imageManager = new BlobStorageManager("images");
        }
        
        public virtual MemoryStream ToStream(string filename)
        {
            //var ms = new MemoryStream();
            var ms = _fileManager.ReadFileAsStream(filename);
            //_workBook.SaveAs(ms);
            ms.Position = 0;
            return ms;
        }
        
        
        public string Build()
        {
            /* STYLE WORKSHEET */
            _workSheet.ColumnWidth = 10.25D;
            _workSheet.Column(1).Width = 16.75D;
            _workSheet.RowHeight = 15.75D;

            /* BUILD WORKSHEET */
            BuildHeader();
            BuildOverview();
            BuildHighlights();

            BuildAgeStatsTable();
            BuildGenderStatsTable();
            BuildDeviceStatsTable();
            BuildAdStatsTable();
            BuildAdQuartileTable();

            //TODO:
            //Disable for now until conversion stats implemented.
            //_input.ShowConversionStatSection = false;
            //if ((_input.ShowConversionStatSection ?? false) && _input.OrderPerformanceDetailConversionStat.Count > 0)
            //{
            //    BuildConversionStatsTable();
            //}            

            //save spreadsheet to disk so it can be opened as spreadsheet light.
            //this is done for to faciliate image injection
            var ms = new MemoryStream();
            _workBook.SaveAs(ms);
            _fileManager.CreateFile(ms);

            /* IMAGES & CHARTS */
            BuildTargetViewImage();
            BuildAgeGroupViewsChart();
            BuildGenderViewsChart();
            BuildDeviceViewsChart();

            return _fileManager.FileUrl;
        }        

        #region Header Helpers

        private void BuildHeader()
        {
            BuildHeaderRange();
            BuildLogoBackgroundRange();
            BuildTileRange();

            if (_input.CampaignOrders != null && _input.CampaignOrders.Count > 1)
            {
                BuildOrderLabel();
                BuildOrderNameRange();
            }
            else
            {
                BuildCampaignLabel();
                BuildCampaignNameRange();
            }

            BuildAdvertiserLabel();
            BuildAdvertiserNameRange();
            BuildPeriodLabel();
            BuildPeriodTextRange();
        }

        private void BuildHeaderRange()
        {
            var headerRange = _workSheet.Range("A1:L1");
            headerRange.Merge();
            headerRange.Style.Fill.BackgroundColor = XLColor.FromHtml(LIGHT_BLUE_HEX);
        }

        private void BuildLogoBackgroundRange()
        {
            var logoBackgroundRange = _workSheet.Range("A2:C4");
            logoBackgroundRange.Merge();
            logoBackgroundRange.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        private void BuildTileRange()
        {
            var reportTitleRange = _workSheet.Range("D2:H4");
            reportTitleRange.Value = "Performance Detail Report";
            reportTitleRange.Merge();
            reportTitleRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Left;
            reportTitleRange.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            reportTitleRange.Style.Font.FontName = "Open Sans";
            reportTitleRange.Style.Font.FontSize = 18;
            reportTitleRange.Style.Font.FontColor = XLColor.FromHtml(BLUE_GRAY_HEX);
            reportTitleRange.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        private void BuildOrderLabel()
        {
            var orderTitleCell = _workSheet.Cell("I3");
            orderTitleCell.Value = "Order:";
            orderTitleCell.Style.Font.FontName = "Open Sans";
            orderTitleCell.Style.Font.FontSize = 10;
            orderTitleCell.Style.Font.FontColor = XLColor.FromHtml(BLUE_GRAY_HEX);
            orderTitleCell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            orderTitleCell.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        private void BuildOrderNameRange()
        {
            var orderNameRange = _workSheet.Range("J3:L3");
            orderNameRange.SetValue(_input.OrderName);
            orderNameRange.Merge();
            orderNameRange.Style.Font.FontName = "Open Sans";
            orderNameRange.Style.Font.FontSize = 10;
            orderNameRange.Style.Font.FontColor = XLColor.FromHtml(BLUE_GRAY_HEX);
            orderNameRange.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            orderNameRange.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        private void BuildCampaignLabel()
        {
            var campaignTitleCell = _workSheet.Cell("I3");
            campaignTitleCell.Value = "Campaign:";
            campaignTitleCell.Style.Font.FontName = "Open Sans";
            campaignTitleCell.Style.Font.FontSize = 10;
            campaignTitleCell.Style.Font.FontColor = XLColor.FromHtml(BLUE_GRAY_HEX);
            campaignTitleCell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            campaignTitleCell.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        private void BuildCampaignNameRange()
        {
            var campaignNameRange = _workSheet.Range("J3:L3");
            campaignNameRange.SetValue(_input.CampaignName);
            campaignNameRange.Merge();
            campaignNameRange.Style.Font.FontName = "Open Sans";
            campaignNameRange.Style.Font.FontSize = 10;
            campaignNameRange.Style.Font.FontColor = XLColor.FromHtml(BLUE_GRAY_HEX);
            campaignNameRange.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            campaignNameRange.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        private void BuildAdvertiserLabel()
        {
            var advertiserTitleCell = _workSheet.Cell("I2");

            advertiserTitleCell.Value = "Advertiser:";

            advertiserTitleCell.Style.Font.FontName = "Open Sans";
            advertiserTitleCell.Style.Font.FontSize = 10;
            advertiserTitleCell.Style.Font.FontColor = XLColor.FromHtml(BLUE_GRAY_HEX);
            advertiserTitleCell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            advertiserTitleCell.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        private void BuildAdvertiserNameRange()
        {
            var advertiserNameRange = _workSheet.Range("J2:L2");

            advertiserNameRange.SetValue(_input.AdvertiserName);

            advertiserNameRange.Merge();
            advertiserNameRange.Style.Font.FontName = "Open Sans";
            advertiserNameRange.Style.Font.FontSize = 10;
            advertiserNameRange.Style.Font.FontColor = XLColor.FromHtml(BLUE_GRAY_HEX);
            advertiserNameRange.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            advertiserNameRange.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        private void BuildPeriodLabel()
        {
            var advertiserTitleCell = _workSheet.Cell("I4");

            advertiserTitleCell.Value = "Period:";

            advertiserTitleCell.Style.Font.FontName = "Open Sans";
            advertiserTitleCell.Style.Font.FontSize = 10;
            advertiserTitleCell.Style.Font.FontColor = XLColor.FromHtml(BLUE_GRAY_HEX);
            advertiserTitleCell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            advertiserTitleCell.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        private void BuildPeriodTextRange()
        {
            var reportPeriodRange = _workSheet.Range("J4:L4");
            DateTime yesterday = DateTime.Now.AddDays(-1);

            string periodString = $"Showing date {_input.StartDate.ToString("MM/dd/yyyy")} - {_input.EndDate.ToString("MM/dd/yyyy")}";
            reportPeriodRange.Value = periodString;

            reportPeriodRange.Merge();
            reportPeriodRange.Style.Font.FontName = "Open Sans";
            reportPeriodRange.Style.Font.FontSize = 10;
            reportPeriodRange.Style.Font.FontColor = XLColor.FromHtml(BLUE_GRAY_HEX);
            reportPeriodRange.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            reportPeriodRange.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        #endregion

        #region Overview Helpers

        private void BuildOverview()
        {
            BuildOverviewLabelRange();

            BuildCostLabel();
            BuildSpendCell();

            BuildReachLabel();
            BuildReachValueCell();

            BuildViewTimeLabel();
            BuildViewTimeValueCell();

            BuildViewsLabel();
            BuildViewsValueCell();

            BuildCpcvLabel();
            BuildCpcvValueCell();

            BuildViewRateLabel();
            BuildViewRateValueCell();

            BuildImpressionsLabel();
            BuildImpressionsValueCell();

            BuildCtrLabel();
            BuildCtrValueCell();

            BuildClicksLabel();
            BuildClicksValueCell();

            BuildOverviewFooter();
        }

        private void BuildOverviewLabelRange()
        {
            var overviewLabelRange = _workSheet.Range("A6:A7");
            overviewLabelRange.Value = "Overview";
            overviewLabelRange.Merge();
            overviewLabelRange.Style.Font.FontSize = 10;
            overviewLabelRange.Style.Font.FontName = "Open Sans";
            overviewLabelRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            overviewLabelRange.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            overviewLabelRange.Style.Fill.BackgroundColor = XLColor.FromHtml(LIGHT_BLUE_HEX);
        }

        private void BuildCostLabel()
        {
            var cell = _workSheet.Cell("B6");
            var label = "Spend";
            var comment = "The total dollar amount spent during the designated period for the order.";

            BuildOverviewValueHeader(cell, label, comment);
        }

        private void BuildOverviewValueHeader(IXLCell cell, string headerText, string comment, double fontSize = 10)
        {
            cell.Value = headerText;

            cell.Style.Font.FontName = "Open Sans";
            cell.Style.Font.FontSize = fontSize;
            cell.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            cell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            cell.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
            cell.Comment.AddText(comment);
        }

        private void BuildSpendCell()
        {
            var cell = _workSheet.Cell("B7");

            cell.Style.Font.FontName = "Open Sans";
            cell.Style.Font.FontSize = 9;
            cell.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            cell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            BuildCurrencyCell(cell, _input.OrderAggregate.Cost);

        }

        private void BuildCurrencyCell(IXLCell cell, decimal value, string format = null)
        {
            cell.Style.Font.FontName = "Open Sans";
            cell.Style.Font.FontSize = 9;
            cell.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            cell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            cell.Value = value.ToString("C");
            
        }

        private void BuildOverviewValueCell(IXLCell cell, object value, string format = null)
        {
            cell.Value = value;

            if (!string.IsNullOrEmpty(format))
                cell.Style.NumberFormat.Format = format;

            cell.Style.Font.FontName = "Open Sans";
            cell.Style.Font.FontSize = 9;
            cell.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            cell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
        }

        private void BuildReachLabel()
        {
            var cell = _workSheet.Cell("C6");
            var label = "% Reach";
            var comment = "The percent of your target population that saw your ads.";
            BuildOverviewValueHeader(cell, label, comment);
        }

        private void BuildReachValueCell()
        {
            var cell = _workSheet.Cell("C7");
            BuildOverviewValueCell(cell, _input.OrderAggregate.Reach / 100);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_PERCENTAGE_FORMAT_ID;
        }

        private void BuildViewTimeLabel()
        {
            var cell = _workSheet.Cell("G6");
            var label = "View Time";
            var comment = "The amount of time an ad was viewed by your target audience. Includes both Views and non-View Impressions (aka Partial Views).";
            BuildOverviewValueHeader(cell, label, comment);
        }

        private void BuildViewTimeValueCell()
        {
            var cell = _workSheet.Cell("G7");
            string viewTimeString = $"{Math.Round(_input.OrderAggregate.ViewTime)} Hours";
            BuildOverviewValueCell(cell, viewTimeString);
        }

        private void BuildViewsLabel()
        {
            var cell = _workSheet.Cell("E6");
            var label = "Views";
            var comment = " The number of times your ads were viewed in full or to 30 seconds, whichever came first.";
            BuildOverviewValueHeader(cell, label, comment);
        }

        private void BuildViewsValueCell()
        {
            var cell = _workSheet.Cell("E7");
            BuildOverviewValueCell(cell, _input.OrderAggregate.Views, WHOLE_NUMBER_FORMAT);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_NUMBER_FORMAT_ID;
        }

        private void BuildCpcvLabel()
        {
            var cell = _workSheet.Cell("H6");
            var label = "CPV";
            var comment = "The average cost per view.";
            BuildOverviewValueHeader(cell, label, comment);
        }

        private void BuildCpcvValueCell()
        {
            var cell = _workSheet.Cell("H7");
            BuildCurrencyCell(cell, _input.OrderAggregate.CPV);
        }

        private void BuildViewRateLabel()
        {
            var cell = _workSheet.Cell("F6");
            var label = "View Rate";
            var comment = "The rate at which your ads were viewed vs. shown. (Views divided by Impressions.)";
            BuildOverviewValueHeader(cell, label, comment, 9);
        }

        private void BuildViewRateValueCell()
        {
            var cell = _workSheet.Cell("F7");
            BuildOverviewValueCell(cell, _input.OrderAggregate.ViewRate);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_PERCENTAGE_FORMAT_ID;
        }

        private void BuildImpressionsLabel()
        {
            var cell = _workSheet.Cell("D6");
            var label = "Impressions";
            var comment = " The number of times your ads were shown to viewers.";
            BuildOverviewValueHeader(cell, label, comment, 9);
        }

        private void BuildImpressionsValueCell()
        {
            var cell = _workSheet.Cell("D7");
            BuildOverviewValueCell(cell, _input.OrderAggregate.Impressions, WHOLE_NUMBER_FORMAT);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_NUMBER_FORMAT_ID;
        }

        private void BuildCtrLabel()
        {
            var cell = _workSheet.Cell("J6");
            var label = "CTR";
            var comment = "The click through rate (CTR) measures how many times viewers clicked your ads vs. how many times your ads were shown. (Clicks divided by Impressions.)";
            BuildOverviewValueHeader(cell, label, comment, 9);
        }

        private void BuildCtrValueCell()
        {
            var cell = _workSheet.Cell("J7");
            BuildOverviewValueCell(cell, _input.OrderAggregate.ClickThroughRate);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_PERCENTAGE_FORMAT_ID;
        }

        private void BuildGrpLabel()
        {
            var cell = _workSheet.Cell("K6");
            var label = "GRPs";
            var comment = "GRPs mean Gross Ratings Points. It is a legacy television measure that is used to indicate how much of the targeted audience saw the ad.";
            BuildOverviewValueHeader(cell, label, comment, 9);
        }

        private void BuildGrpValueCell()
        {
            long impressions = _input.OrderAggregate.Impressions;
            long targetPopulation = _input.TargetPopulation ?? 0;            
            var grp = (targetPopulation == 0) ? 0 : ((100 * impressions) / (1.00 * targetPopulation));

            var cell = _workSheet.Cell("K7");
            BuildOverviewValueCell(cell, grp, WHOLE_NUMBER_FORMAT);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_NUMBER_FORMAT_ID;
        }

        private void BuildClicksLabel()
        {
            var cell = _workSheet.Cell("I6");
            var label = "Clicks";
            var comment = "The number of times viewers clicked the display URLs on your ads.";
            BuildOverviewValueHeader(cell, label, comment, 9);
        }

        private void BuildClicksValueCell()
        {
            var cell = _workSheet.Cell("I7");
            BuildOverviewValueCell(cell, _input.OrderAggregate.Clicks, WHOLE_NUMBER_FORMAT);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_NUMBER_FORMAT_ID;
        }

        private void BuildOverviewFooter()
        {
            var footerRange = _workSheet.Range("K6:L6");
            footerRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            footerRange.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            footerRange.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        #endregion

        #region Highlights Helpers

        private void BuildHighlights()
        {
            BuildHighlightsSectionHeader();
            BuildAgeGroupHighlights();
            BuildGenderHighlights();
            BuildDeviceTypeHighlights();
            BuildAdHighlights();
            BuildHighlightsFooter();
        }

        private void BuildHighlightsSectionHeader()
        {
            var sectionHeaderRange = _workSheet.Range("A9:A13");
            sectionHeaderRange.Value = "Highlights";
            sectionHeaderRange.Merge();
            sectionHeaderRange.Style.Font.FontSize = 10;
            sectionHeaderRange.Style.Font.FontName = "Open Sans";
            sectionHeaderRange.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            sectionHeaderRange.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            sectionHeaderRange.Style.Fill.BackgroundColor = XLColor.FromHtml(LIGHT_BLUE_HEX);
        }

        private void BuildAgeGroupHighlights()
        {
            var ageGroupText = GetAgeGroupHighlights(_input.AgeGroupPerformanceDetails);
            var ageGroupHighlightsRange = _workSheet.Range("B9:K9");

            BuildHighlightsSectionItemRange(ageGroupHighlightsRange, ageGroupText, false);
            ageGroupHighlightsRange.Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
            ageGroupHighlightsRange.Style.Border.SetTopBorderColor(XLColor.FromHtml(BORDER_GRAY_HEX));
        }

        private void BuildHighlightsSectionItemRange(IXLRange range, string text, bool isAlternateRow)
        {
            range.Merge();
            range.Value = text;

            range.Style.Font.FontSize = 10;
            range.Style.Font.FontName = "Open Sans";

            if (isAlternateRow)
                range.Style.Fill.BackgroundColor = XLColor.FromHtml(LIGHT_GRAY_HEX);
        }

        private string GetAgeGroupHighlights(List<AgeGroupPerformanceDetail> ageGroupStats)
        {
            var ageGroupViewsHighlight = GetHighestAgeGroupViewsHighlight(ageGroupStats);
            var ageGroupViewRateHighlight = GetHighestAgeGroupViewRateHighlight(ageGroupStats);

            return string.Format("{0} {1}", ageGroupViewsHighlight, ageGroupViewRateHighlight);
        }

        private string GetHighestAgeGroupViewRateHighlight(List<AgeGroupPerformanceDetail> ageGroupStats)
        {
            var highestViewRate = ageGroupStats.Max(ag => ag.ComputedViewRate);

            if (ageGroupStats == null || ageGroupStats.Count <= 0
                || !highestViewRate.HasValue)
                return string.Empty;

            var ageGroupNamesWithHighestViewRates = GetAgeGroupNamesWithHighestViewRate(ageGroupStats);

            return BuildHighestAgeGroupViewRateHighlight(ageGroupNamesWithHighestViewRates, highestViewRate);
        }

        private string GetHighestAgeGroupViewsHighlight(List<AgeGroupPerformanceDetail> ageGroupStats)
        {
            if (ageGroupStats == null || ageGroupStats.Count <= 0)
                return string.Empty;

            var ageGroupNamesWithHighestViews = GetAgeGroupNamesWithHighestViews(ageGroupStats);

            return BuildHighestAgeGroupViewsHighlight(ageGroupNamesWithHighestViews);
        }

        private List<string> GetAgeGroupNamesWithHighestViewRate(List<AgeGroupPerformanceDetail> ageGroupStats)
        {
            var highestAgeGroupViewRate = ageGroupStats.Max(ag => ag.ComputedViewRate);

            return ageGroupStats.Where(ag => ag.ComputedViewRate == highestAgeGroupViewRate)
                .Select(ag => ag.AgeGroupName)
                .ToList();
        }

        private string BuildHighestAgeGroupViewRateHighlight(
            List<string> ageGroupNamesWithHigestViewRate,
            double? highestViewRate
        )
        {
            var ageGroupHighlights = "Ages {0} has the greatest view rate ({1}).";
            var combinedAgeGroupNames = CombineEntityNames(ageGroupNamesWithHigestViewRate);
            var highestViewRateText = highestViewRate.HasValue ? highestViewRate.Value.ToString("P2") : "0";

            return string.Format(
                ageGroupHighlights,
                combinedAgeGroupNames,
                highestViewRateText
            );
        }

        private string CombineEntityNames(List<string> entityNames)
        {
            var combinedEntityNames = string.Empty;

            for (var i = 0; i < entityNames.Count; i++)
            {
                var item = entityNames[i];

                if (i == 0)
                {
                    combinedEntityNames = item;
                }
                else if (i == (entityNames.Count - 1))
                {
                    combinedEntityNames += (" and " + item);
                }
                else
                {
                    combinedEntityNames += (", " + item);
                }
            }

            return combinedEntityNames;
        }

        private List<string> GetAgeGroupNamesWithHighestViews(List<AgeGroupPerformanceDetail> ageGroupStats)
        {
            var highestAgeGroupViews = ageGroupStats.Max(ag => ag.CompletedViews);

            return ageGroupStats.Where(ag => ag.CompletedViews == highestAgeGroupViews)
                .Select(ag => ag.AgeGroupName)
                .ToList();
        }

        private string BuildHighestAgeGroupViewsHighlight(List<string> ageGroupNamesWithHigestViews)
        {
            var ageGroupHighlights = "Ages {0} has the most views of know ages.";
            var combinedAgeGroupNames = CombineEntityNames(ageGroupNamesWithHigestViews);

            return string.Format(ageGroupHighlights, combinedAgeGroupNames);
        }

        private void BuildGenderHighlights()
        {
            var genderHighlightsText = GetGenderHighlights(_input.GenderPerformanceDetails);
            var genderHighlightsRange = _workSheet.Range("B10:K10");
            BuildHighlightsSectionItemRange(genderHighlightsRange, genderHighlightsText, true);
        }

        private string GetGenderHighlights(List<GenderPerformanceDetail> genderStats)
        {
            if (genderStats == null || genderStats.Count <= 0)
                return string.Empty;

            var genderNamesWithHighestViews = GetGenderNamesWithHighestViews(genderStats);
            var combinedGenderNames = CombineEntityNames(genderNamesWithHighestViews);
            var highestGenderPercentile = GetHighestGenderViewsPercentile(genderStats);
            var genderViewsHighlightTemplate = "{0} accounted for {1} of views.";
            var highestGenderPercentileText = highestGenderPercentile.HasValue ? highestGenderPercentile.Value.ToString("P2") : "0";
            return string.Format(genderViewsHighlightTemplate,combinedGenderNames,highestGenderPercentileText);
        }

        private List<string> GetGenderNamesWithHighestViews(List<GenderPerformanceDetail> genderStats)
        {
            var highestViews = genderStats.Max(g => g.CompletedViews);

            return genderStats.Where(g => g.CompletedViews == highestViews)
                .Select(g => g.GenderName)
                .ToList();
        }

        private double? GetHighestGenderViewsPercentile(List<GenderPerformanceDetail> genderStats)
        {
            var maxGenderViews = (double?)genderStats.Max(g => g.CompletedViews);
            var totalGenderViews = (double?)genderStats.Sum(g => g.CompletedViews);
            return maxGenderViews / totalGenderViews;
        }

        private void BuildDeviceTypeHighlights()
        {
            var deviceTypeHighlightsText = GetDeviceTypeHighlights(_input.DevicePerformanceDetails);
            var deviceTypeHighlightsRange = _workSheet.Range("B11:K11");
            BuildHighlightsSectionItemRange(deviceTypeHighlightsRange, deviceTypeHighlightsText, false);
        }


        public string GetDeviceTypeHighlights(List<DeviceTypePerformanceDetail> deviceStats)
        {
            if (deviceStats == null || deviceStats.Count <= 0)
                return string.Empty;

            var deviceTypeNamesWithHighestViews = GetDeviceTypeNamesWithHighestViewRate(deviceStats);
            var combinedNames = CombineEntityNames(deviceTypeNamesWithHighestViews);
            var highestDeviceTypeViewRate = deviceStats.Max(d => d.ComputedViewRate);
            var deviceTypeViewRateHighlightTemplate = "{0} had the strongest View Rate of known devices at {1}.";
            var highestDeviceTypeViewRateText = highestDeviceTypeViewRate.HasValue ? highestDeviceTypeViewRate.Value.ToString("P2") : "0";
            return string.Format(deviceTypeViewRateHighlightTemplate,combinedNames,highestDeviceTypeViewRateText);
        }

        private List<string> GetDeviceTypeNamesWithHighestViewRate(List<DeviceTypePerformanceDetail> deviceStats)
        {
            var highestDeviceTypeViewRate = deviceStats.Max(c => c.ComputedViewRate);

            return deviceStats.Where(d => d.ComputedViewRate == highestDeviceTypeViewRate)
                .Select(d => d.DeviceTypeName)
                .ToList();
        }

        private void BuildAdHighlights()
        {
            var adHighlightsText = GetAdHighlights(_input.AdPerformanceDetails);
            var adHighlightsRange = _workSheet.Range("B12:K12");
            var adHighlightsRowIndex = 12;
            var rows = _workSheet.Rows(adHighlightsRowIndex, adHighlightsRowIndex);

            BuildHighlightsSectionItemRange(adHighlightsRange, adHighlightsText, true);
            rows.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
            rows.Style.Alignment.SetWrapText(true);
            rows.Height = 40;
        }

        public string GetAdHighlights(List<AdPerformanceDetail> adStats)
        {
            if (adStats.Count <= 1)
                return string.Empty;

            var highlightsTemplate = "{0} ad accounted for the highest number of views while the {1} ad had a higher View Rate.";
            var adNamesWithHighestViews = GetAdNamesWithHighestViews(adStats);
            var adNamesWithHighestViewRate = GetAdNamesWithHighestViewRates(adStats);

            return string.Format(highlightsTemplate, CombineEntityNames(adNamesWithHighestViews), CombineEntityNames(adNamesWithHighestViewRate));
        }

        public List<string> GetAdNamesWithHighestViews(List<AdPerformanceDetail> adStats)
        {
            var highestViews = adStats.Max(a => a.CompletedViews);
            return adStats.Where(a => a.CompletedViews == highestViews)
                .Select(a => a.VideoAdName)
                .ToList();
        }

        public List<string> GetAdNamesWithHighestViewRates(List<AdPerformanceDetail> adStats)
        {
            var highestViewRates = adStats.Max(a => a.ViewRate);
            return adStats.Where(a => a.ViewRate == highestViewRates)
                .Select(a => a.VideoAdName)
                .ToList();
        }

        private void BuildHighlightsFooter()
        {
            var footerHighlightsRange = _workSheet.Range("B13:K13");
            BuildHighlightsSectionItemRange(footerHighlightsRange, string.Empty, false);
            footerHighlightsRange.Style.Border.SetBottomBorder(XLBorderStyleValues.Thin);
            footerHighlightsRange.Style.Border.SetBottomBorderColor(XLColor.FromHtml(BORDER_GRAY_HEX));
        }

        #endregion

        #region Age Stats Helpers

        private void BuildAgeStatsTable()
        {
            BuildStatsTableHeaderRow("Age", 15);

            var tableBodyRowIndex = 16;
            var rowHeight = 15.75;
            var stats = _input.AgeGroupPerformanceDetails.ConvertAll(ag => new StatsDecorator(ag, ag.AgeGroupName));
            BuildStatsTableBody(stats, tableBodyRowIndex);
            AdjustStatsTableRowHeights(tableBodyRowIndex, stats.Count, rowHeight);

            var chartRange = _workSheet.Range("H14:K21");
            chartRange.Style.Font.FontSize = 12;
        }

        private void BuildStatsTableValueCells(StatsDecorator stat, int itemRowIndex, int itemIndex)
        {
            BuildStatsTableImpressionsValueCell(stat, itemRowIndex, itemIndex);
            BuildStatsTableViewsValueCell(stat, itemRowIndex, itemIndex);
            BuildStatsTableViewRateValueCell(stat, itemRowIndex, itemIndex);
            BuildStatsTableCpcvValueCell(stat, itemRowIndex, itemIndex);
            BuildStatsTableClicksValueCell(stat, itemRowIndex, itemIndex);
            BuildStatsTableCtrValueCell(stat, itemRowIndex, itemIndex);
        }

        private void BuildStatsTableImpressionsValueCell(StatsDecorator stat, int itemRowIndex, int itemIndex)
        {
            var cellReference = string.Format("B{0}", itemRowIndex);
            var cell = _workSheet.Cell(cellReference);
            var impressions = stat.Impressions.HasValue ? stat.Impressions.Value : 0;

            BuildStatsTableValueCell(cell, impressions, itemIndex, WHOLE_NUMBER_FORMAT);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_NUMBER_FORMAT_ID;
        }

        private void BuildStatsTableViewRateValueCell(StatsDecorator stat, int itemRowIndex, int itemIndex)
        {
            var cellReference = string.Format("D{0}", itemRowIndex);
            var cell = _workSheet.Cell(cellReference);
            var hasNumericValue = stat.ComputedViewRate.HasValue;

            if (hasNumericValue)
            {
                BuildStatsTableValueCell(cell, stat.ComputedViewRate.Value, itemIndex);
                cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_PERCENTAGE_FORMAT_ID;
            }
            else
            {
                BuildStatsTableValueCell(cell, NOT_AVAILABLE_INDICATOR, itemIndex);
            }
        }

        private void BuildStatsTableViewsValueCell(StatsDecorator stat, int itemRowIndex, int itemIndex)
        {
            var cellReference = string.Format("C{0}", itemRowIndex);
            var cell = _workSheet.Cell(cellReference);
            var views = stat.CompletedViews.HasValue ? stat.CompletedViews.Value : 0;

            BuildStatsTableValueCell(cell, views, itemIndex, WHOLE_NUMBER_FORMAT);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_NUMBER_FORMAT_ID;
        }

        private void BuildStatsTableCpcvValueCell(StatsDecorator stat, int itemRowIndex, int itemIndex)
        {
            var cellReference = string.Format("E{0}", itemRowIndex);
            var cell = _workSheet.Cell(cellReference);
            var cpv = stat.AvgCPCV.HasValue ? stat.AvgCPCV.Value : 0;

            BuildStatsTableValueCell(cell, cpv.ToString("C"), itemIndex);
        }

        private void BuildStatsTableClicksValueCell(StatsDecorator stat, int itemRowIndex, int itemIndex)
        {
            var cellReference = string.Format("F{0}", itemRowIndex);
            var cell = _workSheet.Cell(cellReference);
            var clicks = stat.Clicks.HasValue ? stat.Clicks.Value : 0;

            BuildStatsTableValueCell(cell, clicks, itemIndex, WHOLE_NUMBER_FORMAT);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_NUMBER_FORMAT_ID;
        }

        private void BuildStatsTableCtrValueCell(StatsDecorator stat, int itemRowIndex, int itemIndex)
        {
            var cellReference = string.Format("G{0}", itemRowIndex);
            var cell = _workSheet.Cell(cellReference);
            var hasNumericCtrValue = stat.Impressions.HasValue;

            if (hasNumericCtrValue)
            {
                BuildStatsTableValueCell(cell, stat.ClickThroughRate, itemIndex);
                cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_PERCENTAGE_FORMAT_ID;
            }
            else
            {
                BuildStatsTableValueCell(cell, NOT_AVAILABLE_INDICATOR, itemIndex);
            }
        }

        private void BuildStatsTableNameCell(string text, int rowIndex, int itemIndex)
        {
            var cellReference = string.Format("A{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);
            if (text == "Mobile devices with full browsers")
            {
                cell.Value = "Mobile Phones";
            }
            else if (text == "Tablets with full browsers")
            {
                cell.Value = "Tablets";

            }
            else
            {
                cell.Value = text;
            }
            
            cell.Style.Font.FontSize = 10;
            cell.Style.Font.FontName = "Open Sans";
            cell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            cell.Style.Alignment.WrapText = true;
            cell.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            cell.Style.Border.SetLeftBorderColor(XLColor.FromHtml(BORDER_GRAY_HEX));

            if ((itemIndex % 2) == 1)
                cell.Style.Fill.BackgroundColor = XLColor.FromHtml(LIGHT_GRAY_HEX);
        }


        #endregion

        #region Gender Stats Helpers

        private void BuildGenderStatsTable()
        {
            /* HEADER */
            BuildStatsTableHeaderRow("Gender", 25);

            /* BODY */
            var tableBodyRowIndex = 26;
            var rowHeight = 43;
            var stats = _input.GenderPerformanceDetails.ConvertAll(ag => new StatsDecorator(ag, ag.GenderName));

            BuildStatsTableBody(stats, tableBodyRowIndex);
            AdjustStatsTableRowHeights(tableBodyRowIndex, stats.Count, rowHeight);
        }

        #endregion

        #region Device Stats Helpers

        private void BuildDeviceStatsTable()
        {
            /* HEADER */
            var tableHeaderText = "Device";
            var tableRowIndex = 31;
            BuildStatsTableHeaderRow(tableHeaderText, tableRowIndex);

            /* BODY */
            var tableBodyRowIndex = 32;
            var deviceStatsRowHeight = 29;
            var stats = _input.DevicePerformanceDetails.ConvertAll(ag => new StatsDecorator(ag, ag.DeviceTypeName));
            BuildStatsTableBody(stats, tableBodyRowIndex);
            AdjustStatsTableRowHeights(tableBodyRowIndex, stats.Count, deviceStatsRowHeight);
        }

        #endregion

        #region Ad Stats Helpers

        private void BuildAdStatsTable()
        {
            /* HEADER */
            var tableHeaderText = "Ad";
            var tableRowIndex = 39;
            BuildStatsTableHeaderRow(tableHeaderText, tableRowIndex);
            var cell = _workSheet.Cell("H39");
            BuildGrayStatsTableHeaderCell(cell, "Length");

            /* BODY */
            var tableBodyRowIndex = 40;
            var rowHeight = 64;
            var stats = _input.AdPerformanceDetails.ConvertAll(ag => new StatsDecorator(ag, ag.VideoAdName));

            BuildStatsTableBody(stats, tableBodyRowIndex);
            AdjustStatsTableRowHeights(tableBodyRowIndex, stats.Count, rowHeight);
            BuildStatsTableLengthBody(stats);
        }

        private void BuildStatsTableLengthBody(List<StatsDecorator> stats)
        {
            var rowIndex = 40;

            for (var i = 0; i < stats.Count; i++)
            {
                var item = stats[i];
                var itemRowIndex = (rowIndex + i);

                BuildStatsTableLengthValueCell(item, itemRowIndex, i);
            }
        }

        private void BuildStatsTableLengthValueCell(StatsDecorator stat, int itemRowIndex, int itemIndex)
        {
            var cellReference = string.Format("H{0}", itemRowIndex);
            var cell = _workSheet.Cell(cellReference);
            var seconds = stat.VideoLengthInSeconds != 0 ? stat.VideoLengthInSeconds : 0;
            var timeSpanSeconds = TimeSpan.FromSeconds(seconds);
            var formattedTimeSpan = "'";

            if (seconds > 59)
                formattedTimeSpan = formattedTimeSpan + timeSpanSeconds.ToString("mm':'ss");
            else
                formattedTimeSpan = formattedTimeSpan + timeSpanSeconds.ToString("':'ss");

            BuildStatsTableValueCell(cell, formattedTimeSpan, itemIndex);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_GENERAL_FORMAT_ID;
        }

        #endregion

        #region Quartile Stats Helpers

        private void BuildAdQuartileTable()
        {
            /* HEADER */
            BuildAdQuartileTableHeader();
            BuildAd25QuartileColumnHeader();
            BuildAd50QuartileColumnHeader();
            BuildAd75QuartileColumnHeader();
            BuildAd100QuartileColumnHeader();

            /* BODY */
            var rowIndex = 40;

            for (var i = 0; i < _input.AdPerformanceDetails.Count; i++)
            {
                var item = _input.AdPerformanceDetails[i];
                var itemRowIndex = (rowIndex + i);

                BuildAd25QuartileCell(item, itemRowIndex, i);
                BuildAd50QuartileCell(item, itemRowIndex, i);
                BuildAd75QuartileCell(item, itemRowIndex, i);
                BuildAd100QuartileCell(item, itemRowIndex, i);
            }
        }

        private void BuildAdQuartileTableHeader()
        {
            var range = _workSheet.Range("I38:L38");
            var text = "Audience Retention (Quartiles)";

            range.Merge();
            range.Value = text;
            range.Style.Font.FontSize = 10;
            range.Style.Font.FontName = "Open Sans";
            range.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            range.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            range.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        private void BuildAd25QuartileColumnHeader()
        {
            var cell = _workSheet.Cell("I39");
            string headerText = "0.25";
            var comment = "The percentage of viewers who viewed an ad to 25% of it's length.";

            BuildAdQuartileColumnHeader(cell, headerText, comment);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_PERCENTAGE_FORMAT_ID;
        }

        private void BuildAd50QuartileColumnHeader()
        {
            var cell = _workSheet.Cell("J39");
            string headerText = ".50";
            var comment = "The percentage of viewers who viewed an ad to 50% of it's length.";

            BuildAdQuartileColumnHeader(cell, headerText, comment);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_PERCENTAGE_FORMAT_ID;
        }

        private void BuildAd75QuartileColumnHeader()
        {
            var cell = _workSheet.Cell("K39");
            string headerText = ".75";
            var comment = "The percentage of viewers who viewed an ad to 75% of it's length.";

            BuildAdQuartileColumnHeader(cell, headerText, comment);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_PERCENTAGE_FORMAT_ID;
        }

        private void BuildAd100QuartileColumnHeader()
        {
            var cell = _workSheet.Cell("L39");
            string headerText = "1";
            var comment = "The percentage of viewers who viewed an ad to 100% of it's length.";

            BuildAdQuartileColumnHeader(cell, headerText, comment);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_PERCENTAGE_FORMAT_ID;
        }

        private void BuildAdQuartileColumnHeader(IXLCell cell, string headerText, string comment)
        {
            BuildGrayStatsTableHeaderCell(cell, headerText);
            cell.Style.Border.SetTopBorderColor(XLColor.FromHtml(BORDER_GRAY_HEX));
            cell.Style.Border.SetTopBorder(XLBorderStyleValues.Thin);
            cell.Comment.AddText(comment);
        }

        private void BuildAd25QuartileCell(AdPerformanceDetail stat, int itemRowIndex, int itemIndex)
        {
            var cellReference = string.Format("I{0}", itemRowIndex);
            var cell = _workSheet.Cell(cellReference);
            var quartileData = GetQuartileData(stat.AudienceRetention25);

            BuildStatsTableValueCell(cell, quartileData, itemIndex);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_PERCENTAGE_FORMAT_ID;
        }

        private void BuildAd50QuartileCell(AdPerformanceDetail stat, int itemRowIndex, int itemIndex)
        {
            var cellReference = string.Format("J{0}", itemRowIndex);
            var cell = _workSheet.Cell(cellReference);
            var quartileData = GetQuartileData(stat.AudienceRetention50);

            BuildStatsTableValueCell(cell, quartileData, itemIndex);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_PERCENTAGE_FORMAT_ID;
        }

        private void BuildAd75QuartileCell(AdPerformanceDetail stat, int itemRowIndex, int itemIndex)
        {
            var cellReference = string.Format("K{0}", itemRowIndex);
            var cell = _workSheet.Cell(cellReference);
            var quartileData = GetQuartileData(stat.AudienceRetention75);

            BuildStatsTableValueCell(cell, quartileData, itemIndex);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_PERCENTAGE_FORMAT_ID;
        }

        private void BuildAd100QuartileCell(AdPerformanceDetail stat, int itemRowIndex, int itemIndex)
        {
            var cellReference = string.Format("L{0}", itemRowIndex);
            var cell = _workSheet.Cell(cellReference);
            var quartileData = GetQuartileData(stat.AudienceRetention100);

            BuildStatsTableValueCell(cell, quartileData, itemIndex);
            cell.Style.NumberFormat.NumberFormatId = CLOSED_XML_WHOLE_PERCENTAGE_FORMAT_ID;
        }

        private decimal GetQuartileData(int quartileData)
        {
            if (quartileData == 0)
                return 0;

            return quartileData / 100M;
        }

        #endregion

        #region Conversion Stats Helpers

        private void BuildConversionStatsTable()
        {
            BuildConversionStatsTableHeader();
            BuildConversionStatsTableBody();
            AdjustConversionRowHeights();
            BuildFooterRange();
            BuildSecondaryFooter();
        }

        private int GetConversionStatsTableIndex()
        {
            var lastAdStatItemRowIndex = 38 + _input.AdPerformanceDetails.Count;
            var adStatsSeparatorRowCount = 3;
            return lastAdStatItemRowIndex + adStatsSeparatorRowCount;
        }

        private int GetConversionStatsLastRowIndex()
        {
            var conversionStatTableRowIndex = GetConversionStatsTableIndex();
            var miniumConversionRowCount = 7;
            int conversionStatCount = _input.OrderPerformanceDetailConversionStat.Count;
            var conversionsRowCount = conversionStatCount > miniumConversionRowCount ? conversionStatCount : miniumConversionRowCount;
            return conversionStatTableRowIndex + conversionsRowCount;
        }

        private void BuildClickConversionsColumnHeader(int rowIndex)
        {
            var cellReference = string.Format("B{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);
            var headerText = "View/Click Conversions";

            BuildConversionStatsTableHeaderCell(cell, headerText);
        }

        private void BuildCostPerConversionColumnHeader(int rowIndex)
        {
            var cellReference = string.Format("G{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);
            var headerText = "Cost Per Conversion";

            BuildConversionStatsTableHeaderCell(cell, headerText);
        }

        private void BuildConversionStatsTableHeader()
        {
            var rowIndex = GetConversionStatsTableIndex();

            var row = _workSheet.Row(rowIndex);
            row.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
            row.Style.Alignment.SetWrapText(true);
            row.Height = 40;

            BuildConversionStatsTableHeaderLabel(rowIndex);
            BuildClickConversionsColumnHeader(rowIndex);
            BuildImpressionConversionsColumnHeader(rowIndex);
            BuildCrossDeviceConversionsColumnHeader(rowIndex);
            BuildTotalConversionsColumnHeader(rowIndex);
            BuildTotalConversionRateColumnHeader(rowIndex);
            BuildCostPerConversionColumnHeader(rowIndex);
        }

        private void BuildConversionStatsTableHeaderCell(IXLCell cell, string headerText)
        {
            BuildGrayStatsTableHeaderCell(cell, headerText);
        }

        private void BuildConversionStatsTableHeaderLabel(int rowIndex)
        {
            var cellReference = string.Format("A{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);
            var headerText = "Conversions";

            BuildBlueStatsTableHeaderCell(cell, headerText);
        }

        private void BuildCrossDeviceConversionsColumnHeader(int rowIndex)
        {
            var cellReference = string.Format("D{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);
            var headerText = "Cross-Device Conversions";

            BuildConversionStatsTableHeaderCell(cell, headerText);
        }

        private void BuildImpressionConversionsColumnHeader(int rowIndex)
        {
            var cellReference = string.Format("C{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);
            var headerText = "Impression Conversions";

            BuildConversionStatsTableHeaderCell(cell, headerText);
        }

        private void BuildTotalConversionsColumnHeader(int rowIndex)
        {
            var cellReference = string.Format("E{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);
            var headerText = "Total Conversions";

            BuildConversionStatsTableHeaderCell(cell, headerText);
        }

        private void BuildTotalConversionRateColumnHeader(int rowIndex)
        {
            var cellReference = string.Format("F{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);
            var headerText = "Total Conv Rate";

            BuildConversionStatsTableHeaderCell(cell, headerText);
        }

        private void SetConversionHeaderRowHeight(IXLWorksheet worksheet, int rowIndex)
        {
            var row = worksheet.Row(rowIndex);

            row.Style.Alignment.Vertical = XLAlignmentVerticalValues.Top;
            row.Style.Alignment.SetWrapText(true);
            row.Height = 40;
        }

        private void AdjustConversionRowHeights()
        {
            var headerRowHeight = 1;
            var firstRowIndex = GetConversionStatsTableIndex() + headerRowHeight;
            var lastRowIndex = GetConversionStatsLastRowIndex();
            var tableRows = _workSheet.Rows(firstRowIndex, lastRowIndex);

            foreach (var row in tableRows)
            {
                row.Height = 40;
            }
        }

        private void BuildClickConversionValueCell(ConversionPerformanceDetail stat, int rowIndex)
        {
            var cellReference = string.Format("B{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);

            BuildConversionStatsTableValueCell(
                cell,
                stat.SameDeviceConversions,
                CLOSED_XML_WHOLE_NUMBER_FORMAT_ID
            );
        }

        private void BuildConversionStatsTableBody()
        {
            var headerRowHeight = 1;
            var firstItemRowIndex = GetConversionStatsTableIndex() + headerRowHeight;

            for (int i = 0; i < _input.OrderPerformanceDetailConversionStat.Count; i++)
            {
                var item = _input.OrderPerformanceDetailConversionStat[i];
                var itemRowIndex = firstItemRowIndex + i;

                BuildConversionStatsRowNameCell(item.ConversionActionName, itemRowIndex);
                BuildClickConversionValueCell(item, itemRowIndex);
                BuildImpressionConversionsValueCell(item, itemRowIndex);
                BuildCrossDeviceConversionsValueCell(item, itemRowIndex);
                BuildTotalConversionsValueCell(item, itemRowIndex);
                BuildTotalConversionRateValueCell(item, itemRowIndex);
                BuildCostPerConversionsValueCell(item, itemRowIndex);
            }
        }

        private void BuildConversionStatsRowNameCell(string text, int rowIndex)
        {
            var cellReference = string.Format("A{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);

            cell.Value = text;

            cell.Style.Font.FontSize = 10;
            cell.Style.Font.FontName = "Open Sans";
            cell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            cell.Style.Alignment.WrapText = true;
            cell.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            cell.Style.Border.SetLeftBorderColor(XLColor.FromHtml(BORDER_GRAY_HEX));
        }

        private void BuildConversionStatsTableValueCell(IXLCell cell, object value, int? formatId = null)
        {
            cell.Value = value;

            if (formatId.HasValue)
                cell.Style.NumberFormat.NumberFormatId = formatId.Value;

            cell.Style.Font.FontSize = 10;
            cell.Style.Font.FontName = "Open Sans";
            cell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            cell.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            cell.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            cell.Style.Border.SetLeftBorderColor(XLColor.FromHtml(BORDER_GRAY_HEX));
        }

        private void BuildCostPerConversionsValueCell(ConversionPerformanceDetail stat,int rowIndex)
        {
            var cellReference = string.Format("G{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);

            BuildConversionStatsTableValueCell(cell, stat.CostPerConversion);

            cell.Style.NumberFormat.Format = TWO_DECIMAL_MONEY_FORMAT;
        }

        private void BuildCrossDeviceConversionsValueCell(ConversionPerformanceDetail stat, int rowIndex)
        {
            var cellReference = string.Format("D{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);
            BuildConversionStatsTableValueCell(cell, stat.CrossDeviceConversions, CLOSED_XML_WHOLE_NUMBER_FORMAT_ID);
        }

        private void BuildImpressionConversionsValueCell(ConversionPerformanceDetail stat, int rowIndex)
        {
            var cellReference = string.Format("C{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);
            BuildConversionStatsTableValueCell(cell, stat.ImpressionConversions, CLOSED_XML_WHOLE_NUMBER_FORMAT_ID);
        }

        private void BuildTotalConversionsValueCell(ConversionPerformanceDetail stat, int rowIndex)
        {
            var cellReference = string.Format("E{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);
            BuildConversionStatsTableValueCell(cell, stat.TotalConversions, CLOSED_XML_WHOLE_NUMBER_FORMAT_ID);
        }

        private void BuildTotalConversionRateValueCell(ConversionPerformanceDetail stat,int rowIndex)
        {
            var cellReference = string.Format("F{0}", rowIndex);
            var cell = _workSheet.Cell(cellReference);
            var conversionRate = stat.TotalConversionRate.HasValue
                                 && stat.TotalConversionRate.Value > 0
                ? stat.TotalConversionRate.Value / 100
                : 0;

            BuildConversionStatsTableValueCell(cell, conversionRate, CLOSED_XML_PERCENTAGE_FORMAT_ID);
        }

        private void BuildFooterRange()
        {
            var lastRowIndex = GetFooterRowIndex();
            var footerRangeReferenceTemplate = "A{0}:K{0}";
            var footerReference = string.Format(footerRangeReferenceTemplate, lastRowIndex + 1);
            var footerRange = _workSheet.Range(footerReference);

            footerRange.Merge();
            footerRange.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        private void BuildSecondaryFooter()
        {
            var lastRowIndex = GetFooterRowIndex();
            var footerRangeReferenceTemplate = "A{0}:K{0}";
            var secondaryFooterReference = string.Format(footerRangeReferenceTemplate, lastRowIndex + 2);
            var secondaryFooterRange = _workSheet.Range(secondaryFooterReference);

            secondaryFooterRange.Merge();
            secondaryFooterRange.Style.Fill.BackgroundColor = XLColor.FromHtml(LIGHT_BLUE_HEX);
        }

        private int GetFooterRowIndex()
        {
            var lastConversionStatRowIndex = GetConversionStatsLastRowIndex();
            var footerRowCount = 1;
            return lastConversionStatRowIndex + footerRowCount;
        }

        #endregion

        #region Image Helpers

        private void BuildTargetViewImage()
        {
            using (MemoryStream msRead = _fileManager.ReadFileAsStream())
            {
                using (SLDocument doc = new SLDocument(msRead))
                {
                    var picture = new SLPicture(_imageManager.ReadFileAsByteArray("target-view-logo.png"), ImagePartType.Png);
                    picture.SetPosition(1.1, 0.1);
                    picture.ResizeInPixels(452, 180);
                    doc.InsertPicture(picture);
                    MemoryStream ms = new MemoryStream();
                    doc.SaveAs(ms);
                    _fileManager.CreateFile(ms);
                }
            }
        }

        private void BuildAgeGroupViewsChart()
        {
            int rowId = 14;
            int columnId = 7;

            using (MemoryStream msRead = _fileManager.ReadFileAsStream())
            {
                using (SLDocument doc = new SLDocument(msRead))
                {
                    var ageGroupViewsPieChartGenerator = new AgeGroupViewsPieChartGenerator();
                    MemoryStream chartStream = ageGroupViewsPieChartGenerator.Execute(_input.AgeGroupPerformanceDetails);
                    byte[] chartBytes = chartStream.ToArray();
                    var picture = new SLPicture(chartBytes, ImagePartType.Png);

                    picture.SetPosition(rowId, columnId);
                    doc.InsertPicture(picture);

                    MemoryStream ms = new MemoryStream();
                    doc.SaveAs(ms);
                    _fileManager.CreateFile(ms);
                }
            }
        }

        private void BuildGenderViewsChart()
        {
            int rowId = 24;
            int columnId = 7;

            using (MemoryStream msRead = _fileManager.ReadFileAsStream())
            {
                using (SLDocument doc = new SLDocument(msRead))
                {
                    var genderViewsPieChartGenerator = new GenderViewsPieChartGenerator();
                    MemoryStream chartStream = genderViewsPieChartGenerator.Execute(_input.GenderPerformanceDetails);
                    byte[] chartBytes = chartStream.ToArray();
                    var picture = new SLPicture(chartBytes, ImagePartType.Png);

                    picture.SetPosition(rowId, columnId);
                    doc.InsertPicture(picture);

                    MemoryStream ms = new MemoryStream();
                    doc.SaveAs(ms);
                    _fileManager.CreateFile(ms);
                }
            }
        }

        private void BuildDeviceViewsChart()
        {
            int rowId = 30;
            int columnId = 7;

            using (MemoryStream msRead = _fileManager.ReadFileAsStream())
            {
                using (SLDocument doc = new SLDocument(msRead))
                {
                    var deviceViewsBarChartGenerator = new DeviceViewsBarChartGenerator();
                    MemoryStream chartStream = deviceViewsBarChartGenerator.Execute(_input.DevicePerformanceDetails);
                    byte[] chartBytes = chartStream.ToArray();
                    var picture = new SLPicture(chartBytes, ImagePartType.Png);

                    picture.SetPosition(rowId, columnId);
                    doc.InsertPicture(picture);

                    MemoryStream ms = new MemoryStream();
                    doc.SaveAs(ms);
                    _fileManager.CreateFile(ms);
                }
            }
        }

        #endregion

        #region Common Helpers

        private void BuildStatsTableHeaderRow(string tableHeaderText, int rowIndex)
        {
            var tableHeaderCell = _workSheet.Cell(string.Format("A{0}", rowIndex));
            BuildBlueStatsTableHeaderCell(tableHeaderCell, tableHeaderText);

            var viewsHeaderCell = _workSheet.Cell(string.Format("C{0}", rowIndex));
            BuildGrayStatsTableHeaderCell(viewsHeaderCell, "Views");

            var cpcvHeaderCell = _workSheet.Cell(string.Format("E{0}", rowIndex));
            BuildGrayStatsTableHeaderCell(cpcvHeaderCell, "CPV");

            var viewRateHeaderCell = _workSheet.Cell(string.Format("D{0}", rowIndex));
            BuildGrayStatsTableHeaderCell(viewRateHeaderCell, "View Rate");

            var headerImpressionCell = _workSheet.Cell(string.Format("B{0}", rowIndex));
            BuildGrayStatsTableHeaderCell(headerImpressionCell, "Impressions");

            var ctrHeaderCell = _workSheet.Cell(string.Format("G{0}", rowIndex));
            BuildGrayStatsTableHeaderCell(ctrHeaderCell, "CTR");

            var clicksHeaderCell = _workSheet.Cell(string.Format("F{0}", rowIndex));
            BuildGrayStatsTableHeaderCell(clicksHeaderCell, "Clicks");
        }

        private void BuildStatsTableBody(List<StatsDecorator> stats, int rowIndex)
        {
            for (var i = 0; i < stats.Count; i++)
            {
                var item = stats[i];
                var itemRowIndex = (rowIndex + i);

                BuildStatsTableNameCell(item.Name, itemRowIndex, i);
                BuildStatsTableValueCells(item, itemRowIndex, i);
            }
        }

        private void AdjustStatsTableRowHeights(int tableBodyRowIndex, int rowCount, double rowHeight)
        {
            var lastRowIndex = (tableBodyRowIndex + rowCount) - 1;
            var rows = _workSheet.Rows(tableBodyRowIndex, lastRowIndex);
            rows.Height = rowHeight;
        }

        private void BuildBlueStatsTableHeaderCell(IXLCell cell, string text)
        {
            cell.Value = text;

            cell.Style.Font.FontSize = 10;
            cell.Style.Font.FontName = "Open Sans";
            cell.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            cell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            cell.Style.Fill.BackgroundColor = XLColor.FromHtml(LIGHT_BLUE_HEX);
        }

        private void BuildGrayStatsTableHeaderCell(IXLCell cell, string text)
        {
            cell.Value = text;

            cell.Style.Font.FontSize = 10;
            cell.Style.Font.FontName = "Open Sans";
            cell.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            cell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            cell.Style.Fill.BackgroundColor = XLColor.FromHtml(GRAY_HEX);
        }

        private void BuildStatsTableValueCell(IXLCell cell, object value, int itemIndex, string format = null)
        {
            cell.Value = value;

            if (!string.IsNullOrEmpty(format))
                cell.Style.NumberFormat.Format = format;

            cell.Style.Font.FontSize = 10;
            cell.Style.Font.FontName = "Open Sans";
            cell.Style.Alignment.Vertical = XLAlignmentVerticalValues.Center;
            cell.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
            cell.Style.Border.LeftBorder = XLBorderStyleValues.Thin;
            cell.Style.Border.SetLeftBorderColor(XLColor.FromHtml(BORDER_GRAY_HEX));

            if ((itemIndex % 2) == 1)
                cell.Style.Fill.BackgroundColor = XLColor.FromHtml(LIGHT_GRAY_HEX);
        }

        #endregion
    }
}