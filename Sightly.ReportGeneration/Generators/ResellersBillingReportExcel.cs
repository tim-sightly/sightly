﻿using System;
using System.Collections.Generic;
using System.Linq;
using OfficeOpenXml;
using Reporting.DAL.DTO;
using Sightly.ReportGeneration.Generators.Interface;

namespace Sightly.ReportGeneration.Generators
{
    public class ResellersBillingReportExcel : AExcelReport, IExcelReport
    {
        #region constants
        
        private readonly string INT_FORMAT = "#,##0";
        private readonly string PERCENTAGE_FORMAT = "#0.00%";
        private readonly string MONEY_FORMAT = "$0.00";

        #endregion

        #region variables

        private List<BillingData> _data;
        private DateTime _now;
        private int _rowOffset;
        private readonly string _dateFormatHeader = "MM-dd-yyyy";
        private readonly string _dateFormat = "MM/dd/yyyy";
        

        #endregion
        public ResellersBillingReportExcel(List<BillingData> billingData) 
            : base("Sightly.ReportGeneration.Templates.Excel.ResellerBillingReport.xlsx")
        {
            _data = billingData;
            _now = DateTime.Now;
            _rowOffset = 0;

            Build();
        }

        private void Build()
        {
            TotalSheetBuild();
            int count = 2;
            _data.GroupBy(x => x.Account).Select(grp => grp.ToList()).ToList().ForEach(acct =>
            {
                MakeNewTab(acct, count);
                count++;
            });
        }

        private void TotalSheetBuild()
        {
            int row = 2;
            _data.GroupBy(x => x.Account).Select(grp => grp.ToList()).ToList().ForEach(act =>
            {
                WorkSheet.Cells[row, 1].Value = act.First().Account;
                WorkSheet.Cells[row, 2].Value = act.Sum(m => m.MTDSightlySpend);
                row++;
            });

            //Get total in bold
            WorkSheet.Cells[row, 2].Style.Font.Bold = true;
            WorkSheet.Cells[row, 2].Value = _data.Sum(x => x.MTDSightlySpend);
        }

        private void MakeNewTab(List<BillingData> acct, int count)
        {
            string name = acct.First().Account;
            Report.Workbook.Worksheets.Add(name);
            var worksheet = Report.Workbook.Worksheets[count];
            worksheet.Name = name;
            BuildHeader(worksheet);
            var row = 2;
            acct.ForEach(act =>
            {
                worksheet.Cells[row, 1].Value = act.Campaign;
                worksheet.Cells[row, 2].Value = act.OrderStatus;
                worksheet.Cells[row, 3].Value = act.StartDate.ToString(_dateFormatHeader);
                worksheet.Cells[row, 4].Value = act.EndDate.ToString(_dateFormatHeader);
                worksheet.Cells[row, 5].Style.Numberformat.Format = MONEY_FORMAT;
                worksheet.Cells[row, 5].Value = act.SightlyBudget;
                worksheet.Cells[row, 6].Style.Numberformat.Format = MONEY_FORMAT;
                worksheet.Cells[row, 6].Value = act.MTDSightlySpend;
                row++;
            });

            worksheet.Cells[row, 6].Style.Numberformat.Format = MONEY_FORMAT;
            worksheet.Cells[row, 6].Style.Font.Bold = true;
            worksheet.Cells[row, 6].Value = acct.Sum(x => x.MTDSightlySpend);
        }

        private void BuildHeader(ExcelWorksheet worksheet)
        {
            worksheet.Cells[1, 1].Value = "CampaignName";
            worksheet.Cells[1, 2].Value = "OrderStatus";
            worksheet.Cells[1, 3].Value = "IOStartDate";
            worksheet.Cells[1, 4].Value = "IOEndDate";
            worksheet.Cells[1, 5].Value = "SightlyBudget";
            worksheet.Cells[1, 6].Value = "Billable";
        }
    }
}