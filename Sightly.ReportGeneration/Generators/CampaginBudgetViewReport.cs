﻿using System;
using System.Collections.Generic;
using Reporting.DAL.DTO;
using Sightly.ReportGeneration.Generators.Interface;

namespace Sightly.ReportGeneration.Generators
{
    public class CampaginBudgetViewReport : AExcelReport, IExcelReport
    {
        #region Constants

        private readonly string INT_FORMAT = "#,###";
        private readonly string PERCENTAGE_FORMAT = "#0.00%";
        private readonly string MONEY_FORMAT = "$0.00";
        private readonly string _dateFormat = "MM/dd/yyyy";
        private readonly string NUMBER_FORMAT = "#,##0";

        #endregion

        #region Variables

        private List<CampaignBudgetReport> _data;
        private DateTime _now;

        #endregion
        public CampaginBudgetViewReport(List<CampaignBudgetReport> campaignBudgetData) : base(
            "Sightly.ReportGeneration.Templates.Excel.CampaignBudgetViewReport.xlsx")
        {
            _data = campaignBudgetData;
            _now = DateTime.Now;

            Build();
        }

        public void Build()
        {
            var counter = 1;
            _data.ForEach(row =>
            {
                counter++;
                WorkSheet.Cells[counter, 1].Value = row.AdwordsCampaignName;
                WorkSheet.Cells[counter, 2].Style.Numberformat.Format = NUMBER_FORMAT;
                WorkSheet.Cells[counter, 2].Value = row.Impressions;
                WorkSheet.Cells[counter, 3].Style.Numberformat.Format = NUMBER_FORMAT;
                WorkSheet.Cells[counter, 3].Value = row.Views;
                WorkSheet.Cells[counter, 4].Style.Numberformat.Format = PERCENTAGE_FORMAT;
                WorkSheet.Cells[counter, 4].Value = row.ViewRate;
                WorkSheet.Cells[counter, 5].Style.Numberformat.Format = MONEY_FORMAT;
                WorkSheet.Cells[counter, 5].Value = row.CPV;
                WorkSheet.Cells[counter, 6].Style.Numberformat.Format = NUMBER_FORMAT;
                WorkSheet.Cells[counter, 6].Value = row.Clicks;
                WorkSheet.Cells[counter, 7].Style.Numberformat.Format = PERCENTAGE_FORMAT;
                WorkSheet.Cells[counter, 7].Value = row.CTR;
                WorkSheet.Cells[counter, 8].Style.Numberformat.Format = MONEY_FORMAT;
                WorkSheet.Cells[counter, 8].Value = row.Spend;
                WorkSheet.Cells[counter, 9].Style.Numberformat.Format = NUMBER_FORMAT;
                WorkSheet.Cells[counter, 9].Value = row.VideoCompletion25;
                WorkSheet.Cells[counter, 10].Style.Numberformat.Format = NUMBER_FORMAT;
                WorkSheet.Cells[counter, 10].Value = row.VideoCompletion50;
                WorkSheet.Cells[counter, 11].Style.Numberformat.Format = NUMBER_FORMAT;
                WorkSheet.Cells[counter, 11].Value = row.VideoCompletion75;
                WorkSheet.Cells[counter, 12].Style.Numberformat.Format = NUMBER_FORMAT;
                WorkSheet.Cells[counter, 12].Value = row.VideoCompletion100;
            });
        }
    }
}