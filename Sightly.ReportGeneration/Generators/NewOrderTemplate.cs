﻿namespace Sightly.ReportGeneration.Generators
{
    public class NewOrderTemplate : AExcelReport
    {
        private string _orderName;

        public NewOrderTemplate(string orderName) : base("Sightly.ReportGeneration.Templates.Excel.MediaAgencyPlacementReport.xlsx")
        {
            _orderName = orderName;
            
        }

        public void Build()
        {
            WorkSheet.Cells[1, 1].Value = "Order Name";
            WorkSheet.Cells[2, 1].Value = _orderName;
            WorkSheet.Cells[2, 10].Value = _orderName;


        }
    }
}