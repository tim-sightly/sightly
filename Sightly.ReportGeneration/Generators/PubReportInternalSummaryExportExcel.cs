﻿using System;
using Reporting.DAL.DTO;
using Sightly.ReportGeneration.Generators.Interface;

namespace Sightly.ReportGeneration.Generators
{
    public class PubReportInternalSummaryExportExcel : AExcelReport, IExcelReport
    {
        #region Constants

        private readonly string INT_FORMAT = "#,###";
        private readonly string PERCENTAGE_FORMAT = "#0.00%";
        private readonly string MONEY_FORMAT = "$0.00";
        private readonly string _dateFormat = "MM/dd/yyyy";
        private readonly string NUMBER_FORMAT = "#,##0";

        #endregion

        #region Variables

        private CampaignPubReportCollection _data;
        private DateTime _now;

        public string CampaignName { get; set; }

        #endregion
        public PubReportInternalSummaryExportExcel(CampaignPubReportCollection pubReportByCustomerId) : base(
            "Sightly.ReportGeneration.Templates.Excel.PubInternalReportSummary.xlsx")
        {
            _data = pubReportByCustomerId;

            Build();
        }

        private void Build()
        {
            var counter = 1;
            _data.CampaignPubReports.ForEach(row =>
            {
                counter++;
                WorkSheet.Cells[counter, 1].Value = row.CID;
                WorkSheet.Cells[counter, 2].Value = row.CampaignName;
                WorkSheet.Cells[counter, 3].Value = row.PlacementName;
                WorkSheet.Cells[counter, 4].Value = row.PlacementID;
                WorkSheet.Cells[counter, 5].Value = row.StartDate.ToString(_dateFormat);
                WorkSheet.Cells[counter, 6].Value = row.EndDate.ToString(_dateFormat);
                WorkSheet.Cells[counter, 7].Style.Numberformat.Format = "#,##0";
                WorkSheet.Cells[counter, 7].Value = row.Impressions;
                WorkSheet.Cells[counter, 8].Style.Numberformat.Format = "#,##0";
                WorkSheet.Cells[counter, 8].Value = row.Clicks;
                WorkSheet.Cells[counter, 9].Style.Numberformat.Format = "#,##0";
                WorkSheet.Cells[counter, 9].Value = row.VideoCompletion25;
                WorkSheet.Cells[counter, 10].Style.Numberformat.Format = "#,##0";
                WorkSheet.Cells[counter, 10].Value = row.VideoCompletion50;
                WorkSheet.Cells[counter, 11].Style.Numberformat.Format = "#,##0";
                WorkSheet.Cells[counter, 11].Value = row.VideoCompletion75;
                WorkSheet.Cells[counter, 12].Style.Numberformat.Format = "#,##0";
                WorkSheet.Cells[counter, 12].Value = row.VideoCompletion100;
                WorkSheet.Cells[counter, 13].Style.Numberformat.Format = "#,##0";
                WorkSheet.Cells[counter, 13].Value = row.Views;
                WorkSheet.Cells[counter, 14].Value = row.Cost;

            });
        }
    }
}