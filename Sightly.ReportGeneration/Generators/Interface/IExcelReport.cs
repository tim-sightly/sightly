﻿using System.IO;

namespace Sightly.ReportGeneration.Generators.Interface
{
    public interface IExcelReport
    {
        MemoryStream ToStream();
    }
}