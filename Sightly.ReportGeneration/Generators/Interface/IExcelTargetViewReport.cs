﻿using System.IO;

namespace Sightly.ReportGeneration.Generators.Interface
{
    public interface IExcelTargetViewReport
    {
        string Build();
        MemoryStream ToStream(string filename);
    }
}