﻿using System;
using Reporting.DAL.DTO;
using Sightly.ReportGeneration.Generators.Interface;

namespace Sightly.ReportGeneration.Generators
{
    public class MediaAgencyCampaignPlacementExportExcel : AExcelReport, IExcelReport
    {
        #region Constants
        
        private readonly string INT_FORMAT = "#,###";
        private readonly string PERCENTAGE_FORMAT = "#0.00%";
        private readonly string MONEY_FORMAT = "$0.00";
        private readonly string _dateFormat = "MM/dd/yyyy";

        #endregion

        #region Variables

        private CampaignPlacementReport _data;
        private DateTime _now;

        public string CampaignName {
            get
            {
                return _data.CampaignName;
            }
        }
        
        #endregion

        public MediaAgencyCampaignPlacementExportExcel(CampaignPlacementReport campaignPlacementReport) : base("Sightly.ReportGeneration.Templates.Excel.MediaAgencyCampaignPlacementReport.xlsx")
        {
            _data = campaignPlacementReport;

            Build();
        }

        private void Build()
        {
            var counter = 1;
            _data.MediaAgencyCampaignPlacements.ForEach(row =>
            {
                counter++;
                WorkSheet.Cells[counter, 1].Value = row.TargetViewCampaignName;
                WorkSheet.Cells[counter, 2].Value = row.AdwordsCID;
                WorkSheet.Cells[counter, 3].Value = row.AdwordsCampaignId.ToString();
                WorkSheet.Cells[counter, 4].Value = row.AdwordsCampaignName;
                WorkSheet.Cells[counter, 5].Value = row.StartDate.ToString(_dateFormat);
                WorkSheet.Cells[counter, 6].Value = row.EndDate.ToString(_dateFormat);
                WorkSheet.Cells[counter, 7].Value = row.CampaignBudget;
                WorkSheet.Cells[counter, 8].Value = row.Margin;
                WorkSheet.Cells[counter, 9].Value = row.PlacementValue;
                WorkSheet.Cells[counter, 10].Value = row.PlacementName;
                WorkSheet.Cells[counter, 11].Value = row.BudgetGroupTimedBudgetId;
                WorkSheet.Cells[counter, 12].Value = row.CampaignManagerEmail;
            });
        }
    }
}