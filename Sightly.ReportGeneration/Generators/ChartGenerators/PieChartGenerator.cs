﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting;

namespace Sightly.ReportGeneration.Generators.ChartGenerators
{
    public class PieChartGenerator
    {
        #region Private Methods
        private ChartArea BuildChartArea()
        {
            var chartArea = new ChartArea();
            chartArea.BackColor = Color.Transparent;

            return chartArea;
        }

        private Chart BuildChartControl(int height, int width, string title, Series series)
        {
            var pieChart = new Chart();

            pieChart.Legends.Add(new Legend());
            pieChart.Height = height;
            pieChart.Width = width;
            pieChart.Location = new Point(0, 0);
            pieChart.ChartAreas.Add(BuildChartArea());
            pieChart.BackColor = Color.White;
            pieChart.Titles.Add(title);
            pieChart.Series.Clear();
            pieChart.Series.Add(series);

            return pieChart;
        }

        private Series BuildChartSeries(List<ChartPoint> chartPoints)
        {
            var series = new Series();
            series.IsVisibleInLegend = true;
            series.ChartType = SeriesChartType.Pie;

            foreach (var point in chartPoints)
            {
                var dataPoint = BuildDataPoint(point);

                series.Points.Add(dataPoint);
            }

            return series;
        }

        private DataPoint BuildDataPoint(ChartPoint point)
        {
            var dataPoint = new DataPoint();
            var fontSize = 6;
            var fontFamilyName = "Open Sans";

            dataPoint.SetValueY(point.Value);
            dataPoint.LegendText = point.Name;

            if (!string.IsNullOrEmpty(point.ColorHex))
                dataPoint.Color = ColorTranslator.FromHtml(point.ColorHex);

            dataPoint.Font = new Font(fontFamilyName, fontSize, FontStyle.Regular, GraphicsUnit.Point);

            return dataPoint;
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Method used for generating a pie chart image to
        /// a specific path.
        /// </summary>
        /// <param name="chartPoints">
        ///     List of chart points to be displayed.
        /// </param>
        /// <param name="height">
        ///     Height of chart image to be generated.
        /// </param>
        /// <param name="width">
        ///     Width of chart image to be generated.
        /// </param>
        /// <param name="title">
        ///     Title of chart to be generated.
        /// </param>
        /// <param name="filePath">
        ///     Path on which the chart would be generated at.
        /// </param>
        protected void GenerateImageToPath(List<ChartPoint> chartPoints, int height, int width, string title, string filePath)
        {
            var series = BuildChartSeries(chartPoints);
            var pieChart = BuildChartControl(height, width, title, series);
            var directoryPath = Path.GetDirectoryName(filePath);

            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

            pieChart.SaveImage(filePath, ChartImageFormat.Png);
        }

        protected MemoryStream GenerateImageToStream(List<ChartPoint> chartPoints, int height, int width, string title)
        {
            MemoryStream ms = new MemoryStream();
            var series = BuildChartSeries(chartPoints);
            var pieChart = BuildChartControl(height, width, title, series);
            pieChart.SaveImage(ms, ChartImageFormat.Png);
            ms.Position = 0;
            return ms;
        }
        #endregion
    }
}