﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using Reporting.DAL.DTO.TargetView;

namespace Sightly.ReportGeneration.Generators.ChartGenerators
{
    public class AgeGroupViewsPieChartGenerator : PieChartGenerator
    {
        private const string CHART_TILE = "Views by Known Age";
        private readonly string[] UNKNOWN_AGE_GROUPS = new string[] { "Unknown", "Impt-Must assign" };

        public MemoryStream Execute(List<AgeGroupPerformanceDetail> ageGroupStats)
        {
            var knownAgeGroupStats = ageGroupStats.Where(ag => UNKNOWN_AGE_GROUPS.All(uag => uag != ag.AgeGroupName))
                .ToList();

            var chartPoints = new List<ChartPoint>();

            foreach (var stat in knownAgeGroupStats)
            {
                var value = stat.CompletedViews.HasValue ? (double)stat.CompletedViews.Value : 0D;

                chartPoints.Add(new ChartPoint()
                {
                    Name = stat.AgeGroupName,
                    ColorHex = stat.Color,
                    Value = value
                });
            }

            return GenerateImageToStream(chartPoints, 174, 300, CHART_TILE);
        }
    }
}