﻿using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms.DataVisualization.Charting;

namespace Sightly.ReportGeneration.Generators.ChartGenerators
{
    public class BarChartGenerator
    {
        #region Private Methods
        private ChartArea BuildChartArea()
        {
            var chartArea = new ChartArea();
            var fontSize = 7;
            var fontFamilyName = "Open Sans";

            chartArea.BackColor = Color.Transparent;
            chartArea.AxisX.MajorGrid.LineWidth = 0;
            chartArea.AxisY.MajorGrid.LineWidth = 0;
            chartArea.AxisX.IsLabelAutoFit = false;
            chartArea.AxisX.LabelStyle.Font = new Font(fontFamilyName, fontSize, FontStyle.Regular, GraphicsUnit.Point);

            return chartArea;
        }

        private Chart BuildChartControl(int height, int width, string title, Series series)
        {
            var barChart = new Chart();

            barChart.Height = height;
            barChart.Width = width;
            barChart.Location = new Point(0, 0);
            barChart.ChartAreas.Add(BuildChartArea());
            barChart.BackColor = Color.White;
            barChart.Titles.Add(title);
            barChart.Series.Clear();
            barChart.Series.Add(series);

            return barChart;
        }

        private Series BuildChartSeries(List<ChartPoint> chartPoints)
        {
            var series = new Series();
            series.ChartType = SeriesChartType.Bar;

            foreach (var point in chartPoints)
            {
                var dataPoint = BuildDataPoint(point);

                series.Points.Add(dataPoint);
            }

            return series;
        }

        private DataPoint BuildDataPoint(ChartPoint point)
        {
            var dataPoint = new DataPoint();

            dataPoint.SetValueY(point.Value);
            dataPoint.AxisLabel = point.Name;
            dataPoint.Color = ColorTranslator.FromHtml(point.ColorHex);

            return dataPoint;
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// Method used for generating a bar chart image to
        /// a specific path.
        /// </summary>
        /// <param name="chartPoints">
        ///     List of chart points to be displayed.
        /// </param>
        /// <param name="height">
        ///     Height of chart image to be generated.
        /// </param>
        /// <param name="width">
        ///     Width of chart image to be generated.
        /// </param>
        /// <param name="title">
        ///     Title of chart to be generated.
        /// </param>
        /// <param name="filePath">
        ///     Path on which the chart would be generated at.
        /// </param>
        protected void GenerateImageToPath(
            List<ChartPoint> chartPoints,
            int height, int width,
            string title, string filePath
        )
        {
            var series = BuildChartSeries(chartPoints);
            var barChart = BuildChartControl(height, width, title, series);
            var directoryPath = Path.GetDirectoryName(filePath);

            if (!Directory.Exists(directoryPath))
                Directory.CreateDirectory(directoryPath);

            barChart.SaveImage(filePath, ChartImageFormat.Png);
        }

        protected MemoryStream GenerateImageToStream(List<ChartPoint> chartPoints, int height, int width, string title)
        {
            MemoryStream ms = new MemoryStream();
            var series = BuildChartSeries(chartPoints);
            var barChart = BuildChartControl(height, width, title, series);
            barChart.SaveImage(ms, ChartImageFormat.Png);
            ms.Position = 0;
            return ms;
        }
        #endregion
    }
}