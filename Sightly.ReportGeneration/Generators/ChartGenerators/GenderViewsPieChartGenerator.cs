﻿using System.Collections.Generic;
using System.IO;
using Reporting.DAL.DTO.TargetView;

namespace Sightly.ReportGeneration.Generators.ChartGenerators
{
    public class GenderViewsPieChartGenerator : PieChartGenerator
    {
        private const string CHART_TILE = "Views by Gender";

        public MemoryStream Execute(List<GenderPerformanceDetail> genderStats)
        {
            var chartPoints = new List<ChartPoint>();

            foreach (var stat in genderStats)
            {
                var value = stat.CompletedViews.HasValue ? (double)stat.CompletedViews.Value : 0D;

                chartPoints.Add(new ChartPoint()
                {
                    Name = stat.GenderName,
                    ColorHex = stat.Color,
                    Value = value
                });
            }

            return GenerateImageToStream(chartPoints, 195, 300, CHART_TILE);
        }
    }
}