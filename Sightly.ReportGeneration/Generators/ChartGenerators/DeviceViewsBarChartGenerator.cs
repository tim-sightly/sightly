﻿using System.Collections.Generic;
using System.IO;
using Reporting.DAL.DTO.TargetView;
using ChartArea = DocumentFormat.OpenXml.Office2013.Drawing.ChartStyle.ChartArea;

namespace Sightly.ReportGeneration.Generators.ChartGenerators
{
    public class DeviceViewsBarChartGenerator : BarChartGenerator
    {
        private const string CHART_TILE = "Views by Device";
        public MemoryStream Execute(List<DeviceTypePerformanceDetail> deviceStats)
        {
            var chartPoints = new List<ChartPoint>();

            foreach (var stat in deviceStats)
            {
                var value = stat.CompletedViews.HasValue ? (double)stat.CompletedViews.Value : 0D;

                chartPoints.Add(new ChartPoint()
                {
                    Name = stat.DeviceTypeName,
                    ColorHex = stat.Color,
                    Value = value
                });
            }

            return GenerateImageToStream(chartPoints, 174, 300, CHART_TILE);
        }

        //private MemoryStream GenerateImageToStream(chartPoints, 174, 300, CHART_TILE)
        //{
        //    MemoryStream ms = new MemoryStream();
        //    var series = BuildChartSeries(chartPoints);
        //    var barChart = BuildChartControl(height, width, title, series);
        //    barChart.SaveImage(ms, ChartImageFormat.Png);
        //    ms.Position = 0;
        //    return ms;
        //}

        //private void GenerateImageToPath(
        //    List<ChartPoint> chartPoints,
        //    int height, int width,
        //    string title, string filePath
        //)
        //{
        //    var series = BuildChartSeries(chartPoints);
        //    var barChart = BuildChartControl(height, width, title, series);
        //    var directoryPath = Path.GetDirectoryName(filePath);

        //    if (!Directory.Exists(directoryPath))
        //        Directory.CreateDirectory(directoryPath);

        //    barChart.SaveImage(filePath, ChartImageFormat.Png);
        //}

        //private ChartArea BuildChartArea()
        //{
        //    var chartArea = new ChartArea();
        //    var fontSize = 7;
        //    var fontFamilyName = "Open Sans";

        //    chartArea.BackColor = Color.Transparent;
        //    chartArea.AxisX.MajorGrid.LineWidth = 0;
        //    chartArea.AxisY.MajorGrid.LineWidth = 0;
        //    chartArea.AxisX.IsLabelAutoFit = false;
        //    chartArea.AxisX.LabelStyle.Font = new Font(fontFamilyName, fontSize, FontStyle.Regular, GraphicsUnit.Point);

        //    return chartArea;
        //}
    }
}