﻿namespace Sightly.ReportGeneration.Generators.ChartGenerators
{
    public class ChartPoint
    {
        public string Name { get; set; }
        public string ColorHex { get; set; }
        public double Value { get; set; }
    }
}