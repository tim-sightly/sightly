﻿using System.IO;
using System.Text;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Sightly.ReportGeneration.Storage
{
    public class StorageManager: IStorageManager
    {
        private readonly string _connectionString;
        private CloudStorageAccount _storageAccount;
        private readonly CloudBlobClient _cloudBlobClient;
        private readonly CloudBlobContainer _cloudBlobContainer;
        private CloudBlockBlob _cloudBlockBlob;
        private readonly string _fileName;
        private string _fileUrl;

        public StorageManager(string container, string fileName = null) : this()
        {
            _cloudBlobContainer = _cloudBlobClient.GetContainerReference(container);
            _fileName = fileName;
        }

        private StorageManager()
        {
            _connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["AzureFileStorage"].ConnectionString;
            _storageAccount = CloudStorageAccount.Parse(_connectionString);
            _cloudBlobClient = _storageAccount.CreateCloudBlobClient();
        }

        public string CreateFile(string content, string fileName = null)
        {
            _cloudBlockBlob = _cloudBlobContainer.GetBlockBlobReference(fileName ?? _fileName);
            var memoryStream = new MemoryStream(Encoding.UTF8.GetBytes(content ?? ""));
            _cloudBlockBlob.UploadFromStream(memoryStream);
            _fileUrl = _cloudBlockBlob.Uri.ToString();
            return _fileUrl;
        }

        public string CreateFile(MemoryStream ms, string fileName = null)
        {
            _cloudBlockBlob = _cloudBlobContainer.GetBlockBlobReference(fileName ?? _fileName);
            ms.Seek(0, SeekOrigin.Begin);
            _cloudBlockBlob.UploadFromStream(ms);
            _fileUrl = _cloudBlockBlob.Uri.ToString();
            return _fileUrl;
        }

        public MemoryStream ReadFileAsStream(string fileName = null)
        {
            MemoryStream ms = new MemoryStream();
            _cloudBlockBlob = _cloudBlobContainer.GetBlockBlobReference(fileName ?? _fileName);
            _fileUrl = _cloudBlockBlob.Uri.ToString();
            _cloudBlockBlob.DownloadToStream(ms);
            ms.Seek(0, SeekOrigin.Begin);
            return ms;
        }

        public byte[] ReadFileAsByteArray(string fileName = null)
        {
            _cloudBlockBlob = _cloudBlobContainer.GetBlockBlobReference(fileName ?? _fileName);
            _cloudBlockBlob.FetchAttributes();
            _fileUrl = _cloudBlockBlob.Uri.ToString();
            long byteArrayLength = _cloudBlockBlob.Properties.Length;
            byte[] ba = new byte[byteArrayLength];
            _cloudBlockBlob.DownloadToByteArray(ba, 0);
            return ba;
        }

        public string FileUrl => _fileUrl;
    }
}