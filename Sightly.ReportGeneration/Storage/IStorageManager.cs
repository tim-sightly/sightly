﻿using System.IO;

namespace Sightly.ReportGeneration.Storage
{
    public interface IStorageManager
    {
        string CreateFile(string content, string fileName = null);

        string CreateFile(MemoryStream ms, string fileName = null);
        MemoryStream ReadFileAsStream(string fileName = null);
        byte[] ReadFileAsByteArray(string fileName = null);
        string FileUrl { get; }
    }
}
