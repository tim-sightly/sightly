﻿using System;

namespace Sightly.ReportGeneration.Models
{
    public class PerformanceSummaryVM
    {
        public string orderIds { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
    }
}