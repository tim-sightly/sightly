﻿namespace Sightly.ReportGeneration.Models
{
    public class Place
    {
        public string Address { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        
    }
}