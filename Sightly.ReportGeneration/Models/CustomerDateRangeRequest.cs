﻿using System;
using System.Collections.Generic;

namespace Sightly.ReportGeneration.Models
{
    public class CustomerDateRangeRequest
    {
        public List<long> Customers { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsSummary { get; set; }
    }
    public class OrderDateRangeRequest
    {
        public List<Guid> Orders { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}