﻿using System.Collections.Generic;

namespace Sightly.ReportGeneration.Models
{
     public class LatLongReportRequest
        {
            public List<Place> Places { get; set; }
        }
    
}