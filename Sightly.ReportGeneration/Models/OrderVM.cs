﻿using System;

namespace Sightly.ReportGeneration.Models
{
    public class OrderVM
    {
        public Guid orderId { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
    
    }
}