﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Net.Mime;

namespace Sightly.Email
{
    public class EmailData
    {
        public MailAddress From { get; }
        public List<string> To { get; }
        public List<string> Cc { get; }
        public string Subject { get; }
        public string Body { get; }

        public Attachment SomeAttachment { get; }

        public EmailData(List<string> toRecipients, string fromSender, string subjectText, string bodyText, List<string> cc = null, Attachment attachment = null)
        {
            To = toRecipients;
            From = new MailAddress(fromSender);
            Subject = subjectText;
            Body = bodyText;
            Cc = cc;
            SomeAttachment = attachment;
        }

        public MailMessage SendToMailMessage()
        {
            MailMessage message = new MailMessage();
            message.To.Add(string.Join(",", To));
            if (Cc != null)
            {
                message.CC.Add(string.Join(",", Cc));
            }
            message.From = From;
            message.Subject = Subject;
            message.IsBodyHtml = true;
            message.Body = "<html><body style =\"margin-left:15px;margin-right:15px;color:#333D47;font-size:1.130em;font-family:Helvetica Neue,Helvetica,Arial,Lucida Grande,sans-serif;\">" + Body;
            if (SomeAttachment != null)
            {
                message.Attachments.Add(SomeAttachment);
            }

            return message;
        }
    }
}
