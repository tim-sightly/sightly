﻿using System.Collections.Generic;
using Sightly.Models;


namespace Sightly.Email.Templates
{
    public class AdSwapReady
    {
        public AdSwapReady() { }

        public EmailData MakeAdSwapReady(AdSwapData adSwapData)
        {
            //var to = new List<string> {adSwapData.Email, "dev-test@sightly.com" };
            //var from = "noreply@sightly.com";
            //var subject = $"TEST - Ad swap complete for Customer: {adSwapData.CustomerName}";

            var to = new List<string> {  adSwapData.Email };
            var from = "noreply@sightly.com";
            var subject = $"Ad swap complete for Customer: {adSwapData.CustomerName}";
            var body = "<p class=\"MsoNormal\"><span style=\"font-size: 14pt; font-family: &quot;Open Sans&quot;;\"></span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-size: 14pt; font-family: &quot;Open Sans&quot;;\">Your Ads have been all <b>SWAPPED</b> out!</span></p>" +
                       $"<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">The swapping of Ads was completed for the Customer: {adSwapData.CustomerName} - {adSwapData.CustomerId}.</span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"></span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">If you have any questions or concerns, do not reply to this email as it is sent by our system. Instead, please email us at support@sightly.com.</span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">Cheers,</span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p>" +
                       " <p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">The Sightly Team</span></p></body></html>";

            var emailData = new EmailData(to, from, subject, body);
            return emailData;
        }
        
    }
}