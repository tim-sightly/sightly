﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Email.Templates
{
    public class NewOrderReady
    {
        public NewOrderReady()
        { }

        public EmailData MakeNewOrderReady(OrderEmailData orderData)
        {
            //var to = new List<string> { orderData.CreaterName, "dev-test@sightly.com" };
            //var from = "noreply@sightly.com";
            //var subject = $"TEST - New/Renewal Order Now Ready: {orderData.OrderName}";

            var to = new List<string> { "adops@sightly.com", orderData.CreaterName };
            var from = "noreply@sightly.com";
            var subject = $"New/Renewal Order  Request: {orderData.AccountName} - {orderData.AdvertiserName} - {orderData.CampaignName}";
            var body = "<p class=\"MsoNormal\"><span style=\"font-size: 14pt; font-family: &quot;Open Sans&quot;;\"> </span></p> " +
                       "<p class=\"MsoNormal\"><span style=\"font-size: 14pt; font-family: &quot;Open Sans&quot;;\">New Order Status Now Ready</span></p>" +
                       $"<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">The status of your new video advertising order {orderData.OrderName} for {orderData.AdvertiserName} has changed to <b>Ready</b>.</span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p>" +
                       "<p class=\"MsoNormal\"><b><span style=\"font-family: &quot;Open Sans&quot;;\">What This Means</span></b> <span style=\"font-family: &quot;Open Sans&quot;;\">– The Ready status in TargetView means your order is ready to start running once YouTube approves your ad(s) and theorder’s start date arrives. <b>You do not have to take any action. </b>Once the order starts running, its status will change to Live and TargetView will notify you.</span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p>" +
                       "<p style =\"font-style: italic; font-weight: bold\" class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">Order Summary</span></p>" +
                       "<table class=\"gmail-MsoTableGrid\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse: collapse; border: none;\"><tbody>" +
                       "<tr style =\"height: 19.8pt;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 19.8pt;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Account</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 19.8pt;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.AccountName}</span></p></td></tr>" +
                       "<tr style =\"height: 19.8pt;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 19.8pt;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Advertiser</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 19.8pt;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.AdvertiserName}</span></p></td></tr>" +
                       "<tr style =\"height: 19.8pt;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 19.8pt;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Order Ref Code</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 19.8pt;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.OrderRefCode}</span></p></td></tr>" +
                       "<tr style =\"height: 0.25in;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 0.25in;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Start Date</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 0.25in;\"><p class=\"MsoNormal\"><span style=\"font-size: 13.3333px; font-weight: bold\">{orderData.StartDate}</span></p></td></tr>" +
                       "<tr style =\"height: 0.25in;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 0.25in;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">End Date</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 0.25in;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.EndDate}</span></p></td></tr>" +
                       "<tr style =\"height: 0.25in;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 0.25in;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Total Budget</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 0.25in;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">${orderData.TotalBudget}</span></p></td></tr>" +
                       "<tr style =\"height: 0.25in;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 0.25in;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Notes</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 0.25in;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">${orderData.Notes}</span></p></td></tr></tbody></table>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">If you have any questions or concerns, do not reply to this email as it is sent by our system. Instead, please contact us through the <a href=\"https://sightly.atlassian.net/servicedesk/customer/portal/7\">Sightly Service Desk</a>.</span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p><p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">Cheers,</span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p><p class=\"MsoNormal\" style=\"font-weight: bold;\"></p><p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">The Sightly Team</span></p></body></html>";

            var emailData = new EmailData(to, from, subject, body);
            return emailData;
        }

    }
}
