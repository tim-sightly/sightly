﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sightly.Models;

namespace Sightly.Email.Templates
{
    public class OrderCancelledStatusChange
    {
        public OrderCancelledStatusChange() { }

        public EmailData MakeCancelledOrderStatusChange(OrderCancelData orderData)
        {
            var to = new List<string> { orderData.CurrentUserEmail };
            var from = "noreply@sightly.com";
            var subject = $"Cancel Order Request Received: {orderData.OrderName}";
            var body = "<p class=\"MsoNormal\"><span style=\"font-size: 14pt; font-family: &quot;Open Sans&quot;;\"> </span></p> " +
                       $"<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"></span></p>" +
                       $"<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">Your request to cancel video advertising order {orderData.OrderName} for {orderData.AdvertiserName} has been received by Sightly for processing. NOTE: It may take up to 24 business hours for this action to take effect.</span></p>" +
                       "<p style =\"font-style: italic; font-weight: bold\" class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">Set the following order to CANCEL</span></p>" +
                       "<table class=\"gmail-MsoTableGrid\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border-collapse: collapse; border: none;\"><tbody>" +
                       "<tr style =\"height: 19.8pt;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 19.8pt;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Submitted By:</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 19.8pt;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.CurrentUserEmail}</span></p></td></tr>" +
                       "<tr style =\"height: 19.8pt;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 19.8pt;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Submitted Date:</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 19.8pt;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.ActionDate}</span></p></td></tr>" +
                       "<tr style =\"height: 19.8pt;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 19.8pt;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Account:</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 19.8pt;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.AccountName}</span></p></td></tr>" +
                       "<tr style =\"height: 19.8pt;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 19.8pt;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Advertiser:</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 19.8pt;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.AdvertiserName}</span></p></td></tr>" +
                       "<tr style =\"height: 19.8pt;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 19.8pt;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Order Name:</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 19.8pt;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.OrderName}</span></p></td></tr>" +
                       "<tr style =\"height: 19.8pt;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 19.8pt;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Order Ref Code:</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 19.8pt;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.OrderRefCode}</span></p></td></tr>" +
                       "<tr style =\"height: 0.25in;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 0.25in;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Start Date:</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 0.25in;\"><p class=\"MsoNormal\"><span style=\"font-size: 13.3333px; font-weight: bold\">{orderData.StartDate}</span></p></td></tr>" +
                       "<tr style =\"height: 0.25in;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 0.25in;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">End Date:</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 0.25in;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.EndDate}</span></p></td></tr>" +
                       "<tr style =\"height: 0.25in;\"><td width=\"126\" style=\"width: 94.5pt; padding: 0in 5.4pt; height: 0.25in;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Total Budget:</span></p></td>" +
                       $"<td width =\"497\" style=\"width: 373pt; padding: 0in 5.4pt; height: 0.25in;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">${orderData.TotalBudget}</span></p></td></tr></tbody></table>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">If you have any questions or concerns, do not reply to this email as it is sent by our system. Instead, please contact us through the <a href=\"https://sightly.atlassian.net/servicedesk/customer/portal/7\">Sightly Service Desk</a>.</span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p><p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">Cheers,</span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p><p class=\"MsoNormal\" style=\"font-weight: bold;\"></p><p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">The Sightly Team</span></p></body></html>";

            var emailData = new EmailData(to, from, subject, body);
            return emailData;
        }
    }
}
