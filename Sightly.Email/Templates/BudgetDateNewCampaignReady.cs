﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Email.Templates
{
    public class BudgetDateNewCampaignReady
    {
        public BudgetDateNewCampaignReady() { }

        public EmailData MakeBudgetDateNewCampaignReady(BudgetDateNewCampaignData budgetDateNewCampaign)
        {
            //var to = new List<string> { budgetDateNewCampaign.Email, "dev-test@sightly.com" };
            //var from = "noreply@sightly.com";
            //var subject = $"TEST - Budget - Dates - New Campaigns completed for Customer: {budgetDateNewCampaign.CustomerName}";

            var to = new List<string> {budgetDateNewCampaign.Email };
            var from = "noreply@sightly.com";
            var subject = $"Budget - Dates - New Campaigns completed for Customer: {budgetDateNewCampaign.CustomerName}";
            var body = "<p class=\"MsoNormal\"><span style=\"font-size: 14pt; font-family: &quot;Open Sans&quot;;\"></span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-size: 14pt; font-family: &quot;Open Sans&quot;;\">Budget and Date Changes or new Campaigns are complete!</p>" +
                       $"<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">Budget - Dates - New Campaigns  was completed for the Customer: {budgetDateNewCampaign.CustomerName} - {budgetDateNewCampaign.CustomerId}.</span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"></span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">If you have any questions or concerns, do not reply to this email as it is sent by our system. Instead, please email us at support@sightly.com.</span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">Cheers,</span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p>" +
                       " <p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">The Sightly Team</span></p></body></html>";

            var emailData = new EmailData(to, from, subject, body);
            return emailData;
        }
    }
}
