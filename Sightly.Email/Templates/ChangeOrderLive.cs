﻿using System.Collections.Generic;
using Sightly.Models;

namespace Sightly.Email.Templates
{
    public class ChangeOrderLive
    {
        public ChangeOrderLive() { }

        public EmailData MakeChangeOrderLive(OrderEmailData orderData)
        {
            //var to = new List<string> { orderData.CreaterName, "dev-test@sightly.com" };
            //var from = "noreply@sightly.com";
            //var subject = $"TEST - Change Order Change Now Live: : {orderData.OrderName}";

            var to = new List<string> { orderData.CreaterName };
            var from = "noreply@sightly.com";
            var subject = $"Change Order Change Now Live: : {orderData.OrderName}";
            var body =
                "<p class=\"MsoNormal\"><span style=\"font-size: 14pt; font-family: &quot;Open Sans&quot;;\">Change Order Changes Now Live</span></p> " +
                $"<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">The changes you submitted in your video advertising change order {orderData.OrderName} for {orderData.AdvertiserName} have gone live.</span></p>" +
                "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p>" +
                "<p class=\"MsoNormal\"><b><span style=\"font-family: &quot;Open Sans&quot;;\">What This Means</span></b> <span style=\"font-family: &quot;Open Sans&quot;;\">– The changes you submitted are now incorporated into the original Live order in TargetView. You can see the changes by clicking on the View Order icon (magnifying glass) for the order in the Orders list.</span></p>" +
                "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"></span></p>" +
                "<p style =\"font-style: italic; font-weight: bold\" class=\"MsoNormal\">" +
                "<span style =\"font-family: &quot;Open Sans&quot;;\">Order Summary</span></p>" +
                "<table class=\"gmail-MsoTableGrid\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"style=\"border-collapse: collapse; border: none;\"><tbody>" +
                "<tr style =\"height: 19.8pt;\"><td width=\"156\" valign=\"top\" style=\"width: 117pt; padding: 0in 5.4pt; height: 19.8pt;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Advertiser</span></p></td>" +
                $"<td width =\"467\" valign=\"top\" style=\"width: 350.5pt; padding: 0in 5.4pt; height: 19.8pt;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.AdvertiserName}</span></p></td></tr>" +
                "<tr style =\"height: 19.8pt;\"><td width=\"156\" valign=\"top\" style=\"width: 117pt; padding: 0in 5.4pt; height: 19.8pt;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Order Ref Code</span></p></td>" +
                $"<td width =\"467\" valign=\"top\" style=\"width: 350.5pt; padding: 0in 5.4pt; height: 19.8pt;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.OrderRefCode}</span></p></td></tr>" +
                "<tr style =\"height: 18.9pt;\"><td width=\"156\" valign=\"top\" style=\"width: 117pt; padding: 0in 5.4pt; height: 18.9pt;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Change Order Start </span></p></td>" +
                $"<td width =\"467\" valign=\"top\" style=\"width: 350.5pt; padding: 0in 5.4pt; height: 18.9pt;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{orderData.StartDate}</span></p></td></tr>" +
                "<tr style =\"height: 0.25in;\"><td width=\"156\" valign=\"top\" style=\"width: 117pt; padding: 0in 5.4pt; height: 0.25in;\"><p class=\"MsoNormal\"><span style=\"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">Change Type(s):</span></p></td>";
            orderData.OrderChanges.ForEach(change =>
            {
                body +=    AdditionalTemplate(change);

            }); 
            body +=   "<td width =\"467\" valign=\"top\" style=\"width: 350.5pt; padding: 0in 5.4pt; height: 0.25in;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p></td></tr></tbody></table>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">If you have any questions or concerns, do not reply to this email as it is sent by our system. Instead, please contact us through the <a href=\"https://sightly.atlassian.net/servicedesk/customer/portal/7\">Sightly Service Desk</a>.</span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\"> </span></p>" +
                       "<p class=\"MsoNormal\"><span style=\"font-family: &quot;Open Sans&quot;;\">Cheers, The Sightly Team</span></p>";

            if (orderData.EmailAttachment != null)
            {
                var attachment = orderData.EmailAttachment;
                var emailData = new EmailData(to, from, subject, body, null, attachment);

                return emailData;
            }
            else
            {
                var emailData = new EmailData(to, from, subject, body);

                return emailData;
            }
        }


        public string AdditionalTemplate(OrderChangeInfo changes)
        {
            return $"<tr style=\"height: 0.25in; \"><td width=\"156\" valign=\"top\" style=\"width: 117pt; padding: 0in 5.4pt; height: 0.25in; \"><p class=\"MsoNormal\"><span style=\"font - size: 10pt; font - family: &quot; Open Sans&quot; ; \">{changes.ChangeType}</span></p></td> " +
                   $"<td width=\"467\" valign=\"top\" style=\"width: 350.5pt; padding: 0in 5.4pt; height: 0.25in;\"><p style=\"font-weight: bold\" class=\"MsoNormal\"><span style = \"font-size: 10pt; font-family: &quot;Open Sans&quot;;\">{changes.ChangeDetails} </span ></p></td></tr>";
        }
    }
}
