﻿using System.Net.Mail;

namespace Sightly.Email
{
    public interface IEmailClient
    {
        void SendEmail(MailMessage message);
    }
}
