﻿using System;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Sightly.Service.YoutubeUploader
{
    public class UploaderData
    {
        public Guid VideoAssetId { get; set; }
        public string VideoName { get; set; }
        public string CreatedBy { get; set; }
        public CloudBlockBlob BlobToUpload { get; set; }
    }
}