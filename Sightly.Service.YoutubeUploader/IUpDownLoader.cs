﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sightly.Service.YoutubeUploader
{
    public interface IUpDownLoader
    {
        string DownloadAzureBlobAndUploadToYoutube(Guid videoAssetId);
    }
}
