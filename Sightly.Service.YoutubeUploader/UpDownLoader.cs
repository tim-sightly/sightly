﻿using Sightly.TargetViewDb.DAL.Repositories.Interfaces;
using System;
using System.Data.Services.Client;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Services;
using Google.Apis.Upload;
using Google.Apis.YouTube.v3;
using Google.Apis.YouTube.v3.Data;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;

namespace Sightly.Service.YoutubeUploader
{
    public class UpDownLoader : IUpDownLoader
    {

        private ITvDbRepository _tvDbRepository;

        public static string UploadResponse { get;  set; }

        public UpDownLoader(ITvDbRepository tvDbRepository)
        {
            _tvDbRepository = tvDbRepository;
        }

        public string DownloadAzureBlobAndUploadToYoutube(Guid videoAssetId)
        {
            string result = String.Empty;
            var videoData = _tvDbRepository.GetVideoAssetDataByVideoAssetId(videoAssetId);
            UploaderData uploaderData = new UploaderData
            {
                VideoAssetId = videoData.VideoAssetId,
                VideoName = videoData.VideoName,
                CreatedBy = videoData.CreatedBy
            };

            CloudBlockBlob blobToGrab = GetAzureBlobData(videoAssetId);
            if(blobToGrab == null)
                throw new DataServiceQueryException($"The video asset :{videoAssetId} was unavailable in Azure Storage");
            uploaderData.BlobToUpload = blobToGrab;


           Run(uploaderData).Wait();



             if (UploadResponse != string.Empty)
            {
                _tvDbRepository.SaveVideoAssetIdToVideoAsset(videoAssetId, UploadResponse);
                return result;
            }
            else
            {
                throw new Exception($"Uploading of the Video Asset :{videoAssetId} was not successfully uploaded to Youtube.");
            }

        }

        private CloudBlockBlob GetAzureBlobData(Guid videoAssetId)
        {
            CloudStorageAccount storageAccount =
                CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();


            CloudBlobContainer container = blobClient.GetContainerReference("videoassets");

            CloudBlockBlob blobToGrab = null;

            foreach (IListBlobItem item in container.ListBlobs())
            {
                CloudBlockBlob blob = (CloudBlockBlob)item;

                if (blob.Name.Contains(videoAssetId.ToString().ToLower()))
                {
                    blobToGrab = blob;
                }
            }

            return blobToGrab;
        }
        private static async Task Run(UploaderData uploaderData)
        {
            UserCredential credential;
            using (var stream = new FileStream("Permissions.json", FileMode.Open, FileAccess.Read))
            {
                credential = await GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    // This OAuth 2.0 access scope allows an application to upload files to the
                    // authenticated user's YouTube channel, but doesn't allow other types of access.
                    new[] { YouTubeService.Scope.YoutubeUpload },
                    "Sightly Videos",
                    CancellationToken.None
                );
            }

            var youtubeService = new YouTubeService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = Assembly.GetExecutingAssembly().GetName().Name
            });

            var video = new Video
            {
                Snippet = new VideoSnippet
                {
                    Title = uploaderData.VideoName,
                    Description = uploaderData.BlobToUpload.Name,
                    CategoryId = "2"
                },
                Status = new VideoStatus { PrivacyStatus = "unlisted" }
            };

            
            using (var memoryStream = new MemoryStream())
            {
                uploaderData.BlobToUpload.DownloadToStream(memoryStream);

                var videosInsertRequest =
                    youtubeService.Videos.Insert(video, "snippet,status", memoryStream, "video/*");
                videosInsertRequest.ProgressChanged += videosInsertRequest_ProgressChanged;
                videosInsertRequest.ResponseReceived += videosInsertRequest_ResponseReceived;

                await videosInsertRequest.UploadAsync();
                
            }
            
        }

        static void videosInsertRequest_ProgressChanged(IUploadProgress progress)
        {
            switch (progress.Status)
            {
                case UploadStatus.Uploading:
                    Console.WriteLine("{0} bytes sent.", progress.BytesSent);
                    break;

                case UploadStatus.Failed:
                    Console.WriteLine("An error prevented the upload from completing.\n{0}", progress.Exception);
                    break;
            }
        }

        static void videosInsertRequest_ResponseReceived(Video video)
        {
            Console.WriteLine("Video id '{0}' was successfully uploaded.", video.Id);
            UploadResponse = video.Id;
        }
    }
}