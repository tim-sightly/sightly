﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Sightly.API.Common;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessServices.cs;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/chart")]
    public class ChartController : CommonApiController
    {
        private readonly ILoggingWrapper _loggingWrapper;
        //private readonly ICampaignService _campaignService;
        private readonly IAccountService _accountService;

        public ChartController(ILoggingWrapper loggingWrapper, IAccountService accountService)
        {
            _loggingWrapper = loggingWrapper;
            _accountService = accountService;
            //_campaignService = campaignService;
        }
        
        [HttpGet]
        [Route("list/accounts")]
        public IHttpActionResult ListAccounts()
        {
            try
            {
                List<Account> accounts = _accountService.ListAllAccounts();
                return Ok(new { accounts });
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }
        
        [HttpGet]
        [Route("list/advertisers")]
        public IHttpActionResult ListAdvertisers()
        {
            try
            {
                List<Advertiser> advertisers = _accountService.ListAllAdvertisers();
                return Ok(new { advertisers });
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

    }
}