﻿using Sightly.API.Common;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessServices.cs;
using Sightly.IOC;
using Sightly.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/campaign")]
    public class CampaignController : CommonApiController
    {
        private readonly ILoggingWrapper _loggingWrapper;
        private readonly ICampaignService _campaignService;

        public CampaignController(ILoggingWrapper loggingWrapper, ICampaignService campaignService)
        {
            _loggingWrapper = loggingWrapper;
            _campaignService = campaignService;
        }

        [HttpGet]
        [Route("list")]
        public IHttpActionResult List()
        {
            try
            {
                List<Campaign> campaigns = _campaignService.GetAllCampaigns();
                return Ok(new { campaigns });
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("agencylist")]
        public IHttpActionResult AgencyList()
        {
            try
            {
                List<Campaign> campaigns = _campaignService.GetAllMediaAgencyCampaigns();
                return Ok(new { campaigns });
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getAllCampaignsForAdWordsCustomerId")]
        public IHttpActionResult GetAllCampaignsForAdWordsCustomerId(long adWordsCustomerId)
        {
            try
            {
                List<Campaign> campaigns = _campaignService.GetAllCampaignsForAdWordsCustomerId(adWordsCustomerId);
                return Ok(new { campaigns });
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }
    }
}
