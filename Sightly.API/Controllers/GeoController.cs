﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Http;
using Dapper;
using Google.Api.Ads.AdWords.v201806;
using Sightly.API.Common;

namespace Sightly.API.Controllers
{
    [RoutePrefix("api/geo")]
    public class GeoController : CommonApiController
    {
        [HttpGet]
        [Route("lookahead")]
        public IHttpActionResult LookAhead(string searchInput)
        {
            try
            {
                using (IDbConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TargetView"].ToString()))
                {
                    string sql = @"SELECT TOP 10 GeographyId, GeographyName, Population
                                FROM [zLookups].[Geography]
                                WHERE GeographyName LIKE CONCAT('%',@SearchInput,'%')
                                OR CanonicalName LIKE CONCAT('%',@SearchInput,'%')
                                ORDER BY GeographyName;";

                    List<Geo> geos = conn.Query<Geo>(sql, new {SearchInput = searchInput}).ToList();
                    return Ok(geos);

                }
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("bulksearch")]
        public IHttpActionResult BulkSearch(string searchInput)
        {
            try
            {
                using (IDbConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TargetView"].ToString()))
                {
                    //TODO: build stored proc query string here
                    string sql = @"EXECUTE tvdb.SearchGeographyByCommaStringList @QueryString";

                    List<Geo> geos = conn.Query<Geo>(sql, new { QueryString = searchInput }).ToList();
                    return Ok(geos);

                }
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        private class Geo
        {
            public Guid GeographyId { get; set; }
            public string GeographyName { get; set; }
            public long Population { get; set; }
        }


    }
}
