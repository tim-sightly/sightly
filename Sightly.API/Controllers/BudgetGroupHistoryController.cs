﻿using System;
using System.Net;
using System.Web.Http;
using Sightly.API.Common;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using Sightly.BusinessServices.cs;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/budgetgrouphistory")]
    public class BudgetGroupHistoryController : CommonApiController
    {
        private readonly IBudgetGroupHistoryService _budgetGroupHistoryService;
        private readonly ILoggingWrapper _loggingWrapper;
        public BudgetGroupHistoryController(IBudgetGroupHistoryService budgetGroupHistoryService, ILoggingWrapper loggingWrapper)
        {
            _budgetGroupHistoryService = budgetGroupHistoryService;
            _loggingWrapper = loggingWrapper;
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult GetHistoryById(Guid? id)
        {
            BudgetGroupHistory budgetGroupHistory = null;

            if (!id.HasValue)
                return BadRequest();

            try
            {
                budgetGroupHistory = _budgetGroupHistoryService.GetById(id.Value);
                if (budgetGroupHistory == null)
                    return NotFound();
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
            return Ok(budgetGroupHistory);
        }
    }
}
