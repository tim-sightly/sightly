﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Sightly.API.Common;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessServices.cs;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/dv")]
    public class DoubleVerifyController : CommonApiController
    {
        private readonly IDoubleVerifyService _doubleVerifyService;
        private readonly ILoggingWrapper _loggingWrapper;

        public DoubleVerifyController(IDoubleVerifyService doubleVerifyService, ILoggingWrapper loggingWrapper)
        {
            _doubleVerifyService = doubleVerifyService;
            _loggingWrapper = loggingWrapper;
        }

        [HttpGet]
        [Route("getDailyDvStatsForAccount/{accountId}")]
        public IHttpActionResult GetDailyDvStatsForAccount(Guid accountId)
        {
            try
            {
                List<DoubleVerifyStats> dvData = new List<DoubleVerifyStats>();
                var dvDailyStatsData = _doubleVerifyService.GetDoubleVerifyKpiStatsDatabyAccount(accountId);
                dvDailyStatsData.ForEach(day =>
                {
                    dvData.Add(new DoubleVerifyStats
                    {
                        Id = accountId,
                        Type = "account",
                        StatDate = day.StatDate.Value,
                        BrandSafetyOnTargetPercent_1x1 = day.BrandSafetyOnTargetPercent_1x1,
                        FraudSivtRate = day.FraudSivtRate,
                        FraudSivtRate_1x1 = day.FraudSivtRate_1x1,
                        InGeoRate_1x1 = day.InGeoRate_1x1,
                        VideoViewableRate = day.VideoViewableRate
                    });
                });

                return Ok(dvData);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getDailyDvStatsForAdvertiser/{advertiserId}")]
        public IHttpActionResult GetDailyDvStatsForAdvertiser(Guid advertiserId)
        {
            try
            {
                List<DoubleVerifyStats> dvData = new List<DoubleVerifyStats>();
                var dvDailyStatsData = _doubleVerifyService.GetDoubleVerifyKpiStatsDatabyAdvertiser(advertiserId);
                dvDailyStatsData.ForEach(day =>
                {
                    dvData.Add(new DoubleVerifyStats
                    {
                        Id = advertiserId,
                        Type = "advertiser",
                        StatDate = day.StatDate.Value,
                        BrandSafetyOnTargetPercent_1x1 = day.BrandSafetyOnTargetPercent_1x1,
                        FraudSivtRate = day.FraudSivtRate,
                        FraudSivtRate_1x1 = day.FraudSivtRate_1x1,
                        InGeoRate_1x1 = day.InGeoRate_1x1,
                        VideoViewableRate = day.VideoViewableRate
                    });
                });

                return Ok(dvData);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getDailyDvStatsForCampaign/{campaignId}")]
        public IHttpActionResult GetDailyDvStatsForCampaign(Guid campaignId)
        {
            try
            {
                List<DoubleVerifyStats> dvData = new List<DoubleVerifyStats>();
                var dvDailyStatsData = _doubleVerifyService.GetDoubleVerifyKpiStatsDatabyCampaign(campaignId);
                dvDailyStatsData.ForEach(day =>
                {
                    dvData.Add(new DoubleVerifyStats
                    {
                        Id = campaignId,
                        Type = "campaignId",
                        StatDate = day.StatDate.Value,
                        BrandSafetyOnTargetPercent_1x1 = day.BrandSafetyOnTargetPercent_1x1,
                        FraudSivtRate = day.FraudSivtRate,
                        FraudSivtRate_1x1 = day.FraudSivtRate_1x1,
                        InGeoRate_1x1 = day.InGeoRate_1x1,
                        VideoViewableRate = day.VideoViewableRate,
                        AwCustomerId = day.AwCustomerId
                        
                    });
                });

                return Ok(dvData);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }
    }
}
