﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dapper;
//using System.Web.Http;
using Sightly.API.Common;
using Sightly.BusinessServices.cs;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/tvreports")]
    public class TvReportsController : CommonApiController
    {
        private readonly ILoggingWrapper _loggingWrapper;
        private readonly string _connString;

        public TvReportsController(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
            _connString = ConfigurationManager.ConnectionStrings["TargetView"].ToString();
        }

        [HttpGet]
        [Route("searchCampaignInfoForReporting")]
        public IHttpActionResult SearchCampaignInfoForReporting(string searchId)
        {
            try
            {
                Guid gSearchId = Guid.Parse(searchId);
                using (IDbConnection conn = new SqlConnection(_connString))
                {
                    string sql = @"tvdb.SearchReportingHeirarchyByNonSpecificValue";

                    var p = new DynamicParameters();
                    p.Add("@SearchId", gSearchId);

                    List<CampaignInfo> campInfo = conn.Query<CampaignInfo>(sql, p, commandType: CommandType.StoredProcedure).ToList();

                    return Ok(campInfo);
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [Route("getOrdersForAccount")]
        public IHttpActionResult GetOrdersForAccount(string accountId)
        {
            try
            {
                var userId = Guid.Parse(ActionContext.ActionArguments["UserId"].ToString());

                Guid accountIdGuid = Guid.Parse(accountId);
                using (IDbConnection conn = new SqlConnection(_connString))
                {
                    string sql = @"tvdb.GetOrdersUnderAccountWithSub_ByAccountIdAndUser";

                    var p = new DynamicParameters();
                    p.Add("@AccountId", accountIdGuid);
                    p.Add("@UserId", userId);

                    List<Order> orders = conn.Query<Order>(sql, p, commandType: CommandType.StoredProcedure).ToList();

                    return Ok(orders);
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [Route("getOrdersForAdvertiser")]
        public IHttpActionResult GetOrdersForAdvertiser(string advertiserId)
        {
            try
            {
                var userId = Guid.Parse(ActionContext.ActionArguments["UserId"].ToString());

                Guid advertiserIdGuid = Guid.Parse(advertiserId);
                using (IDbConnection conn = new SqlConnection(_connString))
                {
                    string sql = @"SELECT DISTINCT
                                 aaco.OrderId
                                ,aaco.AccountName
                                ,aaco.AccountId
                                ,aaco.AdvertiserId
                                ,aaco.AdvertiserName
                                ,aaco.CampaignId
                                ,aaco.CampaignName
                                ,aaco.OrderName
                                ,aaco.OrderRefCode
                                ,aaco.CampaignBudget
                                ,aaco.StartDate
                                ,aaco.EndDate
                                ,aaco.OrderStatus
                                FROM [tvdb].[AccAdvCamOrderDataWithBudgetAndDates2] aaco
                                INNER JOIN Users.UserXrefAdvertiser uxa
									ON aaco.AdvertiserId = uxa.AdvertiserId
								WHERE aaco.AdvertiserId = @AdvertiserId
								AND uxa.UserId = @UserId";

                    List<Order> orders = conn.Query<Order>(sql, new {AdvertiserId = advertiserIdGuid, UserId = userId }).ToList();
                    return Ok(orders);
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [Route("getOrdersForCampaign")]
        public IHttpActionResult GetOrdersForCampaign(string campaignId)
        {
            try
            {
                var userId = Guid.Parse(ActionContext.ActionArguments["UserId"].ToString());

                Guid campaignGuid = Guid.Parse(campaignId);
                using (IDbConnection conn = new SqlConnection(_connString))
                {
                    string sql = @"SELECT DISTINCT
                                 aaco.OrderId
                                ,aaco.AccountName
                                ,aaco.AccountId
                                ,aaco.AdvertiserId
                                ,aaco.AdvertiserName
                                ,aaco.CampaignId
                                ,aaco.CampaignName
                                ,aaco.OrderName
                                ,aaco.OrderRefCode
                                ,aaco.CampaignBudget
                                ,aaco.StartDate
                                ,aaco.EndDate
                                ,aaco.OrderStatus
                                FROM [tvdb].[AccAdvCamOrderDataWithBudgetAndDates2] aaco
                                INNER JOIN Users.UserXrefAdvertiser uxa
									ON aaco.AdvertiserId = uxa.AdvertiserId
								WHERE aaco.CampaignId = @CampaignId
								AND uxa.UserId = @UserId";

                    List<Order> orders = conn.Query<Order>(sql, new { CampaignId = campaignGuid, UserId = userId }).ToList();
                    return Ok(orders);
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        private class Order
        {
            public Guid AccountId { get; set; }
            public string AccountName { get; set; }
            public Guid AdvertiserId { get; set; }
            public string AdvertiserName { get; set; }
            public Guid CampaignId { get; set; }
            public string CampaignName { get; set; }
            public Guid OrderId { get; set; }
            public string OrderName { get; set; }
            public string OrderRefCode { get; set; }
            public decimal CampaignBudget { get; set; }
            public DateTime StartDate { get; set; }
            public DateTime EndDate { get; set; }
            public string OrderStatus { get; set; }
            
        }

        public class CampaignInfo
        {
            public Guid CampaignId { get; set; }
            public string CampaignName { get; set; }
            public long AdWordsCustomerId { get; set; }
            public Guid AdvertiserId { get; set; }
            public string AdvertiserName { get; set; }
            public Guid AccountId { get; set; }
            public string AccountName { get; set; }
        }
    }
}
