﻿using OfficeOpenXml;
using Sightly.API.Common;
using Sightly.Business.MediaAgency;
using Sightly.Business.StatsGather;
using Sightly.BusinessServices.cs;
using Sightly.Logging;
using Sightly.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web;
using System.Web.Http;
using Newtonsoft.Json;
using Sightly.Email;
using Sightly.Email.Templates;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/agency")]
    public class MediaAgencyController : CommonApiController
    {
        private readonly ILoggingWrapper _loggingWrapper;
        private readonly IStateTransitionService _stateTransitionService;

        private readonly IDomainManager _domainManager;
        private readonly IEmailClient _emailClient;
        private readonly IEmailService _emailService;
        private readonly IGatherStats _gatherStats;


        public MediaAgencyController(ILoggingWrapper loggingWrapper, IStateTransitionService stateTransitionService, IDomainManager domainManager, 
            IEmailClient emailClient, IEmailService emailService, IGatherStats gatherStats)
        {
            _loggingWrapper = loggingWrapper;
            _stateTransitionService = stateTransitionService;
            _emailClient = emailClient;
            _emailService = emailService;
            _domainManager = domainManager;
            _gatherStats = gatherStats;
        }

        [HttpPost]
        [Route("verifyAdwordsAds")]
        public IHttpActionResult VerifyAdwordsAdsByCustomerId(VerifyAdwordsAdsVM verifyAdwordsAdsVM)
        {

            long customerId;
            if (!Int64.TryParse(verifyAdwordsAdsVM.CID, out customerId)) {

                return BadRequest("Bad CID given.");
            };


            string email = verifyAdwordsAdsVM.UserEmail;

            //Check to see if any work is currently being done on this customerId
            if (_domainManager.IsCustomerLocked(customerId))
            {
                return BadRequest("This CustomerId is currently locked for editing. Please try again later.");
            }
            try
            {

                //Lock the CustomerId
               _domainManager.LockCustomer(customerId, email, "Ad Swap");

                _domainManager.UpdateTvMediaAgencyAds(customerId);

                //Get the TargetView CampaignName from the CustomerId

                var customerName = _domainManager.GetTvCustomerNameByCustomerId(customerId);

                var adswapReadyEmail = new AdSwapReady();
                var emailData =
                    adswapReadyEmail.MakeAdSwapReady(new AdSwapData {CustomerId = customerId, Email = email, CustomerName =  customerName});
                _emailClient.SendEmail(emailData.SendToMailMessage());

            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
            finally
            {
                _domainManager.UnlockCustomer(customerId, email);
            }

            return Ok();
        }

        [HttpPost]
        [Route("upload")]
        public IHttpActionResult Upload()
        {
            List<MediaAgencyImport> mediaAgencyImports = new List<MediaAgencyImport>();

            if (HttpContext.Current.Request.Files.Count == 0)
                return BadRequest("No Files Posted.");

            HttpPostedFile postedFile = HttpContext.Current.Request.Files[0];

            if(postedFile.ContentType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                throw new Exception("Wrong file type. Must be a .xlsx!");

            MemoryStream ms = new MemoryStream();
            postedFile.InputStream.CopyTo(ms);

            ExcelPackage excel = new ExcelPackage(ms);

            try
            {
                mediaAgencyImports = GetMediaAgencyImportsFromExcel(excel);
            }
            catch (Exception)
            {




                return BadRequest("The import file used to upload was not in the correct format.");
            }


            string email = mediaAgencyImports.First().CampaignMangerEmail;
            long customerId =mediaAgencyImports.First().AwCustomerId;
            string customerName = mediaAgencyImports.First().CampaignName;


            //Check to see if any work is currently being done on this customerId
            if (_domainManager.IsCustomerLocked(customerId))
            {
                return BadRequest("This CustomerId is currently locked for editing. Please try again later.");
            }

            try
            {
                //Lock the CustomerId
                _domainManager.LockCustomer(customerId, email, "Import Upload");


                //If BudgetGroupTimedBudget exists its just and update for TimedBudget
                var updateMediaAgencyImports = mediaAgencyImports.Where(x => x.BudgetGroupTimedBudgetId != null)
                    .ToList();
                if (updateMediaAgencyImports != null && updateMediaAgencyImports.Count > 0)
                {
                    var maDateBudgetUpdates = new List<MediaAgencyDateBudgetUpdate>();
                    updateMediaAgencyImports.ForEach(uma =>
                    {
                        maDateBudgetUpdates.Add(new MediaAgencyDateBudgetUpdate
                        {
                            AwCampaignId = uma.AwCampaignId,
                            AwCampaignName = uma.AwCampaignName,
                            AwCustomerId = uma.AwCustomerId,
                            BudgetGroupTimedBudgetId = uma.BudgetGroupTimedBudgetId.Value,
                            CampaignBudget = uma.CampaignBudget,
                            CampaignName = uma.CampaignName,
                            EndDate = uma.EndDate,
                            Margin = uma.Margin,
                            PlacementId = uma.PlacementId,
                            PlacementName = uma.PlacementName,
                            StartDate = uma.StartDate,
                            TotalBudget = uma.TotalBudget
                        });
                    });
                    _domainManager.UpdateBudgetGroupTimedBudgetInfo(maDateBudgetUpdates);
                }


                //if no budgetGroupTimedBudgetId - Insert new as TimedBudget Or new Campaign
                var newMediaAgencyImports = mediaAgencyImports.Where(x => x.BudgetGroupTimedBudgetId == null).ToList();

                if (newMediaAgencyImports != null && newMediaAgencyImports.Count > 0)
                {
                    //Check for rows that are of existing Campaigns and insert into Proposed BudgetGroupTimedBudget
                    List<MediaAgencyImport> newTimedBudgets = new List<MediaAgencyImport>();
                    newMediaAgencyImports.ForEach(camp =>
                    {
                        //Does the CID exist - then insert the budgetGroupTimedBudget and remove the MediaAgencyImport from this list
                        var budgetGroupTimedBudget = _domainManager.DoesAdwordsCampaignExist(camp.AwCampaignId);
                        if (budgetGroupTimedBudget != null && budgetGroupTimedBudget.BudgetGroupXrefPlacementId != null && budgetGroupTimedBudget.OrderId != null)
                        {
                            _domainManager.CreateBudgetGroupTimedBudget(camp, budgetGroupTimedBudget.BudgetGroupId, budgetGroupTimedBudget.BudgetGroupXrefPlacementId.Value, budgetGroupTimedBudget.OrderId.Value);
                            newTimedBudgets.Add(camp);
                        }
                    });

                    List<MediaAgencyImport> newCampaigns = newMediaAgencyImports.Except(newTimedBudgets).ToList();
                    //Run Create on New Campaigns
                    if (newCampaigns.Count > 0)
                    {
                        _domainManager.CreateTvMediaAgencyDomain(newCampaigns, false);
                    }
                }
                 
                //In able to ensure that the upload file can have new email name for campaignManager

                EnsureProperCampaignMangerAssignedToCustomer(customerId, mediaAgencyImports.First().CampaignMangerEmail);


                //Ensure the Proposed is moved to Live based on the CustomerId
                //get All BudgetGroups from the CustomerId
                _domainManager.UpdateBudgetGroupTimedBudgets(customerId);



                var bgReadyEmail = new BudgetDateNewCampaignReady();
                var emailData =
                    bgReadyEmail.MakeBudgetDateNewCampaignReady(new BudgetDateNewCampaignData { CustomerId = customerId, Email = email, CustomerName = customerName});
                _emailClient.SendEmail(emailData.SendToMailMessage());

            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
            finally
            {
                //Release the CustomerId
                _domainManager.UnlockCustomer(customerId, email);
            }

            return Ok(mediaAgencyImports);

        }

        private List<MediaAgencyImport> GetMediaAgencyImportsFromExcel(ExcelPackage excel)
        {

            var mediaAgencyImports = new List<MediaAgencyImport>();

            ExcelWorksheet worksheet = excel.Workbook.Worksheets[1];

            var start = worksheet.Dimension.Start;
            var end = worksheet.Dimension.End;
            var isOrderImport = worksheet.Cells[start.Row, 13].Value != null && worksheet.Cells[start.Row, 13].Value.ToString() == "CampaignManagerEmail";
            try
            {
                for (int row = start.Row + 1; row <= end.Row; row++)
                {
                    if (worksheet.Cells[row, 1].Value == null)
                    {
                        break;
                    }
                    var ma = new MediaAgencyImport();
                    if (isOrderImport)
                    {
                        ma.CampaignName = worksheet.Cells[row, 1].Value.ToString();
                        ma.OrderName = worksheet.Cells[row, 2].Value == null ? worksheet.Cells[row, 1].Value.ToString() : worksheet.Cells[row, 2].Value.ToString();
                        ma.AwCampaignId = Convert.ToInt64(worksheet.Cells[row, 4].Value.ToString());
                        ma.AwCampaignName = worksheet.Cells[row, 5].Value.ToString();
                        ma.AwCustomerId = Convert.ToInt64(worksheet.Cells[row, 3].Value.ToString().Replace("-", ""));
                        ma.CampaignBudget = Convert.ToDecimal(worksheet.Cells[row, 8].Value.ToString().Replace("$", ""));
                        ma.StartDate = DateTime.Parse(worksheet.Cells[row, 6].Value.ToString());
                        ma.EndDate = DateTime.Parse(worksheet.Cells[row, 7].Value.ToString());
                        ma.Margin = Convert.ToDecimal(worksheet.Cells[row, 9].Value.ToString().Replace("%", ""));
                        ma.PlacementId = worksheet.Cells[row, 10].Value.ToString();
                        ma.PlacementName = worksheet.Cells[row, 11].Value.ToString();
                        ma.TotalBudget = 0.0M;
                        ma.BudgetGroupTimedBudgetId = worksheet.Cells[row, 12].Value == null ? (Guid?)null : Guid.Parse(worksheet.Cells[row, 12].Value.ToString());
                        ma.CampaignMangerEmail = worksheet.Cells[row, 13].Value.ToString();
                    }
                    else
                    {
                        ma.CampaignName = worksheet.Cells[row, 1].Value.ToString();
                        ma.OrderName = worksheet.Cells[row, 1].Value.ToString();
                        ma.AwCampaignId = Convert.ToInt64(worksheet.Cells[row, 3].Value.ToString());
                        ma.AwCampaignName = worksheet.Cells[row, 4].Value.ToString();
                        ma.AwCustomerId = Convert.ToInt64(worksheet.Cells[row, 2].Value.ToString().Replace("-", ""));
                        ma.CampaignBudget = Convert.ToDecimal(worksheet.Cells[row, 7].Value.ToString().Replace("$", ""));
                        ma.StartDate = DateTime.Parse(worksheet.Cells[row, 5].Value.ToString());
                        ma.EndDate = DateTime.Parse(worksheet.Cells[row, 6].Value.ToString());
                        ma.Margin = Convert.ToDecimal(worksheet.Cells[row, 8].Value.ToString().Replace("%", ""));
                        ma.PlacementId = worksheet.Cells[row, 9].Value.ToString();
                        ma.PlacementName = worksheet.Cells[row, 10].Value.ToString();
                        ma.TotalBudget = 0.0M;
                        ma.BudgetGroupTimedBudgetId = worksheet.Cells[row, 11].Value == null ? (Guid?)null : Guid.Parse(worksheet.Cells[row, 11].Value.ToString());
                        ma.CampaignMangerEmail = worksheet.Cells[row, 12].Value.ToString();
                    }
                    

                    mediaAgencyImports.Add(ma);

                    
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                throw;
            }

            //figure out what the total budget is
            mediaAgencyImports.GroupBy(x => x.AwCustomerId).ToList().ForEach(ma =>
            {
                decimal totalBudget = mediaAgencyImports.Where(x => x.AwCustomerId == ma.Key)
                    .Sum(y => y.CampaignBudget);
                mediaAgencyImports.Where(x => x.AwCustomerId == ma.Key).ToList().ForEach(mai =>
                {
                    mai.TotalBudget = totalBudget;
                });
            });

            return mediaAgencyImports;
        }

        private void EnsureProperCampaignMangerAssignedToCustomer(long customerId, string campaignMangerEmail)
        {
            _domainManager.EnsureProperCampaignMangerAssignedToCustomer(customerId, campaignMangerEmail);
        }

        public static void SendOrderForLiveSubmission(OrdersProposalRequest request)
        {
            try
            {
                var requestUrl = "api/transition/OrderSetToLive";
                SendPostRequest(requestUrl, request);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

        }

        protected static void SendPostRequest(string actionUrl, BaseRequest request)
        {
            using (var client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromMinutes(5);
                var baseApiUrl = ConfigurationManager.AppSettings["TargetviewApi"];
                var serviceId = ConfigurationManager.AppSettings["ServiceId"];

                request.ServiceId = Guid.Parse(serviceId);

                client.BaseAddress = new Uri(baseApiUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                var jsonRequest = JsonConvert.SerializeObject(request);
                var content = new StringContent(jsonRequest, Encoding.UTF8, "application/json");
                var response = client.PostAsync(actionUrl, content).Result;

                if (response.StatusCode == HttpStatusCode.BadRequest || response.StatusCode == HttpStatusCode.InternalServerError)
                {
                    var httpErrorString = response.Content.ReadAsStringAsync().Result;
                    var httpErrorTemplateObject = new { message = "" };
                    var httpErrorObject = JsonConvert.DeserializeAnonymousType(httpErrorString, httpErrorTemplateObject);
                    throw new HttpRequestException($"Failed POST Request @ {baseApiUrl + actionUrl}. {httpErrorObject.message}");
                }
            }

        }

        private void GatherStats(List<MediaAgencyImport> mediaAgencyImports)
        {
            // Console.WriteLine($"{DateTime.Now} - Starting Data gather");
            var today = DateTime.Now;
            mediaAgencyImports.GroupBy(x => x.AwCustomerId).ToList().ForEach(ma =>
            {

                var startDate = mediaAgencyImports.Where(x => x.AwCustomerId == ma.Key).Min(m => m.StartDate);
                var endDate = mediaAgencyImports.Where(x => x.AwCustomerId == ma.Key).Min(m => m.EndDate);
                if (endDate.Date >= today.Date) endDate = today;

                for (var countDate = startDate; countDate.Date <= endDate.Date; countDate = countDate.AddDays(1))
                {
                    //Console.WriteLine($"{DateTime.Now} - Gather Stats for : {ma.Key} on {countDate}");
                    _gatherStats.Execute(ma.Key, countDate);
                }
            });
        }
    }

    public class BaseRequest
    {
        public Guid ServiceId { get; set; }
    }
    public class OrdersProposalRequest : BaseRequest
    {
        public Guid? OrderProposalId { get; set; }
        public string Notes { get; set; }
        public Guid? UserId { get; set; }
        public Guid? BudgetGroupXrefPlacementId { get; set; }
    }

    public class VerifyAdwordsAdsVM
    {
        public string UserEmail { get; set; }
        public string CID { get; set; }
    }


}
