﻿using System;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace Sightly.API.Controllers
{
    public class CommonApiController : ApiController
    {
        public virtual IHttpActionResult GetResponseForException(Exception ex)
        {
            string exceptionTypeFull = ex.GetType().ToString();
            string exType = exceptionTypeFull.Split('.').Last();

            switch (exType)
            {
                case "BudgetException":
                    return BadRequest(ex.Message);
                case "DataConditionException":
                    return BadRequest(ex.Message);
                case "EntityNotFoundException":
                    return BadRequest(ex.Message);
                default:
                    return Content(HttpStatusCode.InternalServerError, ex.Message);
            }
        }
    }
}
