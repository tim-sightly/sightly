﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Microsoft.Azure;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Sightly.API.Common;
using Sightly.Business.OrderManager;
using Sightly.Logging;
using Sightly.Models.tv;
using Sightly.Youtube.Uploader;
using Sightly.Youtube.Utility;
using System.Diagnostics;


namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("upload/image")]
    public class ImageUploadController : CommonApiController
    {
        private ILoggingWrapper _loggingWrapper;
        private readonly string _connString;
        private const string BaseYoutubeUrl = "https://www.youtube.com/watch?v=";
        public ImageUploadController(ILoggingWrapper loggingWrapper)
        {
            _loggingWrapper = loggingWrapper;
            _connString = ConfigurationManager.ConnectionStrings["TargetView"].ToString();
        }

        [HttpPost]
        [Route("companionbanner")]
        public async Task<HttpResponseMessage> CompanionBanner()
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    var postedFile = httpRequest.Files[file];
                    var allowedFileExtensions = new List<string> { ".jpg", ".jpeg", "jpe", ".gif", ".png", ".bmp", ".svg" };
                    int MaxContentLength = 1024 * 150; //Size = 1.5

                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!allowedFileExtensions.Contains(extension)) // ENFORCE file type
                        {
                            var message = "Please Upload image of type .jpg, .jpeg, jpe, .gif, .png, .bmp, .svg";
                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else if (postedFile.ContentLength > MaxContentLength) //ENFORCE max size
                        {
                            var message = "Please Upload a file upto 1.5 mb.";
                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {
                            //We have passed all requirements now save the file...
                            var contentType = postedFile.ContentType;
                            var streamContents = postedFile.InputStream;
                            var blobName = postedFile.FileName;
                            
                            CloudBlobContainer container = GetBlobContainer();
                            CloudBlockBlob blob = container.GetBlockBlobReference(blobName);
                            blob.Properties.ContentType = contentType;
                            blob.UploadFromStream(streamContents);
                            var uri = string.Format(blob.StorageUri.PrimaryUri.ToString());
                            return Request.CreateResponse(HttpStatusCode.Created, uri);
                        }
                    }
                }
                var res = "No image uploaded. Please try again.";
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            catch (Exception ex)
            {
                var res = "Something went wrong. Please try again.";
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }

        }

        [HttpPost]
        [Route("youtubevideo")]
        public async Task<HttpResponseMessage> YouTubeVideo()
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            try
            {
                var httpRequest = HttpContext.Current.Request;

                foreach (string file in httpRequest.Files)
                {
                    HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created);
                    var postedFile = httpRequest.Files[file];
                    var allowedFileExtensions = new List<string> { ".flv", ".mp4", ".3gp", ".mov", ".avi", ".wmv", ".webm" };

                    if (postedFile != null && postedFile.ContentLength > 0)
                    {
                        var ext = postedFile.FileName.Substring(postedFile.FileName.LastIndexOf('.'));
                        var extension = ext.ToLower();
                        if (!allowedFileExtensions.Contains(extension)) // ENFORCE file type
                        {
                            var message = "Please upload video of type .flv, .mp4, .3gp, .mov, .avi, .wmv, .webm";
                            dict.Add("error", message);
                            return Request.CreateResponse(HttpStatusCode.BadRequest, dict);
                        }
                        else
                        {
                            //TODO: Send uploaded file to YouTube. 
                            var _ytService = new YoutubeServiceGetter().GetYouTubeServiceAsync().Result;

                            var ytUploader = new YoutubeVideoUploader();
                            var ytIdValidator = new YoutubeIdValidator();

                            var uploadIdResult = ytUploader.UploadVideo(_ytService, postedFile).Result;
                            Debug.WriteLine("Starting Buffer...");                         

                            System.Threading.Thread.Sleep(15000);

                            var duplicateResult =
                                ytIdValidator.FindDuplicateIdsFromListOfIds(new List<string>(){ uploadIdResult }, _ytService);
                            var finalId = uploadIdResult;
                            if (duplicateResult.Count > 0)
                            {
                                finalId = duplicateResult[uploadIdResult];
                            }
                            return Request.CreateResponse(HttpStatusCode.Created, BaseYoutubeUrl+finalId);
                        }
                    }
                }
                var res = "No image uploaded. Please try again.";
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Message: {ex.Message}, InnerException: {ex.InnerException}");
                var res = "Something went wrong. Please try again.";
                dict.Add("error", res);
                return Request.CreateResponse(HttpStatusCode.NotFound, dict);
            }

        }

        private CloudBlobContainer GetBlobContainer()
        {
            CloudStorageAccount storageAccount =
                CloudStorageAccount.Parse(CloudConfigurationManager.GetSetting("StorageConnectionString"));

            CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();
        
            return blobClient.GetContainerReference("bannerassets");
        }

    }
}
