﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Sightly.Logging;
using Sightly.Service.YoutubeUploader;

namespace Sightly.API.Controllers
{
    [RoutePrefix("api/youtube")]
    public class YoutubeController : CommonApiController
    {
        private readonly ILoggingWrapper _loggingWrapper;
        private readonly IUpDownLoader _upDownLoader;

        public YoutubeController(ILoggingWrapper loggingWrapper, IUpDownLoader upDownLoader)
        {
            _loggingWrapper = loggingWrapper;
            _upDownLoader = upDownLoader;
        }

        [HttpPost]
        [Route("uploadVideo/{videoAssetId}")]
        public IHttpActionResult UploadVideo(Guid videoAssetId)
        {
            var youTubeId = string.Empty;
            try
            {
                youTubeId = _upDownLoader.DownloadAzureBlobAndUploadToYoutube(videoAssetId);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
            return Ok(youTubeId);
        }
    }
}
