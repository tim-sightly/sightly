﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web.Http;
using Sightly.API.Common;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessServices.cs;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/alert")]
    public class AlertCentralController : CommonApiController
    {
        private readonly IAlertCentralService _alertCentralService;
        private readonly ILoggingWrapper _loggingWrapper;

        public AlertCentralController(IAlertCentralService alertCentralService, ILoggingWrapper loggingWrapper)
        {
            _alertCentralService = alertCentralService;
            _loggingWrapper = loggingWrapper;
        }

        [HttpGet]
        [Route("getAlerts")]
        public IHttpActionResult GetAlertCentral()
        {
            List<AlertKpiDataObject> alerts = new List<AlertKpiDataObject>();
            var listOfCids = _alertCentralService.GetAwCustomersThatAreCurrentlyActive(DateTime.Today);

            listOfCids.ForEach(customer =>
            {

                var kpiData = GetKpiAlertsByCustomerAndDates(customer, null);

                alerts.Add(kpiData);
            });
            return Ok(alerts);
        }

        [HttpGet]
        [Route("getAlertsWithEndDate")]
        public IHttpActionResult GetAlertsWithEndDate(DateTime endDate)
        {
            List<AlertKpiDataObject> alerts = new List<AlertKpiDataObject>();
            var listOfCids = _alertCentralService.GetAwCustomersThatAreCurrentlyActive(DateTime.Today);

            listOfCids.ForEach(customer =>
            {

                var kpiData = GetKpiAlertsByCustomerAndDates(customer, endDate);

                alerts.Add(kpiData);
            });
            return Ok(alerts);
        }

        //ToDo- put this within the other function so it's not being duplicated

        [HttpGet]
        [Route("getAlertsByFlightWithEndDate")]
        public IHttpActionResult GetAlertsByFlightWithEndDate(DateTime endDate)
        {
            List<AlertKpiDataObject> alerts = new List<AlertKpiDataObject>();
            var listOfCids = _alertCentralService.GetAwCustomersThatAreCurrentlyActiveByFlight(DateTime.Today);

            listOfCids.ForEach(customer =>
            {

                var kpiData = GetKpiAlertsByCustomerAndDates(customer, endDate);

                alerts.Add(kpiData);
            });
            return Ok(alerts);
        }

        [HttpGet]
        [Route("getAlertThresholds")]
        public IHttpActionResult GetAlertThresholdsByCustomerId(long customerId)
        {
            List<KpiDefinitionData> KPIThreshold = new List<KpiDefinitionData>();
            KPIThreshold = _alertCentralService.GetKpiDefinitionData(customerId);

            return Ok(KPIThreshold);
        }
        

        private AlertKpiDataObject GetKpiAlertsByCustomerAndDates(AdwordCustomer customer, DateTime? endDate)
        {
            var kpiData = GetAlertKpiDataFromCustomerId(
                    customer.CustomerId, 
                    customer.CustomerName,
                    customer.StartDate,
                    endDate ?? customer.EndDate, 
                    customer.TotalDays, 
                    customer.DaysSoFar,
                    customer.PercentComplete, 
                    customer.DaysLeft);


            kpiData.KpiDefinitions.ForEach(kpi =>
            {
                switch (kpi.KpiMetricAbbreviation)
                {
                    case "audOTR":
                        if (kpiData.NielsenKpiStats != null)
                        {
                            kpi.ActualValue =
                                kpiData.NielsenKpiStats.DarTotalDigital.ToString(CultureInfo.InvariantCulture);
                            kpi.Status = GetStatusFromKpi(kpi);
                        }
                        else
                            kpi.Status = -2;
                        break;
                    case "CPM":
                        if (kpiData.AdwordsKpiStats != null)
                        {
                            kpi.ActualValue =
                                kpiData.AdwordsKpiStats.CPM.ToString(CultureInfo.InvariantCulture);
                            kpi.Status = GetStatusFromKpi(kpi);
                        }
                        else
                            kpi.Status = -2;
                        break;
                    case "CPV":
                        if (kpiData.AdwordsKpiStats != null)
                        {
                            kpi.ActualValue =
                                kpiData.AdwordsKpiStats.CPV.ToString(CultureInfo.InvariantCulture);
                            kpi.Status = GetStatusFromKpi(kpi);
                        }
                        else
                            kpi.Status = -2;
                        break;
                    case "CTR":
                        if (kpiData.AdwordsKpiStats != null)
                        {
                            kpi.ActualValue =
                                kpiData.AdwordsKpiStats.CTR.ToString(CultureInfo.InvariantCulture);
                            kpi.Status = GetStatusFromKpi(kpi);
                        }
                        else
                            kpi.Status = -2;
                        break;
                    case "dv1x1bsOTR":
                        if (kpiData.DoubleVerifyKpiStats != null)
                        {
                            kpi.ActualValue =
                                kpiData.DoubleVerifyKpiStats.BrandSafetyOnTargetPercent_1x1.ToString(CultureInfo.InvariantCulture);
                            kpi.Status = GetStatusFromKpi(kpi);
                        }
                        else
                            kpi.Status = -2;
                        break;
                    case "dv1x1frdOTR":
                        if (kpiData.DoubleVerifyKpiStats != null)
                        {
                            kpi.ActualValue =
                                kpiData.DoubleVerifyKpiStats.FraudSivtRate_1x1.ToString(CultureInfo.InvariantCulture);
                            kpi.Status = GetStatusFromKpi(kpi);
                        }
                        else
                            kpi.Status = -2;
                        break;
                    case "dv1x1geoOTR":
                        if (kpiData.DoubleVerifyKpiStats != null)
                        {
                            kpi.ActualValue =
                                kpiData.DoubleVerifyKpiStats.InGeoRate_1x1.ToString(CultureInfo.InvariantCulture);
                            kpi.Status = GetStatusFromKpi(kpi);
                        }
                        else
                            kpi.Status = -2;
                        break;
                    case "dvfrdOTR":
                        if (kpiData.DoubleVerifyKpiStats != null)
                        {
                            kpi.ActualValue =
                                kpiData.DoubleVerifyKpiStats.FraudSivtRate_1x1.ToString(CultureInfo.InvariantCulture);
                            kpi.Status = GetStatusFromKpi(kpi);
                        }
                        else
                            kpi.Status = -2;
                        break;
                    case "dvmrcvOTR":
                        if (kpiData.DoubleVerifyKpiStats != null)
                        {
                            kpi.ActualValue =
                                kpiData.DoubleVerifyKpiStats.VideoViewableRate.ToString(CultureInfo.InvariantCulture);
                            kpi.Status = GetStatusFromKpi(kpi);
                        }
                        else
                            kpi.Status = -2;
                        break;
                    case "enGRP":
                        kpi.ActualValue = "I don't know what this is!!";
                        kpi.Status = -2;
                        break;
                    case "IMP":
                        if (kpiData.AdwordsKpiStats != null)
                        {
                            kpi.ActualValue =
                                kpiData.AdwordsKpiStats.Impressions.ToString(CultureInfo.InvariantCulture);
                            kpi.Status = GetStatusFromKpi(kpi);
                        }
                        else
                            kpi.Status = -2;
                        break;
                    case "mhumOTR":
                        if (kpiData.MoatKpiStats != null)
                        {
                            kpi.ActualValue =
                                kpiData.MoatKpiStats.HumanRate.ToString(CultureInfo.InvariantCulture);
                            kpi.Status = GetStatusFromKpi(kpi);
                        }
                        else
                            kpi.Status = -2;
                        break;
                    case "mmrcvOTR":
                        if (kpiData.MoatKpiStats != null)
                        {
                            kpi.ActualValue =
                                kpiData.MoatKpiStats.Two_Sec_In_View_Rate_Unfiltered.ToString(CultureInfo.InvariantCulture);
                            kpi.Status = GetStatusFromKpi(kpi);
                        }
                        else
                            kpi.Status = -2;
                        break;
                    case "VR":
                        if (kpiData.AdwordsKpiStats != null)
                        {
                            kpi.ActualValue =
                                kpiData.AdwordsKpiStats.VR.ToString(CultureInfo.InvariantCulture);
                            kpi.Status = GetStatusFromKpi(kpi);
                        }
                        else
                            kpi.Status = -2;
                        break;
                    default:
                        break;
                }
            });

            return kpiData;
        }


        private int GetStatusFromKpi(KpiDefinitionData kpi)
        {
            int status = 0;
            switch (kpi.ConditionDisplay)
            {
                case "<=":
                    status = Convert.ToDecimal(kpi.ActualValue) <= Convert.ToDecimal(kpi.EquationValue) ? 1 : -1;
                    break;
                case ">=":
                    status = Convert.ToDecimal(kpi.ActualValue) >= Convert.ToDecimal(kpi.EquationValue) ? 1 : -1;
                    break;
                case "<":
                    status = Convert.ToDecimal(kpi.ActualValue) < Convert.ToDecimal(kpi.EquationValue) ? 1 : -1;
                    break;
                case ">":
                    status = Convert.ToDecimal(kpi.ActualValue) > Convert.ToDecimal(kpi.EquationValue) ? 1 : -1;
                    break;
                case "=":
                    status = Convert.ToDecimal(kpi.ActualValue) == Convert.ToDecimal(kpi.EquationValue) ? 1 : -1;
                    break;
                default:
                    break;
            }
            return status;
        }



        private AlertKpiDataObject GetAlertKpiDataFromCustomerId(long customerId, string customerName, DateTime startDate, DateTime endDate, int totalDays, int daysSoFar, decimal percentComplete, int daysLeft)
        {
            var kpiData = new AlertKpiDataObject(customerId, customerName, startDate, endDate, totalDays, daysSoFar, percentComplete, daysLeft);
            kpiData.KpiDefinitions = _alertCentralService.GetKpiDefinitionData(customerId);
            kpiData.AdwordsKpiStats = _alertCentralService.GetAdwordsKpiStatsDataByCampaignAndDates(customerId, startDate, endDate);
            kpiData.DoubleVerifyKpiStats = _alertCentralService.GetDoubleVerifyKpiStatsDataByCampaignAndDates(customerId, startDate, endDate);
            kpiData.MoatKpiStats = _alertCentralService.GetMoatKpiStatsDataByCampaignAndDates(customerId, startDate, endDate);
            kpiData.NielsenKpiStats = _alertCentralService.GetNielsenKpiStatsDataByCampaignAndDates(customerId, startDate, endDate);

            return kpiData;
        }

    }
}
