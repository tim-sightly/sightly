﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using Sightly.API.Common;
using Sightly.API.Models;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessServices.cs;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/pacing")]
    public class PacingController : CommonApiController
    {
        private readonly IPacingService _pacingService;
        private readonly ILoggingWrapper _loggingWrapper;
        private readonly IUserService _userService;

        public PacingController(IPacingService pacingService, ILoggingWrapper loggingWrapper, IUserService userService)
        {
            _pacingService = pacingService;
            _loggingWrapper = loggingWrapper;
            _userService = userService;
        }


        [HttpGet]
        [Route("getMediaAgencyPacingByDate")]
        public IHttpActionResult GetMediaAgencyPacingByDate(string managerEmail)
        {
            DateTime pacingDate = DateTime.Now;
            var campaignPacingData = new List<CampaignPacingData>();

            try
            {
                List<PlacementAssociation> placements = _pacingService.GetMediaAgencyPlacementByDate(pacingDate);
                campaignPacingData.AddRange(_pacingService.GetMediaAgencyPacingDataByDate(pacingDate));

                placements.ForEach(place =>
                {
                    var camp = campaignPacingData.FirstOrDefault(c => c.AdWordsCampaignId == place.AdwordsCampaignId);
                    if (camp != null)
                        camp.PlacementValue = place.PlacementValue;
                });

                campaignPacingData.ForEach(campac=>
                {
                    campac.PlacementBudget = campaignPacingData.Where(x => x.PlacementValue == campac.PlacementValue)
                        .Sum(pac => pac.BudgetAmount);
                });

                //This is done to prevent weirdness when grouping by PlacementValue in javascript
                campaignPacingData.ForEach(cp =>
                {
                    if (string.IsNullOrEmpty(cp.PlacementValue))
                    {
                        cp.PlacementValue = Guid.NewGuid().ToString();
                    }
                });

                if (campaignPacingData.Count < 1)
                    return NotFound();

                if (string.IsNullOrEmpty(managerEmail))
                {
                    return Ok(campaignPacingData);
                }
                else
                {
                    List<CampaignPacingData> campainPacingDataFiltered = campaignPacingData.Where(cp => cp.CampaignManagerEmail == managerEmail).ToList();
                    return Ok(campainPacingDataFiltered);
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getLastPacedDate")]
        public IHttpActionResult GetLastPacedDate()
        {
            try
            {
                List<CustomerPacingDate> customerPacingDates = _pacingService.GetLastPaceDateForAllCustomers();
                return Ok(customerPacingDates);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("saveProposedBudgetPacing")]
        public IHttpActionResult SaveProposedBudgetPacing([FromBody] ProposedBudgetPacingViewModel proposedBudgetPacingViewModel)
        {
            if (proposedBudgetPacingViewModel.UserEmail == string.Empty)
                throw new Exception("Missing User Email.");

            try
            {
                var campaignsAffected = new List<string>();
                var pacingDate = DateTime.Now;
                var userId = _userService.GetUserByEmail(proposedBudgetPacingViewModel.UserEmail).UserId != null ? _userService.GetUserByEmail(proposedBudgetPacingViewModel.UserEmail).UserId : null;
                if (userId == null)
                    throw new Exception("Missing User information");


                proposedBudgetPacingViewModel.CampaignPaces.ForEach(cp =>
                {
                    cp.PacingDate = pacingDate;
                    _pacingService.SaveProposedBudgetPacing(
                                cp.CustomerId,
                                cp.CampaignId,
                                cp.RecommendedBudget,
                                cp.OverPacingRate,
                                cp.StartDate,
                                cp.TotalBudget,
                                cp.DaysRemaining,
                                cp.BudgetGroupTimedBudgetId,
                                pacingDate,
                                userId.Value);
                    campaignsAffected.Add(cp.CampaignName);
                });

                return Ok(campaignsAffected);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getManagers")]
        public IHttpActionResult GetManagers()
        {
            try
            {
                List<Manager> managers = _pacingService.GetManagers();
                return Ok(managers);
            }

            catch(Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }




    }
}
