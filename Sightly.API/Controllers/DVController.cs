﻿using OfficeOpenXml;
using Sightly.Logging;
using Sightly.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Web;
using System.Web.Http;
using Sightly.Business.StatsGather.Raw;


namespace Sightly.API.Controllers
{
    //[JwtAuthorize]
    [RoutePrefix("api/dv")]
    public class DVController : CommonApiController
    {
        private readonly ILoggingWrapper _loggingWrapper;
        private readonly IRawStatsService _rawStatsService;

        public DVController(ILoggingWrapper loggingWrapper, IRawStatsService rawStatsService)
        {
            _loggingWrapper = loggingWrapper;
            _rawStatsService = rawStatsService;
        }


        [HttpPost]
        [Route("upload")]
        public IHttpActionResult Upload()
        {
            List<DoubleVerifyImport> doubleVerifyImports = new List<DoubleVerifyImport>();

            if (HttpContext.Current.Request.Files.Count == 0)
                return BadRequest("No files posted.");


            HttpPostedFile postedFile = HttpContext.Current.Request.Files[0];

            //If file not .xlsx then return error
            if (postedFile.ContentType != "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                throw new Exception("Wrong file type. Must be a .xlsx!");

            MemoryStream ms = new MemoryStream();
            postedFile.InputStream.CopyTo(ms);


            ExcelPackage excel = new ExcelPackage(ms);

            //Worksheets and cells have 1-based indexes, NOT 0-based!!!
            ExcelWorksheet worksheet = excel.Workbook.Worksheets[1];

            var start = worksheet.Dimension.Start;
            var end = worksheet.Dimension.End;
            try
            {
                for (int row = start.Row + 1; row <= end.Row; row++)
                {
                    if (worksheet.Cells[row, 2].Value == null)
                        break;
                    if (worksheet.Cells[1, 1].Value.ToString() == "Media Type")
                    {
                        DoubleVerifyImport im = new DoubleVerifyImport();
                        im.PlacementId = Convert.ToInt64(worksheet.Cells[row, 6].Value.ToString());
                        im.PlacementName = worksheet.Cells[row, 5].Value.ToString();
                        im.StatDate = Convert.ToDateTime(worksheet.Cells[row, 7].Value.ToString());
                        im.MonitoredImpressions = Convert.ToInt64(worksheet.Cells[row, 12].Value.ToString());
                        im.BrandSafeImpression_1x1 = Convert.ToInt64(worksheet.Cells[row, 17].Value.ToString());
                        im.FraudSivtFreeImpression_1x1 = Convert.ToInt64(worksheet.Cells[row, 16].Value.ToString());
                        im.InGeoImpression_1x1 = Convert.ToInt64(worksheet.Cells[row, 18].Value.ToString());
                        im.DeviceType = "Combined";
                        doubleVerifyImports.Add(im);
                    }
                    else
                    {
                        DoubleVerifyImport im = new DoubleVerifyImport();
                        im.PlacementId = Convert.ToInt64(worksheet.Cells[row, 6].Value.ToString());
                        im.PlacementName = worksheet.Cells[row, 5].Value.ToString();
                        im.StatDate = Convert.ToDateTime(worksheet.Cells[row, 8].Value.ToString());
                        im.DeviceType = worksheet.Cells[row, 7].Value.ToString();
                        im.MonitoredImpressions = Convert.ToInt64(worksheet.Cells[row, 9].Value.ToString());
                        im.FraudSivtFreeImpression = Convert.ToInt64(worksheet.Cells[row, 31].Value.ToString());
                        im.VideoViewableImpression = Convert.ToInt64(worksheet.Cells[row, 13].Value.ToString());
                        doubleVerifyImports.Add(im);
                    }
                }

            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
            

            var importedData = _rawStatsService.SaveOffDoubleVerify(doubleVerifyImports);

            return Ok(new { importedData });
        }
    }
}
