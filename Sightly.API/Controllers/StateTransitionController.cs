﻿using System;
using System.Web.Http;
using Sightly.API.Common;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using Sightly.BusinessServices.cs;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/transition")]
    public class StateTransitionController : CommonApiController
    {
        private readonly ILoggingWrapper _loggingWrapper;
        private readonly IStateTransitionService _stateTransitionService;

        public StateTransitionController(ILoggingWrapper loggingWrapper, IStateTransitionService stateTransitionService)
        {
            _loggingWrapper = loggingWrapper;
            _stateTransitionService = stateTransitionService;
        }

        [HttpPost]
        [Route("{tolive}")]
        public IHttpActionResult ToLive([FromBody]BudgetGroupProposalRequest request)
        {
            if (!request.BudgetGroupProposalId.HasValue || !request.UserId.HasValue)
                return BadRequest();

            try
            {
                BudgetGroup budgetGroup = _stateTransitionService.ToLive(request.BudgetGroupProposalId.Value,
                    request.UserId.Value,
                    request.Notes);

                if (budgetGroup == null)
                    throw new Exception();

                return Ok(budgetGroup);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("OrderSetToLive")]
        public IHttpActionResult OrderSetToLive(OrdersProposalRequest request)
        {
            if (!request.OrderProposalId.HasValue || !request.UserId.HasValue)
                return BadRequest();
            try
            {
                var budgetGroups = _stateTransitionService.SetLiveByOrderId(
                    request.OrderProposalId.Value,
                    request.UserId.Value,
                    request.Notes);

                if (budgetGroups.Count == 0)
                    throw new Exception();

                return Ok(budgetGroups);


            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }


        public class BudgetGroupProposalRequest
        {
            public Guid? BudgetGroupProposalId { get; set; }
            public string Notes { get; set; }
            public Guid? UserId { get; set; }
        }
        public class OrdersProposalRequest
        {
            public Guid? OrderProposalId { get; set; }
            public string Notes { get; set; }
            public Guid? UserId { get; set; }
        }
    }
}
