﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Sightly.API.Common;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessServices.cs;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/kpiEditor")]
    public class KpiEditorController : CommonApiController
    {
        private readonly ILoggingWrapper _loggingWrapper;
        private readonly IKpiService _kpiService;

        public KpiEditorController(ILoggingWrapper loggingWrapper, IKpiService kpiService)
        {
            _loggingWrapper = loggingWrapper;
            _kpiService = kpiService;
        }

        [HttpGet]
        [Route("getKpiConditions")]
        public IHttpActionResult GetKpiConditions()
        {
            try
            {
                List<KpiCondition> kpiConditions = _kpiService.GetKpiConditions();
                return Ok(new {kpiConditions});
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getKpiMetrics")]
        public IHttpActionResult GetKpiMetrics()
        {
            try
            {
                List<KpiMetric> kpiMetrics = _kpiService.GetKpiMetrics();
                return Ok(new { kpiMetrics });
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getKpiEditorData")]
        public IHttpActionResult GetKpiEditorDataByCustomer(long customerId)
        {
            try
            {
                List<KpiEditorData> kpiEditorData = _kpiService.GetKpiEditorDataByCustomerId(customerId);
                return Ok(new { kpiEditorData });
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("SaveKpiEditorData")]
        public IHttpActionResult SaveKpiEditorData(KpiEditorDataRequest request)
        {
            try
            {
                var kpiEditorData = _kpiService.SaveKpiEditorData(
                    request.AdwordsCustomerId,
                    request.KpiMetricId,
                    request.EquationTypeId,
                    request.EquationValue,
                    request.Sequence
                );

                if (kpiEditorData != 1)
                    return BadRequest("Service was unable to save");
                return Ok();
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("removeKpiData/{adwordsCustomerKpiId}")]
        public IHttpActionResult RemoveKpiEditorData(int adwordsCustomerKpiId)
        {
            try
            {
                _kpiService.RemoveKpiEditorData(adwordsCustomerKpiId);

                return Ok();
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }
    }

    public class KpiEditorDataRequest
    {
        public long AdwordsCustomerId { get; set; }
        public int KpiMetricId { get; set; }
        public int EquationTypeId { get; set; }
        public string EquationValue { get; set; }
        public int Sequence { get; set; }
   }
}

