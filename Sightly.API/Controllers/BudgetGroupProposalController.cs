﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Sightly.API.Common;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using Sightly.BusinessServices;
using Sightly.BusinessServices.cs;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/budgetgroupproposal")]
    public class BudgetGroupProposalController : CommonApiController
    {
        //TODO: Convert to using IUserService
        //private readonly IBudgetGroupProposalService _budgetGroupProposalService;
        //private readonly IUserService _userService;
        //private readonly ILoggingWrapper _loggingWrapper;

        //public BudgetGroupProposalController()
        //{
        //}

        //public BudgetGroupProposalController(IBudgetGroupProposalService budgetGroupProposalService, IUserService userService, ILoggingWrapper loggingWrapper)
        //{
        //    _budgetGroupProposalService = budgetGroupProposalService;
        //    _userService = userService;
        //    _loggingWrapper = loggingWrapper;
        //}

        //[HttpPost]
        //[Route("create")]
        //public IHttpActionResult CreateBudgetGroupProposal([FromBody] BudgetGroupProposalViewModel budgetGroupProposalViewModel)
        //{
        //    if (budgetGroupProposalViewModel == null)
        //        return BadRequest();

        //    try
        //    {
        //        ApiSession apiSession = _userService.(budgetGroupProposalViewModel.ApiSessionId.Value);

        //        //Map data posted to this action method to Sightly domain model shape.
        //        //BudgetGroupProposalViewModel uses Filam's shape for compatibility. 
        //        Order order = new Order()
        //        {
        //            OrderId = Guid.NewGuid(),
        //            OrderTypeId = Guid.Parse("A345DD13-2B34-4BA6-8A05-41000B150770"),
        //            OrderName = budgetGroupProposalViewModel.Order.OrderName,
        //            OrderRefCode = budgetGroupProposalViewModel.Order.OrderRefCode,
        //            AccountId = budgetGroupProposalViewModel.Order.AccountId,
        //            CampaignId = budgetGroupProposalViewModel.Order.CampaignId,
        //            AdvertiserId = budgetGroupProposalViewModel.Order.AdvertiserId,
        //            AdvertiserSubCategoryId = budgetGroupProposalViewModel.Order.AdvertiserSubCategoryId,
        //            BudgetTypeId = budgetGroupProposalViewModel.Order.BudgetTypeId,
        //            CampaignBudget = budgetGroupProposalViewModel.Order.CampaignBudget,
        //            MonthlyBudget = budgetGroupProposalViewModel.Order.MonthlyBudget,
        //            MonthCount = budgetGroupProposalViewModel.Order.MonthCount,
        //            SpendFlex = budgetGroupProposalViewModel.Order.SpendFlex,
        //            DeliveryPriorityId = null,
        //            EndDate = budgetGroupProposalViewModel.Order.EndDate,
        //            EndDateHard = budgetGroupProposalViewModel.Order.EndDateHard,
        //            StartDate = budgetGroupProposalViewModel.Order.StartDate,
        //            StartDateHard = budgetGroupProposalViewModel.Order.StartDateHard,
        //            Deleted = false,
        //            CreatedDatetime = DateTime.UtcNow,
        //            CreatedBy = apiSession.UserId.ToString(),
        //            CreatorId = apiSession.UserId,
        //            LastModifiedDateTime = DateTime.UtcNow,
        //            LastModifiedBy = apiSession.UserId.ToString()
        //        };

        //        BudgetGroupProposal budgetGroupProposal = new BudgetGroupProposal()
        //        {
        //            BudgetGroupName = budgetGroupProposalViewModel.Order.OrderName,
        //            BudgetGroupBudget = budgetGroupProposalViewModel.Order.CampaignBudget,
        //            //BudgetGroupTimedBudgetProposals = new BudgetGroupTimedBudgetProposal() = new BudgetGroupTimedBudgetProposal()
        //            //{
        //            //    BudgetAmount = budgetGroupProposalViewModel.Order.CampaignBudget,
        //            //    Margin = null,
        //            //    StartDate = budgetGroupProposalViewModel.Order.StartDate,
        //            //    EndDate = budgetGroupProposalViewModel.Order.EndDate
        //            //}
        //        };

        //        BudgetGroupProposal newBudgetGroupProposal = _budgetGroupProposalService.Create(budgetGroupProposal, order, apiSession.UserId);

        //        if (newBudgetGroupProposal == null)
        //            throw new Exception("Something went wrong. Unable to create BudgetGroupProposal");

        //        return Ok(newBudgetGroupProposal);
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggingWrapper.Logger.Error(ex);
        //        return GetResponseForException(ex);
        //    }
        //}

        //[HttpGet]
        //[Route("{id}")]
        //public IHttpActionResult GetBudgetGroupProposalById(Guid? id)
        //{
        //    BudgetGroupProposal budgetGroupProposal = null;

        //    if (!id.HasValue)
        //        return BadRequest();

        //    try
        //    {
        //        budgetGroupProposal = _budgetGroupProposalService.GetById(id.Value);
        //        if (budgetGroupProposal == null)
        //            return NotFound();
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggingWrapper.Logger.Error(ex);
        //        return base.GetResponseForException(ex);
        //    }
        //    return Ok(budgetGroupProposal);
        //}

        //[HttpGet]
        //[Route("get/all")]
        //public IHttpActionResult GetAll()
        //{
        //    List<BudgetGroupProposal> budgetGroupProposals = null;

        //    try
        //    {
        //        budgetGroupProposals = _budgetGroupProposalService.GetAll();
        //        if (budgetGroupProposals == null)
        //            return InternalServerError();
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggingWrapper.Logger.Error(ex);
        //        return base.GetResponseForException(ex);
        //    }
        //    return Ok(budgetGroupProposals);
        //}

        ////[HttpPost]
        ////[Route("status/update")]
        ////public IHttpActionResult BudgetGroupProposalStatusUpdate([FromBody] BudgetGroupProposal budgetGroupProposal)
        ////{
        ////    if (budgetGroupProposal == null || !budgetGroupProposal.BudgetGroupId.HasValue ||
        ////        !Enum.IsDefined(typeof(BudgetGroupStatus), budgetGroupProposal.BudgetGroupStatusId))
        ////    {
        ////        return BadRequest();
        ////    }

        ////    BudgetGroupProposal updatedBudgetGroupProposal = null;
        ////    Guid id = budgetGroupProposal.BudgetGroupProposalId.Value;
        ////    BudgetGroupStatus status = (BudgetGroupStatus) budgetGroupProposal.BudgetGroupStatusId.Value;
        ////    Guid user = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");

        ////    try
        ////    {
        ////        updatedBudgetGroupProposal = _budgetGroupProposalService.UpdateStatus(id, status, user);
        ////        if (updatedBudgetGroupProposal == null)
        ////            throw new Exception("Something went wrong. Unable to update BudgetGroupProposal status.");
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        _loggingWrapper.Logger.Error(ex);
        ////        return base.GetResponseForException(ex);
        ////    }
        ////    return Ok(updatedBudgetGroupProposal);
        ////}

        //[HttpPut]
        //[Route("update")]
        //public IHttpActionResult BudgetGroupProposalUpdate([FromBody] BudgetGroupProposal budgetGroupProposal)
        //{
        //    Guid user = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");
        //    BudgetGroupProposal updatedBudgetGroupProposal = null;

        //    try
        //    {
        //        updatedBudgetGroupProposal = _budgetGroupProposalService.Update(budgetGroupProposal, user);
        //        if (updatedBudgetGroupProposal == null)
        //            throw new Exception("Something went wrong. Unable to update BudgetGroupProposal.");
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggingWrapper.Logger.Error(ex);
        //        return base.GetResponseForException(ex);
        //    }

        //    return Ok(updatedBudgetGroupProposal);
        //}

        //[HttpPost]
        //[Route("mediabriefpulled")]
        //public IHttpActionResult MediaBriefPulled([FromBody] RequestParameter requestParameter)
        //{
        //    Guid user = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");

        //    if (requestParameter?.BudgetGroupProposalId == null)
        //        return BadRequest();

        //    try
        //    {
        //        BudgetGroupProposal budgetGroupProposal = _budgetGroupProposalService.MediaBriefPulled(requestParameter.BudgetGroupProposalId.Value, user, requestParameter.Notes);
        //        return Ok(budgetGroupProposal);
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggingWrapper.Logger.Error(ex);
        //        return base.GetResponseForException(ex);
        //    }
        //}

        //[HttpPost]
        //[Route("mediabriefprovisioned")]
        //public IHttpActionResult MediaBriefProvisioned([FromBody] RequestParameter requestParameter)
        //{
        //    Guid user = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");

        //    if (requestParameter?.BudgetGroupProposalId == null)
        //        return BadRequest();

        //    try
        //    {
        //        BudgetGroupProposal budgetGroupProposal = _budgetGroupProposalService.MediaBriefProvisioned(requestParameter.BudgetGroupProposalId.Value, user, requestParameter.Notes);
        //        return Ok(budgetGroupProposal);
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggingWrapper.Logger.Error(ex);
        //        return base.GetResponseForException(ex);
        //    }
        //}

        //[HttpPost]
        //[Route("statsavailable")]
        //public IHttpActionResult StatsAvailable([FromBody] RequestParameter requestParameter)
        //{
        //    Guid user = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");

        //    if (requestParameter?.BudgetGroupProposalId == null)
        //        return BadRequest();

        //    try
        //    {
        //        BudgetGroup budgetGroup = _budgetGroupProposalService.MoveProposalToLive(
        //            requestParameter.BudgetGroupProposalId.Value,
        //            user,
        //            string.IsNullOrEmpty(requestParameter.Notes) ? null : requestParameter.Notes
        //        );

        //        return Ok(budgetGroup);
        //    }
        //    catch (Exception ex)
        //    {
        //        _loggingWrapper.Logger.Error(ex);
        //        return base.GetResponseForException(ex);
        //    }
        //}

        //#region Helpers

        //public class RequestParameter
        //{
        //    public Guid? BudgetGroupProposalId { get; set; }
        //    public string Notes { get; set; }
        //}

        //#endregion
    }
}
