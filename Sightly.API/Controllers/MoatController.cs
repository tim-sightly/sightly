﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Sightly.API.Common;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessServices.cs;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/moat")]
    public class MoatController : CommonApiController
    {
        private readonly IMoatService _moatService;
        private readonly ILoggingWrapper _loggingWrapper;


        public MoatController(IMoatService moatService, ILoggingWrapper loggingWrapper)
        {
            _moatService = moatService;
            _loggingWrapper = loggingWrapper;
        }


        [HttpGet]
        [Route("getDailyMoatStatsForAccount/{accountId}")]
        public IHttpActionResult GetDailyMoatStatsFroAccount(Guid accountId)
        {
            try
            {
                List<MoatDailyStatData> moatData = new List<MoatDailyStatData>();
                var moatDailyStatData = _moatService.GetMoatKpiStatsDataByAccount(accountId);
                moatDailyStatData.ForEach(day =>
                {
                    moatData.Add(new MoatDailyStatData
                    {
                        Id = accountId,
                        Type = "account",
                        StatDate = day.StatDate.Value,
                        Two_Sec_In_View_Rate_Unfiltered = day.Two_Sec_In_View_Rate_Unfiltered,
                        ReachCompletePercent = day.ReachCompletePercent,
                        CustomerQuality = day.CustomerQuality,
                        HumanAndAvocRate = day.HumanAndAvocRate,
                        HumanRate = day.HumanRate
                    });
                });

                return Ok(moatData);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getDailyMoatStatsForAdvertiser/{advertiserId}")]
        public IHttpActionResult GetDailyMoatStatsForAdvertiser(Guid advertiserId)
        {
            try
            {
                List<MoatDailyStatData> moatData = new List<MoatDailyStatData>();
                var moatDailyStatData = _moatService.GetMoatKpiStatsDataByAdvertiser(advertiserId);
                moatDailyStatData.ForEach(day =>
                {
                    moatData.Add(new MoatDailyStatData
                    {
                        Id = advertiserId,
                        Type = "advertiser",
                        StatDate = day.StatDate.Value,
                        Two_Sec_In_View_Rate_Unfiltered = day.Two_Sec_In_View_Rate_Unfiltered,
                        ReachCompletePercent = day.ReachCompletePercent,
                        CustomerQuality = day.CustomerQuality,
                        HumanAndAvocRate = day.HumanAndAvocRate,
                        HumanRate = day.HumanRate
                    });
                });

                return Ok(moatData);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getDailyMoatStatsForCampaign/{campaignId}")]
        public IHttpActionResult GetDailyMoatStatsForCampaign(Guid campaignId)
        {
            try
            {
                List<MoatDailyStatData> moatData = new List<MoatDailyStatData>();
                var moatDailyStatData = _moatService.GetMoatKpiStatsDataByCampaign(campaignId);
                moatDailyStatData.ForEach(day =>
                {
                    moatData.Add(new MoatDailyStatData
                    {
                        Id = campaignId,
                        Type = "campaign",
                        StatDate = day.StatDate.Value,
                        Two_Sec_In_View_Rate_Unfiltered = day.Two_Sec_In_View_Rate_Unfiltered,
                        ReachCompletePercent = day.ReachCompletePercent,
                        CustomerQuality = day.CustomerQuality,
                        HumanAndAvocRate = day.HumanAndAvocRate,
                        HumanRate = day.HumanRate,
                        AwCustomerId = day.AwCustomerId
                    });
                });

                return Ok(moatData);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }
    }
}
