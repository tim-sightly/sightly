﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Http;
using Microsoft.Ajax.Utilities;
using Sightly.API.Common;
using Sightly.API.Models.IntakeEmail;
using Sightly.BusinessServices.cs;
using Sightly.Email;
using Sightly.Email.Templates;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/email")]
    public class EmailController : CommonApiController
    {
        private readonly IEmailClient _emailClient;
        private readonly IEmailService _emailService;
        private readonly ILoggingWrapper _loggingWrapper;

        public EmailController(IEmailClient emailClient, IEmailService emailService, ILoggingWrapper loggingWrapper)
        {
            _emailClient = emailClient;
            _emailService = emailService;
            _loggingWrapper = loggingWrapper;
        }

        [HttpPost]
        [Route("sendNewOrderLiveEmail/{orderId}")]

        public IHttpActionResult SendNewOrderLiveEmail(Guid orderId)
        {
            try
            {
                var orderData = _emailService.GetOrderEmailByOrder(orderId);

                //Alter the recipient for testing purposes
                //orderData.CreaterName = "stefan@sightly.com";

                var orderer = new NewOrderLive();
                var emailData = orderer.MakeNewOrderLive(orderData);

                _emailClient.SendEmail(emailData.SendToMailMessage());

                return Ok("NewOrderLive email sent");
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("sendNewOrderReadyEmail/{orderId}")]

        public IHttpActionResult SendNewOrderReadyEmail(Guid orderId)
        {
            try
            {
                var orderData = _emailService.GetOrderEmailByOrder(orderId);

                //Alter the recipient for testing purposes
                //orderData.CreaterName = "stefan@sightly.com";

                var orderer = new NewOrderReady();
                var emailData = orderer.MakeNewOrderReady(orderData);

                _emailClient.SendEmail(emailData.SendToMailMessage());

                return Ok("NewOrderReady email sent");
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("sendChangeOrderLiveEmail/{orderId}")]

        public IHttpActionResult SendChangeOrderLiveEmail(Guid orderId)
        {
            try
            {
                var orderData = _emailService.GetOrderEmailByOrder(orderId);

                //Alter the recipient for testing purposes
                //orderData.CreaterName = "crist@sightly.com";

                var orderer = new ChangeOrderLive();
                var emailData = orderer.MakeChangeOrderLive(orderData);

                _emailClient.SendEmail(emailData.SendToMailMessage());

                return Ok("NewOrderReady email sent");
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }
        
        [HttpPost]
        [Route("sendOrderIntakeNotificationForAdvanceLocal")]

        public IHttpActionResult SendOrderIntakeNotificationForAdvanceLocal(Models.IntakeEmail.Order order)
        {
            try
            {
                string initiatedByString = string.IsNullOrWhiteSpace(order.Email) ? "" : order.Email.Trim();
                initiatedByString = initiatedByString.TrimStart(',').TrimEnd(',');
                
                string[] initiatedBy = initiatedByString.Split(',');                

                string orderType = string.IsNullOrWhiteSpace(order.OrderType) ? "": order.OrderType.Trim();
                string accountName = string.IsNullOrWhiteSpace(order.BasicInfo.AccountName) ? "": order.BasicInfo.AccountName.Trim();
                string advertiserName = string.IsNullOrWhiteSpace(order.BasicInfo.AdvertiserName) ? "": order.BasicInfo.AdvertiserName.Trim();
                string campaignName = string.IsNullOrWhiteSpace(order.BasicInfo.CampaignName) ? "": order.BasicInfo.CampaignName.Trim();
                string orderName = string.IsNullOrWhiteSpace(order.BasicInfo.OrderName) ? "": order.BasicInfo.OrderName.Trim();
                string orderRefCode = string.IsNullOrWhiteSpace(order.BasicInfo.OrderRefCode)? "": order.BasicInfo.OrderRefCode.Trim();
                string startDate = order.BasicInfo.StartDate.HasValue? order.BasicInfo.StartDate.Value.Date.ToString(): "";
                string endDate = order.BasicInfo.EndDate.HasValue ? order.BasicInfo.EndDate.Value.Date.ToString() : "";
                string budget = order.BasicInfo.Budget.HasValue ? "$" + order.BasicInfo.Budget: "";

                string ageGroupString = string.IsNullOrWhiteSpace(order.Targeting.AgeGroups) ? "": order.Targeting.AgeGroups.Trim();
                string genderString = string.IsNullOrWhiteSpace(order.Targeting.GenderGroups) ? "": order.Targeting.GenderGroups.Trim();
                string parentalStatusString = string.IsNullOrWhiteSpace(order.Targeting.ParentalGroups) ? "": order.Targeting.ParentalGroups.Trim();
                
                
                string audienceTargetingNotes = string.IsNullOrWhiteSpace(order.Targeting.AudienceTargetingNotes) ? "": order.Targeting.AudienceTargetingNotes.Trim();
                string keyTerms = string.IsNullOrWhiteSpace(order.Targeting.KeyTerms) ? "": order.Targeting.KeyTerms.Trim();
                string competitorConquesting = string.IsNullOrWhiteSpace(order.Targeting.CompetitorConquesting) ? "" : order.Targeting.CompetitorConquesting.Trim();
                string objective = string.IsNullOrWhiteSpace(order.Targeting.Objective) ? "": order.Targeting.Objective.Trim();

                string locationListString = string.IsNullOrWhiteSpace(order.Locations)? "": order.Locations.Trim();
                
                
                List<Ad> ads = order.Ads;
                StringBuilder adStringBuilder = new StringBuilder("");
                ads.ForEach(ad =>
                {
                    adStringBuilder.AppendLine("<div>");

                    adStringBuilder.AppendLine($"<span style='inline-block; margin-right: 15px;'><strong>Ad Name:</strong> <span>{ad.AdName}</span></span>");
                    adStringBuilder.AppendLine($"<span style='inline-block; margin-right: 15px;'><strong>YouTube URL:</strong> <span>{ad.YouTubeUrl}</span></span>");
                    adStringBuilder.AppendLine($"<span style='inline-block; margin-right: 15px;'><strong>Display URL:</strong> <span>{ad.DisplayUrl}</span></span>");
                    adStringBuilder.AppendLine($"<span style='inline-block; margin-right: 15px;'><strong>Destination URL:</strong> <span>{ad.DestinationUrl}</span></span>");
                    adStringBuilder.AppendLine($"<span style='inline-block; margin-right: 15px;'><strong>Ad Length:</strong> <span>{ad.AdLength}</span></span>");
                    adStringBuilder.AppendLine($"<span style='inline-block; margin-right: 15px;'><strong>Companion Banner:</strong> <span>{ad.CompanionBanner}</span></span>");
                        
                    adStringBuilder.AppendLine("</div>");
                });
                
                string notes = string.IsNullOrWhiteSpace(order.Notes) ? "": order.Notes.Trim();

                List<string> recipients = new List<string>();
                recipients.AddRange(initiatedBy);
                recipients.Add("danielle@sightly.com");
                recipients.Add("adiadops@sightly.com");
                recipients.Add("aerequests@adihelp.net");

                List<string> ccList = new List<string>();
                ccList.Add("Cfernandez@advancelocal.com");


                recipients.RemoveAll(item => item.IsNullOrWhiteSpace());
                recipients = recipients.Select(recipient => recipient.Trim()).ToList();
                recipients = recipients.Distinct().ToList();


                string from = "order.noreply@sightly.com";
                string subject = $"{accountName}-{advertiserName}-{orderType.ToUpperInvariant()}";

                StringBuilder sb = new StringBuilder();
                sb.Append("<br clear='all' /><div>");
                sb.AppendLine($"<h2>Order Type: {orderType.ToUpperInvariant()}</h2>");
                sb.Append("<br clear='all' /><div>");

                sb.AppendLine("<h1>Basic Info</h1>");
                sb.AppendLine($"<div class='accountName'><strong>Account Name:</strong> <span>{accountName}</div>");
                sb.AppendLine($"<div class='advertiserName'><strong>Advertiser Name:</strong> <span>{advertiserName}</span></div>");
                sb.AppendLine($"<div class='campaignName'><strong>Campaign Name:</strong> <span>{campaignName}</span></div>");
                sb.AppendLine($"<div class='orderName'><strong>Order Name:</strong> <span>{orderName}</span></div>");
                sb.AppendLine($"<div class='orderRefCode'><strong>Order Ref Code:</strong> <span>{orderRefCode}</span></div>");
                sb.AppendLine($"<div class='startDate'><strong>Start Date:</strong> <span>{startDate?? ""}</span></div>");
                sb.AppendLine($"<div class='endDate'><strong>End Date:</strong> <span>{endDate}</span></div>");
                sb.AppendLine($"<div class='budget'><strong>Budget:</strong> <span>{budget}</span></div>");
                
                sb.AppendLine("<h1>Targeting</h1>");
                sb.AppendLine($"<div class='ageGroups'><strong>Age Groups:</strong> <span>{ageGroupString}</span></div>");
                sb.AppendLine($"<div class='genders'><strong>Gender:</strong> <span>{genderString}</span</div>");
                sb.AppendLine($"<div class='parentalStatus'><strong>Parental Status:</strong> <span>{parentalStatusString}</span></div>");
                sb.AppendLine($"<div class='audienceTargetingNotes'><strong>Audience Targeting Notes:</strong> <span>{audienceTargetingNotes}</span></div>");
                sb.AppendLine($"<div class='keyTerms'><strong>Key Terms:</strong> <span>{keyTerms}</span></div>");
                sb.AppendLine($"<div class='competitorConquesting'><strong>Competitor Conquesting:</strong> <span>{competitorConquesting}</span></div>");
                sb.AppendLine($"<div class='objectives'><strong>Objective:</strong> <span>{objective}</span>");
                
                sb.AppendLine("<h1>Location</h1>");
                sb.AppendLine($"<div><strong>Locations:</strong> <span>{locationListString}</span></div>");

                sb.AppendLine("<h1>Ads</h1>");
                if (ads.Count > 0)
                {
                    
                    sb.AppendLine($"{adStringBuilder}");
                }

                sb.AppendLine("<h1>Notes</h1>");
                sb.AppendLine($"<div><strong>Notes:</strong> <span>{notes}</span></div>");

                sb.Append("</div>");
                
                //to from subject body
                EmailData emailData = new EmailData(recipients, from, subject, sb.ToString(), ccList);
                _emailClient.SendEmail(emailData.SendToMailMessage());
                return Ok();
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }
    }
}
