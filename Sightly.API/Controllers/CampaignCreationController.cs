﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Dapper;
using Microsoft.Ajax.Utilities;
using Sightly.API.Common;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessServices.cs;
using Sightly.Logging;
using Sightly.Models;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/campaignCreation")]
    public class CampaignCreationController : CommonApiController
    {
        private readonly ICampaignCreationService _campaignCreationService;
        private readonly ILoggingWrapper _loggingWrapper;

        public CampaignCreationController(ICampaignCreationService campaignCreationService, ILoggingWrapper loggingWrapper)
        {
            _campaignCreationService = campaignCreationService;
            _loggingWrapper = loggingWrapper;
        }

        [HttpGet]
        [Route("getAccounts")]
        public IHttpActionResult GetAccounts()
        {
            try
            {
                List<Account> accounts = _campaignCreationService.GetAllAccounts();
                return Ok(new { accounts });
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getPermAccount")]
        public IHttpActionResult GetPermAccounts()
        {
            try
            {
                var userId = Guid.Parse(ActionContext.ActionArguments["UserId"].ToString());
                List<Account> accounts = _campaignCreationService.GetAllAccountsUnderUser(userId);
                return Ok(accounts);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Message: {ex.Message}, InnerException: {ex.InnerException}");
                //_loggingWrapper.Logger.Error(ex);
                // return GetResponseForException(ex);
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("getAdvertisersByAccount")]
        public IHttpActionResult GetAdvertisersByAccount(Guid accountId)
        {
            try
            {
                List<Advertiser> advertisers = _campaignCreationService.GetAdvertiserByAccountId(accountId);
                return Ok(new { advertisers });
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getPermAdvertisersByAccount")]
        public IHttpActionResult GetPermAdvertisersByAccount(Guid accountId)
        {
            try
            {
                var userId = Guid.Parse(ActionContext.ActionArguments["UserId"].ToString());
                List<Advertiser> advertisers = _campaignCreationService.GetAdvertiserByAccountIdAndUser(accountId, userId);
                return Ok(new { advertisers });
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getAllPermAdvertisers")]
        public IHttpActionResult GetAllPermAdvertisers()
        {
            try
            {
                var userId = Guid.Parse(ActionContext.ActionArguments["UserId"].ToString());
                List<Advertiser> advertisers = _campaignCreationService.GetAllPermAdvertisers(userId);

                return Ok(advertisers);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Message: {ex.Message}, InnerException: {ex.InnerException}");
                //_loggingWrapper.Logger.Error(ex);
                //return GetResponseForException(ex);
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("getAllPermCampaigns")]
        public IHttpActionResult GetAllPermCampaigns()
        {
            try
            {
                var userId = Guid.Parse(ActionContext.ActionArguments["UserId"].ToString());

                //TODO: Is using 'DistinctBy' to filter out app. 1300 duplicates treating the symptom but not the cause? 
                List<CampaignAbbreviated> campaigns = _campaignCreationService.GetAllPermCampaigns(userId)
                    .DistinctBy(c => c.CampaignId).ToList();
                return Ok(campaigns);
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Message: {ex.Message}, InnerException: {ex.InnerException}");
                //_loggingWrapper.Logger.Error(ex);
                // return GetResponseForException(ex);
                return InternalServerError(ex);
            }
        }

        [HttpGet]
        [Route("getCampaignsByAdvertiser")]
        public IHttpActionResult GetCampaignsByAdvertiser(string accountId, string advertiserId)
        {
            try
            {
                Guid accountGuid = Guid.Parse(accountId);
                Guid advertiserIdGuid = Guid.Parse(advertiserId);
                using (IDbConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["TargetView"].ToString()))
                {
                    string sql = @"SELECT c.[CampaignId]
                                  ,a.[AccountId]
                                  ,c.[AdvertiserId]
                                  ,c.[CampaignName]
                                  ,c.[CampaignRefCode]
                                  FROM [Accounts].[Campaign] c
                                    INNER JOIN [Accounts].[Advertiser] a ON c.AdvertiserId = a.AdvertiserId
                                  WHERE @AdvertiserId = c.AdvertiserId
                                  AND 
                                  @AccountId = a.AccountId
                                  ORDER BY c.CampaignName;";
                    List<CampaignAbbreviated> campaigns = conn.Query<CampaignAbbreviated>(sql, new { AccountId = accountGuid, AdvertiserId = advertiserIdGuid }).ToList();
                    return Ok(campaigns);
                }
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("CreateAccount")]
        public IHttpActionResult CreateSubAccount(SubAccountRequest request)
        {
            try
            {
                var account = _campaignCreationService.InsertSubAccount(request.AccountName, request.AccountTypeId, request.ParentAccountId);

                if (account != null)
                    return Ok();

                return InternalServerError(new Exception("Error in Creating a SubAccount"));
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("CreateAdvertiser")]
        public IHttpActionResult CreateAdvertiser(AdvertiserRequest request)
        {
            try
            {
                var advertiser = _campaignCreationService.InsertAdvertiser(request.AdvertiserName, request.AccountId);

                if (advertiser != null)
                    return Ok();

                return InternalServerError(new Exception("Error in Creating an Advertiser"));
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("CreateAdvertiserExtended")]
        public IHttpActionResult CreateAdvertiserExtended(AdvertiserExtendedRequest request)
        {
            try
            {
                var advertiser = _campaignCreationService.InsertAdvertiserExtended(
                    request.AdvertiserName, 
                    request.AdvertiserRefCode,
                    request.AccountId,
                    request.AdvertiserCategoryId,
                    request.AdvertiserSubCategoryId);

                if (advertiser != null)
                    return Ok(advertiser.AdvertiserId);

                return InternalServerError(new Exception("Error in Creating an Advertiser"));
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("CreateCampaign")]
        public IHttpActionResult CreateCampaign(CampaignRequest request)
        {
            try
            {
                var campaign = _campaignCreationService.InsertCampaign(request.CampaignName, request.AdvertiserId);

                if (campaign != null)
                    return Ok(campaign.CampaignId);

                return InternalServerError(new Exception("Error in Creating a Campaign"));
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("CreateOrder")]
        public IHttpActionResult CreateOrder(OrderRequest request)
        {
            try
            {
                var order = _campaignCreationService.InsertOrder(request.OrderName, request.OrderRefCode, request.CampaignId);

                if (order != null)
                    return Ok();

                return InternalServerError(new Exception("Error in Creating an Order"));
            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("createAccountToCampaign")]
        public IHttpActionResult CreateAccountToOrder(CombinedAccountToOrderRequest request)
        {
            try
            {
                if (!request.SubAccount.AccountId.HasValue)
                {
                    var account = _campaignCreationService.InsertSubAccount(request.SubAccount.AccountName, request.SubAccount.AccountTypeId, request.SubAccount.ParentAccountId);
                    if (account == null)
                        return InternalServerError(new Exception("Error in Creating a SubAccount"));

                    request.SubAccount.AccountId = account.AccountId.Value;
                    request.Advertiser.AccountId = account.AccountId.Value;
                }

                if (!request.Advertiser.AdvertiserId.HasValue)
                {
                    var advertiser = _campaignCreationService.InsertAdvertiser(request.Advertiser.AdvertiserName,
                        request.Advertiser.AccountId);
                    if (advertiser == null)
                        return InternalServerError(new Exception("Error in Creating an Advertiser"));

                    request.Campaign.AdvertiserId = advertiser.AdvertiserId;
                }

                if (!request.Campaign.CampaignId.HasValue)
                {
                    var campaign = _campaignCreationService.InsertCampaign(request.Campaign.CampaignName, request.Campaign.AdvertiserId);

                    if (campaign == null)
                        return InternalServerError(new Exception("Error in Creating a Campaign"));
                    request.Campaign.CampaignId = campaign.CampaignId;
                }


                var order = _campaignCreationService.InsertOrder(request.Order.OrderName, request.Order.OrderRefCode, request.Campaign.CampaignId.Value);

                if(order == null)
                    InternalServerError(new Exception("Error in Creating an Order"));

                return Ok();


            }
            catch (Exception ex)
            {
                //_loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

    }

    public class SubAccountRequest
    {
        public Guid? AccountId { get; set; }
        public string AccountName { get; set; }
        public Guid AccountTypeId { get; set; }
        public Guid ParentAccountId { get; set; }
    }

    public class AdvertiserRequest
    {
        public Guid? AdvertiserId { get; set; }
        public string AdvertiserName { get; set; }
        public Guid AccountId { get; set; }
    }

    public class AdvertiserExtendedRequest
    {
        public Guid? AdvertiserId { get; set; }
        public string AdvertiserName { get; set; }
        public string AdvertiserRefCode { get; set; }
        public Guid AccountId { get; set; }
        public Guid AdvertiserCategoryId { get; set; }
        public Guid AdvertiserSubCategoryId { get; set; }
    }

    public class CampaignRequest
    {
        public Guid? CampaignId { get; set; }
        public string CampaignName { get; set; }
        public Guid AdvertiserId { get; set; }
    }

    public class OrderRequest
    {
        public string OrderName { get; set; }
        public string OrderRefCode { get; set; }
        public Guid CampaignId { get; set; }
    }

    public class CombinedAccountToOrderRequest
    {
        public SubAccountRequest SubAccount { get; set; }
        public AdvertiserRequest Advertiser { get; set; }
        public CampaignRequest Campaign { get; set; }

        public OrderRequest Order { get; set; }
    }
}
