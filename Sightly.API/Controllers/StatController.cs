﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Sightly.API.Common;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessServices.cs;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/stat")]
    public class StatController : CommonApiController
    {
        private readonly IStatService _statService;
        private readonly ILoggingWrapper _loggingWrapper;
        private readonly IUserService _userService;

        public StatController(IStatService statService, ILoggingWrapper loggingWrapper, IUserService userService)
        {
            _statService = statService;
            _loggingWrapper = loggingWrapper;
            _userService = userService;
        }
        
        [HttpGet]
        [Route("getDailyStatsDeviceForAdWordsCustomerId")]
        public IHttpActionResult GetDailyDeviceStatsForAdWordsCustomerId(long adWordsCustomerId)
        {
            try
            {
                List<DailyStats> customerPacingDates = _statService.GetDailyDeviceStatsForAdWordsCustomerId(adWordsCustomerId);
                return Ok(customerPacingDates);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }
        
        [HttpGet]
        [Route("getDailyStatsDeviceForAccount")]
        public IHttpActionResult GetDailyStatsDeviceForAccount(Guid accountId)
        {
            try
            {

                List<DailyStats> stats = _statService.GetDailyDeviceStatsForAccount(accountId);
                stats.ForEach(s =>
                {
                    s.Id = accountId;
                    s.Type = "account";
                });
                return Ok(stats);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }
        
        [HttpGet]
        [Route("getDailyStatsDeviceForAdvertiser")]
        public IHttpActionResult GetDailyStatsDeviceForAdvertiser(Guid advertiserId)
        {
            try
            {
                List<DailyStats> stats = _statService.GetDailyDeviceStatsForAdvertiser(advertiserId);
                stats.ForEach(s =>
                {
                    s.Id = advertiserId;
                    s.Type = "advertiser";
                });
                
                return Ok(stats);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }
        
        [HttpGet]
        [Route("getDailyStatsDeviceForCampaign")]
        public IHttpActionResult GetDailyStatsDeviceForCampaign(Guid campaignId)
        {
            try
            {
                List<DailyStats> stats = _statService.GetDailyDeviceStatsForCampaign(campaignId);
                stats.ForEach(s =>
                {
                    s.Id = campaignId;
                    s.Type = "campaign";
                });
                return Ok(stats);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }
    }
}
