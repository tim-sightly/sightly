﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Sightly.API.Common;
using Sightly.BusinessLayer.DomainObjects;
using Sightly.BusinessServices.cs;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    //[JwtAuthorize]
    [RoutePrefix("api/nielsen")]
    public class NielsenController : CommonApiController
    {
        private readonly INielsenService _nielsenService;
        private readonly ILoggingWrapper _loggingWrapper;

        public NielsenController(INielsenService nielsenService, ILoggingWrapper loggingWrapper)
        {
            _nielsenService = nielsenService;
            _loggingWrapper = loggingWrapper;
        }

        [HttpGet]
        [Route("getDailyNielsenStatsForAccount/{accountId}")]
        public IHttpActionResult GetDailyNielsenStatsForAccount(Guid accountId)
        {
            try
            {
                List<NielsenDailyStats> nielsenStats = new List<NielsenDailyStats>();
                List<NielsenKpiStatsData> nielsenDailyStatsData = _nielsenService.GetNielsenKpiStatsDataByAccount(accountId);
                nielsenDailyStatsData.ForEach(day =>
                {
                    nielsenStats.Add(new NielsenDailyStats
                    {
                        Id = accountId,
                        Type = "account",
                        StatDate = day.StatDate.Value,
                        DarComputer =  day.DarComputer,
                        DarMobile = day.DarMobile,
                        DarTotalDigital = day.DarTotalDigital
                    });    
                });

                return Ok(nielsenStats);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getDailyNielsenForAdvertiser/{advertiserId}")]
        public IHttpActionResult GetDailyNielsenStatsForAdvertiser(Guid advertiserId)
        {
            try
            {
                List<NielsenDailyStats> nielsenStats = new List<NielsenDailyStats>();
                List<NielsenKpiStatsData> nielsenDailyStatsData = _nielsenService.GetNielsenKpiStatsDataByAdvertiser(advertiserId);
                nielsenDailyStatsData.ForEach(day =>
                {
                    nielsenStats.Add(new NielsenDailyStats
                    {
                        Id = advertiserId,
                        Type = "advertiser",
                        StatDate = day.StatDate.Value,
                        DarComputer = day.DarComputer,
                        DarMobile = day.DarMobile,
                        DarTotalDigital = day.DarTotalDigital
                    });
                });

                return Ok(nielsenStats);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }


        [HttpGet]
        [Route("getDailyNielsenForCampaign/{campaignId}")]
        public IHttpActionResult GetDailyNielsenForCampaign(Guid campaignId)
        {
            try
            {
                List<NielsenDailyStats> nielsenStats = new List<NielsenDailyStats>();
                List<NielsenKpiStatsData> nielsenDailyStatsData = _nielsenService.GetNielsenKpiStatsDataByCampaign(campaignId);
                nielsenDailyStatsData.ForEach(day =>
                {
                    nielsenStats.Add(new NielsenDailyStats
                    {
                        Id = campaignId,
                        Type = "campaign",
                        StatDate = day.StatDate.Value,
                        DarComputer = day.DarComputer,
                        DarMobile = day.DarMobile,
                        DarTotalDigital = day.DarTotalDigital,
                        AwCustomerId = day.AwCustomerId
                    });
                });

                return Ok(nielsenStats);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }
    }
}
