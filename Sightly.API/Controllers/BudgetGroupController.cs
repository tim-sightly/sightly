﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using Sightly.API.Common;
using Sightly.BusinessLayer.Common;
using Sightly.BusinessLayer.DomainObjects.BudgetGroupStates;
using Sightly.BusinessServices.cs;
using Sightly.Logging;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/budgetgroup")]
    public class BudgetGroupController : CommonApiController
    {
        private readonly IBudgetGroupService _budgetGroupService;
        private readonly ILoggingWrapper _loggingWrapper;

        public BudgetGroupController(IBudgetGroupService budgetGroupService, ILoggingWrapper loggingWrapper)
        {
            _budgetGroupService = budgetGroupService;
            _loggingWrapper = loggingWrapper;
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult BudgetGroupGetById(Guid? id)
        {
            if (!id.HasValue)
                return BadRequest();

            BudgetGroup budgetGroup = null;
            Guid user = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");

            try
            {
                budgetGroup = _budgetGroupService.GetById(id.Value, user);
                if (budgetGroup == null)
                    return NotFound();
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
            return Ok(budgetGroup);
        }

        [HttpGet]
        [Route("getall")]
        public IHttpActionResult GetAll()
        {
            List<BudgetGroup> budgetGroups = null;
            Guid user = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");

            try
            {
                budgetGroups = _budgetGroupService.GetAll(user);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
            return Ok(budgetGroups);
        }

        [HttpGet]
        [Route("getbystatus/{status}")]
        public IHttpActionResult GetByStatusId(BudgetGroupStatus? status)
        {
            if (!status.HasValue)
                return BadRequest();

            List<BudgetGroup> budgetGroups = null;
            Guid user = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");

            try
            {
                budgetGroups = _budgetGroupService.GetByBudgetGroupStatus(status.Value, user);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
            return Ok(budgetGroups);
        }

        [HttpPost]
        [Route("changeorder")]
        public IHttpActionResult ChangeOrder([FromBody]BudgetGroup budgetGroup)
        {
            Guid user = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");

            if (budgetGroup?.BudgetGroupId == null || !Enum.IsDefined(typeof(BudgetGroupStatus), budgetGroup.BudgetGroupStatusId))
            {
                return BadRequest();
            }

            BudgetGroupStatus status = (BudgetGroupStatus)budgetGroup.BudgetGroupStatusId.Value;

            try
            {
                BudgetGroup updatedBudgetGroup = _budgetGroupService.UpdateStatus(budgetGroup.BudgetGroupId.Value, status, user);
                return Ok(updatedBudgetGroup);
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("reneworder")]
        public IHttpActionResult RenewOrder([FromBody]BudgetGroup budgetGroup)
        {
            //Guid user = new Guid("B6B786BB-A8E3-4735-93ED-159964A0F6FE");

            //if (budgetGroup?.BudgetGroupId == null || !Enum.IsDefined(typeof(BudgetGroupStatus), budgetGroup.BudgetGroupStatusId))
            //{
            //    return BadRequest();
            //}

            //BudgetGroupStatus status = (BudgetGroupStatus)budgetGroup.BudgetGroupStatusId.Value;

            //try
            //{
            //    BudgetGroup updatedBudgetGroup = _budgetGroupService.UpdateStatus(budgetGroup.BudgetGroupId.Value, status, user);
            //    return Ok(updatedBudgetGroup);
            //}
            //catch (Exception ex)
            //{
            //_loggingWrapper.Logger.Error(ex);
            //    return GetResponseForException(ex);
            //}
            return Ok();
        }

        #region

        public class RequestParameter
        {
            public Guid? BudgetGroupProposalId { get; set; }
            public string Notes { get; set; }
        }

        #endregion
    }
}
