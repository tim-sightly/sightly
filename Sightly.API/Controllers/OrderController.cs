﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Web.Http;
using Dapper;
using Sightly.API.Common;
using Sightly.Business.OrderManager;
using Sightly.BusinessServices.cs;
using Sightly.Email;
using Sightly.Email.Templates;
using Sightly.Email.Templates.Error;
using Sightly.Email.Templates.Internal;
using Sightly.Logging;
using Sightly.Models;
using Sightly.Order;
using Sightly.Models.tv.extended;
using Sightly.ReportGeneration.Generators.Internal;

namespace Sightly.API.Controllers
{
    [JwtAuthorize]
    [RoutePrefix("api/order")]
    public class OrderController : CommonApiController
    {
        private ILoggingWrapper _loggingWrapper;
        private IOrderComparison _orderComparison;
        private IOrderManager _orderManager;
        private IOrderService _orderService;
        private IEmailClient _emailClient;
        private IEmailService _emailService;
        private readonly string _connString;

        public OrderController(ILoggingWrapper loggingWrapper, IOrderComparison orderComparison, IOrderManager orderManager, IOrderService orderService, IEmailClient emailClient, IEmailService emailService)
        {
            _loggingWrapper = loggingWrapper;
            _orderComparison = orderComparison;
            _orderManager = orderManager;
            _orderService = orderService;
            _emailClient = emailClient;
            _emailService = emailService;
            _connString = ConfigurationManager.ConnectionStrings["TargetView"].ToString();
        }

        [HttpGet]
        [Route("isOrderNameUnique")]
        public IHttpActionResult IsOrderNameUnique(string q)
        {
            try
            {
                using (IDbConnection conn = new SqlConnection(_connString))
                {
                    string sql = @"SELECT Count(1) AS Count FROM Orders.[Order]
                                 WHERE OrderName = @OrderName";
                    int count = conn.QuerySingle<Int32>(sql, new { OrderName = q });
                    return Ok(count == 0);
                }
            }
            catch (Exception ex)
            {
                _loggingWrapper.Logger.Error(ex);
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("searchOrderByString")]
        public IHttpActionResult SearchOrdersByString(string searchValue)
     {
            try
            {
                var userId = Guid.Parse(ActionContext.ActionArguments["UserId"].ToString());
                List<OrderLite> orders = _orderManager.SearchOrderLiteByString(searchValue, userId);

                return Ok(orders);
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getMostRecentOrders")]
        public IHttpActionResult GetMostRecentOrders(int top = 100)
        {
            try
            {
                var userId = Guid.Parse(ActionContext.ActionArguments["UserId"].ToString());

                using (IDbConnection conn = new SqlConnection(_connString))
                {
                    string sql = @"SELECT TOP (@Top)
                                        aaco.AccountName
        		                        ,aaco.AdvertiserId
        		                        ,aaco.AdvertiserName
        		                        ,aaco.CampaignId
                                        ,aaco.CampaignName
                                        ,aaco.OrderId
                                        ,aaco.OrderName
                                        ,aaco.OrderRefCode
                                        ,aaco.CampaignBudget
                                        ,aaco.StartDate
                                        ,aaco.EndDate
                                        ,aaco.OrderStatus
                                        ,aaco.BudgetAmount
                                FROM tvdb.AccAdvCamOrderDataWithCombinedBudgets aaco
                                INNER JOIN Users.UserXrefAdvertiser uxa
                                	ON aaco.AdvertiserId = uxa.AdvertiserId
                                WHERE uxa.UserId = @UserId
                                ORDER BY aaco.CreatedDatetime desc;";

                    List<OrderLite> orders = conn.Query<OrderLite>(sql, new {Top = top, UserId = userId}).ToList();
                    return Ok(orders);
                }
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }

        }

        [HttpPost]
        [Route("setOrderPauseStatus")]
        public IHttpActionResult SetOrderPauseStatus([FromBody]OrderPause orderPause)
        {
            try
            {
                string userName = ActionContext.ActionArguments["UserName"].ToString();
                if (String.IsNullOrEmpty(userName))
                {
                    throw new Exception("setOrderPauseStatus Action: Cannot retrieve UserName from JWT.");
                }

                bool isPaused = _orderManager.SetOrderPauseStatus(orderPause.orderId);

                try
                {
                    //Create an email to send to the Campaign Manager about the change of state
                    OrderPauseData orderPauseData = _orderManager.GetOrderPauseDataByOrderId(orderPause.orderId);
                    orderPauseData.IsPaused = isPaused;
                    //Internal
                    var pausedOrder = new PausedOrder();
                    var emailData = pausedOrder.CreateEmailData(orderPauseData);
                    _emailClient.SendEmail(emailData.SendToMailMessage());

                    //External
                    orderPauseData.CurrentUserEmail = userName;
                    orderPauseData.ActionDate = DateTime.Now.ToString("d");
                    var pausedOrderEmail = new OrderPauseStatusChange();
                    var pausedEmailData = pausedOrderEmail.MakePauseOrderStatusChange(orderPauseData);
                    _emailClient.SendEmail(pausedEmailData.SendToMailMessage());
                }
                catch (Exception ex)
                {
                    var orderData = _orderManager.GetExistingOrderExtended(orderPause.orderId);
                    var pauseEmail = new ErrorEmail();
                    var pauseErrorEmail = pauseEmail.CreateEmailData("Pause/Un-Pause", orderPause.orderId, orderData.Info.OrderName, ex);
                    _emailClient.SendEmail(pauseErrorEmail.SendToMailMessage());
                }


                return Ok(new { OrderId = orderPause.orderId, IsPaused = isPaused });
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("OrderCancel")]
        public IHttpActionResult SetOrderCancelled([FromBody]OrderCancelled orderCancelled)
        {
            try
            {
                string userName = ActionContext.ActionArguments["UserName"].ToString();
                if (String.IsNullOrEmpty(userName))
                {
                    throw new Exception("OrderSubmittal Action: Cannot retrieve UserName from JWT.");
                }

                bool isCancelled = _orderManager.SetOrderCancelled(orderCancelled.OrderId);

                //Create an email to send to the Campaign Manager about the cancellation
                try
                {
                    //Internal
                    OrderCancelData orderCancelData = _orderManager.GetOrderCancelDataByOrderId(orderCancelled.OrderId);
                    var cancelOrder = new CancelOrder();
                    var emailData = cancelOrder.CreateEmailData(orderCancelData);
                    _emailClient.SendEmail(emailData.SendToMailMessage());

                    orderCancelData.CurrentUserEmail = userName;
                    orderCancelData.ActionDate = DateTime.Now.ToString("d");
                    //External
                    var cancelOrderEmail = new OrderCancelledStatusChange();
                    var cancelEmailData = cancelOrderEmail.MakeCancelledOrderStatusChange(orderCancelData);
                    _emailClient.SendEmail(cancelEmailData.SendToMailMessage());
                }
                catch (Exception ex)
                {
                    var orderData = _orderManager.GetExistingOrderExtended(orderCancelled.OrderId);
                    var pauseEmail = new ErrorEmail();
                    var pauseErrorEmail = pauseEmail.CreateEmailData("Cancel", orderCancelled.OrderId, orderData.Info.OrderName, ex);
                    _emailClient.SendEmail(pauseErrorEmail.SendToMailMessage());
                }


                return Ok(new { OrderId = orderCancelled.OrderId });
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("orderSubmittal")]
        public IHttpActionResult OrderSubmitProcess(Sightly.Models.tv.Order order)
        {
            try
            {

                string userName = ActionContext.ActionArguments["UserName"].ToString();
                if (String.IsNullOrEmpty(userName))
                {
                    throw new Exception("Order Submittal Action: Cannot retrieve UserName from JWT.");
                }
                Guid orderId = _orderManager.CreateOrder(order);
                order.Info.OrderId = orderId;
                SaveOrderVersionForHistory(order);

                try
                {
                    //Create an email to send to the Campaign Manager about the new order
                var orderData = _emailService.GetOrderEmailByOrder(orderId);

                orderData.CreaterName = orderData.CampaignManagerEmail;

                NewOrder newOrder = new NewOrder();
                var emailData = newOrder.CreateEmailData(orderData);
                _emailClient.SendEmail(emailData.SendToMailMessage());

                
                orderData.CreaterName = userName;
                orderData.ActionDate = DateTime.Now.ToString("d");

                OrderSubmitted orderSubmitted = new OrderSubmitted();
                var otherEmailData = orderSubmitted.MakeOrderSubmitted(orderData);
                _emailClient.SendEmail(otherEmailData.SendToMailMessage());
                }
                catch (Exception ex)
                {
                    var pauseEmail = new ErrorEmail();
                    var pauseErrorEmail = pauseEmail.CreateEmailData("Submitted", orderId, order.Info.OrderName, ex);
                    _emailClient.SendEmail(pauseErrorEmail.SendToMailMessage());
                }
                
                

                return Ok(orderId);
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }

        }

        [HttpPost]
        [Route("orderUpdate")]

        public async Task<IHttpActionResult> OrderUpdate(Sightly.Models.tv.Order order)
        {
            try
            {
                //Get original order state before the change
                var originalOrder = _orderManager.GetExistingOrder(order.Info.OrderId.Value);
                

                string userName = ActionContext.ActionArguments["UserName"].ToString();
                if (String.IsNullOrEmpty(userName))
                {
                    throw new Exception("OrderUpdate Action: Cannot retrieve UserName from JWT.");
                }
                SaveOrderVersionForHistory(order);
                _orderManager.UpdateOrder(order);

                try
                {
                    var orderExtended = _orderManager.GetExistingOrderExtended(order.Info.OrderId.Value);

                    var orderChanges = _orderComparison.GetOrderDifferences(originalOrder, order);
                    orderChanges.SubmittedBy = userName;
                    orderChanges.SubmittedDate = DateTime.Now;
                    orderChanges.AccountName = orderExtended.Info.AccountName;
                    orderChanges.AdvertiserName = orderExtended.Info.AdvertiserName;
                    orderChanges.OrderName = orderExtended.Info.OrderName;

                    orderChanges.AudienceChanges.SpecificAgeRanges = string.Join(",",
                        orderExtended.Audience.AgeRanges.Select(x => x.AgeGroupName));
                    orderChanges.AudienceChanges.SpecificGenders = string.Join(",",
                        orderExtended.Audience.Genders.Select(x => x.GenderName));
                    orderChanges.AudienceChanges.SpecificHouseholdIncomes = string.Join(",",
                        orderExtended.Audience.HouseholdIncomes.Select(x => x.GroupName));
                    orderChanges.AudienceChanges.SpecificParentalStatus = string.Join(",",
                        orderExtended.Audience.ParentalStatuses.Select(x => x.ParentalStatusName));


                    ChangeOrderDetailExcel detailedReport = new ChangeOrderDetailExcel(orderChanges);


                    var contextType = new System.Net.Mime.ContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
                    var reportAttachment = new Attachment(detailedReport.ToStream(), contextType);
                    reportAttachment.ContentDisposition.FileName = $"{orderChanges.OrderName}_Changes.xlsx";

                    //Create an email to send to the Campaign Manager about the new order
                    var orderData = _emailService.GetOrderEmailByOrder(order.Info.OrderId.Value);

                    var orderChange = new ChangeOrder();
                    var changeData = orderChange.CreateEmailData(orderData);
                    _emailClient.SendEmail(changeData.SendToMailMessage());

                    orderData.CreaterName = userName;
                    orderData.EmailAttachment = reportAttachment;
                    orderData.ActionDate = DateTime.Now.ToString("d");
                    ChangeSubmitted changeOrder = new ChangeSubmitted();
                    var emailData = changeOrder.MakeChangeSubmitted(orderData);
                    _emailClient.SendEmail(emailData.SendToMailMessage());

                }
                catch (Exception ex)
                {
                    var pauseEmail = new ErrorEmail();
                    var pauseErrorEmail = pauseEmail.CreateEmailData("Update", order.Info.OrderId.Value, order.Info.OrderName, ex);
                    _emailClient.SendEmail(pauseErrorEmail.SendToMailMessage());
                }
               
                return Ok();

            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("reorderSubmittal")]
        public IHttpActionResult ReorderSubmitProcess(Sightly.Models.tv.Order order)
        {
            try
            {
                string userName = ActionContext.ActionArguments["UserName"].ToString();
                if (String.IsNullOrEmpty(userName))
                {
                    throw new Exception("Reorder Submittal Action: Cannot retrieve UserName from JWT.");
                }
                order.Info.ParentOrderId = order.Info.OrderId;
                order.Info.OrderId = Guid.NewGuid();
                SaveOrderVersionForHistory(order);
                Guid orderId = _orderManager.CreateReorder(order);

                try
                {
                    //Create an email to send to the Campaign Manager about the new order
                    var orderData = _emailService.GetOrderEmailByOrder(orderId);

                    orderData.CreaterName = userName;
                    orderData.ActionDate = DateTime.Now.ToString("d");

                    var newOrder = new NewOrder();
                    var emailData = newOrder.CreateEmailData(orderData);
                    _emailClient.SendEmail(emailData.SendToMailMessage());

                    OrderSubmitted orderSubmitted = new OrderSubmitted();
                    var otherEmailData = orderSubmitted.MakeOrderSubmitted(orderData);
                    _emailClient.SendEmail(otherEmailData.SendToMailMessage());

                }
                catch (Exception ex)
                {
                    var pauseEmail = new ErrorEmail();
                    var pauseErrorEmail = pauseEmail.CreateEmailData("Reorder", order.Info.OrderId.Value, order.Info.OrderName, ex);
                    _emailClient.SendEmail(pauseErrorEmail.SendToMailMessage());
                }


                return Ok(orderId);
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpPost]
        [Route("saveOrder")]
        public IHttpActionResult SaveOrderJson(Sightly.Models.tv.Order order)
        {
            try
            {
                var orderVersionId = _orderManager.SaveNewOrderBlob(order);
                return Ok(orderVersionId);
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getOrderJson")]
        public IHttpActionResult GetOrder(int newOrderBlobId)
        {
            try
            {
                var orderJson = _orderManager.GetNewOrderBlobById(newOrderBlobId);
                return Ok(orderJson);
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }
    

        [HttpGet]
        [Route("pullExistingOrder")]
        public IHttpActionResult GetExistingOrder(Guid orderId)
        {
            try
            {
                Sightly.Models.tv.Order existingOrder = _orderManager.GetExistingOrder(orderId);
                return Ok(existingOrder);
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("pullExistingOrderWithExtendedProperties")]
        public IHttpActionResult PullExistingOrderWithExtendedProperties(Guid orderId)
        {
            try
            {
                OrderExtended existingOrder = _orderManager.GetExistingOrderExtended(orderId);
                return Ok(existingOrder);
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getAccountName")]
        public IHttpActionResult GetAccountName(Guid accountId)
        {
            try
            {
                using (IDbConnection conn = new SqlConnection(_connString))
                {
                    string sql = @"SELECT AccountName
                                    FROM Accounts.Account
                                    WHERE AccountId = @AccountId;";

                    List<AccountNameLite> accountName = conn.Query<AccountNameLite>(sql, new { AccountId = accountId }).ToList();
                    return Ok(accountName);
                }
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getAdvertiserById")]
        public IHttpActionResult GetAdvertiserById(Guid advertiserId)
        {
            try
            {
                using (IDbConnection conn = new SqlConnection(_connString))
                {
                    string sql = @"SELECT 
                                a.AdvertiserId,
                                a.AccountId,
                                a.AdvertiserName,
                                a.AdvertiserCategoryId,
                                ac.AdvertiserCategoryName,
                                ac.Cpcv AS AdvertiserCategoryCostPerView,
                                advsc.AdvertiserSubCategoryId,
                                advsc.AdvertiserSubCategoryName,
                                advsc.Cpcv AS AdvertiserSubCategoryCostPerView,
                                a.AdvertiserMargin,
                                a.AdvertiserRefCode
                                FROM Accounts.Advertiser a
                                LEFT OUTER JOIN zLookups.AdvertiserCategory ac ON a.AdvertiserCategoryId = ac.AdvertiserCategoryId
                                LEFT OUTER JOIN zLookups.AdvertiserSubCategory advsc ON a.AdvertiserSubCategoryId = advsc.AdvertiserSubCategoryId
                                WHERE AdvertiserId = @AdvertiserId;";

                    AdvertiserExtended advertiser = conn.Query<AdvertiserExtended>(sql, new { AdvertiserId = advertiserId }).FirstOrDefault();
                    return Ok(advertiser);
                }
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getAdvertiserName")]
        public IHttpActionResult GetAdvertiserName(Guid advertiserId)
        {
            try
            {
                using (IDbConnection conn = new SqlConnection(_connString))
                {
                    string sql = @"SELECT AdvertiserName
                                    FROM Accounts.Advertiser
                                    WHERE AdvertiserId = @AdvertiserId;";

                    List<AdvertiserNameLite> advertiserName = conn.Query<AdvertiserNameLite>(sql, new { AdvertiserId = advertiserId }).ToList();
                    return Ok(advertiserName);
                }
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }


        [HttpGet]
        [Route("getCampaignName")]
        public IHttpActionResult GetCampaignName(Guid campaignId)
        {
            try
            {
                using (IDbConnection conn = new SqlConnection(_connString))
                {
                    string sql = @"SELECT CampaignName
                                    FROM Accounts.Campaign
                                    WHERE CampaignId = @CampaignId;";

                    List<CampaignNameLite> campaignName = conn.Query<CampaignNameLite>(sql, new { CampaignId = campaignId }).ToList();
                    return Ok(campaignName);
                }
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getOrderStatus")]
        public IHttpActionResult GetOrderStatus(Guid orderId)
        {
            try
            {
                using (IDbConnection conn = new SqlConnection(_connString))
                {
                    string sql = @"SELECT OrderStatusId
                                    FROM [Orders].[Order]
                                    WHERE OrderId = @OrderId;";

                    Guid statusId = conn.QuerySingle<Guid>(sql, new { OrderId = orderId });
                    return Ok(statusId);
                }
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }

        [HttpGet]
        [Route("getAdvertiserSubCategoriesByCategoryId")]
        public IHttpActionResult GetAdvertiserSubCategoriesByCategoryId(Guid categoryId)
        {
            try
            {
                using (IDbConnection conn = new SqlConnection(_connString))
                {
                    string sql = @"SELECT [AdvertiserSubCategoryId] AS [advertiserSubCategoryId]
                                ,[AdvertiserCategoryId] AS [advertiserCategoryId]
                                ,[AdvertiserSubCategoryName] AS [advertiserSubCategoryName]
                                ,[Cpcv] AS [value]
                                FROM [zLookups].[AdvertiserSubCategory]
                                WHERE [advertiserCategoryId] = @CategoryId;";
                    List<AdvertiserSubCategory> advertieSubCategories = conn.Query<AdvertiserSubCategory>(sql, new { CategoryId = categoryId }).ToList();
                    return Ok(advertieSubCategories);
                }
            }
            catch (Exception ex)
            {
                return GetResponseForException(ex);
            }
        }


        private void SaveOrderVersionForHistory(Sightly.Models.tv.Order order)
        {
            var userId = Guid.Parse(ActionContext.ActionArguments["UserId"].ToString());
            _orderManager.SaveOrderVersionForHistory(order, userId);
        }


        public class AccountNameLite
        {
            public string AccountName { get; set; }
        }

        public class AdvertiserNameLite
        {
            public string AdvertiserName { get; set; }
        }

        public class CampaignNameLite
        {
            public string CampaignName { get; set; }
        }


        public class OrderPause
        {
            public Guid orderId { get; set; }
        }
        public class OrderCancelled
        {
            public Guid OrderId { get; set; }
        }

        public class AdvertiserSubCategory
        {
            public Guid advertiserSubCategoryId { get; set; }
            public Guid advertiserCategoryId { get; set; }
            public string advertiserSubCategoryName { get; set; }
            public decimal value { get; set; }
        }

        public class AdvertiserExtended
        {
            [DataMember(Order = 1)]
            public Guid AdvertiserId { get; set; }

            [DataMember(Order = 2)]
            public Guid AccountId { get; set; }

            [DataMember(Order = 3)]
            public string AdvertiserName { get; set; }

            [DataMember(Order = 4)]
            public Guid AdvertiserCategoryId { get; set; }

            [DataMember(Order = 5)]
            public string AdvertiserCategoryName { get; set; }

            [DataMember(Order = 6)]
            public decimal AdvertiserCategoryCostPerView { get; set; }

            [DataMember(Order = 7)]
            public Guid? AdvertiserSubCategoryId { get; set; }

            [DataMember(Order = 8)]
            public string AdvertiserSubCategoryName { get; set; }

            [DataMember(Order = 9)]
            public decimal? AdvertiserSubCategoryCostPerView { get; set; }

            [DataMember(Order = 10)]
            public decimal AdvertiserMargin { get; set; }

            [DataMember(Order = 11)]
            public string AdvertiserRefCode { get; set; }

            
        }
    }

    
}
