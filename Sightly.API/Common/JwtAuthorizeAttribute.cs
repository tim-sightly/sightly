﻿using JWT;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Sightly.API.Common
{
    public class JwtAuthorizeAttribute : AuthorizeAttribute
    {
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            if (AuthorizeRequest(actionContext))
                return;

            HandleUnauthorizedRequest(actionContext);
        }

        protected override void HandleUnauthorizedRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            //Code to handle unauthorized request
            actionContext.Response = new HttpResponseMessage(HttpStatusCode.Unauthorized);
        }

        private bool AuthorizeRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            try
            {
                string token = GetTokenFromRequest(actionContext);

                if (token == null)
                    return false;

                JWT.Serializers.JsonNetSerializer serializer = new JWT.Serializers.JsonNetSerializer();
                IDateTimeProvider provider = new UtcDateTimeProvider();
                IJwtValidator validator = new JwtValidator(serializer, provider);
                IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
                IJwtDecoder decoder = new JwtDecoder(serializer, validator, urlEncoder);

                string secret = ConfigurationManager.AppSettings.Get("JwtSecret");
                var payload = decoder.DecodeToObject<IDictionary<string, object>>(token, secret, true);

                object userId;
                object userName;
                if (payload.TryGetValue("userId", out userId))
                {
                    actionContext.ActionArguments["UserId"] = userId.ToString();
                }

                if (payload.TryGetValue("userName", out userName))
                {
                    actionContext.ActionArguments["UserName"] = userName.ToString();
                }
            }
            catch (TokenExpiredException ex)
            {
                Trace.TraceError($"Message: {ex.Message}, InnerException: {ex.InnerException}");
                return false;
            }
            catch (SignatureVerificationException ex)
            {
                Trace.TraceError($"Message: {ex.Message}, InnerException: {ex.InnerException}");
                return false;
            }
            catch (Exception ex)
            {
                Trace.TraceError($"Message: {ex.Message}, InnerException: {ex.InnerException}");
                return false;
            }
            return true;
        }

        private string GetTokenFromRequest(System.Web.Http.Controllers.HttpActionContext actionContext)
        {
            //First look in headers for JWT
            if (!string.IsNullOrEmpty(actionContext.Request.Headers.Authorization?.Parameter))
            {
                return actionContext.Request.Headers.Authorization?.Parameter;
            }
            //Now look in query string for JWT
            else
            {
                var queryString = actionContext.Request
                    .GetQueryNameValuePairs()
                    .ToDictionary(x => x.Key, x => x.Value);

                if (queryString.ContainsKey("token") && queryString["token"] != null)
                {
                    return queryString["token"];
                }
                else
                {
                    return null;
                }
            }
        }
    }
}