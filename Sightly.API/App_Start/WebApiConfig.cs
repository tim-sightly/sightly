﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json.Serialization;
using Autofac;
using Autofac.Integration.WebApi;
using Sightly.IOC;
using System.Reflection;
using System.Web.Http.Cors;

namespace Sightly.API
{
    public static class WebApiConfig
    {
        private static List<string> AuthorizedUrls = new List<string>();

        public static void Register(HttpConfiguration config)
        {
            RegisterAuthorizedUrls();
            var cors = new EnableCorsAttribute(GetAuthorizedUrls(), "*", "*");
            config.EnableCors(cors);

            var builder = new ContainerBuilder();
            builder.RegisterApiControllers(Assembly.GetExecutingAssembly());

            (new AutofacConfiguration()).RegisterServices(builder);

            var container = builder.Build();
            config.DependencyResolver = new AutofacWebApiDependencyResolver(container);


            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private static string GetAuthorizedUrls()
        {
            var separator = ",";
            var combinedUrls = string.Join(separator, AuthorizedUrls);

            if (combinedUrls.EndsWith(separator))
                combinedUrls = combinedUrls.Substring(0, combinedUrls.Length - 1);

            return combinedUrls;
        }

        private static void RegisterAuthorizedUrls()
        {
            AuthorizedUrls.Add("http://localhost:3913");
            AuthorizedUrls.Add("http://localhost:65456");
            AuthorizedUrls.Add("https://localhost:44312");
            AuthorizedUrls.Add("http://localhost:3000");
            AuthorizedUrls.Add("http://localhost:5000");
            AuthorizedUrls.Add("http://localhost:8080");
            AuthorizedUrls.Add("http://localhost:8081");
            AuthorizedUrls.Add("http://localhost:8082");
            AuthorizedUrls.Add("http://localhost:8083");
            AuthorizedUrls.Add("http://localhost:8084");
            AuthorizedUrls.Add("http://localhost:64299");
            AuthorizedUrls.Add("http://localhost:3000");
            AuthorizedUrls.Add("http://intrawebbeta.azurewebsites.net");
            AuthorizedUrls.Add("http://intrawebprod.azurewebsites.net");
            AuthorizedUrls.Add("https://tv2intweb.azurewebsites.net");
            AuthorizedUrls.Add("https://intrawebint.azurewebsites.net");
            AuthorizedUrls.Add("https://tv2betaweb.azurewebsites.net");
            AuthorizedUrls.Add("https://intrawebbeta.azurewebsites.net");
            AuthorizedUrls.Add("https://tv2prodweb.azurewebsites.net");
            AuthorizedUrls.Add("https://intrawebprod.azurewebsites.net");
            AuthorizedUrls.Add("https://targetview.sightly.com");
            AuthorizedUrls.Add("https://advance-local-order-intake.azurewebsites.net");
            AuthorizedUrls.Add("https://advance-local-order-intake.sightly.com");
            AuthorizedUrls.Add("http://estimator-spa.azurewebsites.net");
            AuthorizedUrls.Add("https://estimator-spa.azurewebsites.net");
        }
    }
}
