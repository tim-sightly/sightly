﻿using System;

namespace Sightly.API.Models
{
    public class OrderData
    {
        public string CreaterName { get; set; }
        public string OrderName { get; set; }
        public string CampaignName { get; set; }
        public string AdvertiserName { get; set; }
        public string OrderRefCode { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public decimal TotalBudget { get; set; }
    }
}
