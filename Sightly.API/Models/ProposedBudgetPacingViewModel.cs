﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Sightly.API.Models
{
    public class ProposedBudgetPacingViewModel
    {
        public string UserEmail { get; set; }
        public List<CampaignPace> CampaignPaces { get; set; }
        
    }
}