﻿using System;

namespace Sightly.API.Models
{
    public class CampaignPace
    {
        public int? PacingId { get; set; }
        public DateTime? PacingDate { get; set; }
        public string CampaignName { get; set; }
        public long CustomerId { get; set; }
        public long CampaignId { get; set; }
        public decimal RecommendedBudget { get; set; }
        public float OverPacingRate { get; set; }
        public DateTime StartDate { get; set; }
        public decimal TotalBudget { get; set; }
        public int DaysRemaining { get; set; }
        public Guid BudgetGroupTimedBudgetId { get; set; }
    }
}