﻿namespace Sightly.API.Models.IntakeEmail
{
    public class Ad
    {
        public string AdName { get; set; }
        public string YouTubeUrl { get; set; }
        public string DisplayUrl { get; set; }
        public string DestinationUrl { get; set; }
        public int AdLength { get; set; }
        public string CompanionBanner { get; set; }
    }
}