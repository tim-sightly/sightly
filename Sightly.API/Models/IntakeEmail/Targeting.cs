﻿namespace Sightly.API.Models.IntakeEmail
{
    public class Targeting
    {
        public string AgeGroups { get; set; }
        public string GenderGroups { get; set; }
        public string ParentalGroups { get; set; }

        public string AudienceTargetingNotes { get; set; }

        public string KeyTerms { get; set; }
        public string CompetitorConquesting { get; set; }
        public string Objective { get; set; }
    }
}