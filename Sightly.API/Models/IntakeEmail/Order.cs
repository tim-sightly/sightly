﻿using System.Collections.Generic;

namespace Sightly.API.Models.IntakeEmail
{
    public class Order
    {
        public string Email { get; set; }
        public string OrderType { get; set; }
        public BasicInfo BasicInfo { get; set; }
        public Targeting Targeting { get; set; }
        public string Locations { get; set; }
        public List<Ad> Ads { get; set; }
        public string Notes { get; set; }
    }
}