﻿using System;

namespace Sightly.API.Models.IntakeEmail
{
    public class BasicInfo
    {
        public string AccountName { get; set; }
        public string AdvertiserName { get; set; }
        public string CampaignName { get; set; }
        public string OrderName { get; set; }
        public string OrderRefCode { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? Budget { get; set; }
    }
}