using System.Collections.Generic;
using Sightly.BusinessLayer.DomainObjects;

namespace Sightly.API.Models
{
    public class AlertKpiDataObject
    {
        public long CustomerId { get; set; }
        public string CustomerName { get; set; }
        public List<KpiDefinitionData> KpiDefinitions { get; set; }
        public AdwordsKpiStatsData AdwordsKpiStats { get; set; }
        public MoatKpiStatsData MoatKpiStats { get; set; }
        public NielsenKpiStatsData NielsenKpiStats { get; set; }
        public DoubleVerifyKpiStatsData DoubleVerifyKpiStats { get; set; }

        public AlertKpiDataObject(long customerId, string customerName)
        {
            CustomerId = customerId;
            CustomerName = customerName;
            KpiDefinitions = new List<KpiDefinitionData>();
        }

    }
}