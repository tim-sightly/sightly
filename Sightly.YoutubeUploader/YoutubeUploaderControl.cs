﻿using System;
using Microsoft.Azure.WebJobs;
using Sightly.Service.YoutubeUploader;
using Sightly.TargetViewDb.DAL.Repositories.Interfaces;

namespace Sightly.YoutubeUploader
{
    public class YoutubeUploaderControl
    {
        private readonly IUpDownLoader _upDownLoader;
        private readonly ITvDbRepository _tvDbRepository;

        public YoutubeUploaderControl(IUpDownLoader upDownLoader, ITvDbRepository tvDbRepository)
        {
            _upDownLoader = upDownLoader;
            _tvDbRepository = tvDbRepository;
        }

        [NoAutomaticTrigger]
        public void Execute()
        {
            Console.WriteLine($"Started: {DateTime.Now}");

            var getTheAssets = _tvDbRepository.GetVideoAssetVersionsWithNoYoutubeId();
            getTheAssets.ForEach(asset =>
            {
                _upDownLoader.DownloadAzureBlobAndUploadToYoutube(asset);
            });

            Console.WriteLine($"Completed: {DateTime.Now}");
        }
    }
}
