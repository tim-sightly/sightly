﻿using Autofac;
using Microsoft.Azure.WebJobs;
using Sightly.IOC;

namespace Sightly.YoutubeUploader
{
    public class Program
    {
        static void Main(string[] args)
        {
            var builder = new ContainerBuilder();
            (new AutofacConfiguration()).RegisterServices(builder);

            builder.RegisterType<YoutubeUploaderControl>();
            var container = builder.Build();

            var jobHostConfig = new JobHostConfiguration
            {
                JobActivator = new DiJobActivator(container)
            };

            var host = new JobHost(jobHostConfig);
            host.Call(typeof(YoutubeUploaderControl).GetMethod("Execute"));

        }
    }
}
